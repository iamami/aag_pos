:: Hide the command it is executing
echo OFF
:: Rename application title
title Run Smartcard Reader
:: Clear the command prompt screen
cls

echo Running Smartcard Reader ...
call npm run start