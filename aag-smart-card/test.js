const smartcard = require('smartcard');
const legacy = require('legacy-encoding');
const Devices = smartcard.Devices;
const devices = new Devices();
const CommandApdu = smartcard.CommandApdu;


const APDU_LIST = {
    cid	: {
        get: [0x00, 0xc0, 0x00, 0x00, 0x0D],
        set: [0x80, 0xB0, 0x00, 0x04, 0x02, 0x00, 0x0D],
        cleanData: (d)=>{
            console.log(d)
            return  (d+"").substr(0,13)
        }
    },
    full_name_th	: {
        get: [0x00, 0xc0, 0x00, 0x00, 0x64],
        set: [0x80, 0xB0, 0x00, 0x11, 0x02, 0x00, 0x64] ,
        cleanData: (d)=>{
            d = d.replace('�\u0000','')
            for(let i=0;i<100;i++)
                d = d.replace(' ','')
            var s = d.split('#')
            return s
        }
    },
    full_name_en	: {
        get: [0x00, 0xc0, 0x00, 0x00, 0x64],
        set: [0x80, 0xB0, 0x00, 0x75, 0x02, 0x00, 0x64] ,
        cleanData: (d)=>{
            d = d.replace('�\u0000','')
            for(let i=0;i<100;i++)
                d = d.replace(' ','')
            var s = d.split('#')
            return s
        }
    },
    birth	: {
        get: [0x00, 0xc0, 0x00, 0x00, 0x08],
        set: [0x80, 0xB0, 0x00, 0xD9, 0x02, 0x00, 0x08] ,
        cleanData: (d)=>{
            d = d.replace('�\u0000','')
            for(let i=0;i<100;i++)
                d = d.replace(' ','')
            let year = d.substr(0,4)
            let month = d.substr(4,2)
            let day = d.substr(6,2)
            return (parseInt(year)-543)+ "-"+month+"-"+day
        }
    },
    gender	: {
        get: [0x00, 0xc0, 0x00, 0x00, 0x01],
        set: [0x80, 0xB0, 0x00, 0xE1, 0x02, 0x00, 0x01] ,
        cleanData: (d)=>{
            d = d.replace('�\u0000','')
            return d==1?'M':'F'
        }
    },
    address	: {
        get: [0x00, 0xc0, 0x00, 0x00, 0x64],
        set: [0x80, 0xB0, 0x15, 0x79, 0x02, 0x00, 0x64] ,
        cleanData: (d)=>{
            d = d.replace('�\u0000','')
            for(let i=0;i<100;i++)
                d = d.replace(' ','')
            for(let i=0;i<100;i++)
                d = d.replace('#',' ')
            return d
        }
    },
    expire	: {
        get: [0x00, 0xc0, 0x00, 0x00, 0x08],
        set: [0x80, 0xB0, 0x01, 0x6F, 0x02, 0x00, 0x08] ,
        cleanData: (d)=>{
            d = d.replace('�\u0000','')
            for(let i=0;i<100;i++)
                d = d.replace(' ','')
            let year = d.substr(0,4)
            let month = d.substr(4,2)
            let day = d.substr(6,2)
            return (parseInt(year)-543)+ "-"+month+"-"+day
        }
    }
}

devices.on('device-activated', event => {
    const currentDevices = event.devices;
    let device = event.device;
    console.log(`Device '${device}' activated, devices: ${currentDevices}`);
    for (let prop in currentDevices) {
        console.log("Devices: " + currentDevices[prop]);
    }

    device.on('card-inserted', event => {
        let card = event.card;
        card.on('command-issued', event => {
            console.log(`Command '${event.command}' issued to '${event.card}' `);
        });

        card.on('response-received', event => {
            //console.log('response-received',event)
        });
        card
            .issueCommand(new CommandApdu(new CommandApdu({ bytes: [0x00, 0xA4, 0x04, 0x00, 0x08, 0xA0, 0x00, 0x00, 0x00, 0x54, 0x48, 0x00, 0x01] })))
            .then((response) => {
                var list = Object.keys(APDU_LIST)
                var position = 0
                var data_list = []
                var next = ()=>{
                    var key = list[position]
                    var apdu = APDU_LIST[key]
                    readData(card,key,apdu,(d)=>{

                        
                            data_list.push(d)
                        position++
                        if(position<list.length)
                            next()
                        else
                            console.log(data_list)
                    })
                }
                next()
                
            }).catch((error) => {
                console.error(error);
            });


    });
    device.on('card-removed', event => {
        console.log(`Card removed from '${event.name}' `);
    });

});

devices.on('device-deactivated', event => {
    console.log(`Device '${event.device}' deactivated, devices: [${event.devices}]`);
});


function readData(card,name,apdu,cb) {
    card
        .issueCommand((new CommandApdu({ bytes: apdu.set })))
        .then((response) => {
            card
                .issueCommand((new CommandApdu({ bytes: apdu.get })))
                .then((response) => {
                    var buffer = legacy.decode(response, "tis620");
                    if(apdu.cleanData)
                        buffer = apdu.cleanData(buffer)
                
                    cb({
                        [name]: buffer
                    }) 
                }).catch((error) => {
                    console.error(error);
                });
        }).catch((error) => {
            console.error(error);
        });

}
