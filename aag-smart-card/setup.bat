:: Hide the command it is executing
echo OFF
:: Rename application title
title Setup Smartcard
:: Clear the command prompt screen
cls

echo. && echo Upgrade NPM to v6.4.1 ...
call npm i -g npm@6.4.1

echo. && echo Install package ...
call npm install

echo. && echo Done !!! && echo.
pause