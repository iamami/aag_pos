var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var sockets = {}
const smartcard = require('smartcard');
const legacy = require('legacy-encoding');
const Devices = smartcard.Devices;
const devices = new Devices();
const CommandApdu = smartcard.CommandApdu;
const APDU_LIST = require('./apdu').apdu_list
let state = "CLOSE"

devices.on('device-activated', event => {
    state = 'READY'
    io.emit('READY', '');
  const currentDevices = event.devices;
  let device = event.device;
  console.log(`Device '${device}' activated, devices: ${currentDevices}`);
  for (let prop in currentDevices) {
      console.log("Devices: " + currentDevices[prop]);
  }

  device.on('card-inserted', event => {
      let card = event.card;
      card.on('command-issued', event => {
          console.log(`Command '${event.command}' issued to '${event.card}' `);
      });

      card.on('response-received', event => {
          //console.log('response-received',event)
      });
      card
          .issueCommand(new CommandApdu(new CommandApdu({ bytes: [0x00, 0xA4, 0x04, 0x00, 0x08, 0xA0, 0x00, 0x00, 0x00, 0x54, 0x48, 0x00, 0x01] })))
          .then((response) => {
              var list = Object.keys(APDU_LIST)
              var position = 0
              var _data = {}
              var next = ()=>{
                  var key = list[position]
                  var apdu = APDU_LIST[key]
                  readData(card,key,apdu,(d)=>{
                    _data[key] = d[key]
                      position++
                      if(position<list.length)
                          next()
                      else{

                        let data = {
                          name:_data.full_name_th[0]+""+ _data.full_name_th[1]+" " + _data.full_name_th[2]+" " + _data.full_name_th[3],
                          citizen_id: _data.citizen_id,
                          birth_date: _data.birth_date,
                          address: _data.address[0]+" "+_data.address[1]+" "+_data.address[2]+" "+_data.address[3]+" "+_data.address[4]+" "+_data.address[5],
                          city: _data.address[6],
                          province: _data.address[7],
                          gender: _data.gender
                        }
                        io.emit('DATA', data);
                      }
                        
                  })
              }
              next()
              
          }).catch((error) => {
              console.error(error);
          });


  });
  device.on('card-removed', event => {
      console.log(`Card removed from '${event.name}' `);
      io.emit('READY', '');
  });

});

devices.on('device-deactivated', event => {
  console.log(`Device '${event.device}' deactivated, devices: [${event.devices}]`);
  state = 'CLOSE'
  io.emit('CLOSE', '');
});

io.on('connection', function(socket){
  console.log('connection',socket.id)
  sockets[socket.id] = socket

  io.emit(state, '');
  socket.on('disconnect',function(){
    console.log('disconnected',this.id)
  });
});

http.listen(3333, function(){
  console.log('listening on *:3333');
});

function readData(card,name,apdu,cb) {
  card
      .issueCommand((new CommandApdu({ bytes: apdu.set })))
      .then((response) => {
          card
              .issueCommand((new CommandApdu({ bytes: apdu.get })))
              .then((response) => {
                  var buffer = legacy.decode(response, "tis620");
                  if(apdu.cleanData)
                      buffer = apdu.cleanData(buffer)
                  cb({
                      [name]: buffer
                  }) 
              }).catch((error) => {
                  console.error(error);
              });
      }).catch((error) => {
          console.error(error);
      });

}
