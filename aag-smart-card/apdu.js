exports.apdu_list =  {
    citizen_id	: {
        get: [0x00, 0xc0, 0x00, 0x00, 0x0D],
        set: [0x80, 0xB0, 0x00, 0x04, 0x02, 0x00, 0x0D],
        cleanData: (d)=>{
            console.log(d)
            return  (d+"").substr(0,13)
        }
    },
    full_name_th	: {
        get: [0x00, 0xc0, 0x00, 0x00, 0x64],
        set: [0x80, 0xB0, 0x00, 0x11, 0x02, 0x00, 0x64] ,
        cleanData: (d)=>{
            d = d.replace('�\u0000','')
            for(let i=0;i<100;i++)
                d = d.replace(' ','')
            var s = d.split('#')
            return s
        }
    },
    full_name_en	: {
        get: [0x00, 0xc0, 0x00, 0x00, 0x64],
        set: [0x80, 0xB0, 0x00, 0x75, 0x02, 0x00, 0x64] ,
        cleanData: (d)=>{
            d = d.replace('�\u0000','')
            for(let i=0;i<100;i++)
                d = d.replace(' ','')
            var s = d.split('#')
            return s
        }
    },
    birth_date	: {
        get: [0x00, 0xc0, 0x00, 0x00, 0x08],
        set: [0x80, 0xB0, 0x00, 0xD9, 0x02, 0x00, 0x08] ,
        cleanData: (d)=>{
            d = d.replace('�\u0000','')
            for(let i=0;i<100;i++)
                d = d.replace(' ','')
            let year = d.substr(0,4)
            let month = d.substr(4,2)
            let day = d.substr(6,2)
            return (parseInt(year)-543)+ "-"+month+"-"+day
        }
    },
    gender	: {
        get: [0x00, 0xc0, 0x00, 0x00, 0x01],
        set: [0x80, 0xB0, 0x00, 0xE1, 0x02, 0x00, 0x01] ,
        cleanData: (d)=>{
            d = d.replace('�\u0000','')
            return d==1?'MA':'FE'
        }
    },
    address	: {
        get: [0x00, 0xc0, 0x00, 0x00, 0x64],
        set: [0x80, 0xB0, 0x15, 0x79, 0x02, 0x00, 0x64] ,
        cleanData: (d)=>{
            d = d.replace('�\u0000','')
            for(let i=0;i<100;i++)
                d = d.replace(' ','')
            return d.split('#')
        }
    },
    expire	: {
        get: [0x00, 0xc0, 0x00, 0x00, 0x08],
        set: [0x80, 0xB0, 0x01, 0x6F, 0x02, 0x00, 0x08] ,
        cleanData: (d)=>{
            d = d.replace('�\u0000','')
            for(let i=0;i<100;i++)
                d = d.replace(' ','')
            let year = d.substr(0,4)
            let month = d.substr(4,2)
            let day = d.substr(6,2)
            return (parseInt(year)-543)+ "-"+month+"-"+day
        }
    }
}