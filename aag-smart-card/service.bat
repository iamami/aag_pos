:: Hide the command it is executing
echo OFF
:: Rename application title
title Enable Service for Smartcard
:: Clear the command prompt screen
cls

echo. && echo Set auto start for smartcard service ...
sc config SCardSvr start= auto

echo. && echo Done !!! && echo.
pause