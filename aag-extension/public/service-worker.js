const VERSION = '1'
const API_VERSION = '1'
const CACHE_KEY = `cache-v${VERSION}`
const CACHE_API = `cache-api-v${API_VERSION}`
const assetsToCache = [
  '/',
  '/static/css/styles.scss',
  '/static/images/icons/icon-192x192.png',
  '/static/images/icons/icon-512x512.png',
  '/static/images/icons/icon-144x144.png',
  '/favicon.ico'
]

console.log('Load my service-worker')
self.addEventListener('install', event => {
  event.waitUntil(
    caches.open(CACHE_KEY)
      .then(cache => cache.addAll(assetsToCache)).then(() => self.skipWaiting()).catch(err => console.log('install fail with:', err))
  )
})

self.addEventListener('fetch', event => {
  // event.respondWith(
  //   caches.match(event.request).then(cacheResp => {
  //     return cacheResp || fetch(event.request).then(response => {
  //       return caches.open(CACHE_API).then(cache => {
  //         cache.put(event.request, response.clone())
  //         return response
  //       })
  //     }).catch(err => console.log('fetch fail with:',err))
  //   })
  // )

  // var req = event.request.clone();
  if (event.request.method == "GET") {
    event.respondWith(
      fetch(event.request).then(res => {
        return caches.open(CACHE_API).then(cache => {
          cache.put(event.request, res.clone())
          return res
        })
      }).catch(() => {
        console.log('search in cache');
        return caches.match(event.request).catch(err => console.log(`get cache error: ${err}`));
      })
    )
  }

})

self.addEventListener('activate', event => {
  const cacheWhitelist = [CACHE_KEY, CACHE_API]

  event.waitUntil(
    caches.keys().then(keyList => {
      return Promise.all(keyList.map(function(key) {
        if (cacheWhitelist.indexOf(key) === -1) {
          return caches.delete(key)
        }
      }))
    })
  )
  return self.clients.claim()
})