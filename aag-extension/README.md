# AAGold - POS

### Use NVM
- 

### API Server
- build `docker-compose -f "docker-compose.yml" up -d --build`
- shell `docker exec -it service_aagold /bin/sh -c "[ -e /bin/bash ] && /bin/bash || /bin/sh"`
- logs `docker logs -f service_aagold`

### Starts the development server.
- `npm install`
- `npm start`
- `npm run dev`

### Builds the app for staging.
- `npm install`
- `npm run build:staging`
- `npm run start:staging`

### Builds the app for Production And Deploy to Production
- `npm install`
- `npm run build:production`
- `npm run start:production`