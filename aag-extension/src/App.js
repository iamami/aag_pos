/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, {Component} from 'react';
import Login from './components/Layout/Login'
import Main from './components/Layout/Main'
import { connect } from 'react-redux'
import {auth} from './actions'

import { 
    Container,Dimmer, Loader
  } from 'semantic-ui-react';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loader: true
        }
        this.onAccess = this
            .onAccess
            .bind(this);
        this.onLogout = this
            .onLogout
            .bind(this);
    }

    componentDidMount(){
        auth(this.props.dispatch)
    }

    onLogout() {
        auth(this.props.dispatch)
    }

    onAccess() {
        auth(this.props.dispatch)
    }

    render() {
        const {auth} = this.props
        return (<div>
            {!auth.is_ready ? 
            <Dimmer active inverted>
                <Loader inverted>Loading</Loader>
            </Dimmer>
            :
            <Container fluid>
                {auth.is_login ? <Main onLogout={this.onLogout}/> : <Login onAccess={this.onAccess}/>}
            </Container>}</div>
        )
    }
}

const mapStateToProps = ({auth}) =>{
    return ({auth})
  }
  export default connect(
    mapStateToProps
  )(App)