/*eslint eqeqeq: "off"*/
import Utility from '../Utility'
import Settings from '../Settings'


export const open_modal_lease_product = (is_open) => ({
  type: 'OPEN_MODAL_LEASE_PRODUCT',
  data: {
    name: 'lease_product',
    is_open: is_open
  }
})
export const data_list = (name,data) => ({
  type: 'DATA_LIST',
  data: {
    name: name,
    data: data
  }
})

export const loadBranch = async(dispatch) => {

  const branches = await Utility.getAsync(Settings.baseUrl + '/branches/?is_enabled=1')
  dispatch({
    type: 'BRANCHES',
    data: branches.data
  })
}

export const activations_branch = (data) => {

  return{
    type: 'ACTIVATIONS_BRANCH',
    data
  }
}

export const auth = async(dispatch) => {

  const r = await Utility.getAsync(Settings.baseUrl + '/user_profile/')
  dispatch({
    type: 'SET_AUTH',
    data: {
      is_ready: true,
      is_login: r.status_code==200,
      staff: r.data.staff,
      branch: r.data.branch,
      user: r.data.user,
      role: r.data.role
    }
  })
}

export const logout = async(dispatch) => {

  const r = await Utility.getAsync(Settings.baseUrl + '/user_profile/')
  dispatch({
    type: 'SET_AUTH',
    data: {
      is_ready: true,
      is_login: r.status_code==200,
      staff: r.data.staff,
      branch: r.data.branch,
      user: r.data.user,
      role: r.data.role
    }
  })
}

export const loadProductName= (cb) => {

  Utility.get(Settings.baseUrl + '/product_name/?is_enabled=1',(s,r,c)=>{
    cb({
      type: 'PRODUCTS_NAME',
      data: r
    })
  });
}

export const loadBills = async(dispatch,querystring) => {
  const res = await Utility.getAsync(Settings.baseUrl + '/bills/?'+querystring)
  dispatch({
    type: 'SET_BILL_LIST',
    list: res.data,
    filter: querystring
  })
}

export const loadLedger = async(dispatch,querystring) => {
  const res = await Utility.getAsync(Settings.baseUrl + '/ledger/?'+querystring)
  dispatch({
    type: 'SET_LEDGER_LIST',
    list: res.data
  })
}

export const loadLedgerCategory = async(dispatch,querystring) => {
  const res = await Utility.getAsync(Settings.baseUrl + '/ledger_category/?is_enabled=1')
  dispatch({
    type: 'SET_LEDGER_CATEGORY_LIST',
    list: res.data
  })
}

export const lease_modal_edit = async(dispatch,lease_id) => {
  const res = await Utility.getAsync(Settings.baseUrl + `/lease/${lease_id}/`)
  dispatch({
    type: 'LEASE_MODAL_EDIT_OPEN',
    data: res.data
  })
}

export const lease_modal_close = () => {
  
  return {
    type: 'LEASE_MODAL_CLOSE'
  }
}

export const lease_modal_add_open = () => {
  
  return {
    type: 'LEASE_MODAL_ADD_OPEN'
  }
}