/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import {
    Segment
} from 'semantic-ui-react';
import { connect } from 'react-redux'
import Utility from '../../Utility';
import Settings from '../../Settings';
import moment from 'moment';
import CanvasJSChart from '../../canvasjs.react'
var dataPoints =[];
class SellYear extends Component {
    constructor(props) {
        super(props);

        this.state = {
            search: {
                start_date: Utility.formatYear(moment()) + '-01-01',
                end_date: Utility.formatYear(moment()) + '-12-31',
                branch: props.auth.branch.id
            },
            items: []
        }
    }

    componentDidMount() {
        var bills = Utility.getFetch(Settings.baseUrl + '/bills/?kind=SE&is_enabled=1&'+Utility.jsonToQueryString(this.state.search));
        Promise.all([bills]).then((values) => {
            this.setState({
                loading: false,
                items: values[0]
            });


            let items2 = values[0]
            let group_month = {}
            let labels = []
            let data = []
            for (let i in items2) {
                let item = items2[i];
                let k = Utility.formatMonthYear(item.bill_date)
                if (group_month[k] == null) {
                    group_month[k] = {
                        items: [],
                        date: item.bill_date
                    }
                    labels.push(k)
                }
                group_month[k].items.push(item);
            }

            let items = []
            dataPoints = []
            for (let i in group_month) {
                items.push(group_month[i])
                let total = 0
                for (let j in group_month[i].items) {
                    total += parseFloat(group_month[i].items[j].total)
                }
                dataPoints.push({
                    y: total,
                    x: new Date(group_month[i].date)
                })
                
            }

            // dataPoints = [
            //     {
            //       "x": new Date(1483228800000),
            //       "y": 8561.3
            //     },
            //     {
            //       "x": new Date(1485907200000),
            //       "y": 8879.6
            //     },
            //     {
            //       "x": new Date(1488326400000),
            //       "y": 9173.75
            //     },
            //     {
            //       "x": new Date(1491004800000),
            //       "y": 9304.05
            //     },
            //     {
            //       "x": new Date(1493596800000),
            //       "y": 9621.25
            //     },
            //     {
            //       "x": new Date(1496275200000),
            //       "y": 9520.9
            //     },
            //     {
            //       "x": new Date(1498867200000),
            //       "y": 10077.1
            //     },
            //     {
            //       "x": new Date(1501545600000),
            //       "y": 9917.9
            //     },
            //     {
            //       "x": new Date(1504224000000),
            //       "y": 9788.6
            //     },
            //     {
            //       "x": new Date(1506816000000),
            //       "y": 10335.3
            //     },
            //     {
            //       "x": new Date(1509494400000),
            //       "y": 10226.55
            //     },
            //     {
            //       "x": new Date(1512086400000),
            //       "y": 10530.7
            //     }
            //   ]
            if(this.chart)
                this.chart.render();
        });


    }
    render() {

        const options = {
			theme: "light2",
			title: {
				text: "สรุปยอดขายทองปี "+Utility.formatYear(moment())
			},
			axisY: {
				title: "ยอดขาย",
				prefix: "฿",
				includeZero: false
			},
			data: [{
				type: "line",
				xValueFormatString: "MMM YYYY",
				yValueFormatString: "#,##0.00",
				dataPoints: dataPoints
			}]
        }

        console.log('dataPoints',dataPoints)
        return (<div>
            <Segment >
                <CanvasJSChart options = {options} onRef={ref => this.chart = ref} />
             </Segment>
        </div>)
    }
}

const mapStateToProps = state =>{
    
    return ({
      branches: state.branches,
      auth: state.auth
    })
  }
  export default connect(
    mapStateToProps
  )(SellYear)
