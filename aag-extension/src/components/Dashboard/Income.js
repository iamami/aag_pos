/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, {Component} from 'react';
import {
    Segment,
    Header,
    Grid,
    Statistic
} from 'semantic-ui-react';
import moment from 'moment';
import Settings from '../../Settings';
import Utility from '../../Utility';

import { connect } from 'react-redux'


class Gole extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
            income: 0,
            expenses: 0,
            total: 0
        }
        this.loadLedger = this
            .loadLedger
            .bind(this)
    }

    onSubmit(e) {
        e.preventDefault();
        this.setState({visible: true})
        localStorage.setItem("goldpric", JSON.stringify({gold_bar_sell: this.state.gold_bar_sell, gold_bar_buy: this.state.gold_bar_buy, gold_ornaments_buy: this.state.gold_ornaments_buy, time: moment()}));

        setTimeout(() => {
            this.setState({visible: false})
        }, 1000)
        if (this.props.onSubmit) {
            this
                .props
                .onSubmit()
        }
    }

    componentDidMount() {

        const {auth} = this.props
        const {branch} = auth
        this.loadLedger(branch)
    }

    loadLedger(branch) {

        let s1 = {
            start_date: Utility.formatDate2(moment()),
            end_date: Utility.formatDate2(moment())
        }
        s1.branch = branch.id
        let s = Utility.jsonToQueryString(s1)
        var ledger = Utility.getFetch(Settings.baseUrl + '/ledger/?is_enabled=1&' + s);
        Promise
            .all([ledger])
            .then((values) => {

                let income = 0
                let expenses = 0
                let n_income = 0
                let n_expenses = 0
                for (let i in values[0]) {

                    if (values[0][i].kind == 'IN') {
                        income += parseFloat(values[0][i].total)
                        n_income++;
                    } else {
                        expenses += parseFloat(values[0][i].total)
                        n_expenses++
                    }
                }

                this.setState({
                    income: income,
                    expenses: expenses,
                    total: income - expenses,
                    n_income: n_income,
                    n_expenses: n_expenses
                })

            })
    }

    render() {
        return (
            <div>
                {this.props.mod == null
                    ? <Grid columns={2}>
                            <Grid.Row>
                                <Grid.Column>
                                    <Segment color='green'>
                                        <Header as='h2'>รายรับ</Header>
                                        <right>
                                            <Statistic size='tiny' color='green'>
                                                <Statistic.Value >{Utility.priceFormat(this.state.income)}</Statistic.Value>
                                            </Statistic>
                                        </right>
                                    </Segment>
                                </Grid.Column>
                                <Grid.Column>
                                    <Segment color='red'>
                                        <Header as='h2'>รายจ่าย</Header>
                                        <right>
                                            <Statistic size='tiny' color='red'>
                                                <Statistic.Value >{Utility.priceFormat(this.state.expenses)}</Statistic.Value>
                                            </Statistic>
                                        </right>
                                    </Segment>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    : <Segment color='yellow'>
                        <Header as='h2'>คงเหลือ</Header>
                        <right>
                            <Statistic size='tiny' color='yellow'>
                                <Statistic.Value >{Utility.priceFormat(this.state.total)}</Statistic.Value>
                            </Statistic>
                        </right>
                    </Segment>}
            </div>
        );
    }
}

const mapStateToProps = state =>{
    
    return ({
      branches: state.branches,
      auth: state.auth
    })
  }
  export default connect(
    mapStateToProps
  )(Gole)
