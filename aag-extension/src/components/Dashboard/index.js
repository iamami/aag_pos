/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/
import React, {Component} from 'react';
import {Grid,Header} from 'semantic-ui-react';
import GoldPrice from './GoldPrice';
import StockProduct from './StockProduct'
import StockCategory from './StockCategory'
import SellYear from './SellYear'
import Income from './Income';
import AmountCustomer from './AmountCustomer';
import AmountProduct from './AmountProduct';
import DropdownBranch from '../Widget/DropDown/DropDownBranch'
import {activations_branch} from '../../actions'
import { connect } from 'react-redux'


class Dashboard extends Component {

  constructor(props) {
        super(props);

        this.state ={
            branch: props.auth.branch.id
        }
    }


    handlerInput(e,{value}){

        if(value!=this.state.branch){
            const {branches} = this.props
            const branch = branches.find((item)=>item.id==value)
            this.props.handleBranchChange(branch)
        }

      }

    render() {
        const {branch,role} = this.props.auth
        return (
            <div>
                 <Grid>
                    <Grid.Row>
                        <Grid.Column width={6}>
                           <Header floated='left' as='h3'>
                                <div style={{padding: '7px'}} className="pull-left">Dashboard </div>
                                <div className="pull-left">{role==='A' && <DropdownBranch  fluid size='small' fluid selection onChange={this.handlerInput.bind(this)} name="branch" value={branch.id}  />}</div>
                            </Header>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width={10}>
                            <Income/>
                            <Grid columns={2}>
                                <Grid.Row>
                                    <Grid.Column>
                                        <AmountCustomer/>
                                        <AmountProduct/>
                                    </Grid.Column>
                                    <Grid.Column>
                                        <Income mod="balance"/>
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                        </Grid.Column>
                        <Grid.Column width={6}>
                            <GoldPrice/>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width={8}>
                            <StockProduct/>
                            <StockCategory/>
                        </Grid.Column>
                        <Grid.Column width={8}>
                            <SellYear/>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
            
        );
    }
}

const mapStateToProps = state =>{
    return ({
      auth: state.auth,
      branches: state.branches
    })
  }
  const mapDispatchToProps = dispatch => ({
    handleBranchChange: (branch) => {
      dispatch(activations_branch(branch))
      
    }
    
  })
  export default connect(
    mapStateToProps,mapDispatchToProps
  )(Dashboard)
  
