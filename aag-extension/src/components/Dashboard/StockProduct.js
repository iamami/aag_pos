/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import { connect } from 'react-redux'
import {
    Form, Segment, Header, Button, Dimmer, Loader, Sidebar, Message, Icon, Input, Label, Grid, Statistic
} from 'semantic-ui-react';
import moment from 'moment';
import Settings from '../../Settings';
import Utility from '../../Utility';

import {
    Table,
    Column,
    Cell
} from 'fixed-data-table';


class ItemsCell extends Component {
    constructor(props) {
        super(props);

    }
    render() {
        const { rowIndex, field, data, ...props } = this.props;
        return (
            <Cell {...props} >
                <div className={this.props.textAlign}>{data[rowIndex][field]}</div>
            </Cell>
        );
    }
}

class StockProduct extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
            items: []
        }
    }

    onSubmit(e) {
        e.preventDefault();
        this.setState({
            visible: true
        })
        localStorage.setItem("goldpric", JSON.stringify({
            gold_bar_sell: this.state.gold_bar_sell,
            gold_bar_buy: this.state.gold_bar_buy,
            gold_ornaments_buy: this.state.gold_ornaments_buy,
            time: moment()
        }));

        setTimeout(() => {
            this.setState({
                visible: false
            })
        }, 1000)
        if (this.props.onSubmit) {
            this.props.onSubmit()
        }
    }

    componentDidMount() {
        const {branch} = this.props.auth
        this.setState({
            loader_active: true
        });

        var stock_product = Utility.getFetch(Settings.baseUrl + '/stock_product/?dl=out_date&is_enabled=1&branch=' + branch.id);
        Promise.all([stock_product]).then((values) => {
            this.stock_product = values[0];
            this.setState({
                items: this.setFieldValue(values[0])
            });
        });

        this.setState({
            table_width: window.innerWidth
        });
    }

    setFieldValue(items) {

        for (let i = 0; i < items.length; i++) {
            items[i]['in_date_title'] = Utility.formatDate(items[i].in_date)
            items[i]['out_date_title'] = Utility.formatDate(items[i].out_date)
            items[i]['product_name'] = items[i].product.name
            items[i]['product_code'] = items[i].product.code
            items[i]['branch_name'] = items[i].branch.name
            items[i]['product_kind'] = items[i].product.kind.name
            items[i]['product_weight'] = Utility.weightFormat(items[i].product.weight)
            items[i]['product_category'] = items[i].product.category.name;
            items[i]['amount_title'] = Utility.numberFormat(parseInt(items[i].amount))
            items[i]['date'] = Utility.formatDate(items[i].out_date)
            items[i]['time'] = Utility.formatTime(items[i].out_date)
        }

        return items;
    }


    render() {

        const items = this.state.items
        return (

            <Segment >
                <Header as='h4'>สต็อกทองใหม่
                    <Header.Subheader>ขายล่าสุด </Header.Subheader>
                </Header>
                <div id='root-table'>
                    <Table
                        rowsCount={items.length}
                        rowHeight={35}
                        headerHeight={35}
                        width={this.state.table_width}
                        height={Settings.table_hiegh}>
                        <Column
                            header={<Cell>รหัสสินค้า</Cell>}
                            cell={
                                <ItemsCell data={items} field="product_code"  />
                            }
                            width={100}
                        />

                        <Column
                            header={<Cell>ชื่อสินค้า</Cell>}
                            cell={
                                <ItemsCell data={items} field="product_name"  />
                            }
                            width={200}
                        />
                        <Column
                            header={<Cell className='text-right'>จำนวน</Cell>}
                            cell={
                                <ItemsCell data={items} field="amount_title" textAlign='text-right'  />
                            }
                            width={80}
                        />
                        <Column
                            header={<Cell className='text-center'>วันที่ขายล่าสุด</Cell>}
                            cell={
                                <ItemsCell data={items} field="date" textAlign='text-center'  />
                            }
                            width={100}
                        />
                        <Column
                            header={<Cell className='text-center'>เวลา</Cell>}
                            cell={
                                <ItemsCell data={items} field="time" textAlign='text-center'  />
                            }
                            width={60}
                        />

                    </Table></div>
            </Segment>
        );
    }
}
const mapStateToProps = state =>{
    
    return ({
      branches: state.branches,
      auth: state.auth
    })
  }
  export default connect(
    mapStateToProps
  )(StockProduct)
