/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import { connect } from 'react-redux'
import {
    Form, Segment, Header, Button, Dimmer, Loader, Sidebar, Message, Icon, Input, Label, Grid, Statistic
} from 'semantic-ui-react';
import moment from 'moment';
import Settings from '../../Settings';
import Utility from '../../Utility';
import GoldPrice from './GoldPrice';
import ModalGoldPrice from '../GoldPrice/ModalGoldPrice'

import {
    Table,
    Column,
    Cell
} from 'fixed-data-table';


class ItemsCell extends Component {
    constructor(props) {
        super(props);

    }
    render() {
        const { rowIndex, field, data, ...props } = this.props;
        return (
            <Cell {...props} >
                <div className={this.props.textAlign}>{data[rowIndex][field]}</div>
            </Cell>
        );
    }
}

class StockCategory extends Component {

    constructor(props) {
        super(props);
       
        this.state = {
            items: []
        }
    }

    onSubmit(e) {
        e.preventDefault();
        this.setState({
            visible: true
        })
        localStorage.setItem("goldpric", JSON.stringify({
            gold_bar_sell: this.state.gold_bar_sell,
            gold_bar_buy: this.state.gold_bar_buy,
            gold_ornaments_buy: this.state.gold_ornaments_buy,
            time: moment()
        }));

        setTimeout(() => {
            this.setState({
                visible: false
            })
        }, 1000)
        if (this.props.onSubmit) {
            this.props.onSubmit()
        }
    }

    componentDidMount() {
        const {branch} = this.props.auth
        this.setState({
            loader_active: true
        });

        var stock_category = Utility.getFetch(Settings.baseUrl + '/stock_category/?is_enabled=1&branch=' + branch.id);
        Promise.all([stock_category]).then((values) => {
            this.stock_category = values[0];

            this.setState({
                items: this.setFieldValue(values[0])
            });
        });
        
        this.setState({
            table_width: window.innerWidth
        });
    }

    setFieldValue(items) {

        for (let i = 0; i < items.length; i++) {
            items[i]['in_date_title'] = Utility.formatDate(items[i].in_date)
            items[i]['out_date_title'] = Utility.formatDate(items[i].out_date)

            let d = parseFloat(items[i].category.weight)
            items[i]['category'] = items[i].category.name
            items[i]['branch'] = items[i].branch.name
            items[i]['average'] = items[i].total == 0 ? 0 : Utility.weightFormat(parseFloat(items[i].total) / (parseFloat(items[i].weight) / d))
        }

        return items;
    }


    render() {

        const items = this.state.items
        return (

            <Segment >
                <Header as='h4'>สต็อกทองเก่า</Header>

                <div id='root-table'>
                    <Table
                        rowsCount={items.length}
                        rowHeight={35}
                        headerHeight={35}
                        width={this.state.table_width}
                        height={Settings.table_hiegh}>
                        <Column
                            header={<Cell>กลุ่มสินค้า</Cell>}
                            cell={
                                <ItemsCell data={items} field="category" />
                            }
                            width={250}
                        />
                        <Column
                            header={<Cell className='text-right'>น.น.รวม(g.)</Cell>}
                            cell={
                                <ItemsCell data={items} field="weight" textAlign='text-right' />
                            }
                            width={150}
                        />

                        <Column
                            header={<Cell className='text-right'>ราคาทองเฉลี่ย</Cell>}
                            cell={
                                <ItemsCell data={items} field="average" textAlign='text-right' />
                            }
                            width={160}
                        />

                    </Table></div>
            </Segment>
        );
    }
}

const mapStateToProps = state =>{
    
    return ({
      branches: state.branches,
      auth: state.auth
    })
  }
  export default connect(
    mapStateToProps
  )(StockCategory)
