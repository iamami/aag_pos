/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import {
    Form, Segment, Header, Button, Message, Loader, Dimmer, Icon, Dropdown, Input, Modal, Grid, Popup, Label, Table
} from 'semantic-ui-react';
/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import Utility from '../../Utility';
import Settings from '../../Settings';

class ProductDetailPrintPreview extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        let data = this.props.data

        let title = 'รายงานซื่อลูกค้าออมทอง';
        let filename = 'saving-'+Utility.formatDate3(data.start_date)+'-to-'+Utility.formatDate3(data.end_date);
      
        const divStyle = {
            color: '#111',
            'font-size': '10px',
            width: '100%',
            'line-height': '20px',
            'font-family': 'monospace'
        };
        const textRight = {
            'text-align': 'right'
        }
        const textCenter = {
            'text-align': 'center'
        }
        const textU ={
            'text-decoration': 'underline'
        }
  
        return (<div>
            <Modal  open={true} onClose={this.close} size='fullscreen' /*dimmer='blurring'*/>
            <Modal.Header>Preview</Modal.Header>
                <Modal.Content className='scrolling'>

                    <div id='view-print'>
                    <div id='paperA4-portrait'>
                        <Table basic id='table-to-xls' style={divStyle}>
                            <Table.Header>
                                <Table.Row >
                                    <Table.HeaderCell colSpan='6'><center>{title}</center></Table.HeaderCell>
                                </Table.Row>
                                <Table.Row>
                                <Table.HeaderCell style={textCenter}>ลำดับที่</Table.HeaderCell>
                                    <Table.HeaderCell style={textCenter}>เลขที่บิล</Table.HeaderCell>
                                    <Table.HeaderCell style={textCenter}>วันที่ออม</Table.HeaderCell>
                                    <Table.HeaderCell style={textCenter}>ชื่อลูกค้า</Table.HeaderCell>
                                    <Table.HeaderCell style={textRight}>จำนวนเงิน</Table.HeaderCell>
                                    <Table.HeaderCell style={textRight}>นำหนักที่ได้/กรัม</Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>

                            <Table.Body>
                                {this.props.items.map((row, i) => <Table.Row key={i}>
                                <Table.Cell style={textCenter}>{i+1}</Table.Cell>
                                    <Table.Cell style={textCenter}>{row.number}</Table.Cell>
                                    <Table.Cell style={textCenter}>{row.saving_date}</Table.Cell>
                                    <Table.Cell style={textCenter}>{row.customer_name}</Table.Cell>
                                    <Table.Cell style={textRight}>{row.price_view}</Table.Cell>
                                    <Table.Cell style={textRight}>{row.weight}</Table.Cell>
                                </Table.Row>)}

                                <Table.Row >
                                    <Table.HeaderCell style={textRight} colSpan='4'>รวม</Table.HeaderCell>
                                    <Table.HeaderCell style={textRight}><div style={textU}>{Utility.priceFormat(data.total_amount)}</div></Table.HeaderCell>
                                    <Table.HeaderCell style={textRight}><div style={textU}>{Utility.weightFormat(data.total_weight)}</div></Table.HeaderCell>
                                    
                                </Table.Row>
                            </Table.Body>
                        </Table>
                    </div>
                    </div>
                </Modal.Content>
                <Modal.Actions>
                    <ReactHTMLTableToExcel
                        id="test-table-xls-button"
                        className="ui primary button small"
                        table="table-to-xls"
                        filename={filename}
                        sheet={title}
                        buttonText="Download as XLS" >
                    </ReactHTMLTableToExcel>

                    <Button primary icon='print' size='small' onClick={() => {
                        var mywindow = window.open('', 'PRINT', 'height=600,width=1024');
                        mywindow.document.write('<html><head><title>' +title + '</title>');
                        mywindow.document.write('</head><body style="font-size: 8px;font-family: Lato,\'Helvetica Neue\',Arial,Helvetica,sans-serif">');
                        mywindow.document.write(document.getElementById('view-print').innerHTML);
                        mywindow.document.write('</body></html>');
                        mywindow.document.close(); // necessary for IE >= 10
                        mywindow.focus(); // necessary for IE >= 10*/
                        mywindow.print();
                        mywindow.close();
                    }} labelPosition='right' content='Print' />

                    <Button size='small' type='button' onClick={() => { this.props.onClose() }}>ปิด</Button>
                </Modal.Actions>
            </Modal>
        </div>)
    }
}

export default ProductDetailPrintPreview;