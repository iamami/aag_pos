/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import {
  Form, Segment, Header, Button, Message, Loader, Dimmer, Icon, Dropdown, Input, Modal, Grid, Popup, Menu,
  Label, Search,Divider
} from 'semantic-ui-react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import {
  Table,
  Column,
  Cell
} from 'fixed-data-table';
import {
  BrowserRouter as Router,
  Route,
  Link,
} from 'react-router-dom';
import CustomerModal from '../Customer/CustomerModal';
import SavingPrintPreview from './SavingPrintPreview';
import { ContextMenu, Item, ContextMenuProvider } from 'react-contexify';
import Settings from '../../Settings';
import Utility from '../../Utility';


var goldpric = localStorage.getItem("goldpric");

class ItemsCell extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { rowIndex, field, data, ...props } = this.props;
    return (

      <Cell {...props} className='cell-time-click'>
        <ContextMenuProvider
          data={rowIndex}
          id="menu_id">
          <div className={(this.props.textAlign == null ? '' : this.props.textAlign) + ' text-cell'}>{data[rowIndex][field]}</div>
        </ContextMenuProvider>
      </Cell>
    );
  }
}

class SavingSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      s_date: moment(),
      e_date: moment(),
      start_date: Utility.formatDate(moment()),
      end_date: Utility.formatDate(moment()),
    }

    this.submitSearch = this.submitSearch.bind(this)
    this.resetForm = this.resetForm.bind(this)
    this.handleSearchChange = this.handleSearchChange.bind(this)
    this.handleResultSelect = this.handleResultSelect.bind(this)
  }

  resetForm(e, d) {
    e.preventDefault();
    for (let i in this.state) {
      this.setState({
        [i]: ''
      });
    }
    this.setState({
      s_date: moment(),
      e_date: moment(),
      start_date: Utility.formatDate(moment()),
      end_date: Utility.formatDate(moment()),
      customer_id: ''
    })

    delete this.state.s_date;
    delete this.state.e_date;
    this.props.onSubmitSearch({
      start_date: Utility.formatDate(moment()),
      end_date: Utility.formatDate(moment()),
    });
  }

  submitSearch(e, d) {
    e.preventDefault();

    delete this.state.s_date;
    delete this.state.e_date;
    delete this.state.search_results;

    let v = this.state;
    for (let i in v) {
      if (v[i] == '')
        delete v[i]
    }

    this.props.onSubmitSearch(v);
  }

  handleResultSelect(e, d) {
    console.log(d)
    this.setState({
      number: d.title,
      search_results: null
    })
  }

  handleSearchChange(e, d) {

    this.setState({
      search_isLoading: true,
      search_results: null,
      number: d
    })

    let url = Settings.baseUrl + "/saving/?number=" + d
    Utility.get(url, (s, d) => {
      let search_results = [];
      for (let i in d) {
        search_results.push({
          title: d[i].number
        })
      }

      this.setState({
        search_isLoading: false,
        search_results: search_results
      })
    })
  }

  render() {
    return (
      <div>
        <Form className='attached fluid' size='small'>
          <Grid divided='vertically'>
            <Grid.Row columns={4}>
              <Grid.Column>
                <Form.Field >
                  <label>จากวันที่</label>
                  <DatePicker
                    dateFormat="DD/MM/YYYY"
                    value={this.state.start_date}
                    selected={this.state.s_date}
                    onChange={(date) => {
                      this.setState({ s_date: date, start_date: Utility.formatDate(date) });
                    }}
                  />
                </Form.Field>
                <Form.Field >
                  <label>ถึงวันที่</label>
                  <DatePicker
                    dateFormat="DD/MM/YYYY"
                    value={this.state.end_date}
                    selected={this.state.e_date}
                    onChange={(date) => {
                      this.setState({ e_date: date, end_date: Utility.formatDate(date) });
                    }}
                  />
                </Form.Field>
              </Grid.Column>
              <Grid.Column>
                <Form.Dropdown label='สาขา' search selection value={this.state.branches_id} options={this.props.value.branches} onChange={(e, data) => {
                  this.setState({ branches_id: data.value });
                }} />
                <Form.Dropdown label='ซื่อลูกค้า' value={this.state.customer_id} search selection options={this.props.value.customers} onChange={(e, data) => {
                  this.setState({ customer_id: data.value });
                }} />
              </Grid.Column>
              <Grid.Column>
                <Form.Field >
                  <label>รหัสออมทอง</label>
                  <Search
                    loading={this.state.search_isLoading}
                    onResultSelect={this.handleResultSelect}
                    onSearchChange={this.handleSearchChange}
                    results={this.state.search_results}
                    value={this.state.number}
                  />
                </Form.Field>
              </Grid.Column>
              <Grid.Column>
                <Form.Field>
                  <br />
                  <Button content='ค้นหา' floated='right' type='submit' icon='search' labelPosition='right' size='small' onClick={this.submitSearch} />
                </Form.Field>
                <Form.Field>
                  <br />
                  <Button floated='right' type='button' size='small' onClick={this.resetForm}  >แสดงทั้งหมด</Button>
                </Form.Field>
              </Grid.Column>
            </Grid.Row>
          </Grid>

        </Form>
      </div>)
  }
}

class SavingTable extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount() {
    let elHeight = document.getElementById('width_1')
    this.setState({ table_width: window.innerWidth });
  }

  render() {

    let items = this.props.items
    return (
      <div className='relative'>
        <Dimmer active={this.props.loading} inverted>
          <Loader inverted content='Loading' />
        </Dimmer>
        <div id='width_1'>
          <Table
            rowsCount={items.length}
            rowHeight={35}
            headerHeight={35}
            width={this.state.table_width}
            height={Settings.table_hiegh}>
            <Column

              header={<Cell>เลขที่ออมทอง</Cell>}
              cell={
                <ItemsCell data={items} field="number" />
              }
              width={150}
            />
            <Column
              header={<Cell>วันที่</Cell>}
              cell={
                <ItemsCell data={items} field="saving_date" />
              }
              width={120}
            />
            <Column
              header={<Cell>สาขา</Cell>}
              cell={
                <ItemsCell data={items} field="branch_name" />
              }
              width={200}
            />
            <Column
              header={<Cell>ซื่อลูกค้า</Cell>}
              cell={
                <ItemsCell data={items} field="customer_name" />
              }
              width={150}
            />
            <Column
              header={<Cell>จำนวนเงิน</Cell>}
              cell={
                <ItemsCell data={items} field="price_view" textAlign='text-right' />
              }
              width={150}
            />
            <Column
              header={<Cell>น้ำหนักทองที่ได้</Cell>}
              cell={
                <ItemsCell data={items} field="weight" textAlign='text-right' />
              }
              width={150}
            />
            <Column
              header={<Cell>พนักงาน</Cell>}
              cell={
                <ItemsCell data={items} field="staff_name" />
              }
              width={150}
            />
            <Column
              header={<Cell>สถานะยกเลิกบิล</Cell>}
              cell={
                <ItemsCell data={items} field="void_title" />
              }
              width={150}
            />
          </Table>
        </div>
      </div>
    )
  }

}

class Saving extends Component {

  constructor(props) {
    super(props);

    goldpric = localStorage.getItem("goldpric")
    this.state = {
      message_error_hidden: true,
      items: [],
      invoice_detail: '',
      start_date: moment(),
      end_date: moment(),
      modal_title: 'สร้างบัญชีออมทอง',
      gold: goldpric.gold_bar_sell
    }
    

    this.product_code_all = [];
    this.product_name_all = [];
    this.categories = [];
    this.product_types = [];

    this.handlerCloseModal = this.handlerCloseModal.bind(this)
    this.handleSearchChange = this.handleSearchChange.bind(this)
    this.handlerSubmitSaving = this.handlerSubmitSaving.bind(this)
    this.handlerChangeGold = this.handlerChangeGold.bind(this)
    this.handlerSearch = this.handlerSearch.bind(this)

  }

  handlerChangeGold(data) {
    this.setState({
      gold: data
    })
  }

  handlerSubmitSaving(data) {
    const {branch} = this.props.auth
    this.setState({
      loading_save: true
    })
    if (this.state.modal_action == 'add') {
      let formatDate = {
        number: this.state.number,
        saving_date: data.saving_date,
        customer: data.customer_id,
        branch: branch.id,
        gold: Utility.removeCommas(this.state.gold),
        price: Utility.removeCommas(data.price),
        weight: data.weight,
        staff: data.staffs_id
      }

      let url = Settings.baseUrl + "/saving/";
      Utility.post(url, formatDate, (s, d) => {
        this.submitLedger(d, data);
        this.componentDidMount()
        this.setState({
          loading_save: false,
          modal_open: false
        })
      });
    } else {

    }

  }

  componentDidMount() {
    const {branch} = this.props.auth
    this.setState({
      loader_active: true,
      total_item: 0
    });

    let s = 'start_date=' + Utility.formatDate2(moment());
    s += '&end_date=' + Utility.formatDate2(moment());

    var branches = Utility.getFetch(Settings.baseUrl + '/branches/?is_enabled=1');
    var vendors = Utility.getFetch(Settings.baseUrl + '/vendors/?is_enabled=1');
    var saving = Utility.getFetch(Settings.baseUrl + '/saving/?is_enabled=1&' + s);
    var stock_category = Utility.getFetch(Settings.baseUrl + '/stock_category/?is_enabled=1&branch=' + branch.id);
    var customers = Utility.getFetch(Settings.baseUrl + '/customers/?is_enabled=1');
    var staffs = Utility.getFetch(Settings.baseUrl + '/staffs/?is_enabled=1');
    Promise.all([branches, vendors, saving, stock_category, customers, staffs]).then((values) => {
      this.branches = values[0];
      this.vendors = values[1];
      this.stock_category = values[3];
      let branches = [];
      for (let i in values[0]) {
        branches.push({
          key: values[0][i].id,
          value: values[0][i].id,
          text: values[0][i].name,
        });
      }

      let vendors = [];
      for (let i in values[1]) {
        vendors.push({
          key: values[1][i].id,
          value: values[1][i].id,
          text: values[1][i].name,
        });
      }

      var items = this.setFieldValue(values[2]);
      let stock_category = [];
      for (let i in values[3]) {
        stock_category.push({
          key: values[3][i].id,
          value: values[3][i].id,
          text: values[3][i].category.name,
        });
      }
      var items = this.setFieldValue(values[2]);

      this.customers = values[4]
      let customers = [];
      for (let i in values[4]) {
        customers.push({
          key: values[4][i].id,
          value: values[4][i].id,
          text: values[4][i].name,
          content: <Header as='h4' content={values[4][i].name} subheader={values[4][i].code} />
        });
      }
      let customers_code = [];
      for (let i in values[4]) {
        customers_code.push({
          key: values[4][i].id,
          value: values[4][i].id,
          text: values[4][i].code
        });
      }

      let staffs = [];
      for (let i in values[5]) {
        staffs.push({
          key: values[5][i].id,
          value: values[5][i].id,
          text: values[5][i].name
        });
      }

      this.setState({
        stock_category: stock_category,
        products: this.products,
        branches: branches,
        vendors: vendors,
        items: items,
        loader_active: false,
        total_item: items.length,
        customers: customers,
        customers_code: customers_code,
        staffs: staffs
      });
    });
  }


  submitLedger(d, f) {
    const {branch} = this.props.auth

      let payment = 'CS';
      let bankcard = 0
      let cash = 0
      let card = 0
      let card_fee = 0
      let card_service = 0

      if (f.payment == 'CD') {
        payment = 'CD';
        card = f.price
        card_fee = f.card_fee
        card_service = f.card_service
        bankcard = f.bankcard
      } else {
        cash = f.price
      }

      let data = {
        branch: branch.id,
        object_id: d.id,
        staff_id: d.staff,
        customer: d.customer,
        object_number: d.number,
        ledger_date: Utility.formatDate2(f.saving_date),
        kind: 'IN',
        total: d.price,
        ledger_category: '6',
        bankcard: bankcard,
        cash: cash,
        card: card,
        card_fee: card_fee,
        card_service: card_service,
        payment: payment
      }

      let url = Settings.baseUrl + "/ledger/";
      Utility.post(url, data, (s, d) => {
      });
  }

  setFieldValue(item) {
    let total_amount = 0;
    let total_weight = 0;
    for (let i = 0; i < item.length; i++) {

      if (item[i].is_void == 0) {
        total_amount += parseFloat(item[i].price)
        total_weight += parseFloat(item[i].weight)
      }

      item[i].saving_date = Utility.formatDate(item[i].record_date)
      item[i].branch_name = item[i].branch.name
      item[i].customer_name = item[i].customer.name
      item[i].price_view = Utility.priceFormat(parseFloat(item[i].price))
      item[i].weight = Utility.weightFormat(parseFloat(item[i].weight))
      item[i].staff_name = item[i].staff.name
      item[i].void_title = item[i].is_void == 1 ? 'ยกเลิกบิลแล้ว' : ''
    }

    this.setState({
      total_amount: Utility.priceFormat(total_amount),
      total_weight: Utility.weightFormat(total_weight)
    })
    return item;
  }

  clearFormSearch() {
    this.setState({
      branches_id: '',
      vendors_id: '',
      start_date: '',
      end_date: '',
      is_clear_stock: false,
      is_clear_bill: false
    })
  }


  resetForm() {

  }


  handlerCloseModal() {

    this.setState({
      modal_open: false
    })
  }



  handleSearchChange(e, v) {
    this.setState({
      search: v.value,
      search_isLoading: true
    })

    this.timeout = setTimeout()
  }

  submitVoidBill(data) {

    let url = Settings.baseUrl + "/saving/" + data.id + "/";
    data['is_void'] = 1
    data.branch = data.branch.id
    data.staff = data.staff.id
    data.customer = data.customer.id
    Utility.put(url, data, (s, d) => {
      alert('ยกเลิกบิลสำเร็จ');
      this.componentDidMount()
    })
  }

  handlerSearch(data) {
    let data2 = Object.assign({}, data);
    data2.start_date = Utility.formatDateViewToDate(data2.start_date);
    data2.end_date = Utility.formatDateViewToDate(data2.end_date);
    let qrstring = Utility.jsonToQueryString(data2);
    var saving = Utility.getFetch(Settings.baseUrl + '/saving/?is_enabled=1&' + qrstring);
    this.setState({
      loader_active: true,
      start_date: data.start_date,
      end_date: data.end_date
    })
    Promise.all([saving]).then((values) => {
      this.saving = values[0];
      this.setState({
        loader_active: false,
        items: this.setFieldValue(values[0])
      });
    });
  }

  render() {
    let items = this.state.items;
    return (
      <Segment color='black' >
              <Form size='small'>
          <Form.Group>
            <Form.Field width={6}>
              <Header floated='left' as='h3'>ออมทอง</Header>
            </Form.Field>
            <Form.Field width={10}>
            <Button size='small' primary floated='right' onClick={() => {

              }} labelPosition='left' icon='plus' content='สร้างบัญชีออมทอง' />
              <Button size='small' primary floated='right' onClick={this.addledger} labelPosition='left' icon='print' content='พิมพ์' onClick={() => {
                this.setState({
                  open_print: true
                })
              }} />
            </Form.Field>
          </Form.Group>
        </Form>
        <Divider />
      <div className='invoice-box'>
        
         <SavingSearch
          onSubmitSearch={this.handlerSearch}
          value={this.state}
        />
        <br />
        <SavingTable items={items} loading={this.state.loader_active} />
        <br />
        {this.state.open_print ? <SavingPrintPreview
          data={
            {
              start_date: this.state.start_date,
              end_date: this.state.end_date,
              total_amount: this.state.total_amount,
              total_weight: this.state.total_weight
            }
          }
          items={items}
          onClose={() => {
            this.setState({ open_print: false });
          }}
        /> : ''}
        <Form size='small' >
          <Form.Group>
            <Form.Field width={4}>
              <lable>รวมจำนวนเงิน</lable>
              <Input value={this.state.total_amount} className='text-right' />
            </Form.Field>
            <Form.Field width={4}>
              <lable>รวมน้ำหนัก(กรัม)</lable>
              <Input value={this.state.total_weight} className='text-right' />
            </Form.Field>
           
          </Form.Group>
        </Form>
      </div>
      </Segment>
    );
  }
}

export default Saving;
