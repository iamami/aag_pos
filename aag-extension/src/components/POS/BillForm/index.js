/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import {
  Form,
  Header,
  Button,
  Grid,
  Modal,
  Icon,
  Dimmer,
  Loader
} from 'semantic-ui-react';
import { hotkeys } from 'react-keyboard-shortcuts'
import moment from 'moment';
import 'react-dates/lib/css/_datepicker.css';
import Utility from '../../../Utility';
import Settings from '../../../Settings';
import BillInfo from './BillInfo';
import BillItem from './BillItem';
import PaymentModal from '../../Payment/PaymentModal';
import ProductSelect from './ProductSelect';
import InvoicePrintPreview from '../InvoicePrintPreview'
import dateFormat from 'dateformat';
import { connect } from 'react-redux'

class Bill extends Component {

  constructor(props) {
    super(props);
    this.code_change_number = {}
    this.state = {
      bill_number: '',
      productCode: '',
      productName: '',
      productType: '',
      productPrice: '',
      tray: '',
      amount: '',
      sellWeight: '',
      realWeight: '',
      unitPrice: '',
      sellPrice: '',
      fee: '',
      score: '',
      billNumber: '',
      portCode: '',
      status: '',
      products: [],
      prices: {},
      weights: [],
      staffs: [],
      billItems: [],
      billSummary: {},
      billNo: '',
      customers: [],
      customerCode: '',
      customerName: '',
      customerPhone: '',
      bill_date: moment(),
      saleList: '',
      loader: false,
      bill_saved: false,
      customer_id: '',
      bill_id: '',
      bill_staff: []
    };

    this.resetBillForm = this
      .resetBillForm
      .bind(this);
    this.submitAddOrder = this
      .submitAddOrder
      .bind(this);
    this.amountChangeHandler = this
      .amountChangeHandler
      .bind(this);
    this.sellPriceChangeHandler = this
      .sellPriceChangeHandler
      .bind(this);
    this.feeChangeHandler = this
      .feeChangeHandler
      .bind(this);
    this.customerChangeHandler = this
      .customerChangeHandler
      .bind(this);

    this.selectSaleHandler = this
      .selectSaleHandler
      .bind(this);
    this.submitBillHandler = this
      .submitBillHandler
      .bind(this);
    this.saveBill = this
      .saveBill
      .bind(this);
    this.resetBill = this
      .resetBill
      .bind(this)
    this.deleteItem = this
      .deleteItem
      .bind(this)
    this.onEditChange = this
      .onEditChange
      .bind(this)
    this.handlerVoidBill = this.handlerVoidBill.bind(this)
    this.handlerUpdateStock = this.handlerUpdateStock.bind(this)
  }

  deleteItem(e, i) {
    let billItems = this.state.billItems;

    var id_change = billItems[i].id_change

    // item1
    billItems.map((item,i)=>{
       if(item.id_change==id_change){
        billItems.splice(i, 1);
      }
    })
    
    // item2
    billItems.map((item,i)=>{
      if(item.id_change==id_change){
       billItems.splice(i, 1);
     }
   })
    
    this.setBillItem(billItems);
  }

  resetBillForm(value) {
    this.setState({
      productCode: '',
      productName: '',
      productType: '',
      productPrice: '',
      tray: '',
      amount: '',
      sellWeight: '',
      realWeight: '',
      unitPrice: '',
      sellPrice: '',
      fee: '',
      score: '',
      billNumber: '',
      portCode: '',
      status: '',
      bill_id: '',
      loader: false
    });
  }

  resetBill(e) {

    e.preventDefault();
    this.resetBillForm();

    const summary = {
      itemCount: 0,
      total_buy_weight: 0,
      total_sell_weight: 0,
      net_price: 0,
      total_sell_price: 0,
      total_buy_price: 0
    };
    this.setState({ billItems: [], customer_id: 1, billSummary: summary, bill_saved: false })

    this.genBillNumber()
  }

  getProductByCode(id) {
    let categories = this
      .categories
      .filter((o) => {
        return o.id == id
      });
    return categories[0];
  }

  submitAddOrder(e, data) {
    if (this.state.bill_saved) {
      alert('บิลนี้ถูกบันทึกเรียบร้อยแล้ว กรุณาเริ่มรายการใหม่');
    }

    console.log('data',data)

    let billItems = this.state.billItems;
    if (data.sell != '') {
      let p = Utility.getObject(this.products_list, data.sell.product_id);
      billItems.push({
        type_title: 'ขาย',
        type: 'SE',
        'id': billItems.length + 1,
        amount: data.sell.amount,
        sell_price: data.sell.sell_price,
        sell_weight: p.weight,
        sell_weight_real: data.sell.sell_weight_real_g,
        buy_price: 0,
        buy_weight: 0,
        gold_price: data.sell.gold_price,
        product_code: p.code,
        product_id: p.id,
        product_name: p.name,
        product_type: p.kind.name,
        category_name: p.category.name,
        category_id: p.category.id,
        id_change: data.sell.id_change,
        exchange: data.sell.exchange,
        cost: data.sell.product_price,
        sales_force: data.sell.product_price
          ? data.sell.sell_price - (data.sell.product_price * data.sell.amount)
          : 0
      })
    }

    if (data.buy != '') {
      billItems.push({
        type_title: 'ซื้อ',
        type: 'BU',
        'id': billItems.length + 1,
        amount: 0,
        sell_price: 0,
        sell_weight: 0,
        sell_weight_real: 0,
        buy_price: data.buy.buy_price,
        buy_weight: data.buy.buy_weight_real_g,
        gold_price: data.buy.gold_price,
        product_code: '',
        product_name: data.buy_name,
        product_type: '',
        category_name: data.buy.category_name,
        category_id: data.buy.category_id,
        id_change:  data.buy.id_change,
        buy_name: data.buy.buy_name,
        exchange: data.buy.exchange,
        sales_force: 0,
        cost: 0
      })
    }

    this.setBillItem(billItems);
  }

  setBillItem(billItems) {
    let total_sell_price = 0;
    let total_buy_price = 0;
    let total_sell_weight = 0;
    let total_buy_weight = 0;
    let total_exchange = 0
    let product_group = {}
    for (let i = 0; i < billItems.length; i++) {

      if (billItems[i].sell_price != '') {
        total_sell_price += Number(billItems[i].sell_price)
        total_sell_weight += Number(billItems[i].sell_weight_real)
      }
      if (billItems[i].buy_price != '') {
        total_buy_price += Number(billItems[i].buy_price)
        total_buy_weight += Number(billItems[i].buy_weight)
      }
      const _exchange = parseFloat(billItems[i].exchange) 
      if(billItems[i].type == 'BU' && _exchange>0)
        total_exchange += _exchange
      // group
      if (billItems[i].type == 'BU' || this.props.kind == 'XC' || product_group['sell' + billItems[i].product_id] == null) {

        if (billItems[i].type == 'BU'|| this.props.kind == 'XC')
          product_group[billItems[i].id] = billItems[i]
        else {
          billItems[i].amount = parseInt(billItems[i].amount)
          product_group['sell' + billItems[i].product_id] = billItems[i]
        }

      } else {
        product_group['sell' + billItems[i].product_id].amount += parseInt(billItems[i].amount)
      }
    }

    let _billItems = []
    for (let k in product_group) {
      _billItems.push(product_group[k])
    }

    const summary = {
      itemCount: _billItems.length,
      total_buy_weight: Utility.weightFormat(total_buy_weight),
      total_sell_weight: Utility.weightFormat(total_sell_weight),
      net_price: this.props.kind == 'XC' ? total_exchange : total_sell_price - total_buy_price,
      total_sell_price: Utility.priceFormat(total_sell_price),
      total_buy_price: Utility.priceFormat(total_buy_price)
    }

    this.setState({ billItems: _billItems, billSummary: summary });
  }

  customerChangeHandler(value) {
    this.setState({ customer_id: value.id, customerName: value.name, customerCode: value.code, customerPhone: value.phone });
  }

  selectSaleHandler(value) {
    this.setState({ saleList: value });
  }

  saveBill(e) {
    e.preventDefault()

    const {branch} = this.props.auth

    let net_price = Utility.removeCommas(this.state.billSummary.net_price);
    if (this.state.billItems.length == 0) {
      alert('กรุณาเพิ่มรายการซื้อ-ขาย');
      this.setState({ open: true })
    } else if (net_price <= 0) {
      this.submitBillHandler({
        cash: (-1) * net_price,
        total: (-1) * net_price,
        change: 0,
        date: moment(),
        ledger_date: Utility.formatDate2(moment()),
        description: '',
        bank: '',
        card_code: '',
        card_fee: 0,
        bankcard: '',
        card_service: 0,
        card_period: 0,
        card_start: '',
        card_contract_number: 0,
        card_bank_card: '',
        check_code: '',
        check_total: 0,
        card_total: 0,
        payment: 'CS',
        branch: branch.id,
        check_date: Utility.formatDate2(moment())
      })
    } else {
      this.setState({ modal_open: true });
    }

  }

  submitBillHandler(ledger) {
    const {branch} = this.props.auth
    let _billItems = this.state.billItems;
    let total_sell = 0;
    let total_buy = 0;
    let diff = 0;
    for (let i = 0; i < _billItems.length; i++) {
      total_sell += Number(_billItems[i].sell_price)
      total_buy += Number(_billItems[i].buy_price)
    }
    let kind_bill = 'SE';
    ledger['ledger_category'] = 1
    ledger['kind'] = 'IN'
    if (total_sell == 0 && total_buy > 0) {
      kind_bill = 'BU';
      ledger['kind'] = 'EX'
      ledger['ledger_category'] = 2
    } else if (total_sell > 0 && total_buy > 0) {
      kind_bill = 'XC';
      ledger['kind'] = 'IN';
      ledger['ledger_category'] = 3
    }
    diff = total_sell - total_buy;

    let staffs = '';
    let saleList = this.state.saleList;
    if (saleList.length > 0) {
      staffs = JSON.stringify(saleList)
    }

    let total = this.state.billSummary.net_price;
    let bill = {
      bill_number: this.state.bill_number,
      branch: branch.id,
      customer: this.state.customer_id == null ? 1 : this.state.customer_id,
      sell: total_sell,
      buy: total_buy,
      discount: 0,
      amount: _billItems.length,
      description: this.state.description == null ? '' : this.state.description,
      kind: kind_bill,
      total: total,
      bill_date: dateFormat(this.state.bill_date, "yyyy-mm-dd HH:MM:ss"),
      weight_buy: Utility.removeCommas(this.state.billSummary.total_buy_weight),
      exchange: total_buy > 0 && total_sell > 0
        ? total
        : 0,
      staffs: staffs,
      gold_price: this.state.gold_price.id,
      payment: ledger.payment
    };

    this.setState({ loader: true })
    if (this.is_submit)
      return;
    this.is_submit = true
    Utility.post(Settings.baseUrl + "/bills/", bill, async (s, d) => {

      if (s) {
        let saleList = this.state.saleList;
        this.setState({ bill_id: d.id })

        let bill_id = d.id;
        let bill_number = d.bill_number
        for (let i = 0; i < _billItems.length; i++) {
          await this.submitItem(_billItems[i], bill_id, bill_number, total, kind_bill);
        }

        for (let i = 0; i < saleList.length; i++) {
          this.submitSale(saleList[i], bill_id);
        }
        Utility.get(Settings.baseUrl + "/bill/" + bill_id + "/stock_update/", (s, d2) => {
          this.setState({ loader: false, modal_open: false, bill_saved: true, bill_number: d.bill_number });
          if (!s) {
            alert(d2.error)
          }
        });

        if (saleList.length > 0) {
          ledger['staff'] = saleList[0];
        }
        ledger['object_id'] = d.id
        ledger['object_number'] = d.bill_number
        delete ledger['date']
        this.submitledger(ledger);
      } else {
        alert('ผิดพลาดกรุณาลองอีกครั้ง')
      }
    });
  };

  async submitledger(data) {
    if (data.total < 0) {
      data.total = data.total * (-1)
    }
    delete data['date']
    data['customer'] = this.state.customer_id
    let url = Settings.baseUrl + "/ledger/";
    Utility.post(url, data, (s, d) => {
      if (s)
        alert('บันทึกบิลและชำระเงินสำเร็จ')
      else
        alert('ชำระเงิน ผิดพลาดกรุณาลองอีกครั้ง')
    });
  }

  async submitItem(d, bill_id, bill_number, diff) {

    let num = 1
    if(this.code_change_number[d.id_change]==null){
      this.code_change_number[d.id_change] = Object.keys(this.code_change_number).length + 1
      num = this.code_change_number[d.id_change]
    }else{
      num = this.code_change_number[d.id_change]
    }
    let formData = {
      bill: bill_id,
      category: d.category_id,
      weight: d.type == 'BU' ? d.buy_weight : d.sell_weight_real,
      amount: d.amount,
      sell: d.sell_price,
      buy: d.buy_price,
      gold_price: Utility.removeCommas(d.gold_price),
      code_change: bill_number +"-"+ num,
      exchange: diff,
      kind: d.type,
      cost: d.cost,
      product_name: d.buy_name
        ? d.buy_name
        : ''
    };

    if(d.product_id)
      formData['product'] = d.product_id

    const endpoint = Settings.baseUrl + '/bill_items/';
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    var _formData = new FormData();
    for (let k in formData) {
      _formData.append(k, formData[k]);
    }
    let options = {
      method: 'POST',
      mode: 'cors',
      headers: headers,
      body: Utility.jsonToQueryString(formData)
    };
    const response = await fetch(endpoint, options).catch((e) => {console.log(e);});
    return await response.json()
  }

  async submitSale(staff_id, bill_id) {
    let formData = {
      bill: bill_id,
      staff: staff_id
    };

    const endpoint = Settings.baseUrl + '/bill_staff/';
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    let options = {
      method: 'POST',
      mode: 'cors',
      headers: headers,
      body: Utility.jsonToQueryString(formData)
    };
    const response = await fetch(endpoint, options).catch((e) => {console.log(e);});
    return await response.json()
  }

  amountChangeHandler(value) {
    this.setState({
      amount: Number(value),
      sellPrice: Number(value) * this.state.productPrice
    });
  }

  sellPriceChangeHandler(value) {
    this.setState({ sellPrice: Number(value) });
  }

  feeChangeHandler(value) {
    this.setState({ fee: Number(value) });
  }

  loadGoldPrice(bill_date) {
    let d = new Date(bill_date);
    let q = dateFormat(d, "yyyy-mm-dd HH:MM")
    let url = Settings.baseUrl + "/gold_price/?datetime=" + q;
    this.setState({ is_loading: true })
    Utility.get(url, (s, res) => {
      if (s && res.length > 0) {
        this.setState({
          gold_price: res[0],
          is_loading: false
        })
      }
    })
  }

  componentDidMount() {

    const {branch} = this.props.auth
    this.loadGoldPrice(this.state.bill_date)
    Utility.get(Settings.baseUrl + "/products_data/", (e, resJson) => {

      this.products_list = resJson;
      let products = []
      for (let i in resJson) {
        products.push({
          value: resJson[i].id,
          key: resJson[i].id,
          text: resJson[i].name + ' ' + resJson[i].code,
          content: <Header as='h4' content={resJson[i].name} subheader={resJson[i].code} />
        });
      }

      let width_table = document.getElementById('width_table')
      if(width_table)
        this.setState({ width_table: width_table.clientWidth, product_options: products });
    })

    Utility.get(Settings.baseUrl + "/categories/", (e, resJson) => {

      this.categories = resJson;
      let categories = []
      for (let i in resJson) {
        categories.push({ value: resJson[i].id, key: resJson[i].id, text: resJson[i].name });
      }
      this.setState({ categories: categories });
    })

    Utility.get(Settings.baseUrl + "/product_types/", (e, resJson) => {

      this.product_types = resJson;
      let product_types = []
      for (let i in resJson) {
        product_types.push({ value: resJson[i].id, key: resJson[i].id, text: resJson[i].name });
      }
      this.setState({ product_types_option: product_types });
    })

    Utility.get(Settings.baseUrl + "/staffs/?branch=" + branch.id, (e, resJson) => {
      this.staffs = resJson;
      let staffs = []
      for (let i in resJson) {
        staffs.push({ value: resJson[i].id, key: resJson[i].id, text: resJson[i].name });
      }
      this.setState({ staffs: staffs });
    })

    Utility.get(Settings.baseUrl + "/banks/", (e, resJson) => {
      this.banks = resJson;
      let banks = []
      for (let i in resJson) {
        banks.push({ value: resJson[i].id, key: resJson[i].id, text: resJson[i].name });
      }
      this.setState({ banks: banks });
    })

    Utility.get(Settings.baseUrl + "/bank_cards/", (e, resJson) => {
      this.bank_cards = resJson;
      let bank_cards = []
      for (let i in resJson) {
        bank_cards.push({
          value: resJson[i].id,
          key: resJson[i].id,
          text: resJson[i].kind + " " + resJson[i].bank.name
        });
      }
      this.setState({ bank_cards: bank_cards });
    })

    if (this.props.action == 'edit') {
      let bill = this.props.bill
      //bill.bill_date = dateFormat(bill.bill_date, "dd/mm/yyyy HH:MM")
      bill.customer_id = bill.customer.id
      bill.bill_id = bill.id
      this.setState(bill)
      this.loadBillDetail()
    }
  }

  loadBillDetail() {

    let url = Settings.baseUrl + "/bill/" + this.props.bill.id + "/items/"
    Utility.get(url, (e, res) => {
      this.bill_item = res
      this.setItemBill(res)
    })

    url = Settings.baseUrl + "/bill_staff/?bill_id=" + this.props.bill.id
    Utility.get(url, (e, res) => {
      let bill_staff = []
      for (let i in res) {
        bill_staff.push(res[i].staff.id)
      }
      this.setState({
        saleList: bill_staff
      })
    })
  }

  setItemBill(items) {

    let billItems = [];

    for (let i in items) {
      let data = items[i]

      let code_change = data.code_change;
      if (data.kind == 'SE') {
        let p = data.product
        billItems.push({
          type_title: 'ขาย',
          type: 'SE',
          'id': billItems.length + 1,
          status_stock_display: data.status_stock_display,
          amount: data.amount,
          sell_price: data.sell,
          sell_weight: p.weight,
          sell_weight_real: data.weight,
          buy_price: 0,
          buy_weight: 0,
          gold_price: data.gold_price,
          product_code: p.code,
          product_id: p.id,
          product_name: p.name,
          product_type: p.kind.name,
          category_name: p.category.name,
          category_id: p.category.id,
          code_change: code_change,
          exchange: data.exchange,
          cost: data.cost,
          sales_force: 0
        })
      } else if (data.kind == 'BU') {
        billItems.push({
          type_title: 'ซื้อ',
          type: 'BU',
          'id': billItems.length + 1,
          status_stock_display: data.status_stock_display,
          amount: 0,
          sell_price: 0,
          sell_weight: 0,
          sell_weight_real: 0,
          buy_price: data.buy,
          buy_weight: data.weight,
          gold_price: data.gold_price,
          product_code: '',
          product_name: '',
          product_type: '',
          category_name: data.category.name,
          category_id: data.category.id,
          code_change: code_change,
          buy_name: data.product_name,
          exchange: data.exchange,
          sales_force: 0,
          cost: 0
        })
      }
    }

    this.setBillItem(billItems);
  }

  handlerUpdateStock(data){

    if(window.confirm('ยืนยันอัพเดทสตํอก')){

      Utility.get(Settings.baseUrl + "/bill/" + data.id + "/stock_update/", (s, d2) => {

        if (!s) {
          alert(d2.error)
        }else{
          alert('อัพเดทสตํอกสำเร็จ')
          this.componentDidMount();
        }
      });


    }
  }

  async handlerVoidBill(data) {
    const {branch} = this.props.auth

    if (this.state.is_void == 1) {
      alert('ถูกลบแล้ว');
      return;
    }

    if (this.state.description == null || this.state.description == '') {
      alert('กรุณาระบุหมายเหตุ');
      return;
    }

    const _data ={description:this.state.description }

    let urlput = Settings.baseUrl + "/bills/" + this.state.id + "/"
    let urlget = Settings.baseUrl + "/bill/" + this.state.id + "/void/"

    const bill_res = await Utility.patchAsync(urlput,_data)
    console.log('bill_res',bill_res)
    if(bill_res.status_code==403){
      alert('admin เท่านั้นที่สามารถลบยกเลิกบิลได้' )
    }else if(bill_res.status_code==200){
      const bill_res = await Utility.postAsync(urlget,{})
      if(bill_res.status_code==403){
        alert('admin เท่านั้นที่สามารถลบยกเลิกบิลได้' )
      }else if(bill_res.status_code==200){
        alert('ยกเลิกบิลสำเร็จ');
        this.componentDidMount();
        this.setState({
          is_void: 1
        })
      }
    }

  }


  onEditChange(value) {
    let _billItems = this.state.billItems;

    for (let b in _billItems) {
      _billItems[b].exchange = value
    }

    const summary = this.state.billSummary;
    summary.net_price = value;
    this.setState({ billItems: _billItems, billSummary: summary });
  }

  render() {
    const {role} = this.props.auth

    let color = 'green'
    let title = ''
    switch (this.props.kind) {
      case 'SE':
        color = 'green'
        title = 'ขายทอง'
        break
      case 'XC':
        color = 'yellow'
        title = 'เปลี่ยนทอง'
        break
      case 'BU':
        color = 'red'
        title = 'ซื้อทอง'
        break
    }
    return (
      <Modal size="large" open={true} /*dimmer='blurring'*/>
        <Button
          id='btnClose'
          circular
          icon='close'
          basic
          floated='right'
          name=''
          onClick={this.props.onClose} />
        <Modal.Header id='headerModalPOS'>
          <Header  as='h4'>
            <Icon name='file alternate' color={color} /> {title} {this.state.is_void ? <b>(ถูกยกเลิกแล้ว)</b> : ''}
            {this.state.bill_number != '' && <span> [<b>บิลเลขที่</b>  {this.state.bill_number}]</span>}
          </Header>
        </Modal.Header>
        <Modal.Content id='contentModalPOS'>
          {this.state.modal_open && <PaymentModal
            total={this.state.billSummary.net_price}
            customer={this.state.customer_id}
            onClose={() => this.setState({ modal_open: false })}
            onSubmit={this.submitBillHandler} />}
          <Dimmer active={this.state.is_loading} inverted>
            <Loader inverted content='Loading' />
          </Dimmer>
          <div>
            <Grid >
              <Grid.Row>
                <Grid.Column width={4}>
                  <BillInfo
                    bill_number={this.state.bill_number}
                    bill_date={this.state.bill_date}
                    customer_id={this.state.customer_id}
                    onBillDateChange={(date) => {
                      this.setState({ bill_date: date });
                      this.loadGoldPrice(date)
                    }}
                    onCustomerChange={this.customerChangeHandler}
                    staffs={this.state.staffs}
                    saleList={this.state.saleList}
                    description={this.state.description}
                    onSelectSale={this.selectSaleHandler}
                    ondescription={(text) => {
                      this.setState({ description: text })
                    }} />
                </Grid.Column>
                <Grid.Column width={12}>
                  <div id='width_table'>
                    <Form size='small'>
                      <Form.Group>
                        <Form.Field width={6}>
                          <Header floated='left' as='h3'>รายการสินค้า</Header>
                        </Form.Field>
                        <Form.Field id="addstock" width={16}>
                          <Button id='btnAddProduct'
                            content='เพิ่มรายการสินค้า (F1)'
                            onClick={(e) => {
                              this.setState({ open: true })
                            }}
                            disabled={this.state.bill_number !== ''}
                            floated='right'
                            icon='plus'
                            labelPosition='left'
                            type='button'
                            color='green' />
                        </Form.Field>
                      </Form.Group>
                    </Form>

                    {this.state.open
                      ? <ProductSelect
                        product_types_option={this.state.product_types_option}
                        onClose={() => {
                          this.setState({ open: false })
                        }}
                        gold_price={this.state.gold_price}
                        kind={this.props.kind}
                        open={true}
                        formValue={this}
                        products_list={this.products_list}
                        onReset={this.resetBillForm}
                        onNameChange={this.nameChangeHandler}
                        onAmountChange={this.amountChangeHandler}
                        onSellPriceChange={this.sellPriceChangeHandler}
                        onFeeChange={this.feeChangeHandler}
                        onSubmit={this.submitAddOrder} />
                      : ''}
                    <div>
                      <BillItem
                        is_delete={this.state.bill_number == ''}
                        billItems={this.state.billItems}
                        onDelete={this.deleteItem}
                        action={this.props.kind}
                        summary={this.state.billSummary}
                        width={this.state.width_table}
                        delete_button={true} />
                    </div>
                    <div>
                      <Form className='attached fluid' size='small'>
                        <Form.Group>
                          <Form.Input
                            label='จำนวนรายการ'
                            placeholder=''
                            className='text-right'
                            width={3}
                            value={this.state.billSummary.itemCount}
                            readOnly /> {this.state.kind == 'BU'
                              ? ""
                              : <Form.Input
                                label='ยอดรวมราคาขาย'
                                placeholder=''
                                className='text-right'
                                width={3}
                                value={this.state.billSummary.total_sell_price}
                                readOnly />
                          }
                          {this.state.kind == 'BU'
                            ? ""
                            : <Form.Input
                              label='รวมน้ำหนักขาย'
                              placeholder=''
                              className='text-right'
                              width={3}
                              value={this.state.billSummary.total_sell_weight}
                              readOnly />
                          }
                          {this.state.kind == 'SE'
                            ? ""
                            : <Form.Input
                              label='ยอดรวมราคาซื้อ'
                              placeholder=''
                              className='text-right'
                              width={3}
                              value={this.state.billSummary.total_buy_price}
                              readOnly />
                          }
                          {this.state.kind == 'SE'
                            ? ""
                            : <Form.Input
                              label='รวมน้ำหนักซื้อ'
                              placeholder=''
                              className='text-right'
                              width={3}
                              value={this.state.billSummary.total_buy_weight}
                              readOnly />
                          }
                          <Form.Input readOnly tabIndex={-1} width={2} transparent={true} /> {this.state.kind == 'SE' || this.state.kind == 'BU'
                            ? <Form.Input readOnly tabIndex={-1} width={2} transparent={true} />
                            : ''}
                          <Form.Input
                            label='ยอดรวมทั้งสิ้น'
                            placeholder=''
                            className='text-right'
                            width={3}
                            value={Utility.priceFormat(this.state.billSummary.net_price)}
                            readOnly />
                        </Form.Group>

                      </Form>
                    </div>
                  </div>
                </Grid.Column>
              </Grid.Row>
            </Grid>
            {this.state.modal_open_inv
              ? <InvoicePrintPreview
                onClose={() => {
                  this.setState({ modal_open_inv: false })
                }}
                bill_id={this.state.bill_id} />
              : ''}
          </div>

        </Modal.Content>
        <Modal.Actions>

          {this.props.action == 'edit' && role == 'A' ? <Button
            color='red'
            size='small'
            icon='cancel'
            floated='left'
            disabled={this.state.is_void == 1}
            labelPosition='left'
            id='cancel-this'
            onClick={(e) => {
              this.handlerVoidBill(this.props.bill)
            }}
            content='ยกเลิกรายการนี้' /> : ''}
          <Button
            id='btnRefesh'
            size='small'
            icon='redo'
            labelPosition='left'
            onClick={() => {
              this
                .props
                .onReset(this.props.kind)
            }}
            content='เริ่มใหม่ (F5)' />

          <Button
            id='btnPrintVat'
            size='small'
            primary
            icon='print'
            labelPosition='left'
            disabled={this.state.bill_number == ''}
            onClick={() => {
              if (this.state.bill_id)
                this.setState({ modal_open_inv: true })
            }}
            content=' พิมพ์ VAT (F8)' />
          <Button
            id='btnSaveandCheck'
            size='small'
            primary
            icon='payment'
            labelPosition='left'
            onClick={this.saveBill}
            disabled={this.state.bill_number !== ''}
            content=' บันทึกบิลและชำระเงิน (F2)' />
        </Modal.Actions>
      </Modal>
    );
  }

  hot_keys = {
    'f2': {
      priority: 3,
      handler: (event) => {
        event.preventDefault()
        if (this.state.bill_number == '')
          this.saveBill(event)


      }
    }, 'enter': {
      priority: 3,
      handler: (event) => {
        event.preventDefault()
        if (this.state.bill_number == '')
          this.saveBill(event)

      }
    }, 'f1': {
      priority: 3,
      handler: (event) => {
        event.preventDefault()
        this.setState({ open: true })

      }
    }, 'f5': {
      priority: 3,
      handler: (event) => {
        event.preventDefault()
        this
          .props
          .onReset(this.props.kind)

      }
    }, 'f8': {
      priority: 3,
      handler: (event) => {
        event.preventDefault()
        if (this.state.bill_id)
          this.setState({ modal_open_inv: true })

      }
    },
    'esc': {
      priority: 3,
      handler: (event) => {
        event.preventDefault()
        this.props.onClose(event)


      }
    }
  }
}

const mapStateToProps = state =>{
  return ({
    auth: state.auth,
  })
}
export default connect(
  mapStateToProps,
)(hotkeys(Bill))