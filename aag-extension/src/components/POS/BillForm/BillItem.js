/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import {
  Table,
  Column,
  Cell
} from 'fixed-data-table';
import {
  Route,
  NavLink
} from 'react-router-dom';

import {
  Icon,
} from 'semantic-ui-react';
import { ContextMenu, Item, ContextMenuProvider } from 'react-contexify';
import Utility from '../../../Utility';

class MyAwesomeMenu extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <ContextMenu id='menu_id'>
        <Item onClick={this.props.onClick} data='delete'>
          ลบ
        </Item>
      </ContextMenu>
    );
  }
}

class IconCell extends Component {
  render() {
    const { rowIndex, field, data, type, ...props } = this.props;
    return (
      <Cell {...props}  >
        <a id='btnDelete' onClick={(e) => { if(window.confirm('ยืนยันลบรายการนี้'))this.props.onDelete(e, rowIndex) }}><Icon name='window close outline' /></a>
        {data[rowIndex].sell_price > 0 ? <Icon name='file alternate' color='green' /> : <Icon name='file alternate' color='red' />}
      </Cell>
    );
  }
}

class ItemsCell extends Component {
  render() {
    const { rowIndex, field, data, type, ...props } = this.props;

    let v = type == 'number' ? Utility.numberFormat(data[rowIndex][field]) : data[rowIndex][field];
    return (
      <Cell {...props}  >
        <ContextMenuProvider
          data={rowIndex}
          id="menu_id">
          <div className={(this.props.textAlign == null ? '' : this.props.textAlign) + ' text-cell'}>{v}</div>
        </ContextMenuProvider>
      </Cell>
    );
  }
}


class SellBillTable extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sales: []
    };
    this.handlerDelete = this.handlerDelete.bind(this);
  }

  handlerDelete(e, d) {

    if(window.confirm('ยืนยันลบรายการนี้'))
      this.props.onDelete(e, d);
  }

  componentWillMount() {
  }
  render() {
    let items = this.props.billItems
    console.log('items',items)
    return this.exc(items);
  }

  exc(items) {
    return (
      <div id="root-table-bill">
        <MyAwesomeMenu id="menu_id" onClick={(e, r, data) => {
          let index = e.getAttribute('data');
          //const bill = items[index];
          this.props.onDelete(e, index);
        }} />


        <Table
          rowsCount={items.length}
          rowHeight={35}
          headerHeight={30}
          width={this.props.width}
          height={300}>
          {this.props.is_delete && <Column
            header={<Cell></Cell>}
            cell={
              <IconCell data={items} onDelete={this.props.onDelete} />
            }
            width={50}
            fixed={true}
          />}
          <Column
            header={<Cell>รายการ</Cell>}
            cell={
              <ItemsCell data={items} field="type_title" />
            }
            width={80}
            fixed={true}
          />

            <Column
              header={<Cell>สถานะอัพเดทสต็อก</Cell>}
              cell={
                <ItemsCell item='status' data={items} field="status_stock_display" />
              }
              width={120}
            />
          {this.props.action == 'BU' ? '' :
            <Column
              header={<Cell>รหัสสินค้า</Cell>}
              cell={
                <ItemsCell id="idproduct" data={items} field="product_code" />
              }
              width={120}
            />}
          {this.props.action == 'BU' ? '' : <Column
            header={<Cell>ชื่อสินค้า</Cell>}
            cell={
              <ItemsCell data={items} field="product_name" />
            }
            width={140}
          />}
          {this.props.action == 'SE' ? '' :
            <Column
              header={<Cell>กลุ่มสินค้า</Cell>}
              cell={
                <ItemsCell data={items} field="category_name" />
              }
              width={180}
            />}
          {this.props.action == 'SE' ? '' :
            <Column
              header={<Cell>ชื่อทองเก่า</Cell>}
              cell={
                <ItemsCell data={items} field="buy_name" />
              }
              width={180}
            />}
          {this.props.action == 'BU' ? '' :
            <Column
              header={<Cell className='text-right'>จำนวน</Cell>}
              cell={
                <ItemsCell data={items} field="amount" textAlign='text-right' type='number' />
              }
              width={70}
            />}
          {this.props.action == 'BU' ? '' :
            <Column
              header={<Cell className='text-right'>ราคาขาย</Cell>}
              cell={
                <ItemsCell data={items} field="sell_price" textAlign='text-right' type='number' />
              }
              width={120}
            />}
          {this.props.action != 'XC' ? '' :
            <Column
              header={<Cell className='text-right'>ราคาเปลี่ยน</Cell>}
              cell={
                <ItemsCell data={items} field="exchange" textAlign='text-right' type='number' />
              }
              width={100}
            />}
          {this.props.action == 'BU' ? '' :
            <Column
              header={<Cell className='text-right'>ค่าแรงขาย</Cell>}
              cell={
                <ItemsCell id='textCost' data={items} field="sales_force" textAlign='text-right' type='number' />
              }
              width={120}
            />}
          {this.props.action == 'SE' ? '' :
            <Column
              header={<Cell className='text-right'>ราคาซื้อ</Cell>}
              cell={
                <ItemsCell data={items} field="buy_price" textAlign='text-right' type='number' />
              }
              width={120}
            />}
          {this.props.action == 'BU' ? '' :
            <Column
              header={<Cell className='text-right'>น.น.ชั่ง</Cell>}
              cell={
                <ItemsCell data={items} field="sell_weight_real" textAlign='text-right' type='number' />
              }
              width={120}
            />}
          {this.props.action == 'SE' ? '' :
            <Column
              header={<Cell className='text-right'>น.น.ซื้อ</Cell>}
              cell={
                <ItemsCell data={items} field="buy_weight" textAlign='text-right' />
              }
              width={120}
            />
          }
          <Column
            header={<Cell className='text-right'>ราคาทอง</Cell>}
            cell={
              <ItemsCell data={items} field="gold_price" textAlign='text-right' type='number' />
            }
            width={120}
          />
          {this.props.action != 'XC' ? '' :
            <Column
              header={<Cell>รหัสเปลี่ยนทอง</Cell>}
              cell={
                <ItemsCell data={items} field="code_change" />
              }
              width={140}
            />}
        </Table><br />
      </div>
    );
  }
}

export default SellBillTable;
