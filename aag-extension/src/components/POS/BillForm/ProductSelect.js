/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import MsgInput from '../../Error/MsgInput'
import {
  Form,
  Message,
  Button, Modal, Input, Dimmer, Loader, Table, Header, Label
} from 'semantic-ui-react';
import SelectOption from '../../SelectOption'
import Utility from '../../../Utility';
import Settings from '../../../Settings';
import moment from 'moment';
import { hotkeys } from 'react-keyboard-shortcuts'
import { connect } from 'react-redux'

class SellForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      type_form: 'all',
      product_code: '',
      product_id: '',
      product_type: '',
      product_name: '',
      amount: '',
      discount: '',
      sell_weight_real_g: '',
      product_price: '',
      sell_price: '',
      products: [],
      buy_categories_id: '',
      buy_product_id: '',
      buy_weight_real_g: '',
      buy_weight_real_b: '',
      buy_price: '',
      exchange: 0,
      msg_error: '',
      is_loading: false,
      record_date: moment(),
      category_id: '',
      product_options: this.props.formValue.product_options,
      max: 0,
    }
    this.handlerChangeCategory = this.handlerChangeCategory.bind(this)
    this.handlerChangeProductType = this.handlerChangeProductType.bind(this)
    this.handlerReset = this.handlerReset.bind(this);
    this.handlerSubmit = this.handlerSubmit.bind(this);
    this.handlerProductChange = this.handlerProductChange.bind(this);
    this.handlerAmount = this.handlerAmount.bind(this);
    this.handlerBuyWeightG = this.handlerBuyWeightG.bind(this);
    this.handlerBuyWeightB = this.handlerBuyWeightB.bind(this);
    this.handlerBuyPrice = this.handlerBuyPrice.bind(this);
    this.handlerExchange = this.handlerExchange.bind(this);
    this.handlerSellPrice = this.handlerSellPrice.bind(this)
    this.handlerChangeWeightReal = this.handlerChangeWeightReal.bind(this)

  }

  handleResultSelect(e, value) {

    this.weightRealInput.focus()
    this.setState({
      product_name: value.result.title
    })
  }

  componentDidMount() {

    this.setState(this.props.gold_price)

    let url = Settings.baseUrl + "/product_name/"
    Utility.get(url, (s, d) => {
      this.setState({
        products: d
      })
    })

    if (this.props.kind == 'SE' || this.props.kind == 'XC') {
      if(this.inputCategory)
        this.inputCategory.focus()
    } else {
      if(this.categoryBuyInput)
      this.categoryBuyInput.focus()
    }

    this.loadProduct()

  }

  handlerSellPrice(e, v) {
    const sell_price = Utility.parseFloat(v.value,this.state.sell_price)

    this.setState({
      sell_price
    })
  }

  handlerProductChange(e, d) {
    e.preventDefault();
    const p = Utility.getObject(this.products, d.value);
    
    if (p) {
      this.setPriceSell(p, p.weight, 1)
      this.setState({
        max: p.amount,
        category_id: p.category.id,
        product_code: p.code,
        product_id: p.id,
        product_name: p.name,
        product_type: p.kind.id,
        amount: 1,
        discount: 0,
        sell_weight_real_g: p.weight
      });
      this.setDiff();
    } else{
      this.setState({max: 0})
    }
    this.amountInput.focus();
  }

  handlerChangeProductType(e, { value }) {
    this.setState({
      product_type: value
    })
    this.inputProduct.focus()
    this.updateProductOption(this.state.category_id, value)
  }

  handlerChangeCategory(e, { value }) {
    this.setState({
      category_id: value
    })
    this.inputType.focus()
    this.updateProductOption(value, this.state.product_type)
  }

  updateProductOption(category_id, product_type_id) {

    let _product_options = []
    for (let i in this.products) {
      let po = this.products[i]
      if (category_id !== '' && category_id !== 0 && po.category.id != category_id) {
        continue
      }

      if (product_type_id !== '' && product_type_id !== 0 && po.kind.id != product_type_id) {
        continue
      }

      _product_options.push({
        value: po.id,
        key: po.id,
        text: po.name + ' ' + po.code,
        content: <Header as='h4' content={po.name} subheader={po.code} />
      })

    }

    this.setState({
      product_options: _product_options
    })

  }

  handlerAmount(e, d) {
    e.preventDefault();
    const amount = Utility.parseInt(d.value,this.state.amount,1,this.state.max)
    const p = Utility.getObject(this.products, this.state.product_id);
    if (p) {
      this.setState({ amount:   amount});
      this.setPriceSell(p, this.state.sell_weight_real_g, amount)
      this.setDiff();
    }
  }

  setPriceSell(product, weightReal, amount) {
    let productPrice = 0
    let sell_price = 0
    const gold_bar_sell = Utility.removeCommas(this.state.gold_bar_sell);

    if (product) {
      let productPrice = 0;
      if (product.type_sale == 1) {
        let b = (Number(weightReal) / Number(product.category.weight)) * Number(product.category.m_sell);
        productPrice = (b * Number(gold_bar_sell));
      } else {
        productPrice = product.price_tag;
      }
      const product_price = Math.ceil(productPrice)
      this.setState({
        product_price: product_price,
        sell_price: Math.ceil(product_price * amount)
      });

    }

  }

  handlerChangeWeightReal(e, v) {
    const sell_weight_real_g = Utility.parseFloat(v.value,this.state.sell_weight_real_g)
    console.log('sell_weight_real_g',sell_weight_real_g,v.value)
    const p = Utility.getObject(this.products, this.state.product_id);
    if (p) {
      this.setState({ sell_weight_real_g: sell_weight_real_g })
      this.setPriceSell(p, sell_weight_real_g, this.state.amount)
    }


  }

  handlerReset(e, d) {
    e.preventDefault();
    this.setState({
      product_code: '',
      product_id: '',
      product_type: '',
      amount: '',
      discount: '',
      sell_weight_real_g: '',
      product_price: '',
      sell_price: '',
      product_name: '',
      buy_categories_id: '',
      buy_product_id: '',
      buy_weight_real_g: '',
      buy_weight_real_b: '',
      buy_price: '',
      exchange: 0
    })
  }

  handlerSubmit(e, d) {
    e.preventDefault();

    let msg_error = {}

    if(this.state.max<this.state.amount){
      alert('สินค้าในสต็อกไม่เพียงพอ');
      return;
    }
    console.log(this.state.sell_price , this.state.product_price * this.state.amount,this.state.product_price)

    if (this.props.kind !== 'SE' && this.state.buy_categories_id == '') {
      msg_error['buy_categories_id'] = ['เลือก%ทอง']
      this.categoryBuyInput.focus()
    }

    if (this.props.kind !== 'SE' && this.state.buy_weight_real_g == '') {
      msg_error['buy_weight_real_g'] = ['ต้องไม่เป็นค่าว่าง']
    }

    if (this.props.kind !== 'SE' && this.state.buy_weight_real_b == '') {
      msg_error['buy_weight_real_b'] = ['ต้องไม่เป็นค่าว่าง']
    }

    if (this.props.kind !== 'SE' && this.state.product_name == '') {
      msg_error['product_name'] = ['ต้องไม่เป็นค่าว่าง']
    }

    if (this.props.kind !== 'BU' && this.state.product_id == '') {
      msg_error['product_id'] = ['เลือกสินค้า']
    }

    if (this.props.kind !== 'BU' && (this.state.sell_price == '' || this.state.sell_price == '0')) {
      msg_error['sell_price'] = ['ต้องไม่เป็นค่าว่าง']
    }

    if (this.props.kind !== 'BU' && (this.state.sell_price < Math.ceil(this.state.product_price * this.state.amount))) {
      msg_error['sell_price'] = ['ต้องไม่น้อยกว่าราคาขายจริง']
    }

    if(this.state.exchange < 0){
      msg_error['changecost'] = ['กรุณากรอกเงินเปลี่ยนให้ถูกต้อง']
    }
    if (Object.keys(msg_error).length > 0) {
      this.setState({ msg_error: msg_error })
      return;
    }

    const code_change = (new Date()).getTime()

    let sell = ''
    let buy = ''
    if (this.state.sell_price != '') {
      sell = {
        product_code: this.state.product_code,
        product_id: this.state.product_id,
        product_type: this.state.product_type,
        amount: this.state.amount,
        discount: this.state.discount,
        sell_weight_real_g: this.state.sell_weight_real_g,
        product_price: this.state.product_price,
        sell_price: this.state.sell_price,
        gold_price: this.state.gold_bar_sell,
        exchange: this.state.exchange,
        id_change: code_change
      }
    }

    if (this.state.buy_price != '') {
      const p = Utility.getObject(this.products, this.state.buy_product_id);
      let name = '';
      if (p) {
        name = p.name;
      }
      const cate = Utility.getObject(this.props.formValue.categories, this.state.buy_categories_id);
      buy = {
        category_id: this.state.buy_categories_id,
        category_name: cate.name,
        buy_product_id: this.state.buy_product_id,
        buy_weight_real_g: this.state.buy_weight_real_g,
        buy_weight_real_b: this.state.buy_weight_real_b,
        buy_price: this.state.buy_price,
        buy_name: this.state.product_name,
        gold_price: this.state.gold_bar_sell,
        exchange: this.state.exchange,
        id_change: code_change
      }
      this.addProductName(this.state.product_name)
    }

    this.props.onSubmit(e, {
      sell: sell,
      buy: buy
    });

    this.props.onClose()
    this.handlerReset(e);
  }

  setDiff() {
    setTimeout(() => {
      if (this.state.sell_price != '' && this.state.buy_price) {
        this.setState({
          exchange: (Number(this.state.sell_price) - Number(this.state.buy_price)).toFixed(0)
        });
      } else {
        this.setState({
          exchange: 0
        });
      }
    }, 400);
  }

  setDiffExchange() {
    setTimeout(() => {
      if (this.state.sell_price != '' && this.state.buy_price) {
        this.setState({
          buy_price: (Number(this.state.sell_price) - Number(this.state.exchange)).toFixed(0)
        });
      } else {
        this.setState({
          exchange: 0
        });
      }
    }, 400);
  }

  handlerBuyWeightB(e, d) {
    e.preventDefault();

    const buy_weight_real_b = Utility.parseFloat(d.value,this.state.buy_weight_real_b)
    const cate = Utility.getObject(this.props.formValue.categories, this.state.buy_categories_id);
    if (cate != null) {
      let b = buy_weight_real_b * Number(cate.m_buy);
      let buy_price = (b * Number(Utility.removeCommas(this.state.gold_bar_buy))) + (Number(cate.discount_buy) * b);
      buy_price = buy_price < 0 ? 0 : buy_price
      this.setState({
        buy_weight_real_b: buy_weight_real_b,
        buy_weight_real_g: (Number(buy_weight_real_b * cate.weight)).toFixed(3),
        buy_price: buy_price.toFixed(0)
      })
      this.setDiff();
    }
  }

  handlerBuyWeightG(e, d) {
    e.preventDefault();
    const buy_weight_real_g = Utility.parseFloat(d.value,this.state.buy_weight_real_g)
    console.log('buy_weight_real_g',buy_weight_real_g)
    const cate = Utility.getObject(this.props.formValue.categories, this.state.buy_categories_id);
    if (cate != null) {
      let b = buy_weight_real_g / Number(cate.weight) * Number(cate.m_buy);
      let buy_price = (b * Number(Utility.removeCommas(this.state.gold_bar_buy))) + Number(cate.discount_buy);
      buy_price = buy_price < 0 ? 0 : buy_price
      this.setState({
        buy_weight_real_g: buy_weight_real_g,
        buy_weight_real_b: (buy_weight_real_g / cate.weight).toFixed(3),
        buy_price: buy_price.toFixed(0)
      })
      this.setDiff();
    }
  }

  handlerBuyPrice(e, d) {

    const buy_price = Utility.parseFloat(d.value,this.state.buy_price)
    e.preventDefault();
    this.setState({ buy_price });
    this.setDiff();
  }

  handlerExchange(e, d) {

    const exchange = Utility.parseFloat(d.value,this.state.exchange)
    e.preventDefault();
    this.setState({ exchange });
    this.setDiffExchange();
  }

  addProductName(name) {

    let url = Settings.baseUrl + "/product_name/"
    Utility.post(url, { name: name }, (s, d) => {
    })
  }

  loadProduct() {
    const {branch} = this.props.auth
    const products = Utility.getFetch(Settings.baseUrl + '/stock_product/?is_enabled=1&branch=' + branch.id);
    Promise.all([products]).then((values) => {
      let product_options = []
      this.products = []

      for (let i in values[0]) {
        values[0][i].product.amount = values[0][i].amount
        this.products.push(values[0][i].product)

        product_options.push({
          key: values[0][i].product.id,
          value: values[0][i].product.id,
          text: values[0][i].product.code + " " + values[0][i].product.name
        });
      }
      this.setState({ product_options: product_options })
    });
  }

  render() {
    return (
      <Modal open={this.props.open} /*dimmer='blurring'*/>
        <Button id="btnCloseProductSelect" circular icon='close' basic floated='right' name='' onClick={this.props.onClose} />
        <Modal.Header id='headerModalSelectProduct' as='h3'>เลือกรายการสินค้า</Modal.Header>
        <Modal.Content id='contentModalSelectProduct' >
          <Dimmer active={this.state.is_loading} inverted>
            <Loader inverted content='Loading' />
          </Dimmer>
          <Modal.Description>

            <div>
              <div className={this.props.kind == 'BU' ? 'hidden' : ''}>
                <Message id='selectForSell'
                  color='green'
                  attached
                  size='tiny'
                  header='ขายทอง'
                />
                <Form className='attached fluid segment green' color='red' size='small' onSubmit={(e) => {
                  this.handlerSubmit(e)
                }}>
                  <Form.Group>
                    <Form.Field width={4} >
                      <label>*%ทอง </label>
                      <SelectOption  selection options={this.props.formValue.state.categories} name='category_id'
                        autoFocus
                        onChange={this.handlerChangeCategory} value={this.state.category_id} onRef={(input) => { this.inputCategory = input }} />
                    </Form.Field>
                    <Form.Field width={4} >
                      <label>ประเภทสินค้า </label>
                      <SelectOption label='ประเภทสินค้า' selection options={this.props.product_types_option} name='product_type'
                        onRef={(input) => { this.inputType = input; console.log('inputType', input) }}
                        onChange={this.handlerChangeProductType} value={this.state.product_type} />
                    </Form.Field>

                    <Form.Field error={this.state.msg_error.product_id != null} width={4} >
                      <label>*สินค้า <MsgInput text={this.state.msg_error.product_id} /></label>
                      <SelectOption label='สินค้า' selection options={this.state.product_options}
                        name='product_id'
                        onRef={(c) => { this.inputProduct = c }}
                        onChange={this.handlerProductChange} value={this.state.product_id} />
                    </Form.Field>

                    <Form.Field error={this.state.msg_error.amount != null} width={4} >
                      <label>*จำนวน {this.state.max<=0?<span className='red'>*ไม่คงเหลือ</span> : <span  className='green'> คงเหลือ {Utility.numberFormat(this.state.max)}ชิ้น</span>}<MsgInput text={this.state.msg_error.amount} /></label>
                      <Input
                        placeholder=''
                        ref={(input) => { this.amountInput = input; }}
                        value={this.state.amount}
                        onChange={this.handlerAmount}
                        min='0'
                        max={this.state.max}
                        type='number'
                        onKeyPress={(e) => {

                          if (e.key == 'Enter') {
                            this.weightRealGInput.focus()
                            e.preventDefault()
                          }
                        }}
                        className='text-right' />
                    </Form.Field>

                  </Form.Group>
                  <Form.Group>
                    <Form.Field width={5} >
                      <label>น.น.ชั่ง(ก.)</label>
                      <Input value={this.state.sell_weight_real_g}
                        type='number'
                        ref={(c) => {
                          this.weightRealGInput = c
                        }}
                        onFocus={(e) => { e.target.select() }}
                        onChange={this.handlerChangeWeightReal}
                        onKeyPress={(e) => {
                          if (e.key == 'Enter') {
                            this.totalSellInput.focus()
                            e.preventDefault()
                          }
                        }}
                        className='text-right' />
                    </Form.Field>
                    <Form.Input label='ราคา/ชิ้น' readOnly placeholder='' width={5} value={this.state.product_price} className='text-right' />
                    <Form.Field error={this.state.msg_error.sell_price != null} width={5} >
                      <label>*ราคาขาย <MsgInput text={this.state.msg_error.sell_price} /></label>
                      <Input
                        id='inputPrice'
                        placeholder=''
                        type='number'
                        width={5}
                        value={this.state.sell_price}
                        min={this.state.product_price * this.state.amount}
                        onFocus={(e) => { e.target.select() }}
                        onChange={this.handlerSellPrice}
                        ref={(c) => {
                          this.totalSellInput = c
                        }}
                        onKeyPress={(e) => {
                          if (e.key == 'Enter' && this.props.kind == 'XC') {
                            this.categoryBuyInput.focus()
                            e.preventDefault()
                          }
                        }}
                        className='text-right' />
                    </Form.Field>
                  </Form.Group>
                  <div className='hidden' >
                    <Button type='submit' />
                  </div>
                </Form>
              </div>
              <br />
              <div className={this.props.kind == 'SE' ? 'hidden' : ''}>
                <Message
                  color='red'
                  attached
                  header='ซื้อทอง'
                  size='tiny'
                />
                <Form className='attached fluid segment red' color='red' size='small' onSubmit={(e) => {
                  this.handlerSubmit(e)
                }}>
                  <Form.Group >
                    <Form.Field id="percengold" error={this.state.msg_error.buy_categories_id != null}>
                      <label>*%ทอง <MsgInput text={this.state.msg_error.buy_categories_id} /></label>
                      <SelectOption
                       
                      search selection width={4} options={this.props.formValue.state.categories}

                        onRef={(c) => { this.categoryBuyInput = c }}
                        onChange={(e, d) => {
                          this.productNameInput.focus()
                          this.setState({ buy_categories_id: d.value });
                        }} value={this.state.buy_categories_id} defaultValue={this.state.buy_categories_id} />
                    </Form.Field>
                    <Form.Field id="ProductName" width={4} error={this.state.msg_error.product_name != null}>
                      <label>ชื่อสินค้า <MsgInput text={this.state.msg_error.product_name} /></label>

                      <Input
                        fluid list='products'
                        value={this.state.product_name}
                        onChange={(e, v) => this.setState({ product_name: v.value })}
                        ref={(input) => { this.productNameInput = input; }}
                        onKeyPress={(e) => {
                          if (e.key == 'Enter') {
                            this.weightRealInput.focus()
                            e.preventDefault()
                          }
                        }}
                      />
                      <datalist id='products'>
                        {this.state.products.map((v, i) => <option value={v.name} key={i} />)}
                      </datalist>
                    </Form.Field>
                    <Form.Field id="weight-g" width={4} error={this.state.msg_error.buy_weight_real_g != null}>
                      <label>น.น.ซื้อ (ก.) <MsgInput text={this.state.msg_error.buy_weight_real_g} /></label>
                      <Input placeholder='น.น.ซื้อ (ก.)'
                        ref={(c) => {
                          this.weightRealInput = c
                        }}
                        onKeyPress={(e) => {

                          if (e.key == 'Enter') {
                            this.buyPriceInput.focus()
                            e.preventDefault()
                          }
                        }}
                        type='number'
                        value={this.state.buy_weight_real_g}
                        onChange={this.handlerBuyWeightG}
                        className='text-right' />
                    </Form.Field>
                    <Form.Field id="weight-b" width={4} error={this.state.msg_error.buy_weight_real_b != null}>
                      <label>น.น.ซื้อ (บ.) <MsgInput text={this.state.msg_error.buy_weight_real_b} /></label>
                      <Input
                        placeholder='น.น.ซื้อ (บ.)'
                        width={3}
                        onKeyPress={(e) => {

                          if (e.key == 'Enter') {
                            this.buyPriceInput.focus()
                            e.preventDefault()
                          }
                        }}
                        type="number"
                        value={this.state.buy_weight_real_b}
                        onChange={this.handlerBuyWeightB}
                        className='text-right' />
                    </Form.Field>
                    <Form.Field id="money" width={3}>
                      <label>เป็นเงิน</label>
                      <Input value={this.state.buy_price}
                        onFocus={(e) => { e.target.select() }}
                        ref={(e) => { this.buyPriceInput = e }}
                        type="number"
                        onKeyPress={(e) => {

                          if (e.key == 'Enter' && this.props.kind == 'XC') {
                            this.changePriceInput.focus()
                            e.preventDefault()
                          }
                        }}
                        onChange={this.handlerBuyPrice} className='text-right' />
                    </Form.Field>
                  </Form.Group>
                  <Form.Group>
                    <Form.Input label='ราคาทองวันนี้' placeholder='' width={4} value={this.state.gold_bar_sell} className='text-right' />
                    <Form.Input label='ราคาซื้อคืนทองรูปพรรณ' placeholder='' width={4} value={this.state.gold_ornaments_buy} className='text-right' />
                    <Form.Field width={4}></Form.Field>
                    {this.props.kind == 'XC' ?
                      <Form.Field width={4}  error={this.state.msg_error.changecost != null}>
                        <label>เป็นเงินเปลี่ยน <MsgInput text={this.state.msg_error.changecost} /></label>
                        <Input
                          onFocus={(e) => { e.target.select() }}
                          ref={(c) => {
                            this.changePriceInput = c
                          }}
                          className={'text-right'}
                          placeholder='เป็นเงินเปลี่ยน'
                          type="number"
                          onChange={this.handlerExchange}
                          value={this.state.exchange} />
                      </Form.Field> : ''}
                  </Form.Group>
                  <div className='hidden' >
                    <Button type='submit' />
                  </div>
                </Form>
              </div>
              <Table celled size='small'>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell><div className='text-center'>ราคาซื้อทองคำแท่ง</div></Table.HeaderCell>
                    <Table.HeaderCell><div className='text-center'>ราคาขายทองคำแท่ง</div></Table.HeaderCell>
                    <Table.HeaderCell><div className='text-center'>ราคาซื้อคืนทองรูปพรรณ</div></Table.HeaderCell>
                    <Table.HeaderCell><div className='text-center'>ราคาขายทองรูปพรรณ</div></Table.HeaderCell>
                    <Table.HeaderCell ><div className='text-center'>วันที่</div></Table.HeaderCell>
                  </Table.Row>
                </Table.Header>

                <Table.Body>

                  <Table.Row>
                    <Table.Cell><div className='text-center'>{Utility.priceFormat(this.state.gold_bar_buy)}</div></Table.Cell>
                    <Table.Cell><div className='text-center'>{Utility.priceFormat(this.state.gold_bar_sell)}</div></Table.Cell>
                    <Table.Cell><div className='text-center'>{Utility.priceFormat(this.state.gold_ornaments_buy)}</div></Table.Cell>
                    <Table.Cell><div className='text-center'>{Utility.priceFormat(this.state.gold_ornaments_sell)}</div></Table.Cell>
                    <Table.Cell><div className='text-center'>{Utility.formatDate(this.state.record_date)} {Utility.formatTime(this.state.record_date)}</div></Table.Cell>
                  </Table.Row>
                </Table.Body>
              </Table>
            </div>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          <Button id="confirmProduct" type='submit' size='small' primary onClick={this.handlerSubmit} >ลงรายการ</Button>
        </Modal.Actions>
      </Modal>

    );
  }

  hot_keys = {
    'esc': {
      priority: 4,
      handler: this.props.onClose
    },
    'enter': {
      priority: 4,
      handler: (e) => {
        //e.preventDefault()
      }
    }
  }
}


const mapStateToProps = state =>{
  return ({
    auth: state.auth,
  })
}
export default connect(
  mapStateToProps,
)(hotkeys(SellForm))
