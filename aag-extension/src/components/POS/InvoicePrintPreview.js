/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import {
   Segment, Button, Loader, Dimmer, Modal, Table, Image, Checkbox
} from 'semantic-ui-react';
/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import Utility from '../../Utility';
import Settings from '../../Settings';
import logo from '../../logo.png';
import signature from '../../signature.png'
import moment from 'moment';
import { connect } from 'react-redux'

class DisplayPrint extends Component {

    constructor(props) {
        super(props)
    }

    render() {

        const {branch} =this.props
        console.log('branch',branch)
        const divStyle = {
            color: '#111',
            'font-size': '12px',
            'line-height': '20px',
            'font-family': 'monospace',
            'border-collapse': 'collapse',
            border: '1px solid black'
        };
        const textRight = {
            'text-align': 'right'
        }
        const textLeft = {
            'text-align': 'left'
        }
        const textCenter = {
            'text-align': 'center'
        }
        const textU = {
            'text-decoration': 'underline'
        }

        let invoice_info = Settings.invoice_info
        let citizen_id = this.props.citizen_id;
        let total = 0
        let total_weight = 0
        let total_cost = 0

        const bill_info = this.props.bill_info
        const bill_items = this.props.bill_items
        let sell_cost = bill_info.gold_price.gold_ornaments_buy / 15.2

        let is_96 = true
        let sales_force = 0

        let total_pre_vat = 0
        let buy_today = 0
        let total_diff = 0
        let total_vat = 0
        let total_net = bill_info.total

        for (let i in bill_items) {

            const product = bill_items[i].product
            const item = bill_items[i]
            const weight = parseFloat(item.weight)
            const sell = parseFloat(item.sell)

            if (sell > 0) {
                total_weight += weight
                if (product.type_sale == 1) {
                    buy_today += (weight * sell_cost)
                    const diff = (sell - (weight * sell_cost * product.category.m_buy))
                    total_diff += (diff / 1.07)
                    total_vat += (diff / 1.07) * 0.07
                    total_pre_vat += sell - ((diff / 1.07) * 0.07)
                } else {
                    buy_today += 0
                    total_diff += sell
                    total_vat += sell * 0.07
                    total_pre_vat += sell - (sell * 0.07)
                }
            }
        }

        return (<div >
            <div id='paperA4-portrait' style={{ 'page-break-before': 'always' }}>
                <Table basic className='nobarder' id='table-to-xls' style={divStyle}>
                    <Table.Body>
                        <Table.Row>
                            <Table.Cell style={{ width: '10%' }}></Table.Cell>
                            <Table.Cell style={{ width: '10%' }}></Table.Cell>
                            <Table.Cell style={{ width: '10%' }}></Table.Cell>
                            <Table.Cell style={{ width: '10%' }}></Table.Cell>
                            <Table.Cell style={{ width: '10%' }}></Table.Cell>
                            <Table.Cell style={{ width: '10%' }}></Table.Cell>
                            <Table.Cell style={{ width: '10%' }}></Table.Cell>
                            <Table.Cell style={{ width: '10%' }}></Table.Cell>
                            <Table.Cell style={{ width: '10%' }}></Table.Cell>
                            <Table.Cell style={{ width: '10%' }}></Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell rowSpan='3' style={textCenter}>
                                <Image src={logo} width={70} height={70} />
                            </Table.Cell>
                            <Table.Cell colSpan='3' style={textLeft}><b>{invoice_info.name}</b></Table.Cell>
                            <Table.Cell colSpan='3'></Table.Cell>
                            <Table.Cell colSpan='3' style={textCenter}> <b>ใบกำกับภาษี/ใบเสร็จรับเงิน</b></Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell colSpan='3' style={textLeft}>{invoice_info.address1}</Table.Cell>
                            <Table.Cell colSpan='3'></Table.Cell>
                            <Table.Cell colSpan='3' style={textCenter}> <b>Tax Invoice</b></Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell colSpan='3' style={textLeft}>{invoice_info.address2}</Table.Cell>
                            <Table.Cell colSpan='3'></Table.Cell>
                            <Table.Cell colSpan='3' style={textCenter}>{this.props.subtitle}</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell colSpan='7'></Table.Cell>
                            <Table.Cell colSpan='3' style={textCenter}> เลขประจำตัวผู้เสียภาษีอาการ {invoice_info.citizen_id}</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell colSpan='5' style={textLeft}><div style={textU}>&nbsp;&nbsp;สาขาที่ออกใบกำกับภาษี สาขาที่ {branch.code}</div></Table.Cell>
                            <Table.Cell colSpan='2' style={textRight}> วันที่ </Table.Cell>
                            <Table.Cell style={textCenter}>  </Table.Cell>
                            <Table.Cell colSpan='2' style={textLeft}> {Utility.formatDate(moment())}</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell colSpan='5' style={textLeft}></Table.Cell>
                            <Table.Cell colSpan='2' style={textRight}> เลขที่ใบกำกับภาษี </Table.Cell>
                            <Table.Cell style={textCenter}>  </Table.Cell>
                            <Table.Cell colSpan='2' style={textLeft}> {citizen_id}</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell colSpan='5' style={textLeft}></Table.Cell>
                            <Table.Cell colSpan='2' style={textRight}> วันครบกำหนด </Table.Cell>
                            <Table.Cell style={textCenter}>  </Table.Cell>
                            <Table.Cell colSpan='2' style={textLeft}> {Utility.formatDate(moment())}</Table.Cell>
                        </Table.Row>
                    </Table.Body>
                    <Table.Body style={{ border: '1px solid black' }}>
                        <Table.Row>
                            <Table.Cell colSpan='2' style={textRight}>นามลูกค้า</Table.Cell>
                            <Table.Cell colSpan='3' style={textLeft}>{bill_info.customer.name}</Table.Cell>
                            <Table.Cell colSpan='2' style={textRight}>ทองคำแท่งซื้อเข้า บาทละ</Table.Cell>
                            <Table.Cell colSpan='2' style={textRight}>{(bill_info.gold_price.gold_bar_buy)}</Table.Cell>
                            <Table.Cell style={textLeft}>บาท</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell colSpan='2' style={textRight}>ที่อยู่</Table.Cell>
                            <Table.Cell colSpan='3' style={textLeft}>{bill_info.customer.address}</Table.Cell>
                            <Table.Cell colSpan='2' style={textRight}>ทองคำแท่งขายออก บาทละ</Table.Cell>
                            <Table.Cell colSpan='2' style={textRight}>{(bill_info.gold_price.gold_bar_sell)}</Table.Cell>
                            <Table.Cell style={textLeft}>บาท</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell colSpan='2' style={textRight}></Table.Cell>
                            <Table.Cell colSpan='3' style={textLeft}>{bill_info.customer.city} {bill_info.customer.province} {bill_info.customer.postal_code}</Table.Cell>
                            <Table.Cell colSpan='2' style={textRight}>ทองรูปพรรณรับซื่อคืน บาทละ</Table.Cell>
                            <Table.Cell colSpan='2' style={textRight}>  {(bill_info.gold_price.gold_ornaments_buy)}</Table.Cell>
                            <Table.Cell style={textLeft}>บาท</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell colSpan='2' style={textRight}>เลขประจำตัวผู้เสียภาษีอาการ</Table.Cell>
                            <Table.Cell colSpan='3' style={textLeft}>{bill_info.customer.citizen_id}</Table.Cell>
                            <Table.Cell colSpan='2' style={textRight}>ทองรูปพรรณรับซื่อคืน กรัมละ</Table.Cell>
                            <Table.Cell colSpan='2' style={textRight}>  {Utility.priceFormat(sell_cost)}</Table.Cell>
                            <Table.Cell style={textLeft}>บาท</Table.Cell>
                        </Table.Row>
                    </Table.Body>
                    <Table.Body style={{ border: '1px solid black' }}>
                        <Table.Row>
                            <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'center' }}>ลำดับ</Table.Cell>
                            <Table.Cell colSpan='3' style={{ 'border-right': '1px solid black', 'text-align': 'center' }}>รายละเอียด</Table.Cell>
                            <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'center' }}>น้ำหนัก</Table.Cell>
                            <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'center' }}>จำนวน</Table.Cell>
                            <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'center' }}>หน่วย</Table.Cell>
                            <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'center' }}>หน่วยละ</Table.Cell>
                            <Table.Cell colSpan='2' style={textCenter}>จำนวนเงิน</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'center' }}>Item</Table.Cell>
                            <Table.Cell colSpan='3' style={{ 'border-right': '1px solid black', 'text-align': 'center' }}>Descripttion</Table.Cell>
                            <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'center' }}>(กรัม)</Table.Cell>
                            <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'center' }}>Qunantity</Table.Cell>
                            <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'center' }}></Table.Cell>
                            <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'center' }}>Unit</Table.Cell>
                            <Table.Cell colSpan='2' style={textCenter}>Amount</Table.Cell>
                        </Table.Row>
                    </Table.Body>
                    <Table.Body style={{ border: '1px solid black' }}>
                        {bill_items.map((item, i) => {
                            total_cost += parseFloat(item.cost)
                            return (<Table.Row key={i}>
                                <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'center' }}>{i + 1}</Table.Cell>
                                <Table.Cell colSpan='3' style={{ 'border-right': '1px solid black', 'text-align': 'left' }}>{item.product && item.product.name}</Table.Cell>
                                <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'right' }}>{Utility.weightFormat(item.weight)}</Table.Cell>
                                <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'right' }}>{item.amount}</Table.Cell>
                                <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'right' }}></Table.Cell>
                                <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'right' }}>{Utility.priceFormat(item.sell / (item.weight / item.category.weight))}</Table.Cell>
                                <Table.Cell colSpan='2' style={textRight}>{Utility.priceFormat(item.sell)}</Table.Cell>
                            </Table.Row>)
                        })}
                        {bill_items.length < 3 ? <Table.Row>
                            <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'center' }}>&nbsp;</Table.Cell>
                            <Table.Cell colSpan='3' style={{ 'border-right': '1px solid black', 'text-align': 'center' }}></Table.Cell>
                            <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'center' }}></Table.Cell>
                            <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'center' }}></Table.Cell>
                            <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'center' }}></Table.Cell>
                            <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'center' }}></Table.Cell>
                            <Table.Cell colSpan='2' style={textCenter}></Table.Cell>
                        </Table.Row> : ''}
                        <Table.Row>
                            <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'center' }}>&nbsp;</Table.Cell>
                            <Table.Cell colSpan='3' style={{ 'border-right': '1px solid black', 'text-align': 'center' }}></Table.Cell>
                            <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'center' }}></Table.Cell>
                            <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'center' }}></Table.Cell>
                            <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'center' }}></Table.Cell>
                            <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'center' }}></Table.Cell>
                            <Table.Cell colSpan='2' style={textCenter}></Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'center' }}>&nbsp;</Table.Cell>
                            <Table.Cell colSpan='3' style={{ 'border-right': '1px solid black', 'text-align': 'left' }}>{Utility.arabicNumberToText(bill_info.total)}</Table.Cell>
                            <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'center' }}></Table.Cell>
                            <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'center' }}></Table.Cell>
                            <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'center' }}></Table.Cell>
                            <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'center' }}></Table.Cell>
                            <Table.Cell colSpan='2' style={textCenter}></Table.Cell>
                        </Table.Row>

                    </Table.Body>
                    <Table.Body style={{ border: '1px solid black' }}>
                        <Table.Row>
                            <Table.Cell style={textLeft}>&nbsp;&nbsp;หมายเหตุ</Table.Cell>
                            <Table.Cell colSpan='4' style={textLeft}>แก้ไขใบส่งของกรุณาติดต่อภายใน 7วัน หากพ้นกำหนดจะไม่รับผิดชอบใดๆ ทั้งสิ้น</Table.Cell>
                            <Table.Cell style={{ 'border-right': '1px solid black', 'text-align': 'left' }}></Table.Cell>
                            <Table.Cell style={textLeft}>รวมสินค้า</Table.Cell>
                            <Table.Cell style={textLeft} colSpan='2'>รายการ น้ำหนัก </Table.Cell>
                            <Table.Cell style={textRight}>{Utility.weightFormat(total_weight)} กรัม</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell style={textLeft}>&nbsp;&nbsp;ชำระโดย</Table.Cell>
                            <Table.Cell style={textRight}><Checkbox checked={bill_info.payment == 'CS' || bill_info.payment == 'CC'} /></Table.Cell>
                            <Table.Cell style={textLeft}>เงินสด</Table.Cell>
                            <Table.Cell style={textRight}><Checkbox checked={bill_info.payment == 'CD' || bill_info.payment == 'CC'} /></Table.Cell>
                            <Table.Cell style={textLeft}>บัตรเครดิต</Table.Cell>
                            <Table.Cell style={textLeft} style={{ 'border-right': '1px solid black', 'text-align': 'left' }}></Table.Cell>
                            <Table.Cell style={textRight} colSpan='3'>ราคาสินค้ารวมค่ากำเหน็จก่อนภาษี</Table.Cell>
                            <Table.Cell style={textRight} colSpan='1'>{Utility.priceFormat(total_pre_vat)}</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell style={{ 'border-right': '1px solid black' }} colSpan='6'></Table.Cell>
                            <Table.Cell style={textRight} colSpan='3'>ราคารับซื้อทองประจำวัน</Table.Cell>
                            <Table.Cell style={textRight} colSpan='1'>{Utility.priceFormat(buy_today)}</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell style={{ 'border-right': '1px solid black' }} colSpan='6'></Table.Cell>
                            <Table.Cell style={textRight} colSpan='3'>จำนวนส่วนต่าง</Table.Cell>
                            <Table.Cell style={textRight} colSpan='1'>{Utility.priceFormat(total_diff)}</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell style={{ 'border-right': '1px solid black' }} colSpan='6'></Table.Cell>
                            <Table.Cell style={textRight} colSpan='3'>ภาษีมูลค่าเพิ่ม 7%</Table.Cell>
                            <Table.Cell style={textRight} colSpan='1'>{Utility.priceFormat(total_vat)}</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell style={{ background: '#eee', 'text-align': 'left', 'border': '1px solid black' }} colSpan='6'>&nbsp;&nbsp;เอกสารออกเป็นชุด</Table.Cell>
                            <Table.Cell style={textRight} colSpan='3'>รวมเงินรับสุทธิ</Table.Cell>
                            <Table.Cell style={textRight} colSpan='1'>{Utility.priceFormat(total_net)}</Table.Cell>
                        </Table.Row>
                    </Table.Body>
                    <Table.Body style={{ border: '1px solid black' }}>
                        <Table.Row>
                            <Table.Cell style={textLeft} colSpan='10'>&nbsp;&nbsp;ได้รับสินค้าตามรายการในสภาพเรียบร้อย</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell style={textLeft} colSpan='10'>&nbsp;RECEIVED THE ABOVE IN GOODS AND CONDITION</Table.Cell>
                        </Table.Row>
                    </Table.Body>
                    <Table.Body style={{ border: '1px solid black' }}>
                        <Table.Row>
                            <Table.Cell style={textCenter} colSpan='2'></Table.Cell>
                            <Table.Cell style={textCenter} colSpan='2'></Table.Cell>
                            <Table.Cell style={textCenter} colSpan='3'></Table.Cell>
                            <Table.Cell style={textCenter} colSpan='3'>{invoice_info.name}</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell style={textCenter} colSpan='2'><br />.................................</Table.Cell>
                            <Table.Cell style={textCenter} colSpan='2'><br />.................................</Table.Cell>
                            <Table.Cell style={textCenter} colSpan='3'><br />.................................</Table.Cell>
                            <Table.Cell style={textCenter} colSpan='3'><Image src={signature} style={{ margin: 'auto', width: '40%', display: 'block' }} /><p>.................................</p></Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell style={textCenter} colSpan='2'>ผู้รับสินค้า/Received By</Table.Cell>
                            <Table.Cell style={textCenter} colSpan='2'>ผู้ส่งสินค้า/Delivered By</Table.Cell>
                            <Table.Cell style={textCenter} colSpan='3'>ผู้ขาย /  ผู้รับเงิน</Table.Cell>
                            <Table.Cell style={textCenter} colSpan='3'>ผู้มีอำนาจลงนาม/Authorized Signature</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell style={textCenter} colSpan='2'>วันที่/Date……/……/…….</Table.Cell>
                            <Table.Cell style={textCenter} colSpan='2'>วันที่/Date……/……/…….</Table.Cell>
                            <Table.Cell style={textCenter} colSpan='3'>วันที่/Date   {Utility.formatDate(moment())}</Table.Cell>
                            <Table.Cell style={textCenter} colSpan='3'></Table.Cell>
                        </Table.Row>
                    </Table.Body>

                </Table>
            </div>
        </div>)
    }
}

class ProductDetailPrintPreview extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loader: true,
            bill_info: {},
            bill_items: []
        }
    }

    componentDidMount() {


        var bills = Utility.getFetch(Settings.baseUrl + "/bills/" + this.props.bill_id + "/");
        var bill_items = Utility.getFetch(Settings.baseUrl + "/bill/" + this.props.bill_id + "/items/");
        var tax = Utility.getFetch(Settings.baseUrl + "/bill/" + this.props.bill_id + "/tax/");
        Promise.all([bills, bill_items, tax]).then((values) => {
            let citizen_id = values[2].citizen_id
            console.log('citizen_id', citizen_id)
            this.setState({
                loader: false,
                citizen_id: citizen_id,
                bill_info: values[0],
                bill_items: values[1]
            })
        });
    }
    render() {

        const {branch} = this.props.auth

        let title = 'สรุปรายการซื้อ-ขายทอง';
        let filename = 'bills-slip'

        console.log('branch1',branch)


        return (<div>
            <Modal id='modalPreview' open={true} onClose={this.close} size='fullscreen' /*dimmer='blurring'*/>
                <Modal.Header>Preview</Modal.Header>
                <Modal.Content className='scrolling'>

                    {this.state.loader ? <Segment>
                        <Dimmer active inverted>
                            <Loader inverted>Loading</Loader>
                        </Dimmer>
                    </Segment> : <div id='view-print'><DisplayPrint bill_info={this.state.bill_info} bill_items={this.state.bill_items} subtitle='ต้นฉบับ' branch={branch} citizen_id={this.state.citizen_id} /><DisplayPrint branch={branch} citizen_id={this.state.citizen_id} bill_info={this.state.bill_info} bill_items={this.state.bill_items} subtitle='สำเนา' /></div>
                    }
                </Modal.Content>
                <Modal.Actions>
                    <ReactHTMLTableToExcel
                        id="test-table-xls-button"
                        className="ui primary button small"
                        table="table-to-xls"
                        filename={filename}
                        sheet={title}
                        buttonText="Download as XLS" >
                    </ReactHTMLTableToExcel>

                    <Button primary icon='print' size='small' onClick={() => {
                        var mywindow = window.open('', 'PRINT', 'height=600,width=1024');
                        mywindow.document.write('<html><head><title>' + title + '</title>');
                        mywindow.document.write('</head><body style="font-size: 12px;font-family: Lato,\'Helvetica Neue\',Arial,Helvetica,sans-serif;width: 100%;height: 100%; margin: 0;padding: 0;">');
                        mywindow.document.write('<style>');
                        mywindow.document.write('@page {size: A4;margin: 5mm;} ');
                        mywindow.document.write('</style>');
                        mywindow.document.write(document.getElementById('view-print').innerHTML);
                        mywindow.document.write('</body></html>');
                        mywindow.document.close(); // necessary for IE >= 10
                        mywindow.focus(); // necessary for IE >= 10*/
                        mywindow.print();
                        mywindow.close();
                    }} labelPosition='right' content='Print' />

                    <Button id='btnClosePreview' size='small' type='button' onClick={() => { this.props.onClose() }}>ปิด</Button>
                </Modal.Actions>
            </Modal>
        </div>)
    }
}


const mapStateToProps = state =>{
    return ({
      auth: state.auth,
    })
  }

  export default connect(
    mapStateToProps,
  )(ProductDetailPrintPreview)