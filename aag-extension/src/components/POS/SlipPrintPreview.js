/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import {
    Button,  Modal, Table
} from 'semantic-ui-react';
/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import Utility from '../../Utility';
import Settings from '../../Settings';
import moment from 'moment';
import { connect } from 'react-redux'


class ProductDetailPrintPreview extends Component {
    render() {

        const {branch} = this.props.auth
        let title = 'สรุปรายการซื้อ-ขายทอง';
        let filename = 'bills-slip'
        const divStyle = {
            color: '#111',
            'font-size': '12px',
            width: Settings.papter88,
            'line-height': '20px',
            'font-family': 'monospace'
        };
        const textRight = {
            'text-align': 'right'
        }
        const textLeft = {
            'text-align': 'left'
        }
        const textCenter = {
            'text-align': 'center'
        }
        const textU = {
            'text-decoration': 'underline'
        }
        let action = 'ขาย'
        if(this.props.action=='sell'){
            action = 'ขาย'
        }else if(this.props.action=='buy'){
            action = 'ซื้อ'
        }else{
            action = 'เปลี่ยน'
        }
        return (<div>
            <Modal open={true} onClose={this.close} size='fullscreen' /*dimmer='blurring'*/>
                <Modal.Header>Preview</Modal.Header>
                <Modal.Content className='scrolling'>

                    <div id='view-print'>
                        <div id='paperA4-portrait' style={divStyle}>
                            <Table basic id='table-to-xls'>

                                <Table.Body>
                                    <Table.Row >
                                        <Table.Cell style={{ 'font-size': '18px', 'text-align': 'center' }} colSpan='4'>ห้างทองเอเอ ({action})</Table.Cell>
                                    </Table.Row>
                                    <Table.Row >
                                        <Table.Cell style={textRight}>สาขา:</Table.Cell>
                                        <Table.Cell colSpan='3' style={textLeft}>{branch.name} โทร.</Table.Cell>
                                    </Table.Row>
                                    <Table.Row >
                                        <Table.Cell style={textRight}>NO:</Table.Cell>
                                        <Table.Cell colSpan='3' style={textLeft}>{this.props.data.bill_number} </Table.Cell>
                                    </Table.Row>
                                    <Table.Row >
                                        <Table.Cell colSpan='4' style={textLeft}>{Utility.formatDate(moment())} {Utility.formatTime(moment())}</Table.Cell>
                                    </Table.Row>
                                    <Table.Row >
                                        <Table.Cell style={textRight}>พนักงาน:</Table.Cell>
                                        <Table.Cell colSpan='3' style={textLeft}>{this.props.data.bill_number} </Table.Cell>
                                    </Table.Row>
                                    <Table.Row >
                                        <Table.Cell style={textRight}>รหัสลูกค้า:</Table.Cell>
                                        <Table.Cell colSpan='3' style={textLeft}>{this.props.data.bill_number} </Table.Cell>
                                    </Table.Row>
                                    <Table.Row >
                                        <Table.Cell style={textRight}>ชื่อลูกค้า:</Table.Cell>
                                        <Table.Cell style={textCenter} colSpan='3' >____________________________</Table.Cell>
                                    </Table.Row>
                                    <Table.Row >
                                        <Table.Cell style={textRight}></Table.Cell>
                                        <Table.Cell colSpan='3' style={textCenter}>         ({this.props.data.bill_number}) </Table.Cell>
                                    </Table.Row>
                                    <Table.Row >
                                        <Table.Cell style={textRight}>เบอร์โทร:</Table.Cell>
                                        <Table.Cell style={textCenter} colSpan='3' >____________________________</Table.Cell>
                                    </Table.Row>
                                </Table.Body>
                            </Table>
                        </div>
                    </div>
                </Modal.Content>
                <Modal.Actions>
                    <ReactHTMLTableToExcel
                        id="test-table-xls-button"
                        className="ui primary button small"
                        table="table-to-xls"
                        filename={filename}
                        sheet={title}
                        buttonText="Download as XLS" >
                    </ReactHTMLTableToExcel>

                    <Button primary icon='print' size='small' onClick={() => {
                        var mywindow = window.open('', 'PRINT', 'height=600,width=2480');
                        mywindow.document.write('<html><head><title>' + title + '</title><meta charset="UTF-8">');
                        mywindow.document.write('</head><body style="font-size: 8px;font-family: Lato,\'Helvetica Neue\',Arial,Helvetica,sans-serif">');
                        mywindow.document.write(document.getElementById('view-print').innerHTML);
                        mywindow.document.write('</body></html>');
                        mywindow.document.close(); // necessary for IE >= 10
                        mywindow.focus(); // necessary for IE >= 10*/
                        mywindow.print();
                        mywindow.close();
                    }} labelPosition='right' content='Print' />

                    <Button id='btnClosePreview' size='small' type='button' onClick={() => { this.props.onClose() }}>ปิด</Button>
                </Modal.Actions>
            </Modal>
        </div>)
    }
}


const mapStateToProps = state =>{
    return ({
      auth: state.auth,
      branches: state.branches
    })
  }
  
  export default connect(
    mapStateToProps,
  )(ProductDetailPrintPreview)
  