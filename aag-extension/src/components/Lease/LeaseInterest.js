/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import {
    Table,
    Column,
    Cell
} from 'fixed-data-table';
import Utility from '../../Utility';
import Settings from '../../Settings';
import PaymentInterest from './PaymentInterest';
import ItemsCell from '../Widget/ItemsCell'
import ItemsCellIcon from '../Widget/ItemsCell/ItemsCellIcon'

import {
    Form,
    Input,
    Button
} from 'semantic-ui-react';


class LeaseInterest extends Component {

    constructor(props) {
        super(props);
        this.state = {
            activeItem: window.location.pathname,
            table_width: 600,
            table_hiegh: 150,
            lease_product: [],
            loading: false,
            setting: this.props.setting,
            lease_interest: [],
            total_interest: 0,
            pay_interest_open: props.sub_action=='interest' || props.sub_action=='redeem'
        }
        this.handelSubmit = this.handelSubmit.bind(this)
        this.handelDeleteItem = this.handelDeleteItem.bind(this)

    }

    componentDidMount() {

        if (this.props.data.id) {
            let lease_interest = Utility.getFetch(Settings.baseUrl + '/lease/' + this.props.data.id + '/interest/?is_enabled=1');
            this.setState({
                loading: true
            })
                        
            Promise.all([lease_interest]).then((values) => {
                let total_interest = 0;
                for (let i in values[0]) {
                    total_interest += parseFloat(values[0][i].total_receive)
                }
                this.setState({
                    lease_interest: this.setFieldValue(values[0]),
                    total_interest: total_interest
                })
            });
        }
        setTimeout(()=>{

            let elHeight = document.getElementById('w_content')
            this.setState({ w_content: elHeight.clientWidth }); 
        },1000)

    }

    setFieldValue(list) {

        let j = 1;
        for (let i in list) {
            list[i].no = j++;
            list[i].pay_date_display = list[i].pay_date? Utility.formatDate(list[i].pay_date):''
            list[i].interest_date_display = Utility.formatDate(list[i].interest_date)

            list[i].total_pay = Utility.priceFormat(list[i].total_receive)
            list[i].time = Utility.formatTime(list[i].record_date)
            if(list[i].ledger){
                list[i].payment_title = Settings.payment[list[i].ledger.payment]
                list[i].branch_name = list[i].ledger.branch.name
            list[i].card_code = list[i].ledger.card_code
            list[i].card_code = list[i].ledger.card_code
            }
        }

        return list
    }

    handelSubmit() {
        this.setState({pay_interest_open:false})
        this.componentDidMount();
    }

    handelDeleteItem(e,item){

        if(window.confirm('ยืนยันลบ!')){
            Utility.delete(Settings.baseUrl + '/lease/' + this.props.data.id + '/interest/'+item.id+'/',(status,data,code)=>{

                if(status){
                    this.componentDidMount();
                }else if(code==403){
                    alert('ไม่สามารถลบได้ สิทธิ์ admin เท่านั้น')
                }
            })
        }
    }

    render() {
        const items = this.state.lease_interest
        return (<div id="content-body2">
            {this.state.pay_interest_open && <PaymentInterest action={this.props.sub_action} onSave={this.handelSubmit} onClose={()=>{this.setState({pay_interest_open:false})}} data={this.props.data} setting={this.state.setting} />}

            <Form>
                <Form.Group>
                    <Form.Field width={16}>
                        {this.props.data.id ? <Button id='btnAdd' disabled={this.props.data.status > 2} floated='right' primary content='เพิ่ม' size='mini' type='button' onClick={()=>{this.setState({pay_interest_open:true})}} /> : ''
                        }
                    </Form.Field>
                </Form.Group>
            </Form>
            <div id="w_content">
            <Table
                rowsCount={items.length}
                rowHeight={30}
                headerHeight={30}
                width={this.state.table_width}
                height={this.state.table_hiegh}>
                <Column
                    header={<Cell></Cell>}
                    cell={
                        <ItemsCellIcon data={items} onClick={this.handelDeleteItem}  />
                    }
                    width={50}
                />
                <Column
                    header={<Cell textAlign='text-right'>ครั้งที่</Cell>}
                    cell={
                        <ItemsCell data={items} field="no" textAlign='text-right' />
                    }
                    width={50}
                />
                <Column
                    header={<Cell className='text-right'>ดอกเบี้ยคำนวณ</Cell>}
                    cell={
                        <ItemsCell data={items} field="total" textAlign='text-right' />
                    }
                    width={180}
                />
                <Column
                    header={<Cell className='text-right'>ดอกเบี้ยรับ</Cell>}
                    cell={
                        <ItemsCell data={items} field="total_pay" textAlign='text-right' />
                    }
                    width={180}
                />
                <Column
                    header={<Cell>คิดดอกเบี้ยถึงวันที่</Cell>}
                    cell={
                        <ItemsCell data={items} field="interest_date_display" />
                    }
                    width={120}
                />
                <Column
                    header={<Cell>วันที่จ่าย</Cell>}
                    cell={
                        <ItemsCell data={items} field="pay_date_display" />
                    }
                    width={120}
                />
                <Column
                    header={<Cell>เวลา</Cell>}
                    cell={
                        <ItemsCell data={items} field="time" />
                    }
                    width={120}
                />
                <Column
                    header={<Cell>สาขา</Cell>}
                    cell={
                        <ItemsCell data={items} field="branch_name" />
                    }
                    width={120}
                />
                <Column
                    header={<Cell>ประเภทการชำระ</Cell>}
                    cell={
                        <ItemsCell data={items} field="payment_title" />
                    }
                    width={120}
                />
                <Column
                    header={<Cell>หมายเหตุ</Cell>}
                    cell={
                        <ItemsCell data={items} field="description" />
                    }
                    width={120}
                />
            </Table>
            </div>
            <div style={{marginTop: '5px'}}>
                <Form size='small'>
                <Form.Field inline>
                    <label> รวมดอกเบี้ยรับ</label>
                    <Input
                        readOnly
                        value={Utility.priceFormat(this.state.total_interest)}
                        className="text-right" />
                </Form.Field>
                </Form>
                </div>
        </div>
        );
    }
}

export default LeaseInterest;
