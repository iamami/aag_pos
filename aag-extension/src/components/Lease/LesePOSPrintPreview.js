/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import {
    Form, Segment, Header, Button, Message, Loader, Dimmer, Icon, Dropdown, Input, Modal, Grid, Popup, Label, Table
} from 'semantic-ui-react';
/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import Utility from '../../Utility';
import Settings from '../../Settings';

class BranshRow extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const textRight = {
            'text-align': 'right'
        }
        const textCenter = {
            'text-align': 'center'
        }
        const textU = {
            'text-decoration': 'underline'
        }
        return (
            <Table.Body>
                <Table.Row >
                    <Table.HeaderCell  style={textRight}>สาขา:</Table.HeaderCell>
                    <Table.HeaderCell colSpan='13'>{this.props.data.branch_name}</Table.HeaderCell>
                </Table.Row>
                {this.props.data.items.map((row, i) => <Table.Row key={i}>
                    <Table.Cell style={textCenter}>{i + 1}</Table.Cell>
                    <Table.Cell style={textCenter}>{row.number}</Table.Cell>
                    <Table.Cell style={textCenter}>{row.start_date_display}</Table.Cell>
                    <Table.Cell style={textCenter}>{row.customer_name}</Table.Cell>
                    <Table.Cell style={textCenter}>{row.product_name}</Table.Cell>
                    <Table.Cell style={textRight}>{row.weight}</Table.Cell>
                    <Table.Cell style={textRight}>{row.interest}</Table.Cell>
                    <Table.Cell style={textRight}>{row.month}</Table.Cell>
                    <Table.Cell style={textRight}>{row.amount_display}</Table.Cell>
                    <Table.Cell style={textRight}>{row.interest_m}</Table.Cell>
                    <Table.Cell style={textRight}>{row.total_interest}</Table.Cell>
                    <Table.Cell style={textCenter}>{row.end_date_display}</Table.Cell>
                    <Table.Cell style={textCenter}>{row.close_date_display}</Table.Cell>
                    <Table.Cell style={textCenter}>{row.phone}</Table.Cell>
                </Table.Row>)}
                <Table.Row >
                    <Table.HeaderCell colSpan='3'></Table.HeaderCell>
                    <Table.HeaderCell style={textRight}>รวมตามสาขา</Table.HeaderCell>
                    <Table.HeaderCell colSpan='2' style={textRight}><div style={textU}>{Utility.priceFormat(this.props.data.weight)}</div></Table.HeaderCell>
                    <Table.HeaderCell ></Table.HeaderCell>
                    <Table.HeaderCell colSpan='2' style={textRight}><div style={textU}>{Utility.priceFormat(this.props.data.amount)}</div></Table.HeaderCell>
                    <Table.HeaderCell colSpan='2' style={textRight}><div style={textU}>{Utility.priceFormat(this.props.data.total_interest)}</div></Table.HeaderCell>
                    <Table.HeaderCell colSpan='3'></Table.HeaderCell>
                </Table.Row>
            </Table.Body>
        )

    }
}

class ProductDetailPrintPreview extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        let data = this.props.data

        let title = 'รายการขายฝาก';
        let filename = 'lese-' + Utility.formatDate3(data.start_date) + '-to-' + Utility.formatDate3(data.end_date);

        const divStyle = {
            color: '#111',
            'font-size': '10px',
            width: '100%',
            'line-height': '20px',
            'font-family': 'monospace'
        };
        const textRight = {
            'text-align': 'right'
        }
        const textCenter = {
            'text-align': 'center'
        }
        const textU = {
            'text-decoration': 'underline'
        }

        const branch = {}
        let weight = 0
        let amount = 0
        let total_interest = 0
        const branch_arr = []
        for (let i in this.props.items) {
            const item = this.props.items[i]
            if (branch[item.branch.id] == null)
                branch[item.branch.id] = {
                    branch_name: item.branch_name,
                    weight: 0,
                    amount: 0,
                    total_interest: 0,
                    items: []
                }

            branch[item.branch.id].items.push(item)
            branch[item.branch.id].weight += parseFloat(Utility.removeCommas(item.weight))
            branch[item.branch.id].amount += parseFloat(Utility.removeCommas(item.amount))
            branch[item.branch.id].total_interest += parseFloat(Utility.removeCommas(item.total_interest))
            weight += parseFloat(Utility.removeCommas(item.weight))
            amount += parseFloat(Utility.removeCommas(item.amount))
            total_interest += parseFloat(Utility.removeCommas(item.total_interest))
        }

        for (let i in branch) {
            branch_arr.push(branch[i])
        }

        return (<div>
            <Modal open={true} onClose={this.close} size='fullscreen' /*dimmer='blurring'*/>
                <Modal.Header>Preview</Modal.Header>
                <Modal.Content className='scrolling'>

                    <div id='view-print'>
                        <div id='paperA4-portrait'>
                            <Table basic id='table-to-xls' style={divStyle}>
                                <Table.Header>
                                    <Table.Row >
                                        <Table.HeaderCell colSpan='14'><center>{title}</center></Table.HeaderCell>
                                    </Table.Row>
                                    <Table.Row>
                                        <Table.HeaderCell style={textCenter}>ลำดับที่</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter}>เลขที่บิล</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter}>วันที่นำเข้า</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter}>ชื่อลูกค้า</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter}>รายการสินค้า</Table.HeaderCell>
                                        <Table.HeaderCell style={textRight}>น้ำหนักรวม</Table.HeaderCell>
                                        <Table.HeaderCell style={textRight}>อัตราดอกเบี้ย</Table.HeaderCell>
                                        <Table.HeaderCell style={textRight}>จำนวนเดือน</Table.HeaderCell>
                                        <Table.HeaderCell style={textRight}>จำนวนเงิน</Table.HeaderCell>
                                        <Table.HeaderCell style={textRight}>ดอกเบี้ย/เดือน</Table.HeaderCell>
                                        <Table.HeaderCell style={textRight}>รวมดอกเบี้ยรับ</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter}>วันครบกำหนด</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter}>วันที่ไถ่คืน</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter}>เบอร์โทร</Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>


                                {branch_arr.map((row2, i) =>
                                    <BranshRow key={i} data={row2} />
                                )}
                                <Table.Body>
                                    <Table.Row >
                                    <Table.HeaderCell colSpan='3'></Table.HeaderCell>
                                    <Table.HeaderCell style={textRight}>รวมทั้งหมด</Table.HeaderCell>
                                    <Table.HeaderCell colSpan='2' style={textRight}><div style={textU}>{Utility.priceFormat(weight)}</div></Table.HeaderCell>
                                    <Table.HeaderCell ></Table.HeaderCell>
                                    <Table.HeaderCell colSpan='2' style={textRight}><div style={textU}>{Utility.priceFormat(amount)}</div></Table.HeaderCell>
                                    <Table.HeaderCell colSpan='2' style={textRight}><div style={textU}>{Utility.priceFormat(total_interest)}</div></Table.HeaderCell>
                                    <Table.HeaderCell colSpan='3'></Table.HeaderCell>
                                    </Table.Row>
                                </Table.Body>
                            </Table>
                        </div>
                    </div>
                </Modal.Content>
                <Modal.Actions>
                    <ReactHTMLTableToExcel
                        id="test-table-xls-button"
                        className="ui primary button small"
                        table="table-to-xls"
                        filename={filename}
                        sheet={title}
                        buttonText="Download as XLS" >
                    </ReactHTMLTableToExcel>

                    <Button primary icon='print' size='small' onClick={() => {
                        var mywindow = window.open('', 'PRINT', 'height=600,width=1024');
                        mywindow.document.write('<html><head><title>' + title + '</title>');
                        mywindow.document.write('</head><body style="font-size: 8px;font-family: Lato,\'Helvetica Neue\',Arial,Helvetica,sans-serif">');
                        mywindow.document.write(document.getElementById('view-print').innerHTML);
                        mywindow.document.write('</body></html>');
                        mywindow.document.close(); // necessary for IE >= 10
                        mywindow.focus(); // necessary for IE >= 10*/
                        mywindow.print();
                        mywindow.close();
                    }} labelPosition='right' content='Print' />

                    <Button id='btnClosePreview' size='small' type='button' onClick={() => { this.props.onClose() }}>ปิด</Button>
                </Modal.Actions>
            </Modal>
        </div>)
    }
}

export default ProductDetailPrintPreview;