/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import {
  Form, 
  Segment, 
  Header, 
  Button, 
  Loader, 
  Dimmer, 
  Input, 
  Sidebar,
} from 'semantic-ui-react';
import Settings from '../../Settings';
import Utility from '../../Utility';

import {activations_branch} from '../../actions'
import LeaseForm from './LeaseForm'
import { connect } from 'react-redux'
import {lease_modal_edit} from '../../actions'

class LeaseSearchByNumber extends Component {

  constructor(props) {
    super(props);

    this.state = {
      number: ''
    }
    this.handleInput = this.handleInput.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  onSubmit(e) {
    e.preventDefault();
    const {branch} = this.props
    this.setState({
      loading: true
    })

    let url = Settings.baseUrl + "/lease/?number=" + this.state.number+"&branch="+ branch.id
    Utility.get(url, (s, d) => {
      this.setState({
        loading: false
      })
      if (s && d.length == 1) {
        localStorage.setItem('auto_action_lease', this.props.action);
        let lease = d[0];
        if((lease.status ==3 ||lease.status ==4)){
          alert('ใบขายฝากถูก'+ (lease.status==3?'ไถ่คืน':'คัดออก')+ 'แล้ว!') 
          return false
        }
        this.props.handleOpenLease(lease)
      } else {
        alert('ไม่พบข้อมูลขายฝากนี้')
      }

    })
  }

  componentDidMount() {

    console.log('this.props',this.props)
    const {branch} = this.props
    if(branch)
      Utility.get(Settings.baseUrl + "/branch_setting/?branch=" + branch.id, (e, d) => {
        this.setState({
          setting: d[0]
        })
      });
  }

  handleInput(e, v) {

    this.setState({
      number: v.value
    })
  }
  
  render() {
    const {lease_modal} = this.props
    return (
      <div>
        <Form >
                    <Form.Group>
                        <Form.Field width={6}>
                            <Header floated='left' as='h2'>{this.props.title}</Header>
                        </Form.Field>
                    </Form.Group>
                </Form>
      <div className="box-login">
                     
        <Sidebar.Pushable >
          <Segment textAlign='left' >
            <Sidebar animation='overlay' direction='top' visible={this.state.visible} inverted>
              <div className='message-alert'>
                บันทึกข้อมูลสำเร็จ
            </div>
            </Sidebar>
            <Dimmer active={this.state.loader_active} inverted>
              <Loader inverted content='Loading' />
            </Dimmer>
            <Form textAlign='left' onSubmit={this.onSubmit} size='small'>
              <Form.Field>
                <label>เลขที่</label>
                <Input id='inputBillNum' placeholder='เลขที่' value={this.state.number} name='number' onChange={this.handleInput} />
              </Form.Field>
              <center>
                <br />
                <Button id='btnSearch' loading={this.state.loading} type='submit' primary>ค้นหา</Button>
              </center>
            </Form>
          </Segment>
        </Sidebar.Pushable>

        {lease_modal.is_open && this.state.setting &&
          <LeaseForm
            setting={this.state.setting}
            action='edit'
            sub_action={this.props.action}
            open={true}
            loading={this.state.lease_loading}
            data={this.state.data}
            onClose={() => {
              this.setState({ open: false })
            }}
          />  }
      </div>
      </div>
    )
  }

}

const mapStateToProps = state =>{
  return ({
    lease_modal: state.lease_modal,
    auth: state.auth,
    branches: state.branches
  })
}
const mapDispatchToProps = dispatch => ({
  handleBranchChange: (branch) => {
    dispatch(activations_branch(branch))
  },
  handleOpenLease: (lease)=>{
    lease_modal_edit(dispatch,lease.id)
  }
  
})
export default connect(
  mapStateToProps,mapDispatchToProps
)(LeaseSearchByNumber)

