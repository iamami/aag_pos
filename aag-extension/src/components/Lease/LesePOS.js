/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import {
    Form, Header, Button, Loader, Dimmer, Input,Checkbox
} from 'semantic-ui-react';
import moment from 'moment';
import {
    Table,
    Column,
    Cell
} from 'fixed-data-table';
import Settings from '../../Settings';
import Utility from '../../Utility';
import LeaseForm from './LeaseForm';
import LesePOSPrintPreview from './LesePOSPrintPreview';
import LeaseReportModal from './LeaseReportModal';
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";
import InputSearchCustomer from '../Customer/InputSearchCustomer';
import OptionItemsCell from './Cell/OptionItemsCell'
import ImportModal from './Modal/ImportModal'
import {hotkeys} from 'react-keyboard-shortcuts'
import LeaseSearch from './LeaseSearch'
import { connect } from 'react-redux'
import {lease_modal_edit,lease_modal_add_open} from '../../actions'

function collect(props) {
    return { positon: props.positon };
}
class ItemsCell extends Component {
    constructor(props) {
        super(props);
    }


    render() {
        const { rowIndex, field, data, ...props } = this.props;
        return (
            <ContextMenuTrigger id="menu_lese_list"
                holdToDisplay={1000}
                key={rowIndex}
                positon={rowIndex}
                collect={collect}>
                <Cell {...this.props} onClick={() => {
                }} >
                    <div  className={(this.props.textAlign == null ? '' : this.props.textAlign) + ' text-cell'}>{data[rowIndex][field]}</div>
                </Cell>
            </ContextMenuTrigger>
        );
    }
}


class LeasePOS extends Component {

    constructor(props) {
        super(props);

        this.state = {
            message_error_hidden: true,
            items: [],
            invoice_detail: '',
            total_item: 0,
            open: false,
            modal_data: {},
            is_enabled: 1,
            start_date: moment(),
            end_date: moment(),
        }

        this.product_code_all = [];
        this.product_name_all = [];
        this.categories = [];
        this.product_types = [];
        this.onContextMenu = this.onContextMenu.bind(this);
        this.handleClick = this.handleClick.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleSearch = this.handleSearch.bind(this)
    }

    handleSearch(data) {

        this.data_search = data
        if(data.in_date_start)
            data.start_date = Utility.formatDate2(data.in_date_start)
        if(data.in_date_end)
            data.end_date = Utility.formatDate2(data.in_date_end)

        if(data.start_end_date)
            data.start_end_date = Utility.formatDate2(data.start_end_date)
        if(data.end_end_date)
            data.end_end_date = Utility.formatDate2(data.end_end_date)
        
        if(data.start_out_date)
            data.start_out_date = Utility.formatDate2(data.start_out_date)
        if(data.end_out_date)
            data.end_out_date = Utility.formatDate2(data.end_out_date)
            
        if(data.start_close_date)
            data.start_close_date = Utility.formatDate2(data.start_close_date)
        if(data.end_close_date)
            data.end_close_date = Utility.formatDate2(data.end_close_date)

        if(data.due_date)
            data.due_date = Utility.formatDate2(data.due_date)

        if (data.status == 10) {
            data.overdue =1
            data.due_date =  Utility.formatDate2(moment())
        }

        if (data.status > 4) {
            delete data.status
        }

        const {branch,role} = this.props.auth

        data['branch'] = branch.id

        this.setState({
            search_open: false,
            search_loading: true,
            loader_active: true
        })

        let s = Utility.jsonToQueryString(data)
        var lease = Utility.getFetch(Settings.baseUrl + '/lease/?' + s);
        Promise.all([lease]).then((values) => {
            var items = this.setFieldValue(values[0]);

            // calculate
            let total_interest = 0;
            let total = 0;
            let weight = 0;
            let lease_in = '';
            let j = 0;
            let limit = items.length
            for (let i in items) {
                total_interest += parseFloat(items[i].total_interest)
                total += parseFloat(items[i].amount)
                weight += parseFloat(items[i].weight)
                lease_in += items[i].id
                if (j < limit - 1)
                    lease_in += ","
                j++
            }

            this.setState({
                items: items,
                loader_active: false,
                total: total,
                lease_in: lease_in,
                total_interest: total_interest,
                search_loading: false,
            });
        });
    }

    componentDidMount() {

        const {branch} = this.props.auth
        this.setState({
            total_item: 0,
        })

        var s = {
            branch: branch.id,
            is_enabled: 1
        }
        if (this.state.end_date != null && this.state.end_date != '') {
            s['end_date'] = Utility.formatDate2(this.state.end_date)
        }
        if (this.state.start_date != null && this.state.start_date != '') {
            s['start_date'] = Utility.formatDate2(this.state.start_date)
        }

        Utility.get(Settings.baseUrl + "/branch_setting/?branch=" + branch.id, (e, d) => {
            this.setState({
                setting: d[0]
            })
        });

        let elHeight = document.getElementById('client_width')
        this.setState({ table_width: elHeight.clientWidth});
    }

    setFieldValue(item) {

        for (let i = 0; i < item.length; i++) {
            item[i].branch_name = item[i].branch.name
            item[i].customer_name = item[i].customer.name
            item[i].amount_display = Utility.priceFormat(item[i].amount)
            item[i].status_title = Settings.status_lease[item[i].status]
            let amount = parseInt(item[i].amount)
            let interest = parseInt(item[i].interest)
            item[i].interest_m = Utility.priceFormat(amount * (interest / 100))
            item[i].enabled_title = item[i].is_enabled == 0 ? 'ยกเลิกบิลแล้ว' : ''
            item[i].start_date_display = Utility.formatDate(item[i].start_date)
            item[i].end_date_display = Utility.formatDate(item[i].end_date)
            item[i].close_date_display = item[i].status == 3 ? Utility.formatDate(item[i].close_date) : ''
            //this.branches
        }
        return item;
    }

    clearFormSearch() {
        this.setState({
            branches_id: '',
            vendors_id: '',
            start_date: moment(),
            end_date: moment(),
            is_clear_stock: false,
            is_clear_bill: false
        })
    }

    onContextMenu(e, data) {
        e.preventDefault();
        lease_modal_edit(this.props.dispatch,data.id)
    }

    handleClick(e, data) {
        let item = this.state.items[data.positon];
        if (data.action == 'edit') {
            if (item.is_enabled == 0) {
                alert('ไม่สามารถแก้ไขข้อมูลได้เนื่องจากยกเลิกบิลแล้ว')
                return;
            }
            this.loadLeaseDetail(item.id, item)
        } else if (data.action == 'delete') { // delete bill
            if (item.is_enabled == 1) {
                alert('ไม่สามารถลบได้ กรุณายกเลิกบิลก่อน')
                return;
            } else {
                if (window.confirm('ยืนยันลบรายการนี้'))
                    this.submitDelete(item);
            }
        } else if (data.action == 'cancel_1') {
            if (item.is_enabled == 0) {
                alert('รายการนี้ถูกยกเลิกแล้ว')
                return;
            } else {
                if (window.confirm('ยืนยันยกเลิกรายการขายฝาก'))
                    this.submitCancel1(item);
            }
        } else if (data.action == 'cancel_2') {

            if (item.is_enabled == 0) {
                alert('รายการนี้ถูกยกเลิกแล้ว')
                return;
            } else if (item.status == 3) {
                if (window.confirm('ยืนยันยกเลิกไถ่คืน'))
                    this.submitCancel2(item);
            } else {
                alert('ไม่สามารถยกเลิกไถ่คืนได้เนื่องจากรายการนี้ไม่อยู่ในสถานะไถ่คืน')
            }
        } else if (data.action == 'cancel_3') {
            if (item.is_enabled == 0) {
                alert('รายการนี้ถูกยกเลิกแล้ว')
                return;
            } else if (item.status == 4) {
                if (window.confirm('ยืนยันยกเลิกคัดออก'))
                    this.submitCancel3(item);
            } else {
                alert('ไม่สามารถยกเลิกคัดออกได้เนื่องจากรายการนี้ไม่อยู่ในสถานะคัดออก');
            }
        }
    }

    async submitDelete(item) { // ลบรายการขายฝาก

        let url = Settings.baseUrl + "/lease/" + item.id + "/"
        const res = await Utility.deleteAsync(url);
        console.log('res',res)
        if(res.status_code==200 || res.status_code==204){
            alert('ลบเรียบร้อย');
            this.handleSearch(this.data_search)
        }else if(res.status_code==403){
            alert('คุณไม่สามารถลบรายการได้');
        }else
            alert(res.data.error)
    }

    async submitCancel1(item) { // ยกเลิกขายฝาก
        let url = Settings.baseUrl + "/lease/" + item.id + "/cancel/"
        const res = await Utility.postAsync(url, {});
        if(res.status_code==200){
            alert('ยกเลิกเรียบร้อย');
            this.handleSearch(this.data_search)
        }else if(res.status_code==403){
            alert('คุณไม่สามารถลบรายการได้');
        }else
            alert(res.data.error)
    }

    async submitCancel2(item) { // ยกเลิกไถ่คืน
        let url = Settings.baseUrl + "/lease/" + item.id + "/cancel_redeem/"
        const res = await Utility.postAsync(url, {});

        if(res.status_code==200){
            alert('ยกเลิกเรียบร้อย');
            this.handleSearch(this.data_search)
        }else if(res.status_code==403){
            alert('คุณไม่สามารถลบรายการได้');
        }else
            alert(res.data.error)
    }

    async submitCancel3(item) { // ยกเลิกคัดออก

        let url = Settings.baseUrl + "/lease/" + item.id + "/cancel_out/"
        const res = await Utility.postAsync(url, {});

        if(res.status_code==200){
            alert('ยกเลิกเรียบร้อย');
            this.handleSearch(this.data_search)

            
        }else if(res.status_code==403){
            alert('คุณไม่สามารถลบรายการได้');
        }else
            alert(res.data.error)
    }

    handleInputChange(e, v) {
        this.setState({
            [v.name]: v.value
        })
    }

    loadLeaseDetail(id, data) {
        lease_modal_edit(this.props.dispatch,id)
    }

    render() {
        const items = this.state.items;
        const {lease_modal} = this.props
        const {branch,role} = this.props.auth
        return (
            <div>
                <ImportModal open={this.state.import_open} onClose={() => this.setState({import_open: false})} />
                <Form >
                    <Form.Group>
                        <Form.Field width={6}>
                            <Header floated='left' as='h3'>รายการขายฝาก</Header>
                        </Form.Field>
                        <Form.Field width={10}>
                            
                            <Button id='btnImport' content='Import' size='small' onClick={() => this.setState({import_open: true})} floated='right' icon='file alternate outline' labelPosition='left' type='button' primary />
                            <Button id='btnCreateLease' content='สร้างรายการขายฝาก (F1)' size='small' onClick={() => {
                                this.props.dispatch(lease_modal_add_open())
                            }} floated='right' icon='plus' labelPosition='left' type='button' primary />
                        </Form.Field>
                    </Form.Group>
                </Form>
                {role === 'A' && <ContextMenu id="menu_lese_list">
                    <MenuItem data={{ action: 'edit' }} onClick={this.handleClick}>
                        แก้ไข
                    </MenuItem>
                    <MenuItem data={{ action: 'delete' }} onClick={this.handleClick}>
                        ลบ
                    </MenuItem>
                    <MenuItem divider />
                    <MenuItem data={{ action: 'cancel_1' }} onClick={this.handleClick}>
                        ยกเลิกรายการขายฝาก
                    </MenuItem>
                    <MenuItem data={{ action: 'cancel_2' }} onClick={this.handleClick}>
                        ยกเลิกการไถ่คืน
                    </MenuItem>
                    <MenuItem data={{ action: 'cancel_3' }} onClick={this.handleClick}>
                        ยกเลิกการคัดออก
                    </MenuItem>
                </ContextMenu>}
                
                {lease_modal.is_open && this.state.setting &&
                    <LeaseForm
                        setting={this.state.setting}
                        onReset={()=>{
                            this.setState({ open: false })
                            setTimeout(()=>{
                                this.props.dispatch(lease_modal_add_open())
                            },400)
                            
                        }}
                        onCreate={(d) => {
                            this.setState({
                                modal_data: d,
                                modal_action: 'edit'
                            })
                        }}
                        onClose={() => {
                            this.setState({ open: false })
                        }}
                    />
                }
                
                <div className='relative'>
                    <Dimmer active={this.state.loader_active} inverted>
                        <Loader inverted content='Loading' />
                    </Dimmer>
                    {this.state.open_print ? <LesePOSPrintPreview
                        data={
                            {
                                start_date: this.state.start_date,
                                end_date: this.state.end_date,
                                total_amount: this.state.total_amount,
                                total_weight: this.state.total_weight
                            }
                        }
                        items={items}
                        onClose={() => {
                            this.setState({ open_print: false });
                        }}
                    /> : ''}
                    <div id='client_width'>
                        <LeaseSearch onSubmit={this.handleSearch} />
                        <Table
                            rowsCount={items.length}
                            rowHeight={35}
                            headerHeight={30}
                            width={this.state.table_width}
                            height={450}>
                             <Column
                                header={<Cell></Cell>}
                                cell={
                                    <OptionItemsCell action={role=='A'?['edit','delete']:['edit']} data={items} field="number" onClickMenu={this.handleClick} data={items} />
                                }
                                width={70}
                            />
                            
                            <Column
                                header={<Cell>เลขที่</Cell>}
                                cell={
                                    <ItemsCell id='leaseID' data={items} field="number" />
                                }
                                width={100}
                            />
                            <Column
                                header={<Cell>สาขา</Cell>}
                                cell={
                                    <ItemsCell data={items} field="branch_name" />
                                }
                                width={120}
                            />
                            <Column
                                header={<Cell>ซื่อลูกค้า</Cell>}
                                cell={
                                    <ItemsCell id='cusName' data={items} field="customer_name" />
                                }
                                width={120}
                            />
                            <Column
                                header={<Cell className='text-right'>อัตรา ดบ.</Cell>}
                                cell={
                                    <ItemsCell data={items} field="interest" textAlign="text-right" />
                                }
                                width={100}
                            />
                            <Column
                                header={<Cell className='text-right'>จน. เดือน</Cell>}
                                cell={
                                    <ItemsCell data={items} field="month" textAlign="text-right" />
                                }
                                width={100}
                            />
                            <Column
                                header={<Cell className='text-right'>จน. เงิน</Cell>}
                                cell={
                                    <ItemsCell data={items} field="amount_display" textAlign="text-right" />
                                }
                                width={100}
                            />
                            <Column
                                header={<Cell className='text-right'>ดบ./เดือน</Cell>}
                                cell={
                                    <ItemsCell data={items} field="interest_m" textAlign="text-right" />
                                }
                                width={100}
                            />
                            <Column
                                header={<Cell className='text-right'>รวม ดบ.รับ</Cell>}
                                cell={
                                    <ItemsCell data={items} field="total_interest" textAlign="text-right" />
                                }
                                width={100}
                            />
                            <Column
                                header={<Cell>วันที่นำเข้า</Cell>}
                                cell={
                                    <ItemsCell data={items} field="start_date_display" />
                                }
                                width={100}
                            />
                            <Column
                                header={<Cell>วันครบกำหนด</Cell>}
                                cell={
                                    <ItemsCell data={items} field="end_date_display" />
                                }
                                width={100}
                            />
                            <Column
                                header={<Cell>วันที่ไถ่คืน/วันที่คัดออก</Cell>}
                                cell={
                                    <ItemsCell data={items} field="close_date_display" />
                                }
                                width={130}
                            />
                            <Column
                                header={<Cell>สถานะ</Cell>}
                                cell={
                                    <ItemsCell data={items} field="status_title" />
                                }
                                width={100}
                            />

                            <Column
                                header={<Cell>สถานะยกเลิก</Cell>}
                                cell={
                                    <ItemsCell data={items} field="enabled_title" />
                                }
                                width={100}
                            />
                        </Table>
                    </div>
                </div>
                <br />
                <Form size='small' >
                    <Form.Group>
                        <Form.Field width={16}>
                            <LeaseReportModal
                                lease_in={this.state.lease_in}
                                total_interest={this.state.total_interest}
                                total={this.state.total}
                                total_add={this.state.total_add}
                                total_delete={this.state.total_delete}
                                total_sum={this.state.total_sum}
                            />
                            <Button id='print' content='พิมพ์ (F8)' size='small' onClick={() => {
                                this.setState({ open_print: true });
                            }} floated='right' icon='print' labelPosition='left' type='button' primary />

                        </Form.Field>
                    </Form.Group>
                </Form>
            </div>
        );
    }


  hot_keys = {
    'f1': {
      priority: 3,
      handler: (event) => {
        event.preventDefault()        
        this.props.dispatch(lease_modal_add_open())  
      }
    },
    'f8': {
      priority: 3,
      handler: (event) => {
        event.preventDefault()
        this.setState({ open_print: true });
        
      }
    }
  }
}

const mapStateToProps = state =>{
    return ({
      auth: state.auth,
      branches: state.branches,
      lease_modal: state.lease_modal
    })
  }

  export default connect(
    mapStateToProps,
  )(hotkeys(LeasePOS))