/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import {
    Form, Segment, Header, Button, Message, Loader, Dimmer, Icon, Dropdown, Input, Modal, Grid, Popup, Label, Table
} from 'semantic-ui-react';
/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import Utility from '../../Utility';
import Settings from '../../Settings';



class ProductDetailPrintPreview extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true
        }
    }

    componentDidMount() {

        if (this.props.id) {
            let lease = Utility.getFetch(Settings.baseUrl + '/lease/' + this.props.id + '/');
            let product = Utility.getFetch(Settings.baseUrl + '/lease/' + this.props.id + '/product/?is_enabled=1');
            this.setState({
                loading: true
            })

            Promise.all([lease,product]).then((values) => {
                let total_interest = 0;

                this.setState({
                    loading: false,
                    lease: values[0],
                    product: values[1]
                })
            });
        }

    }
    render() {
        let title = 'รายการขายฝาก';
        let filename = 'lese';
        const textRight = {
            'text-align': 'right'
        }
        const textCenter = {
            'text-align': 'center'
        }
        const textU = {
            'text-decoration': 'underline'
        }

        let total_amount = 0;
        let total_weight = 0

        return (<div>
            <Modal id='ModalLeasePreview' open={true} onClose={this.close} size='fullscreen' /*dimmer='blurring'*/>
                <Modal.Header>Preview</Modal.Header>
                <Modal.Content className='scrolling'>
                    {this.state.loading?<Segment>
                        <Dimmer active inverted>
                            <Loader inverted content='Loading' />
                        </Dimmer>
                    </Segment>:
                    <div id='view-print' style={{ 'font-size': '4mm', width: '100%', 'min-height': 'auto', 'font-family': 'monospace' }}>
                        <div id='paperA4-portrait' style={{ width: '3.98in', 'height': '14cm', 'min-height': '5.5in', 'font-size': '4mm', 'page-break-before': 'always' }} >
                            <div style={{ marginLeft: '1mm', marginTop: '10mm', 'font-size': '4mm' }}>
                                <b>{this.state.lease.number}</b>
                            </div>
                            <div style={{ marginLeft: '1mm', 'font-size': '3mm' }}>
                                <b>ID: {this.state.lease.citizen_id}</b>
                            </div>
                            <div style={{ marginTop: '27mm', height: '6mm' }}>
                                <div style={{ float: 'left', marginLeft: '6mm', 'font-size': '4mm', width: '59mm' }}>{this.state.lease.customer.name}</div>
                                <div style={{ float: 'left', width: '28mm' }}>{this.state.lease.phone}</div>
                            </div>
                            <div style={{ marginTop: '5.0mm', height: '15mm', 'font-size': '3.0mm' }}>

                                {this.state.product.map((item,i)=>{

                                    total_weight += parseFloat(item.weight);
                                    total_amount += parseInt(item.amount);
                                    return (<div style={{ height: '4mm', marginLeft: '3mm' }}>
                                    <div style={{ float: 'left', marginLeft: '0mm', width: '50mm' }}>{item.name}</div>
                                    <div style={{ float: 'left', textAlign: 'right', width: '15mm' }}>{item.amount}</div>
                                    <div style={{ float: 'left', textAlign: 'right', width: '20mm' }}>{Utility.weightFormat(item.weight)}</div>
                                </div>)
                                })}
                                <div style={{ height: '3mm', marginLeft: '3mm' }}>
                                    <div style={{ float: 'left', marginLeft: '0mm', width: '50mm' }}>	&nbsp;</div>
                                    <div style={{ float: 'left', textAlign: 'right', width: '15mm', 'text-decoration': 'underline' }}>{total_amount}</div>
                                    <div style={{ float: 'left', textAlign: 'right', width: '20mm', 'text-decoration': 'underline' }}>{Utility.weightFormat(total_weight)}</div>
                                </div>
                            </div>
                            <div style={{ marginTop: '6.5mm', height: '6mm', 'font-size': '4.0mm' }}>
                                <div style={{ float: 'left', marginLeft: '5mm', 'font-size': '4mm', width: '35mm' }}>{Utility.dateThai(this.state.lease.start_date)}</div>
                                <div style={{ float: 'left', marginLeft: '16mm', textAlign: 'center', 'font-size': '4mm', width: '35mm' }}>{Utility.dateThai(this.state.lease.end_date)}</div>
                            </div>
                            <div style={{ marginTop: '0.0mm', height: '6mm', 'font-size': '4.0mm' }}>
                                <div style={{ float: 'left', marginLeft: '15mm', 'font-size': '4mm', width: '30mm', paddingTop: '2mm' }}>{Utility.priceFormat(this.state.lease.amount)}</div>
                                <div style={{ float: 'left', marginLeft: '4mm', textAlign: 'center', 'font-size': '3mm', width: '42mm', paddingTop: '2mm' }}>{Utility.arabicNumberToText(this.state.lease.amount)}</div>
                            </div>
                        </div>
                    </div>}
                </Modal.Content>
                <Modal.Actions>
                    <Button 
                        id='printLease'
                        primary icon='print' size='small' onClick={() => {
                        var mywindow = window.open('', 'PRINT', 'height=600,width=1024');
                        mywindow.document.write('<html><head><title>' + title + '</title>');
                        mywindow.document.write('</head><body style="font-size: 6mm;font-family: monospace;">');
                        mywindow.document.write('<style>');
                        mywindow.document.write('@page {size: 5x6; margin: 0mm;}');
                        mywindow.document.write('</style>');
                        mywindow.document.write(document.getElementById('view-print').innerHTML);
                        mywindow.document.write('</body></html>');
                        mywindow.document.close(); // necessary for IE >= 10
                        mywindow.focus(); // necessary for IE >= 10*/
                        mywindow.print();
                        mywindow.close();
                    }} labelPosition='right' content='Print' />

                    <Button id='btnCloseLeasePreview' size='small' type='button' onClick={() => { this.props.onClose() }}>ปิด</Button>
                </Modal.Actions>
            </Modal>
        </div>)
    }
}

export default ProductDetailPrintPreview;