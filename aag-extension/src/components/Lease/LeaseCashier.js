/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import {
  Form,
  Input,  
  Message,
  Dropdown,
  Button,
  Segment, 
  Dimmer,
  Label, Modal, Grid, Loader
} from 'semantic-ui-react';
import 'react-dates/lib/css/_datepicker.css';
import Utility from '../../Utility';
import Settings from '../../Settings';


class BillAction extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      start_date: moment(),
      check_date: moment(),
      card_start: moment(),
      description: '',
      cash: '',
      change: '',
      date: '',
      card_code: '',
      card_fee: '',
      card_bank_cards_id: '',
      card_service: '',
      check_code: '',
      check_total: 0,
      check_banks_id: '',
      card_total: 0,
      disabled_card: true
    }

    this.submitBill = this.submitBill.bind(this);
    this.handlerSelectSale = this.handlerSelectSale.bind(this);
    this.handlerCash = this.handlerCash.bind(this);
    this.handlerSaveBill = this.handlerSaveBill.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.handlerResetBill = this.handlerResetBill.bind(this)
    this.handlerCardType = this.handlerCardType.bind(this);
    this.handlerCashTotal = this.handlerCashTotal.bind(this);
    this.handlerFee = this.handlerFee.bind(this)
    this.handlerEditChange = this.handlerEditChange.bind(this)
    this.onSubmitCashTotal = this.onSubmitCashTotal.bind(this);
  }

  componentDidMount() {
    let banks = Utility.getFetch(Settings.baseUrl + '/banks/?is_enabled=1');
    let bank_cards = Utility.getFetch(Settings.baseUrl + '/bank_cards/?is_enabled=1');
    Promise.all([banks, bank_cards]).then((values) => {
      this.banks = values[0];
      let banks = [];
      for (let i in this.banks) {
        banks.push({
          key: this.banks[i].id,
          value: this.banks[i].id,
          text: this.banks[i].name,
        })
      }

      this.bank_cards = values[1];
      let bank_cards = [];
      for (let i in this.bank_cards) {
        bank_cards.push({
          key: this.bank_cards[i].id,
          value: this.bank_cards[i].id,
          text: this.bank_cards[i].kind + " " + this.bank_cards[i].bank.name,
        })
      }


      this.setState({
        banks: banks,
        bank_cards: bank_cards
      })
    });
  }

  handlerResetBill(e) {
    this.props.onResetBill(e)
  }

  closeModal() {
    this.props.onCloseModal();
  }
  handlerCash(e, d) {

    let change = this.state.cash_total - parseFloat(d.value)
    this.setState({
      cash: d.value,
      change: (change * -1).toFixed(2)
    })
  }

  onSubmitCashTotal(e, d) {

    e.preventDefault();

    let cash_total = parseFloat(Utility.removeCommas(this.props.net_price));
    let diff = parseFloat(this.state.cash_total) - cash_total;
    if (diff == 0) {
      this.refs.cash.focus();
      this.setState({
        cash: cash_total
      })
    }
    else {
      this.refs.card_code.focus();
    }


  }

  submitBill(e) {

    e.preventDefault();

    let payment = 'CS';
    let cash = this.state.cash == '' ? 0 : parseFloat(this.state.cash);
    let card_total = this.state.card_total == '' ? 0 : parseFloat(this.state.card_pay);
    let check_total = this.state.check_total == '' ? 0 : parseFloat(this.state.check_total);

    let sum = check_total + card_total + cash;
    let net_price = parseFloat(Utility.removeCommas(this.props.net_price));

    if (card_total > 0)
      payment = 'CD'
    if (check_total > 0)
      payment = 'CH'

    if (net_price - sum > 0) {
      this.refs.cash.focus();
      alert('กรุณาใส่จำนวนเงินสดรับ');
      return;
    }

    this.props.onSubmitBill({
      cash: cash,
      change: this.state.change,
      date: this.state.start_date,
      description: this.state.description,
      bank: this.state.check_banks_id,
      card_code: this.state.card_code,
      card_fee: this.state.card_fee,
      bankcard: this.state.card_bank_cards_id,
      card_service: this.state.card_service,
      card_period: this.state.card_period ? this.state.card_period : 0,
      card_start: this.state.card_start,
      card_contract_number: this.state.card_contract_number,
      check_code: this.state.check_code,
      check_total: this.state.check_total,
      card_total: this.state.card_total,
      check_date: Utility.formatDate2(this.state.check_date),
      total: this.props.net_price,
      payment: payment
    });
    this.setState({
      cash: '',
      change: '',
      description: '',
      date: '',
      card_code: '',
      card_fee: '',
      card_bank_cards_id: '',
      card_service: '',
      check_code: '',
      check_total: '',
      check_banks_id: '',
      card_total: 0,
      card_pay: '',
      card_period: ''
    })
  }

  handlerSelectSale(e, d) {

    this.props.onSelectSale(d.value);
  }

  handlerSaveBill(e) {
    e.preventDefault();
    this.props.onSaveBill(e);
  }

  sumCash() {

    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      let cash_total = parseFloat(Utility.removeCommas(this.props.net_price));
      if (this.state.card_fee != '') {
        cash_total += (parseFloat(this.state.card_fee));
      }
      if (this.state.card_service != '') {
        cash_total += (parseFloat(this.state.card_service));
      }

      if (this.state.card_total != '') {
        cash_total += (-parseFloat(this.state.card_total));
      }

      if (this.state.check_total != '') {
        cash_total += (-parseFloat(this.state.check_total));
      }

      this.setState({
        cash_total: cash_total
      });
    }, 1000);
  }

  onPriceChange() {
    if (this.net_price != this.props.net_price) {
      this.sumCash();
    }

    this.net_price = this.props.net_price
  }

  handlerCardType(e, v) {
    let fee = Utility.getObject(this.bank_cards, v.value).fee;
    let net_price = parseFloat(Utility.removeCommas(this.props.net_price));
    let card_pay = net_price - parseFloat(this.state.cash_total);
    let card_service = Math.round(card_pay * (fee / 100))
    let card_total = card_pay + card_service;
    this.setState({
      card_fee: fee,
      card_bank_cards_id: v.value,
      card_service: card_service.toFixed(2),
      card_total: card_total,
      card_pay: card_pay
    });
  }

  handlerCashTotal(e, v) {
    let net_price = parseFloat(Utility.removeCommas(this.props.net_price));
    let cash_total = v.value;
    let card_pay = net_price - parseFloat(cash_total);
    let fee = this.state.card_fee;

    this.setState({
      disabled_card: card_pay <= 0
    })

    this.setState({
      cash_total: cash_total
    });
    if (fee != '') {
      let card_service = card_pay * (fee / 100)
      let card_total = card_pay + card_service;
      this.setState({
        card_fee: fee,
        card_service: card_service,
        card_total: card_total,
        card_pay: card_pay,
        cash_total: cash_total
      });
    }

  }

  handlerFee(e, v) {
    let cash_total = this.state.cash_total;
    let fee = v.value;
    let net_price = parseFloat(Utility.removeCommas(this.props.net_price));
    let card_pay = net_price - parseFloat(cash_total);
    let card_service = card_pay * (fee / 100)
    let card_total = card_pay + card_service;
    this.setState({
      card_fee: fee,
      card_service: card_service,
      card_total: card_total,
      card_pay: card_pay,
      cash_total: cash_total
    });
  }

  handlerEditChange(e) {
    e.preventDefault();

    this.setState({
      exc_modal: true
    })
  }

  render() {
    this.onPriceChange();
    return (
      <div>
        <Modal id='ModalLeaseCashier' open={true} dimmer={false} /*dimmer='blurring'*/ >
          <Modal.Header>ชำระเงิน</Modal.Header>
          <Modal.Content className='scrolling'>
            <Modal.Description>
              <Segment>
                <Dimmer active={this.props.loader} inverted>
                  <Loader inverted content='Loading' />
                </Dimmer>
                <Grid>
                  <Grid.Row columns={2} >
                    <Grid.Column>
                      <Form size='small'>
                        <Form.Field >
                          <label>ราคาขาย</label>
                          <Label as='div' basic color='green' size='huge'><right>{this.props.net_price}</right></Label>
                        </Form.Field>
                      </Form >
                      <Message
                        attached
                        header={
                          <Form.Checkbox label='ชำระด้วยเช็ค' width={3} checked={this.state.is_check} onChange={(e, v) => {
                            this.setState({
                              is_check: v.checked
                            });
                          }} />
                        }
                      />
                      <Form className='attached fluid segment' size='small'>
                        <Form.Field >
                          <label>เลขที่เช็ค</label>
                          <Input disabled={!this.state.is_check} value={this.state.check_code} onChange={(e, v) => {
                            this.setState({
                              check_code: v.value
                            });
                          }} />
                        </Form.Field>
                        <Form.Field >
                          <label>ธนาคาร</label>
                          <Dropdown disabled={!this.state.is_check} search selection width={14} options={this.state.banks}
                            onChange={(e, v) => {
                              this.setState({
                                check_banks_id: v.value
                              });
                              this.sumCash();
                            }} defaultValue={this.state.check_banks_id} value={this.state.check_banks_id} />
                        </Form.Field>
                        <Form.Field >
                          <label>ยอดจ่าย</label>
                          <Input disabled={!this.state.is_check} value={this.state.check_total} onChange={(e, v) => {
                            this.setState({
                              check_total: v.value
                            });
                            this.sumCash();
                          }} />
                        </Form.Field>

                        <Form.Field >
                          <label>เช็คลงวันที่</label>
                          <DatePicker disabled={!this.state.is_check}
                            dateFormat="DD/MM/YYYY"
                            selected={this.state.check_date}
                            onChange={(date) => {
                              this.setState({ check_date: date });
                            }}
                          />
                        </Form.Field>
                      </Form>
                      <Message
                        attached
                        header='บันทึกรายละเอียดการผ่อนด้วยบัตรเครดิต'
                      />
                      <Form className='attached fluid segment' size='small'>
                        <Form.Field >
                          <label>จำนวนงวด</label>
                          <Input disabled={this.state.disabled_card} value={this.state.card_period} onChange={(e, v) => {
                            this.setState({
                              card_period: v.value
                            });
                          }} />
                        </Form.Field>
                        <Form.Field >
                          <label>วันที่เริ่มผ่อน</label>
                          <DatePicker disabled={this.state.disabled_card}
                            dateFormat="DD/MM/YYYY"
                            selected={this.state.card_start}
                            onChange={(date) => {
                              this.setState({ card_start: date });
                            }}
                          />
                        </Form.Field>
                      </Form>
                    </Grid.Column>
                    <Grid.Column>
                      <Form className='attached fluid segment' size='small' onSubmit={this.onSubmitCashTotal}>
                        <Form.Field >
                          <label>ยอดจ่ายเงินสด</label>
                          <Input className="text-right" value={this.state.cash_total} onChange={this.handlerCashTotal} />
                        </Form.Field>
                      </Form>
                      <Form className='attached fluid segment' size='small'>
                        <Form.Field >
                          <label>รหัสบัตร</label>
                          <Input ref="card_code" name="card_code" value={this.state.card_code} onChange={(e, v) => {
                            this.setState({
                              card_code: v.value
                            });
                          }} disabled={this.state.disabled_card} />
                        </Form.Field>
                        <Form.Field >
                          <label>ชนิดบัตร</label>
                          <Dropdown disabled={this.state.disabled_card} search selection width={14} options={this.state.bank_cards}
                            onChange={this.handlerCardType} defaultValue={this.state.card_bank_cards_id} value={this.state.card_bank_cards_id} />
                        </Form.Field>
                        <Form.Field >
                          <label>%ค่าธรรมเนียม</label>
                          <Input disabled={this.state.disabled_card} value={this.state.card_fee} className='text-right' onChange={this.handlerFee} />
                        </Form.Field>
                        <Form.Field >
                          <label>เลขที่สัญญา</label>
                          <Input disabled={this.state.disabled_card} value={this.state.card_contract_number} onChange={(e, v) => {
                            this.setState({
                              card_contract_number: v.value
                            });
                          }} />
                        </Form.Field>
                        <Form.Field >
                          <label>จ่ายเครดิต</label>
                          <Input disabled={this.state.disabled_card} className='text-right' value={this.state.card_pay} onChange={(e, v) => {
                            this.setState({
                              card_pay: v.value
                            });
                          }} />
                        </Form.Field>
                        <Form.Field >
                          <label>ค่าบริการ</label>
                          <Input disabled={this.state.disabled_card} className='text-right' value={this.state.card_service} onChange={(e, v) => {
                            this.setState({
                              card_service: v.value
                            });
                          }} />
                        </Form.Field>
                        <Form.Field >
                          <label>ยอดจ่ายรวม</label>
                          <Input disabled={this.state.disabled_card} className='text-right' value={this.state.card_total} onChange={(e, v) => {
                            this.setState({
                              card_total: v.value
                            });
                          }} />
                        </Form.Field>
                      </Form>
                      <Form className='attached fluid segment' size='small'>
                        <Form.Field >
                          <label>วันที่ชำระเงิน</label>
                          <DatePicker
                            dateFormat="DD/MM/YYYY"
                            selected={this.state.start_date}
                            onChange={(date) => {
                              this.setState({ start_date: date });
                            }}
                          />
                        </Form.Field>
                        <Form.Field >
                          <label>จำนวนเงินรับ</label>
                          <Input value={this.state.cash} onChange={this.handlerCash} ref="cash" name="cash" className='text-right' />
                        </Form.Field>
                        <Form.Field >
                          <label>จำนวนเงินทอน</label>
                          <Input value={this.state.change} className='text-right' />
                        </Form.Field>
                      </Form>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </Segment>
            </Modal.Description>
          </Modal.Content>
          <Modal.Actions>
            <Button size='small' primary onClick={this.submitBill}
              className={this.state.button_class ? 'loading' : ''}>บันทึก</Button>
            <Button size='small' onClick={this.closeModal}>ยกเลิก</Button>
          </Modal.Actions>
        </Modal>
      </div>
    );
  }
}
export default BillAction;
