/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import Settings from '../../Settings';
import Utility from '../../Utility';
import {
  Table,
  Column,
  Cell
} from 'fixed-data-table';
import {
  Form,
  Button,
  Dimmer,
  Loader
} from 'semantic-ui-react';
import { connect } from 'react-redux'

class ItemsCell extends Component {
  constructor (props) {
    super(props);
  }
  render() {
    const {rowIndex, field, data, ...props} = this.props;
    return (
      <Cell {...props} >
      <div className={this.props.textAlign}>{data[rowIndex][field]}</div>
      </Cell>
    );
  }
}

class LeaseStock extends Component {

  constructor (props) {
    super(props);
    this.state = {
      items: [],
      branch: props.auth.branch.id
    }
    this.submitSearch = this.submitSearch.bind(this)
    this.resetForm = this.resetForm.bind(this)
  }

  resetForm(){
    this.setState({
      branch: ''
    })
  }

  componentDidMount(){

    this.branches = this.props.branches
    let branches = []
    for (let i in this.branches) {
      branches.push({
        value: this.branches[i].id,
        key: this.branches[i].id,
        text: this.branches[i].name
      });
    }

    this.setState({branches})
  }

  loadData () {
    var self = this;
    this.setState({
      loader_active: true
    });

    var s = '';
    if(this.state.branch!=null && this.state.branch!=''){
      s += (s==''?'':'&')+'branch='+this.state.branch
    }

    var stock_category =  Utility.getFetch(Settings.baseUrl+'/lease/0/product/?lease_status=1,2&'+s);
   
     Promise.all([stock_category]).then((values)=>{
       this.stock_category = values[0];



       this.setState({
         loader_active: false,
         items: this.setFieldValue(values[0]),
       });
     });
     let table_width = document.getElementById('table_width')
     this.setState({ table_width: table_width.clientWidth });
  }

  submitSearch(event) {
    event.preventDefault();
    this.loadData ();
  }

  getTypeSale(key){
    let type = Settings.type_sale.filter((o)=>{return o.value==key});
    return type[0];
  }

  setFieldValue(items){
    let _items = []
    for(let i =0;i<items.length;i++){
      if(items[i].lease.status>2)
        continue
      let item = items[i];
      item.number = item.lease.number
      item.branch_name = item.lease.branch.name
      item.customer_name = item.lease.customer.name
      item.interest = item.lease.interest
      item.total = Utility.priceFormat(parseFloat(item.lease.total_interest))
      item.end_date = Utility.formatDate(item.lease.end_date)

      item.status_title = Settings.status_lease[item.lease.status]

      _items.push(item)
    }

    return _items;
  }

  render () {
    const items = this.state.items;
    return (
      <div>
      <Dimmer className={this.state.loader_active?'active':''} inverted>
       <Loader content='Loading' inverted />
       </Dimmer>

       <Form className='fluid' size='small' onSubmit={this.submitSearch}>
         <Form.Group>
           <Form.Dropdown label='สาขา' search selection width={4} options={this.state.branches} value={this.state.branch} onChange={(e,data)=>{
             this.setState({branch: data.value});
           }} />
           <Form.Field width={5}>
            <br />
           <Button >ค้นหา</Button>
           <Button onClick={this.resetForm} >ทั้งหมด</Button>
           </Form.Field>
         </Form.Group>
       </Form>
           <div id="table_width">
       <Table
         rowsCount={items.length}
         rowHeight={35}
         headerHeight={35}
         width={this.state.table_width}
         height={500}>
         <Column

           header={<Cell>เลขที่</Cell>}
           cell = {
             <ItemsCell data={items} field="number"   />
           }
           width={150}
         />
         <Column

           header={<Cell>สาขา</Cell>}
           cell = {
             <ItemsCell data={items} field="branch_name"   />
           }
           width={150}
         />
         <Column
           header={<Cell>ชื่อลูกค้า</Cell>}
           cell = {
             <ItemsCell data={items} field="customer_name"  />
           }
           width={150}
         />
         <Column
           header={<Cell>ชื่อสินค้า</Cell>}
           cell = {
             <ItemsCell data={items} field="name"  />
           }
           width={150}
         />
         <Column
           header={<Cell className="text-right">จำนวน</Cell>}
           cell = {
             <ItemsCell data={items} field="amount" textAlign='text-right' />
           }
           width={150}
         />

         <Column
           header={<Cell className="text-right">อัตราดอกเบี้ย</Cell>}
           cell = {
             <ItemsCell data={items} field="interest"  textAlign='text-right' />
           }
           width={160}
         />
         <Column
           header={<Cell className="text-right">น้ำหนัก</Cell>}
           cell = {
             <ItemsCell data={items} field="weight"  textAlign='text-right' />
           }
           width={160}
         />
         <Column
           header={<Cell className="text-right">รวมดอกเบี้ยรับ</Cell>}
           cell = {
             <ItemsCell data={items} field="total"  textAlign='text-right' />
           }
           width={160}
         />
         <Column
           header={<Cell>ครบกำหนด</Cell>}
           cell = {
             <ItemsCell data={items} field="end_date"  />
           }
           width={160}
         />
         <Column
           header={<Cell>สถานะ</Cell>}
           cell = {
             <ItemsCell data={items} field="status_title"  />
           }
           width={160}
         />
         </Table>
         </div>
      </div>
    );
  }
}

const mapStateToProps = state =>{
  return ({
    auth: state.auth,
    branches: state.branches
  })
}

export default connect(
  mapStateToProps,
)(LeaseStock)
