/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import {
    Table,
    Column,
    Cell
} from 'fixed-data-table';
import {
    Form,
    Input,
    Grid,
    Button,
    Dropdown,
    Icon,
    Header,
    Dimmer,
    Loader
} from 'semantic-ui-react';
import moment from 'moment';
import { connect } from 'react-redux'
import Utility from '../../Utility';
import Settings from '../../Settings';


class ItemsCell extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { rowIndex, field, data, ...props } = this.props;
        return (
            <Cell {...props}>
                <div className={(this.props.textAlign == null ? '' : this.props.textAlign) + ' text-cell'}>{data[rowIndex][field]}</div>
            </Cell>
        );
    }
}

class ItemsCellIcon extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { rowIndex, field, data, ...props } = this.props;
        return (
            <Cell {...props} >
                <Button color='grey' size='mini' circular icon={this.props.icon} onClick={(e, v) => {
                    this.props.onClickItem(e, rowIndex)
                }}></Button>
            </Cell>
        );
    }
}

class Search extends Component {

    constructor(props) {
        super(props)
        this.state = {
            loader: false
        }
        this.handlerSerach = this.handlerSerach.bind(this)
        this.handlerInput = this.handlerInput.bind(this)
        this.handlerOverdue = this.handlerOverdue.bind(this)
    }

    handlerInput(e, v) {

        this.setState({
            [v.name]: v.value
        })
    }

    handlerOverdue(e, v) {

        let due_date = null
        let overdue = parseInt(v.value)
        if(overdue<0)
            overdue=overdue*-1
        if (v.value != '') {
            
            let start = moment().add(-overdue, 'day')
            due_date = Utility.formatDate2(start)
        }
        this.setState({
            [v.name]: overdue,
            due_date: due_date
        })
    }

    handlerSerach(e) {
        this.props.onSubmit(e, Utility.jsonToQueryString(this.state))
    }

    render() {
        return (<Form size='tiny' onSubmit={this.handlerSerach}>
            <table>
                <tr>
                    <td><label>ระยะเวลาเกินกำหนด</label></td>
                    <td><Input name='overdue'
                        min={0}
                        className='text-right'
                        type='number'
                        value={this.state.overdue}
                        onChange={this.handlerOverdue} /> วัน </td>
                </tr>
                
                <tr>
                    <td><label>จำนวนเงิน</label></td>
                    <td>
                        <Input name='start_price'
                            className='text-right'
                            value={this.state.start_price}
                            onChange={this.handlerInput} />
                        -
                            <Input
                            className='text-right' name='end_price'
                            value={this.state.end_price}
                            onChange={this.handlerInput} />
                    </td>
                </tr>
                <tr>
                    <td><label>เลขที่ขายฝาก</label></td>
                    <td><Input name='number'
                        value={this.state.number}
                        onChange={this.handlerInput} /></td>
                </tr>
            </table>
            <Button icon><Icon name='search' /> ค้นหา</Button>
        </Form>)
    }
}


class LeaseOut extends Component {

    constructor(props) {
        super(props);

        this.state = {
            items: [],
            items2: [],
            net_items1: [],
            net_items2: [],
            loader: false
        }

        this.handlerSerach = this.handlerSerach.bind(this)
        this.handlerAddItem = this.handlerAddItem.bind(this)
        this.handlerDeleteItem = this.handlerDeleteItem.bind(this)
        this.handlerAddAll = this.handlerAddAll.bind(this)
        this.handlerDeleteAll = this.handlerDeleteAll.bind(this)
        this.handlerCalucate = this.handlerCalucate.bind(this)
        this.handlerOut = this.handlerOut.bind(this)
    }

    handlerCalucate(e) {
        e.preventDefault()

        let items = this.state.items;
        let lease_in = '';
        let j = 0;
        let limit = items.length
        for (let i in items) {
            lease_in += items[i].id
            if (j < limit - 1)
                lease_in += ","
            j++
        }

        this.calculet1(lease_in)
        let items2 = this.state.items2;
        lease_in = '';
        j = 0;
        limit = items2.length
        for (let i in items2) {
            lease_in += items2[i].id
            if (j < limit - 1)
                lease_in += ","
            j++
        }
        this.calculet2(lease_in)
    }

    componentDidMount() {

        var branches = Utility.getFetch(Settings.baseUrl + '/branches/?is_enabled=1');
        Promise.all([branches]).then((values) => {
            this.branches = values[0];
            this.products = values[1];
            let branches = [];
            for (let i in values[0]) {
                branches.push({
                    key: values[0][i].id,
                    value: values[0][i].id,
                    text: values[0][i].name,
                });
            }

            this.setState({
                branches: branches,
                loader_active: false

            });
        });
        let width_1 = document.getElementById('width_1')
        let width_2 = document.getElementById('width_2')
        this.setState({ table_width: width_1.clientWidth, table_width2: width_2.clientWidth });
    }

    handlerAddItem(e, position) {

        let items = this.state.items;
        let items2 = this.state.items2

        let item = items[position]

        for (let i in items2) {
            if (item.id == items2[i].id) {
                alert('เลือกรายการซ้ำกรุณาตรวจสอบใหม่อีกครั้ง')
                return;
            }
        }

        items2.push(JSON.parse(JSON.stringify(item)))
        items.splice(position, 1)

        this.setState({
            items2: items2,
            items: items
        })

    }
    handlerDeleteItem(e, position) {
        let items = this.state.items;
        let items2 = this.state.items2
        items.push(JSON.parse(JSON.stringify(items2[position])))
        items2.splice(position, 1)

        this.setState({
            items2: items2,
            items: items
        })
    }

    handlerAddAll(e) {

        let items = this.state.items;
        this.setState({
            items2: items,
            items: []
        })
    }

    handlerDeleteAll(e) {
        let items2 = this.state.items2;
        this.setState({
            items: items2,
            items2: []
        })
    }

    handlerSerach(e, q) {
        e.preventDefault()

        this.setState({loader: true})
        const {branch} = this.props.auth
        let url = Settings.baseUrl + '/lease/?is_enabled=1&status_in=1,2&branch='+branch.id+"&" + q
        Utility.get(url, (s, d) => {
            if (s) {

                let items = []
                for (let i in d) {
                    if (d[i].status != '4' && d[i].status != '3')
                        items.push(d[i])
                }
                this.setState({
                    items: this.setFieldValue(items),
                    loader: false
                })

            }
        })
    }

    handlerOut(e) {
        e.preventDefault()

        let items2 = this.state.items2
        this.is_stock = false
        if (items2.length > 0) {
            if (!window.confirm('ยืนยันคัดออก'))
                return;
            this.is_stock = window.confirm('ต้องการบันทึกประเภทและน้ำหนัก ไปยังสต็อกทองเก่าหรือไม่?')
            this.setState({
                loader: true
            })
            this.runSubmit();
        } else {
            alert('กรุณาเลือกรายการขายฝากเพื่อคัดออก')
        }
    }

    runSubmit() {
        let posiotn = 0;
        let items2 = this.state.items2
        let next = () => {
            if (posiotn == items2.length - 1) {
                this.setState({
                    items2: [],
                    loader: false
                })
                alert('คัดออกสำเร็จ')
            } else {
                posiotn++;
                submit();
            }
        }
        let submit = () => {
            if (this.is_stock) {
                const url = Settings.baseUrl + "/lease/" + items2[posiotn].id + "/stock/?action=in"
                Utility.getAuth(url, () => {
                    next();
                })
            } else {
                next();
            }
        }

        submit();
    }

    setValue2(d) {

        let items = [];
        let object = {};
        let weight = 0
        let total = 0;

        let lease = {};

        for (let i in d) {
            if (lease['id_' + d[i].lease.id] == null)
                lease['id_' + d[i].lease.id] = {
                    weight: 0
                }
            lease['id_' + d[i].lease.id].weight += parseFloat(d[i].weight)
        }

        for (let i in d) {

            if (object['id_' + d[i].category.id] == null)
                object['id_' + d[i].category.id] = {
                    weight: 0,
                    average: 0
                }
            weight += parseFloat(d[i].weight)
            object['id_' + d[i].category.id].weight += parseFloat(d[i].weight)

            let ww = parseFloat(lease['id_' + d[i].lease.id].weight) / parseFloat(d[i].category.weight)
            let bb = (d[i].lease.amount / ww)

            object['id_' + d[i].category.id].average += (d[i].lease.amount) / (parseFloat(d[i].weight) / parseFloat(d[i].category.weight))
            object['id_' + d[i].category.id].category_name = d[i].category.name
        }

        for (let i in object) {
            object[i].weight = Utility.weightFormat(object[i].weight)
            object[i].average = Utility.priceFormat(object[i].average)
            items.push(object[i])
        }
        return items
    }

    setFieldValue(item) {

        for (let i = 0; i < item.length; i++) {
            item[i].branch_name = item[i].branch.name
            item[i].customer_name = item[i].customer.name
            item[i].amount_display = Utility.priceFormat(item[i].amount)
            item[i].status_title = Settings.status_lease[item[i].status]
            let amount = parseInt(item[i].amount)
            let interest = parseInt(item[i].interest)
            item[i].interest_m = Utility.priceFormat(amount * (interest / 100))
            item[i].enabled_title = item[i].is_enabled == 0 ? 'ยกเลิกบิลแล้ว' : ''
            item[i].start_date_display = Utility.formatDate(item[i].start_date)
            item[i].end_date_display = Utility.formatDate(item[i].end_date)
            item[i].close_date_display = item[i].status == 3 ? Utility.formatDate(item[i].close_date) : ''
            //this.branches
        }
        return item;
    }

    calculet1(lease_in) {
        if (lease_in == '') {
            this.setState({
                net_items1: [],
                weight1: 0,
                n1: 0
            })
            return;
        }

        var product = Utility.getFetch(Settings.baseUrl + '/lease/0/product/?lease_in=' + lease_in);
        var principle = Utility.getFetch(Settings.baseUrl + '/lease/0/principle/?lease_in=' + lease_in);

        this.setState({
            loader: true
        })
        Promise.all([product, principle]).then((values) => {

            let weight = 0;
            for (let i in values[0]) {
                weight += parseFloat(values[0][i].weight);
            }

            let IC = 0;
            let DC = 0;
            for (let i in values[1]) {
                if (values[1][i].kind == 'IC')
                    IC += parseFloat(values[1][i].total);
                if (values[1][i].kind == 'DC')
                    DC += parseFloat(values[1][i].total);
            }
            this.setState({
                net_items1: this.setValue2(values[0]),
                weight1: Utility.weightFormat(weight),
                n1: values[0].length,
                loader: false
            })

        })
    }

    calculet2(lease_in) {

        if (lease_in == '') {
            this.setState({
                net_items2: [],
                weight2: 0,
                n2: 0
            })
            return;
        }

        var product = Utility.getFetch(Settings.baseUrl + '/lease/0/product/?lease_in=' + lease_in);
        var principle = Utility.getFetch(Settings.baseUrl + '/lease/0/principle/?lease_in=' + lease_in);
        this.setState({
            loader: true
        })
        Promise.all([product, principle]).then((values) => {

            let weight = 0;
            for (let i in values[0]) {
                weight += parseFloat(values[0][i].weight);
            }

            let IC = 0;
            let DC = 0;
            for (let i in values[1]) {
                if (values[1][i].kind == 'IC')
                    IC += parseFloat(values[1][i].total);
                if (values[1][i].kind == 'DC')
                    DC += parseFloat(values[1][i].total);
            }
            this.setState({
                net_items2: this.setValue2(values[0]),
                weight2: Utility.weightFormat(weight),
                n2: values[0].length,
                loader: false
            })

        })
    }

    render() {
        const {loader} = this.state
        const items = this.state.items
        const items2 = this.state.items2
        const net_items1 = this.state.net_items1
        const net_items2 = this.state.net_items2
        return (
            <div>
                <Dimmer active={loader} inverted>
                    <Loader inverted>Loading</Loader>
                </Dimmer>
                <Form>
                    <Form.Group>
                        <Form.Field width={6}>
                            <Header floated='left' as='h2'>คัดออก</Header>
                        </Form.Field>

                    </Form.Group>
                </Form>
                <Grid divided='vertically'>
                    <Grid.Row >
                        <Grid.Column width={8}>
                            <Search
                                branches={this.state.branches}
                                onSubmit={this.handlerSerach} />
                            <br />
                            <div id='width_1'>
                                <Table
                                    allowCellsRecycling={true}
                                    rowsCount={items.length}
                                    rowHeight={50}
                                    headerHeight={35}
                                    width={this.state.table_width}
                                    height={200}>
                                    <Column
                                        fixed
                                        header={<Cell></Cell>}
                                        cell={
                                            <ItemsCellIcon data={items} onClickItem={this.handlerAddItem} icon='angle right' />
                                        }
                                        width={70}
                                    />
                                    <Column

                                        header={<Cell>เลขที่</Cell>}
                                        cell={
                                            <ItemsCell id='LeaseNum' data={items} field="number" />
                                        }
                                        width={150}
                                    />
                                    <Column
                                        header={<Cell>ซื่อลูกค้า</Cell>}
                                        cell={
                                            <ItemsCell data={items} field="customer_name" />
                                        }
                                        width={150}
                                    />
                                    <Column
                                        header={<Cell className="text-right" >จน. เงินต้น</Cell>}
                                        cell={
                                            <ItemsCell data={items} field="amount_display" textAlign="text-right" />
                                        }
                                        width={150}
                                    />
                                    <Column
                                        header={<Cell className="text-right">รวม ดบ.รับ</Cell>}
                                        cell={
                                            <ItemsCell data={items} field="total" textAlign="text-right" />
                                        }
                                        width={150}
                                    />
                                    <Column
                                        header={<Cell className="text-right">อัตรา ดบ.</Cell>}
                                        cell={
                                            <ItemsCell data={items} field="interest" textAlign="text-right" />
                                        }
                                        width={150}
                                    />
                                    <Column
                                        header={<Cell className="text-right">จน. เดือน</Cell>}
                                        cell={
                                            <ItemsCell data={items} field="month" textAlign="text-right" />
                                        }
                                        width={150}
                                    />

                                    <Column
                                        header={<Cell className="text-right">ดบ./เดือน</Cell>}
                                        cell={
                                            <ItemsCell data={items} field="interest_m" textAlign="text-right" />
                                        }
                                        width={150}
                                    />

                                    <Column
                                        header={<Cell>วันที่นำเข้า</Cell>}
                                        cell={
                                            <ItemsCell data={items} field="start_date_display" />
                                        }
                                        width={150}
                                    />
                                    <Column
                                        header={<Cell>วันครบกำหนด</Cell>}
                                        cell={
                                            <ItemsCell data={items} field="end_date_display" />
                                        }
                                        width={150}
                                    />
                                    <Column
                                        header={<Cell>วันที่ไถ่คืน/วันที่คัดออก</Cell>}
                                        cell={
                                            <ItemsCell data={items} field="close_date_display" />
                                        }
                                        width={150}
                                    />
                                    <Column
                                        header={<Cell>สถานะ</Cell>}
                                        cell={
                                            <ItemsCell data={items} field="status_title" />
                                        }
                                        width={150}
                                    />

                                    <Column
                                        header={<Cell>สถานะยกเลิก</Cell>}
                                        cell={
                                            <ItemsCell data={items} field="enabled_title" />
                                        }
                                        width={150}
                                    />
                                    <Column
                                        header={<Cell>สาขา</Cell>}
                                        cell={
                                            <ItemsCell data={items} field="branch_name" />
                                        }
                                        width={150}
                                    />


                                </Table>

                                <br />
                                <Table
                                    rowsCount={net_items1.length}
                                    rowHeight={30}
                                    headerHeight={35}
                                    width={this.state.table_width}
                                    height={200}>
                                    <Column
                                        header={<Cell>%ทอง</Cell>}
                                        cell={
                                            <ItemsCell data={net_items1} field="category_name" />
                                        }
                                        width={100}
                                    />
                                    <Column
                                        header={<Cell className="text-right">น้ำหนักทอง(กรัม)</Cell>}
                                        cell={
                                            <ItemsCell data={net_items1} field="weight" textAlign="text-right" />
                                        }
                                        width={190}
                                    />
                                    <Column
                                        header={<Cell className="text-right">ราคาทองเฉลี่ยบาทละ</Cell>}
                                        cell={
                                            <ItemsCell data={net_items1} field="average" textAlign="text-right" />
                                        }
                                        width={160}
                                    />
                                </Table>
                                <br />
                                <Form>
                                    <Form.Group widths='equal'>
                                        <Form.Input width={5} label='จำนวนรายการ' value={this.state.n1} className='text-right' />
                                        <Form.Input width={5} label='น้ำหนัก' value={this.state.weight1} className='text-right' />
                                        <Form.Field width={6}>
                                            <br />
                                            <Button type='button' onClick={this.handlerCalucate}>คำนวณ</Button>
                                        </Form.Field>
                                    </Form.Group>
                                </Form>
                            </div>
                        </Grid.Column>
                        <Grid.Column width={1}>
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <Button color='grey' circular icon='angle double right' onClick={this.handlerAddAll}></Button>
                            <br />
                            <br />
                            <Button color='grey' circular icon='angle double left' onClick={this.handlerDeleteAll}></Button>
                        </Grid.Column>
                        <Grid.Column width={7}>
                            <div id='width_2'>
                                <Table
                                    rowsCount={items2.length}
                                    rowHeight={50}
                                    headerHeight={35}
                                    width={this.state.table_width2}
                                    height={400} >
                                    <Column
                                        fixed
                                        header={<Cell></Cell>}
                                        cell={
                                            <ItemsCellIcon data={items} onClickItem={this.handlerDeleteItem} icon='angle left' />
                                        }
                                        width={70}
                                    />
                                    <Column
                                        header={<Cell>เลขที่</Cell>}
                                        cell={
                                            <ItemsCell data={items2} field="number" />
                                        }
                                        width={150}
                                    />
                                    <Column
                                        header={<Cell>ซื่อลูกค้า</Cell>}
                                        cell={
                                            <ItemsCell data={items2} field="customer_name" />
                                        }
                                        width={150}
                                    />
                                    <Column
                                        header={<Cell className="text-right">จน. เงินต้น</Cell>}
                                        cell={
                                            <ItemsCell data={items2} field="amount_display" textAlign="text-right" />
                                        }
                                        width={150}
                                    />
                                    <Column
                                        header={<Cell className="text-right">รวม ดบ.รับ</Cell>}
                                        cell={
                                            <ItemsCell data={items2} field="total" textAlign="text-right" />
                                        }
                                        width={150}
                                    />
                                    <Column
                                        header={<Cell className="text-right">อัตรา ดบ.</Cell>}
                                        cell={
                                            <ItemsCell data={items2} field="interest" textAlign="text-right" />
                                        }
                                        width={150}
                                    />
                                    <Column
                                        header={<Cell className="text-right">จน. เดือน</Cell>}
                                        cell={
                                            <ItemsCell data={items2} field="month" textAlign="text-right" />
                                        }
                                        width={150}
                                    />

                                    <Column
                                        header={<Cell className="text-right">ดบ./เดือน</Cell>}
                                        cell={
                                            <ItemsCell data={items2} field="interest_m" textAlign="text-right" />
                                        }
                                        width={150}
                                    />

                                    <Column
                                        header={<Cell>วันที่นำเข้า</Cell>}
                                        cell={
                                            <ItemsCell data={items2} field="start_date_display" />
                                        }
                                        width={150}
                                    />
                                    <Column
                                        header={<Cell>วันครบกำหนด</Cell>}
                                        cell={
                                            <ItemsCell data={items2} field="end_date_display" />
                                        }
                                        width={150}
                                    />
                                    <Column
                                        header={<Cell>วันที่ไถ่คืน/วันที่คัดออก</Cell>}
                                        cell={
                                            <ItemsCell data={items2} field="close_date_display" />
                                        }
                                        width={150}
                                    />
                                    <Column
                                        header={<Cell>สถานะ</Cell>}
                                        cell={
                                            <ItemsCell data={items2} field="status_title" />
                                        }
                                        width={150}
                                    />

                                    <Column
                                        header={<Cell>สถานะยกเลิก</Cell>}
                                        cell={
                                            <ItemsCell data={items2} field="enabled_title" />
                                        }
                                        width={150}
                                    />
                                    <Column
                                        header={<Cell>สาขา</Cell>}
                                        cell={
                                            <ItemsCell data={items2} field="branch_name" />
                                        }
                                        width={150}
                                    />
                                </Table>
                                <br />
                                <Table
                                    rowsCount={net_items2.length}
                                    rowHeight={30}
                                    headerHeight={35}
                                    width={this.state.table_width2}
                                    height={200}>
                                    <Column
                                        header={<Cell>%ทอง</Cell>}
                                        cell={
                                            <ItemsCell data={net_items2} field="category_name" />
                                        }
                                        width={100}
                                    />
                                    <Column
                                        header={<Cell className="text-right">น้ำหนักทอง(กรัม)</Cell>}
                                        cell={
                                            <ItemsCell data={net_items2} field="weight" textAlign="text-right" />
                                        }
                                        width={190}
                                    />
                                    <Column
                                        header={<Cell className="text-right">ราคาทองเฉลี่ยบาทละ</Cell>}
                                        cell={
                                            <ItemsCell data={net_items2} field="average" textAlign="text-right" />
                                        }
                                        width={160}
                                    />
                                </Table>
                                <br />
                                <Form>
                                    <Form.Group widths='equal'>
                                        <Form.Input width={5} label='จำนวนรายการ' value={this.state.n2} className='text-right' />
                                        <Form.Input width={5} label='น้ำหนัก' value={this.state.weight2} className='text-right' />
                                        <Form.Field width={6}>
                                            <br />
                                            <Button onClick={this.handlerCalucate} >คำนวณ</Button>
                                        </Form.Field>

                                    </Form.Group>
                                </Form>
                            </div>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
                <right>
                    <Button id='btnLeaseOutItem' onClick={this.handlerOut} loader={this.state.loader} primary>คัดออก</Button>
                </right>

            </div>
        )
    }
}

const mapStateToProps = state =>{
    return ({
      auth: state.auth
    })
  }
export default connect(mapStateToProps)(LeaseOut)