/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import {
    Modal, Form, Button, Grid, Icon, Input, Checkbox, Radio
} from 'semantic-ui-react';
import { DropDownBranch, DropDownProduct } from '../Widget/DropDown'
import InputSearchCustomer from '../Customer/InputSearchCustomer'
import DatePicker from "react-datepicker";
import moment from 'moment';
import Utility from '../../Utility';
import MsgInput from '../Error/MsgInput'

class LeaseSearch extends Component {

    constructor(props) {
        super(props);
        this.state = {
            error: {}
        }

        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleInputChange(e, { name, value, q }) {

        if (name == 'radio') {
            let branch = this.state.branch
            this.state = {}
            if (branch)
                this.state = { branch: branch }
            for (let k in q) {
                if (q[k] == undefined) {
                    delete q[k]
                }
            }
            this.setState(q)
        } else if (name == 'overdue') {

            const overdue = Utility.parseInt(value, this.state.overdue)
            this.setState({
                due_date: moment().add(-overdue, 'day')
            })
        }
        this.setState({ [name]: value })
    }

    handleSubmit(e) {
        const { checked, product } = this.state
        this.setState({
            error: {}
        })
        if (checked && product == '') {
            this.setState({
                error: { product: 'กรุณาเลือกชื่อสินค้า' }
            })
        } else {
            delete this.state['error']
            if (this.state.product == '')
                delete this.state['product']
            this.props.onSubmit(this.state)
        }

    }

    render() {

        const { onClose } = this.props
        const { branch, number, customer, phone, citizen_id, in_date_start, in_date_end, overdue, start_price, end_price, product } = this.state

        return (<div>
            <Form className='attached fluid' size='small' onSubmit={this.handleSubmit} >
                <Form.Group>
                    <Form.Field width={3}>
                        <label>เลขที่</label>
                        <Input id='inputLeaseNum' fluid onChange={this.handleInputChange} name="number" value={number} />
                    </Form.Field>
                    <Form.Field width={3}>
                        <label>ชื่อลูกค้า</label>
                        <InputSearchCustomer fluid onChange={this.handleInputChange} name="customer" value={customer} />
                    </Form.Field>

                    <Form.Field width={3}>
                        <label>เบอร์โทร</label>
                        <Input id='inputPhone' fluid onChange={this.handleInputChange} name="phone" value={phone} />
                    </Form.Field>
                    <Form.Field width={3}>
                        <label>เลขประจำตัวประชาชน</label>
                        <Input id='inputID' fluid onChange={this.handleInputChange} name="citizen_id" value={citizen_id} />
                    </Form.Field>
                    <Form.Field width={3}>
                        <label>รายการเกินกำหนด</label>
                        <Input id='inputOverDue' fluid min={0} type='number' onChange={this.handleInputChange} name="overdue" value={overdue} />
                    </Form.Field>

                </Form.Group>
                <Form.Group>
                    <Form.Field width={6}>
                        <label>วันที่นำเข้า</label>
                        <Grid>
                            <Grid.Row columns={2}>
                                <Grid.Column>
                                    <DatePicker 
                                    dateFormat="DD/MM/YYYY"
                                    selected={this.state.in_date_start} 
                                    onChange={(d) => this.setState({ in_date_start: d, in_date_end: in_date_end ? in_date_end : d })} 
                                    name="in_date_start"  />
                                </Grid.Column>
                                <Grid.Column>
                                    <DatePicker  
                                    dateFormat="DD/MM/YYYY" 
                                    onChange={(d) => this.setState({ in_date_end: d })} name="in_date_end" 
                                    selected={this.state.in_date_end} />
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </Form.Field>
                    <Form.Field width={6}>
                        <label>จำนวนเงินตั้งแต่</label>
                        <Grid>
                            <Grid.Row columns={2}>
                                <Grid.Column>
                                    <Input id='inputStartPrice' fluid min={0} max={end_price} type='number' onChange={this.handleInputChange} name="start_price" value={start_price} />
                                </Grid.Column>
                                <Grid.Column>
                                    <Input id='inputEndPrice' fluid min={start_price} type='number' onChange={this.handleInputChange} name="end_price" value={end_price} />
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </Form.Field>
                    <Form.Field width={3}>
                        <label><Checkbox id='checkProduct' onChange={(e, v) => this.setState({ checked: v.checked, product: '' })} />รายการสินค้า <MsgInput text={this.state.error && this.state.error.product} /></label>
                        <DropDownProduct id='selectProduct' disabled={!this.state.checked} fluid size='small' fluid selection onChange={this.handleInputChange} name="product" value={product} />

                    </Form.Field>
                </Form.Group>
                <Grid columns={5} className="pgrid-3">
                    <Grid.Column>
                        <p>
                            <Radio
                                label='รายการวันนี้'
                                name='radio'
                                value='2'
                                q={{ in_date_start: moment(), in_date_end: moment() }}
                                checked={this.state.radio == '2'}
                                onChange={this.handleInputChange}
                            />
                        </p>
                        <p><Radio
                            label='รายการเดือนนี้'
                            name='radio'
                            value='3'
                            q={{ in_date_start: moment().startOf("month"), in_date_end: moment().endOf("month") }}
                            checked={this.state.radio == '3'}
                            onChange={this.handleInputChange}
                        /></p>
                        <p><Radio
                            label='รายการทั้งหมด'
                            name='radio'
                            value='4'
                            q={{ in_date_start: '', in_date_end: '', status_in: undefined }}
                            checked={this.state.radio == '4'}
                            onChange={this.handleInputChange}
                        />  </p>

                        
                    </Grid.Column>
                    <Grid.Column>
                        <p><Radio
                            label='รายการไถ่คืนวันนี้'
                            name='radio'
                            value='12'
                            q={{ start_close_date: moment(), end_close_date: moment(), status_in: '3' }}
                            checked={this.state.radio == '12'}
                            onChange={this.handleInputChange}
                        /></p>
                        <p><Radio
                            label='รายการไถ่คืนเดือนนี้'
                            name='radio'
                            value='8'
                            q={{ start_close_date: moment().startOf("month"), end_close_date: moment().endOf("month"), status_in: '3' }}
                            checked={this.state.radio == '8'}
                            onChange={this.handleInputChange}
                        /></p>
                        <p><Radio
                            label='รายการไถ่คืนทั้งหมด'
                            name='radio'
                            value='9'
                            q={{ status_in: '3' }}
                            checked={this.state.radio == '9'}
                            onChange={this.handleInputChange}
                        /></p>
                        <p>
                            <Radio
                                label='รายการที่ยังไม่ไถ่คืน'
                                name='radio'
                                value='1'
                                q={{ status_in: '1,2' }}
                                checked={this.state.radio == '1'}
                                onChange={this.handleInputChange}
                            />
                        </p>
                    </Grid.Column>
                    <Grid.Column>
                        <p>
                            <Radio
                                label='รายการคัดออกวันนี้'
                                name='radio'
                                value='6'
                                q={{ start_out_date: moment(), end_out_date: moment(),status_in: '4'  }}
                                checked={this.state.radio == '6'}
                                onChange={this.handleInputChange}
                            />
                        </p>
                        <p>
                            <Radio
                                label='รายการคัดออกเดือนนี้'
                                name='radio'
                                value='13'
                                q={{ start_out_date: moment().startOf("month"), end_out_date: moment().endOf("month") ,status_in: '4' }}
                                checked={this.state.radio == '13'}
                                onChange={this.handleInputChange}
                            />
                        </p>
                        <p>
                            <Radio
                                label='รายการคัดออกทั้งหมด'
                                name='radio'
                                q={{ status_in: '4' }}
                                value='10'
                                checked={this.state.radio == '10'}
                                onChange={this.handleInputChange}
                            />
                        </p>
                    </Grid.Column>
                    <Grid.Column>
                        <p>
                            <Radio
                                label='รายการยกเลิกบิล'
                                name='radio'
                                value='7'
                                q={{ is_enabled: 0 }}
                                checked={this.state.radio == '7'}
                                onChange={this.handleInputChange}
                            />
                        </p>
                        <p>
                            <Radio
                                label='รายการครบกำหนดวันนี้'
                                name='radio'
                                value='5'
                                q={{ start_end_date: moment(), end_end_date: moment() }}
                                checked={this.state.radio == '5'}
                                onChange={this.handleInputChange}
                            />
                        </p>
                        <p>
                            <Radio
                                label='รายการเกินกำหนดทั้งหมด'
                                name='radio'
                                value='11'
                                q={{ due_date: moment(), status_in: '1,2' }}
                                checked={this.state.radio == '11'}
                                onChange={this.handleInputChange}
                            />
                        </p>
                        
                        
                    </Grid.Column>
                    
                    <Grid.Column>
                        <Button id='btnSearch' floated='right' type='submit'>ค้นหา</Button>
                    </Grid.Column>
                </Grid>
            </Form><br />
        </div>);
    }
}

export default LeaseSearch
