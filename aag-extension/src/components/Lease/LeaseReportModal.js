/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import {
    Table,
    Column,
    Cell
} from 'fixed-data-table';
import {
    Segment,
    Form,
    Modal,
    Button,
    Grid,
    Label,
    Input
} from 'semantic-ui-react';
import Settings from '../../Settings';
import Utility from '../../Utility';

class ItemsCell extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { rowIndex, field, data, ...props } = this.props;
        return (
            <Cell {...props} onClick={(e) => { }}>
                <div className={(this.props.textAlign == null ? '' : this.props.textAlign) + ' text-cell'}>
                    {data[rowIndex][field]}
                </div>
            </Cell>
        );
    }
}

class CalculatTotal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            items: []
        }
        this.open = this.open.bind(this)
    }

    componentDidMount() {

        let elHeight = document.getElementById('total-box2')
        if (elHeight)
            this.setState({ table_width: window.innerWidth });
    }

    open() {

        var product = Utility.getFetch(Settings.baseUrl + '/lease/0/product/?lease_in=' + this.props.lease_in);
        var principle = Utility.getFetch(Settings.baseUrl + '/lease/0/principle/?lease_in=' + this.props.lease_in);
        Promise.all([product, principle]).then((values) => {

            let weight = 0;
            for (let i in values[0]) {
                weight += parseFloat(values[0][i].weight);
            }

            let IC = 0;
            let DC = 0;
            for (let i in values[1]) {
                if (values[1][i].kind == 'IC')
                    IC += parseFloat(values[1][i].total);
                if (values[1][i].kind == 'DC')
                    DC += parseFloat(values[1][i].total);
            }
            this.setState({
                weight: weight.toFixed(2),
                items: this.setFieldValue(values[0]),
                total_add: IC,
                total_delete: DC
            })

            let elHeight = document.getElementById('total-box2')
            if (elHeight)
                this.setState({ table_width: window.innerWidth });
        })
    }

    setFieldValue(d) {

        let items = [];
        let object = {};
        let weight = 0
        let total = 0;

        let lease = {};

        for (let i in d) {
            if (lease['id_' + d[i].lease.id]==null)
                lease['id_' + d[i].lease.id] = {
                    weight: 0
                }
            lease['id_' + d[i].lease.id].weight += parseFloat(d[i].weight)
        }

        for (let i in d) {

            if (object['id_' + d[i].category.id] == null)
                object['id_' + d[i].category.id] = {
                    weight: 0,
                    average: 0
                }
            weight += parseFloat(d[i].weight)
            object['id_' + d[i].category.id].weight += parseFloat(d[i].weight)

            let ww = parseFloat(lease['id_' + d[i].lease.id].weight) / parseFloat(d[i].category.weight)
            let bb = (d[i].lease.amount / ww)

            object['id_' + d[i].category.id].average += (parseFloat(d[i].weight) / parseFloat(d[i].category.weight)) * bb
            object['id_' + d[i].category.id].category_name = d[i].category.name
        }

        for (let i in object) {
            object[i].weight = Utility.weightFormat(object[i].weight)
            object[i].average = Utility.priceFormat(object[i].average)
            items.push(object[i])
        }
        return items
    }

    render() {

        const items = this.state.items
        return (
            <Modal id='modalCal' onOpen={this.open} /*dimmer='blurring'*/ closeIcon trigger={<Button id='calculate' size='small' content='คำนวณ (F10)' onClick={this.handleCalculate} floated='right' icon='calculator' labelPosition='left' type='button' primary />}>
                <Modal.Header>คำนวณ</Modal.Header>
                <Modal.Content >
                    <Modal.Description>
                        <Grid columns={2} divided>
                            <Grid.Row>

                                <Grid.Column>
                                    <Form size='small'>

                                        <Form.Field inline>
                                            <label>สรุป น.น. ของขายฝาก น.น.รวม: </label>
                                            <label id='totalweight'>{this.state.weight} </label>
                                            {/* <Input value={this.state.weight} className='text-right' /> */}
                                        </Form.Field>
                                    </Form>
                                    <br />
                                    <div id="total-box2">
                                        <Table
                                            rowsCount={items.length}
                                            rowHeight={30}
                                            headerHeight={35}
                                            width={this.state.table_width}
                                            height={Settings.table_hiegh - 35}>
                                            <Column
                                                header={<Cell>%ทอง</Cell>}
                                                cell={
                                                    <ItemsCell data={items} field="category_name" />
                                                }
                                                width={100}
                                            />
                                            <Column
                                                header={<Cell>น้ำหนักทอง(กรัม)</Cell>}
                                                cell={
                                                    <ItemsCell data={items} field="weight" textAlign="text-right" />
                                                }
                                                width={120}
                                            />
                                            <Column
                                                header={<Cell>ราคาทองเฉลี่ยบาทละ</Cell>}
                                                cell={
                                                    <ItemsCell data={items} field="average" textAlign="text-right" />
                                                }
                                                width={180}
                                            />
                                        </Table>
                                    </div>
                                </Grid.Column>
                                <Grid.Column>
                                    <Form size='small'>
                                        <Form.Field >
                                            <label>ยอดดอกเบี้ย</label>
                                            <label id='interest'>{this.props.total_interest}</label>
                                            {/* <Input value={this.props.total_interest} className='text-right' /> */}
                                        </Form.Field>
                                        <Form.Field >
                                            <label>ยอดขายฝาก</label>
                                            <label id='least'>{Utility.priceFormat((this.props.total) - this.state.total_add + this.state.total_delete)}</label>
                                            {/* <Input value={Utility.priceFormat((this.props.total) - this.state.total_add + this.state.total_delete)} className='text-right' /> */}
                                        </Form.Field>
                                        <Form.Field >
                                            <label>ยอดเพิ่มเงินต้น</label>
                                            <label id='increse'>{Utility.priceFormat(this.state.total_add)}</label>
                                            {/* <Input value={Utility.priceFormat(this.state.total_add)} className='text-right' /> */}
                                        </Form.Field>
                                        <Form.Field >
                                            <label>ยอดลดเงินต้น</label>
                                            <label id='decrese'>{Utility.priceFormat(this.state.total_delete)}</label>
                                            {/* <Input value={Utility.priceFormat(this.state.total_delete)} className='text-right' /> */}
                                        </Form.Field>
                                        <Form.Field >
                                            <label>ยอดคงเหลือ</label>
                                            <label id='total'>{Utility.priceFormat(this.props.total)}</label>
                                            {/* <Input value={Utility.priceFormat(this.props.total)} className='text-right' /> */}
                                        </Form.Field>
                                    </Form>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </Modal.Description>
                </Modal.Content>
            </Modal>

        )
    }
}

export default CalculatTotal;