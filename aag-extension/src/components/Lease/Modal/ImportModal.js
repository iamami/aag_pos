/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';

import {
    Menu,
    Button,
    Modal
} from 'semantic-ui-react';
import ImportLese from '../../Import/ImportLese';
import ImportLeseProduct from '../../Import/ImportLeseProduct'
import ImportLeseInterest from '../../Import/ImportLeseInterest'
import ImportLeseIncreaseDecrease from '../../Import/ImportLeseIncreaseDecrease'

class ImportModal extends Component {

    constructor(props) {
        super(props);

        this.state = {}
    }

    componentDidMount(){

    }

    render(){
        if(this.props.open)
            return (<div>
                {this.state.lease_open && <ImportLese
                onClose={()=>{
                    this.setState({
                        lease_open: false,
                    });
                }}
                onUpdate={()=>{
                    this.setState({
                        lease_open: false,
                    });
                    this.componentDidMount()
                }} />}

                {this.state.lease_product_open && <ImportLeseProduct
                onClose={()=>{
                    this.setState({
                        lease_product_open: false,
                    });
                }}
                onUpdate={()=>{
                    this.setState({
                        lease_product_open: false,
                    });
                    this.componentDidMount()
                }} />}

                {this.state.lease_interest_open && <ImportLeseInterest
                onClose={()=>{
                    this.setState({
                        lease_interest_open: false,
                    });
                }}
                onUpdate={()=>{
                    this.setState({
                        lease_interest_open: false,
                    });
                    this.componentDidMount()
                }} />}

                {this.state.lease_inde_open && <ImportLeseIncreaseDecrease
                onClose={()=>{
                    this.setState({
                        lease_inde_open: false,
                    });
                }}
                onUpdate={()=>{
                    this.setState({
                        lease_inde_open: false,
                    });
                    this.componentDidMount()
                }} />}
            <Modal size='mini' open={this.props.open} >
                <Button id='btnCloseModalImport' circular icon='close'  basic floated='right' name='' onClick={this.props.onClose}/>
                <Modal.Header id='headerModalImport' >Import </Modal.Header>
                <Modal.Content id='contentModalImport' >
                    <Menu vertical fluid id='menuImport'>
                        <Menu.Item onClick={()=>this.setState({lease_open: true})}> Import รายการขายฝาก </Menu.Item>
                        <Menu.Item onClick={()=>this.setState({lease_product_open: true})}>Import สต็อกขายฝาก</Menu.Item>
                        <Menu.Item onClick={()=>this.setState({lease_interest_open: true})}>Import รายการดอกเบี้ยรับ</Menu.Item>
                        <Menu.Item onClick={()=>this.setState({lease_inde_open: true})}>Import เพิ่มลดเงินต้น</Menu.Item>
                    </Menu>
                </Modal.Content>
            </Modal>
        </div>)
        else return null
    }
}

export default ImportModal