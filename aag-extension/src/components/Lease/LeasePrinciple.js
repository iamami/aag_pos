/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import {
    Table,
    Column,
    Cell
} from 'fixed-data-table';
import {
    Form,
    Button,
} from 'semantic-ui-react';
import Utility from '../../Utility';
import Settings from '../../Settings';
import ItemsCellIcon from '../Widget/ItemsCell/ItemsCellIcon'
import IncreaseDecrease from './IncreaseDecrease'
import { connect } from 'react-redux'

function collect(props) {
    return { positon: props.positon };
}

class ItemsCell extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { rowIndex, field, data, ...props } = this.props;
        return (

                <Cell {...props}>

                    <div className={(this.props.textAlign == null ? '' : this.props.textAlign) + ' text-cell'}>{data[rowIndex][field]}</div>

                </Cell>
        );
    }
}


class Leaseprinciple extends Component {

    constructor(props) {
        super(props);

        this.state = {
            activeItem: window.location.pathname,
            table_width: 600,
            table_hiegh: 150,
            lease_product: [],
            loading: false,
            setting: this.props.setting,
            lease_principle: [],
            increase_decrease_open: props.sub_action=='add_delete'
        }

        this.handelSubmit = this.handelSubmit.bind(this)
        this.handelDeleteItem = this.handelDeleteItem.bind(this)

    }

    componentDidMount() {

        if (this.props.data.id) {
            let lease_principle = Utility.getFetch(Settings.baseUrl + '/lease/' + this.props.data.id + '/principle/?is_enabled=1');

            this.setState({
                loading: true
            })
            Promise.all([lease_principle]).then((values) => {

                this.setState({
                    lease_principle: this.setFieldValue(values[0])
                })
            });
        }

        setTimeout(()=>{

            let elHeight = document.getElementById('content-body2')
            this.setState({ w_content: elHeight.clientWidth }); 
        },100)

    }

    setFieldValue(list) {

        let j = 1;
        for (let i in list) {
            list[i].no = j++;
            list[i].date = Utility.formatDate(list[i].record_date)
            if(list[i].kind=='IC')
                list[i].increase = Utility.priceFormat(list[i].total)
            else
                list[i].decrease = Utility.priceFormat(list[i].total)
            list[i].balance_display = Utility.priceFormat(list[i].balance)
            list[i].staff_name = list[i].staff.name
        }

        return list
    }

    handelDeleteItem(e,item){
        console.log('this.props',this.props.data)
        if(window.confirm('ยืนยันลบ!')){
            Utility.delete(Settings.baseUrl + '/lease/' + this.props.data.id + '/principle/'+item.id+'/',(status,data,code)=>{

                if(status){
                    this.componentDidMount();
                }else if(code==403){
                    alert('ไม่สามารถลบได้ สิทธิ์ admin เท่านั้น')
                }
            })
        }
    }

    handelSubmit() {
        this.setState({increase_decrease_open: false})
        this.props.onSave();
        this.componentDidMount();
    }


    render() {

        const items = this.state.lease_principle
        return (<div id="content-body2">
            {this.state.increase_decrease_open && <IncreaseDecrease onClose={()=>this.setState({increase_decrease_open: false})} onSave={this.handelSubmit} lease={this.props.data} setting={this.state.setting} />}
            <Form>
                <Form.Group>
                    <Form.Field width={16}>
                        {this.props.data.id && <Button disabled={this.props.data.status > 2}  floated='right' primary content='เพิ่ม/ลด' size='mini' type='button' onClick={()=>this.setState({increase_decrease_open:true})} /> }
                    </Form.Field>
                </Form.Group>
            </Form>
            <Table
                rowsCount={items.length}
                rowHeight={30}
                headerHeight={30}
                width={this.state.w_content}
                height={this.state.table_hiegh}>
                <Column
                    header={<Cell></Cell>}
                    cell={
                        <ItemsCellIcon data={items} onClick={this.handelDeleteItem}  />
                    }
                    width={50}
                />
                <Column
                    header={<Cell>ครั้งที่</Cell>}
                    cell={
                        <ItemsCell id='count' data={items} field="no" textAlign='text-right' />
                    }
                    width={50}
                />
                <Column
                    header={<Cell>วันที่</Cell>}
                    cell={
                        <ItemsCell data={items} field="date" />
                    }
                    width={180}
                />
                <Column
                    header={<Cell className="text-right">เพิ่ม</Cell>}
                    cell={
                        <ItemsCell data={items} field="increase" textAlign='text-right' />
                    }
                    width={180}
                />
                <Column
                    header={<Cell className="text-right">ลด</Cell>}
                    cell={
                        <ItemsCell data={items} field="decrease"  textAlign='text-right'/>
                    }
                    width={120}
                />
                <Column
                    header={<Cell className="text-right">คงเหลือ</Cell>}
                    cell={
                        <ItemsCell data={items} field="balance_display" textAlign='text-right' />
                    }
                    width={120}
                />
                <Column
                    header={<Cell>พนักงาน</Cell>}
                    cell={
                        <ItemsCell data={items} field="staff_name" />
                    }
                    width={120}
                />
                <Column
                    header={<Cell>หมายเหตุ</Cell>}
                    cell={
                        <ItemsCell data={items} field="description" />
                    }
                    width={120}
                />
               
            </Table>
        </div>
        );
    }
}

const mapStateToProps = state =>{
    return ({
      lease_modal: state.lease_modal
    })
  }

export default connect(mapStateToProps,)(Leaseprinciple)
