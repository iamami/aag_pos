/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, {Component} from 'react';
import {Table, Column, Cell} from 'fixed-data-table';

import Utility from '../../Utility';
import Settings from '../../Settings';
import CalculateDay from './CalculateDay';
import PaymentModal from '../Payment/PaymentModal';
import {
    Form,
    Input,
    Button,
    TextArea,
    Modal,
    Label,
    Grid,
    Select
} from 'semantic-ui-react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import { lease_modal_edit} from '../../actions'
import { connect } from 'react-redux'

class PaymentInterest extends Component {

    constructor(props) {
        super(props);

        let auto_action_lease =localStorage.getItem('auto_action_lease')
        this.state = {
            open: auto_action_lease == 'interest' || auto_action_lease == 'redeem',
            title: auto_action_lease == 'redeem'
                ? 'ไถ่คืน'
                : 'ต่อดอก',
            type: auto_action_lease == 'redeem'
                ? '2'
                : '1',
            interest_date: moment(new Date(this.props.data.interest_date)),
            end_date: moment(new Date(this.props.data.end_date)),
            pay_date: moment(),
            type2: this.props.setting.calculate_date,
            total_pay: 0,
            month_option: this.getMontOption(),
            description: ''
        }
        if (auto_action_lease == 'interest' || auto_action_lease == 'redeem') 
            localStorage.setItem('auto_action_lease', '')
        this.handelCheckbox = this
            .handelCheckbox
            .bind(this)
        this.handelMonth = this
            .handelMonth
            .bind(this)
        this.handelCheckbox2 = this
            .handelCheckbox2
            .bind(this)
        this.handlerInput = this
            .handlerInput
            .bind(this)
        this.openModalInterest = this
            .openModalInterest
            .bind(this)
        this.open = this
            .open
            .bind(this)
        this.handelCalculat = this
            .handelCalculat
            .bind(this)
        this.handelSubmitBill = this
            .handelSubmitBill
            .bind(this)
        this.handelSubmit = this.handelSubmit.bind(this)
        this.handlerInputTotal = this.handlerInputTotal.bind(this)
    }

    getMontOption() {
        let month_option = [];
        for (let i = 1; i <= 12; i++) {
            month_option.push({key: i, value: i, text: i})
        }
        return month_option;
    }

    open() {

        this.setState({open: true})
        this.handelCalculat();
    }
    close = () => this.setState({open: false})

    handelCheckbox(e, v) {
        this.setState({
            type: v.value,
            title: v.value == '1'
                ? 'ต่อดอก'
                : 'ไถ่คืน'
        })
    }

    handelCheckbox2(e, v) {

        this.setState({
            type2: v.value,
        })
        this.setAmountMonthAndDay(this.state.interest_date,this.state.end_date,v.value)
    }

    componentDidMount(){
        this.setAmountMonthAndDay(this.state.interest_date,this.state.end_date,this.state.type2)
    }

    handelMonth(e, v) {
        let end = moment(new Date(this.state.interest_date)).add(parseInt(v.value) * 30, 'days')
        this.setState({
            end_date: end,
            month_select: v.value
        })

        this.setAmountMonthAndDay(this.state.interest_date,end,this.state.type2)
    }

    setAmountMonthAndDay(start,end,type2,is_submit=false){
        const a = moment(new Date(start));
        const b = moment(new Date(end));
        const diff = b.diff(a, 'days')
        const d = diff % 30
        let m = parseInt(diff / 30)
        let is_min = false

        if (d > 0) {
            if (type2 == 1) {
                m += (d / 30)
            } else if (type2 == 2) {
                m += (1 / 4)
            } else if (type2 == 3) {
                m += (1 / 2)
            } else if (type2 == 4) {
                m += (3 / 4)
            } else if (type2 == 5) {
                m += 1
            }
        }

        let total = m.toFixed(2)
        ? (parseFloat(this.props.data.amount) * parseFloat(this.props.data.interest) / 100) * parseFloat(m).toFixed(2)
        : 0
        if (total < this.props.setting.min_interest) {
            total = this.props.setting.min_interest
            is_min = true
        }

        let net_price = Utility.removeCommas(this.state.total_pay)
        if (this.state.type == 2) {
            net_price = (parseFloat(Utility.removeCommas(this.props.data.amount)) + parseFloat(net_price)).toFixed(2)
        }

        const total_pay = this.state.total_pay
        this.setState({
            day: d,
            month: m.toFixed(2),
            is_min: is_min,
            net_price: net_price,
            total: Math.ceil(total),
            total_pay: Math.ceil(is_submit ?total_pay:total)
        })
    }
    
    handlerInput(e, v) {
        this.setState({
            [v.name]: v.value
        })
    }

    handelSubmit(e) {
        this.setAmountMonthAndDay(this.state.interest_date,this.state.end_date,this.state.type2,true)
        this.totalPayInput.focus()
        this.setState({submit_loading: true})
        setTimeout(()=>{
            this.setState({submit_loading: false})
            this.setState({
                modal_lease_cashier: true
            })
        },800)
    }

    handelCalculat(e) {
        this.setAmountMonthAndDay(this.state.interest_date,this.state.end_date,this.state.type2)
        this.totalPayInput.focus()
    }

    openModalInterest() {}

    async handelSubmitBill(ledger) {

        ledger['ledger_category'] = this.state.type == '1' ? 7 : 10

        if(ledger['ledger_category']==10){
            const url = Settings.baseUrl + "/lease/" + this.props.data.id + "/interest/"
            const res = await Utility.getAsync(url)

            let count = 0
            res.data.map((i,item)=>{
                if(item.status==3)
                count++
            })
            if(count>0){
                alert('ไม่สามารถ ไถ่คืนซ้ำได้')
                return;

            }
        }
        
        this.submitLedger(ledger);
    }

    submitLedger(formData2) {
        // save ledger
        this.setState({loading_lease_cashier: true})

        let total = this.state.month
            ? (parseFloat(this.props.data.amount) * parseFloat(this.props.data.interest) / 100) * parseFloat(this.state.month).toFixed(2)
            : 0
        if (total < this.props.setting.min_interest) {
            total = this.props.setting.min_interest
        }

        let url = Settings.baseUrl + "/ledger/"
        Utility.post(url, formData2, (s, d,code) => {

            if(!s){
                alert(d.error)
                this.setState({open: false})
                this.props.onSave()
                lease_modal_edit(this.props.dispatch,this.props.data.id)
                return;
            }
            this.setState({loading_lease_cashier: false, modal_lease_cashier: false})

            // save interest
            let status = this.state.type == '1'
            ? 2
            : 3;
            let formData = {
                lease: this.props.data.id,
                ledger: d.id,
                pay_date: Utility.formatDate2(this.state.pay_date),
                interest_date: Utility.formatDate2(this.state.end_date),
                total_receive: Utility.removeCommas(this.state.total_pay),
                total: Math.ceil(total),
                description: this.state.description,
                status: status
            };
            this.submitInterest(formData);
        });
    }
    submitInterest(formData) {

        let url = Settings.baseUrl + "/lease/" + this.props.data.id + "/interest/"
        Utility.post(url, formData, (s, d) => {
            this.setState({loading_lease_cashier: false, modal_lease_cashier: false})

            alert('บันทึกข้อมูลสำเร็จ');
            this.setState({open: false})
            this.props.onSave()
            lease_modal_edit(this.props.dispatch,this.props.data.id)
        });
    }

    handlerInputTotal(e,v){
        const total_pay = Utility.parseFloat(v.value,this.state.total_pay)
        this.setState({total_pay})
    }

    render() {

        return (
            <Modal id='modalInterest' dimmer={true} open={true} size='small'>
                <Button
                    id='btnCloseInterest'
                    circular
                    icon='close'
                    basic
                    floated='right'
                    name=''
                    onClick={this.props.onClose}/>
                <Modal.Header>
                    {this.state.type == '1'
                        ? 'ต่อดอก'
                        : 'ไถ่คืน'}
                </Modal.Header>
                <Modal.Content className='scrolling'>

                    <center>
                        <Button.Group size='small'>
                            <Button id='btninterest'
                                onClick={() => {
                                this.setState({type: '1'})
                            }}
                                color={this.state.type == '1'
                                ? 'green'
                                : ''}>ต่อดอก</Button>
                            <Button.Or/>
                            <Button id='redeem'
                                onClick={() => {
                                this.setState({type: '2'})
                            }}
                                color={this.state.type == '2'
                                ? 'green'
                                : ''}>ไถ่คืน</Button>
                        </Button.Group>
                    </center>
                    <br/>
                    {this.state.modal_lease_cashier && <PaymentModal
                        object_number={this.props.data.number}
                        object_id={this.props.data.id}
                        total={this.state.net_price}
                        customer={this.props.data.customer.id}
                        onClose={() => {
                            this.setState({modal_lease_cashier: false})
                        }}
                        loading={this.state.loading_lease_cashier}
                        onSubmit={this.handelSubmitBill}/>}
                    <Form unstackable size='small'>
                        <table>
                            <tr>
                                <td
                                    style={{
                                    width: '20%',
                                    textAlign: 'right'
                                }}>
                                    <label>จำนวนเดือน</label>
                                </td>
                                <td
                                    style={{
                                    width: '30%'
                                }}><Select
                                    id='selectMonth'
                                    fluid
                                    onChange={this.handelMonth}
                                    options={this.state.month_option}
                                    value={this.state.month_select}/></td>
                                <td
                                    style={{
                                    width: '15%'
                                }}></td>
                                <td
                                    style={{
                                    width: '30%'
                                }}></td>
                                <td
                                    style={{
                                    width: '5%'
                                }}></td>
                            </tr>
                            <tr>
                                <td
                                    style={{
                                    width: '20%',
                                    textAlign: 'right'
                                }}>
                                    <label>ตั้งแต่วันที่</label>
                                </td>
                                <td
                                    style={{
                                    width: '30%'
                                }}><DatePicker
                                    selected={this.state.interest_date}
                                    selectsStart
                                    dateFormat="DD/MM/YYYY"
                                    startDate={this.state.interest_date}
                                    endDate={this.state.end_date}
                                    onChange={(d) => {
                                        this.setState({interest_date: d})
                                        this.setAmountMonthAndDay(d,this.state.end_date,this.state.type2)
                                    }}/></td>
                                <td
                                    style={{
                                    width: '20%',
                                    textAlign: 'right'
                                }}>
                                    <label>ถึงวันที่</label>
                                </td>
                                <td
                                    style={{
                                    width: '30%'
                                }}><DatePicker
                                    selected={this.state.end_date}
                                    selectsEnd
                                    dateFormat="DD/MM/YYYY"
                                    startDate={this.state.interest_date}
                                    endDate={this.state.end_date}
                                    onChange={(d) => {
                                    console.log(d);
                                    this.setState({end_date: d})
                                    this.setAmountMonthAndDay(this.state.interest_date,d,this.state.type2)
                                }}/></td>
                            </tr>
                            <tr>
                                <td
                                    style={{
                                    width: '20%',
                                    textAlign: 'right'
                                }}>
                                    <label>คิดเป็นเวลา</label>
                                </td>
                                <td
                                    style={{
                                    width: '30%'
                                }}><Input
                                    id='inputMonth'
                                    fluid
                                    value={Math.floor(this.state.month)}
                                    placeholder='เดือน'
                                    name='month'
                                    readOnly/></td>
                                <td
                                    style={{
                                    width: '20%'
                                }}>
                                    <label>เดือน</label>
                                </td>
                                <td
                                    style={{
                                    width: '30%'
                                }}><Input
                                    id='inputDate'
                                    fluid
                                    value={this.state.day}
                                    placeholder='day'
                                    name='day'
                                    readOnly/></td>
                                <td
                                    style={{
                                    width: '5%'
                                }}>วัน</td>
                            </tr>
                        </table>
                        <br/>
                        <CalculateDay onChange={this.handelCheckbox2} value={this.state.type2}/>
                        <br/>
                        <div className="w50">
                            <Form.Field widths={16}>
                                <label>จำนวนเงิน</label>
                                <Input
                                    id='Amountm'
                                    className="text-right"
                                    value={Utility.priceFormat(this.props.data.amount)}
                                    placeholder='จำนวนเงิน'
                                    name='amount'
                                    readOnly
                                    onChange={this.handlerInput}/>
                            </Form.Field>
                            <Form.Field widths={16}>
                                <label>อัตราดอกเบี้ย (%)</label>
                                <Input
                                    id='InterestRatio'
                                    className="text-right"
                                    value={this.props.data.interest}
                                    placeholder='อัตราดอกเบี้ย'
                                    name='month'
                                    readOnly
                                    onChange={this.handlerInput}/>
                            </Form.Field>
                             <Form.Field widths={16}>
                                <label>วันครบกำหนด</label>
                                <DatePicker
                                    id='inputEndDate'
                                    value={Utility.formatDate(moment(new Date(this.state.end_date)))}
                                    selected={this.state.end_date}
                                    dateFormat="DD/MM/YYYY"
                                    placeholder='วันครบกำหนด'
                                    name='end_date'
                                    onChange={(d) => {
                                    console.log(d);
                                    this.setState({end_date: d})
                                    
                                }}/>
                            </Form.Field>
                            <Form.Field >
                                <label>วันที่จ่าย</label>
                                <Input
                                    id='inputPayDate'
                                    value={Utility.formatDate(moment(new Date(this.state.pay_date)))}  

                                    />
                            </Form.Field>
                            <Form.Field >
                                <label>ระยะเวลาที่คิดดอกเบี้ย (เดือน)</label>
                                <Input
                                    id='inputInterestMonth'
                                    className="text-right"
                                    value={this.state.month}
                                    placeholder='ระยะเวลาที่คิดดอกเบี้ย'
                                    name='month'
                                    readOnly
                                    />
                            </Form.Field>
                            <Form.Field >
                                <label>ดอกเบี้ยคำนวณ {this.state.is_min
                                        ? <Label as='span' color='red' pointing='left'>**ดอกเบี้ยขั้นต่ำ</Label>
                                        : ''}</label>
                                <Input
                                    id='InterestTotal'
                                    className="text-right"
                                    value={Utility.priceFormat(this.state.total)}
                                    placeholder='ดอกเบี้ยคำนวณ'
                                    name='total'
                                    readOnly/>
                            </Form.Field>
                            <Form.Field>
                                <label>ดอกเบี้ยรับ</label>
                                <Input
                                    id='inputInterestPay'
                                    ref={(c)=>this.totalPayInput = c}
                                    className="text-right"
                                    value={this.state.total_pay}
                                    placeholder='ดอกเบี้ยรับ'
                                    name='total_pay'
                                    onChange={this.handlerInputTotal}/>
                            </Form.Field>
                            <Form.Field >
                                <label>หมายเหตุ</label>
                                <TextArea
                                    value={this.state.description}
                                    placeholder='หมายเหตุ'
                                    name='description'
                                    onChange={this.handlerInput}/>
                            </Form.Field>
                        </div>
                    </Form>
                </Modal.Content>
                <Modal.Actions>
                    <Button id='btnCal' primary content='คำนวณ' onClick={this.handelCalculat}/>
                    <Button
                        id='btnSaveInterest'
                        primary
                        loading={this.state.submit_loading}
                        content='บันทึก'
                        onClick={this.handelSubmit}/>
                </Modal.Actions>
            </Modal>
        )
    }
}
const mapStateToProps = state =>{
    return ({
      lease_modal: state.lease_modal
    })
  }

export default connect(mapStateToProps,)(PaymentInterest)