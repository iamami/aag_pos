/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import {
  Message
} from 'semantic-ui-react';
class Msg extends Component {

  render() {
    let list = this.props.list
    if (typeof list == 'objec') {
      let _list = []
      for (let k in list)
        _list.push(list[k])
      list = _list
    } else if (list == null || list.length == 0) {
      list = []
    }

    return (<div>
      {list == null || list.length == 0 ? '' : <Message
        error
        header='เกิดข้อผิดพลาด!'
        list={list}
      />}
    </div>

    );
  }
}

export default Msg;
