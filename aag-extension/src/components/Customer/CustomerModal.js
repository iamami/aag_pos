/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, {Component} from 'react';
import {
    Form,
    Button,
    Dropdown,
    Modal,
    Input,
    TextArea,
    Label,
    Icon,
    Checkbox,
    Menu,
    Segment,
    Confirm
} from 'semantic-ui-react';
import DatePicker from "react-datepicker";
import openSocket from 'socket.io-client';
import MsgInput from '../Error/MsgInput'
import Settings from '../../Settings';
import Utility from '../../Utility';
import moment from 'moment';

class Customer extends Component {

    constructor(props) {
        super(props);

        this.modal_open = false;
        this.ref_list = []

        let customer = props.customer

        this.state = {
            message_error_hidden: true,
            action: 'add',
            modal_title: 'สร้างลูกค้า',
            name: '',
            description: '',
            mobile: '',
            email: '',
            citizen_id: '',
            button_class: '',
            search: {},
            msg_error: {},
            is_application: true,
            activeItem: 'profile',
            address_primary: 1,
            birth_date: '',
        }

        if (props.action != 'add') {
            this.state['modal_title'] = 'แก้ไข'
            for (let i in customer) {

                
                this.state[i] = customer[i]
                if(i=='birth_date'){

                    if(customer[i]==null)
                        this.state[i] = ''
                    else
                    this.state.birth_date = moment(this.state.birth_date)
                }
            }
        }
        
        
        this.handlerSubmitForm = this
            .handlerSubmitForm
            .bind(this)
        this.socket = {}//openSocket('http://localhost:3333');
        this.handleItemClick = this.handleItemClick.bind(this)
    }


    componentDidMount() {
        this.setStateCard(0)
        // this
        //     .socket
        //     .on('READY', () => {
        //         this.setStateCard(1)
        //     })
        // this
        //     .socket
        //     .on('CLOSE', () => {
        //         this.setStateCard(0)
        //     })
        // this
        //     .socket
        //     .on('DATA', data => {
        //         console.log('DATA', data)
        //         this.setStateCard(2)
        //         this.setState(data)

        //     })

        Utility.get(Settings.baseUrl + '/system_setting/', (status, data) => {
            for (let i in data) {
                if (data[i].setting == 'CUSF') {
                    this.sys_setting_id = data[i].id
                    this.setState({
                        setting: JSON.parse(data[i].value)
                    })
                    break
                }
            }
        });

        this.nextRef(0)
    }
    resetForm() {
        this.setState({
            name: '',
            description: '',
            mobile: '',
            email: '',
            citizen_id: '',
            button_class: '',
            search: {},
            msg_error: {}
        });
    }

    handleItemClick(e,v){
        this.setState({
            activeItem: v.value
        })
    }

    handlerSubmitForm(e) {
        e.preventDefault();
        var formData = {
            name: this.state.name,
            mobile: this.state.mobile,
            email: this.state.email,
            description: this.state.description,
            citizen_id: this.state.citizen_id,
            is_application: this.state.is_application,
            address: this.state.address,
            address2: this.state.address2,
            address3: this.state.address3,
            district: this.state.district,
            district2: this.state.district2,
            district3: this.state.district3,
            city: this.state.city,
            city2: this.state.city2,
            city3: this.state.city3,
            province: this.state.province,
            province2: this.state.province2,
            province3: this.state.province3,
            postal_code: this.state.postal_code,
            postal_code2: this.state.postal_code2,
            postal_code3: this.state.postal_code3,
            address_primary: this.state.address_primary
        };

        let setting = this.state.setting
        for (let i in setting) {
            const o = setting[i]
            if (this.state[o.key] && o.check) 
                formData[o.key] = this.state[o.key]

            if (o.key == 'birth_date') {
                if(formData[o.key]!='' && formData[o.key]!=null)
                    formData[o.key] = moment(formData[o.key]).format('YYYY-MM-DD')
                else
                    delete formData[o.key]
            }
        }
        console.log(formData)
        const self = this;
        this.setState({button_class: 'loading'})
        if (this.props.action == 'add') {
            const url = Settings.baseUrl + '/customers/';
            Utility.post(url, formData, (status, data) => {
                this.setState({button_class: ''})

                if (status) {

                    alert('บันทึกข้อมูลสำเร็จ')
                    this.props
                        .onClose();
                    this.props
                        .onCreate(data);
                    self.resetForm();
                } else {
                    if (data.length !== 0) 
                        this.setState({msg_error: data})
                    if (data.error) {
                        alert(data.error)
                    }
                }
            });
        } else {
            const url = Settings.baseUrl + '/customers/' + this.state.id + "/";
            Utility.put(url, formData, (status, data) => {
                this.setState({button_class: ''})
                if (status) {
                    this.setState({button_class: '',msg_error: {}})
                    alert('บันทึกข้อมูลสำเร็จ')
                    this.props
                        .onUpdate(data);
                } else {
                    if (data.length !== 0) 
                        this.setState({msg_error: data})
                    if (data.error) {
                        alert(data.error)
                    }
                }
            });
        }
    }

    handlerStateModal() {

        this.modal_open = this.props.open;
    }

    setStateCard(action) {
        let state = {}
        if (action == 0) {
            state['smc_message'] = 'ไม่สามารถเชื่อมต่อกับอุปกรณ์ Smart Card'
            state['smc_color'] = ''
        } else if (action == 1) {
            state['smc_message'] = 'กรุณาเสียบบัตรประชาชนอีกครั้ง'
            state['smc_color'] = 'yellow'
        } else if (action == 2) {
            state['smc_message'] = 'อ่านข้อมูลบัตแล้ว'
            state['smc_color'] = 'green'
        }

        this.setState(state)
    }

    nextRef(index){

        if(index==this.ref_list.length-1){
            this.handlerSubmitForm()
            return false;
        }

        let ref = this.ref_list[index]
        console.log('ref',ref)
        if(ref!=null && ref.focus !=null)
            ref.focus()
    }

    render() {

        const {activeItem} = this.state

        if (this.props.action == 'add') 
            this.handlerStateModal()

        let setting = []
        for (let i in this.state.setting) {
            if (this.state.setting[i].check) 
                setting.push(this.state.setting[i])
        }

        var stateOptions = [ { key: '1', value: 1, text: 'ที่อยู่ 1' },{ key: '2', value: 2, text: 'ที่อยู่ 2' },{ key: '3', value: 3, text: 'ที่อยู่ 3' } ]
        return (
            <div>
            <Modal id="AddCustomerModal" open={this.props.open} size='tiny' /*dimmer='blurring'*/
            >
                <Button id="CloseAddCustomer"
                    circular
                    icon='close'
                    basic
                    floated='right'
                    name=''
                    onClick={(e) => {
                    //this.socket.close()
                    this.props.onClose(e)
                }}/>
                <Modal.Header>{this.state.modal_title}</Modal.Header>
                <Modal.Content>
                    <Modal.Description>

                    <Form size='small'>
                    <Form.Field error={this.state.msg_error.name != null}>
                                    <label>ที่อยู่หลัก</label>

                                <Dropdown value={this.state.address_primary} search selection options={stateOptions}  onChange={(e,v)=>{this.setState({address_primary: v.value})}}/>
                                </Form.Field>
                                <Form.Field error={this.state.msg_error.name != null}>
                                    <label>{this.state.msg_error.is_application}<MsgInput text={this.state.msg_error.is_application}/></label>

                                    <Checkbox
                                        label='ไม่ประสงค์จะใช้งานแอพพลิเคชั่น'
                                        name='is_application'
                                        checked={!this.state.is_application}
                                        onChange={(e, v) => this.setState({
                                        is_application: !v.checked
                                    })}/>
                                </Form.Field>
                        <Menu attached='top' tabular>
                            <Menu.Item
                                name='ข้อมูลส่วนตัว'
                                value='profile'
                                active={activeItem == 'profile'}
                                onClick={this.handleItemClick}/>
                            <Menu.Item
                                name='ที่อยู่ 2'
                                value='address2'
                                active={activeItem == 'address2'}
                                onClick={this.handleItemClick}/>
                            <Menu.Item
                                name='ที่อยู่ 3'
                                value='address3'
                                active={activeItem == 'address3'}
                                onClick={this.handleItemClick}/>
                        </Menu>
                        <Segment attached='bottom'>
                            

                            {activeItem=='profile' && <div>
                            <Form.Field>
                                    <Label basic as='div' color={this.state.smc_color}><Icon name="id card" size="large"/> {this.state.smc_message}</Label>
                                </Form.Field>
                                
                                {this.props.action == 'edit' && <Form.Field error={this.state.msg_error.code != null}>
                                    <label>รหัสลูกค้า {this.state.msg_error.code}<MsgInput text={this.state.msg_error.code}/></label>
                                    <Input readOnly={true} value={this.state.code}/>
                                </Form.Field>}
                                <Form.Field error={this.state.msg_error.name != null}>
                                    <label>ชื่อ <MsgInput text={this.state.msg_error.name}/></label>
                                    <Input
                                        ref={(c)=>this.ref_list[0] = c }
                                        position={0}
                                        onKeyPress={(e)=>{
                                            if (e.key == 'Enter') {
                                                this.nextRef(1)
                                                e.preventDefault()
                                            }
                                          }}
                                        onChange={(e) => this.setState({name: e.target.value})}
                                        value={this.state.name}/>
                                </Form.Field>
                                <Form.Field error={this.state.msg_error.mobile != null}>
                                    <label>เบอร์มือถือ
                                        <MsgInput text={this.state.msg_error.mobile}/></label>
                                    <Input
                                    onKeyPress={(e)=>{
                                        if (e.key == 'Enter') {
                                            this.nextRef(2)
                                            e.preventDefault()
                                        }
                                      }}
                                        ref={(c)=>this.ref_list[1] = c }
                                        position={1}
                                        onChange={(e) => this.setState({mobile: e.target.value})}
                                        value={this.state.mobile}/>
                                </Form.Field>
                                <Form.Field error={this.state.msg_error.email != null}>
                                    <label>อีเมล
                                        <MsgInput text={this.state.msg_error.email}/></label>
                                    <Input
                                    onKeyPress={(e)=>{
                                        if (e.key == 'Enter') {
                                            this.nextRef(3)
                                            e.preventDefault()
                                        }
                                      }}
                                        ref={(c)=>this.ref_list[2] = c }
                                        position={2}
                                        onChange={(e) => this.setState({email: e.target.value})}
                                        value={this.state.email}/>
                                </Form.Field>
                                <Form.Field>
                                    <label>เลขประจำตัวประชาชน</label>
                                    <Input
                                        onKeyPress={(e)=>{
                                            if (e.key == 'Enter') {
                                                this.nextRef(4)
                                                e.preventDefault()
                                            }
                                          }}
                                        ref={(c)=>this.ref_list[3] = c }
                                        position={3}
                                        onChange={(e) => this.setState({citizen_id: e.target.value})}
                                        value={this.state.citizen_id}/>
                                </Form.Field>
                                {setting.map((row, i) => <Form.Field key={i} error={this.state.msg_error[row.key] != null}>
                                    <label>{row.label}
                                        <MsgInput text={this.state.msg_error[row.key]}/></label>
                                    {row.options
                                        ? <Dropdown
                                                options={row.options}
                                                search
                                                selection
                                                name={row.key}
                                                onChange={(e, v) => {
                                                this.setState({
                                                    [v.name]: v.value
                                                })
                                            }}
                                                value={this.state[row.key]}/>
                                        : <div>
                                            {row.key == 'birth_date'
                                                ? <DatePicker
                                                        customInput={<input onKeyPress={(e) => {
                                                                    
                                                            if (e.key == 'Enter') {
                                                                this.nextRef(5+i)
                                                                e.preventDefault()
                                                            }
                                                        }} />}
                                                        ref={(c)=>this.ref_list[4+i] = c }
                                                        dateFormat="DD/MM/YYYY"
                                                        selected={this.state.birth_date}
                                                        onChange={(date) => this.setState({
                                                        [row.key]: date
                                                    })}/>
                                                : <Input
                                                    onKeyPress={(e)=>{
                                                        if (e.key == 'Enter') {
                                                            this.nextRef(5+i)
                                                            e.preventDefault()
                                                        }
                                                    }}
                                                    ref={(c)=>this.ref_list[4+i] = c }
                                                    position={3+i}
                                                    onChange={(e) => {
                                                    this.setState({
                                                        [row.key]: e.target.value
                                                    })
                                                    
                                                }}
                                                    value={this.state[row.key]}/>}
                                        </div>}
                                </Form.Field>)}
                                <Form.Field>
                                    <label>หมายเหตุ</label>
                                    <TextArea
                                        ref={(c)=>this.ref_list[this.ref_list.length] = c }
                                        position={this.ref_list.length}
                                        onChange={(e) => this.setState({description: e.target.value})}
                                        value={this.state.description}/>
                                </Form.Field>
                            </div>}
                            {activeItem=='address2' && <div>
                            <Form.Field>
                                    <label>ที่อยู่</label>
                                    <Input
                                        onChange={(e) => this.setState({address2: e.target.value})}
                                        value={this.state.address2}/>
                                </Form.Field>
                                <Form.Field>
                                    <label>ตำบล</label>
                                    <Input
                                        onChange={(e) => this.setState({district2: e.target.value})}
                                        value={this.state.district2}/>
                                </Form.Field>
                                <Form.Field></Form.Field>
                                <Form.Field>
                                    <label>อำเภอ</label>
                                    <Input
                                        onChange={(e) => this.setState({city2: e.target.value})}
                                        value={this.state.city2}/>
                                </Form.Field>
                                <Form.Field>
                                    <label>จังหวัด</label>
                                    <Input
                                        onChange={(e) => this.setState({province2: e.target.value})}
                                        value={this.state.province2}/>
                                </Form.Field>
                                <Form.Field>
                                    <label>รหัสไปรษณีย์</label>
                                    <Input
                                        onChange={(e) => this.setState({postal_code2: e.target.value})}
                                        value={this.state.postal_code2}/>
                                </Form.Field>
                            </div>}
                            {activeItem=='address3' && <div>
                            <Form.Field>
                                    <label>ที่อยู่</label>
                                    <Input
                                        onChange={(e) => this.setState({address3: e.target.value})}
                                        value={this.state.address3}/>
                                </Form.Field>
                                <Form.Field>
                                    <label>ตำบล</label>
                                    <Input
                                        onChange={(e) => this.setState({district3: e.target.value})}
                                        value={this.state.district3}/>
                                </Form.Field>
                                <Form.Field>
                                    <label>อำเภอ</label>
                                    <Input
                                        onChange={(e) => this.setState({city3: e.target.value})}
                                        value={this.state.city3}/>
                                </Form.Field>
                                <Form.Field>
                                    <label>จังหวัด</label>
                                    <Input
                                        onChange={(e) => this.setState({province3: e.target.value})}
                                        value={this.state.province3}/>
                                </Form.Field>
                                <Form.Field>
                                    <label>รหัสไปรษณีย์</label>
                                    <Input
                                        onChange={(e) => this.setState({postal_code3: e.target.value})}
                                        value={this.state.postal_code3}/>
                                </Form.Field>
                            </div>}
                                
                            

                        </Segment>

                        </Form>
                    </Modal.Description>
                </Modal.Content>
                <Modal.Actions>

                    <Button
                        id="AddConfirm"
                        size='small'
                        primary
                        onClick={this.handlerSubmitForm}
                        className={this.state.button_class}>บันทึก</Button>
                    <Button
                        id="AddCancel"
                        size='small'
                        onClick={(e) => {
                        e.preventDefault();
                        this
                            .props
                            .onClose();
                    }}>ยกเลิก</Button>
                </Modal.Actions>
            </Modal> </div>
        );
    }

}
Customer.defaultProps = {
    action: 'add'
};

export default Customer