/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import {
  Table,
  Column,
  Cell
} from 'fixed-data-table';
import { 
  Dimmer, Loader,Form
} from 'semantic-ui-react';
import 'react-dates/lib/css/_datepicker.css';
import {ContextMenuProvider } from 'react-contexify';
import Settings from '../../Settings';
import Utility from '../../Utility';

class ItemsCell extends Component {


  render() {
    const { rowIndex, field, data, ...props } = this.props;
    return (
      <Cell {...props} className='cell-time-click' onClick={(e) => {
        this.props.onClick(e, data[rowIndex]);
      }}>
        <ContextMenuProvider
          data={rowIndex}
          id="menu_id"
          className={(this.props.textAlign == null ? '' : this.props.textAlign) + ' text-cell'}>
          <div >{data[rowIndex][field]}</div>
        </ContextMenuProvider>
      </Cell>
    );
  }
}

class BillList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      total:{
        '1': 0,
        '2': 0,
        '3': 0,
        '4': 0,
      }
    }

  }

  componentDidMount() {
    this.setState({
      loader_active: true
    });
    let querytoday = Utility.jsonToQueryString({});

    var bills = Utility.getFetch(Settings.baseUrl + '/lease/?is_enabled=1&customer=' + this.props.customer_id);
    Promise.all([bills]).then((values) => {
      this.items = values[0]

      
      this.setState({
        loader_active: false,
        items: this.setFieldValue(this.items),
        table_width: window.innerWidth
      });
    });
  }


  setFieldValue(items) {
    let total ={
      '1': 0,
      '2': 0,
      '3': 0,
      '4': 0,
    }
    let total_interest = 0
    for (let i in items) {
      items[i].branch_name = items[i].branch.name
      items[i].status_title = Settings.status_lease[items[i].status]
      total[items[i].status+''] += parseFloat(items[i].amount)
      total_interest+= parseFloat(items[i].total_interest)
    }

    console.log('total',total)
    this.setState({
      total: total,
      total_interest: total_interest
    })
    return items
  }

  componentWillMount() { this.setState({ table_width: this.props.clientWidth ? this.props.clientWidth : window.innerWidth - 70, table_height: window.innerHeight - 450 }); }
  render() {
    let items = this.state.items
    const activeItem = this.state.activeItem
    return (<div>

      <div className='relative' id='table_width'>
        <Dimmer className={this.state.loader_active ? 'active' : ''} inverted>
          <Loader content='Loading' inverted />
        </Dimmer>

        <Table
          rowsCount={items.length}
          rowHeight={35}
          headerHeight={30}
          width={this.state.table_width}
          height={this.state.table_height}>
          <Column
            header={<Cell>สาขา</Cell>}
            cell={
              <ItemsCell onClick={this.handlerClickItem} data={items} field="branch_name" />
            }
            width={140}
          />
          <Column
            header={<Cell>เลขที่ขายฝาก</Cell>}
            cell={
              <ItemsCell id='leaseID' onClick={this.handlerClickItem} data={items} field="number" />
            }
            width={120}
          />
          <Column
            header={<Cell>วันที่นำเข้า</Cell>}
            cell={
              <ItemsCell onClick={this.handlerClickItem} data={items} field="start_date" />
            }
            width={180}
          />
          <Column
            header={<Cell>วันครบกำหนด</Cell>}
            cell={
              <ItemsCell onClick={this.handlerClickItem} data={items} field="end_date" />
            }
            width={120}
          />
          <Column
            header={<Cell className='text-right'>จำนวนเงิน</Cell>}
            cell={
              <ItemsCell onClick={this.handlerClickItem} data={items} field="amount" textAlign='text-right' />
            }
            width={180}
          />
          <Column
            header={<Cell className='text-center'>สถานะ</Cell>}
            cell={
              <ItemsCell onClick={this.handlerClickItem} data={items} field="status_title" textAlign='text-center'  />
            }
            width={180}
          />
          <Column
            header={<Cell className='text-right'>เกินกำหนด(วัน)</Cell>}
            cell={
              <ItemsCell onClick={this.handlerClickItem} data={items} field="day" textAlign='text-right' />
            }
            width={180}
          />
        </Table>

        <Form size='small'>
        <Form.Group widths='equal'>
          <Form.Input className='text-right' label='คัดออก' value={Utility.priceFormat(this.state.total['4'])} />
          <Form.Input className='text-right' label='ไถ่คืน' value={Utility.priceFormat(this.state.total['3'])} />
          <Form.Input className='text-right' label='ยังไม่ไถ่คืน' value={Utility.priceFormat(this.state.total['1']+this.state.total['2'])} />
          <Form.Input className='text-right' label='รวม' value={Utility.priceFormat(this.state.total['1']+this.state.total['2']+this.state.total['3']+this.state.total['4'])} />
          
        </Form.Group>
        <Form.Group widths='equal'>
            <Form.Field width={3}></Form.Field>
            <Form.Field width={3}></Form.Field>
            <Form.Field width={3}></Form.Field>
            <Form.Input width={3} className='text-right' label='ดอกเบี้ยรับรวม' value={Utility.priceFormat(this.state.total_interest)} />
        </Form.Group>
      </Form>
      </div>
    </div>)
  }
}

export default BillList;
