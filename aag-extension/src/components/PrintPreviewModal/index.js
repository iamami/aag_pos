/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import {
    Button, Modal
} from 'semantic-ui-react';
/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import {hotkeys} from 'react-keyboard-shortcuts'

class PrintPreviewModal extends Component {

    constructor(props){
        super(props)

        this.handlerPrint = this.handlerPrint.bind(this)
    }

    handlerPrint(){
        var mywindow = window.open('', 'PRINT', 'height=600,width=1024');
        mywindow.document.write('<html><head><title>' +this.props.title + '</title>');
        mywindow.document.write('</head><body style="font-size: 8px;font-family: Lato,\'Helvetica Neue\',Arial,Helvetica,sans-serif">');
        mywindow.document.write(document.getElementById('view-print').innerHTML);
        mywindow.document.write('</body></html>');
        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10*/
        mywindow.print();
        mywindow.close();
    }

    render() {
        return (<div>
            <Modal  open={true} onClose={this.close} size='fullscreen' /*dimmer='blurring'*/>
            <Button
            id='closePreview'
          circular
          icon='close'
          basic
          floated='right'
          name=''
          onClick={this.props.onClose}/>
            <Modal.Header>Preview</Modal.Header>
                <Modal.Content className='scrolling'>
                    {this.props.children}
                </Modal.Content>
                <Modal.Actions>
                    <ReactHTMLTableToExcel
                        id="test-table-xls-button"
                        className="ui primary button small"
                        table="table-to-xls"
                        filename={this.props.filename}
                        sheet={this.props.title}
                        buttonText="Download as XLS" >
                    </ReactHTMLTableToExcel>
                    <Button primary icon='print' size='small' onClick={this.handlerPrint} labelPosition='right' content='พิมพ์ (F8)' />
                </Modal.Actions>
            </Modal>
        </div>)
    }

    hot_keys = {
        'f8': {
          priority: 1,
          handler: (event) => {
            this.handlerPrint()
            event.preventDefault()
          }
        },
        'esc': {
            priority: 1,
            handler: (event) => {
                this.props.onClose()
              event.preventDefault()
            }
          }
      }
}

export default hotkeys(PrintPreviewModal)