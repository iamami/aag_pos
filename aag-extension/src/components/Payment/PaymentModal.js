/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, {Component} from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import {
  Form,
  Input,
  Dropdown,
  Button,
  Segment,
  Icon,
  Label,
  Modal,
  Grid,
  Divider
} from 'semantic-ui-react';
import PropTypes from 'prop-types';
import 'react-dates/lib/css/_datepicker.css';
import Utility from '../../Utility';
import Settings from '../../Settings';
import MsgInput from '../Error/MsgInput'
import {hotkeys} from 'react-keyboard-shortcuts'
import { connect } from 'react-redux'

class PaymentModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
        kind: 'IN',
        total: props.total,
        ledger_category: '',
        cash: props.total,
        card: 0,
        card_bank_card: '',
        card_fee: 0,
        card_service: 0,
        card_contract_number: '',
        card_code: '',
        card_period: '',
        card_start: '',
        check_bank: '',
        check_code: '',
        check: 0,
        description: '',
        status: 1,
      show_card: false,
      payment_date: moment(),
      check_date: '',
      cash_receipt: '',
      error: {}
    }


    this.handlerInputChange = this.handlerInputChange.bind(this)
    this.handlerChangeCashReceipt = this.handlerChangeCashReceipt.bind(this)
    this.handlerChangeIsCheck = this.handlerChangeIsCheck.bind(this)
    this.handlerChangeCardBankCard = this.handlerChangeCardBankCard.bind(this)
    this.handlerSubmitPayment = this.handlerSubmitPayment.bind(this)
    this.handlerChangeFee = this.handlerChangeFee.bind(this)

    this.cashTotalInput = React.createRef();

  }

  handlerInputChange(e, v) {
    const value =  Utility.parseFloat(v.value,this.state[v.name])
    this.setState({[v.name]: value})

    if(v.name=='cash' && !this.state.is_check){
      let cash = value
      cash = cash<0 || isNaN(cash)?0:cash
      let card = this.props.total - cash
      if(card>0){
        this.setState({
          cash: cash,
          show_card: true,
          card: card
        })
      }else{
        this.setState({
          show_card: false,
          card: 0,
          card_code: '',
          card_bank_card: ''
        })
      }
    }
  }

  handlerChangeIsCheck(e,v){
    const total = this.props.total

    if(v.checked){
      this.checkCodeInput.focus()
      this.setState({
        is_check: v.checked,
        check_date: moment(),
        check: total,
        cash: 0,
        cash_receipt: 0,
        show_card: false,
        card: 0,
        card_service: 0
      })
    }else{
      this.cashTotalInput. current.focus()
      this.setState({
        is_check: v.checked,
        check_date: '',
        check: 0,
        cash: total,
        cash_receipt: 0,
        show_card: false,
        card: 0,
        card_service: 0
      })
    }
  }

  handlerChangeCashReceipt(e,v){
    const cash_receipt = Utility.parseFloat(v.value,this.state.cash_receipt)
    console.log('cash_receipt',cash_receipt,v.value)
    this.setState({
      cash_receipt,
    })
  }

  componentDidMount(){

    Utility.get(Settings.baseUrl + "/banks/", (e, resJson) => {
      this.banks = resJson;
      let banks = []
      for (let i in resJson) {
        banks.push({value: resJson[i].id, key: resJson[i].id, text: resJson[i].name});
      }
      this.setState({banks_options: banks});
    })

    Utility.get(Settings.baseUrl + "/bank_cards/", (e, resJson) => {
      this.bank_cards = resJson;
      let bank_cards = []
      for (let i in resJson) {
        bank_cards.push({
          value: resJson[i].id,
          key: resJson[i].id,
          text: resJson[i].kind + " " + resJson[i].bank.name
        });
      }
      this.setState({bank_cards_options: bank_cards});
    })

    setTimeout(()=>{
      this.cashTotalInput.current.focus();
    },500)
  }

  handlerChangeCardBankCard(e,v){

    let o = Utility.getObject(this.bank_cards,v.value)
    let card = parseFloat(this.state.card)
    let card_service = Math.round(card * (o.fee / 100))
    let card_total = card + card_service
    this.setState({
      card_bank_card: v.value,
      card_fee: o.fee,
      card_service: card_service,
      card_total: card_total
    })
  }

  handlerChangeFee(e,v){
    if (v.value == null || v.value == '') v.value = 0
    let card = parseFloat(this.state.card)
    console.log('handlerChangeFee',v.value)
    let card_service = (v.value+"")
    if(isNaN(card_service)){
      this.setState({
        error: {
          card_service: 'ตัวเลขเท่านั้น'
        }
      })
      return;
    }
    let card_total = card + parseFloat(card_service+"")
    console.log('handlerChangeFee',card_service)
    this.setState({
      card_service: card_service,
      card_total: card_total
    })
  }

  handlercheckValidate(){
    let error = {}


    if(this.state.is_check){ // valid check
      if(this.state.check_code==null || !this.state.check_code.toString().trim().length){
        error['check_code'] = 'กรุณาระบุเลขที่เช็ค'
      }
      if(this.state.check_bank==null || !this.state.check_bank.toString().trim().length || this.state.check_bank==0){
        error['check_bank'] = 'กรุณาเลือกธนาคาร'
      }
      if(this.state.check==null || !this.state.check.toString().trim().length){
        error['check'] = 'กรุณาระบุยอดจ่าย'
      }
      if(this.state.check_date==null || !this.state.check_date.toString().trim().length){
        error['check_date'] = 'กรุณาระบุวันที่'
      }else if(!moment(this.state.check_date).isValid){
        error['check_date'] = 'วันที่ไม่ถูกต้อง'
      }

    }else if(this.state.card>0){ // valid card
      if(this.state.card_code==null || !this.state.card_code.toString().trim().length){
        error['card_code'] = 'กรุณาระบุรหัสบัตรให้ถูกต้อง'
      }
      if(this.state.card_bank_card==null || !this.state.card_bank_card.toString().trim().length){
        error['card_bank_card'] = 'กรุณาเลือกชนิดบัตร'
      }

      if(this.state.card_fee==null || this.state.card_fee.toString().trim().length){
        this.setState({card_fee: 0})
      }

    }else if(this.state.cash>0){ // valid card
      if(this.state.cash_receipt==null || !this.state.cash_receipt.toString().trim().length || this.state.cash_receipt<=0){
        error['cash_receipt'] = 'กรุณาระบุยอดรับเงินสด'
        this.refs.cash_receipt.focus();
      }
      if(this.state.cash_receipt<this.state.cash){
        error['cash_receipt'] = 'กรุณาระบุยอดรับเงินสดให้ถูกต้อง'
      }
    }

    if(this.state.card!='' && isNaN(parseFloat(this.state.card))){
      error['card'] = 'ตัวเลขเท่านั้น'
    }

    if(this.state.cash_receipt!='' && isNaN((this.state.cash_receipt))){
      error['cash_receipt'] = 'ตัวเลขเท่านั้น'
    }

    this.setState({
      error: error
    })
    return Object.keys(error).length==0
  }

  handlerSubmitPayment(){

    if(!window.confirm('ยืนยันการชำระเงิน'))
      return

    if(this.handlercheckValidate()){
      const {branch,user} = this.props.auth
      let card = this.state.card
      let cash = this.state.cash
      let check = this.state.check
      let payment = 'CS' // cash
      if(check>0){
        payment = 'CH' //check
      }else if(cash >0&&check==0&&card==0){
        payment = 'CS'
      }else if(cash>0&&card>0){
        payment = 'CC' // cash + card
      }else if(cash==0 &&card>0){
        payment = 'CD' //card
      } 


      let ledger = {
        object_id: this.props.object_id,
        object_number: this.props.object_number,
        branch: branch.id,
        staff: this.props.staff,
        ledger_date: this.props.ledger_date,
        kind: 'IN',
        customer: this.props.customer,
        total: this.props.total,
        ledger_category: this.props.ledger_category,
        cash: cash,
        card: card,
        card_bank_card: this.state.card_bank_card,
        card_fee: this.state.card_fee,
        card_service: this.state.card_service,
        card_code: this.state.card_code,
        card_period: this.state.card_period,
        card_start: this.state.card_start!=''?Utility.formatDate2(this.state.card_start):'' ,
        check_bank: this.state.check_bank,
        check_code: this.state.check_code,
        check_date: this.state.check_date!=''?Utility.formatDate2(this.state.check_date):'' ,
        check: check,
        payment: payment,
        description: '',
        status: 1
      }

      console.log(ledger)

      this.props.onSubmit(ledger)
    }else{
      console.log(this.state.error)
    }
  }

  render() {

    let change =  parseFloat(this.state.cash_receipt | 0) - parseFloat(this.state.cash)
    change = change<0?0:change

    return (
      <div>
        <Modal  open={true} >
              <Button
              id='closePaymentModal'
          circular
          icon='close'
          basic
          floated='right'
          name=''
          onClick={this.props.onClose}/>
              <Modal.Header id='headerModalPayment'><Icon name='payment'/> ชำระเงิน</Modal.Header>
              <Modal.Content className='scrolling1'>
                <Modal.Description id='descriptModalPayment'>
                  <div>
                    <Grid>
                      <Grid.Row columns={2} >
                        <Grid.Column>
                        <Label as='div' basic color='green' floated='right' size='huge'>
                            <right>{Utility.priceFormat(this.props.total)}</right>
                        </Label>
                        <br/>
                      <Form
                            className='attached fluid segment'
                            size='small'
                            onSubmit={this.onSubmitCashTotal}>
                            <Form.Field id="resultcash">
                              <label>ยอดจ่ายเงินสด</label>
                              <Input
                                id='cash'
                                onFocus={(e)=> e.target.select()}
                                name='cash'
                                type='number' 
                                min='0'
                                ref={this.cashTotalInput}
                                focus
                                size='big'
                                className="text-right"
                                readOnly={this.state.is_check }
                                onKeyPress={(e)=>{
                                  if (e.key == 'Enter') {
                                    if(this.state.show_card)
                                      this.refs.card_code.focus();
                                    else{
                                      this.refs.cash_receipt.focus();
                                    }
                                    this.setState({cash_receipt: this.state.cash})
                                      
                                  }
                                }}
                                value={this.state.cash}
                                onChange={this.handlerInputChange}/>
                            </Form.Field>
                          </Form>
                          <Form className='attached fluid segment' size='small'>
                              <Form.Field error={this.state.error.card_code!=null}>
                                <label>รหัสบัตร <MsgInput text={this.state.error.card_code}  /></label>
                                <Input
                                  id='IDcredit'
                                  disabled={!this.state.show_card }
                                  ref="card_code"
                                  name="card_code"
                                  value={this.state.card_code}
                                  onChange={(e, v) => {

                                  if( !isNaN(parseInt(v.value)))
                                    this.setState({card_code:v.value});
                                }}/>
                              </Form.Field>
                              <Form.Field error={this.state.error.card_bank_card!=null}>
                                <label>ชนิดบัตร <MsgInput text={this.state.error.card_bank_card}  /></label>
                                <Dropdown
                                  id='type'
                                  disabled={this.state.disabled_card || !this.state.show_card }
                                  search
                                  selection
                                  width={14}
                                  options={this.state.bank_cards_options}
                                  onChange={this.handlerChangeCardBankCard}
                                  value={this.state.card_bank_card}/>
                              </Form.Field>
                              <Form.Field error={this.state.error.card_fee!=null}>
                                <label>%ค่าธรรมเนียม</label>
                                <Input
                                  disabled={this.state.disabled_card|| !this.state.show_card }
                                  value={this.state.card_fee}
                                  className='text-right'
                                  onChange={this.handlerFee}/>
                              </Form.Field>
                              <Form.Field error={this.state.error.card!=null}>
                                <label>จ่ายเครดิต</label>
                                <Input
                                  id='credit'
                                  disabled={this.state.disabled_card|| !this.state.show_card }
                                  className='text-right'
                                  value={Utility.priceFormat(this.state.card)}/>
                              </Form.Field>
                              <Form.Field error={this.state.error.card_service!=null}>
                                <label>ค่าบริการ</label>
                                <Input
                                  id='service'
                                  disabled={this.state.disabled_card|| !this.state.show_card }
                                  className='text-right'
                                  value={this.state.card_service}
                                  onChange={this.handlerChangeFee}/>
                              </Form.Field>
                              <Form.Field error={this.state.error.card_total!=null}>
                                <label>ยอดจ่ายรวม</label>
                                <Input
                                  id='total'
                                  disabled={this.state.disabled_card|| !this.state.show_card }
                                  className='text-right'
                                  value={Utility.priceFormat(this.state.card_total) }/>
                              </Form.Field>
                            </Form>

                        </Grid.Column>
                        <Grid.Column>
                          {this.state.cash>0 && <Segment secondary>
                            <Form className='fluid ' color='purple' size='big'>
                              <Form.Field error={this.state.error.payment_date!=null}>
                                <label>วันที่ชำระเงิน <MsgInput text={this.state.error.payment_date}  /></label>
                                <DatePicker
                                  dateFormat="DD/MM/YYYY"
                                  selected={this.state.payment_date}
                                  onChange={(date) => {
                                  this.setState({payment_date: date});
                                }}/>
                              </Form.Field>
                              <Form.Field error={this.state.error.cash_receipt!=null}>
                                <label>จำนวนรับ เงินสด <MsgInput text={this.state.error.cash_receipt}  /></label>
                                <Input 
                                  id='inputCash'
                                  type='number'
                                  value={(this.state.cash_receipt)}
                                  onChange={this.handlerChangeCashReceipt}
                                  onKeyPress={(e)=>{
                                    if (e.key == 'Enter') {
                                        this.handlerSubmitPayment()                          
                                    }
                                  }}
                                  ref="cash_receipt"
                                  name="cash_receipt"
                                  className='text-right'/>
                              </Form.Field>
                              <Form.Field >
                                <label>จำนวนเงินทอน</label>
                                <Input
                                  id='change'
                                  value={Utility.priceFormat(change)}
                                  className='text-right'
                                  readOnly/>
                              </Form.Field>
                            </Form>
                          </Segment>}

                          {this.state.show_card &&  <Segment>
                                <b>บันทึกรายละเอียดการผ่อนด้วยบัตรเครดิต</b>
                                <Divider />
                                <Form  size='small'>
                                  <Form.Field >
                                    <label>จำนวนงวด <MsgInput text={this.state.error.card_period}  /></label>
                                    <Input
                                      disabled={this.state.disabled_card}
                                      min={0}
                                      type="number"
                                      value={this.state.card_period}
                                      onChange={(e, v) => {
                                      this.setState({card_period: Utility.parseInt(v.value,this.state.card_period)});
                                    }}/>
                                  </Form.Field>
                                  <Form.Field >
                                    <label>วันที่เริ่มผ่อน <MsgInput text={this.state.error.card_start}  /></label>
                                    <DatePicker
                                      disabled={this.state.disabled_card}
                                      dateFormat="DD/MM/YYYY"
                                      selected={this.state.card_start}
                                      onChange={(date) => {

                                       console.log(date)
                                      this.setState({card_start: date});
                                    }}/>
                                  </Form.Field>
                                </Form>
                              </Segment>}
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                  </div>
                </Modal.Description>
              </Modal.Content>
              <Modal.Actions>
                <Button
                id='btnSave'
                  size='small'
                  primary
                  onClick={this.handlerSubmitPayment}
                  className={this.state.button_class
                  ? 'loading'
                  : ''}>บันทึกชำระเงิน</Button>
              </Modal.Actions>
            </Modal>
      </div>
    );
  }
  hot_keys = {
    'esc':{
      priority: 4,
      handler: this.props.onClose
    },
    'enter':{
      priority: 4,
      handler: (e)=>{
        //e.preventDefault()
      }
    }
  }
}

PaymentModal.defaultProps = {
  total: 0,
  staff: '',
  object_id: 0,
  object_number: '',
  ledger_date: Utility.formatDate2(moment()),
  kind: 'IN',// IN or EX,
  customer: 1
};
PaymentModal.propTypes = {
  total: PropTypes.number.isRequired,
  staff: PropTypes.number.isRequired,
  onSubmit: PropTypes.func.isRequired,
  ledger_category: PropTypes.number.isRequired
}

const mapStateToProps = state =>{
  return ({
    auth: state.auth,
  })
}
export default connect(
  mapStateToProps,
)(hotkeys(PaymentModal))
