/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';

import {
  BrowserRouter as Router,
  Route,
  Link,
} from 'react-router-dom';

import {
  Menu,
  Grid,
  Icon
} from 'semantic-ui-react';

import SellGroupPrintPreview from './POS/SellGroupPrintPreview'
import SalesPrintPreview from './POS/SalesPrintPreview'
import SellPrintPreview from './POS/SellPrintPreview'
import SellTypePrintPreview from './POS/SellTypePrintPreview'
import BuyPrintPreview from './POS/BuyPrintPreview'
import ExcPrintPreview from './POS/ExcPrintPreview'
import SellTypeAmountPrintPreview from './POS/SellTypeAmountPrintPreview'
import StockInPrintPreview from './Stock/StockInPrintPreview'
import StockEXPrintPreview from './Stock/StockEXPrintPreview'
import StockCategoryEXPrintPreview from './Stock/StockCategoryEXPrintPreview'
import StockSellBuyPrintPreview from './Stock/StockSellBuyPrintPreview'
import StokcDailyPrintPreview from './Stock/StokcDailyPrintPreview'
import StokcDaily2PrintPreview from './Stock/StokcDaily2PrintPreview'
import StockTypeAmountPrintPreview from './Stock/StockTypeAmountPrintPreview'
import StockTypeAndCodePrintPreview from './Stock/StockTypeAndCodePrintPreview'
import StockVolumePrintPreview from './Stock/StockVolumePrintPreview'
import StockMotionPrintPreview from './Stock/StockMotionPrintPreview'
import StockNotMotionPrintPreview from './Stock/StockNotMotionPrintPreview'
import LeasePrintPreview from './Lease/LeasePrintPreview'
import LeaseInterestPrintPreview from './Lease/LeaseInterestPrintPreview'
import LeaseRedeemPrintPreview from './Lease/LeaseRedeemPrintPreview'
import LeaseEjectPrintPreview from './Lease/LeaseEjectPrintPreview'
import LeaseDuePrintPreview from './Lease/LeaseDuePrintPreview'
import LeaseOverduePrintPreview from './Lease/LeaseOverduePrintPreview'
import LeaseMonthPrintPreview from './Lease/LeaseMonthPrintPreview'
import LeaseGroupMonthPrintPreview from './Lease/LeaseGroupMonthPrintPreview'
import LeaseAllPrintPreview from './Lease/LeaseAllPrintPreview'
import LeaseStockPrintPreview from './Lease/LeaseStockPrintPreview'
import LeasePrinciplePrintPreview from './Lease/LeasePrinciplePrintPreview'
import LeaseInterestHistoryPrintPreview from './Lease/LeaseInterestHistoryPrintPreview'
import Daily from './Conclude/Daily'
import LedgerDaily from './Conclude/LedgerDaily'
import LedgerPrintPreview from './Conclude/LedgerPrintPreview'
import LedgerKindPrintPreview from './Conclude/LedgerKindPrintPreview'
import LedgerCardPrintPreview from './Conclude/LedgerCardPrintPreview'
import LedgerCheckPrintPreview from './Conclude/LedgerCheckPrintPreview'
import SellPrintPreview2 from './Conclude/SellPrintPreview'

class Report extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeItem: this.props.location.pathname
    }

    this.handleItemClick = this.handleItemClick.bind(this);
  }

  state = {
    activeItem: '/report/ledger'
  };

  handleItemClick(e, { name, to }) {
    this.setState({
      activeItem: to
    });
  }

  componentDidMount() {

    let elHeight = document.getElementById('content-body')

    this.setState({
      clientWidth: window.innerWidth
    })
  }
  render() {
    let clientWidth = this.state.clientWidth
    return (
      <Router>
        <div>
          <Grid>
            <Grid.Column width={3}>
              <Menu pointing fluid secondary vertical className='menu-report'>
                <Menu.Item>
                  <Menu.Header><Icon name='money' /> ซื้อ-ขาย</Menu.Header>
                  <Menu.Menu>
                  <Menu.Item
                      name='sell'
                      active={this.state.activeItem == '/report/sell_group'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/sell_group'
                    >รายงานขายทองรูปพรรณ/ทองแท่ง</Menu.Item>
                    <Menu.Item
                      name='sell'
                      active={this.state.activeItem == '/report/sell'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/sell'
                    >รายงานการขายทอง</Menu.Item>

                    <Menu.Item
                      name='bill'
                      active={this.state.activeItem == '/report/sell_type'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/sell_type'
                    >รายงานการขายแยกตามประเภทและน้ำหนัก</Menu.Item>
                    <Menu.Item
                      name='bill'
                      active={this.state.activeItem == '/report/buy'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/buy'
                    >รายงานการซื้อทองเก่า</Menu.Item>
                    <Menu.Item
                      name='bill'
                      active={this.state.activeItem == '/report/exc'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/exc'
                    >รายงานการเปลี่ยนทอง</Menu.Item>
                    <Menu.Item
                      name='bill'
                      active={this.state.activeItem == '/report/sell_type_amount'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/sell_type_amount'
                    >รายงานจำนวนชิ้นขาย</Menu.Item>
                    {/* <Menu.Item
                      name='bill'
                      active={this.state.activeItem == '#'}
                      onClick={this.handleItemClick}
                      as={Link}
                      disabled
                      to='#'
                    >รายงานค่าคอมมิชชั่นพนักงาน</Menu.Item> */}
                  </Menu.Menu>

                </Menu.Item>
                <Menu.Item>
                  <Menu.Header><Icon name='shipping' /> สต็อกทอง</Menu.Header>
                  <Menu.Menu>
                    <Menu.Item
                      name='stock_im'
                      active={this.state.activeItem == '/report/stock_im'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/stock_im'
                    >การนำเข้าทอง</Menu.Item>
                    <Menu.Item
                      name='stock_ex'
                      active={this.state.activeItem == '/report/stock_ex'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/stock_ex'
                    >การนำทองออกจากสต็อก</Menu.Item>

                    <Menu.Item
                      name='stock_category_ex'
                      active={this.state.activeItem == '/report/stock_category_ex'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/stock_category_ex'
                    >การนำทองออกทองเก่า</Menu.Item>
                    <Menu.Item
                      name='stock_daily2'
                      active={this.state.activeItem == '/report/stock_daily2'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/stock_daily2'
                    >รายงานสต็อกสินค้า</Menu.Item>
                    <Menu.Item
                      name='stock_sell_buy'
                      active={this.state.activeItem == '/report/stock_sell_buy'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/stock_sell_buy'
                    >ทองเข้า-ทองออก</Menu.Item>
                    <Menu.Item
                      name='stock_daily'
                      active={this.state.activeItem == '/report/stock_daily'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/stock_daily'
                    >สต็อกคงเหลือตามวันที่</Menu.Item>
                    <Menu.Item
                      name='stock_type_and_code'
                      active={this.state.activeItem == '/report/stock_type_and_code'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/stock_type_and_code'
                    >สต็อกแยกตามประเภท</Menu.Item>
                    <Menu.Item
                      name='stock_type_amount'
                      active={this.state.activeItem == '/report/stock_type_amount'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/stock_type_amount'
                    >จำนวนชิ้นในสต็อก</Menu.Item>
                    <Menu.Item
                      name='stock_volume'
                      active={this.state.activeItem == '/report/stock_volume'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/stock_volume'
                    >ปริมาณทองในสต็อก</Menu.Item>
                    <Menu.Item
                      name='stock_motion'
                      active={this.state.activeItem == '/report/stock_motion'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/stock_motion'
                    >การเคลื่อนไหวของสินค้า</Menu.Item>
                    <Menu.Item
                      name='stock_not_motion'
                      active={this.state.activeItem == '/report/stock_not_motion'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/stock_not_motion'
                    >สินค้าไม่เคลื่อนไหว</Menu.Item>
                  </Menu.Menu>
                </Menu.Item>

                <Menu.Item>
                  <Menu.Header><Icon name='add to calendar' /> ขายฝาก</Menu.Header>
                  <Menu.Menu>
                    <Menu.Item
                      name='lease'
                      active={this.state.activeItem == '/report/lease'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/lease'
                    >ยอดขายฝาก</Menu.Item>
                    <Menu.Item
                      name='lease_interest'
                      active={this.state.activeItem == '/report/lease_interest'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/lease_interest'
                    >ยอดดอกเบี้ย</Menu.Item>
                    <Menu.Item
                      name='redeem'
                      active={this.state.activeItem == '/report/lease_redeem'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/lease_redeem'
                    >ยอดไถ่คืน</Menu.Item>
                    <Menu.Item
                      name='lease_eject'
                      active={this.state.activeItem == '/report/lease_eject'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/lease_eject'
                    >ยอดคัดออก</Menu.Item>
                    <Menu.Item
                      name='lease_due'
                      active={this.state.activeItem == '/report/lease_due'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/lease_due'
                    >ยอดครบกำหนด</Menu.Item>
                    <Menu.Item
                      name='lease_overdue'
                      active={this.state.activeItem == '/report/lease_overdue'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/lease_overdue'
                    >เกินกำหนด</Menu.Item>
                    <Menu.Item
                      name='lease_mounth'
                      active={this.state.activeItem == '/report/lease_mounth'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/lease_mounth'
                    >ยอดขายฝาก,ดอกเบี้ย,รายเดือนตามปีที่ระบุ</Menu.Item>
                    <Menu.Item
                      name='lease_group_mounth'
                      active={this.state.activeItem == '/report/lease_group_mounth'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/lease_group_mounth'
                    >ยอดขายฝาก,ดอกเบี้ย,รายเดือน</Menu.Item>
                    <Menu.Item
                      name='lease_all'
                      active={this.state.activeItem == '/report/lease_all'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/lease_all'
                    >สรุปขายฝากทั้งหมด</Menu.Item>

                    <Menu.Item
                      name='lease_stock'
                      active={this.state.activeItem == '/report/lease_stock'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/lease_stock'
                    >สินค้าขายฝาก</Menu.Item>

                    <Menu.Item
                      name='lease_principle'
                      active={this.state.activeItem == '/report/lease_principle'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/lease_principle'
                    >ประวัติการเพิ่มลดเงินต้น</Menu.Item>
                    <Menu.Item
                      name='lease_history'
                      active={this.state.activeItem == '/report/lease_history'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/lease_history'
                    >ประวัติการต่อดอก</Menu.Item>
                  </Menu.Menu>
                </Menu.Item>
                <Menu.Item>
                  <Menu.Header><Icon name='add to calendar' /> รายงานสรุป</Menu.Header>
                  <Menu.Menu>
                    <Menu.Item
                      name='daily'
                      active={this.state.activeItem == '/report/daily'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/daily'
                    >รายงานสรุปประจำวัน</Menu.Item>

                    <Menu.Item
                      name='ledger_daily'
                      active={this.state.activeItem == '/report/ledger_daily'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/ledger_daily'
                    >รายรับ-รายจ่ายประจำวัน</Menu.Item>
                    <Menu.Item
                      name='ledger_list'
                      active={this.state.activeItem == '/report/ledger_list'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/ledger_list'
                    >บิลรับเงิน-จ่ายเงิน</Menu.Item>
                    <Menu.Item
                      name='sell'
                      active={this.state.activeItem == '/report/sales'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/sales'
                    >รายงานยอดขาย</Menu.Item>
                    <Menu.Item
                      name='ledger_kind'
                      active={this.state.activeItem == '/report/ledger_kind'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/ledger_kind'
                    >บิลรับเงิน-จ่ายเงินแยกตามประเภท</Menu.Item>
                    <Menu.Item
                      name='ledger_card'
                      active={this.state.activeItem == '/report/ledger_card'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/ledger_card'
                    >การชำระเงินแบบเครดิต</Menu.Item>
                    <Menu.Item
                      name='ledger_check'
                      active={this.state.activeItem == '/report/ledger_check'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/ledger_check'
                    >การชำระเงินแบบเช็ค</Menu.Item>
                    <Menu.Item
                      name='ledger_sell'
                      active={this.state.activeItem == '/report/ledger_sell'}
                      onClick={this.handleItemClick}
                      as={Link}
                      to='/report/ledger_sell'
                    >สรุปยอดขายทองรวม</Menu.Item>

                    
                  </Menu.Menu>
                </Menu.Item>
              </Menu>
            </Grid.Column>
            <Grid.Column stretched width={13} className="content-body" >
              <div id="content-body">
                <Route path="/report/sell_group" component={() => (<SellGroupPrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/sell" component={() => (<SellPrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/sales" component={() => (<SalesPrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/sell_type" component={() => (<SellTypePrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/buy" component={() => (<BuyPrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/exc" component={() => (<ExcPrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/sell_type_amount" component={() => (<SellTypeAmountPrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/stock_im" component={() => (<StockInPrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/stock_ex" component={() => (<StockEXPrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/stock_category_ex" component={() => (<StockCategoryEXPrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/stock_sell_buy" component={() => (<StockSellBuyPrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/stock_daily2" component={() => (<StokcDaily2PrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/stock_daily" component={() => (<StokcDailyPrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/stock_type_amount" component={() => (<StockTypeAmountPrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/stock_type_and_code" component={() => (<StockTypeAndCodePrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/stock_volume" component={() => (<StockVolumePrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/stock_motion" component={() => (<StockMotionPrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/stock_not_motion" component={() => (<StockNotMotionPrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/lease" component={() => (<LeasePrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/lease_interest" component={() => (<LeaseInterestPrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/lease_redeem" component={() => (<LeaseRedeemPrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/lease_eject" component={() => (<LeaseEjectPrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/lease_due" component={() => (<LeaseDuePrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/lease_overdue" component={() => (<LeaseOverduePrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/lease_mounth" component={() => (<LeaseMonthPrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/lease_group_mounth" component={() => (<LeaseGroupMonthPrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/lease_all" component={() => (<LeaseAllPrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/lease_stock" component={() => (<LeaseStockPrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/lease_principle" component={() => (<LeasePrinciplePrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/lease_history" component={() => (<LeaseInterestHistoryPrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/daily" component={() => (<Daily items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/ledger_daily" component={() => (<LedgerDaily items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/ledger_list" component={() => (<LedgerPrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/ledger_kind" component={() => (<LedgerKindPrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/ledger_card" component={() => (<LedgerCardPrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/ledger_check" component={() => (<LedgerCheckPrintPreview items={[]} clientWidth={clientWidth} />)} />
                <Route path="/report/ledger_sell" component={() => (<SellPrintPreview2 items={[]} clientWidth={clientWidth} />)} />
                
              </div>
            </Grid.Column>
          </Grid>
        </div>
      </Router>

    );
  }
}

export default Report;
