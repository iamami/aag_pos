/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';

import {
    Table,
    Column,
    Cell
} from 'fixed-data-table';
import Utility from '../../../Utility';


class ItemsCell extends Component {
    render() {
        const { rowIndex, field, data, type, ...props } = this.props;

        let v = type == 'number' ? Utility.numberFormat(data[rowIndex][field]) : data[rowIndex][field];
        return (
            <Cell {...props} >
                <div className={(this.props.textAlign == null ? '' : this.props.textAlign)}>{this.props.type=='f'?Utility.weightFormat(v):v}</div>
            </Cell>
        );
    }
}


class TableCategory extends Component {
    render() {

        let items = this.props.items
        return (<Table
            rowsCount={items.length}
            rowHeight={35}
            headerHeight={30}
            width={this.props.width}
            height={150}>
            <Column
                header={<Cell>%ทอง</Cell>}
                cell={
                    <ItemsCell data={items} field="title" />
                }
                width={80}
                fixed={true}
            />
            <Column
                header={<Cell>น.น.ขาย(กรัม)</Cell>}
                cell={
                    <ItemsCell data={items} field="weight" textAlign='text-right' type='f' />
                }
                width={100}
                fixed={true}
            />
            <Column
                header={<Cell>น.น.ขาย(บาท)</Cell>}
                cell={
                    <ItemsCell data={items} field="weight_b" textAlign='text-right' type='f' />
                }
                width={100}
                fixed={true}
            />
        </Table>)
    }
}

export default  TableCategory 