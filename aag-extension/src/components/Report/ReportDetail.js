/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import Utility from '../../Utility';
import Settings from '../../Settings';
import {
    Table,
    Column,
    Cell
} from 'fixed-data-table';

import {
    Form, Segment, Header, Button, Message, Loader, Dimmer, Icon, Dropdown, Input, Modal, Grid, Popup
} from 'semantic-ui-react';
import 'react-datepicker/dist/react-datepicker.css';

class ItemsCell extends Component {
    constructor(props) {
      super(props);
  
    }
    render() {
      const { rowIndex, field, data, ...props } = this.props;
      return (
        <Cell {...props} >

          <div className={this.props.textAlign}><span className={this.props.color}>{data[rowIndex][field]}</span></div>
        </Cell>
      );
    }
  }


class ReportDetail extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            item: [],
        }
    }

    componentWillMount() { this.setState({ table_width: window.innerWidth - 110 }); }

    render() {

        const data = this.props.data;
        console.log(data)
        return (

            <Modal size='fullscreen' open={this.props.open} /*dimmer='blurring'*/>
                <Modal.Header>สต็อกการ์ดทอง </Modal.Header>
                <Modal.Content className='scrolling'>
                    <Modal.Description>
                        <Table
                            rowsCount={data.length}
                            rowHeight={35}
                            headerHeight={30}
                            width={this.state.table_width}
                            height={400} >
                            <Column
                                header={<Cell>วันที่</Cell>}
                                cell={
                                    <ItemsCell data={data} field="record_date" />
                                }
                                width={80}
                            />
                            <Column
                                header={<Cell>สาขา</Cell>}
                                cell={
                                    <ItemsCell data={data} field="branch_name" />
                                }
                                width={150}
                            />
                            <Column
                                header={<Cell>เลขที่บิลอ้างอิง</Cell>}
                                cell={
                                    <ItemsCell data={data} field="object_number" />
                                }
                                width={150}
                            />
                            <Column
                                header={<Cell>รหัสสินค้า</Cell>}
                                cell={
                                    <ItemsCell data={data} field="product_code" />
                                }
                                width={120}
                            />
                            <Column
                                header={<Cell>ชื่อสินค้า</Cell>}
                                cell={
                                    <ItemsCell data={data} field="product_name" />
                                }
                                width={150}
                            />
                            <Column
                                header={<Cell>จ.น. เริ่มต้น</Cell>}
                                cell={
                                    <ItemsCell data={data} field="before" textAlign='text-right' color='pink' />
                                }
                                width={120}
                            />
                            <Column
                                header={<Cell>จ.น. เข้า</Cell>}
                                cell={
                                    <ItemsCell data={data} field="amount_in" textAlign='text-right' color='blue' />
                                }
                                width={120}
                            />
                            <Column
                                header={<Cell>จ.น. ออก</Cell>}
                                cell={
                                    <ItemsCell data={data} field="amount_out" textAlign='text-right' color='red' />
                                }
                                width={120}
                            />
                            <Column
                                header={<Cell>จ.น. คงเหลือ</Cell>}
                                cell={
                                    <ItemsCell data={data} field="after" textAlign='text-right' color='green' />
                                }
                                width={120}
                            />

                            <Column
                                header={<Cell>น.น. เริ่มต้น</Cell>}
                                cell={
                                    <ItemsCell data={data} field="before_w" textAlign='text-right' color='pink' />
                                }
                                width={150}
                            />
                            <Column
                                header={<Cell>น.น. เข้า</Cell>}
                                cell={
                                    <ItemsCell data={data} field="amount_in_w" textAlign='text-right' color='blue' />
                                }
                                width={150}
                            />
                            <Column
                                header={<Cell>น.น. ออก</Cell>}
                                cell={
                                    <ItemsCell data={data} field="amount_out_w" textAlign='text-right' color='red'/>
                                }
                                width={150}
                            />
                            <Column
                                header={<Cell>น.น. คงเหลือ</Cell>}
                                cell={
                                    <ItemsCell data={data} field="after_w" textAlign='text-right' color='pink' />
                                }
                                width={150}
                            />
                        </Table>
                    </Modal.Description>

                </Modal.Content>

                <Modal.Actions>
                    <Button size='small' onClick={(e) => {
                        e.preventDefault()

                        this.props.onClose()
                    }}>ปิด</Button>
                </Modal.Actions>
            </Modal>


        );
    }
}


export default ReportDetail;