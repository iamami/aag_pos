/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import {
    Form, Segment, Header, Button, Message, Loader, Dimmer, Icon, Dropdown, Input, Modal, Grid, Popup, Label, Table
} from 'semantic-ui-react';

import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import Utility from '../../../Utility';
import Settings from '../../../Settings';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import { connect } from 'react-redux'
import DropdownBranch from '../../Widget/DropDown/DropDownBranch'

class GroupRow extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const textRight = {
            'text-align': 'right'
        }

        const textLeft = {
            'text-align': 'left'
        }
        const textCenter = {
            'text-align': 'center'
        }
        const textU = {
            'text-decoration': 'underline'
        }
        const data = this.props.data
        let kind = {}
        let kind_arr = []
        for (let i in data.items) {
            let item = Utility.cloneObjectJson(data.items[i])
            if (kind[item.product.kind.id] == null)
                kind[item.product.kind.id] = {
                    items: [],
                    kind_name: item.product.kind.name,
                    n: 0,
                    weight: 0
                }
                kind[item.product.kind.id].n += parseInt(item.after) 
                kind[item.product.kind.id].weight += parseInt(item.after)*parseFloat(item.product.weight) 
            kind[item.product.kind.id].items.push(item)
        }
        for (let i in kind) {
            let is_title = true
            for (let j in kind[i].items) {

                if (is_title)
                    kind_arr.push({
                        is_title: is_title,
                        title: kind[i].kind_name
                    });
                is_title = false
                kind_arr.push(kind[i].items[j])
            }

        }
        return (<Table.Body>
            <Table.Row>
                <Table.HeaderCell colSpan='2' style={textLeft}>{data.title}</Table.HeaderCell>
                <Table.HeaderCell colSpan='5'></Table.HeaderCell>
            </Table.Row>
            {kind_arr.map((row, i) => row.is_title ? <Table.Row>
                <Table.HeaderCell ></Table.HeaderCell>
                <Table.HeaderCell style={textLeft}>{row.title}</Table.HeaderCell>
                <Table.HeaderCell colSpan='5'></Table.HeaderCell>
            </Table.Row> : <Table.Row id={row.product.code}>
                    <Table.HeaderCell ></Table.HeaderCell>
                    <Table.HeaderCell style={textLeft}>&nbsp;&nbsp;{row.product.name}</Table.HeaderCell>
                    <Table.HeaderCell style={textCenter}>{row.product.code}</Table.HeaderCell>
                    <Table.HeaderCell style={textRight}>{Utility.weightFormat(row.product.weight)}</Table.HeaderCell>
                    <Table.HeaderCell style={textRight}>{row.after}</Table.HeaderCell>
                    <Table.HeaderCell style={textRight}>{Utility.weightFormat(Utility.removeCommas(row.after) * Utility.removeCommas(row.product.weight))}</Table.HeaderCell>
                    <Table.HeaderCell style={textCenter}>&nbsp;&nbsp;{row.branch.name}</Table.HeaderCell>
                </Table.Row>)}
            <Table.Row>
                <Table.HeaderCell colSpan='4' style={textRight}></Table.HeaderCell>
                <Table.HeaderCell style={textRight}><div id={'amount_'+data.id} style={textU}>{data.n}</div></Table.HeaderCell>
                <Table.HeaderCell style={textRight}><div id={'weight_'+data.id} style={textU}>{Utility.weightFormat(data.weight)}</div></Table.HeaderCell>
                <Table.HeaderCell ></Table.HeaderCell>
            </Table.Row>
            
        </Table.Body>)
    }
}


class PrintPreview extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search: {
                date_lt: moment(),
                branch: props.auth.branch.id,
            },
            items: []
        }

        this.resetForm = this.resetForm.bind(this)
        this.handlerSubmit = this.handlerSubmit.bind(this)
        this.handlerInput = this.handlerInput.bind(this)
    }

    componentDidMount() {

        var branches = Utility.getFetch(Settings.baseUrl + '/branches/?is_enabled=1');
        var staffs = Utility.getFetch(Settings.baseUrl + '/staffs/?is_enabled=1');
        var product_types = Utility.getFetch(Settings.baseUrl + '/product_types/?is_enabled=1');
        Promise.all([branches, staffs, product_types]).then((values) => {
            this.branches = values[0];
            let branches = []
            for (let i in this.branches) {
                branches.push({
                    value: this.branches[i].id,
                    key: this.branches[i].id,
                    text: this.branches[i].name
                });
            }

            this.staffs = values[1];
            let staffs = []
            for (let i in this.staffs) {
                staffs.push({
                    value: this.staffs[i].id,
                    key: this.staffs[i].id,
                    text: this.staffs[i].name
                });
            }

            this.product_types = values[2];
            let product_types = []
            for (let i in this.product_types) {
                product_types.push({
                    value: this.product_types[i].id,
                    key: this.product_types[i].id,
                    text: this.product_types[i].name
                });
            }

            this.setState({
                loader_active: false,
                branches: branches,
                staffs: staffs,
                product_types: product_types
            });
        });
        var url_string = window.location.toString();
        var url = new URL(url_string);
        var temp_date = url.searchParams.get("date_lt");
        if (temp_date !== null){
            this.setState({
                    search:{
                        date_lt: moment(temp_date,'DD/MM/YYYY'),
                    }
            })
        }
    }

    resetForm(e) {

        this.setState({
            search: {
                date_lt: moment(),
                branch: this.props.auth.branch.id,
            }
        })

    }

    handlerSubmit(e) {
        e.preventDefault();
        let search = Utility.cloneObjectJson(this.state.search)
        search.date_lt = Utility.formatDate2(search.date_lt)

        let fromData = Utility.jsonToQueryString(search)
        console.log(fromData)
        let url = Settings.baseUrl + "/stock_product/0/items?" + fromData

        this.setState({
            open: true,
            loading: true
        })
        Utility.get(url, (s, d) => {
            // alert(s)
            if (s == true) {
                this.setState({
                    loading: false,
                    items: this.setFieldValue(d)
                })
            }
        })
    }

    setFieldValue(_items) {
        let items = []
        for (let i in _items) {
            let item = _items[i]

            items.push(item)
        }
        return items
    }

    handlerInput(e, v) {
        let search = this.state.search
        search[v.name] = v.value
        this.setState({
            search: search
        })
    }
    
    render() {
        let data = this.props.data

        let title = 'รายงานสต็อกสินค้า';
        let filename = 'sell_type_amount';

        const divStyle = {
            color: '#111',
            'font-size': '10px',
            width: '100%',
            'line-height': '20px',
            'font-family': 'monospace'
        };
        const textRight = {
            'text-align': 'right',
        }

        const textLeft = {
            'text-align': 'left',
        }
        const textCenter = {
            'text-align': 'center',
        }
        const textU = {
            'text-decoration': 'underline'
        }
        let amount = 0
        //let weight = 0
        let weight_b = 0
        let sell = 0
        let net = 0

        let category = {}
        for (let i in this.state.items) {
            let item = this.state.items[i]
            if (category[item.product.category.id] == null)
                category[item.product.category.id] = {
                    items: [],
                    title: item.product.category.name,
                    id: item.product.category.id,
                    n: 0,
                    weight: 0
                }
            category[item.product.category.id].n += parseInt(item.after)
            category[item.product.category.id].weight += parseInt(item.after)* parseFloat(item.product.weight)
            category[item.product.category.id].items.push(item)
        }
        let items2 = []
        for (let i in category) {
            items2.push(category[i])
        }

        let sum_total = 0;
                let branch_name = ''
        const {branches} = this.props
        if (this.state.search.branch) {
            let b = Utility.getObject(branches, this.state.search.branch)
            if (b)
                branch_name = b.name
        }
        return (<div>

            <div className="box-login">
                <Segment textAlign='left' >

                    <Header size='small'>เงือนไขค้นหา</Header>
                    <Form size='small' onSubmit={this.handlerSubmit}>
                        <Form.Field>
                            <label>สาขา</label>
                            <DropdownBranch  fluid size='small' fluid selection onChange={this.handlerInput} name="branch" value={this.state.search.branch} />
                        </Form.Field>

                        <Form.Field >
                            <label>วันที่</label>
                            <DatePicker
                                dateFormat="DD/MM/YYYY"
                                value={this.state.search.date_lt}
                                selected={this.state.search.date_lt}
                                onChange={(date) => {
                                    this.handlerInput(null, {
                                        name: 'date_lt',
                                        value: date
                                    });
                                }}
                            />
                        </Form.Field>
                        <Button>พิมพ์</Button>
                        <Button type='button' onClick={this.resetForm}>รีเซ็ต</Button>
                    </Form>
                </Segment>
            </div>
            <Modal open={this.state.open} onClose={this.close} size='fullscreen' /*dimmer='blurring'*/>
                <Modal.Header>Preview</Modal.Header>
                <Modal.Content className='scrolling'>
                    {this.state.loading && <Dimmer active={this.state.loading} inverted>
                        <Loader inverted>Loading</Loader>
                    </Dimmer>}
                    <div id='view-print'>
                        <div id='paperA4-portrait'>
                            <Table basic id='table-to-xls' style={divStyle}>
                                <Table.Header>
                                    <Table.Row >
                                        <Table.HeaderCell style={textCenter} colSpan='7'><center>{title}</center></Table.HeaderCell>
                                    </Table.Row>
                                    <Table.Row >
                                        <Table.HeaderCell style={textCenter} colSpan='7'><center id='branch'>สาขา : {branch_name}</center></Table.HeaderCell>
                                    </Table.Row>
                                    <Table.Row >
                                        <Table.HeaderCell style={textCenter} colSpan='7'><center id='date'>ประจำวันที่ : {Utility.formatDate(this.state.search.date_lt)}</center></Table.HeaderCell>
                                    </Table.Row>
                                    <Table.Row>
                                        <Table.HeaderCell colSpan='2' >ประเภท</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter} >รหัสสินค้า</Table.HeaderCell>
                                        <Table.HeaderCell style={textRight} >น.น./ชิ้น</Table.HeaderCell>
                                        <Table.HeaderCell style={textRight} >จำนวน</Table.HeaderCell>
                                        <Table.HeaderCell style={textRight} >น.น.รวม</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter} >สาขา</Table.HeaderCell>
                                    </Table.Row>
                                    
                                </Table.Header>

                                {items2.map((row, i) => <GroupRow key={i} data={row} />)}

                            </Table>
                        </div>
                    </div>
                </Modal.Content>
                <Modal.Actions>
                    <ReactHTMLTableToExcel
                        id="test-table-xls-button"
                        className="ui primary button small"
                        table="table-to-xls"
                        filename={filename}
                        sheet={title}
                        buttonText="Download as XLS" >
                    </ReactHTMLTableToExcel>

                    <Button primary icon='print' size='small' onClick={() => {
                        var mywindow = window.open('', 'PRINT', 'height=600,width=1024');
                        mywindow.document.write('<html><head><title>' + title + '</title>');
                        mywindow.document.write('</head><body style="font-size: 8px;font-family: Lato,\'Helvetica Neue\',Arial,Helvetica,sans-serif">');
                        mywindow.document.write(document.getElementById('view-print').innerHTML);
                        mywindow.document.write('</body></html>');
                        mywindow.document.close(); // necessary for IE >= 10
                        mywindow.focus(); // necessary for IE >= 10*/
                        mywindow.print();
                        mywindow.close();
                    }} labelPosition='right' content='Print' />

                    <Button id='btnClosePreview' size='small' type='button' onClick={() => { this.setState({ open: false }) }}>ปิด</Button>
                </Modal.Actions>
            </Modal>
        </div>)
    }
}

const mapStateToProps = state =>{
    return ({
      auth: state.auth,
      branches: state.branches
    })
  }
  export default connect(
    mapStateToProps,
  )(PrintPreview)