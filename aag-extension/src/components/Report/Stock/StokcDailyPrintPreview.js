/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import {
    Form, Segment, Header, Button, Message, Loader, Dimmer, Icon, Dropdown, Input, Modal, Grid, Popup, Label, Table
} from 'semantic-ui-react';

import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import Utility from '../../../Utility';
import Settings from '../../../Settings';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import { connect } from 'react-redux'
import DropdownBranch from '../../Widget/DropDown/DropDownBranch'

class PrintPreview extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search: {
                date_lt: moment(),
                branch: props.auth.branch.id,
            },
            items: []
        }

        this.resetForm = this.resetForm.bind(this)
        this.handlerSubmit = this.handlerSubmit.bind(this)
        this.handlerInput = this.handlerInput.bind(this)
    }

    componentDidMount() {

        var branches = Utility.getFetch(Settings.baseUrl + '/branches/?is_enabled=1');
        var staffs = Utility.getFetch(Settings.baseUrl + '/staffs/?is_enabled=1');
        var product_types = Utility.getFetch(Settings.baseUrl + '/product_types/?is_enabled=1');
        Promise.all([branches, staffs, product_types]).then((values) => {
            this.branches = values[0];
            let branches = []
            for (let i in this.branches) {
                branches.push({
                    value: this.branches[i].id,
                    key: this.branches[i].id,
                    text: this.branches[i].name
                });
            }

            this.staffs = values[1];
            let staffs = []
            for (let i in this.staffs) {
                staffs.push({
                    value: this.staffs[i].id,
                    key: this.staffs[i].id,
                    text: this.staffs[i].name
                });
            }


            this.product_types = values[2];
            let product_types = []
            for (let i in this.product_types) {
                product_types.push({
                    value: this.product_types[i].id,
                    key: this.product_types[i].id,
                    text: this.product_types[i].name
                });
            }

            this.setState({
                loader_active: false,
                branches: branches,
                staffs: staffs,
                product_types: product_types
            });
        });
        var url_string = window.location.toString();
        var url = new URL(url_string);
        var temp_date = url.searchParams.get("date_lt");
        if (temp_date !== null){
            this.setState({
                    search:{
                        date_lt: moment(temp_date,'DD/MM/YYYY'),
                    }
            })
        }
    }

    resetForm(e) {

        this.setState({
            search: {
                start_date: moment(),
                end_date: moment(),
            }
        })

    }

    handlerSubmit(e) {
        e.preventDefault();
        let search = Utility.cloneObjectJson(this.state.search)
        search.date_lt = Utility.formatDate2(search.date_lt)

        let fromData = Utility.jsonToQueryString(search)
        console.log(fromData)
        let url = Settings.baseUrl + "/stock_product/0/items?" + fromData

        this.setState({
            open: true,
            loading: true
        })
        Utility.get(url, (s, d) => {
            // alert(s)
            if (s == true) {
                this.setState({
                    loading: false,
                    items: this.setFieldValue(d)
                })
            }
        })
    }

    setFieldValue(_items) {
        let items = []
        for (let i in _items) {
            let item = _items[i]

            items.push(item)
        }
        return items
    }
    handlerInput(e, v) {
        let search = this.state.search
        search[v.name] = v.value
        this.setState({
            search: search
        })
    }
    render() {
        let data = this.props.data

        let title = 'รายงานจำนวนชิ้นขาย';
        let filename = 'sell_type_amount';

        const divStyle = {
            color: '#111',
            'font-size': '10px',
            width: '100%',
            'line-height': '20px',
            'font-family': 'monospace',
            border: '1px solid black',
            'border-collapse': 'collapse'
        };
        const textRight = {
            'text-align': 'right',
            border: '1px solid black',
        }

        const textLeft = {
            'text-align': 'left',
            border: '1px solid black',
        }
        const textCenter = {
            'text-align': 'center',
            border: '1px solid black',
        }
        const textU = {
            'text-decoration': 'underline'
        }
        let amount = 0
        //let weight = 0
        let weight_b = 0
        let sell = 0
        let net = 0

        let category = {}
        for (let i in this.state.items) {
            let item = this.state.items[i]
            if (category[item.product.category.id] == null)
                category[item.product.category.id] = {
                    items: [],
                    title: item.product.category.name,
                    id: item.product.category.id
                }

            category[item.product.category.id].items.push(item)
        }
        let items2 = []
        for (let i in category) {
            items2.push(category[i])
        }

        let sum_total = 0;
                let branch_name = ''
        const {branches} = this.props
        if (this.state.search.branch) {
            let b = Utility.getObject(branches, this.state.search.branch)
            if (b)
                branch_name = b.name
        }
        return (<div>

            <div className="box-login">
                <Segment textAlign='left' >

                    <Header size='small'>เงือนไขค้นหา</Header>
                    <Form size='small' onSubmit={this.handlerSubmit}>
                        <Form.Field>
                            <label>สาขา</label>
                            <DropdownBranch  fluid size='small' fluid selection onChange={this.handlerInput} name="branch" value={this.state.search.branch} />
                        </Form.Field>

                        <Form.Field >
                            <label>วันที่</label>
                            <DatePicker
                                dateFormat="DD/MM/YYYY"
                                value={this.state.search.date_lt}
                                selected={this.state.search.date_lt}
                                onChange={(date) => {
                                    this.handlerInput(null, {
                                        name: 'date_lt',
                                        value: date
                                    });
                                }}
                            />
                        </Form.Field>
                        <Button>พิมพ์</Button>
                        <Button type='button' onClick={this.resetForm}>รีเซ็ต</Button>
                    </Form>
                </Segment>
            </div>
            <Modal open={this.state.open} onClose={this.close} size='fullscreen' /*dimmer='blurring'*/>
                <Modal.Header>Preview</Modal.Header>
                <Modal.Content className='scrolling'>
                    {this.state.loading && <Dimmer active={this.state.loading} inverted>
                        <Loader inverted>Loading</Loader>
                    </Dimmer>}
                    <div id='view-print'>
                        <div id='paperA4-portrait'>
                            <Table basic id='table-to-xls' style={divStyle}>
                                <Table.Header>
                                    <Table.Row >
                                        <Table.HeaderCell style={textCenter} colSpan='14'><center>{title}</center></Table.HeaderCell>
                                    </Table.Row>
                                    <Table.Row >
                                        <Table.HeaderCell style={textCenter} colSpan='14'><center id='branch'>สาขา : {branch_name}</center></Table.HeaderCell>
                                    </Table.Row>
                                    <Table.Row >
                                        <Table.HeaderCell style={textCenter} colSpan='14'><center id='date'>ประจำวันที่ : {Utility.formatDate(this.state.search.date_lt)}</center></Table.HeaderCell>
                                    </Table.Row>
                                    <Table.Row>
                                        <Table.HeaderCell style={textCenter} rowspan={2}>ประเภท</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter} rowspan="2">รหัสสินค้า</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter} rowspan="2">น้ำหนัก</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter} colSpan='2'>สต็อก</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter} colSpan='2'>เพิ่ม</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter} colSpan='2'>ลด</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter} >นับ</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter} colSpan='2'>เหลือ</Table.HeaderCell>
                                    </Table.Row>
                                    <Table.Row>
                                        <Table.HeaderCell style={textCenter} >น.น.</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter} >จำนวน</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter} >น.น.</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter} >จำนวน</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter} >น.น.</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter} >จำนวน</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter} >-</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter} >น.น.</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter} >จำนวน</Table.HeaderCell>

                                    </Table.Row>
                                </Table.Header>

                                {items2.map((row, i) => {
                                    
                                    let kind = {}
                                    for(let i in row.items){
                                        let itemm = row.items[i]
                                        if(kind[itemm.product.kind.id]==null){
                                            kind[itemm.product.kind.id]={
                                                items: [],                                                
                                            }
                                        }
                                        kind[itemm.product.kind.id].items.push(itemm)
                                    }
                                    let itemss = []
                                    let product = {}
                                    for(let i in kind){
                                        for(let j in kind[i].items){
                                            let o = kind[i].items[j]
                                            if(product[o.product.id]==null){
                                                product[o.product.id]=o
                                            }
                                        }
                                    }
                                    for(let i in product){
                                        itemss.push(product[i])
                                    }

                                    let total_n = 0;
                                    let total_w = 0
                                    return (<Table.Body>
                                        <Table.Row >
                                            <Table.Cell style={textLeft} colSpan='14'>%ทอง {row.title}</Table.Cell>
                                        </Table.Row>
                                        {itemss.map((row,i)=>{
                                            let add_weight = 0
                                            let add_amount = 0
                                            let date = Utility.formatDate(row.record_date)
                                            let b_weight = 0
                                            let b_amount = 0
                                            let d_weight = 0
                                            let d_amount = 0
                                            if(date==Utility.formatDate(this.state.search.date_lt)){
                                                if(row.kind=='I'){
                                                    add_weight = row.weight_total
                                                    add_amount = row.amount

                                                    b_weight = Utility.weightFormat(row.product.weight*row.before)
                                                    b_amount = row.before
                                                }else{
                                                    b_weight = Utility.weightFormat(row.product.weight*row.before)
                                                    b_amount = row.before

                                                    d_weight = row.weight_total
                                                    d_amount = row.amount
                                                }
                                            }else{
                                                    b_weight = Utility.weightFormat(row.product.weight*row.after)
                                                    b_amount = row.after
                                                   
                                            }
                                                total_w += (row.product.weight*row.after)
                                                total_n += row.after
                                            return(<Table.Row id={row.product.code}>
                                            <Table.Cell style={textCenter} >{row.product.kind.name}</Table.Cell>
                                            <Table.Cell style={textCenter} >{row.product.code}</Table.Cell>
                                            <Table.Cell style={textCenter} >{row.product.weight}</Table.Cell>
                                            <Table.Cell style={textCenter} >{b_weight}</Table.Cell>
                                            <Table.Cell style={textCenter} >{b_amount}</Table.Cell>
                                            
                                            <Table.Cell style={textCenter} >{add_weight}</Table.Cell>
                                            <Table.Cell style={textCenter} >{add_amount}</Table.Cell>
                                            <Table.Cell style={textCenter} >{d_weight}</Table.Cell>
                                            <Table.Cell style={textCenter} >{d_amount}</Table.Cell>
                                            <Table.Cell style={textCenter} >-</Table.Cell>
                                            <Table.Cell style={textCenter} >{Utility.weightFormat(row.product.weight*row.after)}</Table.Cell>
                                            <Table.Cell style={textCenter} >{row.after}</Table.Cell>
                                        </Table.Row>)
                                        })}
                                        
                                        <Table.Row >
                                            <Table.Cell style={textRight} colSpan='10'>รวม%ทอง {row.title}</Table.Cell>
                                            <Table.Cell id={'total_w_'+row.id} style={textCenter} >{Utility.weightFormat(total_w)}</Table.Cell>
                                            <Table.Cell id={'total_n_'+row.id} style={textCenter} >{Utility.numberFormat(total_n)}</Table.Cell>
                                        </Table.Row>
                                    </Table.Body>)
                                })}

                            </Table>
                        </div>
                    </div>
                </Modal.Content>
                <Modal.Actions>
                    <ReactHTMLTableToExcel
                        id="test-table-xls-button"
                        className="ui primary button small"
                        table="table-to-xls"
                        filename={filename}
                        sheet={title}
                        buttonText="Download as XLS" >
                    </ReactHTMLTableToExcel>

                    <Button primary icon='print' size='small' onClick={() => {
                        var mywindow = window.open('', 'PRINT', 'height=600,width=1024');
                        mywindow.document.write('<html><head><title>' + title + '</title>');
                        mywindow.document.write('</head><body style="font-size: 8px;font-family: Lato,\'Helvetica Neue\',Arial,Helvetica,sans-serif">');
                        mywindow.document.write(document.getElementById('view-print').innerHTML);
                        mywindow.document.write('</body></html>');
                        mywindow.document.close(); // necessary for IE >= 10
                        mywindow.focus(); // necessary for IE >= 10*/
                        mywindow.print();
                        mywindow.close();
                    }} labelPosition='right' content='Print' />

                    <Button id='btnClosePreview' size='small' type='button' onClick={() => { this.setState({ open: false }) }}>ปิด</Button>
                </Modal.Actions>
            </Modal>
        </div>)
    }
}

const mapStateToProps = state =>{
    return ({
      auth: state.auth,
      branches: state.branches
    })
  }
  export default connect(
    mapStateToProps,
  )(PrintPreview)