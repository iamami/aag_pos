/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import {
    Form, Segment, Header, Button, Message, Loader, Dimmer, Icon, Dropdown, Input, Modal, Grid, Popup, Label, Table
} from 'semantic-ui-react';
/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import Utility from '../../../Utility';
import Settings from '../../../Settings';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import { connect } from 'react-redux'
import DropdownBranch from '../../Widget/DropDown/DropDownBranch'

class PrintPreview extends Component {
    constructor(props) {
        super(props);

        this.state = {
            search: {
                branch: props.auth.branch.id,
                start_date: moment(),
                end_date: moment(),
            },
            items: []
        }

        this.resetForm = this.resetForm.bind(this)
        this.handlerSubmit = this.handlerSubmit.bind(this)
        this.handlerInput = this.handlerInput.bind(this)
    }

    componentDidMount() {

        var branches = Utility.getFetch(Settings.baseUrl + '/branches/?is_enabled=1');
        var vendors = Utility.getFetch(Settings.baseUrl + '/vendors/?is_enabled=1');
        Promise.all([branches, vendors]).then((values) => {
            this.branches = values[0];
            let branches = []
            for (let i in this.branches) {
                branches.push({
                    value: this.branches[i].id,
                    key: this.branches[i].id,
                    text: this.branches[i].name
                });
            }

            this.vendors = values[1];
            let vendors = []
            for (let i in this.vendors) {
                vendors.push({
                    value: this.vendors[i].id,
                    key: this.vendors[i].id,
                    text: this.vendors[i].name
                });
            }

            this.setState({
                loader_active: false,
                branches: branches,
                vendors: vendors
            });
        });
        var url_string = window.location.toString();
        var url = new URL(url_string);
        var temp_start = url.searchParams.get("start_date");
        var temp_end = url.searchParams.get("end_date");
        if (temp_start !== null && temp_end !== null){
            this.setState({
                    search:{
                        start_date: moment(temp_start,'DD/MM/YYYY'),
                        end_date: moment(temp_end,'DD/MM/YYYY')
                    }
            })
        }
    }

    resetForm(e) {

        this.setState({
            search: {
                start_date: moment(),
                end_date: moment(),
            }
        })

    }

    handlerSubmit(e) {
        e.preventDefault();
        let search = Utility.cloneObjectJson(this.state.search)
        search.start_date = Utility.formatDate2(search.start_date)
        search.end_date = Utility.formatDate2(search.end_date)
        let fromData = Utility.jsonToQueryString(search)
        let url = Settings.baseUrl + "/bill/0/items/?status_stock=Y&" + fromData

        this.setState({
            open: true,
            loading: true
        })
        Utility.get(url, (s, d) => {

            if (s == true) {
                this.setState({
                    loading: false,
                    items: this.setFieldValue(d)
                })
            }
        })
    }

    setFieldValue(_items) {
        let items = []
        let group_date = {}
        for (let i in _items) {
            let item = _items[i]


            if (this.state.search.branch) {
                if (item.bill.branch.id != this.state.search.branch)
                    continue;
            }
            
            let date_title = Utility.formatDate(item.bill.bill_date)
            if(group_date[date_title]==null)
            group_date[date_title]={
                in:0,
                out: 0,
                date_title: date_title
            }
            if(item.buy==0){
                group_date[date_title].out += parseFloat(item.weight)
            }else{
                group_date[date_title].in += parseFloat(item.weight)
            }
        }

        for(let i in group_date){
            items.push(group_date[i])
        }
        return items
    }
    handlerInput(e, v) {
        let search = this.state.search
        search[v.name] = v.value
        this.setState({
            search: search
        })
    }
    render() {
        let data = this.props.data

        let title = 'รายงานทองเข้า-ทองออก';
        let filename = 'stock_in_out';

        const divStyle = {
            color: '#111',
            'font-size': '10px',
            width: '100%',
            'line-height': '20px',
            'font-family': 'monospace'
        };
        const textRight = {
            'text-align': 'right'
        }

        const textLeft = {
            'text-align': 'left'
        }
        const textCenter = {
            'text-align': 'center'
        }
        const textU = {
            'text-decoration': 'underline'
        }

        let branch_name = ''
        const {branches} = this.props
        if (this.state.search.branch) {
            let b = Utility.getObject(branches, this.state.search.branch)
            if (b)
                branch_name = b.name
        }


        let s_in = 0
        let s_out = 0
        return (<div>

            <div className="box-login">
                <Segment textAlign='left' >

                    <Header size='small'>เงือนไขค้นหา</Header>
                    <Form size='small' onSubmit={this.handlerSubmit}>
                        <Form.Field>
                            <label>สาขา</label>
                            <DropdownBranch  fluid size='small' fluid selection onChange={this.handlerInput} name="branch" value={this.state.search.branch} />
                        </Form.Field>
                        <Form.Field >
                            <label>จากวันที่</label>
                            <DatePicker
                                dateFormat="DD/MM/YYYY"
                                value={this.state.search.start_date}
                                selected={this.state.search.start_date}
                                onChange={(date) => {
                                    this.handlerInput(null, {
                                        name: 'start_date',
                                        value: date
                                    });
                                }}
                            />
                        </Form.Field>
                        <Form.Field >
                            <label>ถึงวันที่</label>
                            <DatePicker
                                dateFormat="DD/MM/YYYY"
                                value={this.state.search.end_date}
                                selected={this.state.search.end_date}
                                onChange={(date) => {
                                    this.handlerInput(null, {
                                        name: 'end_date',
                                        value: date
                                    });
                                }}
                            />
                        </Form.Field>
                        <Button>พิมพ์</Button>
                        <Button type='button' onClick={this.resetForm}>รีเซ็ต</Button>
                    </Form>
                </Segment>
            </div>
            <Modal open={this.state.open} onClose={this.close} size='fullscreen' /*dimmer='blurring'*/>
                <Modal.Header>Preview</Modal.Header>
                <Modal.Content className='scrolling'>
                    {this.state.loading && <Dimmer active={this.state.loading} inverted>
                        <Loader inverted>Loading</Loader>
                    </Dimmer>}
                    <div id='view-print'>
                        <div id='paperA4-portrait'>
                            <Table basic id='table-to-xls' style={divStyle}>
                                <Table.Header>
                                    <Table.Row >
                                        <Table.HeaderCell colSpan='10'><center>{title}</center></Table.HeaderCell>
                                    </Table.Row>
                                    <Table.Row >
                                        <Table.HeaderCell colSpan='10'><center id='branch'>สาขา : {branch_name}</center></Table.HeaderCell>
                                    </Table.Row>
                                    <Table.Row >
                                        <Table.HeaderCell colSpan='10'><center id='date'>ตั้งแต่ : {Utility.formatDate(this.state.search.start_date)} ถึง {Utility.formatDate(this.state.search.end_date)}</center></Table.HeaderCell>
                                    </Table.Row>
                                    <Table.Row>
                                        <Table.HeaderCell style={textLeft}>วันที่</Table.HeaderCell>
                                        <Table.HeaderCell style={textRight}>รายการซื้อทอง(ทองเข้า)</Table.HeaderCell>
                                        <Table.HeaderCell style={textRight}>รายการขายทอง(ทองออก)</Table.HeaderCell>
                                        <Table.HeaderCell style={textRight}>ผลต่าง</Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>
                                {this.state.items.map((row, i) => {
                                    s_in+=row.in
                                    s_out+=row.out
                                    return (<Table.Row key={i}>
                                            <Table.Cell style={textLeft}>{row.date_title}</Table.Cell>
                                            <Table.Cell style={textRight}>{Utility.weightFormat(row.in)}</Table.Cell>
                                            <Table.Cell style={textRight}>{Utility.weightFormat(row.out)}</Table.Cell>
                                            <Table.Cell style={textRight}>{Utility.weightFormat(row.in-row.out)}</Table.Cell>
                                        </Table.Row>)
                                    })}
                                    <Table.Row>
                                        <Table.Cell style={textRight}><b><div style={textU}>รวมทั้งหมด</div></b></Table.Cell>
                                        <Table.Cell style={textRight}><div style={textU}><b id='s_in'>{Utility.weightFormat(s_in)}</b></div></Table.Cell>
                                        <Table.Cell style={textRight}><div style={textU}><b id='s_out'>{Utility.weightFormat(s_out)}</b></div></Table.Cell>
                                        <Table.Cell style={textRight}><div style={textU}><b id='s_dif'>{Utility.weightFormat(s_in-s_out)}</b></div></Table.Cell>
                                    </Table.Row>

                                
                            </Table>
                        </div>
                    </div>
                </Modal.Content>
                        <Modal.Actions>
                            <ReactHTMLTableToExcel
                                id="test-table-xls-button"
                                className="ui primary button small"
                                table="table-to-xls"
                                filename={filename}
                                sheet={title}
                                buttonText="Download as XLS" >
                            </ReactHTMLTableToExcel>

                            <Button primary icon='print' size='small' onClick={() => {
                                var mywindow = window.open('', 'PRINT', 'height=600,width=1024');
                                mywindow.document.write('<html><head><title>' + title + '</title>');
                                mywindow.document.write('</head><body style="font-size: 8px;font-family: Lato,\'Helvetica Neue\',Arial,Helvetica,sans-serif">');
                                mywindow.document.write(document.getElementById('view-print').innerHTML);
                                mywindow.document.write('</body></html>');
                                mywindow.document.close(); // necessary for IE >= 10
                                mywindow.focus(); // necessary for IE >= 10*/
                                mywindow.print();
                                mywindow.close();
                            }} labelPosition='right' content='Print' />

                            <Button id='btnClosePreview' size='small' type='button' onClick={() => { this.setState({ open: false }) }}>ปิด</Button>
                        </Modal.Actions>
            </Modal>
        </div>)
    }
}

const mapStateToProps = state =>{
    return ({
      auth: state.auth,
      branches: state.branches
    })
  }
  export default connect(
    mapStateToProps,
  )(PrintPreview)