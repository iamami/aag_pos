/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import {
    Form, Segment, Header, Button, Message, Loader, Dimmer, Icon, Dropdown, Input, Modal, Grid, Popup, Label, Table
} from 'semantic-ui-react';

import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import Utility from '../../../Utility';
import Settings from '../../../Settings';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import { connect } from 'react-redux'
import DropdownBranch from '../../Widget/DropDown/DropDownBranch'

class PrintPreview extends Component {
    constructor(props) {
        super(props);

        this.state = {
            search: {
                branch: props.auth.branch.id,
                start_date: moment(),
                end_date: moment(),
            },
            items: []
        }

        this.resetForm = this.resetForm.bind(this)
        this.handlerSubmit = this.handlerSubmit.bind(this)
        this.handlerInput = this.handlerInput.bind(this)
    }

    componentDidMount() {

        var branches = Utility.getFetch(Settings.baseUrl + '/branches/?is_enabled=1');
        var vendors = Utility.getFetch(Settings.baseUrl + '/vendors/?is_enabled=1');
        Promise.all([branches, vendors]).then((values) => {
            this.branches = values[0];
            let branches = []
            for (let i in this.branches) {
                branches.push({
                    value: this.branches[i].id,
                    key: this.branches[i].id,
                    text: this.branches[i].name
                });
            }

            this.vendors = values[1];
            let vendors = []
            for (let i in this.vendors) {
                vendors.push({
                    value: this.vendors[i].id,
                    key: this.vendors[i].id,
                    text: this.vendors[i].name
                });
            }

            this.setState({
                loader_active: false,
                branches: branches,
                vendors: vendors
            });
        });
        var url_string = window.location.toString();
        var url = new URL(url_string);
        var temp_start = url.searchParams.get("start_date");
        var temp_end = url.searchParams.get("end_date");
        if (temp_start !== null && temp_end !== null){
            this.setState({
                    search:{
                        start_date: moment(temp_start,'DD/MM/YYYY'),
                        end_date: moment(temp_end,'DD/MM/YYYY')
                    }
            })
        }
    }

    resetForm(e) {

        this.setState({
            search: {
                start_date: moment(),
                end_date: moment(),
            }
        })

    }

    handlerSubmit(e) {
        e.preventDefault();
        let search = Utility.cloneObjectJson(this.state.search)
        search.start_date = Utility.formatDate2(search.start_date)
        search.end_date = Utility.formatDate2(search.end_date)

        let fromData = Utility.jsonToQueryString(search)

        let url = Settings.baseUrl + "/bill_category/0/items/?status_stock=Y&" + fromData

        this.setState({
            open: true,
            loading: true
        })
        Utility.get(url, (s, d) => {

            if (s == true) {
                this.setState({
                    loading: false,
                    items: this.setFieldValue(d)
                })
            }
        })
    }

    setFieldValue(_items) {
        let items = []
        for (let i in _items) {
            let item = _items[i]
            if (this.state.search.branch) {
                if (item.bill_category.branch != this.state.search.branch)
                    continue;
            }
            if (this.state.search.vendor) {
                if (item.bill_category.vendor != this.state.search.vendor)
                    continue;
            }
            if (item.bill_category.status_stock != 'Y')
                continue;

            items.push(item)
        }
        return items
    }
    handlerInput(e, v) {
        let search = this.state.search
        search[v.name] = v.value
        this.setState({
            search: search
        })
    }
    render() {
        let data = this.props.data

        let title = 'รายงานการตัดทองเก่า';
        let filename = 'stock_old';

        const divStyle = {
            color: '#111',
            'font-size': '10px',
            width: '100%',
            'line-height': '20px',
            'font-family': 'monospace'
        };
        const textRight = {
            'text-align': 'right'
        }

        const textLeft = {
            'text-align': 'left'
        }
        const textCenter = {
            'text-align': 'center'
        }
        const textU = {
            'text-decoration': 'underline'
        }

        let branch_name = ''
        const {branches} = this.props

        if (this.state.search.branch) {
            let b = Utility.getObject(branches, this.state.search.branch)
            if (b)
                branch_name = b.name
        }

        let date_g = {}
        for (let i in this.state.items) {
            let item = this.state.items[i]
            let date_title = Utility.formatDate(item.bill_category.update_date)
            if (date_g[date_title] == null) {
                date_g[date_title] = {
                    items: [],
                    title: date_title
                }
            }
            date_g[date_title].items.push(item)
        }

        let data_list = []
        for (let i in date_g) {
            data_list.push(date_g[i])
        }

        let t_total = 0
        let t_weight = 0
        let t_cost = 0
        let t_profit2 =0
        return (<div>

            <div className="box-login">
                <Segment textAlign='left' >

                    <Header size='small'>เงือนไขค้นหา</Header>
                    <Form size='small' onSubmit={this.handlerSubmit}>
                        <Form.Field>
                            <label>สาขา</label>
                            <DropdownBranch  fluid size='small' fluid selection onChange={this.handlerInput} name="branch" value={this.state.search.branch} />
                        </Form.Field>
                        <Form.Field >
                            <label>จากวันที่</label>
                            <DatePicker
                                dateFormat="DD/MM/YYYY"
                                value={this.state.search.start_date}
                                selected={this.state.search.start_date}
                                onChange={(date) => {
                                    this.handlerInput(null, {
                                        name: 'start_date',
                                        value: date
                                    });
                                }}
                            />
                        </Form.Field>
                        <Form.Field >
                            <label>ถึงวันที่</label>
                            <DatePicker
                                dateFormat="DD/MM/YYYY"
                                value={this.state.search.end_date}
                                selected={this.state.search.end_date}
                                onChange={(date) => {
                                    this.handlerInput(null, {
                                        name: 'end_date',
                                        value: date
                                    });
                                }}
                            />
                        </Form.Field>

                        <Form.Field>
                            <label>ซื่อโรงงาน/ร้านส่ง</label>
                            <Dropdown search selection options={this.state.vendors} value={this.state.search.vendor} name='vendor' onChange={this.handlerInput} />
                        </Form.Field>
                        <Button>พิมพ์</Button>
                        <Button type='button' onClick={this.resetForm}>รีเซ็ต</Button>
                    </Form>
                </Segment>
            </div>
            <Modal open={this.state.open} onClose={this.close} size='fullscreen' /*dimmer='blurring'*/>
                <Modal.Header>Preview</Modal.Header>
                <Modal.Content className='scrolling'>
                    {this.state.loading && <Dimmer active={this.state.loading} inverted>
                        <Loader inverted>Loading</Loader>
                    </Dimmer>}
                    <div id='view-print'>
                        <div id='paperA4-portrait'>
                            <Table basic id='table-to-xls' style={divStyle}>
                                <Table.Header>
                                    <Table.Row >
                                        <Table.HeaderCell colSpan='7'><center>{title}</center></Table.HeaderCell>
                                    </Table.Row>
                                    <Table.Row >
                                        <Table.HeaderCell colSpan='7'><center id='branch'>สาขา : {branch_name}</center></Table.HeaderCell>
                                    </Table.Row>
                                    <Table.Row >
                                        <Table.HeaderCell colSpan='7'><center id='date'>ตั้งแต่ : {Utility.formatDate(this.state.search.start_date)} ถึง {Utility.formatDate(this.state.search.end_date)}</center></Table.HeaderCell>
                                    </Table.Row>
                                    <Table.Row>
                                        <Table.HeaderCell style={textLeft}>วันที่</Table.HeaderCell>
                                        <Table.HeaderCell style={textLeft}>เลขที่</Table.HeaderCell>
                                        <Table.HeaderCell style={textLeft}>%ทอง</Table.HeaderCell>
                                        <Table.HeaderCell style={textRight}>น.น.ทองเก่า</Table.HeaderCell>
                                        <Table.HeaderCell style={textRight}>ราคารวม</Table.HeaderCell>
                                        <Table.HeaderCell style={textRight}>ค่าหลอม</Table.HeaderCell>
                                        <Table.HeaderCell style={textRight}>กำไรรวม</Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>
                                {data_list.map((row2, i) => {

                                    let total = 0
                                    let weight = 0
                                    let cost = 0
                                    let profit2 =0
                                    return (<Table.Body>
                                        {row2.items.map((row, i) => {

                                            let bath = parseFloat(row.weight)/row.stock.category.weight
                                            let cost_total = row.cost*bath
                                            let profit = (parseFloat(row.price)*bath) -((parseFloat(row.average)*bath)+ row.cost*bath)

                                            weight+=parseFloat(row.weight)
                                            total+=parseFloat(row.total)
                                            cost+=parseFloat(row.cost)
                                            profit2+=profit

                                            t_weight+=parseFloat(row.weight)
                                            t_total+=parseFloat(row.total)
                                            t_cost+=parseFloat(row.cost)
                                            t_profit2+=profit
                                            return (<Table.Row key={i}>
                                            <Table.Cell style={textLeft}>{row2.title}</Table.Cell>
                                            <Table.Cell style={textLeft}>{row.bill_category.bill_number}</Table.Cell>
                                            <Table.Cell style={textLeft}>{row.stock.category.name}</Table.Cell>
                                            <Table.Cell style={textRight}>{Utility.weightFormat(row.weight)}</Table.Cell>
                                            <Table.Cell style={textRight}>{Utility.weightFormat(parseFloat(row.total))}</Table.Cell>
                                            <Table.Cell style={textRight}>{Utility.priceFormat(parseFloat(row.cost))}</Table.Cell>
                                            <Table.Cell style={textRight}>{Utility.priceFormat(parseFloat(profit))}</Table.Cell>
                                        </Table.Row>)
                                        })}
                                        <Table.Row>
                                        <Table.Cell colSpan='3' style={textRight}><div style={textU}>รวม</div></Table.Cell>
                                        <Table.Cell style={textRight}><div style={textU}>{Utility.weightFormat(weight)}</div></Table.Cell>
                                        <Table.Cell style={textRight}><div style={textU}>{Utility.priceFormat(total)}</div></Table.Cell>
                                        <Table.Cell style={textRight}><div style={textU}>{Utility.priceFormat(cost)}</div></Table.Cell>
                                        <Table.Cell style={textRight}><div style={textU}>{Utility.priceFormat(profit2)}</div></Table.Cell>
                                    </Table.Row>
                                    </Table.Body>)
                                    })}
                                    <Table.Row>
                                    <Table.Cell colSpan='3' style={textRight}><div style={textU}>รวมทั้งหมด</div></Table.Cell>
                                    <Table.Cell style={textRight}><div id='t_weight' style={textU}>{Utility.weightFormat(t_weight)}</div></Table.Cell>
                                    <Table.Cell style={textRight}><div id='t_total' style={textU}>{Utility.priceFormat(t_total)}</div></Table.Cell>
                                    <Table.Cell style={textRight}><div id='t_cost' style={textU}>{Utility.priceFormat(t_cost)}</div></Table.Cell>
                                    <Table.Cell style={textRight}><div id='t_profit' style={textU}>{Utility.priceFormat(t_profit2)}</div></Table.Cell>
                                </Table.Row>

                                
                            </Table>
                        </div>
                    </div>
                </Modal.Content>
                        <Modal.Actions>
                            <ReactHTMLTableToExcel
                                id="test-table-xls-button"
                                className="ui primary button small"
                                table="table-to-xls"
                                filename={filename}
                                sheet={title}
                                buttonText="Download as XLS" >
                            </ReactHTMLTableToExcel>

                            <Button primary icon='print' size='small' onClick={() => {
                                var mywindow = window.open('', 'PRINT', 'height=600,width=1024');
                                mywindow.document.write('<html><head><title>' + title + '</title>');
                                mywindow.document.write('</head><body style="font-size: 8px;font-family: Lato,\'Helvetica Neue\',Arial,Helvetica,sans-serif">');
                                mywindow.document.write(document.getElementById('view-print').innerHTML);
                                mywindow.document.write('</body></html>');
                                mywindow.document.close(); // necessary for IE >= 10
                                mywindow.focus(); // necessary for IE >= 10*/
                                mywindow.print();
                                mywindow.close();
                            }} labelPosition='right' content='Print' />

                            <Button id='btnClosePreview' size='small' type='button' onClick={() => { this.setState({ open: false }) }}>ปิด</Button>
                        </Modal.Actions>
            </Modal>
        </div>)
    }
}

const mapStateToProps = state =>{
    return ({
      auth: state.auth,
      branches: state.branches
    })
  }
  export default connect(
    mapStateToProps,
  )(PrintPreview)