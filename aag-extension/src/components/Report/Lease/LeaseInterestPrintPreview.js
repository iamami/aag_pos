/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import {
    Form, Segment, Header, Button, Message, Loader, Dimmer, Icon, Dropdown, Input, Modal, Grid, Popup, Label, Table, Checkbox, Divider
} from 'semantic-ui-react';

import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import Utility from '../../../Utility';
import Settings from '../../../Settings';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import { connect } from 'react-redux'
import DropdownBranch from '../../Widget/DropDown/DropDownBranch'

class PrintPreview extends Component {
    constructor(props) {
        super(props);

        
        this.state = {
            is_all: true,
            is_sort: true,
            search: {
                branch: props.auth.branch.id,
                start_date: moment(),
                end_date: moment(),
            },
            items: []
        }

        this.resetForm = this.resetForm.bind(this)
        this.handlerSubmit = this.handlerSubmit.bind(this)
        this.handlerInput = this.handlerInput.bind(this)
    }

    componentDidMount() {

        var branches = Utility.getFetch(Settings.baseUrl + '/branches/?is_enabled=1');
       
        Promise.all([branches]).then((values) => {
            this.branches = values[0];
            let branches = []
            for (let i in this.branches) {
                branches.push({
                    value: this.branches[i].id,
                    key: this.branches[i].id,
                    text: this.branches[i].name
                });
            }
           
            this.setState({
                loader_active: false,
                branches: branches,
            });
        });
        var url_string = window.location.toString();
        var url = new URL(url_string);
        var temp_start = url.searchParams.get("start_date");
        var temp_end = url.searchParams.get("end_date");
        if (temp_start !== null && temp_end !== null){
            this.setState({
                    search:{
                        start_date: moment(temp_start,'DD/MM/YYYY'),
                        end_date: moment(temp_end,'DD/MM/YYYY')
                    }
            })
        }
    }

    resetForm(e) {

        this.setState({
            search: {
                start_date: moment(),
                end_date: moment(),
            }
        })

    }

    handlerSubmit(e) {
        e.preventDefault();
        let search = Utility.cloneObjectJson(this.state.search)
        search.ledger_category = 7
        search.start_date = Utility.formatDate2(search.start_date)
        search.end_date = Utility.formatDate2(search.end_date)
        if (!this.state.is_sort)
            search.sort = 'date'
        else
            search.sort = 'number'
        let fromData = Utility.jsonToQueryString(search)
        let url = Settings.baseUrl + "/lease/interest/report/?" + fromData

        this.setState({
            loading: true
        })
        Utility.get(url, (s, d) => {
            console.log('lease/interest/report',d,s)
            if (s == true) {
                this.setState({
                    loading: false,
                    items: this.setFieldValue(d)
                })
            }
        })
    }

    setFieldValue(_items) {
        let items = []
        for (let i in _items) {
            let item = _items[i]
            items.push(item)
        }

        return items
    }
    handlerInput(e, v) {
        let search = this.state.search
        search[v.name] = v.value
        this.setState({
            search: search
        })
    }
    render() {
        let data = this.props.data

        let title = 'รายงานยอดดอกเบีย(ต่อดอก)';
        let filename = 'lease-interest-' + Utility.formatDate3(this.state.search.start_date) + '-' + Utility.formatDate3(this.state.search.end_date);

        const divStyle = {
            color: '#111',
            'font-size': '10px',
            width: '100%',
            'line-height': '20px',
            'font-family': 'monospace',
        };
        const textRight = {
            'text-align': 'right',
        }

        const textLeft = {
            'text-align': 'left',
        }
        const textCenter = {
            'text-align': 'center',
        }
        const textU = {
            'text-decoration': 'underline'
        }

                let branch_name = ''
        const {branches} = this.props
        if (this.state.search.branch) {
            let b = Utility.getObject(branches, this.state.search.branch)
            if (b)
                branch_name = b.name
        }


        let weight = 0
        let amount = 0
        let total = 0
        let total_receive = 0
        return (<div>

            <div className="box-login">
                <Segment textAlign='left' >

                    <Header size='small'>เงือนไขค้นหา</Header>
                    <Form size='small' onSubmit={this.handlerSubmit}>
                        <Form.Field>
                            <label>สาขา</label>
                            <DropdownBranch  fluid size='small' fluid selection onChange={this.handlerInput} name="branch" value={this.state.search.branch} />
                        </Form.Field>
                        <Form.Field >
                            <label>จากวันที่</label>
                            <DatePicker
                                dateFormat="DD/MM/YYYY"
                                value={this.state.search.start_date}
                                selected={this.state.search.start_date}
                                onChange={(date) => {
                                    this.handlerInput(null, {
                                        name: 'start_date',
                                        value: date
                                    });
                                }}
                            />
                        </Form.Field>
                        <Form.Field >
                            <label>ถึงวันที่</label>
                            <DatePicker
                                dateFormat="DD/MM/YYYY"
                                value={this.state.search.end_date}
                                selected={this.state.search.end_date}
                                onChange={(date) => {
                                    this.handlerInput(null, {
                                        name: 'end_date',
                                        value: date
                                    });
                                }}
                            />
                        </Form.Field>

                        <Divider />
                        <Checkbox radio label='เรียงลำดับตามเลขที่ขายฝาก' checked={this.state.is_sort} onChange={(e, v) => { this.setState({ is_sort: true }) }} />
                        <br />
                        <Checkbox radio label='เรียงลำดับตามวันที่,เลขที่ขายฝาก' checked={!this.state.is_sort} onChange={(e, v) => { this.setState({ is_sort: false }) }} />
                        <br />
                        <br />
                        <Button type='button' onClick={(e) => {
                            this.setState({
                                open: true
                            })
                            this.handlerSubmit(e)
                        }}>พิมพ์</Button>
                        <Button type='button' onClick={this.resetForm}>รีเซ็ต</Button>
                    </Form>
                </Segment>
            </div>
            <Modal open={this.state.open} onClose={this.close} size='fullscreen' /*dimmer='blurring'*/>
                <Modal.Header>Preview</Modal.Header>
                <Modal.Content className='scrolling'>
                    {this.state.loading && <Dimmer active={this.state.loading} inverted>
                        <Loader inverted>Loading</Loader>
                    </Dimmer>}
                    <div id='view-print'>
                        <div id='paperA4-portrait'>
                            <Table basic id='table-to-xls' style={divStyle}>
                                <Table.Header>
                                    <Table.Row >
                                        <Table.HeaderCell colSpan='10'><center>{title}</center></Table.HeaderCell>
                                    </Table.Row>
                                    <Table.Row >
                                        <Table.HeaderCell colSpan='10'><center id='branch'>สาขา : {branch_name}</center></Table.HeaderCell>
                                    </Table.Row>
                                    <Table.Row >
                                        <Table.HeaderCell colSpan='10'><center id='date'>ตั้งแต่วันที่ : {Utility.formatDate(this.state.search.start_date)} ถึง {Utility.formatDate(this.state.search.end_date)}</center></Table.HeaderCell>
                                    </Table.Row>
                                    <Table.Row>
                                        <Table.HeaderCell style={textCenter}>ลำดับ</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter}>เลขที่ขายฝาก</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter}>วันที่ต่อ</Table.HeaderCell>
                                        <Table.HeaderCell style={textLeft}>ชื่อลูกค้า</Table.HeaderCell>
                                        <Table.HeaderCell style={textLeft}>รายการสินค้า</Table.HeaderCell>
                                        <Table.HeaderCell style={textRight}>นำ้หนัก</Table.HeaderCell>
                                        <Table.HeaderCell style={textRight}>เงินต้น</Table.HeaderCell>
                                        <Table.HeaderCell style={textRight}>ดอกเบี้ยคำนวณ</Table.HeaderCell>
                                        <Table.HeaderCell style={textRight}>ดอกเบี้ย</Table.HeaderCell>
                                        <Table.HeaderCell style={textLeft}>หมายเหตุ</Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>
                                <Table.Body>
                                    {this.state.items.map((item, i) => {

                                        weight += parseFloat(item.data.lease.weight)
                                        amount += parseInt(item.data.lease.amount)
                                        total_receive += parseInt(item.data.total_receive)
                                        total += parseInt(item.data.total)
                                        return (<Table.Row>
                                            <Table.Cell style={textCenter}>{i + 1}</Table.Cell>
                                            <Table.Cell style={textCenter}>{item.data.lease.number}</Table.Cell>
                                            <Table.Cell style={textCenter}>{Utility.formatDate(item.data.record_date)}</Table.Cell>
                                            <Table.Cell style={textLeft}>{item.data.lease.customer.name}</Table.Cell>
                                            <Table.Cell style={textLeft}>{item.name}</Table.Cell>
                                            <Table.Cell style={textRight}>{Utility.weightFormat(item.data.lease.weight)}</Table.Cell>
                                            <Table.Cell style={textRight}>{Utility.priceFormat(item.data.lease.amount)}</Table.Cell>
                                            <Table.Cell style={textRight}>{Utility.priceFormat(item.data.total)}</Table.Cell>
                                            <Table.Cell style={textRight}>{Utility.priceFormat(item.data.total_receive)}</Table.Cell>
                                            <Table.Cell style={textLeft}>{item.data.description}</Table.Cell>
                                        </Table.Row>)
                                    })}

                                    <Table.Row>
                                        <Table.Cell style={textRight} colSpan='5'><div style={textU}><b>ยอดรวม</b></div></Table.Cell>
                                        <Table.Cell style={textRight}><div style={textU}><b id='weight'>{Utility.weightFormat(weight)}</b></div></Table.Cell>
                                        <Table.Cell style={textRight}><div style={textU}><b id='amount'>{Utility.priceFormat(amount)}</b></div></Table.Cell>
                                        <Table.Cell style={textRight}><div style={textU}><b id='total'>{Utility.priceFormat(total)}</b></div></Table.Cell>
                                        <Table.Cell style={textRight}><div style={textU}><b id='total_receive'>{Utility.weightFormat(total_receive)}</b></div></Table.Cell>
                                        <Table.Cell style={textLeft}></Table.Cell>
                                    </Table.Row>
                                </Table.Body>
                            </Table>
                        </div>
                    </div>
                </Modal.Content>
                <Modal.Actions>
                    <ReactHTMLTableToExcel
                        id="test-table-xls-button"
                        className="ui primary button small"
                        table="table-to-xls"
                        filename={filename}
                        sheet={title}
                        buttonText="Download as XLS" >
                    </ReactHTMLTableToExcel>

                    <Button primary icon='print' size='small' onClick={() => {
                        var mywindow = window.open('', 'PRINT', 'height=600,width=1024');
                        mywindow.document.write('<html><head><title>' + title + '</title>');
                        mywindow.document.write('</head><body style="font-size: 8px;font-family: Lato,\'Helvetica Neue\',Arial,Helvetica,sans-serif">');
                        mywindow.document.write(document.getElementById('view-print').innerHTML);
                        mywindow.document.write('</body></html>');
                        mywindow.document.close(); // necessary for IE >= 10
                        mywindow.focus(); // necessary for IE >= 10*/
                        mywindow.print();
                        mywindow.close();
                    }} labelPosition='right' content='Print' />

                    <Button id='btnClosePreview' size='small' type='button' onClick={() => { this.setState({ open: false }) }}>ปิด</Button>
                </Modal.Actions>
            </Modal>


        </div>)
    }
}

const mapStateToProps = state =>{
    return ({
      auth: state.auth,
      branches: state.branches
    })
  }
  export default connect(
    mapStateToProps,
  )(PrintPreview)