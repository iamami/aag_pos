/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import {
    Form, Segment, Header, Button, Message, Loader, Dimmer, Icon, Dropdown, Input, Modal, Grid, Popup, Label, Table
} from 'semantic-ui-react';

import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import Utility from '../../../Utility';
import Settings from '../../../Settings';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import InputSearchCustomer from '../../Customer/InputSearchCustomer';
import { connect } from 'react-redux'
import DropdownBranch from '../../Widget/DropDown/DropDownBranch'

class PrintPreview extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search: {
                branch: props.auth.branch.id,
                start_date: moment(),
                end_date: moment(),
            },
            items: []
        }
        this.resetForm = this.resetForm.bind(this)
        this.handlerSubmit = this.handlerSubmit.bind(this)
        this.handlerInput = this.handlerInput.bind(this)
    }

    componentDidMount() {
        var branches = Utility.getFetch(Settings.baseUrl + '/branches/?is_enabled=1');
        var staffs = Utility.getFetch(Settings.baseUrl + '/staffs/?is_enabled=1');
        var product_types = Utility.getFetch(Settings.baseUrl + '/product_types/?is_enabled=1');
        Promise.all([branches, staffs,  product_types]).then((values) => {
            this.branches = values[0];
            let branches = []
            for (let i in this.branches) {
                branches.push({
                    value: this.branches[i].id,
                    key: this.branches[i].id,
                    text: this.branches[i].name
                });
            }

            this.staffs = values[1];
            let staffs = []
            for (let i in this.staffs) {
                staffs.push({
                    value: this.staffs[i].id,
                    key: this.staffs[i].id,
                    text: this.staffs[i].name
                });
            }
 
            this.product_types = values[2];
            let product_types = []
            for (let i in this.product_types) {
                product_types.push({
                    value: this.product_types[i].id,
                    key: this.product_types[i].id,
                    text: this.product_types[i].name
                });
            }

            this.setState({
                loader_active: false,
                branches: branches,
                staffs: staffs,
                product_types: product_types
            });
        });
        var url_string = window.location.toString();
        var url = new URL(url_string);
        var temp_start = url.searchParams.get("start_date");
        var temp_end = url.searchParams.get("end_date");
        if (temp_start !== null && temp_end !== null){
            this.setState({
                    search:{
                        start_date: moment(temp_start,'DD/MM/YYYY'),
                        end_date: moment(temp_end,'DD/MM/YYYY')
                    }
            })
        }
    }

    resetForm(e) {
        this.setState({
            search:{
                start_date: moment(),
                end_date: moment(),
            }
        })
    }

    handlerSubmit(e) {
        e.preventDefault();
        let search = Utility.cloneObjectJson(this.state.search)
        search.start_date = Utility.formatDate2(search.start_date)
        search.end_date = Utility.formatDate2(search.end_date)

        const fromData = Utility.jsonToQueryString(search)
        this.setState({
            open: true,
            loading: true
        })
        var ledger = Utility.getFetch(Settings.baseUrl + '/ledger/?is_enabled=1&' + fromData);
        var bills = Utility.getFetch(Settings.baseUrl + "/bill_items/?kind=SE,XC&" + fromData)

        Promise.all([bills, ledger]).then((values) => {

            this.ledger = values[1]
            var state = {
                loading: false,
                items: this.setFieldValue(values[0])
            }
            this.setState(state)
        });
    }


  getObjectledger(bill_id) {
    for (let i = 0; i < this.ledger.length; i++) {
      if (this.ledger[i].object_id == bill_id) {
        return this.ledger[i]
      }
    }
  }

    setFieldValue(_items) {
        let items = []
        let bills = {}
        for (let i in _items) {
            let item = _items[i]
            let ledger = this.getObjectledger(item.bill.id)
            if(!ledger){
                continue
            }

            if(this.state.search.customer_id){
                if (item.bill.customer.id != this.state.search.customer_id)
                continue;
            }

            if(bills[item.bill.id]==null)
                bills[item.bill.id] = {
                    bill: item.bill,
                    ledger: ledger,
                    items: []
                }
            bills[item.bill.id].items.push(item)
        }
        items = []
        for(let b in bills)
            items.push(bills[b])
        return items
    }
    handlerInput(e, v) {
        let search = this.state.search
        search[v.name] = v.value
        this.setState({
            search: search
        })
    }
    render() {
        let data = this.props.data

        let title = 'รายงานยอดขาย';
        let filename = 'sales';

        const divStyle = {
            color: '#111',
            'font-size': '10px',
            width: '100%',
            'line-height': '20px',
            'font-family': 'monospace'
        };
        const textRight = {
            'text-align': 'right'
        }

        const textLeft = {
            'text-align': 'left'
        }
        const textCenter = {
            'text-align': 'center'
        }
        const textU = {
            'text-decoration': 'underline'
        }
        let total_cash = 0
        let total_card = 0
        let total_check = 0
        for(let i in this.state.items){
            total_cash+= parseFloat(this.state.items[i].ledger.cash)
            total_card+= parseFloat(this.state.items[i].ledger.card) + parseFloat(this.state.items[i].ledger.card_service)
            total_check+= parseFloat(this.state.items[i].ledger.check)
        }

        let branch_name = ''
        if(this.state.search.branch && this.state.branches!=null){
            let b = Utility.getObjectByValue(this.state.branches,this.state.search.branch)
            if(b)
                branch_name = b.text
        }

        return (<div>
            <div className="box-login">
                <Segment textAlign='left' >

                    <Header size='small'>เงือนไขค้นหา</Header>
                    <Form size='small' onSubmit={this.handlerSubmit}>
                        <Form.Field>
                            <label>สาขา</label>
                            <DropdownBranch  fluid size='small' fluid selection onChange={this.handlerInput} name="branch" value={this.state.search.branch} />
                        </Form.Field>
                        <Form.Field>
                            <label>พนักงาน</label>
                            <Dropdown search selection options={this.state.staffs} value={this.state.search.staff} name='staff' onChange={this.handlerInput} />
                        </Form.Field>
                        <Form.Field>
                            <label>ลูกค้า</label>
                            <InputSearchCustomer placeholder='ลูกค้า' width={16} onChange={this.handlerInput} name="customer_id" value={this.state.search.customer_id} />
                        </Form.Field>
                        <Form.Field>
                            <label>ประเภท</label>
                            <Dropdown search selection options={this.state.product_types} value={this.state.search.product_type} name='product_type' onChange={this.handlerInput} />
                        </Form.Field>
                        <Form.Field >
                            <label>จากวันที่</label>
                            <DatePicker
                                dateFormat="DD/MM/YYYY"
                                value={this.state.search.start_date}
                                selected={this.state.search.start_date}
                                onChange={(date) => {
                                    this.handlerInput(null, {
                                        name: 'start_date',
                                        value: date
                                    });
                                }}
                            />
                        </Form.Field>
                        <Form.Field >
                            <label>ถึงวันที่</label>
                            <DatePicker
                                dateFormat="DD/MM/YYYY"
                                value={this.state.search.end_date}
                                selected={this.state.search.end_date}
                                onChange={(date) => {
                                    this.handlerInput(null, {
                                        name: 'end_date',
                                        value: date
                                    });
                                }}
                            />
                        </Form.Field>
                        <Button>พิมพ์</Button>
                        <Button type='button' onClick={this.resetForm}>รีเซ็ต</Button>
                    </Form>
                </Segment>
            </div>
            <Modal open={this.state.open} onClose={this.close} size='fullscreen' /*dimmer='blurring'*/>
                <Modal.Header>Preview</Modal.Header>
                <Modal.Content className='scrolling'>
                    {this.state.loading && <Dimmer active={this.state.loading} inverted>
                        <Loader inverted>Loading</Loader>
                    </Dimmer>}
                    <div id='view-print'>
                        <div id='paperA4-portrait'>
                            <Table basic id='table-to-xls' style={divStyle}>
                                <Table.Header>
                                    <Table.Row >
                                        <Table.HeaderCell colSpan='13'><center>{title}</center></Table.HeaderCell>
                                    </Table.Row>
                                    <Table.Row >
                                        <Table.HeaderCell colSpan='13'><center id='branch'>สาขา : {branch_name}</center></Table.HeaderCell>
                                    </Table.Row>
                                    <Table.Row >
                                        <Table.HeaderCell colSpan='13'><center id='date'>ตั้งแต่ : {Utility.formatDate(this.state.search.start_date)} ถึง {Utility.formatDate(this.state.search.end_date)}</center></Table.HeaderCell>
                                    </Table.Row> 
                                    <Table.Row>
                                        <Table.HeaderCell style={textCenter}>ลำดับ</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter}>วันที่</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter}>เวลา</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter}>เลขที่บิล</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter}>ซื่อลูกค้า</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter}>รายการ</Table.HeaderCell>
                                        <Table.HeaderCell style={textRight}>รับเงินสด</Table.HeaderCell>
                                        <Table.HeaderCell style={textRight}>รับเครดิต+ค่าธรรมเนียม</Table.HeaderCell>
                                        <Table.HeaderCell style={textRight}>รับเช็ค</Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>
                                {this.state.items.map((row, i) => {
                                    return(
                                        <Table.Body>
                                            <Table.Row key={i}>
                                                <Table.Cell >{i+1}</Table.Cell>
                                                <Table.Cell style={textCenter}>{Utility.formatDate(row.bill.record_date)}</Table.Cell>
                                                <Table.Cell style={textCenter}>{Utility.formatTime(row.bill.record_date)}</Table.Cell>
                                                <Table.Cell style={textCenter}>{row.bill.bill_number}</Table.Cell>
                                                <Table.Cell style={textCenter}>{row.bill.customer.name}</Table.Cell>
                                                <Table.Cell style={textCenter}>{row.bill.kind_display}</Table.Cell>
                                                <Table.Cell style={textRight}>{Utility.priceFormat(row.ledger.cash)}</Table.Cell>
                                                <Table.Cell style={textRight} >{Utility.priceFormat(parseFloat(row.ledger.card) + parseFloat(row.ledger.card_service))}</Table.Cell>
                                                <Table.Cell style={textRight}>{Utility.priceFormat(row.ledger.check)}</Table.Cell>
                                            </Table.Row>
                                        </Table.Body>)}
                                )}

                                <Table.Footer>
                                    <Table.Row >
                                        <Table.Cell colSpan='6' style={textRight}><div style={textU}>รวมทั้งหมด</div></Table.Cell>
                                        <Table.HeaderCell style={textRight}><div id='total_cash' style={textU}>{Utility.priceFormat(total_cash)}</div></Table.HeaderCell>
                                        <Table.HeaderCell style={textRight}><div id='total_card' style={textU}>{Utility.priceFormat(total_card)}</div></Table.HeaderCell>
                                        <Table.HeaderCell style={textRight}><div id='total_check' style={textU}>{Utility.priceFormat(total_check)}</div></Table.HeaderCell>
                                    </Table.Row>
                                </Table.Footer>
                            </Table>
                        </div>
                    </div>
                </Modal.Content>
                <Modal.Actions>
                    <ReactHTMLTableToExcel
                        id="test-table-xls-button"
                        className="ui primary button small"
                        table="table-to-xls"
                        filename={filename}
                        sheet={title}
                        buttonText="Download as XLS" >
                    </ReactHTMLTableToExcel>

                    <Button primary icon='print' size='small' onClick={() => {
                        var mywindow = window.open('', 'PRINT', 'height=600,width=1024');
                        mywindow.document.write('<html><head><title>' + title + '</title>');
                        mywindow.document.write('</head><body style="font-size: 8px;font-family: Lato,\'Helvetica Neue\',Arial,Helvetica,sans-serif">');
                        mywindow.document.write(document.getElementById('view-print').innerHTML);
                        mywindow.document.write('</body></html>');
                        mywindow.document.close(); // necessary for IE >= 10
                        mywindow.focus(); // necessary for IE >= 10*/
                        mywindow.print();
                        mywindow.close();
                    }} labelPosition='right' content='Print' />

                    <Button id='btnClosePreview' size='small' type='button' onClick={() => { this.setState({ open: false }) }}>ปิด</Button>
                </Modal.Actions>
            </Modal>
        </div>)
    }
}

const mapStateToProps = state =>{
    return ({
      auth: state.auth,
      branches: state.branches
    })
  }
  export default connect(
    mapStateToProps,
  )(PrintPreview)