/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import Settings from '../../Settings';
import Utility from '../../Utility';
import MsgInput from '../Error/MsgInput'

import {
  Form, Input, Dropdown, Button, Icon, Label, Modal, Confirm, Dimmer, Loader, Header
} from 'semantic-ui-react';

import {
  Table,
  Column,
  Cell
} from 'fixed-data-table';
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";
function collect(props) {
  return { positon: props.positon };
}

class ItemsCell extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { rowIndex, field, data, ...props } = this.props;
    return (
      <ContextMenuTrigger id="menu_lese_list"
        holdToDisplay={1000}
        key={rowIndex}
        positon={rowIndex}
        collect={collect}>
        <Cell {...props} >
          <div className={this.props.textAlign}>{data[rowIndex][field]}</div>
        </Cell>
      </ContextMenuTrigger>
    );
  }
}

class OptionItemsCell extends Component {
  constructor(props) {
    super(props);

    this.state = {}
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e, v) {

  }

  render() {
    const { rowIndex, field, data, ...props } = this.props;
    return (
      <Cell><center style={{ padding: '4px' }}>
        <a id={rowIndex+"_btnEdit"} onClick={(e) => { this.props.onClickMenu(e, { action: 'edit', positon: rowIndex }) }}><Icon name='edit' /></a>
        <a id={rowIndex+"_btnDelete"} onClick={(e) => { this.props.onClickMenu(e, { action: 'delete', positon: rowIndex }) }} ><Icon name='window close outline' /></a>
      </center></Cell>
    );
  }
}


var timeout = null;

class Users extends Component {

  constructor(props) {
    super(props);
    this.state = {
      items: [],
      username: '',
      password: '',
      first_name: '',
      is_active: true,
      modal_open: false,
      search: {},
      msg_error: {}
    }
    this.submitData = this.submitData.bind(this);
    this.handlerSearch = this.handlerSearch.bind(this);
    this.handlerSubmitSearch = this.handlerSubmitSearch.bind(this);
    this.resetForm = this.resetForm.bind(this);
    this.handleClick = this.handleClick.bind(this)
  }

  checkUser(username) {
    this.setState({ username_loading: true, username_error: false });
    if (username != null && username !== '') {
      Utility.get(Settings.baseUrl + '/users/?username=' + username, (status, data) => {
        this.setState({ username_loading: false });
        let msg_error = this.state.msg_error
        msg_error['username'] = data.length > 0 ? '*ชื้อผู้ใช้ซ้ำ' : null

        this.setState({
          msg_error: msg_error,
          button_class: ''
        });
      });
    }
  }

  submitData() {

    let msg_error = {}

    if (this.state.branch == null || this.state.branch == '') {
      msg_error['branch'] = ['*กรุณาเลือกสาขา']

    }

    if (this.state.modal_action == 'add' || this.state.password == '' || this.state.password == undefined || this.state.password == null) {
      if (this.state.password == '') {
        msg_error['password'] = ['*กรุณาตั้งรหัสผ่าน']
      }
      if (this.state.password.length>0 && this.state.password.length<4) {
        msg_error['password'] = ['ความยาวอย่างน้อย 4 ตัวอักษร']
      }
      if (this.state.password2 == '' || this.state.password2 == undefined || this.state.password2 == null ) {
        msg_error['password2'] = ['*กรุณายืนยันรหัสผ่าน']
      }
      if (this.state.password2.length>0 && (this.state.password2 !== this.state.password)) {
        msg_error['password2'] = ['*กรุณายืนยันรหัสผ่านให้ตรงกัน']
      }
    }

    if (this.state.username == null || this.state.username == '') {
      msg_error['username'] = ['*กรุณาตั้งชื่อผู้ใช้งาน']
    }

    var is_superuser = 0;
    if (this.state.role == null || this.state.role == '') {
      msg_error['role'] = ['*กรุณาเลือกระดับผู้ใช้งาน']

    } else {
      is_superuser = this.state.role == 'a' ? 1 : 0;
    }

    if (Object.keys(msg_error).length > 0) {
      this.setState({
        msg_error: msg_error
      })
      return;
    }

    var user_profile = {
      'user.username': this.state.username,
      'user.is_superuser': is_superuser,
      'user.is_active': this.state.is_active,
      role: this.state.role,
      branch: this.state.branch,
      staff: this.state.staff == '' || this.state.staff == 0 ? null : this.state.staff
    };

    if(this.state.password!='')
      user_profile['user.password'] = this.state.password

    this.setState({ button_class: 'loading' })

    if (this.state.modal_action == 'add') {

          // create user profile
          const url = Settings.baseUrl + '/userprofiles/';
          Utility.post(url, user_profile, (status, data) => {
            if (status) {
              this.setState({
                modal_open: false
              });
              this.componentDidMount();
              this.resetForm();
            }
          });
    } else {
          // create user profile
          const url = Settings.baseUrl + '/userprofiles/' + this.state.object_id + "/";

          Utility.put(url, user_profile, (status, data) => {
            if (status) {
              this.setState({
                modal_open: false
              });
              this.componentDidMount();
              this.resetForm();
            }
          });
    }
  }

  resetForm() {
    this.setState({
      username: '',
      password: '',
      password2: '',
      branch: '',
      role: '',
      staff: '',
      search: {},
      msg_error: {},
      button_class: ''
    });

    this.componentDidMount()
  }

  componentDidMount() {

    this.setState({ loader_active: true });
    var userprofiles = Utility.getFetch(Settings.baseUrl + '/userprofiles/?is_active=1');
    var branches = Utility.getFetch(Settings.baseUrl + '/branches/?is_enabled=1');
    var staffs = Utility.getFetch(Settings.baseUrl + '/staffs/?is_enabled=1');
    var combinedData = { "apiRequest1": {}, "apiRequest2": {}, "apiRequest3": {} };
    Promise.all([ userprofiles, branches, staffs]).then((values) => {

      let branch_option = []
      this.staffs = values[2]

      for(let i in values[1]){
        branch_option.push({
          key: values[1][i].id,
            value: values[1][i].id,
            text: values[1][i].name,
        })
      }

      this.setState({
        branch_option: branch_option,
        loader_active: false,
        items: this.setFieldValue(values[0])
      });
      this.setStaffOption()
      return combinedData;
    });

  }

  setStaffOption(branch) {
    var options_staff = [{
      key: 0,
            value: 0,
            text: '-ไม่เลือก-',
    }];
    branch = branch ? branch : this.state.branch
    for (let i in this.staffs) {
      if (branch) {
        if (branch == this.staffs[i].branch.id)
          options_staff.push({
            key: this.staffs[i].id,
            value: this.staffs[i].id,
            text: this.staffs[i].name,
          });
      } else {
        return
      }
    }

    this.setState({
      options_staff: options_staff
    })
  }

  setFieldValue(v) {

    for (let i in v) {
      v[i].branch_name = v[i].branch.name
      v[i].username = v[i].user.username
      v[i].staff_name = v[i].staff!=null ?v[i].staff.name:''
      v[i].role_name = v[i].role=='A' ?'Admin':'User'
    }
    return v;
  }

  getBranch(user_id) {
    var obj = {};
    if (this.object_userprofiles[user_id] != null) {
      obj = this.object_userprofiles[user_id].branch;
    }
    return obj;
  }

  getUserProfile(user_id) {

    var obj = {};
    if (this.object_userprofiles[user_id] != null) {
      obj = this.object_userprofiles[user_id];
    }
    return obj;
  }

  componentWillMount() {
    this.setState({
      table_width: this.props.clientWidth,
      table_hiegh: window.innerHeight - 300
    });
  }

  handlerSearch(event, v) {
    console.log("event " ,v)
   
    if (v.name) {
   
      let search = this.state.search;
     
      search[v.name] = v.value;
      this.setState({
        search: search
      });
      return;
    } else {
      const target = event.target;
      const value = target.type == 'checkbox' ? target.checked : target.value;
      const name = target.name;
      let search = this.state.search;
      search[name] = value;
      this.setState({
        search: search
      });
    }
  }

  handlerSubmitSearch(e) {
    let search = this.state.search;
    let str = Utility.jsonToQueryString(search);

    this.setState({
      loader_active: true
    });
    Utility.get(Settings.baseUrl + '/userprofiles/?is_active=1&' + str, (s, data) => {
      console.log("data ",data)
      let items = [];
      for (let i in data) {
          items.push(data[i]);
      }
      this.setState({
        loader_active: false,
        items: this.setFieldValue(items)
      });
    })
  }

  handleClick(e, d) {
    let row = this.state.items[d.positon];
    if (d.action == 'edit') {
      this.resetForm();
      this.setState({
        modal_open: true,
        modal_action: 'edit',
        modal_title: 'แก้ไข',
        username: row.username,
        password: '',
        password2: '',
        branch: row.branch.id,
        role: row.role,
        object_id: row.id,
        is_active: row.user.is_active,
        staff: row.staff!=null?row.staff.id:''
      });
      this.setStaffOption(row.branch.id)
    } else {
      this.handleDelete(e,row);
    }
  }

  handleDelete(e,user_profile){

    if(window.confirm('ยืนยันลบผู้ใช้งานนี้')){

      let url = Settings.baseUrl + "/userprofiles/"+user_profile.id+"/"
      Utility.delete(url,(s,r,code)=>{

        if(s){
          this.componentDidMount();
        }else{
          alert(r.error)
        }
      })

    }

  }

  render() {
    const items = this.state.items;
    const stateOptions = this.state.branch_option;
    return (
      <div>
        <Form>
          <Form.Group>
            <Form.Field width={6}>
              <Header floated='left' as='h2'>ผู้ใช้งาน</Header>
            </Form.Field>
            <Form.Field width={10}>
              <Button id='btnAddUsers' content='เพิ่ม' onClick={(e) => {
                e.preventDefault();
                this.resetForm();
                this.setState({
                  modal_open: true,
                  modal_action: 'add',
                  modal_title: 'สร้างผู้ใช้งาน'
                });
              }}
                floated='right' size='small' icon='plus' labelPosition='right' type='button' primary />
            </Form.Field>
          </Form.Group>
        </Form>

        <ContextMenu id="menu_lese_list">
          <MenuItem
            data={{ action: 'edit' }}
            onClick={this.handleClick}>แก้ไข</MenuItem>
          <MenuItem
            data={{ action: 'delete' }}
            onClick={this.handleClick}>ลบ</MenuItem>
        </ContextMenu>
        <Dimmer className={this.state.loader_active ? 'active' : ''} inverted>
          <Loader content='Loading' inverted />
        </Dimmer>
        <Modal open={this.state.modal_open} size='tiny' /*dimmer='blurring'*/ >
          <Button
            id='btnClose'
            circular
            icon='close'
            basic
            floated='right'
            name=''
            onClick={()=>this.setState({ modal_open: false })}/>
          <Modal.Header>{this.state.modal_title}</Modal.Header>
          <Modal.Content className='scrolling'>
            <Modal.Description>
              <Form size='small'>
                <Form.Field error={this.state.msg_error.branch != null}>
                  <label>*สาขา <MsgInput text={this.state.msg_error.branch} /></label>
                  <Dropdown id='dropDownBranch' ref={(input) => { this.branch_ref = input; }} placeholder='สาขา' search selection options={stateOptions} defaultValue={this.state.branch} value={this.state.branch} onChange={(e, data) => {
                    this.setStaffOption(data.value)
                    this.setState({ branch: data.value, branch_error: false });
                  }} />
                </Form.Field>
                <Form.Field error={this.state.msg_error.username != null}>
                  <label>*ชื่อผู้ใช้ <MsgInput text={this.state.msg_error.username} /></label>
                  <Input id='InputUserName' fluid ref={(input) => { this.username_ref = input; }} 
                    onChange={(e) => {
                      var username = e.target.value
                      this.setState({ username: username });
                      clearTimeout(timeout);
                      timeout = setTimeout(() => {
                        this.checkUser(username);
                      }, 1000);
                    }} value={this.state.username} />

                </Form.Field>
                <Form.Field error={this.state.msg_error.password != null}>
                  <label>*รหัสผ่าน <MsgInput text={this.state.msg_error.password} /></label>
                  <Input id='InputPassword' ref={(input) => { this.password_ref = input; }} onChange={(e) => {
                    this.setState({ password: e.target.value })
                    let msg_error = this.state.msg_error
                    if (e.target.value.length < 4) {
                      msg_error['password'] = 'ความยาวอย่างน้อย 4 ตัวอักษร'
                    } else {
                      msg_error['password'] = null
                    }
                    this.setState({
                      msg_error: msg_error
                    })
                  }} type="password" value={this.state.password} />
                </Form.Field>
                <Form.Field error={this.state.msg_error.password2}>
                  <label>*ยืนยันรหัสผ่าน <MsgInput text={this.state.msg_error.password2} /></label>
                  <Input id='InputPasswordAgain' onChange={(e) => {
                    this.setState({ password2: e.target.value })
                    console.log(e.target.value.length, this.state.password);
                    let msg_error = this.state.msg_error
                    if (e.target.value !== this.state.password) {

                      msg_error['password2'] = 'รหัสผ่านไม่ตรงกัน'

                    } else {
                      msg_error['password2'] = null
                    }
                    this.setState({
                      msg_error: msg_error
                    })
                  }} type="password" value={this.state.password2} />
                </Form.Field>
                {this.state.object_id==1?'':<Form.Field error={this.state.msg_error.role}>
                  <label>*ระดับ <MsgInput text={this.state.msg_error.role} /></label>
                  <Dropdown id='dropDownPosition' placeholder='ระดับ' defaultValue={this.state.role} search selection options={Settings.user_role} onChange={(e, data) => {
                    this.setState({ role: data.value, role_error: false });
                  }} />
                </Form.Field>}
                <Form.Field>
                  <label>พนักงาน</label>
                  <Dropdown id='dropDownStaff' placeholder='พนักงาน' defaultValue={this.state.staff} search selection options={this.state.options_staff} onChange={(e, data) => {
                    this.setState({ staff: data.value });
                  }} />
                </Form.Field>

                <Form.Field error={this.state.msg_error.is_active != null}>
                  <label>สถานะ <MsgInput text={this.state.msg_error.is_active} /></label>
                  <Dropdown id='dropDownStatus' selection fluid onChange={(e,v) => this.setState({ is_active: v.value })} value={this.state.is_active} options={Settings.is_active_option} defaultValue={1} />
                  
                </Form.Field>
              </Form>
            </Modal.Description>
          </Modal.Content>
          <Modal.Actions>
            <Button id='btnSaveUsers' size='small' primary onClick={(e) => {
              e.preventDefault();
              this.submitData();
            }}
              className={this.state.button_class}>บันทึก</Button>
            <Button id='btnCancelUsers' size='small' onClick={(e) => { e.preventDefault(); this.setState({ modal_open: false }); }}>ยกเลิก</Button>
          </Modal.Actions>
        </Modal>
        <Form size='small'>
          <Form.Group>
            <Form.Field  width={5}>
              <Input id='InputSearchUsers' placeholder='ชื้อผู้ใช้' value={this.state.search.username} onChange={this.handlerSearch} name='username'/>
            </Form.Field>
            <Form.Field width={5}>
              <Dropdown id='InputSearchBranch' placeholder='สาขา' search selection defaultValue={this.state.search.branch} value={this.state.search.branch} options={stateOptions} onChange={this.handlerSearch} name='branch' />
            </Form.Field>
            <Form.Field width={6}>
              <Button id='btnSearch' size='small' onClick={this.handlerSubmitSearch} type='button'><Icon name='search' /> ค้นหา</Button>
              <Button id='btnSearchAll' size='small' onClick={this.resetForm} type='button' >ทั้งหมด</Button>

            </Form.Field>
          </Form.Group>
        </Form>
        <Table
          rowsCount={items.length}
          rowHeight={35}
          headerHeight={35}
          width={this.state.table_width}
          height={this.state.table_hiegh}>
          <Column
            width={80}
            header={
              <Cell ></Cell>
            }
            cell={<OptionItemsCell onClickMenu={this.handleClick} data={items} />}
          />
          <Column
            header={<Cell className='text-center'>#ID</Cell>}
            cell={
              <ItemsCell id='ID' data={items} field="id" textAlign='text-center' />
            }
            width={80}
          />
          <Column
            header={<Cell>สาขา</Cell>}
            cell={
              <ItemsCell data={items} field="branch_name" />
            }
            width={200}
          />
          <Column
            header={<Cell>ชื่อผู้ใช้งาน</Cell>}
            cell={
              <ItemsCell data={items} field="username" />
            }
            width={200}
          />
          <Column
            header={<Cell>ชื่อพนักงาน</Cell>}
            cell={
              <ItemsCell data={items} field="staff_name" />
            }
            width={200}
          />
          <Column
            header={<Cell>ระดับ</Cell>}
            cell={
              <ItemsCell data={items} field="role_name" />
            }
            width={200}
          />
        </Table>
        <br />

      
      </div>
    );
  }
}
export default Users;
