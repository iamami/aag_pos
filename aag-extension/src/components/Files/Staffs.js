/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import Settings from '../../Settings';
import Utility from '../../Utility';
import MsgInput from '../Error/MsgInput'
import ImportStaff from '../Import/ImportStaff'
import {
  Form, Input, Header, Dropdown, Button, Icon, Modal, Confirm, Dimmer, Loader,TextArea
} from 'semantic-ui-react';
import {
  Table,
  Column,
  Cell
} from 'fixed-data-table';
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";
function collect(props) {
  return { positon: props.positon };
}

class OptionItemsCell extends Component {
  constructor(props) {
    super(props);

    this.state = {}
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e, v) {

  }

  render() {
    const { rowIndex} = this.props;
    return (
      <Cell><center>
        <a onClick={(e) => { this.props.onClickMenu(e, { action: 'edit', positon: rowIndex }) }}><Icon name='edit' /></a>
        <a onClick={(e) => { this.props.onClickMenu(e, { action: 'delete', positon: rowIndex }) }} ><Icon name='window close outline' /></a>
      </center></Cell>
    );
  }
}

class ItemsCell extends Component {
  render() {
    const { rowIndex, field, data, ...props } = this.props;
    return (
      <ContextMenuTrigger id="menu_lese_list"
        holdToDisplay={1000}
        key={rowIndex}
        positon={rowIndex}
        collect={collect}>
        <Cell {...props} >
          <div className={this.props.textAlign}>{data[rowIndex][field]}</div>
        </Cell>
      </ContextMenuTrigger>
    );
  }
}

class Staffs extends Component {

  constructor(props) {
    super(props);
    this.state = {
      items: [],
      username: '',
      password: '',
      first_name: '',
      modal_open: false,
      search: {},
      is_enabled: 1,
      msg_error: {}
    }
    this.submitData = this.submitData.bind(this);
    this.handlerSearch = this.handlerSearch.bind(this);
    this.handlerSubmitSearch = this.handlerSubmitSearch.bind(this);
    this.resetForm = this.resetForm.bind(this);
    this.handleClick = this.handleClick.bind(this)
  }
  componentWillMount() {
    this.setState({
      table_width: this.props.clientWidth,
      table_hiegh: window.innerHeight - 300
    });
  }
  submitData() {
    var formData = {
      phone: this.state.phone,
      branch: this.state.branch,
      user: 0,
      address: this.state.address,
      code: this.state.code,
      name: this.state.name,
      is_enabled: this.state.is_enabled
    };
    const self = this;

    this.setState({ button_loading: 'loading' ,msg_error: {}})
    if (this.state.modal_action === 'add') {
      const url = Settings.baseUrl + '/staffs/';
      Utility.post(url, formData, (status, data)=> {
        this.setState({ button_loading: '' })
        if (status) {
          if (data.id === null) {            
            self.setState({
              button_loading: false
            });
          } else {
            self.setState({
              modal_open: false,
              button_loading: false
            });
            self.componentDidMount();
            self.resetForm();
          }

        }else{ // error
          if(data.length!==0)
          this.setState({
            msg_error: data
          })
        }
      });
    } else {
      const url = Settings.baseUrl + '/staffs/' + this.state.object_id + "/";
      Utility.put(url, formData, (status, data)=> {
        if (status) {
          self.setState({
            modal_open: false
          });
          self.componentDidMount();
          self.resetForm();
        }else{ // error
          if(data.length!==0)
            this.setState({
              button_loading: '',
              msg_error: data
            })
        }
      });
    }
  }

  resetForm() {

    let search = this.state.search;
    for (let i in search) {
      search[i] = '';
    }
    this.setState({
      phone: '',
      branch: '',
      address: '',
      code: '',
      name: '',
      button_loading: false,
      search: search,
      msg_error: {}
    });
    this.componentDidMount()
  }

  componentDidMount() {
    var self = this;

    this.setState({
      loader_active: true
    });

    Utility.get(Settings.baseUrl + '/branches/?is_enabled=1', (status, data) => {
      var branches = data;
      var options_branch = [{
        key: 0,
        value:0,
        text: "- ทั้งหมด -",
      }];
      var object_branch = {};
      for (var i in branches) {
        options_branch.push({
          key: branches[i].id,
          value: branches[i].id,
          text: branches[i].name,
        });
        object_branch[branches[i].id] = branches[i];
      }
      Utility.get(Settings.baseUrl + '/staffs/?is_enabled=1', (status, data) => {
        this.staffs = data;
        var staffs = [];
        var staffs_code = [{
          key: 0,
          value: 0,
          text: "- ทั้งหมด -",
        }];
        for (let i in data) {
          data[i].branch_name = data[i].branch.name;
          staffs.push({
            key: data[i].id,
            value: data[i].id,
            text: data[i].name,
          });
          staffs_code.push({
            key: data[i].id,
            value: data[i].id,
            text: data[i].code +' ' + data[i].name,
          });
        }
       
        self.setState({
          loader_active: false,
          options_branch: options_branch,
          items: data,
          object_branch: object_branch,
          staffs: staffs,
          staffs_code: staffs_code
        });
      });
    });
  }

  genCode(branch_id) {

    Utility.get(Settings.baseUrl + '/staffs/next_number/?branch='+branch_id,(s,d,c)=>{
      if(c===200){
        this.setState({
          code: d.number
        })
      }
    })
    
  }

  getBranchName(b_id) {
    var o = this.state.object_branch[b_id];
    return o === null ? 'not branch' : o.name;
  }

  handlerSearch(event, v) {
    if (v.name) {
      let search = this.state.search;
      search[v.name] = v.value;
      this.setState({
        search: search
      });
      return;
    } else {
      const target = event.target;
      const value = target.type === 'checkbox' ? target.checked : target.value;
      const name = target.name;
      let search = this.state.search;
      search[name] = value;
      this.setState({
        search: search
      });
    }
  }

  handlerSubmitSearch(e) {
    let search = Utility.filterSearch(this.state.search)
    let str = Utility.jsonToQueryString(search);
    Utility.get(Settings.baseUrl + '/staffs/?is_enabled=1&' + str, (s, data) => {
      for (let i in data) {
        data[i].branch_name = data[i].branch.name;
      }
      this.setState({
        loader_active: false,
        items: data
      });
    })
  }

  handleDelete(e,staff){

    if(window.confirm('ยืนยันลบพนักงานนี้')){
      Utility.delete(Settings.baseUrl + "/staffs/"+staff.id+"/",(s,r,code)=>{

        if(s){
          this.componentDidMount();
        }else{
          alert(r.error)
        }
      })

    }

  }

  handleClick(e, d) {
    let row = this.state.items[d.positon];
    if (d.action === 'edit') {
      this.resetForm();
      this.setState({
        modal_open: true,
        modal_action: 'edit',
        modal_title: 'แก้ไข',
        branch: row.branch.id,
        name: row.name,
        code: row.code,
        phone: row.phone,
        address: row.address,
        object_id: row.id,
        object: row,
        object_branch: row.branch,
        is_enabled: row.is_enabled
      });
    } else {
      this.handleDelete(e, row)
      //this.setState({ confirm_open: true, object_id: row.id ,object: row});
    }
  }

  render() {
    const items = this.state.items;
    const stateOptions = this.state.options_branch === null ? [] : this.state.options_branch;
    return (
      <div>

      {this.state.import_open?
        <ImportStaff 
            data = {this.state.items}
            onClose={()=>{
              this.setState({
                import_open: false,
              });
            }}
            onUpdate={()=>{
              this.setState({
                import_open: false,
              });
              this.componentDidMount()
            }} />:''}
        <Form >
          <Form.Group>
            <Form.Field width={6}>
              <Header floated='left' as='h2'>พนักงาน</Header>
            </Form.Field>
            <Form.Field width={10}>

              <Button
                  id="importstaff" 
                  size='small' content='Import' onClick={(e) => {
                  e.preventDefault();
                  this.setState({
                    import_open: true,
                  });
                }}
                floated='right'
                icon='file alternate outline' labelPosition='left' type='button' primary />
              <Button
                id='addstaff' 
                size='small' content='เพิ่ม' onClick={(e) => {
                e.preventDefault();
                this.resetForm();
                this.setState({
                  modal_open: true,
                  modal_action: 'add',
                  modal_title: 'สร้างพนักงาน'
                });
              }}
                floated='right' icon='plus' labelPosition='left' type='button' primary />

            </Form.Field>
          </Form.Group>
        </Form>
        <Dimmer className={this.state.loader_active ? 'active' : ''} inverted>
          <Loader content='Loading' inverted />
        </Dimmer>
        <Modal open={this.state.modal_open} size='mini' /*dimmer='blurring'*/  >
        <Button
          id='closeAddstaff'
          circular
          icon='close'
          basic
          floated='right'
          name=''
          onClick={()=>this.setState({ modal_open: false })}/>
          <Modal.Header>{this.state.modal_title}</Modal.Header>
          <Modal.Content >
            <Modal.Description>
              <Form size='small'>
              <Form.Field error={this.state.msg_error.branch}>
                  <label>*สาขา <MsgInput text={this.state.msg_error.branch} /></label>
                  <Dropdown id="branch" placeholder='สาขา' search selection defaultValue={this.state.branch} value={this.state.branch} options={stateOptions} onChange={ (e, data) => {
                    this.setState({ branch: data.value });
                    this.genCode(data.value)
                  }} />
                </Form.Field>
                <Form.Field error={this.state.msg_error.code!=null}>
                  <label>*รหัสพนักงาน <MsgInput text={this.state.msg_error.code} /></label>
                  <Input
                    id='IDstaff' 
                    readOnly
                    value={this.state.code}
                    onChange={(e) => this.setState({ code: e.target.value })}
                     />
                </Form.Field>
                
                <Form.Field error={this.state.msg_error.name}>
                  <label>*ชื่อพนักงาน <MsgInput text={this.state.msg_error.name} /></label>
                  <Input id="nameStaff" onChange={(e) => this.setState({ name: e.target.value })} value={this.state.name} />
                </Form.Field>
                <Form.Field error={this.state.msg_error.address}>
                  <label>ที่อยู่ <MsgInput text={this.state.msg_error.address} /></label>
                  <TextArea id="addressStaff" onChange={(e) => this.setState({ address: e.target.value })} value={this.state.address} />
                </Form.Field>
                <Form.Field error={this.state.msg_error.phone}>
                  <label>เบอร์โทร <MsgInput text={this.state.msg_error.phone} /></label>
                  <Input id="phoneStaff" onChange={(e) => this.setState({ phone: e.target.value })} type="tel" value={this.state.phone} />
                </Form.Field>
                <Form.Field error={this.state.msg_error.is_enabled != null}>
                  <label>สถานะ <MsgInput text={this.state.msg_error.is_enabled} /></label>
                  <Dropdown id="statusStaff" selection fluid onChange={(e,v) => this.setState({ is_enabled: v.value })} value={this.state.is_enabled} options={Settings.is_enabled_option} defaultValue={1} />
                </Form.Field>
              </Form>
            </Modal.Description>
          </Modal.Content>
          <Modal.Actions>
            <Button
            id='submitAddstaff' 
            size='small' primary onClick={this.submitData}
              className={this.state.button_loading ? 'loading' : ''}>บันทึก</Button>
            <Button 
            id="cancelAddstaff"
            size='small' onClick={(e) => { e.preventDefault(); this.setState({ modal_open: false }); }}>ยกเลิก</Button>
          </Modal.Actions>
        </Modal>
        <Form size='small'>
          <Form.Group>
            <Form.Field width={5}>
              <Input id='searchStaff' placeholder='รหัสพนักงาน,ชื่อพนักงาน' value={this.state.search.text} onChange={this.handlerSearch} name='text'/>
            </Form.Field>
            <Form.Field width={5}>
              <Dropdown id='searchBranch' placeholder='สาขา' search selection  value={this.state.search.branch} options={stateOptions} onChange={this.handlerSearch} name='branch' />
            </Form.Field>

            <Form.Field width={6}>
              <Button id='btnsearchStaff' onClick={this.handlerSubmitSearch} size='small' type='button'><Icon name='search' /> ค้นหา</Button>
              <Button id='allStaff' onClick={this.resetForm} size='small' type='button' >ทั้งหมด</Button>
            </Form.Field>
          </Form.Group>
        </Form>

        <Table
          rowsCount={items.length}
          rowHeight={35}
          headerHeight={35}
          width={this.state.table_width}
          height={this.state.table_hiegh}>
          <Column
            width={80}
            header={
              <Cell ></Cell>
            }
            cell={<OptionItemsCell onClickMenu={this.handleClick} />}
          />

          <Column
            header={<Cell>สาขา</Cell>}
            cell={
              <ItemsCell  data={items} field="branch_name" />
            }
            width={200}
          />

          <Column
            header={<Cell>รหัสพนักงาน</Cell>}
            cell={
              <ItemsCell id='table_idstaff' data={items} field="code" />
            }
            width={200}
          />
          
          <Column
            header={<Cell>ชื่อพนักงาน</Cell>}
            cell={
              <ItemsCell data={items} field="name" />
            }
            width={200}
          />
          <Column
            header={<Cell>ที่อยู่</Cell>}
            cell={
              <ItemsCell data={items} field="address" />
            }
            width={200}
          />
          <Column
            header={<Cell>เบอร์โทร</Cell>}
            cell={
              <ItemsCell data={items} field="phone" />
            }
            width={200}
          />
        </Table>
        <br />
        <ContextMenu id="menu_lese_list">
          <MenuItem
            data={{ action: 'edit' }}
            onClick={this.handleClick}>แก้ไข</MenuItem>
          <MenuItem
            data={{ action: 'delete' }}
            onClick={this.handleClick}>ลบ</MenuItem>
        </ContextMenu>
        <Confirm
          content='ยืนยันลบรายการ'
          open={this.state.confirm_open}
          onCancel={() => {
            this.setState({ confirm_open: false });
          }}
          onConfirm={() => {

            const url = Settings.baseUrl + '/staffs/' + this.state.object_id + "/";
            var staffs = this.state.object
            staffs.is_enabled =0
            staffs.branch = staffs.branch.id

            console.log('staffs',staffs)
            var self = this;
            Utility.put(url, staffs, function (status, data) {
              if (status) {
                self.setState({ confirm_open: false });
                self.componentDidMount();
                self.resetForm();
              }
            });
          }}
        />
      </div>
    );
  }
}
export default Staffs;
