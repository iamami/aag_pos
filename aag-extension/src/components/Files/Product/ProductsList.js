/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import Settings from '../../../Settings';
import Utility from '../../../Utility';
import OptionItemsCell from '../OptionItemsCell'
import ProductFromModal from './ProductFromModal'
import ImportProduct from '../../Import/ImportProduct'

import {
  Form,
  Input,
  Button,
  Icon,
  Confirm, Dimmer, Loader, Dropdown,
  Header,TextArea
} from 'semantic-ui-react';
import {
  Table,
  Column,
  Cell
} from 'fixed-data-table';
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";
function collect(props) {
  return { positon: props.positon };
}

class ItemsCell extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { rowIndex, field, data, ...props } = this.props;
    return (
      <ContextMenuTrigger id="menu_lese_list"
        holdToDisplay={1000}
        key={rowIndex}
        positon={rowIndex}
        collect={collect}>
        <Cell {...props}>
          <div className={this.props.textAlign}>{data[rowIndex][field]}</div>
        </Cell>
      </ContextMenuTrigger>
    );
  }
}


class ProductsList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      items: [],
      
      search: {},
      msg_error: {}
    }
    this.product_code_all = [];
    this.product_name_all = [];
    
    this.resetForm = this
            .resetForm
            .bind(this);
    this.handlerSearch = this.handlerSearch.bind(this);
    this.handlerSubmitSearch = this.handlerSubmitSearch.bind(this);
    this.handleClick = this.handleClick.bind(this)
  }

  componentWillMount() {
    this.setState({
      table_width: this.props.clientWidth,
      table_hiegh: window.innerHeight - 340
    });
  }


  componentDidMount() {
    var self = this;
    this.setState({
      loader_active: true
    });
    var s = '';

    var categories = Utility.getFetch(Settings.baseUrl + '/categories/');
    var product_types = Utility.getFetch(Settings.baseUrl + '/product_types/');
    var products = Utility.getFetch(Settings.baseUrl + '/products/?is_enabled=1');
    Promise.all([categories, product_types, products]).then((values) => {
      this.categories = values[0];
      this.product_types = values[1];

      this.product_list = values[2]

      if (s == '') {
        for (let i in values[2]) {
          this.product_code_all.push({
            key: values[2][i].id,
            value: values[2][i].id,
            text: values[2][i].code,
          });

          this.product_name_all.push({
            key: values[2][i].id,
            value: values[2][i].id,
            text: values[2][i].name,
          });
        }
      }
      let cat = [];
      for (let i in values[0]) {
        cat.push({
          key: values[0][i].id,
          value: values[0][i].id,
          text: values[0][i].name,
        });
      }
      let type = [];
      for (let i in values[1]) {
        type.push({
          key: values[1][i].id,
          value: values[1][i].id,
          text: values[1][i].name,
        });
      }

      let products_code = [];
      for (let i in values[2]) {
        products_code.push({
          key: values[2][i].id,
          value: values[2][i].id,
          text: values[2][i].code,
        });
      }

      let products = [];
      for (let i in values[2]) {
        products.push({
          key: values[2][i].id,
          value: values[2][i].id,
          text: values[2][i].name,
        });
      }

      this.setState({
        categories: cat,
        product_types: type,
        loader_active: false,
        products: products,
        products_code: products_code,
        items: this.setFieldValue(values[2])
      });
    });
  }

  setFieldValue(v) {

    for (let i in v) {
      v[i].category_name = v[i].category.name
      v[i].prodcut_type_name = v[i].kind.name
      v[i].type_sale_title = Utility.getObjectByValue(Settings.type_sale, v[i].type_sale).text
    }

    return v;
  }


  handlerSearch(event, v) {

    if (v.name) {
      let search = this.state.search;
      search[v.name] = v.value;
      this.setState({
        search: search
      });
      return;
    } else {
      const target = event.target;
      const value = target.type == 'checkbox' ? target.checked : target.value;
      const name = target.name;
      let search = this.state.search;
      search[name] = value;
      this.setState({
        search: search
      });
    }
  }

  handlerSubmitSearch(e) {
    let search = this.state.search;
    let str = Utility.jsonToQueryString(search);
    Utility.get(Settings.baseUrl + '/products/?is_enabled=1&' + str, (s, data) => {
      this.product_list = data
      this.setState({
        loader_active: false,
        items: this.setFieldValue(data)
      });
    })
  }

  resetForm() {

    let search = this.state.search;
    for (let i in search) {
        search[i] = '';
    }
    this.setState({
        name: '',
        code: '',
        description: '',
        button_class: false,
        type_sale: 1,
        type_weight: 1,
        price_tag: '',
        cost: '',
        weight: '',
        weight_g: '',
        categories_id: '',
        product_types_id: '',
        search_id: '',
        search_categories_id: '',
        search_product_types_id: '',
        search_weight: '',
        search_weight_g: '',
        search_code: '',
        search_name: '',
        search: search,
        msg_error: {}
    })

}
 
  handleClick(e, d) {
    console.log(this.product_list)
    let row = this.product_list[d.positon];
    if (d.action == 'edit') {

      console.log(row)

      this.setState({
        modal_open: true,
        modal_action: 'edit',
        modal_title: 'แก้ไข',
        product: row
      });
    } else {
      this.setState({ confirm_open: true, object_id: row.id, object_category: row.category.id, object_kind: row.kind.id,object: row });
    }
  }

  render() {
    const items = this.state.items;
    return (
      <div>

        {this.state.import_open?
        <ImportProduct 
            data={this.state.items}
            onClose={()=>{
              this.setState({
                import_open: false,
              });
            }}
            onUpdate={()=>{
              this.setState({
                import_open: false,
              });
              this.componentDidMount()
            }} />:''}

        {this.state.modal_open?
        <ProductFromModal 
        product={this.state.product}
        onClose={()=>{this.setState({modal_open: false})}}
        action={this.state.modal_action}
        onSave={(data)=>{
          this.setState({modal_open: false})
          this.componentDidMount()
        }}  />:''}
        
        <Form size='small'>
          <Form.Group>

            <Form.Field width={6}>
              <Header floated='left' as='h2'>สินค้า</Header>
            </Form.Field>
            <Form.Field width={16}>

            </Form.Field>

            <Button id='addproduct' content='เพิ่ม' onClick={(e) => {
              e.preventDefault();
              this.setState({
                modal_open: true,
                modal_action: 'add',
                modal_title: 'สร้างสินค้า'
              });
            }}
              size='small'
              icon='plus' labelPosition='left' type='button' primary />
             <Button size='small' content='Import' onClick={(e) => {
                e.preventDefault();
                this.setState({
                  import_open: true,
                });
              }}
                icon='file alternate outline' labelPosition='left' type='button' primary />
          </Form.Group>
        </Form>
        <ContextMenu id="menu_lese_list">
          <MenuItem
            data={{ action: 'edit' }}
            onClick={this.handleClick}>แก้ไข</MenuItem>
          <MenuItem
            data={{ action: 'delete' }}
            onClick={this.handleClick}>ลบ</MenuItem>
        </ContextMenu>
        <Dimmer className={this.state.loader_active ? 'active' : ''} inverted>
          <Loader content='Loading' inverted />
        </Dimmer>
 
        <Form size='small'>
          <Form.Group>
            <Form.Field width={4}>
              <Dropdown id='searchproductgroup' placeholder='กลุ่มสินค้า' search selection defaultValue={this.state.search.category_id} value={this.state.search.category_id} options={this.state.categories} onChange={this.handlerSearch} name='category_id' />
            </Form.Field>
            <Form.Field width={4}>
              <Dropdown id='searchproducttype' placeholder='ประเภทสินค้า' search selection defaultValue={this.state.search.product_types_id} value={this.state.search.product_types_id} options={this.state.product_types} onChange={this.handlerSearch} name='product_types_id' />
            </Form.Field>
            <Form.Field width={4}>
              <Dropdown id='searchproductid' placeholder='รหัสสินค้า' search selection defaultValue={this.state.search.product_id} value={this.state.search.product_id} options={this.state.products_code} onChange={this.handlerSearch} name='product_id' />
            </Form.Field>
            <Form.Field width={4}>
              <Dropdown id='searchproductname' placeholder='ชื่อสินค้า' search selection defaultValue={this.state.search.product_id} value={this.state.search.product_id} options={this.state.products} onChange={this.handlerSearch} name='product_id' />
            </Form.Field>

          </Form.Group>
          <Form.Group >
            <Form.Field width={4}>
              <Dropdown id='searchproductsell' placeholder='ประเภทงานขาย' search selection defaultValue={this.state.search.type_sale} value={this.state.search.type_sale} options={Settings.type_sale} onChange={this.handlerSearch} name='type_sale' />
            </Form.Field>
            <Form.Field width={4}>
              <Dropdown id='searchproductweightb' placeholder='น้ำหนัก บาท' search selection defaultValue={this.state.search.weight_b} value={this.state.search.weight_b} options={Settings.weight} onChange={(e, v) => {
                let search = this.state.search;
                search.weight = parseFloat(v.value) * 15.2;
                search.weight_b = v.value
                this.setState({
                  search: search
                })
              }} name='weight' />
            </Form.Field>
            <Form.Field width={4}>
              <Input id='searchproductweightg' placeholder='น้ำหนัก กรัม' search selection value={this.state.search.weight} onChange={this.handlerSearch} name='weight' />
            </Form.Field>

            <Form.Field width={4}>
              <Button id='searchproduct' size='small' onClick={this.handlerSubmitSearch} type='button'><Icon name='search' />  ค้นหา</Button>
              <Button id='searchallproduct' size='small' onClick={(e) => {
                this.resetForm(e);
                this.componentDidMount();
              }} type='button' >ทั้งหมด</Button>


            </Form.Field>
          </Form.Group>

        </Form>

        <Table
          rowsCount={items.length}
          rowHeight={35}
          headerHeight={35}
          width={this.state.table_width}
          height={this.state.table_hiegh}>
          <Column
            width={80}
            header={
              <Cell ></Cell>
            }
            cell={<OptionItemsCell onClickMenu={this.handleClick} />}
          />
          <Column
            header={<Cell>กลุ่มสินค้า</Cell>}
            cell={
              <ItemsCell data={items} field="category_name" />
            }
            width={120}
          />
          <Column
            header={<Cell>ประเภทสินค้า</Cell>}
            cell={
              <ItemsCell data={items} field="prodcut_type_name" />
            }
            width={120}
          />
          <Column
            header={<Cell>รหัสสินค้า</Cell>}
            cell={
              <ItemsCell id='tableproductid' data={items} field="code" />
            }
            width={120}
          />
          <Column
            header={<Cell>ชื่อสินค้า</Cell>}
            cell={
              <ItemsCell data={items} field="name" />
            }
            width={200}
          />
          <Column
            header={<Cell className="text-right">น.น.(ก.)</Cell>}
            cell={
              <ItemsCell data={items} field="weight" textAlign="text-right" />
            }
            width={120}
          />
          <Column
            header={<Cell>ประเภทงานขาย</Cell>}
            cell={
              <ItemsCell data={items} field="type_sale_title" />
            }
            width={120}
          />
          <Column
            header={<Cell className="text-right">ราคาป้าย</Cell>}
            cell={
              <ItemsCell data={items} field="price_tag" textAlign="text-right" />
            }
            width={120}
          />
          <Column
            header={<Cell className="text-right">ต้นทุนชิ้นละ</Cell>}
            cell={
              <ItemsCell data={items} field="cost" textAlign="text-right" />
            }
            width={120}
          />
          <Column
            header={<Cell className="text-right">คะแนน</Cell>}
            cell={
              <ItemsCell data={items} field="score" textAlign="text-right" />
            }
            width={120}
          />
          <Column
            header={<Cell>หมายเหตุ</Cell>}
            cell={
              <ItemsCell data={items} field="description" />
            }
            width={200}
          />
        </Table>
            
        <Confirm
          id='deleteproduct'
          content='ยืนยันลบรายการ'
          open={this.state.confirm_open}
          onCancel={() => {
            this.setState({ confirm_open: false });
          }}
          onConfirm={() => {
            const url = Settings.baseUrl + '/products/' + this.state.object_id + "/";
            var self = this;
            Utility.delete(url, (status, data)=> {
              if (status) {
                self.setState({ confirm_open: false });
                self.componentDidMount();
                self.resetForm();
              }else{
                alert(data.error)
              }
            });
          }}
        />
      </div>
    );
  }
}
export default ProductsList;
