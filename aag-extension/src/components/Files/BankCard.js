/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import Settings from '../../Settings';
import Utility from '../../Utility';
import OptionItemsCell from './OptionItemsCell'
import MsgInput from '../Error/MsgInput'

import {
  Form,
  Input,
  Button,
  Icon,
  Modal,
  Confirm, Dimmer, Loader, Dropdown, Header
} from 'semantic-ui-react';

import {
  Table,
  Column,
  Cell
} from 'fixed-data-table';
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";
function collect(props) {
  return { positon: props.positon };
}

class ItemsCell extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { rowIndex, field, data, ...props } = this.props;
    return (
      <ContextMenuTrigger id="menu_lese_list"
        holdToDisplay={1000}
        key={rowIndex}
        positon={rowIndex}
        collect={collect}>
        <Cell {...props}>
          <div className={this.props.textAlign}>{data[rowIndex][field]}</div>
        </Cell>
      </ContextMenuTrigger>
    );
  }
}

class ItemsCellOption extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { rowIndex, field, data, ...props } = this.props;
    return (
      <Cell {...props} className='cell-time-click'>
        <Button.Group basic size='mini'>
          <Button onClick={(e) => {
            this.props.onClickEdit(e, data[rowIndex]);
          }}><Icon color='green' name='edit' /></Button>
          <Button onClick={(e) => {
            this.props.onClickDelete(e, data[rowIndex]);
          }}><Icon color='red' name='delete' /></Button>
        </Button.Group>
      </Cell>
    );
  }
}

class Branch extends Component {

  constructor(props) {
    super(props);
    this.state = {
      items: [],
      name: '',
      code: '',
      remark: '',
      modal_open: false,
      msg_error: {}
    }
    this.submitData = this.submitData.bind(this);
    this.handleClick = this.handleClick.bind(this)
  }

  submitData() {
    var msg_error = {}
    if (this.state.banks_id == null || this.state.banks_id == '') {
      msg_error['bank'] = ['*เลือกธนาคาร']
    }

    if (this.state.kind == null || this.state.kind == '') {
      console.log("stat kindddd d d d d d",this.state.kind)
      msg_error['kind'] = ['*ต้องไม่เป็นค่าว่าง']
    }

    if (this.state.fee == null || this.state.fee == '') {
      msg_error['fee'] = ['*ต้องไม่เป็นค่าว่าง & *ต้องไม่มีตัวอักษร']
    }


    if (Object.keys(msg_error).length > 0) {
      this.setState({
        msg_error: msg_error
      })
      return;
    }

    var formData = {
      bank: this.state.banks_id,
      kind: this.state.kind,
      fee: this.state.fee
    };

    const self = this;
    this.setState({ button_class: 'loading' })
    if (this.state.modal_action == 'add') {
      const url = Settings.baseUrl + '/bank_cards/';
      Utility.post(url, formData, (status, data) => {
        this.setState({ button_class: '' })
        if (status) {
          self.setState({
            modal_open: false
          });
          self.componentDidMount();
          self.resetForm();
        } else { // error
          if (data.length !== 0)
            this.setState({
              msg_error: data
            })
        }
      });
    } else {
      const url = Settings.baseUrl + '/bank_cards/' + this.state.object_id + "/";
      Utility.put(url, formData, (status, data) => {
        this.setState({ button_class: '' })
        if (status) {
          self.setState({
            modal_open: false
          });
          self.componentDidMount();
          self.resetForm();
        } else { // error
          if (data.length !== 0)
            this.setState({
              msg_error: data
            })
        }
      });
    }
  }

  resetForm() {
    this.setState({
      name: '',
      kind: '',
      fee: '',
      button_class: '',
      msg_error: {}
    });
  }

  componentWillMount() {
    this.setState({
      table_width: this.props.clientWidth,
      table_hiegh: window.innerHeight - 250
    });
  }

  componentDidMount() {
    var self = this;
    this.setState({
      loader_active: true
    });
    Utility.get(Settings.baseUrl + '/bank_cards/?is_enabled=1', (status, data) => {
      self.setState({
        items: this.setFieldValue(data),
        loader_active: false
      });
    });

    Utility.get(Settings.baseUrl + '/banks/?is_enabled=1', (status, data) => {
      let banks = []
      for (let i in data) {
        banks.push({
          value: data[i].id,
          text: data[i].name
        });
      }
      this.setState({
        banks: banks
      });
    });
  }

  setFieldValue(v) {

    for (let i in v) {
      v[i].bank_name = v[i].bank.name
    }

    return v;
  }

  handleClick(e, d) {
    let data = this.state.items[d.positon];
    if (d.action == 'edit') {
      this.resetForm();
      this.setState({
        modal_open: true,
        modal_action: 'edit',
        modal_title: 'แก้ไข',
        name: data.name,
        kind: data.kind,
        fee: data.fee,
        banks_id: data.bank.id,
        object_id: data.id
      });
    } else {
      this.setState({ confirm_open: true, object_id: data.id, object_bank_id: data.bank.id });
    }
  }

  render() {
    const items = this.state.items;
    return (
      <div>
        <Form size='small'>
          <Form.Group>
            <Form.Field width={6}>
              <Header floated='left' as='h2'>ชนิดบัตร</Header>
            </Form.Field>
            <Form.Field width={16}>
              <Button id='btnAddCard' size='small' content='เพิ่ม' onClick={(e) => {
                e.preventDefault();
                this.resetForm();
                this.setState({
                  modal_open: true,
                  modal_action: 'add',
                  modal_title: 'สร้างชนิดบัตร'
                });
              }}
                floated='right' icon='plus' labelPosition='right' type='button' primary />
            </Form.Field>
          </Form.Group>
        </Form>
        <ContextMenu id="menu_lese_list">
          <MenuItem
            data={{ action: 'edit' }}
            onClick={this.handleClick}>แก้ไข</MenuItem>
          <MenuItem
            data={{ action: 'delete' }}
            onClick={this.handleClick}>ลบ</MenuItem>
        </ContextMenu>
        <Dimmer className={this.state.loader_active ? 'active' : ''} inverted>
          <Loader content='Loading' inverted />
        </Dimmer>
        <Modal open={this.state.modal_open} size='tiny' /*dimmer='blurring'*/ >
        <Button
          id='btnCloseCard'
          circular
          icon='close'
          basic
          floated='right'
          name=''
          onClick={()=>this.setState({ modal_open: false })}/>
          <Modal.Header>{this.state.modal_title}</Modal.Header>
          <Modal.Content>
            <Modal.Description>
              <Form size='small'>
                <Form.Field error={this.state.msg_error.kind != null}>
                  <label>ประเภทบัตร <MsgInput text={this.state.msg_error.kind} /></label>
                  <Input id='inputCardType' onChange={(e) => this.setState({ kind: e.target.value })}
                    value={this.state.kind} />
                </Form.Field>
                <Form.Field error={this.state.msg_error.fee != null}>
                  <label>%ค่าธรรมเนียม <MsgInput text={this.state.msg_error.fee} /></label>
                  <Input id='inputCardFree' type="number" onChange={(e) => this.setState({ fee: e.target.value })} value={this.state.fee} />
                </Form.Field>
                <Form.Field error={this.state.msg_error.bank != null}>
                  <label>ชื่อธนาคาร <MsgInput text={this.state.msg_error.bank} /></label>
                  <Dropdown id='dorpDownBankName' search selection width={14} options={this.state.banks}
                    onChange={(e, v) => {
                      this.setState({ banks_id: v.value });
                    }} defaultValue={this.state.banks_id} value={this.state.banks_id} />
                </Form.Field>
              </Form>
            </Modal.Description>
          </Modal.Content>
          <Modal.Actions>
            <Button id='btnSaveCard' size='small' primary onClick={(e) => {
              e.preventDefault();
              this.submitData();
              
            }}
              className={this.state.button_class}>บันทึก</Button>
            <Button id='btnCancelCard' size='small' onClick={(e) => { e.preventDefault(); this.setState({ modal_open: false }); }}>ยกเลิก</Button>
          </Modal.Actions>
        </Modal>
        <Table
          rowsCount={items.length}
          rowHeight={35}
          headerHeight={35}
          width={this.state.table_width}
          height={this.state.table_hiegh}>
          <Column
            width={80}
            header={
              <Cell ></Cell>
            }
            cell={<OptionItemsCell onClickMenu={this.handleClick} />}
          />
          <Column
            header={<Cell>ประเภทบัตร</Cell>}
            cell={
              <ItemsCell id='cardType' data={items} field="kind" />
            }
            width={200}
          />
          <Column
            header={<Cell>ค่าธรรมเนียม</Cell>}
            cell={
              <ItemsCell data={items} field="fee" />
            }
            width={200}
          />
          <Column
            header={<Cell>ชื่อธนาคาร</Cell>}
            cell={
              <ItemsCell data={items} field="bank_name" />
            }
            width={200}
          />
        </Table>

        <Confirm
          id='modalConfirmDeleteCard'
          content='ยืนยันลบรายการ'
          open={this.state.confirm_open}
          onCancel={() => {
            this.setState({ confirm_open: false });
          }}
          onConfirm={() => {
            const url = Settings.baseUrl + '/bank_cards/' + this.state.object_id + "/"
            Utility.delete(url,(status, data,code)=> {
              if (status) {
                this.setState({ confirm_open: false });
                this.componentDidMount();
                this.resetForm();
              }else if(data.error && code==400){
                alert(data.error)
              }else if(code==404){
                alert('ถูกลบไปแล้ว')
              }
            });

          }}
        />
      </div>
    );
  }
}
export default Branch;
