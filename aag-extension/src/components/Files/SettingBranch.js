/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import {
  Form,
  Button,
  Grid,
  Input,
  Checkbox,
  Header,
  Segment,
  Dropdown
} from 'semantic-ui-react';
import Utility from '../../Utility';
import Settings from '../../Settings';
import CalculateDay from '../Lease/CalculateDay';
import MsgInput from '../Error/MsgInput'
import { connect } from 'react-redux'


class Setting extends Component {

  constructor(props) {
    super(props);

    const {branch} = props.auth
    this.state = {
      error: {},
      setting: {
        branch: branch.id
      },
      branch_id: branch.id
    }

    this.handlerChangeBranch = this.handlerChangeBranch.bind(this);
    this.onChangeInput = this.onChangeInput.bind(this)
    this.saveSetting = this.saveSetting.bind(this)
    this.handlerChangeCalculateDate = this.handlerChangeCalculateDate.bind(this)
  }

  componentDidMount() {
    this.setState({
      loader_active: true
    });

    var branches = Utility.getFetch(Settings.baseUrl + '/branches/?is_enabled=1');
    var lease_setting = Utility.getFetch(Settings.baseUrl + '/branch_setting/?is_enabled=1');
    Promise.all([branches, lease_setting]).then((values) => {

      this.branches = values[0]
      this.lease_setting = values[1]

      let branches = []
      for (let i in this.branches) {
        branches.push({
          value: this.branches[i].id,
          key: this.branches[i].id,
          text: this.branches[i].name
        });
      }

      this.setState({
        branches: branches,
        loader_active: false
      });

      let setting = this.getSettingByBranch(this.state.branch_id)

      if (setting) {
        this.setState({
          setting: setting
        })
      }
    });
  }

  getSettingByBranch(b_id) {
    for (let i in this.lease_setting) {
      if (this.lease_setting[i].branch == b_id) {
        return this.lease_setting[i]
      }
    }
    return false
  }

  onChangeInput(e, v) {
    let setting = this.state.setting;

    if(v.name=='counter' || v.name == 'tax_counter' || v.name =='interest' || v.name == 'min_interest' || v.name == 'month'){
      let j = parseInt(v.value)
      if(j<0)
        v.value = 0 
      if( v.name =='interest' && j>100)
        v.value = 100
    }
    if (v.type == 'checkbox') {
      setting[v.name] = v.checked ? 1 : 0;
    } else
      setting[v.name] = v.value;

    this.setState({
      setting: setting
    })
  }

  saveSetting(e) {
    let setting = this.state.setting;
    console.log(setting);
    this.setState({
      button_loading: true
    })
    if (setting.id == null) {
      let url = Settings.baseUrl + "/branch_setting/";
      Utility.post(url, setting, (s, d) => {
       
        if(s){
          
          this.setState({
            button_loading: false,
            error: {}
          })
          this.componentDidMount()
          alert('บันทึกสำเร็จ')
        }else{
          this.componentDidMount()
          this.setState({
            button_loading: false,
            error: d
          })
          
        }
      });
    } else {
      let url = Settings.baseUrl + "/branch_setting/" + setting.id + "/";
      Utility.put(url, setting, (s, d) => {


        let error = {}
        let setting = {}
        if(s){
          setting = d
          this.componentDidMount()
          alert('บันทึกสำเร็จ')
        }else{
          error = d
        }
        this.setState({
          button_loading: false,
          error: error
        })

      });
    }
  }

  handlerChangeBranch(e, v) {
    this.setState({
      branch_id: v.value
    })
    let setting = this.getSettingByBranch(v.value)

    if (setting) {
      this.setState({
        setting: setting
      })
    } else {
      this.setState({
        setting: {
          branch: v.value,
          counter: '',
          code: '',
          month: '',
          interest: '',
          min_interest: '',
          date: ''
        }
      })
    }
  }

  handlerChangeCalculateDate(e, v) {
    let setting = this.state.setting;
    setting.calculate_date = v.value;

    this.setState({
      setting: setting
    })
  }

  render() {
    return (
      <div>
        <Form>
          <Form.Field>
            <label>สาขา</label>
            <Dropdown id='dropDownBranch' placeholder='สาขา' search selection width={4} value={this.state.branch_id}
            defaultValue={this.state.branch_id}
            options={this.state.branches}
            onChange={this.handlerChangeBranch} />
            </Form.Field>
        </Form>
        <br/>
        <Form className='attached fluid ' size='small'>

        
          
          <Grid divided='vertically'>
            <Grid.Row >
              <Grid.Column width={8}>
                <Segment>
                  <Header size='huge'>ใบกำกับภาษี</Header>
                  <Form.Field error={this.state.error.tax_code!=null}>
                    <label>หมวดเริ่มต้น <MsgInput text={this.state.error.tax_code}/></label>
                    <Input id='inputCatagoryVat' value={this.state.setting.tax_code} pattern="[A-Z]{2}"  name='tax_code' onChange={this.onChangeInput} />
                    <small>ต้องเป็นตัวพิมพ์ใหญ่ A-Z และ2ตัวอักษรเช่น AA</small>
                  </Form.Field>
                  <Form.Field>
                    <label>เลขที่เริ่มต้น</label>
                    <Input id='inputStartIDVat' value={this.state.setting.tax_counter} pattern="[0-9]"  name='tax_counter' onChange={this.onChangeInput} min='0' type='number' />
                  </Form.Field>
                </Segment>
              </Grid.Column>
              <Grid.Column width={8}>
                <Segment>
                  <Header size='huge'>รหัสพนักงาน</Header>
                  <Form.Field error={this.state.error.staff_code!=null}>
                    <label>หมวดเริ่มต้น <MsgInput text={this.state.error.staff_code}/></label>
                    <Input id='inputCatagoryStaff' value={this.state.setting.staff_code} name='staff_code' pattern="[A-Z]{1}"  onChange={this.onChangeInput} />
                    <small>ต้องเป็นตัวพิมพ์ใหญ่ A-Z และ1ตัวอักษรเช่น A</small>
                  </Form.Field>
                  <Form.Field>
                    <label>เลขที่เริ่มต้น</label>
                    <Input id='IDstaffstart' value={this.state.setting.staff_counter} pattern="[0-9]"  name='staff_counter' onChange={this.onChangeInput} min='0' type='number' />
                  </Form.Field>
                </Segment>
              </Grid.Column>
            </Grid.Row>
          </Grid>

          
          <Segment>
          <Header size='huge'>ขายฝาก</Header>
          <Grid divided='vertically'>
            <Grid.Row >
              <Grid.Column width={4}>
                <Form.Field error={this.state.error.code!=null}>
                  <label>หมวดเริ่มต้น <MsgInput text={this.state.error.code}/></label>
                  <Input id='inputCatagoryLease' value={this.state.setting.code} pattern="[A-Z]{2}"  name='code' onChange={this.onChangeInput} maxLength='2' />
                  <small>ต้องเป็นตัวพิมพ์ใหญ่ A-Z และ2ตัวอักษรเช่น AA</small>
                </Form.Field>
                <Form.Field>
                  <label>เลขที่เริ่มต้น</label>
                  <Input id='inputStartIDLease' value={this.state.setting.counter} min='0' pattern="[0-9]"  name='counter' onChange={this.onChangeInput} type="number" />
                </Form.Field>
                <Form.Field>
                  <label>จำนวนเดือนขายฝาก</label>
                  <Input id='inputMonthLease' className='text-right' value={this.state.setting.month} name='month' onChange={this.onChangeInput} type='number' min={0} />
                </Form.Field>
                <Form.Field>
                  <label>อัตราดอกเบี้ยขายฝาก (%)</label>
                  <Input id='inputRateLease' className='text-right' value={this.state.setting.interest} name='interest' onChange={this.onChangeInput} type='number' min={0} max={100} />
                  <small>limit 100%</small>
                </Form.Field>
                <Form.Field>
                  <label>ดอกเบี้ยขายฝากขั้นต่ำ</label>
                  <Input id='inputMinRateLease' className='text-right' value={this.state.setting.min_interest} name='min_interest' onChange={this.onChangeInput} type='number' min={0}  />
                </Form.Field>

              </Grid.Column>
              <Grid.Column width={10}>
                <CalculateDay
                  onChange={this.handlerChangeCalculateDate}
                  value={this.state.setting.calculate_date}
                />
                <br />
                <Form.Field>
                  <Checkbox id='userCanChangeRate' label='User สามารถแก้ไขอัตราดอกเบี้ยได้' value='1' checked={this.state.setting.is_user_edit_interest == 1} name='is_user_edit_interest' onChange={this.onChangeInput} />
                </Form.Field>
              </Grid.Column>
            </Grid.Row>
          </Grid>

          </Segment>
          <Form.Group>

            <Form.Field width={16}>
              <br />
              <Button id='btnsave_setting' content='บันทึก' onClick={this.saveSetting}
                loading={this.state.button_loading}
                floated='right' type='button' primary />
            </Form.Field>
          </Form.Group>
        </Form>
      </div>
    );
  }
}

const mapStateToProps = state =>{
  return ({
    auth: state.auth,
    branches: state.branches
  })
}

export default connect(
  mapStateToProps,
)(Setting)
