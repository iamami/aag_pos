/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import {
    Cell
  } from 'fixed-data-table';
  import {
    Label
  } from 'semantic-ui-react';

class StatusCell extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {
            rowIndex,
            data
        } = this.props;
        return (
            <Cell>
                <Label size='mini' color={data[rowIndex].is_enabled==1?"green":"grey"}>{data[rowIndex].is_enabled==1?"Active":"Inactive"}</Label>
            </Cell>
        );
    }
}

export default StatusCell