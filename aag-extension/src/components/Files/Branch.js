/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import Settings from '../../Settings';
import Utility from '../../Utility';
import OptionItemsCell from './OptionItemsCell'
import MsgInput from '../Error/MsgInput'
import ImportBranch from '../Import/ImportBranch'
import {
  Table,
  Column,
  Cell
} from 'fixed-data-table';
import {
  Form,
  Input,
  Button,
  Icon,
  Modal,
  Confirm, Dimmer, Loader, Header,TextArea,Dropdown
} from 'semantic-ui-react';
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";
function collect(props) {
  return { positon: props.positon };
}
class ItemsCell extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { rowIndex, field, data, ...props } = this.props;
    return (
      <ContextMenuTrigger id="menu_lese_list"
        holdToDisplay={1000}
        key={rowIndex}
        positon={rowIndex}
        collect={collect}>
        <Cell {...props}>
          <div className={this.props.textAlign}>{data[rowIndex][field]}</div>
        </Cell>
      </ContextMenuTrigger>
    );
  }
}


class Branch extends Component {

  constructor(props) {
    super(props);
    this.state = {
      items: [],
      name: '',
      code: '',
      remark: '',
      modal_open: false,
      msg_error: {}
    }
    this.submitData = this.submitData.bind(this);
    this.handleClick = this.handleClick.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
  }

  submitData() {

    var formData = {
      name: this.state.name,
      code: this.state.code,
      description: this.state.description,
      is_enabled: this.state.is_enabled
    };

    const self = this;
    this.setState({ button_class: 'loading', msg_error: {} })
    if (this.state.modal_action == 'add') {
      const url = Settings.baseUrl + '/branches/';
      Utility.post(url, formData, (status, data) => {
        this.setState({ button_class: '' })
        if (status) {
          self.componentDidMount();
          self.resetForm();
          self.setState({
            modal_open: false
          });
        } else { // error
          if (data.length !== 0)
            this.setState({
              msg_error: data
            })
        }
      });

    } else {

      const url = Settings.baseUrl + '/branches/' + this.state.object_id + "/";
      Utility.put(url, formData, (status, data) => {
        this.setState({ button_class: '' })
        if (status) {
          self.setState({
            modal_open: false
          });
          self.componentDidMount();
          self.resetForm();
        } else { // error
          if (data.length !== 0)
            this.setState({
              msg_error: data
            })
        }
      });
    }
  }

  genCode() {
    Utility.getCounter('branches', (s, d) => {
      this.setState({
        code: this.pad(d.number)
      });
    });
  }

  pad(num, size) {
    num = num + ""
    var s = "000" + num;
    return s.substr(num.length);
  }

  resetForm() {
    this.setState({
      name: '',
      code: '',
      description: '',
      button_class: '',
      msg_error: {}
    });
  }

  componentDidMount() {
    this.setState({
      loader_active: true
    });
    Utility.get(Settings.baseUrl + '/branches/?is_enabled=1', (status, data) => {
      if(status)
        this.setState({
          items: data,
          loader_active: false
        });
    });
  }

  componentWillMount() {
    this.setState({
      table_width: this.props.clientWidth,
      table_hiegh: window.innerHeight - 270
    });
  }

  handleDelete(e,branch){

    if(window.confirm('ยืนยันลบ')){

      let url = Settings.baseUrl + "/branches/"+branch.id+"/"
      Utility.delete(url,(s,r,code)=>{

        if(s){
          this.componentDidMount();
        }else{
          alert(r.error)
        }
      })

    }

  }

  handleClick(e, d) {
    let data = this.state.items[d.positon];
    if (d.action == 'edit') {
      this.resetForm();
      this.setState({
        modal_open: true,
        modal_title: 'แก้ไข',
        modal_action: 'edit',
        name: data.name,
        code: data.code,
        description: data.description,
        object_id: data.id,
        object: data
      })
    } else {
      this.handleDelete(e, data)
    }
  }

  render() {
    const items = this.state.items;
    return (
      <div>
        {this.state.import_branch_open?
        <ImportBranch 
            onClose={()=>{
              this.setState({
                import_branch_open: false,
              });
            }}
            onUpdate={()=>{
              this.setState({
                import_branch_open: false,
              });
              this.componentDidMount()
            }} />:''}
        <Form >
          <Form.Group>
            <Form.Field width={6}>
              <Header floated='left' as='h2'>สาขา</Header>
            </Form.Field>
            <Form.Field width={10}>
            <Button 
                id='importBranch'
                size='small' content='Import' 
                onClick={(e) => {
                e.preventDefault();
                this.setState({
                  import_branch_open: true,
                });
              }}
                floated='right' icon='file alternate outline' labelPosition='left' type='button' primary />
              <Button 
                id='addBranch'
                size='small' content='เพิ่ม' 
                onClick={(e) => {
                e.preventDefault();
                this.resetForm();
                this.setState({
                  modal_open: true,
                  modal_action: 'add',
                  modal_title: 'สร้างสาขา'
                });
              }}
                floated='right' icon='plus' labelPosition='left' type='button' primary />
            </Form.Field>
          </Form.Group>
        </Form>
        <ContextMenu id="menu_lese_list">
          <MenuItem
            data={{ action: 'edit' }}
            onClick={this.handleClick}>แก้ไข</MenuItem>
          <MenuItem
            data={{ action: 'delete' }}
            onClick={this.handleClick}>ลบ</MenuItem>
        </ContextMenu>
        <Dimmer className={this.state.loader_active ? 'active' : ''} inverted>
          <Loader content='Loading' inverted />
        </Dimmer>
        <Modal id='modalCreateBranch' open={this.state.modal_open} size='mini' /*dimmer='blurring'*/ >
        <Button
          id='btnClose'
          circular
          icon='close'
          basic
          floated='right'
          name=''
          onClick={()=>this.setState({ modal_open: false })}/>
          <Modal.Header>{this.state.modal_title}</Modal.Header>
          <Modal.Content>
            <Modal.Description>
              <Form size='small' > 
                <Form.Field error={this.state.msg_error.code != null} >
                  <label>*รหัสสาขา <MsgInput text={this.state.msg_error.code} /></label>
                  <Input id='inputBranchID' 
                    onChange={(e) => this.setState({ code: e.target.value })}
                    ref='code'
                    readOnly
                    value={this.state.code}
                    action={
                      <Button id='btnGenID' onClick={(e) => { e.preventDefault(); this.genCode(); }} disabled={this.state.modal_action=='edit'} icon='angle double left' />
                    } />
                  
                </Form.Field>
                <Form.Field error={this.state.msg_error.name != null}>
                  <label>*ชื่อสาขา <MsgInput text={this.state.msg_error.name} /></label>
                  <Input id='inputBranchName' onChange={(e) => this.setState({ name: e.target.value })} value={this.state.name} />
                  
                </Form.Field>
                <Form.Field error={this.state.msg_error.is_enabled != null}>
                  <label>สถานะ <MsgInput text={this.state.msg_error.is_enabled} /></label>
                  <Dropdown id='inputBranchStatus' selection fluid onChange={(e,v) => this.setState({ is_enabled: v.value })} value={this.state.is_enabled} options={Settings.is_enabled_option} defaultValue={1} />
                  
                </Form.Field>
                <Form.Field error={this.state.msg_error.description!=null}>
                  <label>หมายเหตุ <MsgInput text={this.state.msg_error.description} /></label>
                  <TextArea onChange={(e) => this.setState({ description: e.target.value })} value={this.state.description} />
                </Form.Field>
              </Form>
            </Modal.Description>
          </Modal.Content>
          <Modal.Actions>
            <Button id='btnSave' size='small' primary onClick={this.submitData}
              className={this.state.button_class}>บันทึก</Button>
            <Button id='btnCancel' size='small' onClick={(e) => { e.preventDefault(); this.setState({ modal_open: false }); }}>ยกเลิก</Button>
          </Modal.Actions>
        </Modal>
        <Table
          rowsCount={items.length}
          rowHeight={35}
          headerHeight={35}
          width={this.state.table_width}
          height={this.state.table_hiegh}>
          <Column
            width={80}
            header={
              <Cell ></Cell>
            }
            cell={<OptionItemsCell onClickMenu={this.handleClick} />}
          />
          <Column

            header={<Cell>รหัสสาขา</Cell>}
            cell={
              <ItemsCell id='branchID' data={items} field="code" />
            }
            width={120}
          />
          <Column
            header={<Cell>ชื่อสาขา</Cell>}
            cell={
              <ItemsCell data={items} field="name" />
            }
            width={200}
          />
          <Column
            header={<Cell>หมายเหตุ</Cell>}
            cell={
              <ItemsCell data={items} field="description" />
            }
            width={200}
          />
          
        </Table>
        <br />
        <Confirm
          content='ยืนยันลบรายการ'
          open={this.state.confirm_open}
          onCancel={() => {
            this.setState({ confirm_open: false });
          }}
          onConfirm={() => {
            const url = Settings.baseUrl + '/branches/' + this.state.object_id + "/";
            var formData = this.state.object;
            formData['is_enabled'] = 0;            
            this.setState({ confirm_open: false, loader_active: true });
            Utility.put(url, formData, (status, data) => {
              if (status) {
                this.componentDidMount();
                this.resetForm();
              }
            });
          }}
        />
      </div>
    );
  }
}
export default Branch;
