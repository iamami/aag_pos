/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, {Component} from 'react';
import ImportCSV from './ImportCSV'
import {

} from 'semantic-ui-react';
import Utility from '../../Utility';
import Settings from '../../Settings';


class ImportProduct extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fields: this.setFields([],[]),
            is_loading: false
        }
        this.handleSubmitData = this.handleSubmitData.bind(this)
    }

    handleSubmitData(data_list){
        this.setState({
            is_loading: true,
            loading_title: '0/'+(data_list.length)
        })
        var position = 0
        let run = ()=>{
            let data = data_list[position]
            
            let url_search = Settings.baseUrl + '/lease/?number='+data.lease
            Utility.get(url_search,(e,res)=>{

                if(res.length>0){
                    let url = Settings.baseUrl + '/lease/'+res[0].id+'/principle/'
                    data['lease'] = res[0].id
                    Utility.post(url,data,(e,res)=>{
                        position++;
                        if(position<data_list.length)
                            run()
                        else
                            this.props.onUpdate()
                    })
                }else{
                    position++;
                        if(position<data_list.length)
                            run()
                        else
                            this.props.onUpdate()
                }
        })

        }
        run()
    }

    setFields(staff_options){
        return [
            {
                name: 'lease',
                title: '*เลขที่ขายฝาก',
                type: 'string',
                required: true,
            },
            {
                name: 'total',
                title: '*จำนวนเงิน',
                type: Number,
                required: true,
            },
            {
                name: 'balance',
                title: '*คงเหลือ',
                required: true,
                type: Number
            },
            {
                name: 'staff',
                title: '*พนักงาน',
                required: true,
                type: 'string',
                options: staff_options
            },
            {
                name: 'kind',
                title: '*ประเภท',
                required: true,
                type: 'string',
                options: [
                    {
                        value: 'IC',
                        key: 'IC',
                        text: 'เพิ่ม'
                    },
                    {
                        value: 'DC',
                        key: 'DC',
                        text: 'ลด'
                    }
                ]
            },
            {
                name: 'description',
                title: 'หมายเหตุ',
                type: 'string'
            }
        ]
    }


    componentDidMount() {

        var staffs = Utility.getFetch(Settings.baseUrl + '/staffs/');
        Promise.all([ staffs]).then((values) => {
    
          let staff_options = [];
          for (let i in values[0]) {
            staff_options.push({
              key: values[0][i].id,
              value: values[0][i].id,
              text: values[0][i].name,
            });
          }
    
          this.setState({
              fields: this.setFields(staff_options)
          })
        });
      }

    render() {

        return (
            <div>
                <ImportCSV
                    fields={this.state.fields}
                    onClose={this.props.onClose}
                    onSubmitData={this.handleSubmitData}
                    loading={this.state.is_loading}
                    loading_title={this.state.loading_title}
                 />
            </div>
        )
    }
}

export default ImportProduct