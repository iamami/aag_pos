/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import {
    Menu,Icon,Confirm
} from 'semantic-ui-react';
import {BrowserRouter as Router, Route, Link,Redirect} from 'react-router-dom'
import { connect } from 'react-redux'
import logo from '../../logo.png';
import Settings from '../../Settings';
import Utility from '../../Utility';

import { setTimeout } from 'timers';
var config = require( '../../config' )

class StaffInfo extends Component {
    render() {
        const {auth} = this.props
        const {user,branch} = auth
        let infoText = <b>{user.username}</b>;
        let b = branch?<small><i>{branch.name}</i> </small>:''
        
        return (
            <span><Icon name='user circle' color='grey'/> {infoText} {b}</span>
        );
    }
}

class NavBar extends Component {

  constructor(props) {
    super(props);
    this.state = {
        active_path: '/'
    }

    this.handleItemClick = this.handleItemClick.bind(this)
  }

  handleItemClick(e, {name, to}) {

        this.setState({active_path: to});
    }

    onHandleLogout(){
        const url = Settings.revokeTokenUrl;
        const token = JSON.parse(localStorage.getItem('token'))
        const formData = {
            token: token.access_token,
            grant_type: 'password',
            client_id: Settings.client_id,
            client_secret: Settings.client_secret
          };
        this.setState({
            confirm_open: false
        })
        Utility.postBasic(url, formData, (status, data,code) => {
            if(code==200){
                localStorage.setItem("token", JSON.stringify({}));
                this.props.onLogout()
            }
        })
    }

  render() {        
        const {auth} = this.props
        const {role} = auth
    return (
        <Menu pointing secondary size='small' >
            <Menu.Item
                color='black' 
                name='home'
                active={this.state.active_path == '/'}
                onClick={this.handleItemClick}
                as={Link}
                to='/'><Icon name='home' color='black' />
                <b>AAG</b>
            </Menu.Item>
            {role != 'U'
                ? <Menu.Item id='btnSetting'
                        color='purple' 
                        name='files'
                        active={this.state.active_path == '/files/branches'}
                        onClick={this.handleItemClick}
                        as={Link}
                        to='/files/branches'>
                        <Icon name='folder outline' color='purple'  />
                        แฟ้มข้อมูล
                    </Menu.Item>
                : ''}
            {role != 'U'
                ? <Menu.Item id='btnStock'
                        color='yellow' 
                        name='stock'
                        active={this.state.active_path == '/stock/import'}
                        onClick={this.handleItemClick}
                        as={Link}
                        to='/stock/import'>
                        <Icon name='shipping' color='yellow'  />
                        สต็อกทอง
                    </Menu.Item>
                : ''}

            <Menu.Item id='btnCustomer'
                name='customer'
                color='olive' 
                active={this.state.active_path == '/customer'}
                onClick={this.handleItemClick}
                as={Link}
                to='/customer'>
                <Icon name='users' color='olive' />
                ลูกค้า
            </Menu.Item>
            <Menu.Item id='btnPOS'
                name='sell'
                color='red' 
                active={this.state.active_path == '/pos'}
                onClick={this.handleItemClick}
                as={Link}
                to='/pos'>
                <Icon name='cart' color='red' />
                POS ซื้อ-ขาย
            </Menu.Item>
            <Menu.Item id='btnLease'
                name='lease'
                color='teal'
                active={this.state.active_path == '/lease/pos'}
                onClick={this.handleItemClick}
                as={Link}
                to='/lease/pos'><Icon name='add to calendar' color='teal' />
                ขายฝาก
            </Menu.Item>
            <Menu.Item id='btnReportMoney'
                color='blue'
                name='report1'
                active={this.state.active_path == '/ledger'}
                onClick={this.handleItemClick}
                as={Link}
                to='/ledger'>
                <Icon name='book' color='blue' />รายรับ-รายจ่าย</Menu.Item>
                <Menu.Item id='btnReport'
                        color='brown'
                        name='report1'
                        active={this.state.active_path == '/report'}
                        onClick={this.handleItemClick}
                        as={Link}
                        to='/report'>
                        <Icon name='bar chart' color='brown'/>
                        รายงาน
                </Menu.Item>
                <Menu.Item id='btnReward'
                        color='pink'
                        name='report1'
                        active={this.state.active_path == '/reward/redeem'}
                        onClick={this.handleItemClick}
                        as={Link}
                        to='/reward/redeem'>
                        <Icon name='gift' color='pink'/>
                        ของรางวัล
                    </Menu.Item>
            <Menu.Menu position='right'>
                <Menu.Item>
                    <StaffInfo auth={auth}/>
                </Menu.Item>
                <Menu.Item id='logout' title='ออกจากระบบ' onClick={()=>this.setState({confirm_open: true})}>
                    <b><Icon name='power' color='red'/></b>
                    <Confirm id='confirmLogout'
                    content='คุณต้องการออกจากระบบ'
                    open={this.state.confirm_open}
                    onCancel={() => {
                        this.setState({confirm_open: false});
                    }}
                    onConfirm={this.onHandleLogout.bind(this)}/>
                </Menu.Item>
            </Menu.Menu>
        </Menu>
    );
  }
}

const mapStateToProps = ({auth}) =>{
    return ({auth})
  }
  export default connect(
    mapStateToProps
  )(NavBar)