/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, {Component} from 'react';
import Utility from '../../../../Utility';
import Settings from '../../../../Settings';
import MsgInput from '../../../Error/MsgInput'
import BillItemModal from './BillItemModal'
import { connect } from 'react-redux'
import {Table, Column, Cell} from 'fixed-data-table';
import {
  Form,
  Header,
  Button,
  Loader,
  Dimmer,
  Icon,
  Dropdown,
  Input,
  Modal,
  Grid,
  Popup,
  TextArea,
  Checkbox
} from 'semantic-ui-react';
import BillPrintPreview from './BillPrintPreview'
import DatePicker from 'react-datepicker';
import moment from 'moment';
import {ContextMenu, MenuItem} from "react-contextmenu";
import OptionItemsCell from '../../OptionItemsCell'

class ItemsCell extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const {
      rowIndex,
      field,
      data,
      ...props
    } = this.props;
    return (
        <Cell {...props}>
          <div className={this.props.textAlign}>{data[rowIndex][field]}</div>
        </Cell>
    );
  }
}

class InvoiceDetail extends Component {

  constructor(props) {
    super(props);

    this.modal_status = false;
    this.state = {
      message_error_hidden: true,
      items: [],
      invoice_date: moment(),
      vendor: '',
      status_stock: 'N',
      modal_open: this.props.open,
      action_title: 'เพิ่มสินค้า',
      action: 'add',
      invoice_item: [],
      button_print_disabled: true,
      button_update_stoick_disabled: true,
      button_save_disabled: false,
      button_update_stoick_loading: false,
      invoice_title: this.props.action == 'import'
        ? 'เพิ่มรายการทองใหม่เข้าสต๊อก'
        : 'เพิ่มรายการทองใหม่ออกจากสต๊อก',
      loader: false,
      status_bill: 'N',
      button_save_title: 'สร้าง',
      invoice_add_product_disabled: true,
      invoice_description: '',
      msg_error: {}
    }

    this.product_code_all = [];
    this.product_name_all = [];
    this.categories = [];
    this.product_types = [];

    this.onOpenModal = this
      .onOpenModal
      .bind(this);
    this.onEditProduct = this
      .onEditProduct
      .bind(this);
    this.onDeleteProduct = this
      .onDeleteProduct
      .bind(this);
  }

  submitSaveInvoice() {
    let msg_error = {}
    if (this.state.branch_id === undefined) {
      msg_error['branch'] = ['กรุณาเลือกสาขา']
    }

    if (this.state.vendor === '' && this.props.action == 'import') {
      msg_error['vendor'] = ['กรุณาเลือกโรงงาน/ร้านส่ง']
    }

    if (Object.keys(msg_error).length > 0) {
      this.setState({msg_error: msg_error});
      return;
    }

    var formData = {
      branch: this.state.branch_id,
      vendor: this.state.vendor,
      ref_number: this.state.ref_number,
      invoice_date: Utility.formatDate2(this.state.invoice_date),
      status_bill: this.state.status_bill,
      status_stock: this.state.status_stock,
      kind: this.props.action == 'import'
        ? 'IM'
        : 'EX',
      description: this.state.invoice_description
    };

    this.setState({button_save_loading: true, msg_error: {}})
    if (this.props.modal_action == 'add') {
      const url = Settings.baseUrl + '/invoices/';
      Utility.post(url, formData, (status, data) => {
        this.setState({button_save_loading: false})
        if (status) {
          this.setState({button_save_loading: false, invoice_add_product_disabled: false, invoice_disabled: true, button_save_title: 'บันทึก'})
          this
            .props
            .onAddInvoice(data);
          this.loadInvoiceItem()
        } else { // error
          this.setState({msg_error: data})
        }
      });
    } else {
      var formData2 = {
        branch: this.state.branch_id,
        vendor: this.state.vendor,
        ref_number: this.state.ref_number,
        invoice_date: Utility.formatDate2(this.state.invoice_date),
        status_bill: this.state.status_bill,
        description: this.state.invoice_description
      };

      const url = Settings.baseUrl + '/invoices/' + this.props.invoice_detail.id + "/";

      Utility.put(url, formData2, (status, data) => {
        this.setState({button_save_loading: false})
        if (status) {
          this.setState({button_save_loading: false})
          this
            .props
            .onAddInvoice(data);
            this.loadInvoiceItem()
        } else { // error
          this.setState({msg_error: data})
        }
      });
    }
  }


  loadInvoiceItem() {

    var invoices = Utility.getFetch(Settings.baseUrl + '/invoices/' + this.props.invoice_detail.id + "/");
    var items = Utility.getFetch(Settings.baseUrl + '/invoices/' + this.props.invoice_detail.id + '/items/');
    this.setState({loader: true});
    Promise
      .all([invoices, items])
      .then((values) => {
        // set invoice detail
        let item = values[0];

        this.setState({
          number: item.number,
          branch_id: item.branch,
          vendor: item.vendor,
          status_stock: item.status_stock,
          status_bill: item.status_bill,
          ref_number: item.ref_number,
          invoice_description: item.description,
          invoice_disabled: true,
          invoice_item: this.setFieldValue(values[1]),
          button_print_disabled:false,
          button_update_stoick_disabled: values[1].length == 0 || item.status_stock == 'Y',
          button_save_title: 'บันทึก',
          invoice_add_product_disabled: item.status_stock == 'Y',
          invoice_title: <span>{this.props.action == 'import'
              ? <span>แก้ไขรายการทองใหม่เข้าสต๊อก
                </span>
              : <span>แก้ไขรายการทองใหม่ออกจากสต๊อก</span>
          } </span>,
          loader: false
        });
      });
  }

  setFieldValue(item) {

    let invoice_weight_total = 0;
    let invoice_product_total = 0;
    let invoice_itme_total = item.length;
    let invoice_profit_total = 0;
    let invoice_
    for (let i = 0; i < item.length; i++) {
      item[i]['status_stock_title'] = Settings.status_stock[item[i].status_stock]
      item[i]['product_code'] = item[i].product.code
      item[i]['product_name'] = item[i].product.name
      item[i]['category_title'] = item[i].product.category.name
      item[i]['kind_title'] = item[i].product.kind.name

      if (item[i].product.type_sale == 2) {
        invoice_profit_total += parseFloat(item[i].product.cost) * parseFloat(item[i].amount);

        item[i]['price_total'] = Utility.priceFormat(parseFloat(item[i].product.cost) * parseFloat(item[i].amount));
        item[i]['type_sale_title'] = 'งานชิ้น';
        item[i]['cost'] = Utility.priceFormat(parseFloat(item[i].product.cost));
        item[i]['price_tag'] = Utility.priceFormat(parseFloat(item[i].product.price_tag));
      } else {
        item[i]['price_total'] = Utility.priceFormat(0.00);
        item[i]['type_sale_title'] = 'งานชั่ง';
        item[i]['cost'] = Utility.priceFormat(0.00);
        item[i]['price_tag'] = Utility.priceFormat(0.00);
      }

      invoice_weight_total += parseFloat(item[i].weight_total);
      invoice_product_total += parseInt(item[i].amount);
    }
    this.setState({
      invoice_weight_total: Utility.weightFormat(invoice_weight_total),
      invoice_product_total: Utility.numberFormat(invoice_product_total),
      invoice_itme_total: Utility.numberFormat(invoice_itme_total),
      invoice_profit_total: Utility.priceFormat(invoice_profit_total)
    });
    return item;
  }

  resetFormProduct() {
    this.setState({
      product_id: '',
      product_name: '',
      product_weight: '',
      product_weight_total: '',
      product_weight_real: '',
      product_amount: '',
      action_title: 'เพิ่มสินค้า',
      action: 'add',
      msg_error: {}
    });
  }


  onOpenModal() {
    if (this.props.modal_action == "edit") {
      this.loadInvoiceItem();
    } else {
      this.setState({button_save_disabled: false})
    }
  }

  onClosenModal() {
    this.setState({
      number: '',
      branch_id: '',
      vendor: '',
      status_stock: 'N',
      ref_number: '',
      invoice_disabled: false,
      invoice_item: [],
      button_update_stoick_disabled: true,
      button_save_title: 'สร้าง',
      button_save_disabled: true,
      invoice_add_product_disabled: true,
      product_id: '',
      product_name: '',
      product_weight: '',
      product_weight_total: '',
      product_weight_real: '',
      product_amount: '',
      button_update_stoick_loading: false
    });
  }

  onStatusModal() {
    if (this.modal_status != this.props.open) {
      if (this.props.open) 
        this.onOpenModal()
      else 
        this.onClosenModal()
    }
    this.modal_status = this.props.open;
  }

  onEditProduct(e, d) {

    let data = this.state.invoice_item[d.positon];
    if (data.status_stock == 'N') {
      this.setState({
        invoice_item: data,
        action: 'edit',
        action_title: 'แก้ไข',
        modal_item: true
      });

    } else 
      alert('ไม่สามารถแก้ไขรายการนี้ได้เนื่องถูกอัพเดทสตํอกแล้ว');
    }
  
  onDeleteProduct(e, d) {
    let data = this.state.invoice_item[d.positon];
    if (data.status_stock == 'N') {
      if (!window.confirm('ยืนยันลบ')) 
        return
      const url = Settings.baseUrl + '/invoice_item/' + data.id + '/';
      Utility.delete(url, (status, data) => {

        this.setState({button_product_loading: false})
        this.resetFormProduct();
        this.loadInvoiceItem();
      });
    } else 
      alert('ไม่สามารถลบรายการนี้ได้');
    }
  
  submitUpdateStock() {

    this.setState({button_update_stoick_loading: true})
    const url = Settings.baseUrl + '/invoices/' + this.props.invoice_detail.id + '/stock/';
    Utility.get(url, (status, data) => {

      if(status){
        this
        .props
        .onAddInvoice();
      this.loadInvoiceItem();
      }else{
        alert(data.error)
      }

      this.setState({button_update_stoick_loading: false})
    });
  }

  componentDidMount() {
    setTimeout(()=>{
      let table_width = document.getElementById('table_w')
      this.setState({ table_width:table_width.clientWidth });
    },400)
    this.setState({loader_active: true, btn_stock: true});
  }


  render() {
    this.onStatusModal()
    let branch_option = Utility.getOptions(this.props.branches)
    let items = this.state.invoice_item;
    return (
      <Modal size="large" open={true} /*dimmer='blurring'*/>
        <Button
          id='btnCloseModalImEx'
          circular
          icon='close'
          basic
          floated='right'
          name=''
          onClick={this.props.onClose}/>
        <Modal.Header>
          <Header as='h4'>
            <Header.Content>
              {this.state.invoice_title}
              {this.state.number && <span>[บิลเลขที่ {this.state.number}]</span>}
              <Header.Subheader>
                <span><Icon
                  id='iconimport'
                  name={this.state.status_stock == 'N' ? 'warning circle' : 'check circle'}
                  color={this.state.status_stock == 'N' ? 'yellow' : 'green'}/>{Settings.status_stock[this.state.status_stock]}</span>
              </Header.Subheader>
            </Header.Content>
          </Header>
        </Modal.Header>
        <Modal.Content>
          {this.state.modal_item && <BillItemModal data={this.state.invoice_item} onSuccess={()=>{this.loadInvoiceItem()}} action={this.state.action} invoice={this.props.invoice_detail} onClose={()=>this.setState({modal_item: false})} title={this.state.action_title} />}
          <div >
            <div className='relative'>
            {this.state.loader && <Dimmer active={this.state.loader} inverted>
                <Loader inverted content='Loading'/>
              </Dimmer>}
              <div >
                <Grid >
                  <Grid.Row>
                    <Grid.Column width={4}>
                      <Form size='small'>
                        <Form.Group>
                          <Form.Field width={16} error={this.state.msg_error.branch != null}>
                            <label>*สาขา
                              <MsgInput text={this.state.msg_error.branch}/></label>
                            <Dropdown
                              id='branchimport'
                              placeholder='สาขา'
                              width={16}
                              className={this.state.branch_id_error
                              ? 'error'
                              : ''}
                              search
                              selection
                              options={branch_option}
                              value={this.state.branch_id}
                              onChange={(e, data) => {
                              this.setState({branch_id: data.value});
                            }}
                              disabled={this.state.invoice_disabled}/>
                          </Form.Field>
                        </Form.Group>
                        <Form.Group>
                          <Form.Field width={16} error={this.state.msg_error.vendor != null}>
                            <label>{this.state.invoice_disabled || this.props.action == 'export'
                                ? ''
                                : '*'}ชื่อโรงงาน/ร้านส่ง
                              <MsgInput text={this.state.msg_error.vendor}/></label>
                            <Dropdown
                              id='vendorimport'
                              width={16}
                              label=''
                              placeholder='ชื่อโรงงาน/ร้านส่ง'
                              search
                              disabled={this.state.invoice_disabled}
                              selection
                              options={this.props.vendors}
                              value={this.state.vendor}
                              onChange={(e, data) => {
                                this.setState({vendor: data.value});
                              }}/>
                          </Form.Field>
                        </Form.Group>
                        <Form.Group>
                          {this.state.invoice_disabled || this.props.action == 'export'
                            ? ''
                            : <Form.Field width={16} error={this.state.msg_error.ref_number != null}>
                              <label>เลขที่บิลร้านส่ง
                                <MsgInput text={this.state.msg_error.ref_number}/></label>
                                <Input
                                  id='ref_number'
                                  placeholder='เลขที่บิลร้านส่ง'
                                  value={this.state.ref_number}
                                  onChange={(e, data) => {
                              this.setState({ref_number: data.value});
                            }}/></Form.Field>}
                        </Form.Group>
                        <Form.Group>
                          <Form.Field width={16}>
                            <label>วันที่</label>
                            <DatePicker
                              id='date'
                              dateFormat="DD/MM/YYYY"
                              selected={this.state.invoice_date}
                              onChange={(date) => {
                              this.setState({invoice_date: date});
                            }}
                              disabled={this.state.invoice_disabled}/>
                          </Form.Field>
                        </Form.Group>

                        <Form.Group inline>
                        <Form.Field >
                            
                          <Checkbox
                            id='clearbillimport'
                            checked={this.state.status_bill == 'Y'}
                            onChange={(e, v) => {
                            this.setState({
                              status_bill: v.checked
                                ? 'Y'
                                : 'N'
                            });
                          }}/>
                          <label>เคลียร์บิล</label>
                           </Form.Field>
                        </Form.Group>

                        <Form.Group >
                          <Form.Field width={16} error={this.state.msg_error.description != null}>
                            <label>หมายเหตุ
                              <MsgInput text={this.state.msg_error.description}/></label>
                            <TextArea
                              placeholder=''
                              width={16}
                              value={this.state.invoice_description}
                              onChange={(e, v) => {
                              this.setState({invoice_description: v.value})
                            }}/>
                          </Form.Field>
                        </Form.Group>
                      </Form>
                    </Grid.Column>
                    <Grid.Column width={12}>

                      <Form size='small'>
                        <Form.Group>
                          <Form.Field width={6}>
                            <Header floated='left' as='h3'>รายการสินค้า</Header>
                          </Form.Field>
                          <Form.Field width={10}>
                            <Button
                              id='add'
                              disabled={this.state.invoice_add_product_disabled}
                              size='small'
                              content='เพิ่มรายการสินค้า'
                              onClick={(e) => {
                              e.preventDefault();
                              this.setState({modal_item: true})
                            }}
                              floated='right'
                              icon='plus'
                              labelPosition='left'
                              type='button'
                              color='green'/>
                          </Form.Field>
                        </Form.Group>
                      </Form>
                      <div id='table_w'>
                        <Table
                          rowsCount={items.length}
                          rowHeight={35}
                          headerHeight={30}
                          width={this.state.table_width}
                          height={350}>
                          <Column
                            width={50}
                            header={< Cell > </Cell>}
                            cell={< OptionItemsCell edit={false} onClickMenu = {
                            (e, data) => {
                              if (data.action == 'edit') 
                                this.onEditProduct(e, data)
                              else 
                                  this.onDeleteProduct(e, data)
                            }
                          } />}/>
                          <Column
                            header={< Cell > สถานะปรับปรุงสต็อก </Cell>}
                            cell={< ItemsCell data = {
                            items
                          }
                          field = "status_stock_title" />}
                            width={150}/>
                          <Column
                            header={< Cell > รหัสสินค้า </Cell>}
                            cell={< ItemsCell data = {
                            items
                          }
                          field = "product_code" />}
                            width={120}/>
                          <Column
                            header={< Cell > กลุ่มสินค้า </Cell>}
                            cell={< ItemsCell data = {
                            items
                          }
                          field = "category_title" />}
                            width={120}/>
                          <Column
                            header={< Cell > ประเภท </Cell>}
                            cell={< ItemsCell data = {
                            items
                          }
                          field = "kind_title" />}
                            width={120}/>
                          <Column
                            header={< Cell > ชื่อสินค้า </Cell>}
                            cell={< ItemsCell data = {
                            items
                          }
                          field = "product_name" />}
                            width={150}/>
                          <Column
                            header={< Cell className = 'text-right' > น.น./ชิ้น</Cell >}
                            cell={< ItemsCell data = {
                            items
                          }
                          field = "weight" textAlign = 'text-right' />}
                            width={120}/>
                          <Column
                            header={< Cell className = 'text-right' > จำนวน </Cell>}
                            cell={< ItemsCell data = {
                            items
                          }
                          field = "amount" textAlign = 'text-right' />}
                            width={120}/>
                          <Column
                            header={< Cell className = 'text-right' > น.น.รวม </Cell>}
                            cell={< ItemsCell data = {
                            items
                          }
                          field = "weight_total" textAlign = 'text-right' />}
                            width={120}/>
                          <Column
                            header={< Cell className = 'text-right' > น.น.ชั่งจริง </Cell>}
                            cell={< ItemsCell data = {
                            items
                          }
                          field = "weight_real" textAlign = 'text-right' />}
                            width={120}/>

                          <Column
                            header={< Cell className = 'text-right' > ต้นทุนชิ้นละ </Cell>}
                            cell={< ItemsCell data = {
                            items
                          }
                          field = "cost" textAlign = 'text-right' />}
                            width={120}/>
                          <Column
                            header={< Cell className = 'text-right' > ราคาป้ายชิ้นละ </Cell>}
                            cell={< ItemsCell data = {
                            items
                          }
                          field = "price_tag" textAlign = 'text-right' />}
                            width={120}/>
                          <Column
                            header={< Cell className = 'text-center' > ประเภทการขาย </Cell>}
                            cell={< ItemsCell data = {
                            items
                          }
                          field = "type_sale_title" textAlign = 'text-center' />}
                            width={120}/>
                        </Table>
                      </div>
                      <Form className='fluid' size='small'>
                        <Form.Group >
                          <Form.Input
                            id='invoiceitem'
                            label='จำนวนรายการ'
                            placeholder=''
                            className='text-right'
                            width={4}
                            value={this.state.invoice_itme_total}
                            readOnly/>
                          <Form.Input
                            id='invoiceprofit'
                            label='ค่าแรงขายปลีกรวม'
                            placeholder=''
                            className='text-right'
                            width={4}
                            value={this.state.invoice_profit_total}
                            readOnly/>
                          <Form.Input
                            id='weighttotal'
                            label='น้ำหนักรวม'
                            placeholder=''
                            className='text-right'
                            width={4}
                            value={this.state.invoice_weight_total}
                            readOnly/>
                          <Form.Input
                            id='producttotal'
                            label='จำนวนรวม'
                            placeholder=''
                            className='text-right'
                            width={4}
                            value={this.state.invoice_product_total}
                            readOnly/>
                        </Form.Group>

                      </Form>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </div>
              
              <br/>
              <ContextMenu id="menu_lese_list">
                <MenuItem
                  data={{
                  action: 'edit'
                }}
                  onClick={this.onEditProduct}>แก้ไข</MenuItem>
                <MenuItem
                  data={{
                  action: 'delete'
                }}
                  onClick={this.onDeleteProduct}>ลบ</MenuItem>
              </ContextMenu>

              <br/>

            </div>

            {this.state.open_print
              ? <BillPrintPreview
                  invoice_detail={this.props.invoice_detail}
                  items={items}
                  vendors={this.props.vendors}
                  product_total={this.state.invoice_product_total}
                  weight_total={this.state.invoice_weight_total}
                  onClose={() => {
                  this.setState({open_print: false})
                }}/>
              : ''}

          </div>

        </Modal.Content>
        <Modal.Actions>
          <Button
            id='print'
            primary
            size='small'
            icon='print'
            disabled={this.state.button_print_disabled}
            onClick={(e) => {
            e.preventDefault();
            this.setState({open_print: true})
          }}
            content='พิมพ์'/>
          <Button
            id='save'
            size='small'
            primary
            icon='save'
            labelPosition='left'
            onClick={(e) => {
            e.preventDefault();
            this.submitSaveInvoice();
          }}
            className={this.state.button_save_loading
            ? 'loading'
            : ''}
            content={this.state.button_save_title}/>
          <Button
            id='updateimport'
            size='small'
            icon='lightning'
            color='red'
            onClick={(e) => {
            e.preventDefault();
            if (window.confirm('ยืนยันบันทึกและอัพเดทสต๊อก')) 
              this.submitUpdateStock();
            }}
            className={this.state.button_update_stoick_loading
            ? 'loading'
            : ''}
            disabled={this.state.button_update_stoick_disabled}
            content='บันทึกและอัพเดทสต๊อก'/>

        </Modal.Actions>
      </Modal>
    )
  }

}


const mapStateToProps = state =>{
  return ({
    auth: state.auth,
    branches: state.branches
  })
}
export default connect(
  mapStateToProps,
)(InvoiceDetail)
