/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, {Component} from 'react';
import Utility from '../../../../Utility';
import Settings from '../../../../Settings';
import MsgInput from '../../../Error/MsgInput'
import ProductFromModal from '../../../Files/Product/ProductFromModal'

import {
  Form,
  Button,
  Input,
  Modal
} from 'semantic-ui-react';

class BillItemModal extends Component{

    constructor(props){
        super(props)
        this.state = {
            msg_error: {},
            max: 0
        }

        if(props.action==='edit'){
          
          for(let k in props.data)
            if(k==='product' || k==='invoice')
              this.state[k] = props.data[k].id
            else
            this.state[k] = props.data[k]
        }
        this.handleSubmit = this.handleSubmit.bind(this)
    }


    handleSubmit(e) {
      let msg_error = {}
      let formData = {
        invoice: this.props.invoice.id,
        product: this.state.product,
        status_stock: 'N',
        amount: this.state.amount,
        weight: this.state.weight,
        weight_total: this.state.weight_total,
        weight_real: this.state.weight_real
      };

      if (this.state.product === null || this.state.product === undefined || this.state.product === '') {
        msg_error['product'] = ['กรุณาเลือกสินค้า']
      }
      if (this.state.amount === null || this.state.amount === undefined || this.state.amount === '') {
        msg_error['amount'] = [' กรุณาระบุจำนวนให้ถูกต้อง']
      }
      if (this.state.weight_real === null || this.state.weight_real === undefined || this.state.weight_real === '') {
        msg_error['weight_real'] = ['กรุณาระบุน้ำหนัก']
      }
      if(this.state.weight_real < 0 || this.state.weight_real === null || this.state.weight_real === undefined || this.state.weight_real === ''){
        msg_error['weight_real'] = ['กรุณาระบุ น.น.ชั่ง(กรัม) ให้ถูกต้อง']
      }
      if(this.state.weight < 0 || this.state.weight === null || this.state.weight === undefined || this.state.weight === ''){
        msg_error['weight'] = ['กรุณาระบุ น.น.(กรัม) ให้ถูกต้อง']
      }
      if(this.state.weight_total < 0 || this.state.weight_total === null || this.state.weight_total === undefined || this.state.weight_total === ''){
        msg_error['weight_total'] = ['กรุณาระบุ น.น.รวม(กรัม) ให้ถูกต้อง']
      }
  
      if (Object.keys(msg_error).length > 0) {
        this.setState({msg_error: msg_error})
        return;
      }
  
      this.setState({modal_product: false})
  
      this.setState({button_product_loading: true})
      if (this.props.action === 'add') {
        const url = Settings.baseUrl + '/invoice_item/';
        Utility.post(url, formData, (status, data) => {
          this.setState({button_product_loading: false})

          if(status){
            this.props.onSuccess()
            this.props.onClose()
          }else{
            this.setState({
              msg_error: data
            })
          }

        });
      } else {
        const url = Settings.baseUrl + '/invoice_item/' + this.state.invoice + '/';
        Utility.put(url, formData, (status, data) => {
          if (status) {
            this.setState({button_product_loading: false})
            this.props.onSuccess()
            this.props.onClose()
          }
        });
      }
  
    }


  loadProduct(branch) {

    if(this.props.invoice.kind==='EX'){
      var products = Utility.getFetch(Settings.baseUrl + '/stock_product/?is_enabled=1&branch='+branch);
      Promise .all([products]) .then((values) => {
          let product_options = []
          this.products = []

          for(let i in values[0]){
            values[0][i].product.amount = values[0][i].amount
            this.products.push(values[0][i].product)

            product_options.push({key: values[0][i].product.id,
              value: values[0][i].product.id,
              text: values[0][i].product.code + " " +values[0][i].product.name
            });
          }
          this.setState({product_options: product_options})
        });
      }else{
        var products = Utility.getFetch(Settings.baseUrl + '/products/?is_enabled=1');
        Promise .all([products]) .then((values) => {
            let product_options = []
            this.products = values[0]
            for (let i in values[0]) {
              product_options.push({key: values[0][i].id,
                value: values[0][i].id,
                text: values[0][i].code + " " +values[0][i].name
              });
            }
    
            this.setState({product_options: product_options})
          });
    
      }
    
  }

    componentDidMount(){
      this.loadProduct(this.props.invoice.branch) 
    }

    render(){

        return(<Modal size='mini' open={true}  /*dimmer='blurring'*/>
        <Button
          id='btnCloseProduct'
          circular
          icon='close'
          basic
          floated='right'
          name=''
          onClick={this.props.onClose}/>
        <Modal.Header>
          {this.props.title}
        </Modal.Header>
        <Modal.Content>
          <Form className='attached fluid' size='small' onSubmit={this.handleSubmit}>
          <Form.Field error={this.state.msg_error.product != null}>
            <label>*ชื่อสินค้า <MsgInput text={this.state.msg_error.product}/></label>
            <Form.Dropdown
              id='productimport'
              search
              selection
              options={this.state.product_options}
              placeholder='ชื่อสินค้า'
              defaultValue={this.state.product}
              value={this.state.product}
              onChange={(e, data) => {
                let item = Utility.getObject(this.products, data.value);
                console.log(item)
                this.setState({product: data.value, weight: item.weight,max: item.amount});
                this.productAmountInput.focus()
            }}/>
            </Form.Field>
            <Form.Field error={this.state.msg_error.amount != null}>
              <label>*จำนวน {this.props.invoice.kind==='EX' && <span>{this.state.max<=0?<span className='red'>*ไม่คงเหลือ</span> : <span  className='green'> คงเหลือ {Utility.numberFormat(this.state.max)}ชิ้น</span>}</span>}
                <MsgInput text={this.state.msg_error.amount}/></label>
              <Input
                id='amountimport'
                min={0}
                max={this.state.max}
                ref={(c)=>this.productAmountInput = c}
                placeholder='จำนวน(ชิ้น)'
                type='number'
                className='text-right'
                value={this.state.amount}
                onKeyPress={(e)=> {
                  if(e.key === 'Enter'){
                      this.productWeightInputReal.focus()
                  } 
              }}
                onChange={(e, data) => {
                const amount = Utility.parseInt(data.value,this.state.amount,1,this.state.max)
                this.setState({
                  amount: amount,
                  weight_total: (amount * this.state.weight).toFixed(3),
                  weight_real: (amount * this.state.weight).toFixed(3)
                });
              }}/>
            </Form.Field>
            <Form.Field error={this.state.msg_error.weight != null}>
              <label>น.น.(กรัม) <MsgInput text={this.state.msg_error.weight}/></label>
              <Input
              id='weightimport'
              placeholder=''
              value={this.state.weight}
              className='text-right'/>
              </Form.Field>
            <Form.Field error={this.state.msg_error.weight_total != null}>
            <label>น.น.รวม(กรัม) <MsgInput text={this.state.msg_error.weight_total}/></label>
              <Input
                id='weighttotalimport'
                placeholder=''
                value={this.state.weight_total}
              className='text-right'/>
              </Form.Field>
            <Form.Field error={this.state.msg_error.weight_real != null}>
              <label>*น.น.ชั่ง(กรัม) <MsgInput text={this.state.msg_error.weight_real}/></label>
              <Input
                id='weightrealimport'
                placeholder=''
                type='number'
                onFocus={(e)=>{e.target.select()}}
                value={this.state.weight_real}
                ref={(c)=>this.productWeightInputReal = c}
                className='text-right'
                onKeyPress={(e)=> {
                  if(e.key === 'Enter'){
                      this.handleSubmit(e)
                  } 
              }}
                onChange={(e, data) => {
                this.setState({weight_real: data.value});
              }}/>
              </Form.Field>
          </Form>
          {this.state.product_modal_open
            ? <ProductFromModal
                onClose={() => {
                this.setState({product_modal_open: false})
              }}
                action='add'
                onSave={(data) => {
                  this.products.push(data)
                  let product_options = this.state.product_options
                  product_options.push({key: data.id,
                    value: data.id,
                    text: data.code + " " +data.name
                  });
                this.setState({
                  product: data.id,
                  weight: data.weight,
                  product_modal_open: false,
                  product_options: product_options
                })
              }}/>
            : ''}
        </Modal.Content>
        <Modal.Actions>
          <Button
            id='createimport'
            size='small'
            icon='add'
            onClick={(e) => {
            e.preventDefault();
            this.setState({product_modal_open: true})
          }}
            content="สร้างสินค้า"/>
          <Button
            id='addimport'
            size='small'
            primary
            disabled={this.state.invoice_add_product_disabled}
            className={this.state.button_product_loading}
            onClick={this.handleSubmit}>เพิ่ม</Button>
        </Modal.Actions>
      </Modal>)
    }

}

export default BillItemModal