/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import {
  Form, Header, Button, Loader, Dimmer, Icon, Input
} from 'semantic-ui-react';
import OptionItemsCell from '../OptionItemsCell'
import DatePicker from 'react-datepicker';
import moment from 'moment';
import {
  Table,
  Column,
  Cell
} from 'fixed-data-table';

import Settings from '../../../Settings';
import Utility from '../../../Utility';
import BillModal from './BillModal';

function collect(props) {
  return { positon: props.positon };
}

class ItemsCell extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { rowIndex, field, data, ...props } = this.props;
    return (
        <Cell {...props}>
          <div className={(this.props.textAlign == null ? '' : this.props.textAlign) + ' text-cell'}>{data[rowIndex][field]}</div>
        </Cell>
    );
  }
}

class IconItemsCell extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { rowIndex, field, data, ...props } = this.props;
    return (
        <Cell {...props}>
          {}
          <Icon name={data[rowIndex].status_stock == 'N' ? 'warning circle' : 'check circle'} color={data[rowIndex].status_stock == 'N' ? 'yellow' : 'green'} />
        </Cell>
    );
  }
}

class Invoice extends Component {

  constructor(props) {
    super(props);
    this.state = {
      message_error_hidden: true,
      items: [],
      invoice_detail: '',
      start_date: moment(),
      end_date: moment(),
      is_clear_stock: false,
      is_clear_bill: false,
      total_item: 0,
      table_hiegh: Settings.table_hiegh
    }

    this.product_code_all = [];
    this.product_name_all = [];
    this.categories = [];
    this.product_types = [];
    this.handleClickMenu = this.handleClickMenu.bind(this);
  }

  componentDidMount() {
    this.setState({
      loader_active: true,
      total_item: 0
    });

    var s = '';
    if (this.state.end_date != null && this.state.end_date != '') {      
      s += (s == '' ? '' : '&') + 'end_date=' + Utility.formatDate2(this.state.end_date);
    }else{
      this.setState({'end_date': this.state.start_date})
      s += (s == '' ? '' : '&') + 'end_date=' + Utility.formatDate2(this.state.start_date);
    }
    if (this.state.start_date != null && this.state.start_date != '') {
      s += (s == '' ? '' : '&') + 'start_date=' + Utility.formatDate2(this.state.start_date);
    }

    if (this.state.branch_id != null && this.state.branch_id != '') {
      s += (s == '' ? '' : '&') + 'branch_id=' + this.state.branch_id;
    }
    if (this.state.vendor != null && this.state.vendor != '') {
      s += (s == '' ? '' : '&') + 'vendor=' + this.state.vendor;
    }

    if (this.state.is_clear_stock) {
      s += (s == '' ? '' : '&') + 'status_stock=N';
    }

    if (this.state.is_clear_bill) {
      s += (s == '' ? '' : '&') + 'status_bill=N';
    }

    var branches = Utility.getFetch(Settings.baseUrl + '/branches/');
    var vendors = Utility.getFetch(Settings.baseUrl + '/vendors/?is_enabled=1');
    var invoices = Utility.getFetch(Settings.baseUrl + '/invoices/?is_enabled=1&kind=' + (this.props.action == 'import' ? "IM" : "EX") + "&" + s);
    var products = Utility.getFetch(Settings.baseUrl + '/products/?is_enabled=1');
    Promise.all([branches, vendors, invoices, products]).then((values) => {
      this.branches = values[0];
      this.vendors = values[1];
      this.products = values[3];
      let branches = [];

      branches.push({
        key: 0,
        value: 0,
        text: '- ทั้งหมด -',
      });
      for (let i in values[0]) {
        if (values[0][i].is_enabled == 1)
          branches.push({
            key: values[0][i].id,
            value: values[0][i].id,
            text: values[0][i].name,
          });
      }

      let vendors = [];
      vendors.push({
        key: 0,
        value: 0,
        text: '- ทั้งหมด -',
      });
      for (let i in values[1]) {
        vendors.push({
          key: values[1][i].id,
          value: values[1][i].id,
          text: values[1][i].name,
        });
      }

      var items = this.setFieldValue(values[2]);
      let products_code = [];
      let products_name = [];
      for (let i in values[3]) {
        products_code.push({
          key: values[3][i].id,
          value: values[3][i].id,
          text: values[3][i].code,
        });
        products_name.push({
          key: values[3][i].id,
          value: values[3][i].id,
          text: values[3][i].name,
        });
      }
      var items = this.setFieldValue(values[2]);

      this.setState({
        products_code: products_code,
        products_name: products_name,
        products: this.products,
        branches: branches,
        vendors: vendors,
        items: items,
        loader_active: false,
        total_item: items.length
      });
    });

    let elHeight = document.getElementById('table_width')
    this.setState({ table_width: elHeight.clientWidth, table_hiegh: window.innerHeight - 400 });
    var url_string = window.location.toString();
    var url = new URL(url_string);
    var temp_start = url.searchParams.get("start_date");
    var temp_end = url.searchParams.get("end_date");
    if (temp_start !== null && temp_end !== null){
        this.setState({
              start_date: moment(temp_start,'DD/MM/YYYY'),
              end_date: moment(temp_end,'DD/MM/YYYY')
        })
    }
  }

  setFieldValue(item) {
    for (let i = 0; i < item.length; i++) {
      item[i]['status_stock_title'] = Settings.status_stock[item[i].status_stock]
      item[i]['status_bill_title'] = Settings.status_bill[item[i].status_bill]
      item[i]['update_date_title'] = item[i].status_stock == 'N' ? '' : Utility.formatDate(item[i].update_date)
      item[i]['invoice_date_title'] = Utility.formatDate(item[i].invoice_date)
      let v = Utility.getObject(this.vendors, item[i].vendor);
      item[i]['vendor_title'] = v != null ? v.name : '';
      item[i]['branche_title'] = Utility.getObject(this.branches, item[i].branch).name
    }
    return item;
  }

  clearFormSearch() {
    this.setState({
      //branch_id: '',
      //vendor: '',
      start_date: moment(),
      end_date: moment(),
      is_clear_stock: false,
      is_clear_bill: false
    })
  }

  onClickitem(e, data) {
    e.preventDefault();
    this.setState({
      open_invoice: true,
      invoice_detail: data,
      bill_number: data.number,
      modal_action: 'edit'
    });
  }

  handleClickMenu(e, data) {

    let item = this.state.items[data.positon];

    this.setState({
      open_invoice: true,
      invoice_detail: item,
      bill_number: item.number,
      modal_action: 'edit'
    });

  }

  render() {
    let items = this.state.items;
    return (

      <div className='invoice-box'>
        <Form size='small'>
          <Form.Group>
            <Form.Field width={6}>
              <Header floated='left' as='h2'>{this.props.action == 'import' ? 'นำเข้าทองใหม่' : 'นำออกทองใหม่'}</Header>
            </Form.Field>
            <Form.Field width={16}>
              <Button id='addproductimport' size='small' content={this.props.action == 'import' ? 'สร้างใบนำเข้าทอง' : 'สร้างใบนำออกทอง'} onClick={() => {
                this.setState({ open_invoice: true, modal_action: 'add' });
              }} floated='right' icon='plus' labelPosition='right' type='button' primary />
            </Form.Field>
          </Form.Group>
        </Form>
        <Form className='attached fluid ' size='small'>
          <Form.Group>
            <Form.Dropdown id='branch' width={4} value={this.state.branch_id} defaultValue={0} label='สาขา' search selection options={this.state.branches} onChange={(e, data) => {
              this.setState({ branch_id: data.value });
            }} />
            {this.props.action == 'export' ? '' : <Form.Dropdown id='vendor' width={4} defaultValue={0} value={this.state.vendor} label='ชื่อโรงงาน/ร้านส่ง' search selection options={this.state.vendors} onChange={(e, data) => {
              this.setState({ vendor: data.value });
            }} />}
            <Form.Field width={4}>
              <label>วันที่</label>
              <DatePicker
                id='startdate'
                dateFormat="DD/MM/YYYY"
                selected={this.state.start_date}
                onChange={(date) => {
                  this.setState({ start_date: date });
                }}
              />
            </Form.Field>
            <Form.Field width={4} >
              <label>ถึงวันที่</label>
              <DatePicker
                id='enddate'
                dateFormat="DD/MM/YYYY"
                selected={this.state.end_date}
                onChange={(date) => {
                  this.setState({ end_date: date });
                }}
              />
            </Form.Field>
            <Form.Field width={6}>
              <br />
              <Button id='searchtoday' content='ยกเลิก' floated='right' type='button' size='small' onClick={() => {
                this.clearFormSearch();
                setTimeout(()=>{
                  this.componentDidMount();
                },500)
              }}  ><Icon name='search' /> วันนี้</Button>
              <Button id='search' floated='right' type='button' size='small' onClick={() => {
                this.componentDidMount();
              }} ><Icon name='search' /> ค้นหา</Button>
            </Form.Field>
          </Form.Group>
          <Form.Group>
            <Form.Checkbox id='update' label='ยังไม่ปรับปรุงสต็อก' floated='right'
              value={this.state.is_clear_stock} onChange={(e, v) => {

                this.setState({
                  is_clear_stock: !this.state.is_clear_stock
                });
              }} />
            <Form.Checkbox id='clear' label='ยังไม่เคลียร์บิล' value={this.state.is_clear_bill} onChange={(e, v) => {
              this.setState({
                is_clear_bill: !this.state.is_clear_bill
              });
            }} />
          </Form.Group>
        </Form>
        {this.state.loader_active && <Dimmer active={this.state.loader_active} inverted>
          <Loader inverted content='Loading' />
        </Dimmer>}
        <div id="table_width">
          <Table
            rowsCount={items.length}
            rowHeight={35}
            headerHeight={35}
            width={this.state.table_width}
            height={this.state.table_hiegh}>
            <Column
              width={40}
              header={
                <Cell ></Cell>
              }
              cell={<OptionItemsCell delete={false} onClickMenu={this.handleClickMenu} />}
            />
            <Column

              header={<Cell></Cell>}
              cell={
                <IconItemsCell data={items} field="status_stock_title" />
              }
              width={30}
            />
            <Column

              header={<Cell>สถานะปรับปรุงสต็อก</Cell>}
              cell={
                <ItemsCell data={items} field="status_stock_title" />
              }
              width={150}
            />
            <Column

              header={<Cell>เลขที่บิล</Cell>}
              cell={
                <ItemsCell data={items} field="number" />
              }
              width={120}
            />
            <Column
              header={<Cell>วันที่</Cell>}
              cell={
                <ItemsCell data={items} field="invoice_date_title" />
              }
              width={120}
            />
            <Column
              header={<Cell>วันที่ปรับปรุงสต็อก</Cell>}
              cell={
                <ItemsCell data={items} field="update_date_title" />
              }
              width={120}
            />
            <Column
              header={<Cell>ชื่อโรงงาน/ร้านส่ง</Cell>}
              cell={
                <ItemsCell data={items} field="vendor_title" />
              }
              width={150}
            />
            <Column
              header={<Cell>สถานะเคลียร์บิล</Cell>}
              cell={
                <ItemsCell data={items} field="status_bill_title" />
              }
              width={150}
            />
            <Column
              header={<Cell>สาขา</Cell>}
              cell={
                <ItemsCell data={items} field="branche_title" />
              }
              width={150}
            />
            <Column
              header={<Cell>หมายเหตุ</Cell>}
              cell={
                <ItemsCell data={items} field="description" />
              }
              width={250}
            />
          </Table>
        </div>
        <br />
        <Form size='small' >
          <Form.Group>
            <Form.Field width={2}>
              <lable>จำนวน</lable>
              <Input id='amount' className='text-right' value={this.state.total_item} readOnly />
            </Form.Field>
            <Form.Field width={16}>
              <br />
              {this.state.open_invoice ?
                <BillModal
                  action={this.props.action}
                  modal_action={this.state.modal_action}
                  invoice_detail={this.state.invoice_detail}
                  branches={this.state.branches}
                  vendors={this.state.vendors}
                  products={this.state.products}
                  products_name={this.state.products_name}
                  products_code={this.state.products_code}
                  bill_number={this.state.bill_number}
                  open={true}

                  onNew={() => {
                    this.setState({ open_invoice: false });
                    this.componentDidMount();
                    setTimeout(() => {
                      this.setState({ open_invoice: true, modal_action: 'add' });
                    }, 500);

                  }}
                  onClose={() => {
                    this.setState({ open_invoice: false });
                    this.componentDidMount();
                  }}
                  onAddInvoice={(d) => {
                    if (d != null) {
                      this.setState({
                        invoice_detail: d,
                        modal_action: 'edit'
                      });
                    }
                    this.componentDidMount();
                  }}
                /> : ''}
            </Form.Field>
          </Form.Group>
        </Form>
      </div>
    );
  }
}


export default Invoice;
