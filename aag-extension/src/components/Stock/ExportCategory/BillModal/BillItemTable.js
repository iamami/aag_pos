/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, {Component} from 'react';
import {Table, Column, Cell} from 'fixed-data-table';
import ItemsCell from '../../ItemsCell'
import OptionItemsCell from '../../OptionItemsCell'
class BillItemTable extends Component {

    constructor(props){
        super(props)
        this.state = {
            table_width: 600,
        }
    }

    componentDidMount(){
        setTimeout(()=>{
            let table_width = document.getElementById('table_width2')
            if(table_width)
                this.setState({ table_width:table_width.clientWidth });
          },400)
    }

    render() {
        const items = this.props.items

        return (

            <div id="table_width2">
            <Table
                rowsCount={items.length}
                rowHeight={35}
                headerHeight={30}
                width={this.state.table_width}
                height={300}>
                <Column
                    header={< Cell > </Cell>}
                    cell={< OptionItemsCell edit={false} data = { items }  field = "status_stock_title" onClickMenu={this.props.onActionItemCell}/>}
                    width={50}/>
                <Column
                    header={< Cell > สถานะปรับปรุงสต็อก </Cell>}
                    cell={< ItemsCell data = {
                    items
                }
                field = "status_stock_title" />}
                    width={150}/>
                <Column
                    header={< Cell > กลุ่มสินค้า </Cell>}
                    cell={< ItemsCell  data = {
                    items
                }
                field = "category_title" />}
                    width={150}/>

                <Column
                    header={< Cell className = 'text-right' > น.น. </Cell>}
                    cell={< ItemsCell data = {
                    items
                }
                field = "weight" textAlign = 'text-right' />}
                    width={150}/>
                <Column
                    header={< Cell className = 'text-right' > ราคาเฉลี่ยบาทละ </Cell>}
                    cell={< ItemsCell data = {
                    items
                }
                field = "average" textAlign = 'text-right' />}
                    width={150}/>

                <Column
                    header={< Cell className = 'text-right' > ราคาที่ตัด / บาท </Cell>}
                    cell={< ItemsCell data = {
                    items
                }
                field = "price" textAlign = 'text-right' />}
                    width={150}/>
                <Column
                    header={< Cell className = 'text-right' > ราคาที่ตัดรวม </Cell>}
                    cell={< ItemsCell data = {
                    items
                }
                field = "total" textAlign = 'text-right' />}
                    width={150}/>
                <Column
                    header={< Cell className = 'text-right' > ส่วนต่างรวม </Cell>}
                    cell={< ItemsCell data = {
                    items
                }
                field = "total_diff" textAlign = 'text-right' />}
                    width={150}/>
                <Column
                    header={< Cell className = 'text-right' > ค่าหลอม / บาท </Cell>}
                    cell={< ItemsCell data = {
                    items
                }
                field = "cost" textAlign = 'text-right' />}
                    width={150}/>
            </Table></div>
        )
    }

}

export default BillItemTable