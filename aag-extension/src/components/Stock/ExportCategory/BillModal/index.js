/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import Utility from '../../../../Utility';
import Settings from '../../../../Settings';
import MsgInput from '../../../Error/MsgInput'
import AddProductModal from './AddProductModal'
import PaymentModal from '../../../Payment/PaymentModal'
import BillItemTable from './BillItemTable'
import {
  Form,Header, Button,Loader, Dimmer, Icon, Dropdown, Input, Modal, Grid
} from 'semantic-ui-react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import { connect } from 'react-redux'

class BillModal extends Component {

  constructor(props) {
    super(props);
    this.modal_status = false;
    this.state = {
      message_error_hidden: true,
      items: [],
      invoice_date: moment(),
      branch: '',
      vendor: '',
      status_stock: 'N',
      modal_open: this.props.open,
      product_action_title: 'เพิ่มสินค้า',
      product_action: 'add',
      item_invoice: [],
      button_update_stoick_disabled: true,
      button_save_disabled: false,
      button_update_stoick_loading: false,
      invoice_title: 'เพิ่มรายการนำออกทองเก่า',
      loader: false,
      status_bill: 'N',
      button_save_title: 'สร้าง',
      invoice_add_product_disabled: true,
      description: '',
      msg_error: {}
    }

    this.product_code_all = [];
    this.product_name_all = [];
    this.categories = [];
    this.product_types = [];
    this.onEditProduct = this.onEditProduct.bind(this);
    this.onDeleteProduct = this.onDeleteProduct.bind(this);
    
    this.handelActionItemCell = this.handelActionItemCell.bind(this)
    this.submitBillHandler = this.submitBillHandler.bind(this)
    this.handelSubmitProduct = this.handelSubmitProduct.bind(this)
  }

  handelSubmitSaveInvoice() {
    const {branch}=this.props.auth
    if (this.state.vendor == '') {
      alert('กรุณาเลือกโรงงาน/ร้านส่ง');
      return;
    }


    var formData = {
      branch: this.state.branch,
      vendor: this.state.vendor,
      ref_number: this.state.ref_number,
      bill_date: Utility.formatDate2(this.state.invoice_date),
      status_bill: 'N',
      status_stock: this.state.status_stock,
      kind: this.props.action == 'import' ? 'IM' : 'EX',
      description: this.state.description
    };

    this.setState({ button_save_loading: true })
    if (this.props.modal_action == 'add') {
      const url = Settings.baseUrl + '/bill_category/';
      Utility.post(url, formData, (status, data) => {
        this.setState({
          button_save_loading: false,
          invoice_add_product_disabled: false,
          invoice_disabled: true,
          button_save_title: 'บันทึก'
        })
        
        this.props.onAddInvoice(data);
        this.loadInvoiceItem()
      });
    } else {
      var formData2 = {
        branch: this.state.branch,
        vendor: this.state.vendor,
        ref_number: this.state.ref_number,
        invoice_date: this.state.invoice_date,
        status_bill: this.state.status_bill,
        description: this.state.description
      };

      const url = Settings.baseUrl + '/bill_category/' + this.props.invoice_detail.id + "/";
      Utility.put(url, formData2, (status, data) => {
        if (status) {
          this.setState({ button_save_loading: false })
          this.props.onAddInvoice(data);
          this.loadInvoiceItem()
        }
      });
    }
  }

  handelSubmitProduct(e,data) {
    
    let formData = {
      bill_category: this.props.invoice_detail.id,
      branch: this.props.invoice_detail.branch,
      stock: data.stock_category,
      weight: Utility.removeCommas(data.product_weight),
      average: Utility.removeCommas(data.average),
      total_average: data.total_average.toFixed(2),
      price: Utility.removeCommas(data.price_sell),
      total: Utility.removeCommas(data.total_sell),
      cost: Utility.removeCommas(data.cost),
      status_stock: 'N',
      vendor: this.state.vendor
    };

    this.setState({ button_product_loading: true })
    if (this.state.product_action == 'add') {
      const url = Settings.baseUrl + '/bill_category/' + this.props.invoice_detail.id + "/items/";
      Utility.post(url, formData, (status, data) => {
        if (status) {
          this.setState({ button_product_loading: false, modal_product: false })
          this.setState(data)
          this.loadInvoiceItem();
        } else { // error
          if(data.error){
            alert(data.error)
          }
          if (data.length !== 0)
            this.setState({
              msg_error: data
            })
        }
      });
    } else {
      const url = Settings.baseUrl + '/bill_category/' + this.state.item_id + '/items/' + this.state.item_id + '/';
      Utility.put(url, formData, (status, data) => {
        if (status) {
          this.setState({ button_product_loading: false, modal_product: false })
          this.loadInvoiceItem();
        } else { // error
          if (data.length !== 0)
            this.setState({
              msg_error: data
            })
        }
      });
    }
  }

  loadInvoiceItem() {

    var invoices = Utility.getFetch(Settings.baseUrl + '/bill_category/' + this.props.invoice_detail.id + "/");
    var items = Utility.getFetch(Settings.baseUrl + '/bill_category/' + this.props.invoice_detail.id + '/items/');
    this.setState({ loader: true });
    Promise.all([invoices, items]).then((values) => {
      // set invoice detail
      let item = values[0];
      this.setState({
        bill_number: item.bill_number,
        description: item.description,
        status_bill: item.status_bill,
        invoice_date: moment(item.invoice_date),
        branch: item.branch,
        vendor: item.vendor,
        invoice_disabled: true,
        item_invoice: this.setFieldValue(values[1]),
        button_save_title: 'บันทึก',
        status_stock: item.status_stock,
        button_update_stoick_disabled: values[1].length == 0 || item.status_stock == 'Y',
        invoice_add_product_disabled: item.status_stock == 'Y',
        invoice_title: <span>{<span>แก้ไขรายการนำออกทองเก่า</span>} </span>,
        loader: false
      });
    });
  }

  setFieldValue(item) {

    let invoice_weight_total = 0;
    let invoice_price_total = 0;
    let invoice_cost_total = 0;
    let invoice_net = 0;
    for (let i = 0; i < item.length; i++) {
      item[i]['status_stock_title'] = Settings.status_stock[item[i].status_stock]
      item[i]['category_title'] = item[i].stock.category.name
      item[i]['category_title'] = item[i].stock.category.name
      invoice_weight_total += parseFloat(item[i].weight);
      invoice_price_total += Math.ceil(parseFloat(item[i].total));
      invoice_cost_total += (item[i].cost * (item[i].weight/item[i].stock.category.weight));

      let d = item[i].stock.category.weight;
      item[i]['total_diff'] = Utility.priceFormat((parseFloat(item[i].price) - parseFloat(item[i].average)) * (item[i].weight / d))
      item[i]['average'] = Utility.priceFormat(item[i].average)
      item[i]['price'] = Utility.priceFormat(item[i].price)
      item[i]['total'] = Utility.priceFormat(item[i].total)
      item[i]['cost'] = Utility.priceFormat(item[i].cost)
    }
    this.setState({
      invoice_weight_total: Utility.priceFormat(invoice_weight_total),
      invoice_price_total: Utility.priceFormat(invoice_price_total),
      invoice_cost_total: Utility.priceFormat(invoice_cost_total),
      invoice_net: Math.ceil(parseFloat(invoice_price_total) - parseFloat(invoice_cost_total))
    });
    return item;
  }



  componentDidMount() {
    var self = this;
    this.setState({
      loader_active: true,
      btn_stock: true
    });

    if(this.props.modal_action=='edit'){
      this.loadInvoiceItem()
    }
  }

  onEditProduct(e, d) {
    let data = this.state.item_invoice[d.positon];
    this.setState({
      item_id: data.id,
      stock_category: data.stock.id,
      product_weight: data.weight,
      average: data.average,
      price_sell: Utility.removeCommas(data.price),
      total_sell: Utility.removeCommas(data.total),
      cost: Utility.removeCommas(data.cost),
      total_diff: data.total_diff,
      price_diff: parseFloat(Utility.removeCommas(data.price)) - parseFloat(Utility.removeCommas(data.average)),
      product_action: 'edit',
      product_action_title: 'แก้ไข'
    });
  }

  onDeleteProduct(e, d) {
    let data = this.state.item_invoice[d.positon];
    if (data.status_stock == 'N') {
      const url = Settings.baseUrl + '/bill_category/' + data.bill_category.id + '/items/' + data.id + '/';
      Utility.delete(url, (status, data) => {

        this.setState({ button_product_loading: false })
        this.loadInvoiceItem();
      });
    } else
      alert('ไม่สามารถลบรายการนี้ได้');
  }

  handelActionItemCell(e,d){

    if(d.action=='delete'){
        if(window.confirm('ยืนยันลบรายการนี้'))
        this.onDeleteProduct(e,d)
    }else{
        this.onEditProduct(e,d)
    }

  }

  submitUpdateStock(ledger) {

    this.setState({
      button_update_stoick_loading: true,
      payment_open: false
    })
    const url = Settings.baseUrl + '/bill_category/' + this.props.invoice_detail.id + '/stock/';
    Utility.get(url, (status, data) => {
      this.submitledger(ledger)

      this.props.onAddInvoice();
      this.loadInvoiceItem();

      this.setState({
        button_update_stoick_loading: false
      })
    });
  }

  submitBillHandler(ledger){

    if(window.confirm('ยืนยันบันทึกและอัพเดทสต็อก')){
      this.submitUpdateStock(ledger)
    }

  }
  submitledger(ledger) {
    
    ledger['object_number'] = this.props.invoice_detail.bill_number
    ledger['object_id'] = this.props.invoice_detail.id
    ledger['ledger_category'] = 5

    let url = Settings.baseUrl + "/ledger/";
    Utility.post(url, ledger, (s, d) => {

    });
  }
  render() {
    const {branch} = this.props.auth
    let items = this.state.item_invoice;
    const branch_option = Utility.getOptions(this.props.branches)
    return (

      <Modal size="large" open={true} /*dimmer='blurring'*/>
        <Button id='btnClose' circular icon='close' basic floated='right' name='' onClick={this.props.onClose} />
        <Modal.Header>
          <Header as='h4'>
            <Header.Content>
              {this.state.invoice_title} {this.state.bill_number&& <span id='textBillID'>[บิลเลขที่ {this.state.bill_number}]</span>}
                  <Header.Subheader>
                <span><Icon name={this.state.status_stock == 'N' ? 'warning circle' : 'check circle'} color={this.state.status_stock == 'N' ? 'yellow' : 'green'} />{Settings.status_stock[this.state.status_stock]}</span>
              </Header.Subheader>
            </Header.Content>
          </Header>
        </Modal.Header>
        <Modal.Content>
          {this.state.modal_product && <AddProductModal onClose={()=>this.setState({modal_product: false})} title={this.state.product_action_title} branch={this.props.invoice_detail.branch} onSubmit={this.handelSubmitProduct} />}
          <div >
            {this.state.payment_open && <PaymentModal
            ledger_category={5}
            total={this.state.invoice_net}
            onClose={()=>this.setState({payment_open: false})}
            onSubmit={this.submitBillHandler} 
             />}
            <div className='relative'>
            {this.state.loader && <Dimmer active={this.state.loader} inverted>
                <Loader inverted content='Loading' />
              </Dimmer>}
              <div>
                <Grid>
                  <Grid.Row>
                    <Grid.Column width={4}>
                      <Form className=' fluid' size='small' >
                        <Form.Field error={this.state.msg_error.branch != null} width={16} >
                          <label>สาขา <MsgInput text={this.state.msg_error.branch} /></label>
                          <Dropdown id='branch' search selection width={16} options={branch_option} value={this.state.branch} onChange={(e, data) => {
                            this.setState({ branch: data.value });
                          }} disabled={this.state.invoice_disabled} />
                        </Form.Field>
                        <br />
                        <Form.Dropdown id='vendor' label='ชื่อโรงงาน/ร้านส่ง' search selection width={16} options={this.props.vendors} value={this.state.vendor} defaultValue={this.state.vendor} onChange={(e, data) => {
                          this.setState({ vendor: data.value });
                        }} disabled={this.state.invoice_disabled || this.props.action == 'export'} /><br />

                        <Form.Field width={16}>
                          <label>วันที่</label>
                          <DatePicker
                            id='date'
                            dateFormat="DD/MM/YYYY"
                            selected={this.state.invoice_date}
                            onChange={(date) => {
                              this.setState({ invoice_date: date });
                            }}
                            disabled={this.state.invoice_disabled}
                          />
                        </Form.Field><br />

                        <Form.Checkbox id='btnClearBill' label='เคลียร์บิล' width={16} checked={this.state.status_bill == 'Y'} onChange={(e, v) => {
                          
                        
                          this.setState({
                            status_bill: v.checked ? 'Y' : 'N'
                          });
                        }} /><br />
                        <Form.TextArea label='หมายเหตุ' placeholder='' width={16} value={this.state.description} onChange={(e, v) => {
                          this.setState({ description: v.value })
                        }} />
                      </Form>

                    </Grid.Column>
                    <Grid.Column width={12}>
                      <Form size='small'>
                        <Form.Group>
                          <Form.Field width={6}>
                            <Header floated='left' as='h3'>รายการสินค้า</Header>
                          </Form.Field>
                          <Form.Field width={10}>
                            <Button id='add' disabled={this.state.invoice_add_product_disabled} size='small' content='เพิ่มรายการสินค้า' onClick={(e) => {
                              e.preventDefault();
                              this.setState({ modal_product: true })
                            }}
                              floated='right' icon='plus' labelPosition='left' type='button' color='green' />
                          </Form.Field>
                        </Form.Group>
                      </Form>
                            <BillItemTable items={items} table_width={this.state.table_width} onActionItemCell={this.handelActionItemCell} />
                      <br />
                      <Form className='fluid' size='small'>
                        <Form.Group >
                          <Form.Input id='weighttotal' label='น้ำหนักรวม' placeholder='' className='text-right' width={4} value={this.state.invoice_weight_total} readOnly />
                          <Form.Input id='pricetotal' label='ราคาที่ตัดรวม' placeholder='' className='text-right' width={4} value={this.state.invoice_price_total} readOnly />
                          <Form.Input id='costtotal' label='ค่าหลอมรวม' placeholder='' className='text-right' width={4} value={this.state.invoice_cost_total} readOnly />
                          <Form.Input id='invoicenet' label='ราคาสุทธิ' placeholder='' className='text-right' width={4} value={Utility.priceFormat(this.state.invoice_net)} readOnly />
                        </Form.Group>
                      </Form>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </div>
            </div>
          </div>

        </Modal.Content>
        <Modal.Actions>
          <Button
            id='save'
            size='small'
            primary
            icon='save'
            labelPosition='left'
            onClick={(e) => {
              e.preventDefault();
              this.handelSubmitSaveInvoice();
            }}
            className={this.state.button_save_loading ? 'loading' : ''}
            content={this.state.button_save_title} />
          <Button 
            id='btnUpdate'
            size='small' 
            icon='lightning'
            color='red' 
            onClick={(e) => this.setState({payment_open:true})}
            className={this.state.button_update_stoick_loading ? 'loading' : ''}
            disabled={this.state.button_update_stoick_disabled}
            content='บันทึกและอัพเดทสต๊อก' />

        </Modal.Actions>
      </Modal>
    )
  }

}

const mapStateToProps = state =>{
  return ({
    auth: state.auth,
    branches: state.branches
  })
}
export default connect(
  mapStateToProps,
)(BillModal)