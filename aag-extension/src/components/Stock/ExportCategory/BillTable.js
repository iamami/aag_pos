/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, {Component} from 'react';
import {Table, Column, Cell} from 'fixed-data-table';
import ItemsCell from '../ItemsCell'
import OptionItemsCell from '../OptionItemsCell'

import {Icon } from 'semantic-ui-react';

class IconItemsCell extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { rowIndex, field, data, ...props } = this.props;
    return (
        <Cell {...props}>
          {}
          <Icon name={data[rowIndex].status_stock == 'N' ? 'warning circle' : 'check circle'} color={data[rowIndex].status_stock == 'N' ? 'yellow' : 'green'} />
        </Cell>
    );
  }
}

class BillTable extends Component {

    constructor(props){
        super(props)
    }

    render() {
        const items = this.props.items

        return (
            
<Table
          rowsCount={items.length}
          rowHeight={35}
          headerHeight={30}
          width={this.props.table_width}
          height={300}>
          <Column
            width={40}
            header={
              <Cell ></Cell>
            }
            cell={<OptionItemsCell delete={false} onClickMenu={this.props.onActionItemCell} />}
          />
          <Column

            header={<Cell></Cell>}
            cell={
              <IconItemsCell data={items} field="status_stock_title" />
            }
            width={30}
          />
          <Column

            header={<Cell>สถานะปรับปรุงสต็อก</Cell>}
            cell={
              <ItemsCell data={items} field="status_stock_title" />
            }
            width={150}
          />
          <Column
            header={<Cell>เลขที่บิล</Cell>}
            cell={
              <ItemsCell id='tablebill' data={items} field="bill_number" />
            }
            width={120}
          />
          <Column
            header={<Cell className='text-right'>น้ำหนักทองเก่ารวม</Cell>}
            cell={
              <ItemsCell data={items} field="total" textAlign='text-right' />
            }
            width={120}
          />
          <Column
            header={<Cell className='text-center'>วันที่</Cell>}
            cell={
              <ItemsCell data={items} field="invoice_date_title" className='text-center' />
            }
            width={150}
          />
          <Column
            header={<Cell className='text-center'>วันที่ปรับปรุงสต็อก</Cell>}
            cell={
              <ItemsCell id='dateUpdate' data={items} field="update_date_title" className='text-center' />
            }
            width={150}
          />
          <Column
            header={<Cell>สถานะเคลียร์บิล</Cell>}
            cell={
              <ItemsCell data={items} field="status_bill_title" />
            }
            width={150}
          />
          <Column
            header={<Cell>สาขา</Cell>}
            cell={
              <ItemsCell data={items} field="branche_title" />
            }
            width={150}
          />
          <Column
            header={<Cell>หมายเหตุ</Cell>}
            cell={
              <ItemsCell data={items} field="description" />
            }
            width={250}
          />
        </Table>
        )
    }

}

export default BillTable

