/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import {
   Button, Modal,Table
} from 'semantic-ui-react';
/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import Utility from '../../../Utility';

class GroupRow extends Component {
    render() {
        const textRight = {
            'text-align': 'right'
        }

        const textLeft = {
            'text-align': 'left'
        }
        const textCenter = {
            'text-align': 'center'
        }
        const textU = {
            'text-decoration': 'underline'
        }
        const data = this.props.data
        let kind = {}
        let kind_arr = []
        for (let i in data.items) {
            let item = Utility.cloneObjectJson(data.items[i])
            if (kind[item.product.kind.id] == null)
                kind[item.product.kind.id] = {
                    items: [],
                    kind_name: item.product.kind.name,
                    n: 0,
                    weight: 0
                }
            kind[item.product.kind.id].items.push(item)
        }
        for (let i in kind) {
            let is_title = true
            for (let j in kind[i].items) {

                if (is_title)
                    kind_arr.push({
                        is_title: is_title,
                        title: kind[i].kind_name
                    });
                is_title = false
                kind_arr.push(kind[i].items[j])
            }

        }
        return (<Table.Body>
            <Table.Row>
                <Table.HeaderCell colSpan='2' style={textLeft}>{data.category.name}</Table.HeaderCell>
                <Table.HeaderCell colSpan='5'></Table.HeaderCell>
            </Table.Row>
            {kind_arr.map((row, i) => row.is_title ? <Table.Row>
                <Table.HeaderCell ></Table.HeaderCell>
                <Table.HeaderCell style={textLeft}>{row.title}</Table.HeaderCell>
                <Table.HeaderCell colSpan='5'></Table.HeaderCell>
            </Table.Row> : <Table.Row>
                    <Table.HeaderCell ></Table.HeaderCell>
                    <Table.HeaderCell style={textLeft}>&nbsp;&nbsp;{row.product.name}</Table.HeaderCell>
                    <Table.HeaderCell style={textCenter}>{row.product.code}</Table.HeaderCell>
                    <Table.HeaderCell style={textRight}>{Utility.weightFormat(row.product.weight)}</Table.HeaderCell>
                    <Table.HeaderCell style={textRight}>{row.amount}</Table.HeaderCell>
                    <Table.HeaderCell style={textRight}>{Utility.weightFormat(Utility.removeCommas(row.amount) * Utility.removeCommas(row.product.weight))}</Table.HeaderCell>
                    <Table.HeaderCell style={textCenter}>&nbsp;&nbsp;{row.branch.name}</Table.HeaderCell>
                </Table.Row>)}
            <Table.Row>
                <Table.HeaderCell colSpan='4' style={textRight}></Table.HeaderCell>
                <Table.HeaderCell style={textRight}><div style={textU}>{data.n}</div></Table.HeaderCell>
                <Table.HeaderCell style={textRight}><div style={textU}>{Utility.weightFormat(data.weight)}</div></Table.HeaderCell>
                <Table.HeaderCell ></Table.HeaderCell>
            </Table.Row>
        </Table.Body>)
    }
}

class ProductDetailPrintPreview extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        let data = this.props.data

        let title = 'รายงานสต็อกสินค้า';
        let filename = 'stock-prodcut';

        const divStyle = {
            color: '#111',
            'font-size': '10px',
            width: '100%',
            'line-height': '20px',
            'font-family': 'monospace'
        };
        const textRight = {
            'text-align': 'right'
        }

        const textLeft = {
            'text-align': 'left'
        }
        const textCenter = {
            'text-align': 'center'
        }
        const textU = {
            'text-decoration': 'underline'
        }
        let category = {}
        let category_arr = []
        let n=0
        let weight = 0
        for (let i in this.props.items) {
            const item = Utility.cloneObjectJson(this.props.items[i])
            if (category[item.product.category.id] == null)
                category[item.product.category.id] = {
                    items: [],
                    category: item.product.category,
                    n: 0,
                    weight: 0
                }
            category[item.product.category.id].weight += (parseInt(item.amount) * parseFloat(item.product.weight))
            category[item.product.category.id].n += parseInt(item.amount)
            category[item.product.category.id].items.push(item)
            weight+=(parseInt(item.amount) * parseFloat(item.product.weight))
            n+= parseInt(item.amount)
        }
        for (let i in category) {
            category_arr.push(category[i])
        }
        //category=null

        return (<div>
            <Modal open={true} onClose={this.close} size='fullscreen' /*dimmer='blurring'*/>
                <Modal.Header>Preview</Modal.Header>
                <Modal.Content className='scrolling'>

                    <div id='view-print'>
                        <div id='paperA4-portrait'>
                            <Table basic id='table-to-xls' style={divStyle}>
                                <Table.Header>
                                    <Table.Row >
                                        <Table.HeaderCell colSpan='7'><center>{title}</center></Table.HeaderCell>
                                    </Table.Row>
                                    <Table.Row>
                                        <Table.HeaderCell style={textCenter}></Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter}></Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter}>รหัสสินค้า</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter}>น.น./ชิ้น</Table.HeaderCell>
                                        <Table.HeaderCell style={textRight}>จำนวน</Table.HeaderCell>
                                        <Table.HeaderCell style={textRight}>น้ำหนักรวม(กรัม)</Table.HeaderCell>
                                        <Table.HeaderCell style={textCenter}>สาขา</Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>
                                {category_arr.map((row, i) => <GroupRow key={i} data={row} />)}

                                <Table.Row>
                                    <Table.HeaderCell colSpan='4' style={textRight}></Table.HeaderCell>
                                    <Table.HeaderCell style={textRight}><div style={textU}>{n}</div></Table.HeaderCell>
                                    <Table.HeaderCell style={textRight}><div style={textU}>{Utility.weightFormat(weight)}</div></Table.HeaderCell>
                                    <Table.HeaderCell ></Table.HeaderCell>
                                </Table.Row>
                            </Table>
                        </div>
                    </div>
                </Modal.Content>
                <Modal.Actions>
                    <ReactHTMLTableToExcel
                        id="test-table-xls-button"
                        className="ui primary button small"
                        table="table-to-xls"
                        filename={filename}
                        sheet={title}
                        buttonText="Download as XLS" >
                    </ReactHTMLTableToExcel>

                    <Button id='btnPrint' primary icon='print' size='small' onClick={() => {
                        var mywindow = window.open('', 'PRINT', 'height=600,width=1024');
                        mywindow.document.write('<html><head><title>' + title + '</title>');
                        mywindow.document.write('</head><body style="font-size: 8px;font-family: Lato,\'Helvetica Neue\',Arial,Helvetica,sans-serif">');
                        mywindow.document.write(document.getElementById('view-print').innerHTML);
                        mywindow.document.write('</body></html>');
                        mywindow.document.close(); // necessary for IE >= 10
                        mywindow.focus(); // necessary for IE >= 10*/
                        mywindow.print();
                        mywindow.close();
                    }} labelPosition='right' content='Print' />

                    <Button id='btnClose' size='small' type='button' onClick={() => { this.props.onClose() }}>ปิด</Button>
                </Modal.Actions>
            </Modal>
        </div>)
    }
}

export default ProductDetailPrintPreview;