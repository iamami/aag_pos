/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import Utility from '../../../Utility';
import Settings from '../../../Settings';
import {
    Table,
    Column,
    Cell
} from 'fixed-data-table';

import {
    Form,Button, Modal
} from 'semantic-ui-react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';

class ItemsCell extends Component {
    constructor(props) {
        super(props);

    }
    render() {
        const { rowIndex, field, data, ...props } = this.props;
        return (
            <Cell {...props} >

                <div className={this.props.textAlign}><span className={this.props.color}>{data[rowIndex][field]}</span></div>
            </Cell>
        );
    }
}


class ReportDetail extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            item: [],
            start_date: moment().add('-60', 'days'),
            end_date: moment()
        }

        this.submitSearch = this.submitSearch.bind(this)
    }

    submitSearch(e){
        e.preventDefault()
        this.componentDidMount()
    }

    componentDidMount() {

        this.setState({
            loader_active: true
        });

        let q = Utility.jsonToQueryString({
            start_date: Utility.formatDate2(this.state.start_date),
            end_date: Utility.formatDate2(this.state.end_date)
        })
        Utility.get(Settings.baseUrl + '/stock_product/' + this.props.id + '/items?'+q, (status, data) => {

            if (status == true) {
                this.setState({
                    data: this.setValue2(data),
                    loader_active: false
                });
            }
        });
    }

    setValue2(data) {
        for (let i = 0; i < data.length; i++) {
          data[i].record_date = Utility.formatDate(data[i].record_date)
          data[i].branch_name = data[i].branch.name
          data[i].product_code = data[i].product.code
          data[i].product_name = data[i].product.name
          data[i].product_kind_name = data[i].product.kind.name
    
          data[i].after_w = Utility.weightFormat(parseFloat(data[i].product.weight) * parseInt(data[i].after))
          data[i].before_w = Utility.weightFormat(parseFloat(data[i].product.weight) * parseInt(data[i].before))
          if (data[i].kind == 'I' || data[i].kind == 'V' || data[i].kind == 'TI') {
            data[i].amount_in = data[i].amount
            data[i].amount_in_w = Utility.weightFormat(parseFloat(data[i].product.weight) * parseInt(data[i].amount))
          } else {
            data[i].amount_out_w = Utility.weightFormat(parseFloat(data[i].product.weight) * parseInt(data[i].amount))
            data[i].amount_out = data[i].amount
          }
        }
        return data;
      }

    componentWillMount() { 
        setTimeout(()=>{

            let elHeight = document.getElementById('table_width2')
            this.setState({ table_width: elHeight.clientWidth }); 
        },200)
    }

    render() {

        const data = this.state.data;
        return (

            <Modal size='large' open={this.props.open} /*dimmer='blurring'*/>

            <Button id='btnClose' circular icon='close' title='ปิด' basic floated='right' name='' onClick={(e) => {
                e.preventDefault();
                this.props.onClose();
              }} />
                <Modal.Header>สต็อกการ์ดทอง - {this.props.name}</Modal.Header>
                
                <Modal.Content className=''>
                    <Modal.Description>
                        <Form className='fluid ' size='small' onSubmit={this.submitSearch}>
                            <Form.Group>
                                <Form.Field >
                                    <label>จากวันที่</label>
                                    <DatePicker
                                        id='dateStart'
                                        dateFormat="DD/MM/YYYY"
                                        value={this.state.start_date}
                                        selected={this.state.start_date}
                                        onChange={(date) => {
                                            this.setState({ start_date: date });
                                        }}
                                    />
                                </Form.Field>
                                <Form.Field >
                                    <label>ถึงวันที่</label>
                                    <DatePicker
                                        id='dateEnd'
                                        dateFormat="DD/MM/YYYY"
                                        value={this.state.end_date}
                                        selected={this.state.end_date}
                                        onChange={(date) => {
                                            this.setState({ end_date: date });
                                        }}
                                    />
                                </Form.Field>
                                <Form.Field width={5}>
                                    <br />
                                    <Button id='btnSearch' >ค้นหา</Button>
                                </Form.Field>
                            </Form.Group>

                        </Form>
                        <div id="table_width2">
                        <Table
                            rowsCount={data.length}
                            rowHeight={35}
                            headerHeight={30}
                            width={this.state.table_width}
                            height={500} >
                            <Column
                                header={<Cell>วันที่</Cell>}
                                cell={
                                    <ItemsCell data={data} field="record_date" />
                                }
                                width={80}
                            />
                            <Column
                                header={<Cell>สาขา</Cell>}
                                cell={
                                    <ItemsCell data={data} field="branch_name" />
                                }
                                width={150}
                            />
                            <Column
                                header={<Cell>เลขที่บิลอ้างอิง</Cell>}
                                cell={
                                    <ItemsCell id='billID' data={data} field="object_number" />
                                }
                                width={150}
                            />
                            <Column
                                header={<Cell>ประเภท</Cell>}
                                cell={
                                    <ItemsCell data={data} field="kind_display" />
                                }
                                width={120}
                            />
                            <Column
                                header={<Cell>รหัสสินค้า</Cell>}
                                cell={
                                    <ItemsCell data={data} field="product_code" />
                                }
                                width={120}
                            />
                            <Column
                                header={<Cell>ชื่อสินค้า</Cell>}
                                cell={
                                    <ItemsCell data={data} field="product_name" />
                                }
                                width={150}
                            />
                            <Column
                                header={<Cell className='text-right'>จ.น. เริ่มต้น</Cell>}
                                cell={
                                    <ItemsCell data={data} field="before" textAlign='text-right' color='pink' />
                                }
                                width={120}
                            />
                            <Column
                                header={<Cell className='text-right'>จ.น. เข้า</Cell>}
                                cell={
                                    <ItemsCell data={data} field="amount_in" textAlign='text-right' color='blue' />
                                }
                                width={120}
                            />
                            <Column
                                header={<Cell className='text-right'>จ.น. ออก</Cell>}
                                cell={
                                    <ItemsCell data={data} field="amount_out" textAlign='text-right' color='red' />
                                }
                                width={120}
                            />
                            <Column
                                header={<Cell className='text-right'>จ.น. คงเหลือ</Cell>}
                                cell={
                                    <ItemsCell data={data} field="after" textAlign='text-right' color='green' />
                                }
                                width={120}
                            />

                            <Column
                                header={<Cell className='text-right'>น.น. เริ่มต้น</Cell>}
                                cell={
                                    <ItemsCell data={data} field="before_w" textAlign='text-right' color='pink' />
                                }
                                width={150}
                            />
                            <Column
                                header={<Cell className='text-right'>น.น. เข้า</Cell>}
                                cell={
                                    <ItemsCell data={data} field="amount_in_w" textAlign='text-right' color='blue' />
                                }
                                width={150}
                            />
                            <Column
                                header={<Cell className='text-right'>น.น. ออก</Cell>}
                                cell={
                                    <ItemsCell data={data} field="amount_out_w" textAlign='text-right' color='red' />
                                }
                                width={150}
                            />
                            <Column
                                header={<Cell className='text-right'>น.น. คงเหลือ</Cell>}
                                cell={
                                    <ItemsCell data={data} field="after_w" textAlign='text-right' color='pink' />
                                }
                                width={150}
                            />
                        </Table></div>
                    </Modal.Description>

                </Modal.Content>
            </Modal>


        );
    }
}


export default ReportDetail;