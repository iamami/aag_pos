/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import Utility from '../../../Utility';
import Settings from '../../../Settings';
import StockProdcutDetail from './StockProdcutDetail';
import StockProductPrintPreview from './StockProductPrintPreview'
import ImportStockProduct from '../../Import/ImportStockProduct';
import {
  Table,
  Column,
  Cell
} from 'fixed-data-table';
import {
  Form,
  Input,
  Button,
  Header,
  Dimmer, 
  Loader, 
  Dropdown
} from 'semantic-ui-react';
import moment from 'moment';

class ItemsCell extends Component {
  constructor(props) {
    super(props);

  }
  render() {
    const { rowIndex, field, data, ...props } = this.props;
    return (
      <Cell {...props} onClick={() => {
        this.props.onContextMenu(data[rowIndex].id,data[rowIndex].product_name);
      }} className="cell-time-click">
        <div className={this.props.textAlign}>{data[rowIndex][field]}</div>
      </Cell>
    );
  }
}

class FormSearch extends Component {

  constructor(props) {
    super(props);
    this.state = {
      s_date: moment(),
      e_date: moment(),
      start_date: Utility.formatDate(moment()),
      end_date: Utility.formatDate(moment()),
      branch: 0,
      category: 0,
      product: 0,
      kind: 0,
      type_sale: 0,
      weight_b: 0
    }
    
    this.handlerSearch = this.handlerSearch.bind(this);
    this.handlerDropdownSearch = this.handlerDropdownSearch.bind(this);
    this.submitSearch = this.submitSearch.bind(this);
    this.resetForm = this.resetForm.bind(this);
  }

  resetForm(event) {
    event.preventDefault();
    for (let i in this.state) {
      this.setState({
        [i]: 0
      });
    }
    this.setState({
      weight: '',
      weight_b: 0,
      s_date: moment(),
      e_date: moment(),
      start_date: Utility.formatDate(moment()),
      end_date: Utility.formatDate(moment())
    })

    delete this.state.s_date;
    delete this.state.e_date;
    this.props.onSubmitSearch({
      start_date: Utility.formatDate(moment()),
      end_date: Utility.formatDate(moment()),
    });
  }
  handlerDropdownSearch(event, value) {
    this.setState({
      [value.name]: value.value
    });
  }

  submitSearch(event) {
    event.preventDefault();

    delete this.state.s_date;
    delete this.state.e_date;

    let v = this.state;
    for (let i in v) {
      if (v[i] == '' || v[i].value == 0)
        delete v[i]
    }

    this.props.onSubmitSearch(v);
  }

  handlerSearch(event) {
    const target = event.target;
    const value = target.type == 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }

  render() {
    return (
      <Form className='attached fluid' size='small' onSubmit={this.submitSearch}>
        <Form.Group>
          <Form.Dropdown id='dropDownBranch' label='สาขา' placeholder='สาขา' search selection width={5} name='branch' value={this.state.branch}
            options={this.props.value.state.branches} onChange={this.handlerDropdownSearch} />
          <Form.Dropdown id='dropDownGropProduct' label='กลุ่มสินค้า' placeholder='กลุ่มสินค้า' search selection width={5} name='category' value={this.state.category}
            options={this.props.value.state.categories} onChange={this.handlerDropdownSearch} />
          <Form.Dropdown id='dropDownProductID' label='รหัสสินค้า' placeholder='รหัสสินค้า' search selection width={5} name='product' value={this.state.product}
            options={this.props.value.state.prodcut_code} onChange={this.handlerDropdownSearch} />
          <Form.Dropdown id='dropDownProductName' label='ชื่อสินค้า' placeholder='ชื่อสินค้า' search selection width={5} name='product' value={this.state.product}
            options={this.props.value.state.prodcut_name} onChange={this.handlerDropdownSearch} />
          <Form.Field width={5}>
            <br />
            <Button id='btnSearch' >ค้นหา</Button>
          </Form.Field>
        </Form.Group>
        <Form.Group>

          <Form.Dropdown id='dropDownProductType' label='ประเภทสินค้า' placeholder='ประเภทสินค้า' search selection width={5} name="kind" value={this.state.kind}
            options={this.props.value.state.product_type}
            onChange={this.handlerDropdownSearch} />
          <Form.Field width={5}>
            <label>ประเภทงานขาย</label>
            <Dropdown id='dropDownSaleType' placeholder='ประเภทงานขาย' search selection defaultValue={this.props.value.state.type_sale}
              value={this.state.type_sale} options={Settings.type_sale}
              onChange={this.handlerDropdownSearch} name='type_sale' />
          </Form.Field>
          <Form.Field width={5}>
            <label>น้ำหนัก บาท</label>
            <Dropdown id='dropDownWeightB' placeholder='น้ำหนัก บาท' search selection defaultValue={this.props.value.state.weight_b} value={this.state.weight_b} options={Settings.weight} onChange={(e, v) => {

              this.setState({
                weight: (parseFloat(v.value) * 15.2).toFixed(3),
                weight_b: v.value
              })
            }} name='weight' />
          </Form.Field>
          <Form.Field width={5}>
            <label>น้ำหนัก กรัม</label>
            <Input id='inputWeightF' placeholder='น้ำหนัก กรัม' search selection value={this.state.weight} onChange={this.handlerSearch} name='weight' />
          </Form.Field>

          <Form.Field width={5}>
            <br />
            <Button id='btnAll' onClick={this.resetForm} >ทั้งหมด</Button>
          </Form.Field>
        </Form.Group>

      </Form>)
  }
}


class StockProduct extends Component {

  constructor(props) {
    super(props);
    this.state = {
      items: [],
      data: [],
      table_hiegh: Settings.table_hiegh
    }
    this.submitData = this.submitData.bind(this);
    this.onContextMenu = this.onContextMenu.bind(this);
    this.handlerSearch = this.handlerSearch.bind(this)
  }

  onContextMenu(id,name) {   

    this.setState({
      id: id,
      name: name
    })

    this.setState({
      open_report: true
    })
  }

  setValue2(data) {
    for (let i = 0; i < data.length; i++) {
      data[i].record_date = Utility.formatDate(data[i].record_date)
      data[i].branch_name = data[i].branch.name
      data[i].product_code = data[i].product.code
      data[i].product_name = data[i].product.name
      data[i].product_kind_name = data[i].product.kind.name

      data[i].after_w = Utility.weightFormat(parseFloat(data[i].product.weight) * parseInt(data[i].after))
      data[i].before_w = Utility.weightFormat(parseFloat(data[i].product.weight) * parseInt(data[i].before))
      if (data[i].kind == 'I') {
        data[i].amount_in = data[i].amount
        data[i].amount_in_w = Utility.weightFormat(parseFloat(data[i].product.weight) * parseInt(data[i].amount))
      } else {
        data[i].amount_out_w = Utility.weightFormat(parseFloat(data[i].product.weight) * parseInt(data[i].amount))
        data[i].amount_out = data[i].amount
      }
    }
    return data;
  }


  submitData() {

  }

  handlerSearch(data) {

    console.log(data);
    this.search = data;
    let qrstring = Utility.jsonToQueryString(data);
    var stock_product = Utility.getFetch(Settings.baseUrl + '/stock_product/?is_enabled=1&' + qrstring);
    Promise.all([stock_product]).then((values) => {
      this.stock_product = values[0];
      let items = [];

      for (let i in values[0]) {
        let item = values[0][i];
        console.log(item);
        if ((this.search.product == null || item.product.id == this.search.product) &&
          (this.search.category == null || item.product.category.id == this.search.category) &&
          (this.search.type_sale == null || item.product.type_sale == this.search.type_sale) &&
          (this.search.weight == null || item.product.weight == this.search.weight) &&
          (this.search.kind == null || item.product.kind.id == this.search.kind)) {
          items.push(item)
        }

      }

      this.setState({
        loader_active: false,
        items: this.setFieldValue(items)
      });
    });
  }

  componentDidMount() {
    var self = this;
    this.setState({
      loader_active: true
    });

    var s = '';
    if (this.state.search_weight != null && this.state.search_weight != '') {
      s += (s == '' ? '' : '&') + 'weight=' + (this.state.search_weight * 15.2).toFixed(3);
    }

    var stock_product = Utility.getFetch(Settings.baseUrl + '/stock_product/?is_enabled=1&' + s);
    var branches = Utility.getFetch(Settings.baseUrl + '/branches/');
    var categories = Utility.getFetch(Settings.baseUrl + '/categories/');
    var product_type = Utility.getFetch(Settings.baseUrl + '/product_types/')
    var products = Utility.getFetch(Settings.baseUrl + '/products/')
    Promise.all([stock_product, branches, categories, product_type, products]).then((values) => {
      this.stock_product = values[0];

      this.branches = values[1];
      let branches = [{
        value: 0,
        key: 0,
        text: '- ทั้งหมด -'
      }]
      for (let i in this.branches) {

        if (this.branches[i].is_enabled == 1)
        branches.push({
          value: this.branches[i].id,
          key: this.branches[i].id,
          text: this.branches[i].name
        });
      }

      this.categories = values[2];
      let categories = [{
        value: 0,
        key: 0,
        text: '- ทั้งหมด -'
      }]
      for (let i in this.categories) {
        categories.push({
          value: this.categories[i].id,
          key: this.categories[i].id,
          text: this.categories[i].name
        });
      }

      this.product_type = values[3];
      let product_type = [{
        value: 0,
        key: 0,
        text: '- ทั้งหมด -'
      }]
      for (let i in this.product_type) {
        product_type.push({
          value: this.product_type[i].id,
          key: this.product_type[i].id,
          text: this.product_type[i].name
        });
      }
      this.prodcuts = values[4];
      let prodcuts_code = [{
        value: 0,
        key: 0,
        text: '- ทั้งหมด -'
      }]
      for (let i in this.prodcuts) {
        prodcuts_code.push({
          value: this.prodcuts[i].id,
          key: this.prodcuts[i].id,
          text: this.prodcuts[i].code
        });
      }
      let prodcuts_name = [{
        value: 0,
        key: 0,
        text: '- ทั้งหมด -'
      }]
      for (let i in this.prodcuts) {
        prodcuts_name.push({
          value: this.prodcuts[i].id,
          key: this.prodcuts[i].id,
          text: this.prodcuts[i].name
        });
      }


      this.setState({
        loader_active: false,
        items: this.setFieldValue(values[0]),
        branches: branches,
        categories: categories,
        product_type: product_type,
        prodcut_code: prodcuts_code,
        prodcut_name: prodcuts_name,

      });
      let elHeight = document.getElementById('table_width')
    this.setState({ table_width: elHeight.clientWidth, table_hiegh: window.innerHeight - 380 });
    });
  }

  getTypeSale(key) {
    let type = Settings.type_sale.filter((o) => { return o.value == key });
    return type[0];
  }

  setFieldValue(items) {

    for (let i = 0; i < items.length; i++) {
      items[i]['in_date_title'] = Utility.formatDate(items[i].in_date)
      items[i]['out_date_title'] = Utility.formatDate(items[i].out_date)
      items[i]['product_name'] = items[i].product.name
      items[i]['product_code'] = items[i].product.code
      items[i]['branch_name'] = items[i].branch.name
      items[i]['product_kind'] = items[i].product.kind.name
      items[i]['product_weight'] = Utility.weightFormat(items[i].product.weight)
      items[i]['product_category'] = items[i].product.category.name;
      items[i]['amount_title'] = Utility.numberFormat(parseInt(items[i].amount))
      items[i]['weight_total_title'] = Utility.weightFormat(parseInt(items[i].amount) * parseFloat(items[i].product.weight))
      items[i]['product_type_sale'] = this.getTypeSale(items[i].product.type_sale).text
      items[i]['product_price_tag'] = items[i].product.price_tag
    }

    return items;
  }

  componentWillMount() { }
  render() {
    const items = this.state.items;
    return (
      <div id="table_width">
      {this.state.import_open?<ImportStockProduct
      onClose={()=>{
        this.setState({
          import_open: false,
        });
      }}
      onUpdate={()=>{
        this.setState({
          import_open: false,
        });
        this.componentDidMount()
      }} />:''}
      <Form size='small'>
        <Form.Group>
          <Form.Field width={6}>
            <Header floated='left' as='h2'>สต็อกทองใหม่</Header>
          </Form.Field>
          <Form.Field width={10}>
              <Button id='btnImport' size='small' content='Import' onClick={(e) => {
                  e.preventDefault();
                  this.setState({
                    import_open: true,
                  });
                }} floated='right'
                icon='file alternate outline' labelPosition='left' type='button' primary />
          </Form.Field>
        </Form.Group>
      </Form>

        {this.state.loader_active && <Dimmer className={this.state.loader_active ? 'active' : ''} inverted>
          <Loader content='Loading' inverted />
        </Dimmer>}
        <FormSearch value={this} onSubmitSearch={this.handlerSearch} />
        <Table
          rowsCount={items.length}
          rowHeight={35}
          headerHeight={35}
          width={this.state.table_width}
          height={this.state.table_hiegh}>
          <Column

            header={<Cell>สาขา</Cell>}
            cell={
              <ItemsCell id='rowBranch' data={items} field="branch_name" onContextMenu={this.onContextMenu} />
            }
            width={150}
          />
          <Column
            header={<Cell>กลุ่มสินค้า</Cell>}
            cell={
              <ItemsCell data={items} field="product_category" onContextMenu={this.onContextMenu} />
            }
            width={120}
          />
          <Column
            header={<Cell>ประเภทสินค้า</Cell>}
            cell={
              <ItemsCell data={items} field="product_kind" onContextMenu={this.onContextMenu} />
            }
            width={120}
          />
          <Column
            header={<Cell className="text-right">น.น./ชิ้น(g.)</Cell>}
            cell={
              <ItemsCell data={items} field="product_weight" textAlign='text-right' onContextMenu={this.onContextMenu} />
            }
            width={120}
          />

          <Column
            header={<Cell>ชื่อสินค้า</Cell>}
            cell={
              <ItemsCell id='productName' data={items} field="product_name" onContextMenu={this.onContextMenu} />
            }
            width={200}
          />
          <Column
            header={<Cell className="text-right">จำนวน</Cell>}
            cell={
              <ItemsCell id={items+'_rowAmount'} data={items} field="amount_title" textAlign='text-right' onContextMenu={this.onContextMenu} />
            }
            width={80}
          />
          <Column
            header={<Cell className="text-right">น.น.รวม(g.)</Cell>}
            cell={
              <ItemsCell data={items} field="weight_total_title" textAlign='text-right' onContextMenu={this.onContextMenu} />
            }
            width={120}
          />
          <Column
            header={<Cell>รหัสสินค้า</Cell>}
            cell={
              <ItemsCell data={items} field="product_code" onContextMenu={this.onContextMenu} />
            }
            width={120}
          />
          <Column
            header={<Cell>วันที่ซื้อ</Cell>}
            cell={
              <ItemsCell data={items} field="in_date_title" onContextMenu={this.onContextMenu} />
            }
            width={100}
          />
          <Column
            header={<Cell>ขายครั้งสุดท้าย</Cell>}
            cell={
              <ItemsCell data={items} field="out_date_title" onContextMenu={this.onContextMenu} />
            }
            width={120}
          />

          <Column
            header={<Cell>ประเภทงานขาย</Cell>}
            cell={
              <ItemsCell data={items} field="product_type_sale" onContextMenu={this.onContextMenu} />
            }
            width={130}
          />
          <Column
            header={<Cell className="text-right">ราคาป้ายวันนี้</Cell>}
            cell={
              <ItemsCell data={items} field="product_price_tag" textAlign='text-right' onContextMenu={this.onContextMenu} />
            }
            width={200}
          />
          <Column
            header={<Cell className="text-right"></Cell>}
            cell={
              <ItemsCell  data={items}  field='ee' />
            }
            width={25}
          />
        </Table>
        {this.state.open_print?<StockProductPrintPreview
        
        onClose={() => {
                this.setState({ open_print: false });
              }} 
        items={items}
         />:''}
        <br />
        {this.state.open_report?<StockProdcutDetail
          id={this.state.id}
          name={this.state.name}
          open={true}
          onClose={() => {
            this.setState({ open_report: false });
            //this.componentDidMount();
          }}
        />:''}
        <Form size='small' >
          <Form.Group>
            <Form.Field width={16}>
                           
              <Button id='btnPrint' content='พิมพ์' onClick={() => {
                this.setState({ open_print: true });
              }} floated='right' icon='print' labelPosition='right' type='button' primary />

            </Form.Field>
          </Form.Group>
        </Form>
      </div>

    );
  }
}
export default StockProduct;
