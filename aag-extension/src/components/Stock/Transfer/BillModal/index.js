/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, {Component} from 'react';
import Utility from '../../../../Utility';
import Settings from '../../../../Settings';
import MsgInput from '../../../Error/MsgInput'
import OptionItemsCell from '../../OptionItemsCell'
import {Table, Column, Cell} from 'fixed-data-table';
import BillItemModal from './BillItemModal'
import BillPrintPreview from './BillPrintPreview'
import {
  Form,
  Header,
  Button,
  Loader,
  Dimmer,
  Icon,
  Dropdown,
  Modal,
  Grid
} from 'semantic-ui-react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';


class ItemsCell extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const {
      rowIndex,
      field,
      data,
      ...props
    } = this.props;
    return (
        <Cell {...props}>
          <div className={this.props.textAlign}>{data[rowIndex][field]}</div>
        </Cell>
    );
  }
}

class BillModal extends Component {

  constructor(props) {
    super(props);

    this.modal_status = false;
    props.branches.shift();
    this.state = {
      branches: props.branches,
      message_error_hidden: true,
      items: [],
      invoice_date: moment(),
      branch: '',
      branch_to : '',
      status_stock: 'N',
      modal_open: this.props.open,
      product_action_title: 'เพิ่มสินค้า',
      product_action: 'add',
      invoice_item: [],
      button_print_disabled:true,
      button_update_stoick_disabled: true,
      button_save_disabled: false,
      button_update_stoick_loading: false,
      invoice_title: 'เพิ่มรายการโอนทองใหม่ระหว่างสาขา',
      loader: false,
      status_bill: 'N',
      button_save_title: 'สร้าง',
      invoice_add_product_disabled: true,
      description: '',
      msg_error: {}
    }

    this.product_code_all = [];
    this.product_name_all = [];
    this.categories = [];
    this.product_types = [];

    this.onDeleteProduct = this
      .onDeleteProduct
      .bind(this);
  }

  submitSaveInvoice() {

    let msg_error = {}
    if (this.state.branch == '') {
      msg_error['branch'] = ['กรุณาเลือกสาขา']
    }
    if (this.state.branch_to == '' || this.state.branch_to == 0) {
      msg_error['branch_to'] = ['กรุณาเลือกสาขา']
    }

    if (this.state.branch_to == this.state.branch) {
      msg_error['branch_to'] = ['เลือกสาขาไม่ถูกต้อง']
    }
    this.setState({msg_error: msg_error})
    if (Object.keys(msg_error).length > 0) {
      
      return;
    }

    var formData = {
      branch: this.state.branch,
      branch_to: this.state.branch_to,
      ref_number: this.state.ref_number,
      invoice_date: Utility.formatDate2(this.state.invoice_date),
      status_bill: 'N',
      status_stock: this.state.status_stock,
      kind: 'MV',
      description: this.state.description
    };

    this.setState({button_save_loading: true})
    if (this.props.action == 'add') {
      formData.status_stock = 'N'

      console.log(formData);
      const url = Settings.baseUrl + '/invoices/';
      Utility.post(url, formData, (status, data) => {
        console.log(data);
        this.setState({button_save_loading: false, invoice_add_product_disabled: false, invoice_disabled: true, button_save_title: 'บันทึก'})
        this
          .props
          .onAddInvoice(data);
          this.loadInvoiceItem();
      });
    } else {
      var formData2 = {
        branch: this.state.branch,
        ref_number: this.state.ref_number,
        invoice_date: Utility.formatDate2(this.state.invoice_date),
        status_bill: this.state.status_bill,
        description: this.state.description
      };

      const url = Settings.baseUrl + '/invoices/' + this.props.invoice_detail.id + "/";

      Utility.put(url, formData2, (status, data) => {
        if (status) {
          this.setState({button_save_loading: false})
          this
            .props
            .onAddInvoice(data);
            this.loadInvoiceItem();
        }
      });
    }
  }

  supmitProduct() {

    let formData = {
      invoice: this.props.invoice_detail.id,
      product: this.state.product_id,
      status_stock: 'N',
      amount: this.state.product_amount,
      weight: this.state.product_weight,
      weight_total: this.state.product_weight_total,
      weight_real: this.state.product_weight_real
    };

    let msg_error = {}
    if (this.state.product_id == null || this.state.product_id == '') {
      msg_error['product'] = ['กรุณาเลือกสินค้า']
    }
    if (this.state.product_amount == null || this.state.product_amount == '') {
      msg_error['amount'] = ['กรุณาระบุจำนวน']
    }

    if (Object.keys(msg_error).length > 0) {
      this.setState({msg_error: msg_error})
      return;
    }

    this.setState({button_product_loading: true})
    if (this.state.product_action == 'add') {
      const url = Settings.baseUrl + '/invoice_item/';
      Utility.post(url, formData, (status, data) => {
        if (status) {
          this.setState({button_product_loading: false, modal_product: false})
          this.loadInvoiceItem();
        } else { // error
          if (data.length !== 0) 
            this.setState({msg_error: data})
        }
      });
    } else {
      const url = Settings.baseUrl + '/invoice_item/' + this.state.item_id + '/';
      Utility.put(url, formData, (status, data) => {
        if (status) {
          this.setState({button_product_loading: false, modal_product: false})
          this.loadInvoiceItem();
        } else { // error
          if (data.length !== 0) 
            this.setState({msg_error: data})
        }
      });
    }

  }

  loadInvoiceItem() {

    var invoices = Utility.getFetch(Settings.baseUrl + '/invoices/' + this.props.invoice_detail.id + "/");
    var items = Utility.getFetch(Settings.baseUrl + '/invoices/' + this.props.invoice_detail.id + '/items/');
    this.setState({loader: true});
    Promise
      .all([invoices, items])
      .then((values) => {
        // set invoice detail
        let item = values[0];
        //this.setState(item)
        this.setState({
          number: item.number,
          description: item.description,
          branch_to: item.branch_to,
          branch: item.branch,
          status_bill: item.status_bill,
          invoice_date: moment(item.invoice_date),
          invoice_disabled: true,
          button_save_title: 'บันทึก',
          invoice_item: this.setFieldValue(values[1]),
          button_print_disabled:false,
          status_stock: item.status_stock,
          button_update_stoick_disabled: values[1].length == 0 || item.status_stock == 'Y',
          invoice_add_product_disabled: item.status_stock == 'Y',
          invoice_title: <span >{< span > แก้ไขรายการโอนทองใหม่ระหว่างสาขา </span>} </span>,
          loader: false
        });

      });
  }

  setFieldValue(item) {

    let invoice_weight_total = 0;
    let invoice_product_total = 0;
    let invoice_itme_total = item.length;
    let invoice_profit_total = 0;
    let invoice_
    for (let i = 0; i < item.length; i++) {
      item[i]['status_stock_title'] = Settings.status_stock[item[i].status_stock]
      item[i]['product_code'] = item[i].product.code
      item[i]['product_name'] = item[i].product.name
      item[i]['category_title'] = item[i].product.category.name
      item[i]['kind_title'] = item[i].product.kind.name

      if (item[i].product.type_sale == 2) {
        invoice_profit_total += parseFloat(item[i].product.cost) * parseFloat(item[i].amount);

        item[i]['price_total'] = Utility.priceFormat(parseFloat(item[i].product.cost) * parseFloat(item[i].amount));
        item[i]['type_sale_title'] = 'งานชิ้น';
        item[i]['cost'] = Utility.priceFormat(parseFloat(item[i].product.cost));
        item[i]['price_tag'] = Utility.priceFormat(parseFloat(item[i].product.price_tag));
      } else {
        item[i]['price_total'] = Utility.priceFormat(0.00);
        item[i]['type_sale_title'] = 'งานชั่ง';
        item[i]['cost'] = Utility.priceFormat(0.00);
        item[i]['price_tag'] = Utility.priceFormat(0.00);
      }

      invoice_weight_total += parseFloat(item[i].weight_total);
      invoice_product_total += parseInt(item[i].amount);
    }
    this.setState({
      invoice_weight_total: Utility.weightFormat(invoice_weight_total),
      invoice_product_total: Utility.numberFormat(invoice_product_total),
      invoice_itme_total: Utility.numberFormat(invoice_itme_total),
      invoice_profit_total: Utility.priceFormat(invoice_profit_total)
    });
    return item;
  }

  onDeleteProduct(e, d) {

    if(!window.confirm('ยืนยันลบ'))
      return

    let data = this.state.invoice_item[d.positon];
    if (data.status_stock == 'N') {
      const url = Settings.baseUrl + '/invoice_item/' + data.id + '/';
      Utility.delete(url, (status, data) => {

        this.setState({button_product_loading: false})
        this.loadInvoiceItem();
      });
    } else 
      alert('ไม่สามารถลบรายการนี้ได้');
    }
  
  submitUpdateStock() {

    this.setState({button_update_stoick_loading: true})
    const url = Settings.baseUrl + '/invoices/' + this.props.invoice_detail.id + '/stock/';
    Utility.get(url, (status, data) => {

      if(status){
        this.props.onAddInvoice();
        this.loadInvoiceItem();
      }else{
        alert(data.error)
      }

      this.setState({button_update_stoick_loading: false})
    });
  }

  componentDidMount() {
    this.setState({loader_active: true, btn_stock: true});

    if(this.props.action=='edit')
      this.loadInvoiceItem();
      setTimeout(()=>{
        let table_width = document.getElementById('table_width2')
        this.setState({ table_width:table_width.clientWidth });
      },400)
  
    
  }

  render() {

    let items = this.state.invoice_item;
    return (
      <Modal size="large" open={true} /*dimmer='blurring'*/ > 
      < Button id='btnClose' circular icon = 'close' basic floated = 'right' name = '' onClick = { this.props.onClose } /> 
    <Modal.Header>
      <Header as='h4'>
        <Header.Content>
          {this.state.invoice_title}
          {this.state.number && <span id='textBillID'>[บิลเลขที่ {this.state.number}]</span>}
          <Header.Subheader>
            <span id='statusBill'><Icon
              name={this.state.status_stock == 'N'
      ? 'warning circle'
      : 'check circle'}
              color={this.state.status_stock == 'N'
      ? 'yellow'
      : 'green'}/>{Settings.status_stock[this.state.status_stock]}</span>
          </Header.Subheader>
        </Header.Content>
      </Header>
    </Modal.Header> < Modal.Content > <div >
      <div className='relative'>
      {this.state.loader && <Dimmer active={this.state.loader} inverted>
          <Loader inverted content='Loading'/>
        </Dimmer>}
        <div>
          {this.state.modal_product &&  <BillItemModal onSuccess={()=>{
            this.setState({modal_product: false})
            this.loadInvoiceItem()
            }} invoice={this.state.invoice_item} onClose={()=>this.setState({modal_product: false})} invoice={this.props.invoice_detail} />}
          <Grid >
            <Grid.Row>
              <Grid.Column width={4}>
                <Form className='fluid' size='small'>

                  <Form.Field width={16} error={this.state.msg_error.branch != null}>
                    <label>*จากสาขา
                      <MsgInput text={this.state.msg_error.branch}/></label>
                    <Dropdown
                      id='dropDownFromBranchForAdd'
                      placeholder='จากสาขา'
                      search
                      selection
                      width={16}
                      options={this.props.branches}
                      defaultValue={this.state.branch}
                      value={this.state.branch}
                      onChange={(e, data) => {
                      let branches = []
                      for (let i in this.props.branches) {
                        if (this.props.branches[i].key != data.value) {
                          branches.push(this.props.branches[i])
                        }
                      }
                      this.setState({branch: data.value, branches: branches});
                    }}
                      disabled={this.state.invoice_disabled}/>
                  </Form.Field>
                  <Form.Group >

                    <Form.Field width={16} error={this.state.msg_error.branch_to != null}>
                      <label>*ไปสาขา
                        <MsgInput text={this.state.msg_error.branch_to}/></label>
                      <Dropdown
                        id='dropDownToBranchForAdd'
                        placeholder='ไปสาขา'
                        disabled={this.state.invoice_disabled}
                        search
                        selection
                        width={16}
                        options={this.state.branches}
                        value={this.state.branch_to}
                        onChange={(e, data) => {
                        this.setState({branch_to: data.value});
                      }}/>
                    </Form.Field>
                  </Form.Group >

                  <Form.Group >
                    <Form.Field width={16}>
                      <label>วันที่</label>
                      <DatePicker
                        id='dateStartForAdd'
                        dateFormat="DD/MM/YYYY"
                        selected={this.state.invoice_date}
                        onChange={(date) => {
                        this.setState({invoice_date: date});
                      }}
                        disabled={this.state.invoice_disabled}/>
                    </Form.Field>
                  </Form.Group >
                  <Form.Group inline>
                    <Form.Checkbox
                      id='clearBill'
                      label='เคลียร์บิล'
                      width={16}
                      checked={this.state.status_bill == 'Y'}
                      onChange={(e, v) => {
                      this.setState({
                        status_bill: v.checked
                          ? 'Y'
                          : 'N'
                      });
                    }}/>
                  </Form.Group>

                  <Form.Group >
                    <Form.TextArea
                      label='หมายเหตุ'
                      placeholder=''
                      width={16}
                      value={this.state.description}
                      onChange={(e, v) => {
                      this.setState({description: v.value})
                    }}/>
                  </Form.Group>
                </Form>
              </Grid.Column>
              <Grid.Column width={12}>
                <Form size='small'>
                  <Form.Group>
                    <Form.Field width={6}>
                      <Header floated='left' as='h3'>รายการสินค้า</Header>
                    </Form.Field>
                    <Form.Field width={10}>
                      <Button
                        id='btnAdd'
                        disabled={this.state.invoice_add_product_disabled}
                        size='small'
                        content='เพิ่มรายการสินค้า'
                        onClick={() =>this.setState({modal_product: true})}
                        floated='right'
                        icon='plus'
                        labelPosition='left'
                        type='button'
                        color='green'/>
                    </Form.Field>
                  </Form.Group>
                </Form>
                <div id="table_width2">
                  <Table
                    rowsCount={items.length}
                    rowHeight={35}
                    headerHeight={35}
                    width={this.state.table_width}
                    height={400}>
                      <Column
                            width={50}
                            header={< Cell > </Cell>}
                            cell={< OptionItemsCell edit={false} onClickMenu = {
                            (e, data) => {
                              if (data.action == 'delete')
                                  this.onDeleteProduct(e, data)
                            }
                          } />}/>
                    <Column
                      header={< Cell > สถานะปรับปรุงสต็อก </Cell>}
                      cell={< ItemsCell data = {
                      items
                    }
                    field = "status_stock_title" />}
                      width={150}/>
                    <Column
                      header={< Cell > รหัสสินค้า </Cell>}
                      cell={< ItemsCell data = {
                      items
                    }
                    field = "product_code" />}
                      width={100}/>
                    <Column
                      header={< Cell > กลุ่มสินค้า </Cell>}
                      cell={< ItemsCell data = {
                      items
                    }
                    field = "category_title" />}
                      width={120}/>
                    <Column
                      header={< Cell > ประเภท </Cell>}
                      cell={< ItemsCell data = {
                      items
                    }
                    field = "kind_title" />}
                      width={120}/>
                    <Column
                      header={< Cell > ชื่อสินค้า </Cell>}
                      cell={< ItemsCell id='ProductName' data = {
                      items
                    }
                    field = "product_name" />}
                      width={150}/>
                    <Column
                      header={< Cell className = 'text-right' > น.น./ชิ้น</Cell >}
                      cell={< ItemsCell data = {
                      items
                    }
                    field = "weight" textAlign = 'text-right' />}
                      width={100}/>
                    <Column
                      header={< Cell className = 'text-right' > จำนวน </Cell>}
                      cell={< ItemsCell data = {
                      items
                    }
                    field = "amount" textAlign = 'text-right' />}
                      width={100}/>
                    <Column
                      header={< Cell className = 'text-right' > น.น.รวม </Cell>}
                      cell={< ItemsCell data = {
                      items
                    }
                    field = "weight_total" textAlign = 'text-right' />}
                      width={100}/>
                    <Column
                      header={< Cell className = 'text-right' > น.น.ชั่งจริง </Cell>}
                      cell={< ItemsCell data = {
                      items
                    }
                    field = "weight_real" textAlign = 'text-right' />}
                      width={100}/>

                    <Column
                      header={< Cell className = 'text-right' > ต้นทุนชิ้นละ </Cell>}
                      cell={< ItemsCell data = {
                      items
                    }
                    field = "cost" textAlign = 'text-right' />}
                      width={120}/>
                    <Column
                      header={< Cell className = 'text-right' > ราคาป้ายชิ้นละ </Cell>}
                      cell={< ItemsCell data = {
                      items
                    }
                    field = "price_tag" textAlign = 'text-right' />}
                      width={120}/>
                    <Column
                      header={< Cell className = 'text-center' > ประเภทการขาย </Cell>}
                      cell={< ItemsCell data = {
                      items
                    }
                    field = "type_sale_title" textAlign = 'text-center' />}
                      width={120}/>
                  </Table>
                </div>
                <br/>
                <Form className='fluid' size='small'>
                  <Form.Group >
                    <Form.Input
                      id='invoice_itme_total'
                      label='จำนวนรายการ'
                      placeholder=''
                      className='text-right'
                      width={4}
                      value={this.state.invoice_itme_total}
                      readOnly/>
                    <Form.Input
                      id='invoice_profit_total'
                      label='ค่าแรงขายปลีกรวม'
                      placeholder=''
                      className='text-right'
                      width={4}
                      value={this.state.invoice_profit_total}
                      readOnly/>
                    <Form.Input
                      id='invoice_weight_total'
                      label='น้ำหนักรวม'
                      placeholder=''
                      className='text-right'
                      width={4}
                      value={this.state.invoice_weight_total}
                      readOnly/>
                    <Form.Input
                      id='invoice_product_total'
                      label='จำนวนรวม'
                      placeholder=''
                      className='text-right'
                      width={4}
                      value={this.state.invoice_product_total}
                      readOnly/>
                  </Form.Group>
                </Form>

              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>
        
      </div> </div>
        {this.state.open_print
          ? <BillPrintPreview
              branches={this.state.branches}
              invoice_detail={this.props.invoice_detail}
              items={items}
              product_total={this.state.invoice_product_total}
              weight_total={this.state.invoice_weight_total}
              onClose={() => {
              this.setState({open_print: false})
            }}/>
          : ''}
        </Modal.Content > <Modal.Actions>
        <Button
            id='btnPrint'
            primary
            size='small'
            icon='print'
            disabled={this.state.button_print_disabled}
            onClick={(e) => {
            e.preventDefault();
            this.setState({open_print: true})
          }}
            content='พิมพ์'/>
      <Button
        id='btnSave'
        size='small'
        primary
        icon='save'
        labelPosition='left'
        onClick={(e) => {
        e.preventDefault();
        this.submitSaveInvoice();
      }}
        className={this.state.button_save_loading
        ? 'loading'
        : ''}
        content={this.state.button_save_title}/>
      <Button
        id='btnSaveAndUpdate'
        size='small'
        icon='lightning'
        color='red'
        onClick={(e) => {
        e.preventDefault();
        if (window.confirm('ยืนยันบันทึกและอัพเดทสต๊อก')) 
          this.submitUpdateStock();
        }}
        className={this.state.button_update_stoick_loading
        ? 'loading'
        : ''}
        disabled={this.state.button_update_stoick_disabled}
        content='บันทึกและอัพเดทสต๊อก'/>

    </Modal.Actions> </Modal>
    )
  }

}

export default BillModal;