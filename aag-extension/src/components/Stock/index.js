/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, {Component} from 'react';
import {
  Form,
  Segment,
  Header,
  Icon,
  Grid,
  Menu
} from 'semantic-ui-react';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';

import StockProduct from './StockProduct';
import StockCategory from './StockCategory'
import Transfer from './Transfer';
import ImportExportProduct from './ImportExportProduct';
import ExportCategory from './ExportCategory';

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeItem: this.props.location.pathname
    }

    this.handleItemClick = this
      .handleItemClick
      .bind(this);
  }

  handleItemClick(e, {name, to}) {
    this.setState({activeItem: to});
  }

  render() {
    const {
      rowIndex,
      field,
      data
    } = this.props;
    return (
      <div>
        <Form size='small'>
          <Form.Group>
            <Form.Field width={6}>
              <Header floated='left' as='h4'><Icon name='shipping'/>
                สต็อกทอง</Header>
            </Form.Field>
          </Form.Group>
        </Form>
        <Router>

          <Grid>
            <Grid.Column width={3}>
              <Segment color='black'>

                <Menu secondary vertical className='menu-left'>
                  <Menu.Item
                    id='importStock'
                    name='import'
                    active={this.state.activeItem == '/stock/import'}
                    onClick={this.handleItemClick}
                    as={Link}
                    to='/stock/import'>
                    นำเข้าทองใหม่
                  </Menu.Item>
                  <Menu.Item
                    id='exportStock'
                    name='export'
                    active={this.state.activeItem == '/stock/export'}
                    onClick={this.handleItemClick}
                    as={Link}
                    to='/stock/export'>
                    นำออกทองใหม่
                  </Menu.Item>
                  <Menu.Item
                    id='transferStock'
                    name='transfer'
                    active={this.state.activeItem == '/stock/transfer'}
                    onClick={this.handleItemClick}
                    as={Link}
                    to='/stock/transfer'>
                    โอนทองใหม่ระหว่างสาขา
                  </Menu.Item>
                  <Menu.Item
                    id='exportOldStock'
                    name='export_old'
                    active={this.state.activeItem == '/stock/export_old'}
                    onClick={this.handleItemClick}
                    as={Link}
                    to='/stock/export_old'>
                    นำออกทองเก่า
                  </Menu.Item>
                  <Menu.Item
                    id='newStock'
                    name='product'
                    active={this.state.activeItem == '/stock/product'}
                    onClick={this.handleItemClick}
                    as={Link}
                    to='/stock/product'>
                    สต็อกทองใหม่
                  </Menu.Item>
                  <Menu.Item
                    id='oldStock'
                    name='category'
                    active={this.state.activeItem == '/stock/category'}
                    onClick={this.handleItemClick}
                    as={Link}
                    to='/stock/category'>
                    สต็อกทองเก่า
                  </Menu.Item>
                </Menu>
              </Segment>
            </Grid.Column>
            <Grid.Column width={13}>
              <Segment color='black'>
                <Route path="/stock/import" component={() => (<ImportExportProduct action="import"/>)}/>
                <Route path="/stock/export" component={() => (<ImportExportProduct action="export"/>)}/>
                <Route path="/stock/transfer" component={() => (<Transfer/>)}/>
                <Route path="/stock/export_old" component={() => (<ExportCategory/>)}/>
                <Route path="/stock/product" component={() => (<StockProduct/>)}/>
                <Route path="/stock/category" component={() => (<StockCategory/>)}/>
              </Segment>
            </Grid.Column>
          </Grid>

        </Router>
      </div>
    );
  }
}
export default Main;
