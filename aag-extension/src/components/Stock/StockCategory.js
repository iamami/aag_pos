/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import Settings from '../../Settings';
import Utility from '../../Utility';
import {
  Table,
  Column,
  Cell
} from 'fixed-data-table';
import {
  Form,
  Button,
  Dimmer,
  Loader,
  Header
} from 'semantic-ui-react';

class ItemsCell extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { rowIndex, field, data, ...props } = this.props;
    return (
      <Cell {...props}>
        <div className={this.props.textAlign}>{data[rowIndex][field]}</div>
      </Cell>
    );
  }
}

class ProductType extends Component {

  constructor(props) {
    super(props);
    this.state = {
      items: [],
      branches_id: 0,
      table_hiegh: Settings.table_hiegh
    }
    this.submitSearch = this.submitSearch.bind(this)
    this.resetForm = this.resetForm.bind(this)
  }

  resetForm() {
    this.setState({
      branches_id:0
    })
  }

  componentDidMount() {
    var self = this;
    this.setState({
      loader_active: true
    });

    var s = '';
    if (this.state.branches_id != null && this.state.branches_id != '') {
      s += (s == '' ? '' : '&') + 'branch=' + this.state.branches_id
    }

    var stock_category = Utility.getFetch(Settings.baseUrl + '/stock_category/?is_enabled=1&' + s);
    var branches = Utility.getFetch(Settings.baseUrl + '/branches/');
    Promise.all([stock_category, branches]).then((values) => {
      this.stock_category = values[0];

      this.branches = values[1];
      let branches = [{
          value: 0,
          key: 0,
          text: '- ทั้งหมด -'
      }]
      for (let i in this.branches) {
        if (this.branches[i].is_enabled == 1)
          branches.push({
            value: this.branches[i].id,
            key: this.branches[i].id,
            text: this.branches[i].name
          });
      }

      this.setState({
        loader_active: false,
        items: this.setFieldValue(values[0]),
        branches: branches
      });

      this.setState({ table_width: window.innerWidth, table_hiegh: window.innerHeight - 260 });
    });
  }

  submitSearch(event) {
    event.preventDefault();
    console.log(this.state)
    this.componentDidMount();
  }

  getTypeSale(key) {
    let type = Settings.type_sale.filter((o) => { return o.value == key });
    return type[0];
  }

  setFieldValue(items) {

    for (let i = 0; i < items.length; i++) {
      items[i]['in_date_title'] = Utility.formatDate(items[i].in_date)
      items[i]['out_date_title'] = Utility.formatDate(items[i].out_date)

      let d = parseFloat(items[i].category.weight)
      items[i]['category'] = items[i].category.name
      items[i]['branch'] = items[i].branch.name
      items[i]['average'] = items[i].weight == 0 ? 0 : Utility.weightFormat(parseFloat(items[i].total) / (parseFloat(items[i].weight) / d))
    }

    return items;
  }

  render() {
    const items = this.state.items;
    return (
      <div id="table_width">
        <Form size='small'>
          <Form.Group>
            <Form.Field width={6}>
              <Header floated='left' as='h2'>สต็อกทองเก่า</Header>
            </Form.Field>
          </Form.Group>
        </Form>
        {this.state.loader_active && <Dimmer className={this.state.loader_active ? 'active' : ''} inverted>
          <Loader content='Loading' inverted />
        </Dimmer>}
        <Form className='fluid' size='small' onSubmit={this.submitSearch}>
          <Form.Group>
            <Form.Dropdown id='dropDownBranch' label='สาขา' search selection width={4} options={this.state.branches} value={this.state.branches_id} onChange={(e, data) => {
              this.setState({ branches_id: data.value });
            }} />
            <Form.Field width={5}>
              <br />
              <Button id='btnSearch'>ค้นหา</Button>
              <Button id='btnAll' onClick={this.resetForm} >ทั้งหมด</Button>
            </Form.Field>
          </Form.Group>
        </Form>
        <Table
          rowsCount={items.length}
          rowHeight={35}
          headerHeight={35}
          width={this.state.table_width}
          height={this.state.table_hiegh}>
          <Column

            header={<Cell>สาขา</Cell>}
            cell={
              <ItemsCell id='branch' data={items} field="branch" />
            }
            width={150}
          />
          <Column
            header={<Cell>กลุ่มสินค้า</Cell>}
            cell={
              <ItemsCell data={items} field="category" />
            }
            width={150}
          />
          <Column
            header={<Cell className='text-right'>น.น.(g.)</Cell>}
            cell={
              <ItemsCell data={items} field="weight" textAlign='text-right' />
            }
            width={150}
          />

          <Column
            header={<Cell className='text-right'>ราคาทองเฉลี่ย</Cell>}
            cell={
              <ItemsCell data={items} field="average" textAlign='text-right' />
            }
            width={160}
          />

        </Table>
      </div>
    );
  }
}
export default ProductType;
