/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import { Cell} from 'fixed-data-table';
import {
  Form, Header, Button, Loader, Dimmer, Icon, Input
} from 'semantic-ui-react';

class ItemsCell extends Component {
    constructor(props) {
      super(props);
    }
    render() {
      const { rowIndex, field, data, ...props } = this.props;
      return (
          <Cell {...props}>
            <div className={this.props.textAlign}>{data[rowIndex][field]}</div>
          </Cell>
      );
    }
  }
export default ItemsCell