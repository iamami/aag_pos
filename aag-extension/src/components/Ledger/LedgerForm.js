/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import Settings from '../../Settings';
import Utility from '../../Utility';
import MsgInput from '../Error/MsgInput'
import LedgerCategoryForm from './LedgerCategoryForm'
import { connect } from 'react-redux'
import {
  Form,
  Input,
  Button,
  Icon,
  Modal, Dropdown,
  Grid,
  Header,
  TextArea,
  FormField
} from 'semantic-ui-react';
import InputSearchCustomer from '../Customer/InputSearchCustomer';


class LedgerForm extends Component{

    constructor(props){
        super(props);
        this.ledger_category_list = props.ledger_category.list
        this.state = {
            msg_error: {},
            ledger: props.ledger,
        }
        this.handlerModalInputChange = this.handlerModalInputChange.bind(this)
        this.handleSubmitledger = this.handleSubmitledger.bind(this)
        this.updateLedgerCategory = this.updateLedgerCategory.bind(this)
    }

    setLedgerCategoryOption(kind){
      let ledger_category_options = []
      for(let i in this.ledger_category_list){
        let d =this.ledger_category_list[i]
        if(d.kind != kind || (d.is_standard==1 && d.id !=12))
          continue
        ledger_category_options.push({
          value: d.id,
          key: d.id,
          text: d.title
        })
      }
      this.setState({ledger_category_options: ledger_category_options})
    }

    componentDidMount(){

      const {auth} = this.props

      Utility.get(Settings.baseUrl + "/banks/", (e, resJson) => {
        this.banks = resJson;
        let banks = []
        for (let i in resJson) {
          banks.push({
            value: resJson[i].id,
            key: resJson[i].id,
            text: resJson[i].name
          });
        }
        this.setState({
          banks: banks
        });
      })
      Utility.get(Settings.baseUrl + "/staffs/?branch="+auth.branch.id, (e, resJson) => {
        this.staffs = resJson;
        let staffs = []
        for (let i in resJson) {
          staffs.push({
            value: resJson[i].id,
            key: resJson[i].id,
            text: resJson[i].name
          });
        }
        this.setState({
          staffs: staffs
        });
      })
      Utility.get(Settings.baseUrl + "/bank_cards/", (e, resJson) => {
        this.bank_cards = resJson;
        let bank_cards = []
        for (let i in resJson) {
          bank_cards.push({
            value: resJson[i].id,
            key: resJson[i].id,
            text: resJson[i].kind + " " + resJson[i].bank.name
          });
        }
        this.setState({
          bank_cards: bank_cards
        });
      })

      Utility.get(Settings.baseUrl + "/bank_account/", (e, resJson) => {
        this.bank_accounts = resJson;
  
        let bank_accounts = []
        for (let i in this.bank_accounts) {
          bank_accounts.push({
            value: this.bank_accounts[i].id,
            key: this.bank_accounts[i].id,
            text: this.bank_accounts[i].bank.name + ' ' + this.bank_accounts[i].number + ' (' + this.bank_accounts[i].name + ')',
            content: <Header as='h4' content={this.bank_accounts[i].bank.name} subheader={this.bank_accounts[i].number + ' (' + this.bank_accounts[i].name + ')'} />
          });
        }
        this.setState({
          bank_accounts: bank_accounts
        });
      })

      this.setLedgerCategoryOption(this.props.ledger.kind)
    }

    handleChangeKind(kind){
      let ledger = this.state.ledger
      ledger.kind = kind
      this.setLedgerCategoryOption(kind)
      this.setState({ ledger: ledger })
    }

    handlerModalInputChange(e, d) {
      let ledger = this.state.ledger;

      ledger[d.name] = d.value;
      if(d.type=='number'){
        let c = parseFloat(d.value)
        c = c<0?c*-1:c
        ledger[d.name] =c ;

      }

      if(d.name == 'check_code' || d.name == 'card_code'){
        console.log(d.name,isNaN(parseInt(d.value)))
        if(isNaN(parseInt(d.value))){
          ledger[d.name] = ''
        }else{
          ledger[d.name] = parseInt(d.value)
        }
      }

      if(d.name=='bankcard'){
        let o = Utility.getObject(this.bank_cards,d.value)
        ledger.card_fee = o.fee;
      }
      
      this.setState({
        ledger: ledger
      })
    }

    updateLedgerCategory(ledger_category=null){

      Utility.get(Settings.baseUrl + "/ledger_category/?is_enabled=1",(status,data)=>{
        this.ledger_category_list = data
        let ledger = this.state.ledger
        this.setLedgerCategoryOption(ledger.kind )

        if(ledger_category!=null)
          ledger.ledger_category = ledger_category.id
        this.setState({ledger_category_open: false,ledger: ledger})
      })
    }

    handleSubmitledger(e) {
      e.preventDefault()

      const {branch} = this.props.auth

      let msg_error = {}
      let data = this.state.ledger
  
        if (data.ledger_category == null) {
          msg_error['ledger_category'] = ['*กรุณาเลือก รายการ']
        }
  
        if (data.payment == null) {
  
          msg_error['payment'] = ['*กรุณาเลือก ประเภทการชำระ']
        }

        if (data.staff == null) {
  
          msg_error['staff'] = ['*กรุณาเลือก พนักงาน']
        }

        if(data.description==null || data.description==''){
          msg_error['description'] = ['*กรุณาเลือกระบุหมายเหตุ']
        }

        if( (data.payment =='CS' || data.payment =='CC')  && (data.cash==null || data.cash=='')){
          console.log('error cash',data)
          msg_error['cash'] = ['*กรุณาระบุยอดเงิน']
        }

        if((data.payment =='CD' || data.payment =='CC')  && (data.card==null || data.card=='')){
          msg_error['card'] = ['*กรุณาระบุยอดเงิน']
        }

        if(data.payment =='CH' && (data.check==null || data.check=='')){
          msg_error['check'] = ['*กรุณาระบุยอดเงิน']
        }

        if(data.payment =='TF' && (data.transfer==null || data.transfer=='')){
          msg_error['transfer'] = ['*กรุณาระบุยอดเงิน']
        }

        if((data.payment =='CD' || data.payment =='CC') && (data.card_code==null || data.card_code=='')){
          msg_error['card_code'] = ['*กรุณาระบุรหัสบัตรเครดิต']
        }
        
        if((data.payment =='CD' || data.payment =='CC') && (data.card_bank_card==null || data.card_bank_card=='')){
          msg_error['card_bank_card'] = ['*กรุณาระบุชนิดบัตรเครดิต']
        }  
        if((data.payment =='CD' || data.payment =='CC') && (data.card_fee==null || data.card_fee=='')){
          msg_error['card_fee'] = ['*กรุณาระบุ%หักค่าธรรมเนียมบัตรเครดิต']
        } 
        if((data.payment =='CD' || data.payment =='CC') && (data.card_service==null || data.card_service=='')){
          msg_error['card_service'] = ['*กรุณาระบุค่าธรรมเนียมบัตรเครดิต']
        } 
        if (Object.keys(msg_error).length > 0) {
          this.setState({
            msg_error: msg_error
          })
          return;
        }

        if(data.payment =='CS')
          data.total = data.cash
        else if(data.payment =='CD')
          data.total = data.card
        else if(data.payment =='CC')
          data.total = parseFloat(data.card) + parseFloat(data.cash)
        else if(data.payment =='CH')
          data.total = data.check
        else if(data.payment =='TF')
          data.total = data.transfer

        if(this.state.ledger.staff==null)
          this.state.ledger.staff = ''
        if(this.state.ledger.card_bank_card==null)
          this.state.ledger.card_bank_card = ''
      
      if (this.props.action == 'EDIT') {
        let url = Settings.baseUrl + "/ledger/" + this.state.ledger.id + "/"
        let data = this.state.ledger
  
        data.branch = data.branch.id;
        this.setState({
          loadingSubmitledger: true
        })
        Utility.patch(url, data, (s, d) => {
          if(s){
            this.props.onSave()
          }else{
            alert('ผิดพลาด กรุณาลองอีกครั้ง')
          }
        });
      } else {
        let url = Settings.baseUrl + "/ledger/";
        let data = Utility.cloneObjectJson(this.state.ledger)
        data.ledger_date = Utility.formatDate2(data.ledger_date)
        data.branch = branch.id
  
        this.setState({
          loadingSubmitledger: true
        })
        Utility.post(url, data, (s, d) => {
          if(s){
            this.props.onSave()
          }else{
            this.setState({
              loadingSubmitledger: false,
              msg_error: d
            })
          }
        });
      }
    }

    render(){

            return(<div>
              
                <Modal size='tiny' open={true} /*dimmer='blurring'*/>
                <Button id='closeIn-Out' circular icon='close' basic floated='right' name='' onClick={this.props.onClose}/>
            <Modal.Header>
              <Grid>
                <Grid.Row columns={2}>
                  <Grid.Column>
                    <Icon name='sticky note' color={this.state.ledger.kind == 'IN' ? 'green' : 'red'} />รายละเอียด{this.state.ledger.kind == 'IN' ? 'รายรับ' : 'รายจ่าย'}
                </Grid.Column>
                  <Grid.Column>
                    
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Modal.Header>
            <Modal.Content className="scrolling">
           {this.state.ledger_category_open && <LedgerCategoryForm  onSave={this.updateLedgerCategory} onClose={()=>this.setState({ledger_category_open: false})}/>} 
                <center>
                      <Button.Group size='small'>
                        <Button id='income' onClick={() => this.handleChangeKind('IN')} color={this.state.ledger.kind == 'IN' ? 'green' : ''}>รายรับ</Button>
                        <Button.Or />
                        <Button id='outcome' onClick={() => this.handleChangeKind('EX')} color={this.state.ledger.kind == 'EX' ? 'red' : ''}>รายจ่าย</Button>
                      </Button.Group>
                    </center>
                    <br/>
              <Form size='small'>
                <Form.Field width={16} error={this.state.msg_error.ledger_category != null}>
                  <label>*รายการ <MsgInput text={this.state.msg_error.ledger_category} /></label>
                  {this.props.auth.role != 'U'
                                ? <div className='ui action input'>
                                <Dropdown id='menuIn-Out' width={16} placeholder='รายการ' fluid search selection name="ledger_category" value={this.state.ledger.ledger_category}
                                  options={this.state.ledger_category_options}
                                  onChange={this.handlerModalInputChange}
                                />
                                <Button id='add-order' onClick={()=>this.setState({ledger_category_open: true})} icon='plus' />
                              </div>
                                : <div className='ui action input'>
                                <Dropdown id='menuIn-Out' width={16} placeholder='รายการ' fluid search selection name="ledger_category" value={this.state.ledger.ledger_category}
                                  options={this.state.ledger_category_options}
                                  onChange={this.handlerModalInputChange}
                                />
                              </div>}
                  
                </Form.Field>
                <Form.Field width={16} error={this.state.msg_error.payment != null}>
                  <label>*ประเภทการชำระ <MsgInput text={this.state.msg_error.payment} /></label>
                  <Dropdown id='typepayment' label='ประเภทการชำระ' placeholder='ประเภทการชำระ' search selection name="payment" value={this.state.ledger.payment}
                    options={Settings.payment_option}
                    onChange={this.handlerModalInputChange} />
                </Form.Field >
                {this.state.ledger.payment != 'CD' && this.state.ledger.payment != 'CC' ? '' : <Form.Field error={this.state.msg_error.card_code != null}><label>*รหัสบัตรเครดิต <MsgInput text={this.state.msg_error.card_code} /></label>
                <Form.Input id='IDcredit' placeholder='รหัสบัตรเครดิต' type="text" name="card_code" value={this.state.ledger.card_code} onChange={this.handlerModalInputChange} disabled={this.state.ledger.payment != 'CD' && this.state.ledger.payment != 'CC'}  /> </Form.Field> } 
                 
                  
                {this.state.ledger.payment != 'CD' && this.state.ledger.payment != 'CC' ? '' : <Form.Field error={this.state.msg_error.card_bank_card != null}><label>*ชนิดบัตรเครดิต <MsgInput text={this.state.msg_error.card_bank_card} /></label>
                <Form.Dropdown id='Typecredit' placeholder='ชนิดบัตรเครดิต' search selection name="card_bank_card" value={this.state.ledger.card_bank_card}
                  options={this.state.bank_cards}
                  onChange={this.handlerModalInputChange} disabled={this.state.ledger.payment != 'CD' && this.state.ledger.payment != 'CC'} /> </Form.Field> }
                
                {this.state.ledger.payment != 'CD' && this.state.ledger.payment != 'CC' ? '' : <Form.Field error={this.state.msg_error.card_fee != null}><label>*%หักค่าธรรมเนียมบัตรเครดิต <MsgInput text={this.state.msg_error.card_fee} /></label>
                <Form.Input id='percenVatcredit' type="number" placeholder='%หักค่าธรรมเนียมบัตรเครดิต' search selection name="card_fee" value={this.state.ledger.card_fee}
                  onChange={this.handlerModalInputChange} disabled={this.state.ledger.payment != 'CD' && this.state.ledger.payment != 'CC'} /> </Form.Field> }

                {this.state.ledger.payment != 'CD' && this.state.ledger.payment != 'CC' ? '' : <Form.Field error={this.state.msg_error.card_service != null}><label>*ค่าธรรมเนียมบัตรเครดิต <MsgInput text={this.state.msg_error.card_service} /></label>
                <Form.Input id='Vatcredit' type="number" placeholder='ค่าธรรมเนียมบัตรเครดิต' search selection name="card_service" value={this.state.ledger.card_service}
                  onChange={this.handlerModalInputChange} disabled={this.state.ledger.payment != 'CD' && this.state.ledger.payment != 'CC'} /> </Form.Field> }
                
                
                {(this.state.ledger.payment == 'TF') && <Form.Dropdown label='บัญชีธนาคาร' placeholder='บัญชีธนาคาร' search selection name="transfer_bank_account" value={this.state.ledger.transfer_bank_account}
                  options={this.state.bank_accounts}
                  onChange={this.handlerModalInputChange} />}
                  {(this.state.ledger.payment == 'CH') && <Form.Dropdown label='ธนาคาร' placeholder='ธนาคาร' search selection name="check_bank" value={this.state.ledger.check_bank}
                  options={this.state.banks}
                  onChange={this.handlerModalInputChange} />}
                   {(this.state.ledger.payment == 'CH') && <Form.Input label='เลขที่เช็ค'  type="number" placeholder='เลขที่เช็ค' search selection name="check_code" value={this.state.ledger.check_code}
                 
                  onChange={this.handlerModalInputChange} />}
                <InputSearchCustomer label='ชื่อลูกค้า' placeholder='ชื่อลูกค้า' onChange={this.handlerModalInputChange} name="customer" value={this.state.ledger.customer} />
                <Form.Field width={16} error={this.state.msg_error.staff != null}>
                  <label>*พนักงาน <MsgInput text={this.state.msg_error.staff} /></label>
                  <Dropdown id='staff' label='พนักงาน' placeholder='พนักงาน' search selection name="staff" value={this.state.ledger.staff}
                    options={this.state.staffs}
                    onChange={this.handlerModalInputChange} />
                </Form.Field>
                <Form.Input label='เลขที่อ้างอิง' placeholder='เลขที่อ้างอิง' search selection name="object_number" value={this.state.ledger.object_number}
                  onChange={this.handlerModalInputChange} />


                {this.state.ledger.payment == 'CH' && <Form.Field width={16} error={this.state.msg_error.check != null}>
                  <label>*จำนวนเงินชำระ(เช็ค) <MsgInput text={this.state.msg_error.check} /></label>
                  <Input placeholder='จำนวนเงินชำระ' search selection name="check" value={this.state.ledger.check}
                    type='number'
                    min={0}
                    onChange={this.handlerModalInputChange} />
                </Form.Field>}

                {this.state.ledger.payment == 'TF' && <Form.Field width={16} error={this.state.msg_error.transfer != null}>
                  <label>*จำนวนเงินชำระ(โอน) <MsgInput text={this.state.msg_error.transfer} /></label>
                  <Input  placeholder='จำนวนเงินชำระ' search selection name="transfer" value={this.state.ledger.transfer}
                  type='number'  min={0}
                    onChange={this.handlerModalInputChange} />
                </Form.Field>}

                {(this.state.ledger.payment == 'CD'|| this.state.ledger.payment == 'CC') &&<Form.Field width={16} error={this.state.msg_error.card != null}>
                  <label>*จำนวนเงินชำระ(บัตร) <MsgInput text={this.state.msg_error.card} /></label>
                  <Input id='inputcreditIn-Out' placeholder='จำนวนเงินชำระ' search selection name="card" value={this.state.ledger.card}
                  type='number'  min={0}
                    onChange={this.handlerModalInputChange} />
                </Form.Field>}

                {(this.state.ledger.payment == 'CS' || this.state.ledger.payment == 'CC') &&<Form.Field width={16} error={this.state.msg_error.cash != null}>
                  <label>*จำนวนเงินชำระ(เงินสด) <MsgInput text={this.state.msg_error.cash} /></label>
                  <Input id='inputcashIn-Out' placeholder='จำนวนเงินชำระ' search selection name="cash" value={this.state.ledger.cash}
                  type='number'  min={0}
                    onChange={this.handlerModalInputChange} />
                </Form.Field>}

                <Form.Field error={this.state.msg_error.description != null}>

                  <label>*หมายเหตุ <MsgInput text={this.state.msg_error.description} /></label>
                  <TextArea
                    id='ex' 
                    placeholder='หมายเหตุ' 
                    search selection 
                    name="description" 
                    value={this.state.ledger.description}
                    onChange={this.handlerModalInputChange} />
                  </Form.Field> 
              </Form>
            </Modal.Content>
            <Modal.Actions>
              <Button id='confirmIn-Out' size='small' primary loading={this.state.loadingSubmitledger} onClick={this.handleSubmitledger}>
                บันทึก
            </Button>

            </Modal.Actions>
          </Modal>
            </div>)
    }
}

const mapStateToProps = state =>{
  return ({
    auth: state.auth,
    branches: state.branches,
    ledger_category: state.ledger_category
  })
}

export default connect(
  mapStateToProps,
)(LedgerForm)