/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import Settings from '../../Settings';
import Utility from '../../Utility';
import LedgerForm from './LedgerForm'
import LedgerFormSearch from './LedgerFormSearch'
import { connect } from 'react-redux'
import {
  Table,
  Column,
  Cell
} from 'fixed-data-table';
import {
  Form,
  Button,
  Icon,
  Dimmer, Loader, Segment,
  Label,
  Header,
} from 'semantic-ui-react';
import moment from 'moment';
import DropdownBranch from '../Widget/DropDown/DropDownBranch'
import { ContextMenuProvider } from 'react-contexify';
import LedgerPrintPreview from './LedgerPrintPreview'
import {activations_branch,loadLedger} from '../../actions'

class OptionItemsCell extends Component {
  constructor(props) {
    super(props);

    this.state = {}
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e, v) {

    console.log(e.target);

  }

  render() {
    const { rowIndex, field, data, ...props } = this.props;
    return (
      <Cell><center style={{ padding: '4px' }}>
        {/* <a onClick={(e) => { this.props.onClickMenu(e, { action: 'edit', positon: rowIndex }) }}><Icon name='edit' /></a> */}
        <a onClick={(e) => { this.props.onClickMenu(e, { action: 'delete', positon: rowIndex }) }} ><Icon name='window close outline' /></a>
      </center></Cell>
    );
  }
}

class ItemsCell extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { rowIndex, field, data, ...props } = this.props;

    let color = '';
    if (data[rowIndex].kind == 'IN' && field == 'income') color = 'green'
    else if (data[rowIndex].kind == 'EX' && field == 'expenses') color = 'red'
    
    return (
      <Cell {...props} >
        <ContextMenuProvider
          data={rowIndex}
          id="menu_id">
          <div className={this.props.textAlign}><span className={color}>{data[rowIndex][field]}</span></div>
        </ContextMenuProvider>
      </Cell>
    );
  }
}

class IconItemsCell extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { rowIndex, field, data, ...props } = this.props;
    return (
      <Cell {...props} >
          <div className={this.props.textAlign}><span>{data[rowIndex].kind == 'IN' ? <Icon name='sticky note' color='green' /> : <Icon name='sticky note' color='red' />} {data[rowIndex].kind_title}</span></div>
      </Cell>
    );
  }
}


class Ledger extends Component {

  constructor(props) {
    super(props);
    this.state = {
      items: [],
      bill: {},
      billItems: [],
      ledger: {
        customer_id: 0,
        ledger_date: moment(),
        kind: 'IN',
        card_bank_card: ''
      },
      start_date: moment(),
      end_date: moment(),
      msg_error: {}
    }

    this.submitData = this.submitData.bind(this);
    this.handlerSearch = this.handlerSearch.bind(this)
    this.handlerVoidBill = this.handlerVoidBill.bind(this)
    this.handlerModalInputChange = this.handlerModalInputChange.bind(this)

    this.addledger = this.addledger.bind(this)
    this.addledgerList = this.addledgerList.bind(this)
  }

  submitData() {

  }

  resetForm() {
    let ledger = {
      ledger_date: moment()
    }
    this.setState({
      modal_ledger: false,
      ledger: ledger,
      ledger_action: 'ADD',
      msg_error: {}
    })
  }


  addledger(e) {
    e.preventDefault()
    let ledger = {
      customer_id: 0,
      ledger_date: moment(),
      kind: 'IN'
    }
    this.setState({
      modal_ledger: true,
      ledger: ledger,
      ledger_action: 'ADD'
    })

  }

  handlerSearch(data) {
    let data2 = Object.assign({}, data);
    console.log(data2.start_date)
    this.setState({
      start_date:Utility.formatDateViewToDate(data2.start_date),
      end_date:Utility.formatDateViewToDate(data2.end_date),
    })
    const {branch} = this.props.auth
    delete data2.e_date
    delete data2.s_date
    data2.is_enabled = 1
    data2.branch = branch.id
    data2.start_date = Utility.formatDateViewToDate(data2.start_date);
    data2.end_date = Utility.formatDateViewToDate(data2.end_date);

    this.props.intData(data2)
  }

  async componentDidMount() {
    
    const {branch} = this.props.auth
    this.setState({
      loader_active: true
    });

    const q = {
      start_date: Utility.formatDate2(moment()),
      end_date: Utility.formatDate2(moment()),
      branch: branch.id,
      is_enabled: 1
    }

    this.props.intData(q)
    let elHeight = document.getElementById('table_width')
    this.setState({ table_width: elHeight.clientWidth});
  }

  getTypeSale(key) {
    let type = Settings.type_sale.filter((o) => { return o.value == key });
    return type[0];
  }

  setFieldValue(items) {

    const {ledger_category} = this.props

    let amount = items.length;
    let expenses = 0;
    let income = 0;
    for (let i = 0; i < items.length; i++) {
      const ledger_category = items[i].ledger_category
      items[i].object_title = ledger_category.title;
      items[i].is_void = 1
      items[i].branch_name = items[i].branch.name;
      items[i].date = Utility.formatDate(items[i].record_date);
      items[i].time = Utility.formatTime(items[i].record_date);
      items[i].kind_title = items[i].kind == 'EX' ? 'รายจ่าย' : 'รายรับ'

      items[i].income = Utility.priceFormat((items[i].kind == 'IN' ? parseFloat(items[i].total)  : 0)+ parseFloat(items[i].card_service))
      items[i].expenses = Utility.priceFormat(items[i].kind != 'IN' ? items[i].total : 0)

      

      if (items[i].ledger_category.id <= 3) {
        let item = items[i].object_bill;
        if (item) {
          items[i].bill_status_title = item.is_void ? 'ยกเลิกบิลแล้ว' : ''
          items[i].is_void = item.is_void;
        }
      }

      items[i].type_pay = Utility.getObjectByValue(Settings.payment_option, items[i].payment).text
      if (items[i].payment == 'CC') {
        items[i].cash_card = Utility.priceFormat(items[i].cash);
      }

    }

    return items;
  }

  loadBillDetail(data) {

    let action = 'exc';
    if (data.kind == 'BU')
      action = 'buy';
    else if (data.kind == 'SE')
      action = 'sell';

    this.setState({
      modal_detail: true,
      bill_loader_active: true,
      bill: data,
      total_sell_price: Utility.priceFormat(parseFloat(data.sell)),
      otal_sell_weight: 0,
      total_buy_price: Utility.priceFormat(parseFloat(data.buy)),
      net_price: data.total,
      action: action,
      itemCount: data.amount,
      description: data.description
    })

    let url = Settings.baseUrl + "/bill/" + data.id + "/items/";
    Utility.get(url, (e, res) => {
      let billItems = [];
      for (let i in res) {
        let d = res[i]
        if (d.kind == 'SE') {
          let p = Utility.getObject(this.products, d.product_id)
          billItems.push({
            type_title: 'ขาย',
            type: 'SE',
            id: i + 1,
            amount: d.amount,
            sell_price: d.sell,
            sell_weight: 0,
            sell_weight_real: d.weight,
            buy_price: 0,
            buy_weight: 0,
            gold_price: d.price_gold,
            product_code: p.code,
            product_id: p.id,
            product_name: p.name,
            product_type: p.kind.name,
            category_name: p.category.name,
            category_id: p.category.id,
            code_change: d.code_change,
            exchange: d.exchange,
          })
        } else {
          let c = Utility.getObject(this.categories, d.category)
          billItems.push({
            type_title: 'ซื้อ',
            type: 'BU',
            'id': i + 1,
            amount: 0,
            sell_price: 0,
            sell_weight: 0,
            sell_weight_real: 0,
            buy_price: d.buy,
            buy_weight: d.weight,
            gold_price: d.price_gold,
            product_code: '',
            product_name: d.product_name,
            product_type: '',
            category_name: c.name,
            category_id: d.catory,
            code_change: d.code_change,
            exchange: d.exchange,
          })
        }
      }


      this.setState({
        bill_loader_active: false,
        billItems: billItems
      })

    });

    let url2 = Settings.baseUrl + "/bill_staff/?bill_id=" + data.id;
    Utility.get(url2, (e, res) => {
      this.setState({
        billstaff: res
      })
    });
  }

  loadBillInfo(bill_id) {

    let url2 = Settings.baseUrl + "/bills/" + bill_id + "/";
    Utility.get(url2, (e, res) => {
      this.loadBillDetail(res);
    });
  }

  handlerVoidBill(e, data) {

    if (data.is_void == 1) {
      alert('ถูกลบแล้ว');
      return;
    }

    let url = Settings.baseUrl + "/bill/" + data.id + "/void/"
    Utility.getAuth(url, (e, d) => {

      alert('ยกเลิกบิลสำเร็จ');
      this.componentDidMount();
      this.setState({
        modal_detail: false,
        bill: {}
      })
    });
  }

  loadledger(object) {
    this.setState({
      modal_ledger: true,

    })
    const _o = JSON.parse(JSON.stringify(object))
    _o.customer = object.customer.id
    this.setState({
      ledger: _o,
      ledger_action: 'EDIT'
    })
  }

  handlerModalInputChange(e, d) {

    const {ledger_category} = this.props

    let ledger = this.state.ledger;
    ledger[d.name] = d.value;

    if (d.name == 'kind') {
      let _ledger_category = []
      for (let i in ledger_category) {
        if (_ledger_category[i].kind == d.value)
        _ledger_category.push({
            value: ledger_category[i].id,
            key: ledger_category[i].id,
            text: ledger_category[i].title
          });
      }
      this.setState({
        ledger_category: _ledger_category
      })
    }


    this.setState({
      ledger: ledger
    })
  }

  addledgerList(e) {
    e.preventDefault()
    this.setState({
      modal_ledger: false,
      modal_ledger_list: true
    })

  }

  handlerDropdownSearch(e,{value}){
    const {branches} = this.props
    const branch = branches.find((item)=>item.id==value)
    this.props.handleBranchChange(branch)
  }

  deleteledger(object) {
    let url = Settings.baseUrl + "/ledger/" + object.id + "/";
    let data = {
      status: 0
    }
    Utility.patch(url,data, (s, d,c) => {

      if(s){
        alert('ลบข้อมูลสำเร็จ')
        this.resetForm()
        this.componentDidMount();
        
      }else{
        alert(d.error)
      }

      this.setState({
        loadingSubmitledger: false
      })
      
    });
  }


  render() {
    
    const {ledger_list} = this.props
    const {branch,role} = this.props.auth
    const items = this.setFieldValue(ledger_list.list);

    const amount = items.length;
    let expenses = 0
    let income = 0;
    items.map((item,i)=>{

      income += parseFloat(item.kind == 'IN' ? parseFloat(item.total)+parseFloat(item.card_service)  : 0)
      expenses += parseFloat(item.kind != 'IN' ? item.total : 0)

    });

    return (
      <div>
        {this.state.modal_ledger &&
        <LedgerForm
            action={this.state.ledger_action}
            ledger={this.state.ledger}
            onClose={()=>this.setState({modal_ledger: false})}
            ledger_category_list={this.state.ledger_category_list}
            onSave={()=>{
              alert('บันทึกข้อมูลสำเร็จ!')
              this.componentDidMount()
              this.setState({modal_ledger: false})
            }} />}
        <Form size='small'>
          <Form.Group>
            <Form.Field width={6}>
              <Header floated='left' as='h3'>
                <div style={{padding: '7px'}} className="pull-left">รายรับ-รายจ่าย </div>
                <div className="pull-left">{role==='A' && <DropdownBranch  fluid size='small' fluid selection onChange={this.handlerDropdownSearch.bind(this)} name="branch" value={branch.id}  />}</div>
              </Header>
             
            </Form.Field>
            <Form.Field width={10}>
              <Button id='addIn-Out' size='small' primary floated='right' onClick={this.addledger} labelPosition='left' icon='plus' content='เพิ่มรายการ' />
            </Form.Field>
          </Form.Group>
        </Form>
        <Segment color='black' >
          <Dimmer className={ledger_list.is_ready ?'':'active'} inverted>
            <Loader content='Loading' inverted />
          </Dimmer>
          <LedgerFormSearch value={this} onSubmitSearch={this.handlerSearch} />
          <div id='table_width'>
            <Table
              rowsCount={items.length}
              rowHeight={35}
              headerHeight={35}
              width={this.state.table_width}
              height={Settings.table_hiegh}>

              <Column
                header={<Cell></Cell>}
                cell={
                  <OptionItemsCell data={items} field="kind_title" onClickMenu={(e, data) => {
                    let index = data.positon;
                    let object = items[index];
                    if (data.action == 'view') {
                      if (object.ledger_category <= 1)
                       this.loadledger(object);
                      else {
                        this.loadledger(object);
                      }
                    } else if (data.action == 'edit') {
                      this.loadledger(object);
                    } else if (data.action == 'delete') {
                      if (object.is_void) {
                        if (window.confirm('ยืนยันลบรายการนี้')) {
                          this.deleteledger(object)
                        }
                      }
                      else
                        alert('กรุณายกเลิกบิลก่อน');
                    }
                  }} />
                }
                width={80}
              />

              <Column
                header={<Cell>รับ-จ่าย</Cell>}
                cell={
                  <IconItemsCell data={items} field="kind_title" />
                }
                width={80}
              />
              <Column
                header={<Cell>สาขา</Cell>}
                cell={
                  <ItemsCell data={items} field="branch_name" />
                }
                width={150}
              />
              <Column
                header={<Cell>เลขที่ใบสำคัญ</Cell>}
                cell={
                  <ItemsCell data={items} field="number" />
                }
                width={150}
              />
              <Column
                header={<Cell>เลขที่อ้างอิง</Cell>}
                cell={
                  <ItemsCell data={items} field="object_number" />
                }
                width={150}
              />
              <Column
                header={<Cell >วันที่</Cell>}
                cell={
                  <ItemsCell data={items} field="date" />
                }
                width={100}
              />

              <Column
                header={<Cell>เวลา</Cell>}
                cell={
                  <ItemsCell data={items} field="time" />
                }
                width={80}
              />
              <Column
                header={<Cell>รายการ</Cell>}
                cell={
                  <ItemsCell data={items} field="object_title" />
                }
                width={180}
              />

              <Column
                header={<Cell className='text-right'>จำนวนรับ</Cell>}
                cell={
                  <ItemsCell data={items} field="income" textAlign='text-right' color='green' />
                }
                width={120}
              />
              <Column
                header={<Cell className='text-right'>จำนวนจ่าย</Cell>}
                cell={
                  <ItemsCell data={items} field="expenses" textAlign='text-right' color='red' />
                }
                width={180}
              />
              <Column
                header={<Cell>ประเภทชำระ</Cell>}
                cell={
                  <ItemsCell data={items} field="type_pay" />
                }
                width={100}
              />

              <Column
                header={<Cell>ชื่อลูกค้า</Cell>}
                cell={
                  <ItemsCell data={items} field="customer_name" />
                }
                width={150}
              />
              <Column
                header={<Cell >พนักงาน</Cell>}
                cell={
                  <ItemsCell data={items} field="staff_name" />
                }
                width={150}
              />
              <Column
                header={<Cell className='text-right'>สถานะยกเลิกบิล</Cell>}
                cell={
                  <ItemsCell data={items} field="bill_status_title" textAlign='text-right' />
                }
                width={120}
              />

              <Column
                header={<Cell className='text-right'>ยอดเงินสด+เครดิต</Cell>}
                cell={
                  <ItemsCell data={items} field="cash_card" textAlign='text-right' />
                }
                width={200}
              />
              <Column
                header={<Cell >หมายเหตุ</Cell>}
                cell={
                  <ItemsCell data={items} field="description"  />
                }
                width={300}
              />
            </Table>
          </div>
          {this.state.open_print ? <LedgerPrintPreview
            data={{
              start_date: this.state.start_date,
              end_date: this.state.end_date
            }}
            onClose={() => {
              this.setState({ open_print: false });
            }}
            items={items}
          /> : ''}
          <br />
          <Form className='attached fluid' size='small' >
            <Form.Group>
              <Form.Input id = 'amounttrans' label='จำนวน' placeholder='' className='text-right' width={2} value={amount} />
              <Form.Input disabled tabIndex={-1} width={8} transparent={true} />
              <Form.Input  id = 'amountIncomes' label='รับ' placeholder='' className='text-right green' width={2} value={Utility.priceFormat(income)} color='green' readOnly />
              <Form.Input id = 'amountOutcomes' label='จ่าย' placeholder='' className='text-right red' width={2} value={Utility.priceFormat(expenses)} color='red' readOnly />
              <Form.Input id = 'amountTotals' label='คงเหลือ' placeholder='' className='text-right yellow' width={2} value={Utility.priceFormat(income - expenses)} color='yellow' readOnly />
            </Form.Group>
            <Form.Group>
              <Form.Field width={16}>

                <Button size='small' primary floated='right' onClick={this.addledger} labelPosition='left' icon='print' content='พิมพ์' onClick={() => {
                  this.setState({
                    open_print: true
                  })
                }} />
              </Form.Field>
            </Form.Group>
          </Form>
        </Segment>
      </div>
    );
  }
}

const mapStateToProps = ({ledger_category,ledger_list,branches,auth}) =>{
  console.log('mapStateToProps',ledger_category)
  return ({
    auth,
    branches,
    ledger_list,
    ledger_category
  })
}


const mapDispatchToProps = dispatch => ({
  intData: (q)=>{
    loadLedger(dispatch,Utility.jsonToQueryString(q))
  },
  handleBranchChange: (branch) => {
    dispatch(activations_branch(branch))

    const q = {
      start_date: Utility.formatDate2(moment()),
      end_date: Utility.formatDate2(moment()),
      branch: branch.id,
      is_enabled: 1
    }
    loadLedger(dispatch,Utility.jsonToQueryString(q))
  }
  
})
export default connect(
  mapStateToProps,mapDispatchToProps
)(Ledger)
