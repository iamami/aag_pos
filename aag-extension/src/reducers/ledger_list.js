const default_state = {
  is_ready: false,
  list: [],
  filter: {}

}
const ledger_list = (state = default_state, action) => {
  switch (action.type) {
    case 'SET_LEDGER_LIST':
      return {
        ...state,
        is_ready: true,
        list: action.list,
      }
      default:
        return state
  }
}

export default ledger_list