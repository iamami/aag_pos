const redirect = (state = '', action) => {
  switch (action.type) {
    case 'REDIRECT':
      return action.path
  default:
      return state
  }
}

export default redirect