import {
  combineReducers
} from 'redux'
import modal_open from './modal_open'
import branches from './branches'
import products_name from './products_name'
import auth from './auth'
import bills_list from './bills_list'
import ledger_list from './ledger_list'
import ledger_category from './ledger_category'
import lease_modal from './lease_modal'

export default combineReducers({
  modal_open,branches,products_name,auth,bills_list,ledger_list,ledger_category,lease_modal
})