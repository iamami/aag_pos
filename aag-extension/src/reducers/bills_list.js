const default_state = {
  is_ready: false,
  list: [],
  filter: {}

}
const bills_list = (state = default_state, action) => {
  switch (action.type) {
    case 'SET_BILL_LIST':
      return {
        ...state,
        list: action.list,
        filter: action.filter
      }
    case 'SEARCH_BILL_LIST':
      return {
        ...state,
        filter: action.filter,
      }
      default:
        return state
  }
}

export default bills_list