const modal_open = (state = {}, action) => {
    switch (action.type) {
      case 'OPEN_MODAL_LEASE_PRODUCT':
        return action.data
    default:
        return state
    }
  }
  
  export default modal_open