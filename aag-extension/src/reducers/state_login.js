const state_login = (state = '', action) => {
    switch (action.type) {
      case 'STATE_LOGIN':
        return action.is_login
    default:
        return state
    }
  }
  
  export default state_login