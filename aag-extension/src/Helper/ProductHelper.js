/*eslint eqeqeq: "off"*/
import Utility from '../Utility';
import Settings from '../Settings';
const productHelper = {

    getCode: (category, type, type_weight, weight, weight_g, type_sale,price_tag) => {

        //let price_tag = 25000
        let typ_w = type_weight == 1 ? 'B' : 'G';
        let w = type_weight == 1 ? parseInt(weight * 100) : parseInt(weight_g * 100);
        weight_g = Utility.weightFormat(weight_g)

        if (type_sale == 1) {
            let code = type.code + category.code + typ_w + w;
            let n_w = type_weight == 1 ? productHelper.getNameBaht(weight) : weight_g + 'ก.';
            let name = type.name + n_w + '(' + category.name + ')';
            return {
                name: name,
                code: code
            };
        } else {
            let code = type.code + category.code + 'F' + parseInt(price_tag);
            let n_w = type_weight == 1 ? productHelper.getNameBaht(weight) : weight_g + 'ก.';
            let name = type.name + n_w + '(' + category.name + ')';
            return {
                name: name,
                code: code
            };
        }
    },

    getNameBaht: (v) => {
        let o = Utility.getObjectByValue(Settings.weight, v);

        if (o == null) {
            return v + 'บ.'
        } else {
            return o.text;
        }
    }
}

export default productHelper