/*eslint eqeqeq: "off"*/
/*eslint jsx-a11y/anchor-is-valid: "off"*/
/*eslint no-unused-vars: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint array-callback-return: "off"*/
/*eslint no-redeclare: "off"*/
/*eslint default-case: "off"*/
/*eslint no-whitespace-before-property: "off"*/
/*eslint react/no-direct-mutation-state: "off"*/
/*eslint no-useless-constructor: "off"*/
/*eslint react/jsx-no-duplicate-props: "off"*/

import React, { Component } from 'react';
import { Icon } from 'semantic-ui-react';
var config = require( './config' )

// VERSION
const VERSION = '1.2.44';

const Settings = {
  mode: process.env.NODE_ENV,
  version: VERSION,
  invoice_info: {
    name: 'บริษัท ห้างทอง เอเอ เยาวราช จำกัด',
    address1: '123/20,12/21 ถนนฉลองกรุง',
    address2: 'แขวงลำปลาทิว เขตลาดกระบัง กรุงเทพมหานคร 10520',
    citizen_id: '0105558153546'
  },
  'app_name': '',
  'baseUrl': config.api.url + '/v2',
  'authUrl': config.api.url + '/o/token/',
  'revokeTokenUrl': config.api.url + '/o/revoke_token/',
  'client_id': config.client.id,
  'client_secret': config.client.secret,
  'table_width': 1200,
  'table_hiegh': 300,
  'input_size': 'tiny',
  'papter88': 332.598,
  'papterA4': 2480,
  'payment': {
    'CD': 'บัตร',
    'CS': 'เงินสด',
    // 'CH': 'เช็ค',
    'CC': 'บัตร+เงินสด'
  },
  'payment_option': [
    {
      key: 'CD',
      value: 'CD',
      text: 'บัตร'
    }, {
      key: 'CS',
      value: 'CS',
      text: 'เงินสด'
    // }, {
    //   key: 'CH',
    //   value: 'CH',
    //   text: 'เช็ค'
    }, {
      key: 'CC',
      value: 'CC',
      text: 'บัตร+เงินสด'
    // }, {
      // key: 'TF',
      // value: 'TF',
      // text: 'โอน'
    }
  ],
  'ledger_payment': [
    {
      key: 'CS',
      value: 'CS',
      text: 'เงินสด'
    }, {
      key: 'CD',
      value: 'CD',
      text: 'บัตร'
    }, {
      // key: 'TF',
      // value: 'TF',
      // text: 'โอน'
    }
  ],
  'status_bill': {
    N: 'ยังไม่เคลียร์บิล',
    Y: 'เคลียร์บิลแล้ว'
  },
  'status_stock': {
    N: 'ยังไม่อัพเดทสต๊อก',
    Y: 'อัพเดทสต๊อกแล้ว'
  },
  'user_role': [
    {
      key: 'A',
      value: 'A',
      text: 'Admin'
    }, {
      key: 'U',
      value: 'U',
      text: 'User'
    }
  ],
  type_weight: [
    {
      key: 1,
      value: 1,
      text: 'บาท'
    }, {
      key: 2,
      value: 2,
      text: 'กรัม'
    }
  ],
  type_sale: [
    {
      key: 1,
      value: 1,
      text: 'งานชั่ง'
    }, {
      key: 2,
      value: 2,
      text: 'งานชิ้น'
    }
  ],
  weight: [
    {
      key: 0.125,
      value: 0.125,
      text: '1/2สลึง'
    }, {
      key: 0.25,
      value: 0.25,
      text: '1สลึง'
    }, {
      key: 0.50,
      value: 0.50,
      text: '2สลึง'
    }, {
      key: 0.75,
      value: 0.75,
      text: '3สลึง'
    }, {
      key: 1.5,
      value: 1.5,
      text: '6สลึง'
    }, {
      key: 1.0,
      value: 1.0,
      text: '1บาท'
    }, {
      key: 2.0,
      value: 2.0,
      text: '2บาท'
    }, {
      key: 3.0,
      value: 3.0,
      text: '3บาท'
    }, {
      key: 4.0,
      value: 4.0,
      text: '4บาท'
    }, {
      key: 5.0,
      value: 5.0,
      text: '5บาท'
    }, {
      key: 6.0,
      value: 6.0,
      text: '6บาท'
    }, {
      key: 7.0,
      value: 7.0,
      text: '7บาท'
    }, {
      key: 8.0,
      value: 8.0,
      text: '8บาท'
    }, {
      key: 9.0,
      value: 9.0,
      text: '9บาท'
    }, {
      key: 10.0,
      value: 10.0,
      text: '10บาท'
    }
  ],
  ledger_kind: {
    'IN': 'รายรับ',
    'EX': 'รายจ่าย'
  },
  ledger_kind_option: [
    {
      key: 'IN',
      value: 'IN',
      text: 'รายรับ',
      content: <div ><Icon color='green' name='sticky note'/>
          รายรับ</div>
    }, {
      key: 'EX',
      value: 'EX',
      text: 'รายจ่าย',
      content: <div ><Icon color='red' name='sticky note'/>
          รายจ่าย</div>
    }
  ],
  status_lease: {
    '1': 'ใหม่',
    '2': 'ต่อดอก',
    '3': 'ไถ่คืน',
    '4': 'คัดออก'
  },
  status_lease_options: [
    {
      key: 1,
      value: 1,
      text: 'ใหม่'
    },
    {
      key: 2,
      value: 2,
      text: 'ต่อดอก'
    },
    {
      key: 3,
      value: 3,
      text: 'ไถ่คืน'
    },
    {
      key: 4,
      value: 4,
      text: 'คัดออก'
    }
  ],
  is_enabled_option: [
    {
      key: 1,
      value: 1,
      text: 'แสดง'
    },
    {
      key: 0,
      value: 0,
      text: 'ซ่อน'
    }],
    is_enabled_option_search: [{
      key: 2,
      value: 2,
      text: '--สถานะ--'
    },
      {
        key: 1,
        value: 1,
        text: 'แสดง'
      },
      {
        key: 0,
        value: 0,
        text: 'ซ่อน'
      }],
    is_active_option: [
      {
        key: 1,
        value: true,
        text: 'Active'
      },
      {
        key: 0,
        value: false,
        text: 'Inactive'
      }
  ]
};

export default Settings;
