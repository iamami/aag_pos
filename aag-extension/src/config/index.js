console.log('process.env',window.location.href.includes('pos.aagold-th'))
if (process.env.REACT_APP_MODE === 'production') {
    module.exports = require('./prod.config');
}else if (process.env.REACT_APP_MODE === 'staging') {
    module.exports = require('./stg.config');
}else if (process.env.REACT_APP_MODE === 'testing') {
    module.exports = require('./test.config');
}else {
    module.exports = require('./dev.config');
}