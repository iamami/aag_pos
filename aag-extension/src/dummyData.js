/*eslint eqeqeq: "off"*/
const dummyData = {
  branches : [
    {
      key: '01',
      value: 'ลาดกระบัง',
      text: 'ลาดกระบัง',
    },
    {
      key: '02',
      value: 'พัทยา',
      text: 'พัทยา',
    }
  ],
  staffs : [
    {
      key: '0101',
      value: 'sale 1',
      text: 'sale 1',
      branch: 'ลาดกระบัง'
    },
    {
      key: '0102',
      value: 'sale 2',
      text: 'sale 2',
      branch: 'ลาดกระบัง'
    },
    {
      key: '0103',
      value: 'sale 3',
      text: 'sale 3',
      branch: 'ลาดกระบัง'
    }
  ],
  categories : [
    {
      key: 1,
      value: '40',
      text: '40%',
      weight: 0
    },
    {
      key: 2,
      value: '90',
      text: '90%',
      weight: 0
    },
    {
      key: 3,
      value: '96.5',
      text: '96.5%',
      weight: 15.16
    },
    {
      key: 4,
      value: '99.99',
      text: '99.99%',
      weight: 15.244
    }
  ],
  weights : [
    {
      key: 1,
      value: '1 สลึง',
      text: '1 สลึง',
      ratio: 0.25
    },
    {
      key: 2,
      value: '2 สลึง',
      text: '2 สลึง',
      ratio: 0.5
    },
    {
      key: 3,
      value: '1 บาท',
      text: '1 บาท',
      ratio: 1
    },
    {
      key: 4,
      value: '2 บาท',
      text: '2 บาท',
      ratio: 2
    }
  ],
  productTypes : [
    {
      key: 1,
      value: 'ทองแท่ง',
      text: 'ทองแท่ง'
    },
    {
      key: 2,
      value: 'แหวน',
      text: 'แหวน'
    },
    {
      key: 3,
      value: 'กำไร',
      text: 'กำไร'
    },
    {
      key: 4,
      value: 'สร้อยคอ',
      text: 'สร้อยคอ'
    },
    {
      key: 5,
      value: 'ต่างหู',
      text: 'ต่างหู'
    }
  ],
  styles : [
    {
      key: 1,
      value: 'โซ่',
      text: 'โซ่'
    },
    {
      key: 2,
      value: 'กระดิ่งทอง',
      text: 'กระดิ่งทอง'
    },
    {
      key: 3,
      value: 'เกร็ดดาว',
      text: 'เกร็ดดาว'
    },
    {
      key: 4,
      value: 'คตกิต',
      text: 'คตกิต'
    },
    {
      key: 5,
      value: 'บิดตะไบ',
      text: 'บิดตะไบ'
    }
  ],
  products : [
    {
      key : 1,
      value : 'N01B025',
      text : 'สร้อยคอ 1 สลึง',
      category : '96.5%',
      weight : '1 สลึง',
      type : 'สร้อยคอ',
      style : 'โซ่',
      weightG : 3.79
    },
    {
      key : 2,
      value : 'N02B050',
      text : 'สร้อยคอ 2 สลึง',
      category : '96.5%',
      weight : '2 สลึง',
      type : 'สร้อยคอ',
      style : 'คตกิต',
      weightG : (3.79 * 2)      
    }
  ],
  productCodes : [
    {
      key : 1,
      value : 'N01B025',
      text : 'N01B025',
      name : 'สร้อยคอ 1 สลึง',
      category : '96.5%',
      weight : '1 สลึง',
      type : 'สร้อยคอ',
      style : 'โซ่',
      weightG : 3.79
    },
    {
      key : 2,
      value : 'N02B050',
      text : 'N01B050',
      name: 'สร้อยคอ 2 สลึง',
      category : '96.5%',
      weight : '2 สลึง',
      type : 'สร้อยคอ',
      style : 'คตกิต',
      weightG : (3.79 * 2)
    }
  ],
  nextBillId : 'B010003',
  nextCustomerId : 'C010001',
  getProductByCode : function (value) {
    for (let i=0; i<this.products.length; i++) {
      if (this.products[i].value == value) {
        return this.products[i];
      }
    }

    return false;
  },

  getProductByName : function (value) {
    for (let i=0; i<this.products.length; i++) {
      if (this.products[i].text == value) {
        return this.products[i];
      }
    }

    return false;
  }
};

export default dummyData;




