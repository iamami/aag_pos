DELETE FROM public.mapi_notificationdata WHERE score_item_id in (Select id FROM public.aag_api_scoreitem WHERE bill_item_id in (SELECT id FROM public.aag_api_billitem WHERE bill_id in (SELECT id FROM public.aag_api_bill WHERE branch_id = 3)));
Delete FROM public.aag_api_scoreitem WHERE bill_item_id in (SELECT id FROM public.aag_api_billitem WHERE bill_id in (SELECT id FROM public.aag_api_bill WHERE branch_id = 3));
Delete FROM public.aag_api_billitem WHERE bill_id in (SELECT id FROM public.aag_api_bill WHERE branch_id = 3);
Delete FROM public.aag_api_billstaff WHERE bill_id in (SELECT id FROM public.aag_api_bill WHERE branch_id = 3);
DELETE FROM public.aag_api_bill WHERE branch_id = 3;

DELETE FROM public.mapi_notificationdata WHERE score_item_id in (SELECT id FROM aag_api_scoreitem WHERE interest_id in (SELECT id  FROM public.aag_api_leaseinterest WHERE lease_id in (SELECT id FROM public.aag_api_lease WHERE branch_id = 3)));
DELETE FROM aag_api_scoreitem WHERE interest_id in (SELECT id  FROM public.aag_api_leaseinterest WHERE lease_id in (SELECT id FROM public.aag_api_lease WHERE branch_id = 3));
DELETE FROM public.aag_api_leaseinterest WHERE lease_id in (SELECT id FROM public.aag_api_lease WHERE branch_id = 3);
DELETE FROM public.aag_api_leaseproduct WHERE lease_id in (SELECT id FROM public.aag_api_lease WHERE branch_id = 3);
DELETE FROM public.aag_api_lease WHERE branch_id = 3;

DELETE FROM public.aag_api_stockproductitem WHERE branch_id = 3;
DELETE FROM public.aag_api_stockproduct WHERE branch_id = 3; 

DELETE FROM public.aag_api_stockcategoryitem WHERE branch_id = 3;
DELETE FROM public.aag_api_stockcategory WHERE branch_id = 3;