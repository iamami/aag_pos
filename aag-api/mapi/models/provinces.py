from django.db import models
# Create your models here.
class Provinces(models.Model):

    code = models.CharField(max_length=6,unique=True,db_index=True)
    name = models.CharField(max_length=150)
    name_eng = models.CharField(max_length=150)
    geo_id = models.IntegerField()