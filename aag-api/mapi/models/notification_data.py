from django.db import models
from django.contrib.auth.models import User
from mapi.models.customer_user import CustomerUser
from aag_api.models.customer import Customer
from django.core.validators  import RegexValidator
from django.utils.crypto import get_random_string
from pyfcm import FCMNotification
from django.conf import settings
from django.contrib.postgres.fields import JSONField

# Create your models here.
class NotificationData(models.Model):

    title = models.CharField(max_length=55)
    message = models.CharField(max_length=255)
    created_datetime = models.DateTimeField(auto_now_add=True)
    gold_price = models.ForeignKey('aag_api.GoldPrice', related_name='data_gold_price', on_delete=models.CASCADE,blank=True,null=True)
    reward = models.ForeignKey('aag_api.Reward', related_name='data_reward', on_delete=models.CASCADE,blank=True,null=True)
    redeem = models.ForeignKey('aag_api.Redeem', related_name='data_redeem', on_delete=models.CASCADE,blank=True,null=True)
    score_item = models.ForeignKey('aag_api.ScoreItem', related_name='data_score_item', on_delete=models.CASCADE,blank=True,null=True)
    data = JSONField()
    

    def __str__(self):
        return '%s' % (self.title)