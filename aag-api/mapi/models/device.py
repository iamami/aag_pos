from django.db import models
from django.contrib.auth.models import User
from mapi.models.customer_user import CustomerUser
from aag_api.models.customer import Customer
from django.core.validators  import RegexValidator
from django.utils.crypto import get_random_string
from pyfcm import FCMNotification
from django.conf import settings
from mapi.models.notification_data import NotificationData
from mapi.models.notification import Notification


DEVICE_KIND_CHOICES = (
    ('and','Android'),
    ('ios','iOS')
)

# Create your models here.
class Device(models.Model):

    name = models.CharField(max_length=55, help_text='Device name')
    customer_user = models.ForeignKey(CustomerUser, related_name='device_user', on_delete=models.CASCADE)
    token = models.CharField(max_length=200, help_text='device registration_id',db_index=True)
    created_datetime = models.DateTimeField(auto_now_add=True)
    uuid = models.CharField(max_length=55, help_text='device UUID')
    kind = models.CharField(max_length=5, choices=DEVICE_KIND_CHOICES, help_text='and = Android, ios = iOS')

    def __str__(self):
        return '%s' % (self.token)

    
    def groupUser(device_list):

        g_user = {}
        n_user = []
        for item in device_list:
            k = item.customer_user.pk
            if k in g_user:
                continue
            else:
                g_user[k] = True
                n_user.append(item.customer_user)
        return n_user

    def sendNotiGoldPrice(device_list,title=None,message=None,data=None,gold_price=None):

        print("------------------")
        print(data)

        noti_data = NotificationData.objects.create(title=title,message=message,data=data,gold_price=gold_price )

        # delete noti gold_price
        Notification.objects.all().filter(kind=1).delete()

        # create noti list
        user_list = Device.groupUser(device_list)
        for item in user_list:
            Notification.objects.create(
                notification_data=noti_data,
                kind=1,
                status='unred',
                customer_user=item
            )
        
        return Device.sendNotiMessage(device_list,title,message,data)

    def sendNotiPromotion(device_list,title=None,message=None,data=None):

        noti_data = NotificationData.objects.create(
            title=title,
            message=message,
            data=data
        )

        # create noti list
        user_list = Device.groupUser(device_list)
        for item in user_list:
            Notification.objects.create(
                notification_data=noti_data,
                kind=2,
                status='unred',
                customer_user=item
            )
        
        return Device.sendNotiMessage(device_list,title,message,data)

    def sendNotiRewardNew(device_list,title=None,message=None,data=None,reward=None):

        noti_data = NotificationData.objects.create(
            title=title,
            message=message,
            data=data,
            reward=reward
        )

        # create noti list
        user_list = Device.groupUser(device_list)
        for item in user_list:
            Notification.objects.create(
                notification_data=noti_data,
                kind=3,
                status='unred',
                customer_user=item
            )
        
        return Device.sendNotiMessage(device_list,title,message,data)

    def sendNotiRedeem(device_list,title=None,message=None,data=None,redeem=None):

        try:
            noti_data = NotificationData.objects.get(redeem=redeem.pk)
            # delete
            Notification.objects.all().filter(notification_data=noti_data).delete()
        except NotificationData.DoesNotExist:
            noti_data = NotificationData.objects.create(
                title=title,
                message=message,
                data=data,
                redeem=redeem
            )

        # create noti list
        user_list = Device.groupUser(device_list)
        for item in user_list:
            Notification.objects.create(
                notification_data=noti_data,
                kind=4,
                status='unred',
                customer_user=item
            )
        
        return Device.sendNotiMessage(device_list,title,message,data)

    def sendNotiScoreItem(device_list,title=None,message=None,data=None,score_item=None):

        noti_data = NotificationData.objects.create(
            title=title,
            message=message,
            data=data,
            score_item=score_item
        )

        # create noti list
        user_list = Device.groupUser(device_list)
        for item in user_list:
            Notification.objects.create(
                notification_data=noti_data,
                kind=5,
                status='unred',
                customer_user=item
            )
        
        return Device.sendNotiMessage(device_list,title,message,data)

    def sendNotiMessage(device_list,title=None,message=None,data=None):

        push_service = FCMNotification(settings.FCM_SERVER_KEY)
        registration_ids = []
        for o in device_list:
            registration_ids.append(o.token)
        message_title = title
        message_body = message
        data_message = data
        notification_extra_kwargs = {
            'android_channel_id': 2
        }
        extra_kwargs = {
            'mutable_content': True
        }
        res = push_service.notify_multiple_devices(
                registration_ids=registration_ids, message_title=message_title, message_body=message_body, content_available=True, extra_kwargs=extra_kwargs, badge=10, sound='Default', data_message=data_message)
        
        # auto delete registration_ids
        index = 0
        error_registration_ids = []
        for item in res['results']:

            if 'error' in item:
                re_id = registration_ids[index]
                error_registration_ids.append(re_id)
            
            index = index + 1
        Device.objects.all().filter(token__in=error_registration_ids).delete()
        return res
