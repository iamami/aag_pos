from django.db import models
from django.contrib.auth.models import User
from mapi.models.customer_user import CustomerUser
from aag_api.models.customer import Customer
from django.core.validators  import RegexValidator
from django.utils.crypto import get_random_string
from pyfcm import FCMNotification
from django.conf import settings


TYPE_CHOICES = (
    (1,'ราคาทองเปลี่ยนแปลง'),
    (2,'โปรโมชั่นมาใหม่'),
    (3,'ของรางวัลมาใหม่'),
    (4,'สถานะการแลกของรางวัล'),
    (5,'รายการแต้มมีการเคลื่อนไหว')
)
STATUS_CHOICES = (
    ('read','Read'),
    ('unred','Unread')
)


# Create your models here.
class Notification(models.Model):

    kind = models.IntegerField(choices=TYPE_CHOICES)
    status = models.CharField(max_length=5, choices=STATUS_CHOICES)
    customer_user = models.ForeignKey(CustomerUser, related_name='notification_user', on_delete=models.CASCADE)
    created_datetime = models.DateTimeField(auto_now_add=True)
    notification_data = models.ForeignKey('NotificationData', related_name='notification_data', on_delete=models.CASCADE)

    class Meta:
        unique_together = ('customer_user', 'notification_data',)
    def __str__(self):
        return '%s' % (self.get_kind_display())