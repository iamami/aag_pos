from django.db import models
from django.contrib.auth.models import User
from aag_api.models.customer import Customer
from django.core.validators  import RegexValidator
from django.utils.crypto import get_random_string
from django.core.validators  import RegexValidator,MinValueValidator,MaxValueValidator
from django.core import mail
from django.template import loader
import requests
from django.conf import settings

KIND_CHOICES = (
    ('EM','Email'),
    ('PH','Phone')
)

# Create your models here.
class OneTimePassword(models.Model):

    customer = models.ForeignKey(Customer, related_name='opt_customer', on_delete=models.CASCADE)
    created_datetime = models.DateTimeField(auto_now_add=True)
    email = models.CharField(max_length=55,blank=True,null=True)
    phone = models.CharField(max_length=55,blank=True,null=True)
    kind = models.CharField(max_length=5, choices=KIND_CHOICES, help_text='EM = Email, PH = Phone')
    secret = models.CharField(max_length=55, help_text='Secret code')
    count = models.IntegerField(validators=[MinValueValidator(0)],default=0,blank=True)
    reference = models.CharField(max_length=55, help_text='reference code')
    is_confirm = models.BooleanField(default=False)

    def createCode(length):
        return get_random_string(length=length, allowed_chars='0123456789')
    
    def sendOtpEmail(customer,email):

        secret = OneTimePassword.createCode(6)
        reference = OneTimePassword.createCode(5)

        # delete old otp
        OneTimePassword.objects.filter(customer=customer).delete()
        OneTimePassword.objects.filter(email=email).delete()

        # create new otp
        o = OneTimePassword.objects.create(customer=customer,kind='EM',secret=secret,email=email,reference=reference)

        # send email
        context = {'otp': o}
        html_message = loader.render_to_string("mapi/mail/otp.html", context)
        EMAIL_FORM = 'AA gold <noreply@aagold-th.com>'
        mail.send_mail('แจ้งรหัส OTP สำหรับใช้งานระบบ AA gold application', "", settings.EMAIL_FORM, [email], html_message=html_message)

        return o

    def sendOtpPhone(customer,phone):

        secret = OneTimePassword.createCode(6)
        reference = OneTimePassword.createCode(5)
        
        # delete old otp
        OneTimePassword.objects.filter(customer=customer).delete()
        OneTimePassword.objects.filter(phone=phone).delete()

        # create new otp
        o = OneTimePassword.objects.create(customer=customer,kind='PH',secret=secret,phone=phone,reference=reference)

        # send MSM
        to = phone
        msg = "รหัส OTP สำหรับ AA gold application ของคุณ %s คือ %s  / จาก ห้างทอง เอ เอ เยาวราช" % (customer.name,secret)
        r = requests.get("http://www.thsms.com/api/rest?method=send&username=%s&password=%s&from=%s&to=%s&message=%s"%(settings.SMS_USER,settings.SMS_PASS,settings.SMS_FROM,to,msg))

        return o

    def __str__(self):
        return '%s %s' % (self.secret,self.kind)