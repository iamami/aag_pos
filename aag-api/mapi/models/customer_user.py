from django.db import models
from django.contrib.auth.models import User
from aag_api.models.customer import Customer
from django.core.validators  import RegexValidator
from django.utils.crypto import get_random_string
from django.core.validators  import RegexValidator,MinValueValidator,MaxValueValidator

DEVICE_KIND_CHOICES = (
    ('and','Android'),
    ('ios','iOS')
)

# Create your models here.
class CustomerUser(models.Model):

    user = models.OneToOneField(User, related_name='member_user', on_delete=models.CASCADE)
    customer = models.OneToOneField(Customer, related_name='member_customer', on_delete=models.CASCADE)
    created_datetime = models.DateTimeField(auto_now_add=True) 
    is_alert_gold_price = models.BooleanField(default=True)
    is_alert_promotion = models.BooleanField(default=True)
    is_alert_newreward = models.BooleanField(default=True)
    is_alert_redeem = models.BooleanField(default=True)
    is_alert_score = models.BooleanField(default=True)
    secret = models.CharField(max_length=32,blank=True, null=True,validators=[RegexValidator(r'^[a-z0-9]{7}$','"Value is not valid."')])
    login_fail = models.IntegerField(validators=[MinValueValidator(0)],default=0,blank=True)

    def createCodeSecurity(self):
        self.secret = get_random_string(length=32, allowed_chars='abcdefghijklmnopqrstuvwxyz0123456789')  
        self.save()
        return self.secret
        
    def __str__(self):
        return '%s' % (self.user.username)