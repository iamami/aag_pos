from django.db import models

# Create your models here.
class Zipcodes(models.Model):

    district_code = models.CharField(max_length=6)
    zipcode = models.CharField(max_length=5)

    def __str__(self):
        return '%s' % (self.zipcode)