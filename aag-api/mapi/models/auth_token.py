from django.db import models
from django.contrib.auth.models import User
from aag_api.models.customer import Customer
from django.core.validators  import RegexValidator
from oauth2_provider.models import Application
from django.contrib.auth.models import User,Group
import datetime
from django.utils.crypto import get_random_string
from oauth2_provider.models import AccessToken,AbstractAccessToken
from oauth2_provider.models import RefreshToken
from django.shortcuts import get_object_or_404

# Create your models here.
class AuthToken:

    logo = models.ImageField()
    agree = models.BooleanField()

    def delete_token(token,application):

        l = AccessToken.objects.all().filter(token=token)
        if l.count()==0:
            return False

        access_token = l[0]
        RefreshToken.objects.all().filter(access_token=access_token,application=application.pk).delete()
        access_token.delete()

        return True

    def create_token(user,application):

        date_1 = datetime.datetime.today()
        access_token = AccessToken.objects.create(
            user=user,
            application = application,
            expires = date_1 + datetime.timedelta(days=365),
            token = get_random_string(length=30, allowed_chars='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'),
            scope = 'read write'
        )

        r = RefreshToken.objects.create(
            access_token = access_token,
            application = application,
            token = get_random_string(length=30, allowed_chars='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'),
            user = user
        )

        return r