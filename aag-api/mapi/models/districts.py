from django.db import models
from mapi.models.provinces import *
from mapi.models.amphures import *
# Create your models here.
class Districts(models.Model):

    code = models.CharField(max_length=6,unique=True,db_index=True)
    name = models.CharField(max_length=150)
    name_eng = models.CharField(max_length=150)
    amphur = models.ForeignKey(Amphures, related_name='districts', on_delete=models.CASCADE)
    province = models.ForeignKey(Provinces, related_name='district_province', on_delete=models.CASCADE)
    geo_id = models.IntegerField()

    def __str__(self):
        return self.name