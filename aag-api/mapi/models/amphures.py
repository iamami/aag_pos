from django.db import models
from mapi.models.provinces import *
# Create your models here.
class Amphures(models.Model):

    code = models.CharField(max_length=6,unique=True,db_index=True)
    name = models.CharField(max_length=150)
    name_eng = models.CharField(max_length=150)
    province = models.ForeignKey(Provinces, related_name='amphures_province', on_delete=models.CASCADE)
    geo_id = models.IntegerField()