from django.conf.urls import url
from django.contrib import admin
from rest_framework.authtoken import views
from rest_framework.urlpatterns import format_suffix_patterns
from mapi.views.customer import *
from mapi.views.gold_price import GoldPriceView
from mapi.views.device import DeviceView
from mapi.views.score import *
from mapi.views.score_item import scoreItemList,scoreItemDetail
from fcm_django.api.rest_framework import FCMDeviceAuthorizedViewSet
from mapi.views.reward import *
from mapi.views.redeem import *
from mapi.views.user import *
from mapi.views.auth import *
from mapi.views.otp import *
from mapi.views.revoke import *
from mapi.views.branch import *
from mapi.views.regulations import *
from mapi.views.notification import *
from mapi.views.address import *

urlpatterns = [    
    # customer profile
    url(r'^v1/customer/checking/$', CustomerCheckingView.as_view(),name='customer-checking'),
    url(r'^v1/customer/profile/$', CustomerProfileView.as_view(),name='customer-profile'),
    url(r'^v1/customer/$', CustomerProfileEditView.as_view(),name='customer-profile-edit'),
    # customer score
    url(r'^v1/customer/score/$', scoreView.as_view()),
    url(r'^v1/customer/score_item/$', scoreItemList.as_view(),name='scoreitem-list'),
    url(r'^v1/customer/score_item/(?P<pk>[0-9]+)/$', scoreItemDetail.as_view(),name='scoreitem-detail'),
    url(r'^v1/score/condition/$', ScoreConditionView.as_view()),
    
    url(r'^v1/address/$', AddressDetail.as_view()),

    # otp 
    url(r'^v1/otp/$',OneTimePasswordCreateView.as_view(),name='opt-create'),
    url(r'^v1/otp/forgetpassword/$',OneTimePasswordForGetPasswordCreateView.as_view(),name='opt-create-forgetpassword'),
    url(r'^v1/otp/username/$',OneTimePasswordEditProfileCreateView.as_view(),name='opt-create-edit-username'),
    url(r'^v1/otp/(?P<pk>[0-9]+)/confirm/$',OneTimePasswordDetail.as_view(),name='opt-comfirm'),
    url(r'^v1/otp/(?P<pk>[0-9]+)/(?P<secret>[0-9A-Za-z]+)/customer/$', OneTimePasswordCustomerDetail.as_view(),name='opt-customer-detail'),

    url(r'^v1/user/$', UserView.as_view(),name='user'),
    url(r'^v1/user/newpassword/$', NewPasswordView.as_view(),name='user-newpassword'),
    url(r'^v1/user/register/$', RegisterView.as_view() ,name='user-register'),
    url(r'^v1/user/setting/$', UserSettingView.as_view() ,name='user-setting'),
    url(r'^v1/user/login/$', LoginView.as_view() ,name='user-login'),
    url(r'^v1/user/password/checking/$', PasswordCheckingView.as_view() ,name='user-password-checking'),
    url(r'^v1/user/password/change/$', ChangePasswordView.as_view(),name='user-changepassword'),

    url(r'^v1/gold_price/$', GoldPriceView.as_view()),

    url(r'^v1/devices/$', DeviceView.as_view(), name='devices-create'),

    url(r'^v1/reward/$', RewardList.as_view(),name='reward-list'),
    url(r'^v1/reward/(?P<pk>[0-9]+)/$', RewardDetail.as_view(),name='reward-detail'),

    url(r'^v1/redeem/$', RedeemList.as_view(),name='redeem-list'),
    url(r'^v1/redeem/(?P<pk>[0-9]+)/$', RedeemDetail.as_view(),name='redeem-detail'),
    url(r'^v1/redeem/condition/$', RedeemConditionView.as_view(),name='redeem-condition'),


    url(r'^v1/revoke/token/$', RevokeTokenView.as_view(),name='revoke-token'),


    url(r'^v1/branch/$', BranchListView.as_view(),name='branch-list'),
    # url(r'^v1/branch/(?P<pk>[0-9]+)/reward/$', BranchRewardList.as_view(),name='branch-reward-list'),
    # url(r'^v1/branch/(?P<pk>[0-9]+)/reward/(?P<reward>[0-9]+)/$', BranchRewardDetail.as_view(),name='branch-reward-detail'),

    url(r'^v1/regulations/$', RegulationsView.as_view(),name='regulations'),

    url(r'^v1/notification/counter/$', CounterView.as_view(),name='notification-counter'),
    url(r'^v1/notification/$', NotificationList.as_view(),name='notification-list'),
    url(r'^v1/notification/sender/$', SenderView.as_view(),name='notification-sender'),
    url(r'^v1/notification/(?P<pk>[0-9]+)/read/$', NotificationDetail.as_view(),name='notification-read'),
    
]
urlpatterns = format_suffix_patterns(urlpatterns)
