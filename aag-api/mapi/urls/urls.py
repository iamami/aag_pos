from django.conf.urls import url, include
from rest_framework import routers
from rest_framework.authtoken import views
from django.contrib import admin
from mapi.views.debug import *
from django.conf import settings
from django.conf.urls.static import static
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    # OAuth 2 endpoints:
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns.append(url(r'^debug/delete_user/(?P<customer_code>C[0-9]+)/$',DeleteUser.as_view(),name='debug-delete-user'))
    
