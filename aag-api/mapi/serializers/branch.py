from rest_framework import serializers
from aag_api.models.branch import Branch

class BranchListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Branch
        fields = ('id','code','name')
