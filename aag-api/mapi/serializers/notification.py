from rest_framework import serializers
from django.contrib.auth.models import User,Group
from mapi.models.notification import Notification
from mapi.models.notification_data import NotificationData

NOTIFICATION_KIND_CHOICES = (
    (1,'ราคาทองเปลี่ยนแปลง'),
    (2,'มีโปรโมชั่นมาใหม่'),
    (3,'มีของรางวัลมาใหม่'),
    (4,'สถานะการแลกของรางวัล'),
    (5,'รายการแต้มมีการเคลื่อนไหว')
)

class JSONSerializerField(serializers.Field):
    """ Serializer for JSONField -- required to make field writable"""
    def to_internal_value(self, data):
        return data
    def to_representation(self, value):
        return value

class NotificationSenderSerializer(serializers.Serializer):
    kind = serializers.ChoiceField(help_text='ประเภท',choices=NOTIFICATION_KIND_CHOICES)
    title = serializers.CharField(max_length=55,help_text='หัวข้อ')
    message = serializers.CharField(required=False,min_length=6,max_length=255,help_text='ข้อความ', allow_blank=True)
    data = serializers.CharField()
    client_id = serializers.CharField(max_length=55,help_text='client id')
    client_secret = serializers.CharField(max_length=230,help_text='client secret')
    user = serializers.IntegerField(required=False)

    def validate_user(self, value):
        
        if value is not None and User.objects.all().filter(pk=value).count() == 0:
            raise serializers.ValidationError("ไม่พบ User นี้")
            
        return value

class NotificationDataSerializer(serializers.ModelSerializer):


    class Meta:
        model = NotificationData
        fields = ('title','message','data')

class NotificationSerializer(serializers.ModelSerializer):
    notification_data = NotificationDataSerializer()
    kind_display = serializers.CharField(source='get_kind_display')
    status_display = serializers.CharField(source='get_status_display')

    class Meta:
        model = Notification
        fields = ('id','kind','customer_user','status','created_datetime','notification_data','kind_display','status_display')

class NotificationReadSerializer(serializers.ModelSerializer):
   
    class Meta:
        model = Notification
        fields = ('status',)