from rest_framework import serializers
from aag_api.models.redeem import Redeem
from aag_api.serializers import RewardSerializer,BranchSerializer,BranchShortSerializer,RewardShortSerializer

class RedeemSubmitSerializer(serializers.ModelSerializer):

    class Meta:
        model = Redeem
        fields = ('branch','reward','amount','receipt_date','address','kind')


class RedeemListSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='redeem-detail',
        lookup_field='pk'
    )
    status_display = serializers.CharField(source='get_status_display')
    kind_display = serializers.CharField(source='get_kind_display')
    reward = RewardShortSerializer()
    branch = BranchShortSerializer()
    class Meta:
        model = Redeem
        fields = ('branch','reward','amount','status','status_display','url','created_datetime','receipt_date','address','kind','kind_display')

class RedeemFullSerializer(serializers.ModelSerializer):
    reward = RewardSerializer()
    branch = BranchShortSerializer()
    class Meta:
        model = Redeem
        fields = ('branch','reward','amount','remrak','status','created_datetime','receipt_date')

