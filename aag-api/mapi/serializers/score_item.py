from rest_framework import serializers
from aag_api.models.score_item import ScoreItem

class ScoreItemSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='scoreitem-detail',
        lookup_field='pk'
    )
    action_display = serializers.CharField(source='get_action_display')
    kind_display = serializers.CharField(source='get_kind_display')
    class Meta:
        model = ScoreItem
        fields = ('url','action','action_display','kind','kind_display','amount','created_datetime','pk')
    
class ScoreItemFullSerializer(serializers.ModelSerializer):

    action_display = serializers.CharField(source='get_action_display')
    kind_display = serializers.CharField(source='get_kind_display')
    class Meta:
        model = ScoreItem
        fields = ('action','kind','amount','created_datetime','id','action_display','kind_display')