from rest_framework import serializers
from mapi.models.customer_user import CustomerUser
from aag_api.models.customer import Customer
from django.contrib.auth.models import User,Group
from mapi.serializers.customer import CustomerSerializer
from django.core.validators  import RegexValidator
import re

class LoginSubmitSerializer(serializers.Serializer):
    regex_data = RegexValidator(r'^[a-zA-Z0-9\-.@_]*$','รูปแบบอีเมลหรือเบอร์เมือถือเท่านั้น')

    username = serializers.CharField(max_length=55,help_text='เบอร์โทร หรือ อีเมล',validators=[regex_data])
    password = serializers.CharField(max_length=55,help_text='รหัสผ่าน')
    client_id = serializers.CharField(max_length=55,help_text='client id')
    client_secret = serializers.CharField(max_length=230,help_text='client secret')

    def validate_username(self, value):
        
        regex_email = re.compile(r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$")
        regex_phone_number = re.compile(r"^0[0-9]{9}$")
        is_valid = False
        if regex_email.match(value):
            is_valid = True
            
        elif regex_phone_number.match(value):
            is_valid = True
        
        if not is_valid:
            raise serializers.ValidationError("ต้องเป็นอีเมลหรือเบอร์โทรเท่านั้น")
        return value

class RevokeTokenSerializer(serializers.Serializer):
    regex_data = RegexValidator(r'^[a-zA-Z0-9\-.@_]*$','รูปแบบอีเมลหรือเบอร์เมือถือเท่านั้น')

    access_token = serializers.CharField(max_length=55,help_text='Access Token')
    client_id = serializers.CharField(max_length=55,help_text='client id')
    client_secret = serializers.CharField(max_length=230,help_text='client secret')
    device_token = serializers.CharField(required=False, max_length=200, allow_blank=True)