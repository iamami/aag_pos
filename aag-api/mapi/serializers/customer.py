from rest_framework import serializers
from aag_api.models.customer import Customer
from django.core.validators  import RegexValidator
import re
from versatileimagefield.serializers import VersatileImageFieldSerializer

class CustomerSerializer(serializers.HyperlinkedModelSerializer):
    image = VersatileImageFieldSerializer(
        sizes=[
            ('full_size', 'url'),
            ('thumbnail', 'thumbnail__300x300'),
            ('crop', 'crop__300x300')
        ]
    )
    class Meta:
        model = Customer
        fields = ('name','code','citizen_id','id','nick_name','address','city','province','postal_code','address2','city2','province2','postal_code2','address3','city3','province3','postal_code3','mobile','birth_date','nationality','email','image','mobile','district','district2','district3','address_primary')
        read_only_fields = ('code',)


class CustomerEditSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Customer
        fields = ('name','address','city','province','postal_code','address2','city2','province2','postal_code2','address3','city3','province3','postal_code3','mobile','birth_date','email','image','district','district2','district3','address_primary')

    def validate(self, data):
            """
            Check that start is before finish.
            """
            fields = ["address","city","province","postal_code","district"]
            is_address1 = False
            is_error1 = False
            error1 = {}
            for f in fields:
                if f in data and data[f] != '':
                    is_address1 = True
                else:
                    is_error1 = True
                    error1[f] = "กรุณากรอก"
            
            if is_address1 and is_error1:
                raise serializers.ValidationError(error1)

            fields2 = ["address2","city2","province2","postal_code2","district2"]
            is_address2 = False
            is_error2 = False
            error2 = {}
            for f in fields2:
                if f in data and data[f] != '':
                    is_address2 = True
                else:
                    is_error2 = True
                    error2[f] = "กรุณากรอก"
            
            if is_address2 and is_error2:
                raise serializers.ValidationError(error2)

            fields3 = ["address3","city3","province3","postal_code3","district3"]
            is_address3 = False
            is_error3 = False
            error3 = {}
            for f in fields3:
                if f in data and data[f] != '':
                    is_address3 = True
                else:
                    is_error3 = True
                    error3[f] = "กรุณากรอก"
            
            if is_address3 and is_error3:
                raise serializers.ValidationError(error3)        

            return data

class CheckingSerializer(serializers.Serializer):
    regex_code = RegexValidator(r'^C[0-9]{6}$',"รูปแบบรหัสลูกค้าไม่ถูกต้อง")
    regex_data = RegexValidator(r'^[a-zA-Z0-9\-.@_]*$','รูปแบบอีเมลหรือเบอร์เมือถือเท่านั้น')

    customer_code = serializers.CharField(max_length=7,validators=[regex_code],help_text='รหัสลูกค้า')
    username = serializers.CharField(max_length=55,help_text='เบอร์โทร หรือ อีเมล',validators=[regex_data])

    def validate_username(self, value):
        
        regex_email = re.compile(r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$")
        regex_mobile_number = re.compile(r"^0[0-9]{9}$")
        is_valid = False
        if regex_email.match(value):
            is_valid = True
            
        elif regex_mobile_number.match(value):
            is_valid = True
        
        if not is_valid:
            raise serializers.ValidationError("ต้องเป็นอีเมลหรือเบอร์โทรเท่านั้น")
        return value