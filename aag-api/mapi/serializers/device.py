from rest_framework import serializers
from mapi.models.device import Device
from mapi.models.customer_user import CustomerUser


class DeviceSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Device
        fields = ('token', 'uuid', 'kind', "name")

    def create(self, validated_data):

        d = Device()
        d.token = validated_data['token']
        d.uuid = validated_data['uuid']
        d.kind = validated_data['kind']
        d.name = validated_data['name']
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            d.customer_user = CustomerUser.objects.get(user=request.user.pk)
        d.save()
        return d
