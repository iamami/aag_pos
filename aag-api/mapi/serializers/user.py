from rest_framework import serializers
from mapi.models.customer_user import CustomerUser
from aag_api.models.customer import Customer
from django.contrib.auth.models import User,Group
from mapi.serializers.customer import CustomerSerializer
from django.core.validators  import RegexValidator
import re

class UserSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = User
        fields = ('username','password')
        extra_kwargs = {'password': {'write_only': True}}

class CustomerUserSettingSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerUser
        fields = ('is_alert_gold_price','is_alert_promotion','is_alert_newreward')

class CustomerUserFullSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    customer = CustomerSerializer()
    class Meta:
        model = CustomerUser
        fields = ('user','customer','is_alert_gold_price','is_alert_promotion')

class CustomerUserSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    customer = CustomerSerializer()
    class Meta:
        model = CustomerUser
        fields = ('user','customer')

class UserRegisterSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    secret = serializers.CharField(required=True, max_length=32)
    class Meta:
        model = CustomerUser
        fields = ('customer','user',)

    def validate_user(self, value):

        return value

    def create(self, validated_data):

        secret = validated_data['secret']
        o = validated_data['customer']
        
        if o.secret != secret:
            raise serializers.ValidationError({'secret': "Is not valid."})

        username = validated_data['user']['username']
        group = Group.objects.get(name='customer')
        
        try:
            user = User.objects.get(username=validated_data['user']['username'])
        except User.DoesNotExist:
            # new Create User
            user = User.objects.create(
                username=validated_data['user']['username'],
                is_superuser=False,
                is_active=True
            )
            user.set_password(validated_data['user']['password'])
            user.save()

        group.user_set.add(user)
        user_profile = CustomerUser.objects.create(
            user=user,
            customer=validated_data['customer'],
            uuid=validated_data['uuid']
        )
        user_profile.createCodeSecurity()
        o.createCodeSecurity()

        return user_profile

class RegisterSubmitSerializer(serializers.Serializer):
    regex_code = RegexValidator(r'^C[0-9]{6}$',"รูปแบบรหัสลูกค้าไม่ถูกต้อง")
    regex_data = RegexValidator(r'^[a-zA-Z0-9\-.@_]*$','รูปแบบอีเมลหรือเบอร์เมือถือเท่านั้น')

    customer_code = serializers.CharField(max_length=7,validators=[regex_code],help_text='รหัสลูกค้า')
    username = serializers.CharField(max_length=55,help_text='เบอร์โทร หรือ อีเมล',validators=[regex_data])
    password = serializers.CharField(max_length=55,help_text='รหัสผ่าน')
    client_id = serializers.CharField(max_length=55,help_text='client id')
    client_secret = serializers.CharField(max_length=230,help_text='client secret')

    def validate_username(self, value):
        
        regex_email = re.compile(r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$")
        regex_phone_number = re.compile(r"^0[0-9]{9}$")
        is_valid = False
        if regex_email.match(value):
            is_valid = True
            
        elif regex_phone_number.match(value):
            is_valid = True
        
        if not is_valid:
            raise serializers.ValidationError("ต้องเป็นอีเมลหรือเบอร์โทรเท่านั้น")
        return value


class NewPasswordSerializer(serializers.Serializer):
    regex_data = RegexValidator(r'^[a-zA-Z0-9\-.@_]*$','รูปแบบอีเมลหรือเบอร์เมือถือเท่านั้น')
    username = serializers.CharField(max_length=55,help_text='เบอร์โทร หรือ อีเมล',validators=[regex_data])
    newpassword = serializers.CharField(min_length=6,max_length=55,help_text='รหัสผ่าน')
    client_id = serializers.CharField(max_length=55,help_text='client id')
    client_secret = serializers.CharField(max_length=230,help_text='client secret')

    def validate_username(self, value):
        
        regex_email = re.compile(r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$")
        regex_phone_number = re.compile(r"^0[0-9]{9}$")
        is_valid = False
        if regex_email.match(value):
            is_valid = True
            
        elif regex_phone_number.match(value):
            is_valid = True
        
        if not is_valid:
            raise serializers.ValidationError("ต้องเป็นอีเมลหรือเบอร์โทรเท่านั้น")
        return value


class PasswordCheckingSerializer(serializers.Serializer):
    password = serializers.CharField(min_length=6,max_length=55,help_text='รหัสผ่าน')



class ChangePasswordSerializer(serializers.Serializer):
    regex_data = RegexValidator(r'^[a-zA-Z0-9\-.@_]*$','รูปแบบอีเมลหรือเบอร์เมือถือเท่านั้น')
    old_password = serializers.CharField(min_length=6,max_length=55,help_text='รหัสผ่านเก่า')
    password = serializers.CharField(min_length=6,max_length=55,help_text='รหัสผ่าน')