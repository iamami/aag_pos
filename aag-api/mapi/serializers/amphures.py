from rest_framework import serializers
from mapi.models.amphures import Amphures

class AmphuresSerializer(serializers.ModelSerializer):
    districts = serializers.StringRelatedField(many=True)

    class Meta:
        model = Amphures
        fields = ('name', 'districts')