from rest_framework import serializers
from aag_api.models.reward import Reward
from aag_api.serializers import RewardSerializer,BranchSerializer,BranchShortSerializer,RewardShortSerializer
from versatileimagefield.serializers import VersatileImageFieldSerializer

class RewardSerializer(serializers.ModelSerializer):
    is_enabled_display = serializers.CharField(source='get_is_enabled_display',)
    image = VersatileImageFieldSerializer(
        sizes=[
            ('full_size', 'url'),
            ('thumbnail', 'thumbnail__300x300'),
            ('crop', 'crop__300x300'),
            ('small_square_crop', 'crop__100x100')
        ]
    )
    class Meta:
        model = Reward
        fields = ('code','name','image','score','created_datetime','detail','id','is_enabled_display','is_enabled')
        read_only_fields = ('code','is_enabled_display',)
        
class RewardListSerializer(serializers.ModelSerializer):
    image = VersatileImageFieldSerializer(
        sizes=[
            ('full_size', 'url'),
            ('crop', 'crop__300x300')
        ]
    )
    class Meta:
        model = Reward
        fields = ('code','name','image','score','created_datetime','id',)
        read_only_fields = ('code',)


class StockRewardListSerializer(serializers.ModelSerializer):
    image = VersatileImageFieldSerializer(
        sizes=[
            ('full_size', 'url'),
            ('crop', 'crop__300x300')
        ]
    )
    amount = serializers.CharField()
    class Meta:
        model = Reward
        fields = ('code','name','image','score','created_datetime','id','amount')
        read_only_fields = ('code',)

class StockRewardSerializer(serializers.ModelSerializer):
    is_enabled_display = serializers.CharField(source='get_is_enabled_display',)
    image = VersatileImageFieldSerializer(
        sizes=[
            ('full_size', 'url'),
            ('thumbnail', 'thumbnail__300x300'),
            ('crop', 'crop__300x300'),
            ('small_square_crop', 'crop__100x100')
        ]
    )
    amount = serializers.CharField()
    class Meta:
        model = Reward
        fields = ('code','name','image','score','created_datetime','detail','id','is_enabled_display','is_enabled','amount')
        read_only_fields = ('code','is_enabled_display',)