from rest_framework import serializers
from mapi.serializers.customer import CustomerSerializer
from mapi.models.otp import OneTimePassword
from django.core.validators  import RegexValidator
import re

regex_code = RegexValidator(r'^C[0-9]{6}$',"รูปแบบรหัสลูกค้าไม่ถูกต้อง")
regex_data = RegexValidator(r'^[a-zA-Z0-9\-.@_]*$','รูปแบบอีเมลหรือเบอร์เมือถือเท่านั้น')

class OneTimePasswordViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = OneTimePassword
        fields = ('email','phone','kind','reference','created_datetime','id','is_confirm')

class OneTimePasswordCustomerViewSerializer(serializers.ModelSerializer):

    customer = CustomerSerializer()
    class Meta:
        model = OneTimePassword
        fields = ('customer',)

class OneTimePasswordConfirmSerializer(serializers.ModelSerializer):

    class Meta:
        model = OneTimePassword
        fields = ('secret','is_confirm','id')
        extra_kwargs = {'secret': {'write_only': True}}
        read_only_fields = ('is_confirm','id',)

class OneTimePasswordCreateSerializer(serializers.Serializer):

    customer_code = serializers.CharField(max_length=7,validators=[regex_code],help_text='รหัสลูกค้า')
    username = serializers.CharField(max_length=55,help_text='เบอร์โทร หรือ อีเมล',validators=[regex_data])

    def validate_username(self, value):
        
        regex_email = re.compile(r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$")
        regex_phone_number = re.compile(r"^0[0-9]{9}$")
        is_valid = False
        if regex_email.match(value):
            is_valid = True
            
        elif regex_phone_number.match(value):
            is_valid = True
        
        if not is_valid:
            raise serializers.ValidationError("ต้องเป็นอีเมลหรือเบอร์โทรเท่านั้น")
        return value

class OneTimePasswordEditProfileCreateSerializer(serializers.Serializer):

    username = serializers.CharField(max_length=55,help_text='เบอร์โทร หรือ อีเมล',validators=[regex_data])
    def validate_username(self, value):
        
        regex_email = re.compile(r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$")
        regex_phone_number = re.compile(r"^0[0-9]{9}$")
        is_valid = False
        if regex_email.match(value):
            is_valid = True
            
        elif regex_phone_number.match(value):
            is_valid = True
        
        if not is_valid:
            raise serializers.ValidationError("ต้องเป็นอีเมลหรือเบอร์โทรเท่านั้น")
        return value

class OneTimePasswordForGetPasswordCreateSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=55,help_text='เบอร์โทร หรือ อีเมล',validators=[regex_data])
    def validate_username(self, value):
        
        regex_email = re.compile(r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$")
        regex_phone_number = re.compile(r"^0[0-9]{9}$")
        is_valid = False
        if regex_email.match(value):
            is_valid = True
            
        elif regex_phone_number.match(value):
            is_valid = True
        
        if not is_valid:
            raise serializers.ValidationError("ต้องเป็นอีเมลหรือเบอร์โทรเท่านั้น")
        return value