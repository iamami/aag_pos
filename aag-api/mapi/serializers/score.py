from rest_framework import serializers
from aag_api.models.score import Score

class ScoreSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Score
        fields = ('sell','saving','interest','total','created_datetime')