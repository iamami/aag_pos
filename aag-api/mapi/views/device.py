from mapi.serializers.device import DeviceSerializer
from rest_framework import permissions
from mapi.models.device import Device
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView
from django.shortcuts import get_object_or_404
from rest_framework import generics

class DeviceView(generics.CreateAPIView):

    serializer_class = DeviceSerializer
    queryset = Device.objects.all().order_by('-id')
    permission_classes = (permissions.IsAuthenticated,)

    def perform_create(self, serializer):

        Device.objects.all().filter(token=serializer.validated_data['token']).delete()
        serializer.save()