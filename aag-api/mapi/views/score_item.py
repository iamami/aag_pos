from mapi.serializers.score_item import ScoreItemSerializer,ScoreItemFullSerializer
from rest_framework import permissions
from aag_api.models.score import Score
from aag_api.models.score_item import ScoreItem
from mapi.pagination import LargeResultsSetPagination
from rest_framework import generics
from django.shortcuts import get_object_or_404
from mapi.models.customer_user import CustomerUser

class scoreItemList(generics.ListAPIView):

    def get_queryset(self):
        user = self.request.user
        customer = get_object_or_404(CustomerUser, user=user.pk)
        o = get_object_or_404(Score, customer=customer.customer.pk)
        queryset = ScoreItem.objects.all().order_by('-id').filter(score=o.pk)

        kind = self.request.query_params.get('kind', None)
        if kind is not None:
            queryset = queryset.filter(kind=kind)

        action = self.request.query_params.get('action', None)
        if action is not None:
            queryset = queryset.filter(action=action)
        return queryset
    
    pagination_class = LargeResultsSetPagination
    serializer_class = ScoreItemSerializer
    permission_classes = (permissions.IsAuthenticated,)

class scoreItemDetail(generics.RetrieveAPIView):
    def get_queryset(self):
        user = self.request.user
        customer = get_object_or_404(CustomerUser, user=user.pk)
        o = get_object_or_404(Score, customer=customer.customer.pk)
        queryset = ScoreItem.objects.all().order_by('id').filter(score=o.pk)        

        return queryset
    serializer_class = ScoreItemFullSerializer

    permission_classes = (permissions.IsAuthenticated,)