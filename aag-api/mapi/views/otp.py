from mapi.serializers.user import *
from mapi.serializers.otp import *
from rest_framework import permissions
from mapi.models.customer_user import CustomerUser
from aag_api.models.customer import Customer
from mapi.models.otp import OneTimePassword
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework import generics
from rest_framework.views import APIView
from django.contrib.auth.models import User,Group

from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope, TokenHasScope
from oauth2_provider.views.generic import ProtectedResourceView
from django.http import HttpResponse
from rest_framework import status
from rest_framework import serializers
from django.utils import timezone
from django.db.models import Q

# Register and send otp
class OneTimePasswordCreateView(APIView):
    serializer_class = OneTimePasswordCreateSerializer
    def post(self, request,format=None):
        serializer = OneTimePasswordCreateSerializer(data=request.data)

        if serializer.is_valid():            
            # Check Customer
            customer = get_object_or_404(Customer, code=serializer.data['customer_code'])

            # Formate Username and Phone number
            regex_email = re.compile(r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$")
            regex_phone_number = re.compile(r"^0[0-9]{9}$")
        
            isEmail = False
            # Check username is email
            if regex_email.match(serializer.data['username']):
                isEmail = True
                if str(customer.email) != str(serializer.data['username']):
                    msg_error = {'username' : 'ไม่สามารถใช้อีเมลนี้ในระบบ เนื่องจากไม่ตรงในข้อมูลลูกค้า'}
                    return Response(msg_error,status.HTTP_403_FORBIDDEN)
            
            # Check username is phone
            elif regex_phone_number.match(serializer.data['username']):
                isEmail = False

                if str(customer.mobile) != str(serializer.data['username']):
                    msg_error = {'username' : 'ไม่สามารถใช้เบอร์โทรนี้ในระบบ เนื่องจากไม่ตรงในข้อมูลลูกค้า'}
                    return Response(msg_error,status.HTTP_403_FORBIDDEN)
            
            # Check time to resend otp
            try:
                otp = OneTimePassword.objects.get(customer=customer.pk)
                diff = timezone.now() - otp.created_datetime 
                if diff.seconds < 30:
                    msg_error = {'error' : 'ไม่สามารถขอ OTP อีกครั้งได้กรุณารอ %s วินาที' % (30-diff.seconds)}
                    return Response(msg_error,status.HTTP_400_BAD_REQUEST)
            except OneTimePassword.DoesNotExist:
                otp = None
                
              
            # Create OTP
            if isEmail:
                # create and send email
                data = OneTimePassword.sendOtpEmail(customer,serializer.data['username'])
                serializer = OneTimePasswordViewSerializer(data)
            else:
                # create and send email
                data = OneTimePassword.sendOtpPhone(customer,serializer.data['username'])
                serializer = OneTimePasswordViewSerializer(data)
                        
            # Is valid
            return Response(serializer.data,status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status.HTTP_403_FORBIDDEN)

class OneTimePasswordEditProfileCreateView(APIView):

    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = OneTimePasswordEditProfileCreateSerializer
    def post(self, request,format=None):
        serializer = OneTimePasswordEditProfileCreateSerializer(data=request.data)

        if serializer.is_valid():            
            # Check Customer
            customer_user = get_object_or_404(CustomerUser, user=request.user.pk)
            customer = customer_user.customer

            # Formate Username and Phone number
            regex_email = re.compile(r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$")
            regex_phone_number = re.compile(r"^0[0-9]{9}$")
        
            isEmail = False
            # Check username is email
            if regex_email.match(serializer.data['username']):
                isEmail = True
                try:
                    Customer.objects.get(email=serializer.data['username'])
                    msg_error = {'error' : 'อีเมลนี้ถูกใช้งานแล้ว'}
                    return Response(msg_error,status.HTTP_400_BAD_REQUEST)
                except Customer.DoesNotExist:
                    pass
            
            # Check username is phone
            elif regex_phone_number.match(serializer.data['username']):
                isEmail = False
                try:
                    Customer.objects.get(mobile=serializer.data['username'])
                    msg_error = {'error' : 'เบอร์โทรนี้ถูกใช้งานแล้ว'}
                    return Response(msg_error,status.HTTP_400_BAD_REQUEST)
                except Customer.DoesNotExist:
                    pass
            
            # Check time to resend otp
            try:
                otp = OneTimePassword.objects.get(customer=customer.pk)
                diff = timezone.now() - otp.created_datetime 
                if diff.seconds < 30:
                    msg_error = {'error' : 'ไม่สามารถขอ OTP อีกครั้งได้กรุณารอ %s วินาที' % (30-diff.seconds)}
                    return Response(msg_error,status.HTTP_400_BAD_REQUEST)
            except OneTimePassword.DoesNotExist:
                otp = None
                
              
            # Create OTP
            if isEmail:
                # create and send email
                data = OneTimePassword.sendOtpEmail(customer,serializer.data['username'])
                serializer = OneTimePasswordViewSerializer(data)
            else:
                # create and send email
                data = OneTimePassword.sendOtpPhone(customer,serializer.data['username'])
                serializer = OneTimePasswordViewSerializer(data)
                        
            # Is valid
            return Response(serializer.data,status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status.HTTP_403_FORBIDDEN)


class OneTimePasswordForGetPasswordCreateView(APIView):
    serializer_class = OneTimePasswordForGetPasswordCreateSerializer
    def post(self, request,format=None):
        serializer = OneTimePasswordForGetPasswordCreateSerializer(data=request.data)

        if serializer.is_valid():            
            username = serializer.data['username']
            # find username 
            r = CustomerUser.objects.all().filter(Q(customer__email=username) | Q(customer__mobile=username))

            if r.count() != 1:
                msg_error = {'username' : 'ไม่พบผู้ใช้งานนี้ กรุณาลงทะเบียนหรือติดต่อหน้าร้าน'}
                return Response(msg_error,status.HTTP_400_BAD_REQUEST)  

            c_user = r[0]
            customer = c_user.customer
            
            # Check time to resend otp
            try:
                otp = OneTimePassword.objects.get(customer=customer.pk)
                diff = timezone.now() - otp.created_datetime 
                if diff.seconds < 30:
                    msg_error = {'error' : 'ไม่สามารถขอ OTP อีกครั้งได้กรุณารอ %s วินาที' % (30-diff.seconds)}
                    return Response(msg_error,status.HTTP_400_BAD_REQUEST)
            except OneTimePassword.DoesNotExist:
                otp = None
            
            # Formate Username and Phone number
            regex_email = re.compile(r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$")
        
            isEmail = False
            # Check username is email
            if regex_email.match(serializer.data['username']):
                isEmail = True
              
            # Create OTP
            if isEmail:
                # create and send email
                data = OneTimePassword.sendOtpEmail(customer,serializer.data['username'])
                serializer = OneTimePasswordViewSerializer(data)
            else:
                # create and send email
                data = OneTimePassword.sendOtpPhone(customer,serializer.data['username'])
                serializer = OneTimePasswordViewSerializer(data)
                        
            # Is valid
            return Response(serializer.data,status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status.HTTP_403_FORBIDDEN)

class OneTimePasswordDetail(generics.UpdateAPIView):

    queryset = OneTimePassword.objects.all()
    serializer_class = OneTimePasswordConfirmSerializer
    def perform_update(self, serializer):
        
        otp = self.get_object()
        secret = serializer.validated_data['secret']

        if otp.secret != secret :

            # update count for incorrect
            otp.count = otp.count + 1
            otp.save()

            if otp.count > 3 :
                raise serializers.ValidationError({'secret': 'คุณใส่รหัส OTP ผิดเกิน 3 ครั้งกรุณาขอ OTP อีกครั้ง'})

            raise serializers.ValidationError({'secret': 'รหัส OTP ไม่ถูกต้อง'})

        instance = serializer.save(is_confirm=True)

class OneTimePasswordCustomerDetail(generics.RetrieveAPIView):

    serializer_class = CustomerSerializer
    def get_object(self):
        secret = self.kwargs['secret']
        pk = self.kwargs['pk']
        otp = get_object_or_404(OneTimePassword, pk=pk,secret=secret )
        return otp.customer