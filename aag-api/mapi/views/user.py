from mapi.serializers.user import *
from rest_framework import permissions
from mapi.models.customer_user import CustomerUser
from mapi.models.otp import *
from aag_api.models.customer import Customer
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework import generics
from rest_framework.views import APIView
from django.contrib.auth.models import User,Group
from mapi.models.auth_token import AuthToken
from django.db.models import Q
from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope, TokenHasScope
from oauth2_provider.views.generic import ProtectedResourceView
from django.http import HttpResponse
from rest_framework import status
import requests
from oauth2_provider.models import Application
from django_hosts.resolvers import reverse
import json


# Register and send otp
class RegisterView(APIView):
    serializer_class = RegisterSubmitSerializer
    def post(self, request,format=None):
        serializer = RegisterSubmitSerializer(data=request.data)

        if serializer.is_valid():

            # Application
            try:
                application = Application.objects.get(client_id=serializer.data['client_id'],client_secret=serializer.data['client_secret'])
            except Application.DoesNotExist:
                msg_error = {'error' : 'invalid_client'}
                return Response(msg_error,status.HTTP_403_FORBIDDEN)

            # Check Customer
            customer = get_object_or_404(Customer, code=serializer.data['customer_code'])

            # Check Registered
            if CustomerUser.objects.all().filter(customer=customer.pk).count() > 0:
                msg_error = {'customer_code' : 'รหัสลูกค้า %s ลงทะเบียนผู้ใช้งานแล้ว' % (serializer.data['customer_code'])}
                return Response(msg_error,status.HTTP_403_FORBIDDEN)

            # Formate Username and Phone number
            regex_email = re.compile(r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$")
            regex_phone_number = re.compile(r"^0[0-9]{9}$")
        
            isEmail = False

            # Check username is email
            if regex_email.match(serializer.data['username']):
                isEmail = True
                if str(customer.email) != str(serializer.data['username']):
                    msg_error = {'username' : 'ไม่สามารถใช้อีเมลนี้ในระบบ'}
                    return Response(msg_error,status.HTTP_403_FORBIDDEN)
                
                # Check Username Registered
                if User.objects.all().filter(username=serializer.data['username']).count() > 0:
                    msg_error = {'username' : 'อีเมล %s ลงทะเบียนผู้ใช้งานแล้ว' % (serializer.data['username'])}
                    return Response(msg_error,status.HTTP_403_FORBIDDEN)
            
            # Check username is phone
            elif regex_phone_number.match(serializer.data['username']):
                isEmail = False

                if str(customer.mobile) != str(serializer.data['username']):
                    msg_error = {'username' : 'ไม่สามารถใช้เบอร์โทรนี้ในระบบ'}
                    return Response(msg_error,status.HTTP_403_FORBIDDEN)
                
                # Check Username Registered
                if User.objects.all().filter(username=serializer.data['username']).count() > 0:
                    msg_error = {'username' : 'เบอร์มือถือ %s ลงทะเบียนผู้ใช้งานแล้ว' % (serializer.data['username'])}
                    return Response(msg_error,status.HTTP_403_FORBIDDEN)

            # check verify otp
            otp_list = OneTimePassword.objects.all().filter(customer=customer.pk,is_confirm=True)
            if isEmail:
                otp_list = otp_list.filter(email=serializer.data['username'],kind='EM')
            else:
                otp_list = otp_list.filter(phone=serializer.data['username'],kind='PH')
            
            if otp_list.count()==0:
                msg_error = {'username' : 'ไม่สามารถลงทะเบียนได้ เนื้องจาก %s ไม่ได้รับการยืนยัน' % (serializer.data['username'])}
                return Response(msg_error,status.HTTP_403_FORBIDDEN)            
            else:
                # group customer
                group = Group.objects.get(name='customer')

                # # create User
                user = User.objects.create(
                    username=serializer.data['customer_code'],
                    is_superuser=False,
                    is_active=True
                )
                user.set_password(serializer.data['password'])
                user.save()

                # # set user group
                group.user_set.add(user)

                # # create customer_user
                customer_user = CustomerUser.objects.create(
                    user=user,
                    customer=customer
                )
                customer_user.createCodeSecurity()

                token = AuthToken.create_token(user,application)

                # clear otp
                otp_list.delete()
            # Is valid
            return Response({
                "access_token": token.access_token.token,
                "token_type": "Bearer",
                "expires": token.access_token.expires,
                "refresh_token": token.token
            },status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status.HTTP_400_BAD_REQUEST)
        
# Register and send otp
class NewPasswordView(APIView):
    serializer_class = NewPasswordSerializer
    def post(self, request,format=None):
        serializer = NewPasswordSerializer(data=request.data)
        if serializer.is_valid():
            username = serializer.data['username']
        
            # find username 
            r = CustomerUser.objects.all().filter(Q(customer__email=username) | Q(customer__mobile=username))

            if r.count() != 1:
                msg_error = {'username' : 'ไม่พบผู้ใช้งานนี้ กรุณาลงทะเบียนหรือติดต่อหน้าร้าน'}
                return Response(msg_error,status.HTTP_400_BAD_REQUEST)  

            c_user = r[0]
            customer = c_user.customer

            # check OTP
            # check verify otp
            otp_list = OneTimePassword.objects.all().filter(customer=customer.pk,is_confirm=True)

            if otp_list.count()==0:
                msg_error = {'username' : 'ไม่สามารถแก้ไขรหัสผ่านได้เนื่องจากยังไม่มีการยืนยัน OTP'}
                return Response(msg_error,status.HTTP_403_FORBIDDEN)            
            else:

                # Application
                try:
                    application = Application.objects.get(client_id=serializer.data['client_id'],client_secret=serializer.data['client_secret'])
                except Application.DoesNotExist:
                    msg_error = {'error' : 'invalid_client'}
                    return Response(msg_error,status.HTTP_403_FORBIDDEN)
                
                # update passowrd
                c_user.user.set_password(serializer.data['newpassword'])
                c_user.user.save()

                # get access token
                token = AuthToken.create_token(c_user.user,application)

                # clear otp
                otp_list.delete()

                # Is valid
                return Response({
                    "access_token": token.access_token.token,
                    "token_type": "Bearer",
                    "expires": token.access_token.expires,
                    "refresh_token": token.token
                },status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status.HTTP_400_BAD_REQUEST)
        
# profile
class UserView(generics.RetrieveUpdateAPIView):
    serializer_class = CustomerSerializer
    def get(self, request,format=None):

        customer = get_object_or_404(CustomerUser, user__pk=request.user.pk)
        serializer = CustomerSerializer(customer.customer)
        return Response(serializer.data)

    queryset = Customer.objects.all()

    def get_object(self):
        request = self.request
        o = get_object_or_404(CustomerUser, user__pk=request.user.pk)        
        obj = o.customer
        self.check_object_permissions(self.request, obj)
        return obj
    permission_classes = (permissions.IsAuthenticated,TokenHasReadWriteScope)

# setting
class UserSettingView(generics.RetrieveUpdateAPIView):

    serializer_class = CustomerUserSettingSerializer
    queryset = CustomerUser.objects.all()
    def get(self, request,format=None):

        customer = get_object_or_404(CustomerUser, user__pk=request.user.pk)
        serializer = CustomerUserSettingSerializer(customer)
        return Response(serializer.data)

    def get_object(self):
        request = self.request        
        obj = get_object_or_404(CustomerUser, user__pk=request.user.pk)
        self.check_object_permissions(self.request, obj)
        return obj
    permission_classes = (permissions.IsAuthenticated,)

# password checing
class PasswordCheckingView(APIView):
    serializer_class = PasswordCheckingSerializer
    permission_classes = (permissions.IsAuthenticated,)
    def post(self, request,format=None):
        serializer = PasswordCheckingSerializer(data=request.data)

        if serializer.is_valid():
            user = request.user

            # Check Password
            if not user.check_password(serializer.data['password']):
                msg_error = {'password' : 'รหัสผ่านไม่ถูกต้อง'}
                return Response(msg_error,status.HTTP_403_FORBIDDEN)

            # Is valid
            return Response(serializer.data,status.HTTP_200_OK)
        else:
            return Response(serializer.errors,status.HTTP_403_FORBIDDEN)
       
# Register and send otp
class ChangePasswordView(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = ChangePasswordSerializer
    def post(self, request,format=None):
        serializer = ChangePasswordSerializer(data=request.data)
        if serializer.is_valid():
            
            user = request.user

            # Check Password
            if not user.check_password(serializer.data['old_password']):
                msg_error = {'old_password' : 'รหัสผ่านไม่ถูกต้อง'}
                return Response(msg_error,status.HTTP_403_FORBIDDEN)

            # Is valid
            user.set_password(serializer.data['password'])
            user.save()
            return Response({},status.HTTP_200_OK)
        else:
            return Response(serializer.errors,status.HTTP_400_BAD_REQUEST)
   