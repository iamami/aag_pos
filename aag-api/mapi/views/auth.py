from mapi.serializers.auth import *
from rest_framework import permissions
from mapi.models.customer_user import CustomerUser
from mapi.models.otp import *
from aag_api.models.customer import Customer
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework import generics
from rest_framework.views import APIView
from django.contrib.auth.models import User,Group

from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope, TokenHasScope
from oauth2_provider.views.generic import ProtectedResourceView
from django.http import HttpResponse
from rest_framework import status
import requests
from oauth2_provider.models import Application
from django_hosts.resolvers import reverse
import json
from django.contrib.auth import authenticate
from mapi.models.auth_token import AuthToken
from django.db.models import Q
from django.conf import settings
from django.utils import timezone
import datetime

class LoginView(APIView):
    serializer_class = LoginSubmitSerializer
    def post(self, request,format=None):
        serializer = LoginSubmitSerializer(data=request.data)

        if serializer.is_valid():
            username = serializer.data['username']
            password = serializer.data['password']

            # Application
            try:
                application = Application.objects.get(client_id=serializer.data['client_id'],client_secret=serializer.data['client_secret'])
            except Application.DoesNotExist:
                msg_error = {'error' : 'invalid_client'}
                return Response(msg_error,status.HTTP_403_FORBIDDEN)

            # find username 
            r = CustomerUser.objects.all().filter(Q(customer__email=username) | Q(customer__mobile=username))

            if r.count() != 1:
                msg_error = {'username' : 'ไม่พบผู้ใช้งานนี้ กรุณาลงทะเบียนหรือติดต่อหน้าร้าน'}
                return Response(msg_error,status.HTTP_400_BAD_REQUEST)  

            c_user = r[0]

            user = authenticate(request, username=c_user.user.username, password=password)

            # login not success
            if user is None :

                c_user.login_fail = c_user.login_fail + 1
                c_user.save()

                if c_user.login_fail > 3:
                    msg_error = {'password' : 'คุณใส่รหัสผ่านผิดเกิน 3 ครั้ง กรุณากดลืมรหัสผ่าน เพือขอรหัสผ่านใหม่อีกครั้ง'}
                    return Response(msg_error,status.HTTP_403_FORBIDDEN)

                msg_error = {'password' : 'รหัสผ่านไม่ถูกต้อง'}
                return Response(msg_error,status.HTTP_400_BAD_REQUEST)
            else:
                c_user.login_fail = 0
                c_user.save()
            
            if user.groups.filter(name='customer').exists() == False:
                msg_error = {'username' : 'ไม่พบผู้ใช้งานนี้'}
                return Response(msg_error,status.HTTP_403_FORBIDDEN)

            auth = AuthToken.create_token(user,application)

            today = datetime.datetime.now() #timezone.now()
            regex_email = re.compile(r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$")
            regex_phone_number = re.compile(r"^0[0-9]{9}$")

            mylist = []
            mylist.append(today)
            date = '%s/%s' % (today.strftime('%d/%m'),int(today.year)+543)
            time = today.strftime('%H:%M:%S %Z')

            # send Email
            if regex_email.match(username):
                context = {
                    'customer': c_user.customer,
                    'time': time,
                    'date': date
                    }
                html_message = loader.render_to_string("mapi/mail/login_alert.html", context)
                EMAIL_FORM = 'AA gold <noreply@aagold-th.com>'
                mail.send_mail('แจ้งเตือนการเข้าสู่ระบบ AA gold application', "", settings.EMAIL_FORM, [username], html_message=html_message)

            # send SMS
            elif regex_phone_number.match(username):
                        # send MSM
                to = username
                msg = "แจ้งเตือนการเข้าสู่ระบบ AA gold application ของคุณ %s เมื่อวันที่ %s เวลา:  %s  / จาก ห้างทอง เอ เอ เยาวราช" % (c_user.customer.name,date,time)
                r = requests.get("http://www.thsms.com/api/rest?method=send&username=%s&password=%s&from=%s&to=%s&message=%s"%(settings.SMS_USER,settings.SMS_PASS,settings.SMS_FROM,to,msg))

            # Is valid
            return Response({
                "access_token": auth.access_token.token,
                "token_type": "Bearer",
                "expires": auth.access_token.expires,
                "refresh_token": auth.token
            },status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status.HTTP_400_BAD_REQUEST)