from mapi.serializers.score import ScoreSerializer
from rest_framework import permissions
from aag_api.models.score import Score
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView
from mapi.models.customer_user import CustomerUser
import json

class scoreView(APIView):

    def get(self, request, format=None):

        customer = get_object_or_404(CustomerUser, user=request.user.pk)
        o = get_object_or_404(Score, customer=customer.customer.pk)
        serializer = ScoreSerializer(o)
        return Response(serializer.data)
    permission_classes = (permissions.IsAuthenticated,)


class ScoreConditionView(APIView):

    permission_classes = (permissions.IsAuthenticated,)
    def get(self, request, format=None):
        json_data = open('./static/json/score_conditions.json')
        return Response(json.load(json_data))
