from rest_framework.views import APIView
from rest_framework.response import Response
import json

class RegulationsView(APIView):

    def get(self, request, format=None):

        json_data = open('./static/json/regulations.json') 
        return Response(json.load(json_data))
