from mapi.serializers.auth import *
from rest_framework import permissions
from mapi.models.customer_user import CustomerUser
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView
from django.contrib.auth.models import User, Group
from django.http import HttpResponse
from rest_framework import status
import requests
from oauth2_provider.models import Application
from django_hosts.resolvers import reverse
import json
from django.contrib.auth import authenticate
from mapi.models.auth_token import AuthToken
from django.db.models import Q
from mapi.models.device import Device
from mapi.serializers.notification import *
import argparse
from django.conf import settings
from pyfcm import FCMNotification
from aag_api.pagination import LargeResultsSetPagination
from rest_framework import generics
import json

class NotificationList(generics.ListAPIView):
    pagination_class = LargeResultsSetPagination
    serializer_class = NotificationSerializer
    def get_queryset(self):

        customer_user = CustomerUser.objects.get(user=self.request.user.pk)
        queryset = Notification.objects.all().filter(customer_user=customer_user.pk).order_by('-id')

        return queryset
    permission_classes = (permissions.IsAuthenticated,)

class NotificationDetail(generics.UpdateAPIView):
    serializer_class = NotificationReadSerializer
    def get_queryset(self):
        customer_user = CustomerUser.objects.get(user=self.request.user.pk)
        queryset = Notification.objects.all().filter(customer_user=customer_user.pk).order_by('-id')
        return queryset
        
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

class SenderView(APIView):
    serializer_class = NotificationSenderSerializer

    def post(self, request, format=None):
        serializer = NotificationSenderSerializer(data=request.data)

        if serializer.is_valid():
            # Application
            try:
                application = Application.objects.get(
                    client_id=serializer.data['client_id'], client_secret=serializer.data['client_secret'])
            except Application.DoesNotExist:
                msg_error = {'error': 'invalid_client'}
                return Response(msg_error, status.HTTP_403_FORBIDDEN)

            if application.name != 'NOTIFICATION':
                msg_error = {'error': 'invalid_client'}
                return Response(msg_error, status.HTTP_403_FORBIDDEN)
            # Send notification to

            device_list = Device.objects.all()
            size = device_list.count()


            message_title = serializer.data['title']
            message_body = serializer.data['message']
 
            data_message = json.loads(serializer.data['data'])

            notification_extra_kwargs = {
                'android_channel_id': 2
            }
            extra_kwargs = {
                'mutable_content': True
            }



            if serializer.data['kind'] in (4, 5):

                msg_error = {'user': 'ต้องระบุผู้รับ'}
                return Response(msg_error, status.HTTP_403_FORBIDDEN)

            elif serializer.data['kind'] == 1:  # ราคาทองเปลี่ยนแปลง
                device_list = device_list.filter(
                    customer_user__is_alert_gold_price=True)
                result = Device.sendNotiGoldPrice(device_list,message_title,message_body,data_message)

            elif serializer.data['kind'] == 2:  # มีโปรโมชั่นมาใหม่
                device_list = device_list.filter(
                    customer_user__is_alert_promotion=True)
                result = Device.sendNotiPromotion(device_list,message_title,message_body,data_message)

            elif serializer.data['kind'] == 3:  # มีของรางวัลมาใหม่
                device_list = device_list.filter(
                    customer_user__is_alert_newreward=True)
                result = Device.sendNotiRewardNew(device_list,message_title,message_body,data_message)


            
            return Response({'result': result, 'notification': serializer.data, 'size': size, 'send': device_list.count()}, status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)


class CounterView(APIView):

    permission_classes = (permissions.IsAuthenticated,)
    def get(self, request, format=None):
        customer_user = CustomerUser.objects.get(user=request.user.pk)

        return Response({
            'count':  Notification.objects.all().filter(customer_user=customer_user.pk,status='unred').count()
        })

