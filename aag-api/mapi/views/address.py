from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from mapi.serializers.amphures import *

from mapi.models.zipcodes import *
from mapi.models.districts import *
from mapi.models.amphures import *
from mapi.models.provinces import *


class AddressDetail(APIView):

    def get(self, request,format=None):
        zipcode = ""

        if 'zipcode' in request.GET:
            zipcode = request.GET['zipcode']
        data = {
            'error' : 'ไม่พบข้อมูล'
        }
        zipcodes = Zipcodes.objects.all().filter(zipcode=zipcode)
        if zipcodes.count() > 0:
            districts_list = []
            amphures_list = []
            amphures_list_str = []

            for z in zipcodes:
                districts = Districts.objects.all().filter(code=z.district_code)
                
                provinces_name = ""
                for o in districts:
                    districts_list.append(o.name)
                    amphures = Amphures.objects.get(pk=o.amphur_id)
                    provinces = Provinces.objects.get(pk=o.province_id)
                    provinces_name = provinces.name

                    if amphures.name not in amphures_list_str:
                        amphures_list_str.append(amphures.name)
                        amphures_list.append(AmphuresSerializer(amphures).data)
            
            data = {
                    "amphures" : amphures_list,
                    "provinces": provinces_name
                }
        return Response(data,status.HTTP_200_OK)