from mapi.serializers.branch import BranchListSerializer
from aag_api.serializers import *
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework import generics
from mapi.pagination import LargeResultsSetPagination
from aag_api.models.branch import Branch
from aag_api.models.stock_reward import StockReward
from mapi.serializers.reward import StockRewardListSerializer,StockRewardSerializer
from django.shortcuts import get_object_or_404
from rest_framework import serializers

class BranchListView(generics.ListAPIView):



    serializer_class = BranchListSerializer

    def get_queryset(self):
        queryset = Branch.objects.all().filter(is_enabled=1).order_by('id')

        reward = self.request.GET.get('reward',None)
        if reward is not None:
            b_arr = []
            reward_list = StockReward.objects.all().filter(reward=reward,amount__gt=0)
            for item in reward_list:
                b_arr.append(item.branch.pk)
            
            queryset = queryset.filter(pk__in=b_arr)

        
        return queryset
    #pagination_class = LargeResultsSetPagination
    #permission_classes = (permissions.IsAuthenticated,)

# class BranchRewardList(generics.ListAPIView):
#     pagination_class = LargeResultsSetPagination
#     serializer_class = StockRewardListSerializer
    
#     def get_queryset(self):
#         pk = self.kwargs['pk']
        
#         b = get_object_or_404(Branch,pk=pk)
#         queryset = StockReward.objects.all().filter(branch=pk,amount__gte=1,reward__is_enabled=1) # >=1

#         o_list = []
#         for o in queryset:
#             r = o.reward
#             r.amount = o.amount
#             o_list.append(r)

#         if len(o_list)==0:
#             raise serializers.ValidationError({'error': 'ไม่มีของรางวัลที่สามารถแลกได้จากสาขา%s กรุณาเลือกสาขาอื่น' % (b.name)})
        
#         sortDi = self.request.query_params.get('sortDi', 'DESC')
#         columnKey = self.request.query_params.get('columnKey', 'created_datetime')
#         page_size = self.request.query_params.get('limit', 30)

#         if columnKey is not None and sortDi is not None:
#             if sortDi == 'ASC':
#                 queryset = queryset.order_by("reward__"+columnKey)
#             else:
#                 queryset = queryset.order_by('-'+"reward__"+columnKey)
              
#         return o_list
#     permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

# class BranchRewardDetail(generics.RetrieveUpdateDestroyAPIView):

#     def get_object(self):

#         pk = self.kwargs['pk']
#         reward = self.kwargs['reward']

#         b = get_object_or_404(Branch,pk=pk)

#         o = get_object_or_404(StockReward,branch=b.pk,reward=reward)
#         r = o.reward
#         r.amount = o.amount
#         return r

#     def get_queryset(self):
#         pk = self.kwargs['pk']

#         b = get_object_or_404(Branch,pk=pk)

#         queryset = StockReward.objects.all().filter(branch=b.pk,amount__gte=1) # >=1

#         o_list = []
#         for o in queryset:
#             r = o.reward
#             r.amount = o.amount
#             o_list.append(r)

#         return o_list
#     serializer_class = StockRewardSerializer
#     permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    