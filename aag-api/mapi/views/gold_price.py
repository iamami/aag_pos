from mapi.serializers.customer import CustomerSerializer
from rest_framework import permissions
from aag_api.models.gold_price import GoldPrice
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import generics
from aag_api.serializers import GoldPriceSerializer
from rest_framework.pagination import PageNumberPagination
from mapi.pagination import LargeResultsSetPagination

class GoldPriceView(generics.ListAPIView):

    serializer_class = GoldPriceSerializer
    queryset = GoldPrice.objects.all().order_by('-id')
    pagination_class = LargeResultsSetPagination