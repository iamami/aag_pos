from oauth2_provider.models import AccessToken
from rest_framework.views import APIView
from rest_framework.response import Response
from oauth2_provider.models import Application
from django.contrib.auth.models import User,Group
import datetime
from django.utils.crypto import get_random_string
from mapi.models.auth_token import AuthToken
from django.shortcuts import get_object_or_404
from aag_api.models.customer import Customer
from mapi.models.customer_user import CustomerUser
from oauth2_provider.models import RefreshToken

class DeleteUser(APIView):
    """
    View to list all users in the system.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """

    def get(self, request,customer_code, format=None):
        """
        Return a list of all users.
        """

        c = get_object_or_404(CustomerUser,customer__code=customer_code)
        user = c.user

        g = Group.objects.get(name='customer')
        g.user_set.remove(user)
        c.delete()

        AccessToken.objects.all().filter(user=user).delete()
        RefreshToken.objects.all().filter(user=user).delete()
        user.delete()

        msg_error = {'status' : 'Success!'}
        return Response(msg_error,200)