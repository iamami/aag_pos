from mapi.serializers.customer import *
from mapi.models.customer_user import CustomerUser
from rest_framework import permissions
from aag_api.models.customer import Customer
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework import generics
from aag_api.serializers import *
from mapi.models.otp import OneTimePassword
from rest_framework import serializers

class CustomerQrcodeView(APIView):

    def get(self, request,pk,secret,format=None):

        customer = get_object_or_404(Customer, pk=pk,secret=secret)
        serializer = CustomerSerializer(customer)
        return Response(serializer.data)
    permission_classes = (permissions.IsAuthenticated,)

class CustomerProfileView(APIView):

    def get(self, request,format=None):
        
        user_customer = get_object_or_404(CustomerUser, user__pk=request.user.pk)
        serializer = CustomerProfileSerializer(user_customer.customer)
        return Response(serializer.data)
    permission_classes = (permissions.IsAuthenticated,)

class CustomerProfileEditView(generics.UpdateAPIView):

    def get_object(self):
        request = self.request        
        obj = get_object_or_404(CustomerUser, user__pk=request.user.pk)
        self.check_object_permissions(self.request, obj.customer)
        return obj.customer
    
    def perform_update(self, serializer):


        data = self.get_object()
        is_change = False
        print(data.mobile)
        # mobile change
        if 'mobile' in  serializer.validated_data and data.mobile != serializer.validated_data['mobile']:        
                
            otp_list = OneTimePassword.objects.all().filter(customer=data.pk,is_confirm=True,phone=serializer.validated_data['mobile'])
            if otp_list.count()==0:
                print('error' + str(data.code))
                msg_error = {'mobile' : 'ยังไม่มีการยืนยัน OTP'}
                raise serializers.ValidationError(msg_error)

        # email change
        if 'email' in  serializer.validated_data and data.email != serializer.validated_data['email']:
            is_change = True
            otp_list = OneTimePassword.objects.all().filter(customer=data.pk,is_confirm=True,email=serializer.validated_data['email'])
            if otp_list.count()==0:
                msg_error = {'email' : 'ยังไม่มีการยืนยัน OTP'}
                raise serializers.ValidationError(msg_error)

        instance = serializer.save()

    serializer_class = CustomerEditSerializer
    queryset = Customer.objects.all()
    permission_classes = (permissions.IsAuthenticated,)

# customer checking
class CustomerCheckingView(APIView):
    serializer_class = CheckingSerializer
    def post(self, request,format=None):
        serializer = CheckingSerializer(data=request.data)

        if serializer.is_valid():

            # Check Customer
            customer = get_object_or_404(Customer, code=serializer.data['customer_code'])

            # Check Registered
            if CustomerUser.objects.all().filter(customer=customer.pk).count() > 0:
                msg_error = {'customer' : 'รหัสลูกค้า %s ลงทะเบียนผู้ใช้งานแล้ว' % (serializer.data['customer_code'])}
                return Response(msg_error,status.HTTP_403_FORBIDDEN)

            # Formate Username and Phone number
            regex_email = re.compile(r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$")
            regex_phone_number = re.compile(r"^0[0-9]{9}$")
        
            isEmail = False

            # Check username is email
            if regex_email.match(serializer.data['username']):
                isEmail = True
                if str(customer.email) != str(serializer.data['username']):
                    msg_error = {'username' : 'ไม่สามารถใช้อีเมลนี้ในระบบ เนื่องจากไม่ตรงในข้อมูลลูกค้า'}
                    return Response(msg_error,status.HTTP_403_FORBIDDEN)
            
            # Check username is phone
            elif regex_phone_number.match(serializer.data['username']):
                isEmail = False

                if str(customer.mobile) != str(serializer.data['username']):
                    msg_error = {'username' : 'ไม่สามารถใช้เบอร์โทรนี้ในระบบ เนื่องจากไม่ตรงในข้อมูลลูกค้า'}
                    return Response(msg_error,status.HTTP_403_FORBIDDEN)

            # Is valid
            return Response(serializer.data,status.HTTP_200_OK)
        else:
            return Response(serializer.errors,status.HTTP_403_FORBIDDEN)