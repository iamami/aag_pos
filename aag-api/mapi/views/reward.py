from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly,IsAuthenticated
from aag_api.serializers import *
from django.db.models import Q
from rest_framework.response import Response
from datetime import datetime as dt
import datetime
from rest_framework import status
from aag_api.pagination import LargeResultsSetPagination

class RewardList(generics.ListAPIView):
    pagination_class = LargeResultsSetPagination
    serializer_class = RewardListSerializer
    
    def get_queryset(self):
        queryset = Reward.objects.all().filter(is_enabled=1,stock_Reward__amount__gt=0)
        

        sortDi = self.request.query_params.get('sortDi', 'DSC')
        columnKey = self.request.query_params.get('columnKey', 'created_datetime')
        page_size = self.request.query_params.get('limit', 30)

        if columnKey is not None and sortDi is not None:
            if sortDi == 'ASC':
                queryset = queryset.order_by(columnKey)
            else:
                queryset = queryset.order_by('-'+columnKey)
              
        score_lte = self.request.query_params.get('score_lte', None)
        if score_lte is not None:
            queryset = queryset.filter(score__lte=score_lte)
        
        

        return queryset
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

class RewardDetail(generics.RetrieveAPIView):
    queryset = Reward.objects.all()
    serializer_class = RewardSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)