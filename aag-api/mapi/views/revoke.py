from mapi.serializers.auth import *
from rest_framework import permissions
from mapi.models.customer_user import CustomerUser
from mapi.models.otp import *
from aag_api.models.customer import Customer
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework import generics
from rest_framework.views import APIView
from django.contrib.auth.models import User,Group

from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope, TokenHasScope
from oauth2_provider.views.generic import ProtectedResourceView
from django.http import HttpResponse
from rest_framework import status
import requests
from oauth2_provider.models import Application
from django_hosts.resolvers import reverse
import json
from django.contrib.auth import authenticate
from mapi.models.auth_token import AuthToken
from django.db.models import Q
from mapi.models.device import Device

class RevokeTokenView(APIView):
    serializer_class = RevokeTokenSerializer
    def post(self, request,format=None):
        serializer = RevokeTokenSerializer(data=request.data)

        if serializer.is_valid():
            # Application
            try:
                application = Application.objects.get(client_id=serializer.data['client_id'],client_secret=serializer.data['client_secret'])
            except Application.DoesNotExist:
                msg_error = {'error' : 'invalid_client'}
                return Response(msg_error,status.HTTP_403_FORBIDDEN)


            s = AuthToken.delete_token(serializer.data['access_token'],application)

            # auto delete device
            if 'device_token' in  serializer.data and serializer.data['device_token'] is not None:
                Device.objects.all().filter(token=serializer.data['device_token']).delete()

            # Is valid
            if s:
                return Response({},status.HTTP_200_OK)
            else:
                return Response({'error': 'ไม่พบ Access Token ในระบบ'},status.HTTP_400_BAD_REQUEST)
        else:
            return Response(serializer.errors,status.HTTP_400_BAD_REQUEST)