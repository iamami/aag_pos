from mapi.serializers.redeem import RedeemSubmitSerializer, RedeemListSerializer,RedeemFullSerializer
from rest_framework import permissions
from aag_api.models.redeem import Redeem
from aag_api.models.score import Score
from aag_api.models.branch import Branch
from aag_api.models.stock_reward import StockReward
from aag_api.models.stock_reward_item import StockRewardItem

from aag_api.models.score_item import ScoreItem
from aag_api.models.reward import Reward
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework import generics
from mapi.models.customer_user import CustomerUser
from mapi.pagination import LargeResultsSetPagination
from rest_framework.response import Response
from rest_framework import status
from rest_framework import serializers
from rest_framework.views import APIView
from datetime import date,datetime,timedelta
import json
from mapi.models.device import Device

class RedeemList(generics.ListCreateAPIView):
    def get_serializer_class(self):
        if self.request.method=='GET':
            return RedeemListSerializer
        return RedeemSubmitSerializer
    
    def get_queryset(self):
        request = self.request
        queryset = Redeem.objects.all()
        customer = get_object_or_404(CustomerUser, user__pk=request.user.pk)
        queryset = queryset.filter(customer=customer.customer)

        return queryset
    
    def perform_create(self, serializer):
        
        request = self.request
        # check score
        customer = get_object_or_404(CustomerUser, user__pk=request.user.pk)
        score = get_object_or_404(Score,customer=customer.customer)
        
        reward = serializer.validated_data['reward']        
        kind = serializer.validated_data['kind']
        amount = serializer.validated_data['amount']
        score_need = reward.score * amount

        status = '1'
        if kind == '1': # สาขา
            branch = serializer.validated_data['branch']

        # check stock item
        # get stock reward
        stock_reward = None        
        try:
            stock_reward = StockReward.objects.get(reward=reward.pk)
        except StockReward.DoesNotExist:
            stock_reward = StockReward.objects.create(reward=reward,branch=branch,amount=0,updated_datetime=datetime.today())

        if stock_reward.amount < amount:
            raise serializers.ValidationError({'error': 'สินค้าไม่เพียงพอ'})
        
        if score_need > score.total:
            raise serializers.ValidationError({'error': 'คะแนนไม่เพียงพอสำหรับแรกของรางวัล'})

        if kind == '1' and 'branch' not in serializer.validated_data and 'receipt_date' not in serializer.validated_data :
            raise serializers.ValidationError({'branch': 'กรุณาเลือกสาขา','receipt_date': 'กรุณาเลือกวันที่รับสินค้า'})

        elif kind == '1' and 'branch' not in serializer.validated_data :
            raise serializers.ValidationError({'branch': 'กรุณาเลือกสาขา'})

        elif kind == '1' and 'receipt_date' not in serializer.validated_data :
            raise serializers.ValidationError({'receipt_date': 'กรุณาเลือกวันที่รับสินค้า'})
        
        if kind == '2' and ('address' not in serializer.validated_data or serializer.validated_data['address'] is None or serializer.validated_data['address'] == '') :
            raise serializers.ValidationError({'address': 'กรุณาระบุที่อยู่จัดส่ง'})
        
        if kind == '1' and serializer.validated_data['receipt_date'] < date.today() :
            raise serializers.ValidationError({'receipt_date': 'ไม่สามารถเลือกวันที่ย้อนหลังได้'})

        # + 5days
        days5 = date.today()+timedelta(days=5)
        if kind == '1' and serializer.validated_data['receipt_date'] < days5 :
            raise serializers.ValidationError({'receipt_date': 'ไม่สามารถเลือกวันที่ดังกล่าวได้'})
        
        redeem = serializer.save(customer=customer.customer,status=status)

        # decrease score
        ScoreItem.saveRedeem(customer.customer,redeem,score_need)

        # send notification
        device_list = Device.objects.all().filter(customer_user__customer=redeem.customer.pk)
        
        title = "สถานะของรางวัลของคุณคือ %s " % (redeem.get_status_display())
        message = "ของรางวัล %s " % (redeem.reward.name)
        res = Device.sendNotiRedeem(device_list,title,message,{
            'type': 4,
            'data': {
                'id' : redeem.id,
                'reward': str(redeem.reward),
                'status': redeem.status,
                'status_display': redeem.get_status_display(),
                'kind': redeem.kind,
                'kind_display': redeem.get_kind_display()
            }
        },redeem)
        
        # Update Stock reward
        amount = redeem.amount
        before = stock_reward.amount
        stock_reward.amount = stock_reward.amount-redeem.amount
        stock_reward.save()
        after = stock_reward.amount

        #add log stock reward
        StockRewardItem.objects.create(stock_reward=stock_reward,amount=amount,before=before,after=after,kind = "E",user=self.request.user,redeem=redeem)

        redeem.status_stock = 'Y'
        redeem.save()

    permission_classes = (permissions.IsAuthenticated,)
    pagination_class = LargeResultsSetPagination

class RedeemDetail(generics.RetrieveAPIView):
    def get_queryset(self):
        request = self.request
        queryset = Redeem.objects.all()
        customer = get_object_or_404(CustomerUser, user__pk=request.user.pk)
        queryset = queryset.filter(customer=customer.customer)
        return queryset
    serializer_class = RedeemFullSerializer
    permission_classes = (permissions.IsAuthenticated,)


class RedeemConditionView(APIView):

    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        json_data = open('./static/json/redeem_conditions.json') 
        return Response(json.load(json_data))