#Install Project
virtualenv -p python3 env
source env/bin/activate
cd aag-api/
pip install -r requirements.txt 
python manage.py makemigrations aag_api
python manage.py makemigrations mapi
python manage.py migrate
python manage.py loaddata db/user.json # username: admin , password: ***aag*** 
python manage.py loaddata db/initial_data.json  


#Deploy Staging
sudo /home/ubuntu/deployment_tools/touch_vassal_and_setperm/touch_vassal_and_setperm.aagold-mgmt-sys-stg.sh

#Deploy Production
sudo /home/ubuntu/deployment_tools/touch_vassal_and_setperm/touch_vassal_and_setperm.aagold-mgmt-sys.sh

# run coverage
coverage run --source='.' manage.py test
coverage report
coverage html

# Reset Database
*clear lease
DELETE FROM public.aag_api_leaseproduct;
DELETE FROM public.aag_api_leaseprinciple;
DELETE FROM public.aag_api_lease;
DELETE FROM public.aag_api_leaseinterest;

DELETE FROM public.aag_api_scoreitem;
DELETE FROM public.aag_api_score;

*clear bill
DELETE FROM public.aag_api_billitem;
DELETE FROM public.aag_api_billstaff;
DELETE FROM public.aag_api_bill;

*clear ledger
DELETE FROM public.aag_api_ledger;