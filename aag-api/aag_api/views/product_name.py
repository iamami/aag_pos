from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly,IsAuthenticated
from aag_api.serializers import *
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime as dt
import datetime


class ProductNameList(generics.ListCreateAPIView):
   
    serializer_class = ProductNameSerializer

    def get_queryset(self):
        queryset = ProductName.objects.all()
        name = self.request.query_params.get('name', None)
        if name is not None:
            queryset = queryset.filter(name=name)
        
        q = self.request.query_params.get('q', None)
        if q is not None:
            queryset = queryset.filter(name__contains=q)
        return queryset

    permission_classes = (permissions.IsAuthenticated,)

class ProductNameDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ProductName.objects.all()
    serializer_class = ProductNameSerializer
    permission_classes = (permissions.IsAuthenticated,)
