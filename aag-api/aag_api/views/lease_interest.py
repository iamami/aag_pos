from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly,IsAuthenticated
from aag_api.serializers import *
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime ,date,time
from aag_api.models.system_setting import *
from aag_api.models.score_item import *
from aag_api.models.lease import *
from aag_api.models.ledger import *
from decimal import Decimal
from django.shortcuts import get_object_or_404
from aag_api.permissions import *
from dateutil.relativedelta import relativedelta

class LeaseInterestList(generics.ListCreateAPIView):
   
    def get_serializer_class(self):
        if self.request.method=='GET':
            return LeaseInterestFullSerializer
        return LeaseInterestSerializer

    def perform_create(self, serializer):

        lease = self.kwargs.get("lease")
        lease_interest = LeaseInterest.objects.all().filter(lease=lease,status=3)

        if len(lease_interest)>0 :
            lease_interest[0].lease.status = 3
            lease_interest[0].lease.save()
            Ledger.objects.get(pk=serializer.validated_data['ledger'].pk).delete()
        else:
            instance = serializer.save(user=self.request.user)
            # calculate score
            setting_score = SystemSetting.getSettingScore()
            score = instance.total * Decimal(0.10) * Decimal(setting_score)

            # add score
            ScoreItem.saveInterest(self.request.user,instance.lease.customer,instance,int(score))

            instance.lease.end_date = instance.lease.end_date + relativedelta(months=instance.lease.month)
            if instance.status in [2,3]:
                instance.lease.status = instance.status
                instance.lease.interest_date = date.today()
                instance.lease.close_date = date.today()
                instance.lease.total_interest = instance.lease.total_interest + instance.total_receive
            instance.lease.save()

    def get_queryset(self):
        lease = self.kwargs.get("lease")
        queryset = LeaseInterest.objects.all()

        branch = self.request.query_params.get('branch', None)
        if branch is not None:
            queryset = queryset.filter(lease__branch=branch).order_by('id')
        if lease is not None and lease != '0':        
            queryset = queryset.filter(lease=lease)
        start_date = self.request.query_params.get('start_date', None)
        end_date = self.request.query_params.get('end_date', None)
        if start_date is not None and end_date is not None:
            from_date = datetime.strptime(start_date, '%Y-%m-%d').date()
            from_date = datetime.combine(from_date, time.min)
            to_date = datetime.strptime(end_date, '%Y-%m-%d').date()
            to_date = datetime.combine(to_date, time.max)
            queryset = queryset.filter(record_date__range=[from_date, to_date])

        return queryset

    permission_classes = (permissions.IsAuthenticated,)

class LeaseInterestDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = LeaseInterest.objects.all()
    serializer_class = LeaseInterestSerializer
    permission_classes = (permissions.IsAuthenticated,IsAdminOnly)

    def delete(self, request, lease,pk, *args, **kwargs):

        obj = get_object_or_404(LeaseInterest, pk=pk)

        # cancel ledger
        l = obj.ledger
        l.status = 0 # cancel
        l.save()

        return self.destroy(request, *args, **kwargs)


class LeaseInterestReport(APIView):

    def get(self, request,format=None):
        queryset = LeaseInterest.objects.all()

        ledger_category = self.request.query_params.get('ledger_category', None)
        if ledger_category is not None:
            queryset = queryset.filter(ledger__ledger_category = ledger_category)

        sort = self.request.query_params.get('sort', None)
        if sort =='number':
            queryset = queryset.order_by('lease')
        if sort =='date':
            queryset = queryset.order_by('record_date')

        start_date = self.request.query_params.get('start_date', None)
        end_date = self.request.query_params.get('end_date', None)
        if start_date is not None and end_date is not None:
            from_date = datetime.strptime(start_date, '%Y-%m-%d').date()
            from_date = datetime.combine(from_date, time.min)
            to_date = datetime.strptime(end_date, '%Y-%m-%d').date()
            to_date = datetime.combine(to_date, time.max)
            queryset = queryset.filter(record_date__range=[from_date, to_date])
        
        list_o = []
        for o in queryset:
            b = ''
            lease = o.lease
            items = LeaseProduct.objects.all().filter(lease=o.lease)
            for i in items:
                if b=='':
                    b = b+ i.name
                else:
                    b = b+ '+'+i.name
            
            branch = self.request.query_params.get('branch', None)    
            
            if branch is not None:
                if str(lease.branch.id) != branch:
                    continue

            list_o.append({
                'data': LeaseInterestFullSerializer(o).data,
                'name': b
            })

        return Response(list_o)
    permission_classes = (permissions.IsAuthenticated,)
