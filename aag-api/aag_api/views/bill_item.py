from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly,IsAuthenticated
from aag_api.serializers import *
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime as dt
import datetime


class BillItemList2(generics.ListCreateAPIView):
    def get_serializer_class(self):
        if self.request.method=='GET':
            return BillItemFullSerializer
        return BillItemSerializer
        
    def get_queryset(self):
        queryset = BillItem.objects.all()

        start_date = self.request.query_params.get('start_date', None)
        end_date = self.request.query_params.get('end_date', None)
        if start_date is not None and end_date is not None:
            from_date = dt.strptime(start_date, '%Y-%m-%d').date()
            from_date = datetime.datetime.combine(from_date, datetime.time.min)
            to_date = dt.strptime(end_date, '%Y-%m-%d').date()
            to_date = datetime.datetime.combine(to_date, datetime.time.max)
            queryset = queryset.filter(record_date__range=[from_date, to_date])

        # filter bill
        bill=self.kwargs.get("bill")
        if bill is not None and bill != '0':
            queryset = queryset.filter(bill=self.kwargs.get("bill"))
        
        # filter kind
        kind = self.request.query_params.get('kind', None)
        if kind is not None:
            queryset = queryset.filter(kind=kind)
        
        # filter status_stock
        status_stock = self.request.query_params.get('status_stock', None)
        if status_stock is not None:
            queryset = queryset.filter(status_stock=status_stock)

        # filter customer id
        customer_id = self.request.query_params.get('customer_id', None)
        if customer_id is not None:
            customer_id = int(customer_id)
            _queryset = []
            for q in queryset:
                if customer_id==q.bill.customer.id:
                    _queryset.append(q)
            queryset = _queryset
        
        return queryset
    permission_classes = (permissions.IsAuthenticated,)

class BillItemList(generics.ListCreateAPIView):
    queryset = BillItem.objects.all()
    def get_queryset(self):
        queryset = BillItem.objects.all()

        branch = self.request.query_params.get('branch', None)
        if branch is not None:
            queryset = queryset.filter(bill__branch=branch)
        
        start_date = self.request.query_params.get('start_date', None)
        end_date = self.request.query_params.get('end_date', None)
        if start_date is not None and end_date is not None:
            from_date = dt.strptime(start_date, '%Y-%m-%d').date()
            from_date = datetime.datetime.combine(from_date, datetime.time.min)
            to_date = dt.strptime(end_date, '%Y-%m-%d').date()
            to_date = datetime.datetime.combine(to_date, datetime.time.max)
            queryset = queryset.filter(record_date__range=[from_date, to_date])
        
        product = self.request.query_params.get('product', None)
        if product is not None:
            queryset = queryset.filter(product=product)

        product_type = self.request.query_params.get('product_type', None)
        if product_type is not None:
            queryset = queryset.filter(product__kind=product_type)
        
        kind = self.request.query_params.get('kind', None)
        
        if kind is not None:

            kind_list = []
            if ',' in kind:
                kind_list = kind.split(',')            
                queryset = queryset.filter(bill__kind__in=kind_list)
            else:
                queryset = queryset.filter(bill__kind=kind)
        return queryset

    def get_serializer_class(self):
        if self.request.method=='GET':
            return BillItemFullSerializer
        return BillItemSerializer
    permission_classes = (permissions.AllowAny,)

class BillItemDetail(generics.RetrieveAPIView):
    queryset = BillItem.objects.all()
    serializer_class = BillItemSerializer
    permission_classes = (permissions.IsAuthenticated,)
