from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly,IsAuthenticated
from aag_api.serializers import *
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime as dt
import datetime

class SystemSettingList(generics.ListCreateAPIView):
   
    serializer_class = SystemSettingSerializer
    queryset = SystemSetting.objects.all()
    permission_classes = (permissions.IsAuthenticated,)

class  SystemSettingDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset =  SystemSetting.objects.all()
    serializer_class =  SystemSettingSerializer
    permission_classes = (permissions.IsAuthenticated,)