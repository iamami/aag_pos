from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly,IsAuthenticated
from aag_api.serializers import *
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime as dt
import datetime

class BillStaffList(generics.ListCreateAPIView):
    
    def get_queryset(self):
        queryset = BillStaff.objects.all()
        bill_id = self.request.query_params.get('bill_id', None)
        if bill_id is not None:
            queryset = queryset.filter(bill=bill_id)
        staff = self.request.query_params.get('staff', None)
        if staff is not None:
            queryset = queryset.filter(staff=staff)
        return queryset
    def get_serializer_class(self):
        if self.request.method=='GET':
            return BillStaffFullSerializer
        return BillStaffSerializer
    permission_classes = (permissions.AllowAny,)

class BillStaffDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = BillStaff.objects.all()
    serializer_class = BillStaffSerializer
    permission_classes = (permissions.IsAuthenticated,)
