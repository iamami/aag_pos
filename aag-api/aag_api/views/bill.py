from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly,IsAuthenticated
from aag_api.serializers import *
from aag_api.models.score import *
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime as dt
import datetime
from rest_framework import status
from aag_api.models.system_setting import *
from aag_api.models.score_item import *
from decimal import Decimal
import json
from aag_api.permissions import *

def getScoreFormWeightBaht(product):
    json_data = open('./static/json/score_unit_setting.json')
    unit_setting = json.load(json_data) 
    setting_score = SystemSetting.getSettingScore()
    unit = 0
    for item in unit_setting :
        if product.weight == (Decimal(item['value']) * product.category.weight):
            unit = item['unit']
            break
        elif 'type' in item and item['type'] == 'max' and product.weight > (Decimal(item['value']) * product.category.weight):
            unit = item['unit']
            break
    
    print(unit)
    return unit * int(setting_score)

def getScoreFormWeightGram(product):
    setting_score = SystemSetting.getSettingScore()
    unit = 0
    
    if product.weight >= Decimal('22.8'):
        unit = 50
    elif product.weight >= Decimal('3.7'):
        unit = 20
    elif product.weight >= Decimal('1.9'):
        unit = 13
    elif product.weight >= Decimal('1'):
        unit = 8
    
    return unit * int(setting_score)

class BillList(generics.ListCreateAPIView):

    def get_serializer_class(self):
        if self.request.method=='GET':
            return BillFullSerializer
        return BillSerializer
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def get_queryset(self):
        queryset = Bill.objects.all().order_by('-record_date')
        bill_number = self.request.query_params.get('bill_number', None)
        tax_number = self.request.query_params.get('tax_number', None)
        bill_number_contains = self.request.query_params.get('bill_number_contains', None)
        tax_number_contains = self.request.query_params.get('tax_number_contains', None)

        if bill_number_contains is not None and bill_number_contains != '':
            queryset = queryset.filter(bill_number__contains=bill_number_contains)
        if tax_number_contains is not None and tax_number_contains != '':
            queryset = queryset.filter(tax_number__contains=tax_number_contains)

        branch = self.request.query_params.get('branch', None)
        if branch is not None and branch != '':
            queryset = queryset.filter(branch=branch)

        if bill_number is not None and bill_number != '':
            queryset = queryset.filter(bill_number=bill_number)
        elif tax_number is not None:
            queryset = queryset.filter(tax_number__contains=tax_number)
        else:
            customer_id = self.request.query_params.get('customer_id', None)
            if customer_id is not None and customer_id != '':
                queryset = queryset.filter(customer=customer_id)
            check_code = self.request.query_params.get('check_code', None)
            if check_code is not None and check_code != '':
                queryset = queryset.filter(check_code=check_code)
            
            kind = self.request.query_params.get('kind', None)
            if kind is not None and kind != '':
                queryset = queryset.filter(kind=kind)
            
            payment = self.request.query_params.get('payment', None)
            if payment is not None and payment != '':
                queryset = queryset.filter(payment=payment)

            start_date = self.request.query_params.get('start_date', None)
            end_date = self.request.query_params.get('end_date', None)
            if start_date is not None and end_date is not None and bill_number is None:
                from_date = dt.strptime(start_date, '%Y-%m-%d').date()
                from_date = datetime.datetime.combine(from_date, datetime.time.min)
                to_date = dt.strptime(end_date, '%Y-%m-%d').date()
                to_date = datetime.datetime.combine(to_date, datetime.time.max)
                queryset = queryset.filter(bill_date__range=[from_date, to_date])

        return queryset
    permission_classes = (IsAuthenticated,)

class BillDetail(generics.RetrieveUpdateAPIView):
    queryset = Bill.objects.all()
    def get_serializer_class(self):
        if self.request.method=='GET':
            return BillFullSerializer
        return BillSerializer
    permission_classes = (IsAuthenticated,IsAdminDeleteOnly,IsAdminUpdateOnly,)

class BillUpdateStock(APIView):

    def get(self, request,bill,format=None):
        b = Bill.objects.get(pk=bill)
        items = BillItem.objects.filter(bill=bill)
        if b is None :
            resp = {'status' : False,'error' : 'ERROR_NOTF'}
            return Response(resp,status.HTTP_400_BAD_REQUEST)
        elif b.status_stock == 'Y' and  items.count() > 0:
            resp = {'status' : False,'error' : 'ERROR_UPDATED','item' : items.count(),"user" : request.user.id}
            return Response(resp,status.HTTP_400_BAD_REQUEST)

        object_type = 1
        kind = 'IN'
        title = 'ขายทอง'
        if b.kind=='BU':
            title = 'ซื้อทองเก่า'
            kind = 'EX'
            object_type = 2
        if b.kind == 'XC':
            title = 'เปลี่ยนทอง'
            kind = 'IN'
            object_type = 3

        products = []

        for o in items:

            if o.status_stock == 'Y':
                continue
            if o.kind == 'SE':
                prod = StockProduct.objects.filter(product_id=o.product_id,branch=b.branch)
                
                before = 0
                after = 0
                # update stock counter
                if len(prod)==0 :
                    resp = {'error' : 'พบสินค้าในสต็อก (%s)' % ( o.product)}
                    return Response(resp,status.HTTP_400_BAD_REQUEST)
                    prod = StockProduct(product_id=o.product_id,branch=b.branch,amount=-o.amount)
                    after = -o.amount
                    prod.save()
                else:

                    prod = prod[0]
                    before = prod.amount
                    prod.amount = prod.amount-o.amount
                    if prod.amount < 0 :
                        resp = {'error' : 'สินค้าในสต็อกบางรายการไม่เพียงพอ "%s"' % ( o.product)}
                        return Response(resp,status.HTTP_400_BAD_REQUEST)
                    # update stock product
                    after = prod.amount
                    prod.out_date = datetime.date.today()
                    prod.save()

                # log stock item
                sp = StockProductItem(object_number=b.bill_number,object_id=b.id,weight=o.weight,weight_total=o.weight,weight_real=o.weight,amount=o.amount,branch=b.branch,product_id=o.product_id,user_id=b.user_id,stock_id=prod.id,kind='O',before=before,after=after)
                sp.save()
                o.status_stock = 'Y'
                o.save()
                print('bb============================')
                print('SE')

                # score condition
                score = 0
                setting_score = SystemSetting.getSettingScore()

                if o.product.category.id == 1 : # ทองรูปพรรณ 90
                    score = int(Decimal(0.05) * o.sell * int(setting_score))

                elif o.product.type_weight == 1 and o.product.type_sale == 1 and o.product.kind.id != 12 and o.product.category.id == 2: # งานชั่งทองรูปพรรณ 96.5 น้ำหนักบาท
                    score = getScoreFormWeightBaht(o.product)
                   
                elif o.product.type_weight == 2 and o.product.type_sale == 1 and o.product.kind.id != 12 and o.product.category.id == 2: # งานชั่งทองรูปพรรณ 96.5 น้ำหนักกรัม
                    score = getScoreFormWeightGram(o.product)
           

                if score > 0:
                    ScoreItem.saveBillItem(request.user,b.customer,o,score)

            else:
                cate = StockCategory.objects.filter(category=o.category,branch=b.branch)
                if len(cate)==0 :
                    cate = StockCategory(category_id=o.category_id,branch=b.branch,weight=o.weight,total=b.buy)
                    cate.save()
                else:
                    cate = cate[0]
                    cate.weight = cate.weight+o.weight # update stock product
                    cate.int_date = datetime.date.today()
                    cate.total += b.buy
                    cate.save()

                # log stock item
                sp = StockCategoryItem(object_id=b.id,weight=o.weight,category_id=o.category_id,branch=b.branch,user_id=b.user_id,stock_id=cate.id)
                sp.save()
                o.status_stock = 'Y'
                o.save()
                print('aa============================')
                print(o.kind)
        b.status_stock = 'Y'
        b.save()

        resp = {'status' : True,'error' : '','size' : items.count()}
        return Response(resp)
    permission_classes = (permissions.IsAuthenticated,)

class VoidBillUpdateStock(APIView):

    def post(self, request,bill,format=None):
        b = Bill.objects.get(pk=bill)
        if b is None :
            resp = {'status' : False,'error' : 'ERROR_NOTF'}
            return Response(resp)
        # elif b.status_stock == 'N':
        #     resp = {'status' : False,'error' : 'ERROR_UPDATED'}
        #     return Response(resp)

        items = BillItem.objects.filter(bill=b.id)
        kind = 'IN'
        title = 'ยกเลิกบิล'
        if b.kind=='BU':
            kind = 'EX'
        if b.kind == 'XC':
            kind = 'IN'

        products = []
        for o in items:

            if o.status_stock == 'N':
                continue
                
            if o.kind == 'SE':
                prod = StockProduct.objects.filter(product=o.product,branch=b.branch)
                # update stock counter
                before = 0
                after = 0
                if len(prod)==0 :
                    prod = StockProduct(product=o.product,branch=b.branch,amount=prod.amount)
                    after = prod.amount
                    prod.save()
                else:
                    prod = prod[0]
                    before = prod.amount
                    prod.amount = prod.amount+o.amount # update stock category
                    after = prod.amount
                    prod.in_date = datetime.date.today()
                    prod.save()

                # log stock item
                sp = StockProductItem(
                    object_number=b.bill_number,
                    object_id=b.id,
                    weight=o.weight,
                    weight_total=o.weight,
                    weight_real=o.weight,
                    amount=o.amount,
                    branch=b.branch,
                    product=o.product,
                    user_id=b.user_id,
                    stock_id=prod.id,
                    kind='V',
                    before=before,
                    after=after)
                sp.save()
                o.status_stock = 'N'
                o.save()

                # score
                score_amount = o.product.score * o.amount
                if score_amount > 0:
                    ScoreItem.voidBillItem(request.user,b.customer,o,score_amount)
            else:
                cate = StockCategory.objects.filter(category=o.category,branch=b.branch)
                if len(cate)==0 :
                    cate = StockCategory(category_id=o.category_id,branch=b.branch,weight=0)
                    cate.save()
                else:
                    cate = cate[0]
                    cate.weight = cate.weight-o.weight # update stock product
                    cate.out_date = datetime.date.today()
                    cate.total -= o.buy
                    cate.save()

                # log stock item
                sp = StockCategoryItem(object_id=b.id,weight=o.weight,category_id=o.category_id,branch=b.branch,user_id=b.user_id,stock_id=cate.id,kind='V')
                sp.save()
                o.status_stock = 'N'
                o.save()
        b.status_stock = 'N'
        b.is_void = 1
        b.canceler = request.user.id
        b.save()
        resp = {'status' : True,'error' : ''}

        # cancel
        ledger = Ledger.objects.get(object_id=b.id)
        ledger.status = 0
        ledger.save()

        return Response(resp)
    permission_classes = (IsAuthenticated,IsAdminDeleteOnly,IsAdminUpdateOnly,)

class BillTax(APIView):
    def get(self, request,bill,format=None):
        b = Bill.objects.get(pk=bill)
        if b.tax_number == '':
            s = BranchSetting.objects.get(branch=b.branch)
            sz = "%09d" % (s.tax_counter,)
            b.tax_number = s.tax_code+sz
            s.tax_counter = s.tax_counter+1
            s.save()
        b.save()
        resp = {'status' : True,'branch': b.branch.id,'tax_number': b.tax_number}
        return Response(resp)
    permission_classes = (permissions.IsAuthenticated,)