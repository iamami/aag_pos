from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly,IsAuthenticated
from aag_api.serializers import *
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime as dt, date
from rest_framework import status
import datetime
from aag_api.permissions import *
from rest_framework import serializers
class LedgerList(generics.ListCreateAPIView):
    
    def get_queryset(self):
        queryset = Ledger.objects.all().filter(status=1).order_by('-record_date')

        id = self.request.query_params.get('id', None)
        ledger_object = self.request.query_params.get('ledger_object', None)
        number = self.request.query_params.get('number', None)
        object_number = self.request.query_params.get('object_number', None)

        if  number is not None:
            queryset = queryset.filter(number__contains=number)
        elif  object_number is not None:
            queryset = queryset.filter(object_number__contains=object_number)
        elif  ledger_object is not None:
            queryset = queryset.filter(object_number__contains=ledger_object)
        else:
            branch = self.request.query_params.get('branch', None)
            if branch is not None:
                queryset = queryset.filter(branch=branch)
            
            customer_id = self.request.query_params.get('customer_id', None)
            if customer_id is not None and customer_id != '':
                queryset = queryset.filter(customer=customer_id)
                    
            ledger_category = self.request.query_params.get('ledger_category', None)
            if ledger_category is not None:
                queryset = queryset.filter(ledger_category=ledger_category)
            
            kind = self.request.query_params.get('kind', None)
            if kind is not None:
                queryset = queryset.filter(kind=kind)        
            
            start_date = self.request.query_params.get('start_date', None)
            end_date = self.request.query_params.get('end_date', None)
            if start_date is not None and end_date is not None:
                from_date = dt.strptime(start_date, '%Y-%m-%d').date()
                from_date = datetime.datetime.combine(from_date, datetime.time.min)
                to_date = dt.strptime(end_date, '%Y-%m-%d').date()
                to_date = datetime.datetime.combine(to_date, datetime.time.max)
                queryset = queryset.filter(record_date__range=[from_date, to_date])
        return queryset
    
    def perform_create(self, serializer):

        ledger_category = serializer.validated_data['ledger_category']
        if ledger_category.pk == 10:
            s = Ledger.objects.all().filter(object_id=serializer.validated_data['object_id'],ledger_category=10,status=1)
            if len(s) >0 :
                o = Lease.objects.get(pk=serializer.validated_data['object_id'])
                o.status = 3
                o.save()
                raise serializers.ValidationError({'error': 'ไม่สามารถทำรายการไถ่คืนซ้ำได้'})

        serializer.save(user=self.request.user)
        
    def get_serializer_class(self):
        if self.request.method=='GET':
            return LedgerFullSerializer
        return LedgerSerializer

    permission_classes = (permissions.IsAuthenticated,)

class LedgerDetail(generics.RetrieveUpdateAPIView):
    queryset = Ledger.objects.all()
    serializer_class = LedgerSerializer
    permission_classes = (permissions.IsAuthenticated,IsAdminOnly)

    def perform_create(self, serializer):

        serializer.save(interest_date=serializer.data['start_date'])

    def perform_update(self, serializer):

        # over 1 day
        o = self.get_object()
        if o.ledger_date != datetime.date.today():
            raise serializers.ValidationError({'error': 'ไม่สามารถลบย้อนหลังได้'})
        instance = serializer.save()