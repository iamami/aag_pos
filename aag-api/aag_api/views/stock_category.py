from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly,IsAuthenticated
from aag_api.serializers import *
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime as dt
import datetime


class StockCategoryList(generics.ListCreateAPIView):
    #queryset = StockCategory.objects.all()
    def get_queryset(self):
        queryset = StockCategory.objects.all()
        branch_id = self.request.query_params.get('branch', None)
        if branch_id is not None:
            queryset = queryset.filter(branch_id=branch_id)
        return queryset
    serializer_class = StockCategorySerializer
    permission_classes = (permissions.IsAuthenticated,)

class StockCategoryDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = StockCategory.objects.all()
    serializer_class = StockCategorySerializer
    permission_classes = (permissions.IsAuthenticated,)
