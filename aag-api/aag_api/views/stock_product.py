from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly,IsAuthenticated
from aag_api.serializers import *
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime as dt
import datetime

class StockProductList(generics.ListCreateAPIView):

    def get_serializer_class(self):
        if self.request.method=='GET':
            return StockProductGetSerializer
        return StockProductSerializer
    
    def get_queryset(self):
        queryset = StockProduct.objects.all().order_by('product__name')
        branch = self.request.query_params.get('branch', None)
        if branch is not None:
            queryset = queryset.filter(branch=branch)
        
        product = self.request.query_params.get('product', None)
        if product is not None:
            queryset = queryset.filter(product=product)

        category = self.request.query_params.get('category', None)
        if category is not None:
            queryset = queryset.filter(product__category=category)

        out_date_lt = self.request.query_params.get('out_date_lt', None)
        if out_date_lt is not None:
            from_date = dt.strptime(out_date_lt, '%Y-%m-%d').date()
            from_date = datetime.datetime.combine(from_date, datetime.time.min)
            queryset = queryset.filter(out_date__lt=from_date)
        
        out_date_gte = self.request.query_params.get('out_date_gte', None)
        if out_date_gte is not None:
            from_date = dt.strptime(out_date_gte, '%Y-%m-%d').date()
            from_date = datetime.datetime.combine(from_date, datetime.time.max)
            queryset = queryset.filter(out_date__gte=from_date)

        in_date_lt = self.request.query_params.get('in_date_lt', None)   
        in_date_gte = self.request.query_params.get('in_date_gte', None)
        if in_date_gte is not None and in_date_lt  is not None:
            from_date = dt.strptime(in_date_gte, '%Y-%m-%d').date()
            from_date = datetime.datetime.combine(from_date, datetime.time.max)
            from_date2 = dt.strptime(in_date_lt, '%Y-%m-%d').date()
            from_date2 = datetime.datetime.combine(from_date2, datetime.time.min)
            queryset = queryset.filter(Q(in_date__gte=from_date) | Q(in_date__lt=from_date2))
        else:
            if in_date_gte is not None:
                from_date = dt.strptime(in_date_gte, '%Y-%m-%d').date()
                from_date = datetime.datetime.combine(from_date, datetime.time.max)
                queryset = queryset.filter(in_date__gte=from_date)
            if in_date_lt is not None:
                from_date = dt.strptime(in_date_lt, '%Y-%m-%d').date()
                from_date = datetime.datetime.combine(from_date, datetime.time.min)
                queryset = queryset.filter(in_date__lt=from_date)

        return queryset
    serializer_class = StockProductSerializer
    permission_classes = (permissions.IsAuthenticated,)

class StockProductDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = StockProduct.objects.all()
    serializer_class = StockProductSerializer
    permission_classes = (permissions.IsAuthenticated,)

class StockProductItemList(generics.ListCreateAPIView):
   
    def get_serializer_class(self):
        if self.request.method=='GET':
            return StockProductItemFullSerializer
        return StockProductItemSerializer

    def get_queryset(self):
        stock = self.kwargs.get("stock")
        queryset = StockProductItem.objects.all().order_by('-record_date')

        if stock != '0':
            queryset = queryset.filter(stock=stock)
        
        # filter branch
        branch = self.request.query_params.get('branch', None)
        if branch is not None:
            queryset = queryset.filter(branch=branch)

        
        start_date = self.request.query_params.get('start_date', None)
        end_date = self.request.query_params.get('end_date', None)
        if start_date is not None and end_date is not None:
            from_date = dt.strptime(start_date, '%Y-%m-%d').date()
            from_date = datetime.datetime.combine(from_date, datetime.time.min)
            to_date = dt.strptime(end_date, '%Y-%m-%d').date()
            to_date = datetime.datetime.combine(to_date, datetime.time.max)
            queryset = queryset.filter(record_date__range=[from_date, to_date])
        
        date_lt = self.request.query_params.get('date_lt', None)
        if date_lt is not None:
            from_date = dt.strptime(date_lt, '%Y-%m-%d').date()
            from_date = datetime.datetime.combine(from_date, datetime.time.max)
            queryset = queryset.filter(record_date__lt=from_date)

        return queryset

    permission_classes = (permissions.IsAuthenticated,)

class StockProductBuyTotal(APIView):

    def get(self, request,format=None):

        queryset = StockProduct.objects.all()
        branch = self.request.query_params.get('branch', None)
        if branch is not None:
            queryset = queryset.filter(branch=branch)
        
        product_type = self.request.query_params.get('product_type', None)
        if product_type is not None:
            queryset = queryset.filter(product__kind=product_type)

        product = self.request.query_params.get('product', None)
        if product is not None:
            queryset = queryset.filter(product=product)

        out_date_lt = self.request.query_params.get('out_date_lt', None)
        if out_date_lt is not None:
            from_date = dt.strptime(out_date_lt, '%Y-%m-%d').date()
            from_date = datetime.datetime.combine(from_date, datetime.time.min)
            queryset = queryset.filter(out_date__lt=from_date)
        
        out_date_gte = self.request.query_params.get('out_date_gte', None)
        if out_date_gte is not None:
            from_date = dt.strptime(out_date_gte, '%Y-%m-%d').date()
            from_date = datetime.datetime.combine(from_date, datetime.time.max)
            queryset = queryset.filter(out_date__gte=from_date)

        in_date_lt = self.request.query_params.get('in_date_lt', None)   
        in_date_gte = self.request.query_params.get('in_date_gte', None)
        if in_date_gte is not None and in_date_lt  is not None:
            from_date = dt.strptime(in_date_gte, '%Y-%m-%d').date()
            from_date = datetime.datetime.combine(from_date, datetime.time.max)
            from_date2 = dt.strptime(in_date_lt, '%Y-%m-%d').date()
            from_date2 = datetime.datetime.combine(from_date2, datetime.time.min)
            queryset = queryset.filter(Q(in_date__gte=from_date) | Q(in_date__lt=from_date2))
        else:
            if in_date_gte is not None:
                from_date = dt.strptime(in_date_gte, '%Y-%m-%d').date()
                from_date = datetime.datetime.combine(from_date, datetime.time.max)
                queryset = queryset.filter(in_date__gte=from_date)
            if in_date_lt is not None:
                from_date = dt.strptime(in_date_lt, '%Y-%m-%d').date()
                from_date = datetime.datetime.combine(from_date, datetime.time.min)
                queryset = queryset.filter(in_date__lt=from_date)
        list_o = []
        for o in queryset:
            b = 0
            items = StockProductItem.objects.all().filter(stock=o,kind='O')
            for i in items:
                b = b+ i.amount
            list_o.append({
                'data': StockProductGetSerializer(o).data,
                'buy': b
            })
        return Response(list_o)
    permission_classes = (permissions.IsAuthenticated,)
