from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly,IsAuthenticated
from aag_api.serializers import *
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime as dt
import datetime
from rest_framework import status

class CategoryList(generics.ListCreateAPIView):
    def get_queryset(self):
        queryset = Category.objects.all()

        is_enabled = self.request.query_params.get('is_enabled', 1)
        if is_enabled is not None:
            queryset = queryset.filter(is_enabled=is_enabled)

        return queryset
    serializer_class = CategorySerializer
    permission_classes = (permissions.IsAuthenticated,)

class CategoryDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = (permissions.IsAuthenticated,)


    def delete(self, request,pk,**kwargs):
        
        # check in bill
        if Product.objects.all().filter(category=pk).count() > 0 or BillItem.objects.all().filter(category=pk).count()>0 or StockCategory.objects.all().filter(category=pk).count()>0 :
            content = {'error': 'ไม่สามารถลบกลุ่มสินค้าได้ เนื่องจากถูกใช้งานแล้ว'}
            return Response(content, status=status.HTTP_403_FORBIDDEN)

        return self.destroy(request, pk, **kwargs)
