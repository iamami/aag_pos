from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly,IsAuthenticated
from aag_api.serializers import *
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime as dt
import datetime
from aag_api.models.lease import Lease
from aag_api.models.lease_interest import LeaseInterest
from aag_api.models.lease_product import LeaseProduct
from aag_api.models.product_name import ProductName
from aag_api.models.ledger import Ledger
from aag_api.permissions import *
from django.shortcuts import get_object_or_404

class LeaseList(generics.ListCreateAPIView):

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def get_serializer_class(self):
        if self.request.method=='GET':
            return LeaseFullSerializer
        elif self.request.method=='POST':
            return LeaseCreateSerializer
        return LeaseSerializer
    
    def get_queryset(self):
        queryset = Lease.objects.all()

        sort = self.request.query_params.get('sort', None)
        if sort =='number':
            queryset = queryset.order_by('number')
        
        if sort =='in_date':
            queryset = queryset.order_by('-start_date')

        number = self.request.query_params.get('number', None)
        id = self.request.query_params.get('id', None)
        if number is not None:
            queryset = queryset.filter(number=number)
            branch = self.request.query_params.get('branch', None)
            if branch is not None:
                queryset = queryset.filter(branch=branch)
        elif id is not None:
            queryset = queryset.filter(id=id)
        else:

            is_enabled = self.request.query_params.get('is_enabled', None)
            if is_enabled is not None:
                queryset = queryset.filter(is_enabled=is_enabled)
            else:
                queryset = queryset.filter(is_enabled=1)

            start_date = self.request.query_params.get('start_date', None)
            end_date = self.request.query_params.get('end_date', None)
            if start_date is not None and end_date is not None:
                from_date = dt.strptime(start_date, '%Y-%m-%d').date()
                from_date = datetime.datetime.combine(from_date, datetime.time.min)
                to_date = dt.strptime(end_date, '%Y-%m-%d').date()
                to_date = datetime.datetime.combine(to_date, datetime.time.max)
                queryset = queryset.filter(start_date__range=[from_date, to_date])

            start_close_date = self.request.query_params.get('start_close_date', None)
            end_close_date = self.request.query_params.get('end_close_date', None)
            if start_close_date is not None and end_close_date is not None:
                from_date = dt.strptime(start_close_date, '%Y-%m-%d').date()
                from_date = datetime.datetime.combine(from_date, datetime.time.min)
                to_date = dt.strptime(end_close_date, '%Y-%m-%d').date()
                to_date = datetime.datetime.combine(to_date, datetime.time.max)
                queryset = queryset.filter(close_date__range=[from_date, to_date])
            
            start_end_date = self.request.query_params.get('start_end_date', None)
            end_end_date = self.request.query_params.get('end_end_date', None)
            if start_end_date is not None and end_end_date is not None:
                from_date = dt.strptime(start_end_date, '%Y-%m-%d').date()
                from_date = datetime.datetime.combine(from_date, datetime.time.min)
                to_date = dt.strptime(end_end_date, '%Y-%m-%d').date()
                to_date = datetime.datetime.combine(to_date, datetime.time.max)
                queryset = queryset.filter(end_date__range=[from_date, to_date])
            
            due_date = self.request.query_params.get('due_date', None)
            if due_date is not None:
                to_date = dt.strptime(due_date, '%Y-%m-%d').date()
                to_date = datetime.datetime.combine(to_date, datetime.time.max)
                queryset = queryset.filter(end_date__lt=to_date)
            
            start_date = self.request.query_params.get('start_out_date', None)
            end_date = self.request.query_params.get('end_out_date', None)
            if start_date is not None and end_date is not None:
                from_date = dt.strptime(start_date, '%Y-%m-%d').date()
                from_date = datetime.datetime.combine(from_date, datetime.time.min)
                to_date = dt.strptime(end_date, '%Y-%m-%d').date()
                to_date = datetime.datetime.combine(to_date, datetime.time.max)
                queryset = queryset.filter(out_date__range=[from_date, to_date])
            
            start_price = self.request.query_params.get('start_price', None)
            end_price = self.request.query_params.get('end_price', None)
            if start_price is not None and end_price is not None:
                queryset = queryset.filter(amount__range=[start_price, end_price])
            elif start_price is not None and end_price is None:
                queryset = queryset.filter(amount__gte=start_price)
            elif start_price is None and end_price is not None:
                queryset = queryset.filter(amount__lte=end_price)
                
            branch = self.request.query_params.get('branch', None)
            if branch is not None:
                queryset = queryset.filter(branch=branch)
            
            product = self.request.query_params.get('product', None)
            if product is not None:
                name = ProductName.objects.get(pk=product).name
                queryset = queryset.filter(lease_prodcut__name = name)
            
            status = self.request.query_params.get('status', None)
            if status is not None:
                queryset = queryset.filter(status=status)

            status_in = self.request.query_params.get('status_in', None)
            if status_in is not None:
                queryset = queryset.filter(status__in=status_in.split(','))
                
            customer_id= self.request.query_params.get('customer', None)
            if customer_id is not None:
                queryset = queryset.filter(customer=customer_id)
            else:
                citizen_id = self.request.query_params.get('citizen_id', None)
                if citizen_id is not None:
                    queryset = queryset.filter(citizen_id=citizen_id)
                
                phone = self.request.query_params.get('phone', None)
                if phone is not None:
                    queryset = queryset.filter(phone=phone)


                
        return queryset

    permission_classes = (permissions.IsAuthenticated,)

class LeaseDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Lease.objects.all()
    def get_serializer_class(self):
        if self.request.method=='GET':
            return LeaseFullSerializer
        return LeaseUpdateSerializer
    permission_classes = (permissions.IsAuthenticated,IsAdminUpdateOnly,IsAdminDeleteOnly)

class LeaseCancel(APIView):
    
    permission_classes = (permissions.IsAuthenticated,IsAdminOnly)
    def post(self, request,pk,format=None):
        status = 200

        obj = get_object_or_404(Lease, pk=pk)

        if obj.start_date != datetime.date.today():

            resp = {
                'error': 'ไม่สามารถลบย้อนหลังได้'
            }
            return Response(resp,400)

        obj.is_enabled = 0
        obj.save()
        
        # cancel LeaseProduct
        items = LeaseProduct.objects.all().filter(lease=pk)
        for o in items:
            o.is_enabled = 0
            o.save()

        resp = {}
        return Response(resp,status)

# cancel Redeem
class LeaseCancelRedeem(APIView):
    
    permission_classes = (permissions.IsAuthenticated,IsAdminOnly)
    def post(self, request,pk,format=None):
        status = 200

        try:
            lease = Lease.objects.get(pk=pk)

            if lease.status != 3:
                return Response({
                    'error': 'ใบขายฝากไม่อยู่ในสถานะ ไถ่คืน'
                },400)
        except Lease.DoesNotExist:
            return Response({
                'error': 'ไม่พบใบขายฝาก'
            },400)
        
        try:
            interest = LeaseInterest.objects.get(lease=pk,status=3)
            if interest.pay_date != datetime.date.today():

                resp = {
                    'error': 'ไม่สามารถทำรายการย้อนหลังได้'
                }
                return Response(resp,400)
            # delete interest
            interest.delete()
            # delete Ledger item
            interest.ledger.status = 0
            interest.ledger.save()
        except LeaseInterest.DoesNotExist:
            pass
        

        interest_list = LeaseInterest.objects.all().filter(lease=pk)
        if len(interest_list) > 0:
            lease.status = 2 # ต่อดอก
        else:
            lease.status = 1 # new

        # update total_interest
        total_interest = 0
        for item in interest_list:
            total_interest += item.total_receive
        lease.total_interest = total_interest
        lease.save()

        resp = {}
        return Response(resp,status)

# cancel Out
class LeaseCancelOut(APIView):
    
    permission_classes = (permissions.IsAuthenticated,IsAdminOnly)
    def post(self, request,pk,format=None):
        status = 200

        try:
            lease = Lease.objects.get(pk=pk)
            if lease.status != 4:
                return Response({
                    'error': 'ใบขายฝากไม่อยู่ในสถานะ คัดออก'
                },400)
        except Lease.DoesNotExist:
            return Response({
                'error': 'ไม่พบใบขายฝาก'
            },400)
    

        # return stock
        items = LeaseProduct.objects.filter(lease=pk)
        products = []
        for o in items:
            price = lease.amount
            cate = StockCategory.objects.filter(category=o.category,branch=lease.branch)
            if len(cate)==0 :
                cate = StockCategory(category_id=o.category_id,branch_id=lease.branch_id,weight=0,total=0)
                cate.save()
            else:
                cate = cate[0]
                cate.weight = cate.weight-o.weight # add stock category
                cate.int_date = datetime.date.today()
                cate.total -= price
                cate.save()

            # log stock item
            sp = StockCategoryItem(object_id=lease.id,weight=o.weight,category_id=o.category.id,branch=lease.branch,user_id=lease.user.id,stock_id=cate.id,kind='EJ')
            sp.save()
            o.status_stock = 'N'
            o.save()
        lease.status_stock = 'N'

        interest_list = LeaseInterest.objects.all().filter(lease=pk)
        if len(interest_list) > 0:
            lease.status = 2 # ต่อดอก
        else:
            lease.status = 1 # new

        # update total_interest
        total_interest = 0
        for item in interest_list:
            total_interest += item.total_receive
        lease.total_interest = total_interest
        lease.save()

        resp = {}
        return Response(resp,status)


class LeaseReport(generics.ListAPIView):
    serializer_class = LeaseFullSerializer
    def get_queryset(self):

        queryset = Lease.objects.all()
        status = self.request.query_params.get('status', None)
        if status is not None:
            queryset = queryset.filter(status=status)
        
        status_in = self.request.query_params.get('status_in', None)
        if status_in is not None:
            queryset = queryset.filter(status__in=status_in.split(','))
        
        amount_from = self.request.query_params.get('amount_from', None)
        amount_to = self.request.query_params.get('amount_to', None)
        if amount_from is not None and amount_to is not None:
            queryset = queryset.filter(amount__range=[amount_from, amount_to])

        sort = self.request.query_params.get('sort', None)
        if sort =='number':
            queryset = queryset.order_by('id')
        if sort =='in_date':
            queryset = queryset.order_by('start_date')

        lease_from = self.request.query_params.get('lease_from', None)
        lease_to = self.request.query_params.get('lease_to', None)
        if lease_from is not None and lease_to is not None:
            queryset = queryset.filter(pk__range=[lease_from, lease_to])

        # search due
        start_date = self.request.query_params.get('start_due', None)
        end_date = self.request.query_params.get('end_due', None)

        if start_date is not None and end_date is not None:
            from_date = dt.strptime(start_date, '%Y-%m-%d').date()
            from_date = datetime.datetime.combine(from_date, datetime.time.min)
            to_date = dt.strptime(end_date, '%Y-%m-%d').date()
            to_date = datetime.datetime.combine(to_date, datetime.time.max)
            queryset = queryset.filter(end_date__range=[from_date, to_date])

        # search out date
        start_date = self.request.query_params.get('start_out_date', None)
        end_date = self.request.query_params.get('end_out_date', None)

        if start_date is not None and end_date is not None:
            from_date = dt.strptime(start_date, '%Y-%m-%d').date()
            from_date = datetime.datetime.combine(from_date, datetime.time.min)
            to_date = dt.strptime(end_date, '%Y-%m-%d').date()
            to_date = datetime.datetime.combine(to_date, datetime.time.max)
            queryset = queryset.filter(out_date__range=[from_date, to_date])

        number = self.request.query_params.get('number', None)
        if number is not None:
            queryset = queryset.filter(number=number)
        else:
            start_date = self.request.query_params.get('start_date', None)
            end_date = self.request.query_params.get('end_date', None)
            if start_date is not None and end_date is not None:
                from_date = dt.strptime(start_date, '%Y-%m-%d').date()
                from_date = datetime.datetime.combine(from_date, datetime.time.min)
                to_date = dt.strptime(end_date, '%Y-%m-%d').date()
                to_date = datetime.datetime.combine(to_date, datetime.time.max)
                queryset = queryset.filter(start_date__range=[from_date, to_date])
            
            due_date = self.request.query_params.get('due_date', None)
            if due_date is not None:
                to_date = dt.strptime(due_date, '%Y-%m-%d').date()
                to_date = datetime.datetime.combine(to_date, datetime.time.max)
                queryset = queryset.filter(end_date__lt=to_date)
            
            start_price = self.request.query_params.get('start_price', None)
            end_price = self.request.query_params.get('end_price', None)
            if start_price is not None and end_price is not None:
                queryset = queryset.filter(amount__range=[start_price, end_price])
                
            branch = self.request.query_params.get('branch', None)
            if branch is not None:
                queryset = queryset.filter(branch=branch)
            
            customer_id= self.request.query_params.get('customer_id', None)
            if customer_id is not None:
                queryset = queryset.filter(customer=customer_id)
            else:
                citizen_id = self.request.query_params.get('citizen_id', None)
                if citizen_id is not None:
                    queryset = queryset.filter(citizen_id=citizen_id)
                
                phone = self.request.query_params.get('phone', None)
                if phone is not None:
                    queryset = queryset.filter(phone=phone)

            is_enabled = self.request.query_params.get('is_enabled', None)
            if is_enabled is not None:
                queryset = queryset.filter(is_enabled=is_enabled)
        return queryset
    permission_classes = (permissions.IsAuthenticated,)

class LeaseUpdateStock(APIView):
    def get(self, request,lease,format=None):
        action = request.query_params.get('action', None)
        if action == 'in':
            b = Lease.objects.get(pk=lease)
            if b is None :
                resp = {'status' : False,'error' : 'ERROR_NOTF'}
                return Response(resp)
            elif b.status_stock == 'Y':
                resp = {'status' : False,'error' : 'ERROR_UPDATED'}
                return Response(resp)

            items = LeaseProduct.objects.filter(lease=lease)
            products = []
            for o in items:
                price = b.amount
                cate = StockCategory.objects.filter(category=o.category,branch=b.branch)
                if len(cate)==0 :
                    cate = StockCategory(category_id=o.category_id,branch_id=b.branch_id,weight=o.weight,total=price)
                    cate.save()
                else:
                    cate = cate[0]
                    cate.weight = cate.weight+o.weight # add stock category
                    cate.int_date = datetime.date.today()
                    cate.total += price
                    cate.save()

                # log stock item
                sp = StockCategoryItem(object_id=b.id,weight=o.weight,category_id=o.category.id,branch=b.branch,user_id=b.user.id,stock_id=cate.id,kind='EJ')
                sp.save()
                o.status_stock = 'Y'
                o.save()
            b.status = 4
            b.status_stock = 'Y'
            b.out_date = datetime.date.today()
            b.save()
        else: # action out
            b = Lease.objects.get(pk=lease)
            if b is None :
                resp = {'status' : False,'error' : 'ERROR_NOTF'}
                return Response(resp)
            elif b.status_stock == 'N':
                resp = {'status' : False,'error' : 'ERROR_UPDATED'}
                return Response(resp)

            items = LeaseProduct.objects.filter(lease=lease)
            products = []
            for o in items:
                price = b.amount
                cate = StockCategory.objects.filter(category=o.category,branch=b.branch)
                if len(cate)==0 :
                    cate = StockCategory(category_id=o.category_id,branch_id=b.branch_id,weight=0,total=0)
                    cate.save()
                else:
                    cate = cate[0]
                    cate.weight = cate.weight-o.weight # add stock category
                    cate.int_date = datetime.date.today()
                    cate.total -= price
                    cate.save()

                # log stock item
                sp = StockCategoryItem(object_id=b.id,weight=o.weight,category_id=o.category.id,branch=b.branch,user_id=b.user.id,stock_id=cate.id,kind='EJ')
                sp.save()
                o.status_stock = 'N'
                o.save()
            b.status_stock = 'N'
            b.status = 1
            b.save()

        resp = {'status' : True,'error' : ''}
        return Response(resp)
    permission_classes = (permissions.IsAuthenticated,IsAuthenticated)

class LeaseOut(APIView):

    def get(self, request,format=None):
        queryset = Lease.objects.all()
        return Response(queryset)
    permission_classes = (permissions.IsAuthenticated,)

class LeaseNextNumber(APIView):

    def get(self, request,format=None):
        branch = self.request.query_params.get('branch', None)
        if branch is not None:
            obj = get_object_or_404(Branch, pk=branch)
            number = Lease.getNextNumber(obj)
        resp = {'number' : number}
        return Response(resp)
    permission_classes = (permissions.IsAuthenticated,)
