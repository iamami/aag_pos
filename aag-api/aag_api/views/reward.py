from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly,IsAuthenticated
from aag_api.serializers import *
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime as dt
import datetime
from rest_framework import status
from aag_api.pagination import LargeResultsSetPagination

class RewardList(generics.ListCreateAPIView):
    pagination_class = LargeResultsSetPagination

    def get_serializer_class(self):
        if self.request.method=='GET':
            return RewardSerializer
        else:
            return RewardCreateSerializer

    def get_queryset(self):
        queryset = Reward.objects.all()

        text = self.request.query_params.get('text', None)
        if text is not None:
            queryset = queryset.filter(Q(code__contains=text) | Q(name__contains=text) | Q(score__contains=text))
        is_enabled = self.request.query_params.get('is_enabled', None)
        if is_enabled is not None:
            queryset = queryset.filter(is_enabled=is_enabled)
        return queryset
    permission_classes = (permissions.IsAuthenticated,)

class RewardDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Reward.objects.all()
    serializer_class = RewardSerializer
    permission_classes = (permissions.IsAuthenticated,)
    

    def delete(self, request,pk,**kwargs):
        
        # check in bill
        if StockReward.objects.all().filter(reward=pk).count() > 0 :
            content = {'error': 'ไม่สามารถลบพนักงานได้ เนื่องจากถูกใช้งานแล้ว'}
            return Response(content, status=status.HTTP_403_FORBIDDEN)

        return self.destroy(request, pk, **kwargs)
