from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly,IsAuthenticated
from aag_api.serializers import *
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime as dt
import datetime

class BranchSettingList(generics.ListCreateAPIView):
   
    serializer_class = BranchSettingSerializer

    def get_queryset(self):
        queryset = BranchSetting.objects.all()
        branch = self.request.query_params.get('branch', None)
        if branch is not None:
            queryset = queryset.filter(branch=branch)
        return queryset

    permission_classes = (permissions.IsAuthenticated,)

class BranchSettingDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = BranchSetting.objects.all()
    serializer_class = BranchSettingSerializer
    permission_classes = (permissions.IsAuthenticated,)