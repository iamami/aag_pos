from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly,IsAuthenticated
from aag_api.serializers import *
from aag_api.models.score import *
from aag_api.models.saving import *
from aag_api.models.gold_price import *
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime as dt
import datetime
from aag_api.pagination import LargeResultsSetPagination
from django.shortcuts import get_object_or_404
from decimal import Decimal

class SavingItemList(generics.ListCreateAPIView):

    def get_serializer_class(self):
        if self.request.method=='GET':
            return SavingItemFullSerializer
        return SavingItemCreateSerializer
    
    def get_queryset(self):
        queryset = SavingItem.objects.all().filter(saving=self.kwargs['saving']).order_by('-record_date')
        return queryset
    
    def perform_create(self, serializer):
        saving = get_object_or_404(Saving,pk=self.kwargs['saving'])
        gold = GoldPrice.objects.all().order_by('-id')[0]
        kind = serializer.validated_data['kind']
        gold_bar_sell = Decimal(gold.gold_bar_sell)
        price = Decimal(serializer.validated_data['price'])

        print(gold_bar_sell)
  
        weight = (price/gold_bar_sell) * Decimal(15.2)
        befor_weight = saving.weight
        befor_price = saving.price
        if kind is 'I':
            after_weight = saving.weight + weight
            after_price = saving.price + price
            saving.weight = after_weight
            saving.price = after_price
        else:
            after_weight = saving.weight - weight
            after_price = saving.price - price
            saving.weight = after_weight
            saving.price = after_price

        serializer.save(saving=saving,user=self.request.user,gold=gold_bar_sell,weight=weight,befor_weight=befor_weight,befor_price=befor_price,after_weight=after_weight,after_price=after_price)
        saving.save()
    permission_classes = (IsAuthenticated,)
    pagination_class = LargeResultsSetPagination

class SavingItemDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = SavingItem.objects.all()
    def get_serializer_class(self):
        if self.request.method=='GET':
            return SavingItemFullSerializer
        return SavingItemSerializer
    permission_classes = (IsAuthenticated,)