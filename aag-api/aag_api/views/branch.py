from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly,IsAuthenticated
from aag_api.serializers import *
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime as dt
import datetime
from aag_api.models import *
from rest_framework import status

class BranchList(generics.ListCreateAPIView):

    serializer_class = BranchSerializer
    def get_queryset(self):
        queryset = Branch.objects.all().order_by('id')
        is_enabled = self.request.query_params.get('is_enabled', 1)
        if is_enabled is not None:
            queryset = queryset.filter(is_enabled=1)
        return queryset
    permission_classes = (permissions.IsAuthenticated,)

class BranchDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Branch.objects.all()
    serializer_class = BranchSerializer

    def put(self, request, pk, **kwargs):
        # delete branch
        if 'is_enabled' in request.data and request.data['is_enabled'] == '0' :
            staffs = Staff.objects.all().filter(branch=pk)
            for staff in staffs :
                staff.is_enabled = 0
                staff.save()
            user_profiles = UserProfile.objects.all().filter(branch=pk)
            for up in user_profiles :
                up.user.is_active = False
                up.user.save()
        else:
            staffs = Staff.objects.all()
            staffs = staffs.filter(pk=pk)

        return self.update(request, pk, **kwargs)

    def delete(self, request,pk,**kwargs):
        
        # check in bill
        if Staff.objects.all().filter(branch=pk).count() or Bill.objects.all().filter(branch=pk).count() > 0 or Invoice.objects.all().filter(branch=pk).count()>0 or Lease.objects.all().filter(branch=pk).count()>0 or Ledger.objects.all().filter(branch=pk).count()>0 or Bill.objects.all().filter(branch=pk).count()>0:
            content = {'error': 'ไม่สามารถลบสาขาได้ เนื่องจากถูกใช้งานแล้ว'}
            return Response(content, status=status.HTTP_403_FORBIDDEN)

        return self.destroy(request, pk, **kwargs)

    permission_classes = (permissions.IsAuthenticated,)