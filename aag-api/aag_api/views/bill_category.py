from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly,IsAuthenticated
from aag_api.serializers import *
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime as dt
import datetime
from rest_framework import serializers


class BillCategoryList(generics.ListCreateAPIView):
    
    def get_serializer_class(self):
        if self.request.method=='GET':
            return BillCategorySerializer
        return BillCategorySerializer
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
    def get_queryset(self):
        queryset = BillCategory.objects.all()
        start_date = self.request.query_params.get('start_date', None)
        end_date = self.request.query_params.get('end_date', None)
        if start_date is not None and end_date is not None:
            queryset = queryset.filter(bill_date__range=[start_date, end_date])
        
        branch_id = self.request.query_params.get('branch_id', None)
        if branch_id is not None:
            queryset = queryset.filter(branch=branch_id)
        status_stock = self.request.query_params.get('status_stock', None)
        if status_stock is not None:
            queryset = queryset.filter(status_stock=status_stock)
        status_bill = self.request.query_params.get('status_bill', None)
        if status_bill is not None:
            queryset = queryset.filter(status_bill=status_bill)

        return queryset
    permission_classes = (permissions.IsAuthenticated,)

class BillCategoryDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = BillCategory.objects.all()
    serializer_class = BillCategorySerializer
    permission_classes = (permissions.IsAuthenticated,)

class BillCategoryItemList(generics.ListCreateAPIView):

    def get_queryset(self):

        queryset = BillCategoryItem.objects.all()

        bill_category = self.kwargs.get("bill_category")
        if bill_category is not None and bill_category != '0':
            queryset = queryset.filter(bill_category=bill_category)
        
        status_stock = self.request.query_params.get('status_stock', None)
        if status_stock is not None:
            queryset = queryset.filter(status_stock=status_stock)
        
        start_date = self.request.query_params.get('start_date', None)
        end_date = self.request.query_params.get('end_date', None)
        if start_date is not None and end_date is not None:
            from_date = dt.strptime(start_date, '%Y-%m-%d').date()
            from_date = datetime.datetime.combine(from_date, datetime.time.min)
            to_date = dt.strptime(end_date, '%Y-%m-%d').date()
            to_date = datetime.datetime.combine(to_date, datetime.time.max)
            queryset = queryset.filter(update_date__range=[from_date, to_date])
        
        return queryset
    def get_serializer_class(self):
        if self.request.method=='GET':
            return BillCategoryItemFullSerializer
        return BillCategoryItemSerializer
    def perform_create(self, serializer):
        bill_category = self.kwargs.get("bill_category")
        stock = serializer.validated_data['stock']
        if BillCategoryItem.objects.all().filter(bill_category=bill_category,stock=stock).count() > 0:
            raise serializers.ValidationError({'error': 'ไม่สามารถเพิ่มสินค้าได้อีกครั้ง'})
        serializer.save()
    permission_classes = (permissions.IsAuthenticated,)

class BillCategoryItemDetail(generics.RetrieveUpdateDestroyAPIView):
    def get_queryset(self):
        queryset = BillCategoryItem.objects.filter(bill_category=self.kwargs.get("bill_category"))
        return queryset
    serializer_class = BillCategoryItemSerializer
    permission_classes = (permissions.IsAuthenticated,)

class BillCategoryStock(APIView):

    def get(self, request,bill_category,format=None):
        inv = BillCategory.objects.get(pk=bill_category)
        total_w = 0
        total = 0
        cost = 0
        if inv is None :
            resp = {'status' : False,'error' : 'ERROR_NOTF'}
            return Response(resp)
        elif inv.status_stock == 'Y':
            resp = {'status' : False,'error' : 'ERROR_UPDATED'}
            return Response(resp)

        items = BillCategoryItem.objects.filter(bill_category=inv.id)

        products = []
        for o in items:
            cate = StockCategory.objects.filter(pk=o.stock.id,branch=inv.branch)
            if len(cate)==0 :
                cate = StockCategory(category_id=o.category_id,branch_id=inv.branch,weight=o.weight,total=o.total)
                cate.save()
            else:
                cate = cate[0]
                cate.weight = cate.weight - o.weight # cut stock product
                if cate.weight < 0:
                    cate.weight = 0
                    
                cate.in_date = datetime.date.today()
                cate.total = cate.total - o.total_average
                if cate.total < 0:
                    cate.total = 0
                
                cate.save()
            total_w +=  o.weight
            cost += o.cost
            total += o.total
            # log stock item
            sp = StockCategoryItem(object_id=inv.id,weight=o.weight,category=o.stock.category,branch=o.branch,user=request.auth.user,stock_id=cate.id)
            sp.save()
            o.status_stock = 'Y'
            o.update_date = datetime.date.today()
            o.save()
        inv.weight = total_w
        inv.total = total-cost
        inv.status_stock = 'Y'
        inv.update_date = datetime.date.today()
        inv.save()

        resp = {'status' : True,'error' : ''}
        return Response(resp)
    permission_classes = (permissions.IsAuthenticated,)
