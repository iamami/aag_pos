from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly,IsAuthenticated
from aag_api.serializers import *
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime as dt
import datetime
from rest_framework import status


class BankCardList(generics.ListCreateAPIView):

    def get_queryset(self):
        queryset = BankCard.objects.all()
               
        is_enabled = self.request.query_params.get('is_enabled', None)
        if is_enabled is not None:
            queryset = queryset.filter(is_enabled=is_enabled)

        return queryset
    def get_serializer_class(self):
        if self.request.method=='GET':
            return BankCardFullSerializer
        return BankCardSerializer
    permission_classes = (permissions.IsAuthenticated,)

class BankCardDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = BankCard.objects.all()
    serializer_class = BankCardSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def delete(self, request,pk,**kwargs):
        
        # check in bill
        if Ledger.objects.all().filter(card_bank_card=pk).count() > 0:
            content = {'error': 'ไม่สามารถลบได้ เนื่องจากถูกใช้งานแล้ว'}
            return Response(content, status=status.HTTP_403_FORBIDDEN)

        return self.destroy(request, pk, **kwargs)