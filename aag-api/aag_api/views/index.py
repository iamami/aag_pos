from django.shortcuts import render
from django.conf import settings
def IndexView(request):
    return render(request, 'aag_api/index.html', {
        'API_VERSION': settings.API_VERSION
    })