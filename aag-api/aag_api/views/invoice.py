from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly
from aag_api.serializers import *
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime as dt
import datetime
from rest_framework import status

class InvoiceUpdateStock(APIView):

    def get(self, request,invoice,format=None):

        user = request.auth.user
        inv = Invoice.objects.get(pk=invoice)
        if inv is None :
            resp = {'status' : False,'error' : 'ERROR_NOTF'}
            return Response(resp)
        elif inv.status_stock == 'Y':
            resp = {'status' : False,'error' : 'ERROR_UPDATED'}
            return Response(resp)

        items = InvoiceItem.objects.filter(invoice=inv.id)

        products = []
        for o in items:

            if o.status_stock == 'Y':
                continue
            prod = StockProduct.objects.filter(product_id=o.product.id,branch_id=inv.branch_id)

            before = 0
            after = 0
            # update stock counter
            if len(prod)==0 :
                if inv.kind == 'EX' or inv.kind == 'MV':
                    resp = {'error' : 'พบสินค้าในสต็อก (%s)' % ( o.product)}
                    return Response(resp,status.HTTP_400_BAD_REQUEST)

                    prod = StockProduct(product_id=o.product_id,branch_id=inv.branch_id,amount=-o.amount)
                    after = -o.amount
                else:
                    prod = StockProduct(product_id=o.product_id,branch_id=inv.branch_id,amount=o.amount)
                    after = o.amount
                prod.save()
            else:
                prod = prod[0]
                before = prod.amount

                # kind invoice
                if inv.kind == 'EX' or inv.kind == 'MV':
                    prod.amount = prod.amount-o.amount

                    if prod.amount < 0 :
                         resp = {'error' : 'สินค้าในสต็อกบางรายการไม่เพียงพอ "%s"' % ( o.product)}
                         return Response(resp,status.HTTP_400_BAD_REQUEST)
                else:
                    prod.amount = prod.amount+o.amount
                after = prod.amount
                prod.in_date = datetime.date.today()
                prod.save()
            
            k = 'I'
            if inv.kind == 'EX':
                k = 'O'
            if inv.kind == 'MV':
                k = 'TO'

            # log stock item
            sp = StockProductItem(object_number=inv.number,object_id=inv.id,weight=o.weight,weight_total=o.weight_total,weight_real=o.weight_real,amount=o.amount,branch=inv.branch,branch_to=inv.branch_to,branch_from=inv.branch.id,product=o.product,user=user,stock_id=prod.id,kind=k,before=before,after=after)
            sp.save()
            o.status_stock = 'Y'
            o.save()

            # Transfer in
            if inv.kind == 'MV':
                prod = StockProduct.objects.filter(product_id=o.product_id,branch_id=inv.branch_to)
                before = 0
                after = 0
                # update stock counter
                if len(prod)==0 :
                    prod = StockProduct(product_id=o.product_id,branch_id=inv.branch_to,amount=o.amount)
                    after = o.amount
                    prod.save()
                else:
                    prod = prod[0]
                    before = prod.amount
                    prod.amount = prod.amount+o.amount
                    after = prod.amount
                    prod.in_date = datetime.date.today()
                    prod.save()
                
                k = 'TI'

                # log stock item
                branchto = Branch.objects.get(pk=inv.branch_to)
                sp = StockProductItem(object_number=inv.number,object_id=inv.id,weight=o.weight,weight_total=o.weight_total,weight_real=o.weight_real,amount=o.amount,branch=branchto,branch_to=inv.branch_to,branch_from=inv.branch.id,product=o.product,user=user,stock_id=prod.id,kind=k,before=before,after=after)
                sp.save()
                o.status_stock = 'Y'
                o.save()
            # end Transfer
        inv.status_stock = 'Y'
        inv.save()

        resp = {'status' : True,'error' : ''}
        return Response(resp)
    permission_classes = (permissions.IsAuthenticated,)

class InvoiceItemViewList(generics.ListCreateAPIView):

    def get_queryset(self):
        inv = self.kwargs.get("invoice")
        if inv != '0':
            queryset = InvoiceItem.objects.filter(invoice=inv)
        else:
            queryset = InvoiceItem.objects.all()
        
        status_stock = self.request.query_params.get('status_stock', None)
        if status_stock is not None:
            queryset = queryset.filter(status_stock=status_stock)

        start_date = self.request.query_params.get('start_date', None)
        end_date = self.request.query_params.get('end_date', None)
        if start_date is not None and end_date is not None:
            from_date = dt.strptime(start_date, '%Y-%m-%d').date()
            from_date = datetime.datetime.combine(from_date, datetime.time.min)
            to_date = dt.strptime(end_date, '%Y-%m-%d').date()
            to_date = datetime.datetime.combine(to_date, datetime.time.max)
            queryset = queryset.filter(record_date__range=[from_date, to_date])
        return queryset
    serializer_class = InvoiceItemViewSerializer
    permission_classes = (permissions.IsAuthenticated,)


class InvoiceList(generics.ListCreateAPIView):

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def get_queryset(self):
        queryset = Invoice.objects.all()
        is_enabled = self.request.query_params.get('is_enabled', None)
        if is_enabled is not None:
            queryset = queryset.filter(is_enabled=is_enabled)
        branch_id = self.request.query_params.get('branch_id', None)
        if branch_id is not None:
            queryset = queryset.filter(branch=branch_id)

        vendor = self.request.query_params.get('vendor', None)
        if vendor is not None:
            queryset = queryset.filter(vendor=vendor)
        
        branch_to = self.request.query_params.get('branch_to', None)
        if branch_to is not None:
            queryset = queryset.filter(branch_to=branch_to)
        
        kind = self.request.query_params.get('kind', None)
        if kind is not None:
            queryset = queryset.filter(kind=kind)

        status_bill = self.request.query_params.get('status_bill', None)
        if status_bill is not None:
            queryset = queryset.filter(status_bill=status_bill)

        status_stock = self.request.query_params.get('status_stock', None)
        if status_stock is not None:
            queryset = queryset.filter(status_stock=status_stock)

        start_date = self.request.query_params.get('start_date', None)
        end_date = self.request.query_params.get('end_date', None)
        if start_date is not None and end_date is not None:
            queryset = queryset.filter(invoice_date__range=[start_date, end_date])
        return queryset
    serializer_class = InvoiceSerializer
    permission_classes = (permissions.IsAuthenticated,)

class InvoiceDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer
    permission_classes = (permissions.IsAuthenticated,)

class InvoiceItemList(generics.ListCreateAPIView):
    queryset = InvoiceItem.objects.all()
    serializer_class = InvoiceItemSerializer
    permission_classes = (permissions.IsAuthenticated,)

class InvoiceItemDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = InvoiceItem.objects.all()
    serializer_class = InvoiceItemSerializer
    permission_classes = (permissions.IsAuthenticated,)