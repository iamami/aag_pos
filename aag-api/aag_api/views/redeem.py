from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly,IsAuthenticated
from aag_api.serializers import *
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime as dt
import datetime
from rest_framework import status
from aag_api.pagination import LargeResultsSetPagination
from mapi.models.device import Device

class RedeemList(generics.ListCreateAPIView):
    pagination_class = LargeResultsSetPagination

    def get_serializer_class(self):
        if self.request.method=='GET':
            return RedeemFullSerializer
        else:
            return RedeemCreateSerializer
    
    
    def perform_create(self, serializer):
        
        request = self.request
        if 'staff' not in serializer.validated_data:
            raise serializers.ValidationError({'staff': 'กรุณาเลือกพนักงาน'})

        # check score
        customer = serializer.validated_data['customer']
        score = Score.objects.get(customer=customer)
        
        reward = serializer.validated_data['reward']        
        kind = serializer.validated_data['kind']
        amount = serializer.validated_data['amount']
        score_need = reward.score * amount

        status = serializer.validated_data['status']   
        
        if kind == '1': # สาขา
            branch = serializer.validated_data['branch']


        # check stock item
        stock_reward = None        
        try:
            stock_reward = StockReward.objects.get(reward=reward.pk)
        except StockReward.DoesNotExist:
            stock_reward = StockReward.objects.create(reward=reward,amount=0,updated_datetime=dt.today())

        if stock_reward.amount < amount:
            raise serializers.ValidationError({'error': 'สินค้าไม่เพียงพอ'})
        
        if score_need > score.total:
            raise serializers.ValidationError({'error': 'คะแนนไม่เพียงพอสำหรับแรกของรางวัล'})

        if kind == '1' and 'branch' not in serializer.validated_data and 'receipt_date' not in serializer.validated_data :
            raise serializers.ValidationError({'branch': 'กรุณาเลือกสาขา','receipt_date': 'กรุณาเลือกวันที่รับสินค้า'})

        elif kind == '1' and 'branch' not in serializer.validated_data :
            raise serializers.ValidationError({'branch': 'กรุณาเลือกสาขา'})

        elif kind == '1' and 'receipt_date' not in serializer.validated_data :
            raise serializers.ValidationError({'receipt_date': 'กรุณาเลือกวันที่รับสินค้า'})
        
        if kind == '2' and 'address' not in serializer.validated_data :
            raise serializers.ValidationError({'address': 'กรุณาระบุที่อยู่จัดส่ง'})
        
        if kind == '1' and serializer.validated_data['receipt_date'] < date.today() :
            raise serializers.ValidationError({'receipt_date': 'ไม่สามารถเลือกวันที่ย้อนหลังได้'})
        
        
        redeem = serializer.save(customer=customer,status=status)

        device_list = Device.objects.all().filter(customer_user__customer=redeem.customer.pk)
        
        title = "สถานะของรางวัลของคุณคือ %s " % (redeem.get_status_display())
        message = "ของรางวัล %s " % (redeem.reward.name)
        res = Device.sendNotiRedeem(device_list,title,message,{
            'type': 4,
            'data': {
                'id' : redeem.id,
                'reward': str(redeem.reward),
                'status': redeem.status,
                'status_display': redeem.get_status_display(),
                'kind': redeem.kind,
                'kind_display': redeem.get_kind_display()
            }
        },redeem)
        print(res)
        print(title)
        print(message)

        # decrease score
        ScoreItem.saveRedeem(customer,redeem,score_need)

        # update stock
        amount = redeem.amount
        before = stock_reward.amount
        stock_reward.amount = stock_reward.amount-redeem.amount
        stock_reward.save()
        after = stock_reward.amount
        print(stock_reward.amount)

        #add log stock reward
        StockRewardItem.objects.create(stock_reward=stock_reward,amount=amount,before=before,after=after,kind = "E",user=self.request.user,redeem=redeem)

        redeem.status_stock = 'Y'
        redeem.save()


    def get_queryset(self):
        queryset = Redeem.objects.all().order_by('-id')

        text = self.request.query_params.get('text', None)
        if text is not None:
            queryset = queryset.filter(Q(code__contains=text) | Q(reward__name__contains=text) | Q(reward__code__contains=text) | Q(customer__code__contains=text) | Q(customer__name__contains=text))
        status = self.request.query_params.get('status', None)
        if status is not None:
            queryset = queryset.filter(status=status)
        kind = self.request.query_params.get('kind', None)
        if kind is not None:
            queryset = queryset.filter(kind=kind)
        return queryset
    permission_classes = (permissions.IsAuthenticated,)

class RedeemDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Redeem.objects.all()
    serializer_class = RedeemSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def perform_update(self,serializer):

        status = serializer.validated_data['status'] 
        reward = serializer.validated_data['reward']
        kind = serializer.validated_data['kind']

        if kind == '1': # สาขา
            branch = serializer.validated_data['branch']


        stock_reward = None        
        try:
            stock_reward = StockReward.objects.get(reward=reward.pk)
        except StockReward.DoesNotExist:
            stock_reward = StockReward.objects.create(reward=reward,amount=0,updated_datetime=dt.today())

        if stock_reward.amount <= 0:
            raise serializers.ValidationError({'error': 'สินค้าหมด'})       
            
        instance = serializer.save()
        device_list = Device.objects.all().filter(customer_user__customer=instance.customer.pk)
        
        title = "สถานะของรางวัลของคุณคือ %s " % (instance.get_status_display())
        message = "ของรางวัล %s " % (instance.reward.name)
        res = Device.sendNotiRedeem(device_list,title,message,{
            'type': 4,
            'data': {
                'id' : instance.id,
                'reward': str(instance.reward),
                'status': instance.status,
                'status_display': instance.get_status_display(),
                'kind': instance.kind,
                'kind_display': instance.get_kind_display()
            }
        },instance)
        print(res)
        print(title)
        print(message)


    # def delete(self, request,pk,**kwargs):
        
    #     # check in bill
    #     if StockRedeem.objects.all().filter(redeem=pk).count() > 0 :
    #         content = {'error': 'ไม่สามารถลบพนักงานได้ เนื่องจากถูกใช้งานแล้ว'}
    #         return Response(content, status=status.HTTP_403_FORBIDDEN)

    #     return self.destroy(request, pk, **kwargs)
