from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly,IsAuthenticated
from aag_api.serializers import *
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime as dt
import datetime
from rest_framework import status
from django.shortcuts import get_object_or_404

class StaffList(generics.ListCreateAPIView):
    #queryset = Staff.objects.all()
    #serializer_class = StaffSerializer
    def get_serializer_class(self):
        if self.request.method=='GET':
            return StaffFullSerializer
        return StaffSerializer
    def get_queryset(self):
        queryset = Staff.objects.all()
        branch = self.request.query_params.get('branch', None)
        if branch is not None and branch != '':
            queryset = queryset.filter(branch=branch)

        staff_id = self.request.query_params.get('staff_id', None)
        if staff_id is not None and staff_id != '':
            queryset = queryset.filter(pk=staff_id)
        
        text = self.request.query_params.get('text', None)
        if text is not None and text != '':
            queryset = queryset.filter(Q(code__contains=text) | Q(name__contains=text))

        is_enabled = self.request.query_params.get('is_enabled', None)
        if is_enabled is not None:
            queryset = queryset.filter(is_enabled=1)
        return queryset
    permission_classes = (permissions.IsAuthenticated,)

class StaffDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Staff.objects.all()
    serializer_class = StaffSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def delete(self, request,pk,**kwargs):
        
        # check in bill
        if BillStaff.objects.all().filter(staff=pk).count() > 0 or Lease.objects.all().filter(staff=pk).count()>0 or Ledger.objects.all().filter(staff=pk).count()>0:
            content = {'error': 'ไม่สามารถลบพนักงานได้ เนื่องจากถูกใช้งานแล้ว'}
            return Response(content, status=status.HTTP_403_FORBIDDEN)

        return self.destroy(request, pk, **kwargs)

class StaffNextNumber(APIView):

    def get(self, request,format=None):
        branch = self.request.query_params.get('branch', None)
        if branch is not None:
            obj = get_object_or_404(Branch, pk=branch)
            number = Staff.getNextNumber(obj)
        resp = {'number' : number}
        return Response(resp)
    permission_classes = (permissions.IsAuthenticated,)