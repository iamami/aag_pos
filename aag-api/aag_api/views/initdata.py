from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
import subprocess
import requests
import json

class ClearData(APIView):

    def get(self, request, format=None):
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "auth"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "oauth2_provider"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/group.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/user.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/oauth2_provider.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/branch.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/staff.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/user_profile.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/system_setting.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/branch_setting_2.json"])
        return Response({'data': 1})

class InitDataV1(APIView):

    def get(self, request, format=None):
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "auth"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "oauth2_provider"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/group.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/user.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/oauth2_provider.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/counter.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/branch.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/staff.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/user_profile.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/category.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/producttype.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/product.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/vendor.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/bank.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/bank_card.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/bank_account.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/ledger_category.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/system_setting.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/stockproduct.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/goldprice.json"])
        # subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/amphures.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/districts.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/zipcodes.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/provinces.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/branch_setting_2.json"])

        return Response({'data': 1})

class InitDataV1_Admin(APIView):

    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "auth"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "oauth2_provider"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/group.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/user.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/oauth2_provider.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/counter.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/branch.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/staff.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/user_profile.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/category.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/producttype.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/product.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/vendor.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/bank.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/bank_card.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/bank_account.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/ledger_category.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/system_setting.json"]))
        # subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/stockproduct.json"])
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/goldprice.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_search.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/amphures.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/districts.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/zipcodes.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/provinces.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/branch_setting_2.json"]))


        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_stock_product_admin.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_stock_product_item_admin.json"]))
        
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_bill.json"])) 
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_bill_item.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_bill_staff.json"]))         

        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_stock_catrgory_admin.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_stock_catrgory_item_admin.json"]))


        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_bill_category.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_bill_category_item.json"]))
        
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_ledger.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_lease.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_lease_interest.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_lease_principle.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_lease_product.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_product_name.json"]))

        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_score.json"]))        
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_score_item.json"]))
        
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_invoice.json"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_invoice_item.json"]))

        return Response({'data': 1})


class InitDataV1_Admin_Report(APIView):

    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "auth"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "oauth2_provider"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/aagold-pos-live-20191018-09-46.json"]))
        return Response({'data': 1})

class ClearSettingCustomer(APIView):
    def get(self, request, format=None):
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","systemsetting"])  
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/system_setting.json"])
        return Response({'data': 1})

class SearchCustomer(APIView):
    def get(self, request, format=None):
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","customer"])  
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","counter"])  
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/counter.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_search.json"])
        return Response({'data': 1})

class InfoCustomer(APIView):
    def get(self, request, format=None):
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","billstaff"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","billitem"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","bill"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","scoreitem"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","score"])    
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","leaseinterest"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","leaseprinciple"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","leaseproduct"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","lease"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","customer"])  
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","ledger"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","ledgercategory"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","branchsetting"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","counter"])
 
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","stockproductitem"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","stockproduct"])

        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","productname"])

        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/counter.json"]) 
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/branch_setting_2.json"])

        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/ledger_category.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_search.json"])

        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_stock_product.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_stock_product_item.json"])

        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_bill.json"])        
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_bill_item.json"])  
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_bill_staff.json"])
        
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_ledger.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_lease.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_lease_interest.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_lease_principle.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_lease_product.json"])

        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_product_name.json"])        
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_score.json"])        
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_score_item.json"])

        return Response({'data': 1})

class PointCustomer(APIView):
    def get(self, request, format=None):
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","scoreitem"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","score"])    
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","customer"]) 
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","ledger"]) 
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_search.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_score.json"])        
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_score_item.json"])
        return Response({'data': 1})

class BranchSetting(APIView):
    def get(self, request, format=None):
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","branchsetting"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/branch_setting_2.json"])
        return Response({'data': 1})

class Branch(APIView):
    def get(self, request, format=None):
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","counter"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","branch"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/branch.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/counterjson"])
        return Response({'data': 1})

class Lease(APIView):
    def get(self, request, format=None):    

        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_lease.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_lease_interest.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_lease_principle.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_lease_product.json"])
        return Response({'data': 1})

class Bill(APIView):
    def get(self, request, format=None):    
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_bill.json"])        
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_bill_item.json"])  
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_bill_staff.json"])
        return Response({'data': 1})

class Ledger(APIView):
    def get(self, request, format=None):    
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_ledger.json"])
        return Response({'data': 1})

class Score(APIView):
    def get(self, request, format=None):    
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_product_name.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_score.json"])        
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_score_item.json"])
        return Response({'data': 1})

class Product(APIView):
    def get(self, request, format=None):    
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_stock_product_item.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_stock_product.json"])
        return Response({'data': 1})

class ClearInfoCustomer(APIView):
    def get(self, request, format=None):
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","billstaff"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","billitem"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","bill"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","scoreitem"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","score"])    
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","leaseinterest"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","leaseprinciple"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","leaseproduct"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","lease"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","customer"])  
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","ledger"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","ledgercategory"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","branchsetting"])

        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","productname"])  
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","stockproductitem"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","stockproduct"])
        return Response({'data': 1})

class InfoCustomerETC(APIView):
    def get(self, request, format=None):
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/counter.json"]) 
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/branch_setting_2.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/ledger_category.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_search.json"])
        return Response({'data': 1})


class V11(APIView):

    def get(self, request, format=None):
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "auth"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "oauth2_provider"])
        return Response({'data': 1})

class V12(APIView):

    def get(self, request, format=None):
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/group.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/user.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/oauth2_provider.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/branch.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/staff.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/user_profile.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/category.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/producttype.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/product.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/vendor.json"])
        return Response({'data': 1})

class V13(APIView):

    def get(self, request, format=None):
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/bank.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/bank_card.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/bank_account.json"])
        return Response({'data': 1})

class V14(APIView):

    def get(self, request, format=None):
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/system_setting.json"])
        return Response({'data': 1})

class V15(APIView):

    def get(self, request, format=None):
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/stockproduct.json"])
        return Response({'data': 1})

class V18(APIView):

    def get(self, request, format=None):
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/goldprice.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/amphures.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/districts.json"])
        return Response({'data': 1})


class V19(APIView):

    def get(self, request, format=None):
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/zipcodes.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/provinces.json"])
        return Response({'data': 1})



class M1(APIView):

    def get(self, request, format=None):
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "auth"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "oauth2_provider"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "initdata/mapi.json"])
        return Response({'data': 1})

class M2(APIView):

    def get(self, request, format=None):
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "initdata/group.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures/user.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "initdata/oauth2_provider.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "initdata/branch.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "initdata/staff.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "initdata/user_profile.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "initdata/category.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "initdata/producttype.json"])
        return Response({'data': 1})

class M3(APIView):

    def get(self, request, format=None):
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "initdata/product.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "initdata/vendor.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "initdata/bank.json"])
        return Response({'data': 1})

class M4(APIView):

    def get(self, request, format=None):
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "initdata/bank_card.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "initdata/bank_account.json"])
        return Response({'data': 1})

class M5(APIView):

    def get(self, request, format=None):
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "initdata/system_setting.json"])
        return Response({'data': 1})

class M6(APIView):

    def get(self, request, format=None):
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "initdata/stockproduct.json"])
        return Response({'data': 1})


class M7(APIView):

    def get(self, request, format=None):
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "initdata/zipcodes.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "initdata/goldprice.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "initdata/amphures.json"])
        return Response({'data': 1})

class M7(APIView):

    def get(self, request, format=None):
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "initdata/districts.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "initdata/provinces.json"])
        return Response({'data': 1})

class M8(APIView):

    def get(self, request, format=None):
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "initdata/zipcodes.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "initdata/provinces.json"])
        return Response({'data': 1})

class M9(APIView):

    def get(self, request, format=None):
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "initdata/auth.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "initdata/branch_setting2.json"])
        return Response({'data': 1})

class M10(APIView):

    def get(self, request, format=None):
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "initdata/counter.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "initdata/customer.json"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "initdata/ledger_category.json"])
        return Response({'data': 1})

class ClearStaff_Product(APIView):

    def get(self, request, format=None):
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","staff"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","productname"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","product"])
        subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api","--models","stockproduct"])
        # subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_search.json"])
        # subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_bill.json"])        
        # subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_bill_item.json"])  
        # subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot/customer_bill_staff.json"])
        return Response({'data': 1})


class Admin_1(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "auth"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "oauth2_provider"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/group.json"]))
        return Response({'data': 1})

class Admin_2(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/user.json"]))
        return Response({'data': 1})
class Admin_3(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/oauth2_provider.json"]))
        return Response({'data': 1})
class Admin_4(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/counter.json"]))
        return Response({'data': 1})
class Admin_5(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/branch.json"]))
        return Response({'data': 1})
class Admin_6(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/staff.json"]))
        return Response({'data': 1})
class Admin_7(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/user_profile.json"]))
        return Response({'data': 1})
class Admin_8(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/category.json"]))
        return Response({'data': 1})
class Admin_9(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/producttype.json"]))
        return Response({'data': 1})
class Admin_10(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/product.json"]))
        return Response({'data': 1})
class Admin_11(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/vendor.json"]))
        return Response({'data': 1})
class Admin_12(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/bank.json"]))
        return Response({'data': 1})
class Admin_13(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/bank_card.json"]))
        return Response({'data': 1})
class Admin_14(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/bank_account.json"]))
        return Response({'data': 1})
class Admin_15(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/ledger_category.json"]))
        return Response({'data': 1})
class Admin_16(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/system_setting.json"]))
        return Response({'data': 1})
class Admin_17(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/goldprice.json"]))
        return Response({'data': 1})
class Admin_18(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_search.json"]))
        return Response({'data': 1})
class Admin_19(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/amphures.json"]))
        return Response({'data': 1})
class Admin_20(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/districts.json"]))
        return Response({'data': 1})
class Admin_21(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/zipcodes.json"]))
        return Response({'data': 1})
class Admin_22(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/provinces.json"]))
        return Response({'data': 1})
class Admin_23(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/branch_setting_2.json"]))
        return Response({'data': 1})
class Admin_24(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_stock_product_admin.json"]))
        return Response({'data': 1})
class Admin_25(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_stock_product_item_admin.json"]))
        return Response({'data': 1})
class Admin_26(APIView):
    def get(self, request, format=None):   
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_product_name.json"]))
        return Response({'data': 1})
class Admin_27(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_bill.json"])) 
        return Response({'data': 1})
class Admin_28(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_bill_item.json"]))
        return Response({'data': 1})
class Admin_29(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_bill_staff.json"]))         
        return Response({'data': 1})
class Admin_30(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_stock_catrgory_admin.json"]))
        return Response({'data': 1})
class Admin_31(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_stock_catrgory_item_admin.json"]))
        return Response({'data': 1})
class Admin_32(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_bill_category.json"]))
        return Response({'data': 1})
class Admin_33(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_bill_category_item.json"]))
class Admin_34(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_ledger.json"]))
        return Response({'data': 1})
class Admin_35(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_lease.json"]))
        return Response({'data': 1})
class Admin_36(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_lease_interest.json"]))
        return Response({'data': 1})
class Admin_37(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_lease_principle.json"]))
        return Response({'data': 1})
class Admin_38(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_lease_product.json"]))
        return Response({'data': 1})
class Admin_39(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_score.json"]))        
        return Response({'data': 1})
class Admin_40(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_score_item.json"]))
        return Response({'data': 1})
class Admin_41(APIView):
    def get(self, request, format=None):     
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_invoice.json"]))
        return Response({'data': 1})
class Admin_42(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_robot_admin/customer_invoice_item.json"]))
        return Response({'data': 1})


class Report_1(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "aag_api"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "auth"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "truncate", "--apps", "oauth2_provider"]))
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_group.json"]))
        return Response({'data': 1})

class Report_2(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_user.json"]))
        return Response({'data': 1})
class Report_3(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/oauth2_provider.json"]))
        return Response({'data': 1})
class Report_4(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_counter.json"]))
        return Response({'data': 1})
class Report_5(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_branch.json"]))
        return Response({'data': 1})
class Report_6(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_staff.json"]))
        return Response({'data': 1})
class Report_7(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_user_profile.json"]))
        return Response({'data': 1})
class Report_8(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_category.json"]))
        return Response({'data': 1})
class Report_9(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_product_type.json"]))
        return Response({'data': 1})
class Report_10(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_product.json"]))
        return Response({'data': 1})
class Report_11(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_vendor.json"]))
        return Response({'data': 1})
class Report_12(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_bank.json"]))
        return Response({'data': 1})
class Report_13(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_bank_card.json"]))
        return Response({'data': 1})
class Report_14(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_bank_account.json"]))
        return Response({'data': 1})
class Report_15(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_ledger_category.json"]))
        return Response({'data': 1})
class Report_16(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/system_setting.json"]))
        return Response({'data': 1})
class Report_17(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_gold_price.json"]))
        return Response({'data': 1})
class Report_18(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_customer.json"]))
        return Response({'data': 1})
class Report_19(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/amphures.json"]))
        return Response({'data': 1})
class Report_20(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/districts.json"]))
        return Response({'data': 1})
class Report_21(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/zipcodes.json"]))
        return Response({'data': 1})
class Report_22(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/provinces.json"]))
        return Response({'data': 1})
class Report_23(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_branch_setting.json"]))
        return Response({'data': 1})
class Report_24(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_stock_product.json"]))
        return Response({'data': 1})
class Report_25(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_stock_product_item.json"]))
        return Response({'data': 1})
class Report_26(APIView):
    def get(self, request, format=None):   
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_product_name.json"]))
        return Response({'data': 1})
class Report_27(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_bill.json"])) 
        return Response({'data': 1})
class Report_28(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_bill_item.json"]))
        return Response({'data': 1})
class Report_29(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_bill_staff.json"]))         
        return Response({'data': 1})
class Report_30(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_stock_catrgory.json"]))
        return Response({'data': 1})
class Report_31(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_stock_catrgory_item.json"]))
        return Response({'data': 1})
class Report_32(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_bill_category.json"]))
        return Response({'data': 1})
class Report_33(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_bill_category_item.json"]))
class Report_34(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_ledger.json"]))
        return Response({'data': 1})
class Report_35(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_lease.json"]))
        return Response({'data': 1})
class Report_36(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_lease_interest.json"]))
        return Response({'data': 1})
class Report_37(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_lease_principle.json"]))
        return Response({'data': 1})
class Report_38(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_lease_product.json"]))
        return Response({'data': 1})
class Report_39(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_score.json"]))        
        return Response({'data': 1})
class Report_40(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_score_item.json"]))
        return Response({'data': 1})
class Report_41(APIView):
    def get(self, request, format=None):     
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_invoice.json"]))
        return Response({'data': 1})
class Report_42(APIView):
    def get(self, request, format=None):
        print(subprocess.run(["/usr/local/bin/python",  "/usr/src/app/manage.py", "loaddata", "fixtures_report/report_invoice_item.json"]))
        return Response({'data': 1})