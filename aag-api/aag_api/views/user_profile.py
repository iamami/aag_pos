from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly,IsAuthenticated
from aag_api.serializers import *
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime as dt
import datetime
from rest_framework import status

class UserProfileView(APIView):
    def get(self, request, format=None):
        user = UserInfoSerializer(request.user)
        userprofile = UserProfile.objects.get(user=request.user.id)
        serializer = UserProfileFullSerializer(userprofile)
        return Response(serializer.data)
    permission_classes = (permissions.IsAuthenticated,)

class UserProfileList(generics.ListCreateAPIView):
    def get_serializer_class(self):
        if self.request.method=='GET':
            return UserProfileFullSerializer
        return UserProfileSerializer

    def get_queryset(self):
        queryset = UserProfile.objects.all()
        user = self.request.query_params.get('user_id', None)
        if user is not None:
            queryset = queryset.filter(user=user)
        
        username = self.request.query_params.get('username', None)
        if username is not None and username is not '':
            queryset = queryset.filter(user__username=username)
        
        is_active = self.request.query_params.get('is_active', None)
        if is_active is not None:
            queryset = queryset.filter(user__is_active=is_active)

        branch = self.request.query_params.get('branch', None)
        if branch is not None and branch!='':
            queryset = queryset.filter(branch=branch)

        return queryset
    permission_classes = (permissions.IsAuthenticated,)

class UserProfileDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def delete(self, request,pk,**kwargs):

        user_profile = UserProfile.objects.get(pk=pk)
        user = user_profile.user
        
        # check in bill
        if Bill.objects.all().filter(user=user.pk).count() > 0 or Invoice.objects.all().filter(user=user.pk).count()>0 or Lease.objects.all().filter(user=user.pk).count()>0 or Ledger.objects.all().filter(user=user.pk).count()>0 or Invoice.objects.all().filter(user=user.pk).count()>0 or BillCategory.objects.all().filter(user=user.pk).count()>0:
            content = {'error': 'ไม่สามารถลบผู้ใช้งานได้ เนื่องจากถูกใช้งานแล้ว'}
            return Response(content, status=status.HTTP_403_FORBIDDEN)
        return self.destroy(request, pk, **kwargs)
