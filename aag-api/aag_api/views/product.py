from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly,IsAuthenticated
from aag_api.serializers import *
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime as dt
import datetime
from rest_framework import status
from aag_api.permissions import *


class ProductList(generics.ListCreateAPIView):
    def get_serializer_class(self):
        if self.request.method=='GET':
            return ProductFullSerializer
        return ProductSerializer
    
    def get_queryset(self):
        queryset = Product.objects.all()
        category_id = self.request.query_params.get('category_id', None)
        if category_id is not None and category_id!='':
            queryset = queryset.filter(category=category_id)

        product_types_id = self.request.query_params.get('product_types_id', None)
        if product_types_id is not None and product_types_id!='':
            queryset = queryset.filter(kind=product_types_id)

        product_id = self.request.query_params.get('product_id', None)
        if product_id is not None and product_id!='':
            queryset = queryset.filter(pk=product_id)
        
        type_sale = self.request.query_params.get('type_sale', None)
        if type_sale is not None and type_sale!='':
            queryset = queryset.filter(type_sale=type_sale)
        
        weight = self.request.query_params.get('weight', None)
        if weight is not None and type_sale!='':
            queryset = queryset.filter(weight=weight)

        is_enabled = self.request.query_params.get('is_enabled', None)
        if is_enabled is not None:
            queryset = queryset.filter(is_enabled=is_enabled)

        id = self.request.query_params.get('id', None)
        if id is not None:
            queryset = queryset.filter(id=id)   
        code = self.request.query_params.get('code', None)
        if code is not None:
            queryset = queryset.filter(code=code)
        return queryset
    permission_classes = (permissions.IsAuthenticated,IsUserReadOnly,)

class ProducrtDatatList(generics.ListCreateAPIView):
    queryset = Product.objects.all()
    serializer_class = ProducrtDatatSerializer

    permission_classes = (permissions.IsAuthenticated,IsUserReadOnly,)

class ProductDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = (permissions.IsAuthenticated,IsUserReadOnly,)

    def delete(self, request,pk,**kwargs):
        
        # check in bill
        if BillItem.objects.all().filter(product=pk).count() > 0 or StockProduct.objects.all().filter(product=pk).count()>0 or InvoiceItem.objects.all().filter(product=pk).count()>0:
            content = {'error': 'ไม่สามารถลบได้ เนื่องจากถูกใช้งานแล้ว'}
            return Response(content, status=status.HTTP_403_FORBIDDEN)

        return self.destroy(request, pk, **kwargs)