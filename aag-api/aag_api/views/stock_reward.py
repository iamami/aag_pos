from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly,IsAuthenticated
from aag_api.serializers import *
from aag_api.models.stock_reward_item import StockRewardItem
from django.db.models import Q
from rest_framework import status
from aag_api.pagination import LargeResultsSetPagination
from datetime import datetime

class StockRewardList(generics.ListCreateAPIView):
    pagination_class = LargeResultsSetPagination

    def get_serializer_class(self):
        if self.request.method=='GET':
            return StockRewardFullSerializer
        return StockRewardCreateSerializer

    def get_queryset(self):
        queryset = StockReward.objects.all().order_by('id')

        branch = self.request.query_params.get('branch', None)
        if branch is not None:
            queryset = queryset.filter(branch=branch)

        keyword = self.request.query_params.get('keyword', None)
        if keyword is not None and keyword != '':
            queryset = queryset.filter(Q(reward__code__contains=keyword) | Q(reward__name__contains=keyword))
        return queryset
    
    def perform_create(self, serializer):

        serializer.save(updated_datetime=datetime.today())
        s = StockReward.objects.get(pk=serializer.data['reward'])
        amount = serializer.data['amount']

        # add log stock reward
        StockRewardItem.objects.create(stock_reward=s,amount=amount,before=0,after=amount,kind = "I",user=self.request.user,description='สร้างรายการใหม่')
    permission_classes = (permissions.IsAuthenticated,)

class StockRewardDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = StockReward.objects.all()
    serializer_class = StockRewardSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def perform_update(self, serializer):

        serializer.save()
        s = StockReward.objects.get(pk=serializer.data['id'])

        # get last amount
        o = StockRewardItem.objects.all().filter(stock_reward=s.pk).order_by('-id')[0]
        amount = serializer.data['amount']
        _amount = 0
        kind = amount = serializer.data['kind']
        if amount < o.after:
            _amount = o.after - amount
            kind = 'O'
        else:
            _amount = amount - o.after
 
        # add log
        StockRewardItem.objects.create(stock_reward=s,amount=_amount,before=o.after,after=amount,kind = kind,user=self.request.user)
