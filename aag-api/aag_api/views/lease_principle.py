from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly,IsAuthenticated
from aag_api.serializers import *
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime as dt
from django.shortcuts import get_object_or_404
import datetime
from aag_api.permissions import *

class LeasePrincipleList(generics.ListCreateAPIView):
   
    def get_serializer_class(self):
        if self.request.method=='GET':
            return LeasePrincipleFullSerializer
        return LeasePrincipleSerializer

    def perform_create(self, serializer):
        instance = serializer.save(user=self.request.user)

        instance.lease.amount = instance.balance
        instance.lease.save()

    def get_queryset(self):
        lease = self.kwargs.get("lease")
        queryset = LeasePrinciple.objects.all()

        branch = self.request.query_params.get('branch', None)
        if branch is not None:
            queryset = queryset.filter(lease__branch=branch).order_by('id')

        start_date = self.request.query_params.get('start_date', None)
        end_date = self.request.query_params.get('end_date', None)
        if start_date is not None and end_date is not None:
            from_date = dt.strptime(start_date, '%Y-%m-%d').date()
            from_date = datetime.datetime.combine(from_date, datetime.time.min)
            to_date = dt.strptime(end_date, '%Y-%m-%d').date()
            to_date = datetime.datetime.combine(to_date, datetime.time.max)
            queryset = queryset.filter(record_date__range=[from_date, to_date])

        if lease != '0' :
            queryset = queryset.filter(lease=lease)

        lease_in = self.request.query_params.get('lease_in', None)
        if lease_in is not None :
            lease_in = lease_in.split(',')
            queryset = queryset.filter(lease__in=lease_in)

        return queryset.order_by('id')

    permission_classes = (permissions.IsAuthenticated,)

class LeasePrincipleDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = LeasePrinciple.objects.all()
    serializer_class = LeasePrincipleSerializer
    permission_classes = (permissions.IsAuthenticated,IsAdminUpdateOnly,IsAdminDeleteOnly)

    def delete(self, request, lease,pk, *args, **kwargs):
        obj = get_object_or_404(LeasePrinciple, pk=pk)

        # cancel ledger
        l = obj.ledger
        l.status = 0 # cancel
        l.save()

        return self.destroy(request, *args, **kwargs)