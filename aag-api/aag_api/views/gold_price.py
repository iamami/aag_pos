# -*- coding: utf-8 -*-
from rest_framework import generics
from rest_framework import permissions
from aag_api.models import *
from aag_api.serializers import *
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from django.db.models import Q
from rest_framework.views import APIView

from rest_framework.authtoken.models import Token
from rest_framework.authtoken.serializers import AuthTokenSerializer
from datetime import datetime as dt
import datetime


class GoldPriceList(generics.ListCreateAPIView):
    def get_queryset(self):
        limit = self.request.query_params.get("limit")
        datetime = self.request.query_params.get("datetime")
        if datetime is not None:
            queryset = GoldPrice.objects.all().filter(record_date__lte = datetime).order_by("-record_date")[:1]
        elif limit is None:
            queryset = GoldPrice.objects.all().order_by("-record_date")
        else:
            queryset = GoldPrice.objects.all().order_by("-record_date")[:int(limit)]
        return queryset

    serializer_class = GoldPriceSerializer
    permission_classes = (permissions.IsAuthenticated,)

class  GoldPriceDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset =  GoldPrice.objects.all()
    serializer_class =  GoldPriceSerializer
    permission_classes = (permissions.IsAuthenticated,)

class goldpriceView(APIView):
    def get(self,  request, format=None):
        r = requests.get('https://www.aagold-th.com/goldprice')
        return Response(r.json())
    permission_classes = (permissions.IsAuthenticated,)