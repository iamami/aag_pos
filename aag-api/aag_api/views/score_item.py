from aag_api.serializers import *
from rest_framework import permissions
from aag_api.models.score import Score
from aag_api.models.score_item import ScoreItem
from aag_api.pagination import LargeResultsSetPagination
from rest_framework import generics
from django.shortcuts import get_object_or_404

class scoreItemList(generics.ListAPIView):

    def get_queryset(self):
        customer_id = self.kwargs['customer_id']
        user = self.request.user
        o = get_object_or_404(Score, customer=customer_id)
        queryset = ScoreItem.objects.all().order_by('-id').filter(score=o.pk)

        kind = self.request.query_params.get('kind', None)
        if kind is not None:
            queryset = queryset.filter(kind=kind)

        action = self.request.query_params.get('action', None)
        if action is not None:
            queryset = queryset.filter(action=action)
        return queryset
    
    pagination_class = LargeResultsSetPagination
    serializer_class = ScoreItemFullSerializer
    permission_classes = (permissions.IsAuthenticated,)

class scoreItemCreateList(generics.CreateAPIView):

    queryset = ScoreItem.objects.all()
    serializer_class = ScoreItemSerializer
