from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly,IsAuthenticated
from aag_api.serializers import *
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime as dt
import datetime
from rest_framework import status


class BankList(generics.ListCreateAPIView):
    def get_queryset(self):
        queryset = Bank.objects.all()
        is_enabled = self.request.query_params.get('is_enabled', None)
        if is_enabled is not None:
            queryset = queryset.filter(is_enabled=is_enabled)
        return queryset

    serializer_class = BankSerializer
    permission_classes = (permissions.IsAuthenticated,)

class BankDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Bank.objects.all()
    serializer_class = BankSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def delete(self, request,pk,**kwargs):
        
        # check in bill
        if BankAccount.objects.all().filter(bank=pk).count() > 0 or BankCard.objects.all().filter(bank=pk).count()>0:
            content = {'error': 'ไม่สามารถลบได้ เนื่องจากถูกใช้งานแล้ว'}
            return Response(content, status=status.HTTP_403_FORBIDDEN)

        return self.destroy(request, pk, **kwargs)