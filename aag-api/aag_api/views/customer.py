from rest_framework import generics
from rest_framework import permissions
from aag_api.serializers import *
from django.db.models import Q
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from django.shortcuts import get_object_or_404
from aag_api.pagination import LargeResultsSetPagination
from aag_api.permissions import IsAdminOnly

class CustomerList(generics.ListCreateAPIView):
    serializer_class = CustomerSerializer
    pagination_class = LargeResultsSetPagination
    def get_queryset(self):
        queryset = Customer.objects.all()

        columnKey = self.request.query_params.get('columnKey', None)
        sortDi = self.request.query_params.get('sortDi', None)
        if columnKey is not None and sortDi is not None:
            if sortDi == 'ASC':
                queryset = queryset.order_by(columnKey)
            else:
                queryset = queryset.order_by('-'+columnKey)

        code = self.request.query_params.get('code', None)
        if code is not None:
            queryset = queryset.filter(code__contains=code)
        customer_id = self.request.query_params.get('customer_id', None)
        if customer_id is not None and customer_id != '':
            queryset = queryset.filter(pk=customer_id)
        phone = self.request.query_params.get('phone', None)
        name = self.request.query_params.get('name', None)
        if name is not None:
            queryset = queryset.filter(name__contains=name)
        phone = self.request.query_params.get('phone', None)
        if phone is not None:
            queryset = queryset.filter(phone__contains=phone)        
        citizen_id = self.request.query_params.get('citizen_id', None)
        if citizen_id is not None:
            queryset = queryset.filter(citizen_id__contains=citizen_id)
        is_enabled = self.request.query_params.get('is_enabled', None)
        if is_enabled is not None:
            queryset = queryset.filter(is_enabled=1)

        text = self.request.query_params.get('text', None)
        if text is not None:
            queryset = queryset.filter(Q(code__contains=text) | Q(name__contains=text) | Q(phone__contains=text) | Q(citizen_id__contains=text)| Q(mobile__contains=text)| Q(email__contains=text))
        
        return queryset
    permission_classes = (permissions.IsAuthenticated,)

class CustomerDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_permissions(self):
        if self.request.method == 'DELETE':
            self.permission_classes = [permissions.IsAuthenticated, IsAdminOnly]
        else:
            self.permission_classes = [permissions.IsAuthenticated,]

        return super(CustomerDetail, self).get_permissions()

    def get(self, request,pk,format=None):
        customer = get_object_or_404(Customer, pk=pk)
        if customer.secret is None:
            customer.secret = customer.createCodeSecurity()
        serializer = CustomerGetSerializer(customer,context={'request': request})
        
        return Response(serializer.data)

    def delete(self, request,pk,**kwargs):
        
        # check in bill
        if Bill.objects.all().filter(customer=pk).count() > 0 or Ledger.objects.all().filter(customer=pk).count() > 0 or Lease.objects.all().filter(customer=pk).count() > 0 :
            content = {'error': 'ไม่สามารถลบได้ เนื่องจากถูกใช้งานแล้ว'}
            return Response(content, status=status.HTTP_403_FORBIDDEN)

        return self.destroy(request, pk, **kwargs)

class CustomerQrcodeView(APIView):

    def get(self, request,pk,secret,format=None):

        customer = get_object_or_404(Customer, pk=pk,secret=secret)
        if customer.secret is None:
            customer.secret = customer.createCodeSecurity()

        serializer = CustomerCodeSecuritySerializer(customer,context={'request': request})
        return Response(serializer.data)
    permission_classes = (permissions.IsAuthenticated,)

class CustomerBillItemList(generics.ListAPIView):
    def get_serializer_class(self):
        if self.request.method=='GET':
            return BillItemFullSerializer
        return BillItemSerializer
    def get_queryset(self):
        queryset = BillItem.objects.all().order_by('-id')

        # filter bill
        customer_id=self.kwargs.get("pk")
        if customer_id is not None:
            queryset = queryset.filter(bill__customer=customer_id)
        
        # filter kind
        kind = self.request.query_params.get('kind', None)
        if kind is not None:
            queryset = queryset.filter(kind=kind)

        category_not = self.request.query_params.get('category_not', None)

        if category_not is not None:
            queryset = queryset.filter(~Q(category = category_not))

        category = self.request.query_params.get('category', None)
        if category is not None:
            queryset = queryset.filter(category=category)

        # filter status_stock
        status_stock = self.request.query_params.get('status_stock', None)
        if status_stock is not None:
            queryset = queryset.filter(status_stock=status_stock)
        
        return queryset
    permission_classes = (permissions.IsAuthenticated,)
    pagination_class = LargeResultsSetPagination