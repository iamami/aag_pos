from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly,IsAuthenticated
from aag_api.serializers import *
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime as dt
import datetime
from aag_api.models.counter import Counter



class CounterDetail(APIView):

    def get(self, request):
        model = self.request.query_params.get('model', None)
        c = 0
        if model=='staffs':
            c = Counter.getObject("staff").count
        if model=='branches':
            c = Counter.getObject("branch").count
        if model=='customers':
            c = Counter.getObject("customer").count

        return Response({'model' : model,'last_id': c,'number':c})
    permission_classes = (permissions.IsAuthenticated,)

