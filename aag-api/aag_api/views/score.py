from rest_framework import generics
from rest_framework import permissions
from aag_api.serializers import *
from aag_api.models.score import *
from rest_framework.response import Response
from rest_framework.views import APIView
from django.shortcuts import get_object_or_404

class scoreView(APIView):

    def get(self, request,customer_id,format=None):

        o = get_object_or_404(Score, customer=customer_id)
        serializer = ScoreSerializer(o)
        return Response(serializer.data)
    permission_classes = (permissions.IsAuthenticated,)

    