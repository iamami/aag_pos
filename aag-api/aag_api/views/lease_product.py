from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly,IsAuthenticated
from aag_api.serializers import *
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime as dt
import datetime
from django.shortcuts import get_object_or_404
from django.db.models import Sum
from aag_api.permissions import *

class LeaseProductList(generics.ListCreateAPIView):
   
    def get_serializer_class(self):
        if self.request.method=='GET':
            return LeaseFullProductSerializer
        return LeaseProductSerializer

    def get_queryset(self):
        lease = self.kwargs.get("lease")
        queryset = LeaseProduct.objects.all()
        queryset = queryset.filter(lease__is_enabled=1)

        start_date = self.request.query_params.get('start_date', None)
        end_date = self.request.query_params.get('end_date', None)
        if start_date is not None and end_date is not None:
            from_date = dt.strptime(start_date, '%Y-%m-%d').date()
            from_date = datetime.datetime.combine(from_date, datetime.time.min)
            to_date = dt.strptime(end_date, '%Y-%m-%d').date()
            to_date = datetime.datetime.combine(to_date, datetime.time.max)
            queryset = queryset.filter(record_date__range=[from_date, to_date])

        status = self.request.query_params.get('status', None)
        if status is not None:
            queryset = queryset.filter(status=status)
        
        lease_status = self.request.query_params.get('lease_status', None)
        if lease_status is not None:
            queryset = queryset.filter(lease__status__in=lease_status.split(","))

        branch = self.request.query_params.get('branch', None)
        if branch is not None:
            queryset = queryset.filter(lease__branch=branch)

        lease_in = self.request.query_params.get('lease_in', None)
        
        if lease_in is not None :
            lease_in = lease_in.split(',')
            queryset = queryset.filter(lease__in=lease_in)

        if lease != '0' :
            queryset = queryset.filter(lease=lease)

        branch = self.request.query_params.get('branch', None)
        if branch is not None:
            queryset = queryset.filter(lease__branch=branch)
        is_enabled = self.request.query_params.get('is_enabled', None)
        if is_enabled is not None:
            queryset = queryset.filter(is_enabled=is_enabled)
        return queryset

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

        lease = self.kwargs.get("lease")
        weight = LeaseProduct.objects.all().filter(lease=lease).aggregate(Sum('weight'))

        obj = get_object_or_404(Lease, pk=lease)
        obj.weight = weight['weight__sum']
        obj.save()

    permission_classes = (permissions.IsAuthenticated,)


class LeaseProductDetail(generics.DestroyAPIView):
    queryset = LeaseProduct.objects.all()
    serializer_class = LeaseProductSerializer
    permission_classes = (permissions.IsAuthenticated,IsAdminOnly)

    def perform_destroy(self, serializer):
        serializer.delete()

        lease = self.kwargs.get("lease")
        weight = LeaseProduct.objects.all().filter(lease=lease).aggregate(Sum('weight'))

        obj = get_object_or_404(Lease, pk=lease)
        obj.weight = weight['weight__sum']
        obj.save()
