from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly,IsAuthenticated
from aag_api.serializers import *
from django.db.models import Q
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from datetime import datetime as dt
import datetime
from aag_api.pagination import LargeResultsSetPagination
from aag_api.models.score_item import *
from aag_api.models.customer import *
class SavingList(generics.ListCreateAPIView):

    def get_serializer_class(self):
        if self.request.method=='GET':
            return SavingFullSerializer
        return SavingSerializer
    
    def get_queryset(self):
        queryset = Saving.objects.all().order_by('-record_date')
        return queryset
    permission_classes = (IsAuthenticated,)
    #pagination_class = LargeResultsSetPagination

class SavingDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Saving.objects.all()
    def get_serializer_class(self):
        if self.request.method=='GET':
            return SavingFullSerializer
        return SavingSerializer
    permission_classes = (IsAuthenticated,)


class SavingAddScore(APIView):
    serializer_class = SavingAddScoreSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request,format=None):
        serializer = SavingAddScoreSerializer(data=request.data)

        if serializer.is_valid():
            customer = Customer.objects.get(pk=serializer.data['customer'])
            amount = serializer.data['amount']
            score = serializer.data['score']
            
            ScoreItem.saveSaving(request.user,customer,None,int(score))
            return Response(serializer.errors,status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status.HTTP_400_BAD_REQUEST)