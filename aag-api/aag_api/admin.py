from django.contrib import admin

from aag_api.models.user_profile import UserProfile
from aag_api.models.branch import Branch
from aag_api.models.customer import Customer
from aag_api.models.system_setting import SystemSetting
from aag_api.models.counter import Counter
from aag_api.models.ledger import Ledger
from aag_api.models.gold_price import GoldPrice
from aag_api.models.branch_setting import BranchSetting

class BranchSettingAdmin(admin.ModelAdmin):
    list_display = ('branch',)
    search_fields = ('branch__code','branch__name',)
admin.site.register(BranchSetting,BranchSettingAdmin)

class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('branch','user','staff', 'role')
    search_fields = ('user__username','staff__name',)
admin.site.register(UserProfile,UserProfileAdmin)

admin.site.register(Branch)

class CustomerAdmin(admin.ModelAdmin):
    list_display = ('code','name', 'mobile','email')
    search_fields = ('code','name', 'mobile','email')
admin.site.register(Customer,CustomerAdmin)

class SystemSettingAdmin(admin.ModelAdmin):
    list_display = ('setting', )
admin.site.register(SystemSetting,SystemSettingAdmin)

class CounterAdmin(admin.ModelAdmin):
    list_display = ('name', 'count')
admin.site.register(Counter,CounterAdmin)

class LedgerAdmin(admin.ModelAdmin):
    list_display = ('number', 'branch','kind','total')
    search_fields = ('number',)
admin.site.register(Ledger,LedgerAdmin)

class GoldPriceAdmin(admin.ModelAdmin):
    list_display = ('gold_bar_buy','gold_bar_sell','gold_ornaments_buy', 'gold_ornaments_sell')
admin.site.register(GoldPrice,GoldPriceAdmin)