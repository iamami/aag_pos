
from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *
from aag_api.models.score import Score
from django.core.validators import ValidationError
from mapi.models.device import Device

SCORE_KIND = (
    ('SE','ซื้อทอง'),
    ('SA','ออมทอง'),
    ('IR','ต่อดอกเบี้ย'),
    ('OR','แลกของรางวัล')
)
ACTION_CHOICES = (
    ('ADD','รับคะแนน'),
    ('DEL','ใช้คะแนน'),
)

class ScoreItem(models.Model):

    score = models.ForeignKey('Score', related_name='score_item', on_delete=models.CASCADE)
    action = models.CharField(max_length=3, choices=ACTION_CHOICES, help_text='Action')
    kind = models.CharField(max_length=2, choices=SCORE_KIND, help_text='Kind')
    bill_item = models.ForeignKey('BillItem', related_name='score_item_bill_item', on_delete=models.CASCADE,null=True,blank=True)
    saving = models.ForeignKey('Saving', related_name='score_item_saving', on_delete=models.CASCADE,null=True,blank=True)
    interest = models.ForeignKey('LeaseInterest', related_name='score_item_lease_interest', on_delete=models.CASCADE,null=True,blank=True)
    redeem = models.ForeignKey('Redeem', related_name='score_item_redeem', on_delete=models.CASCADE,null=True,blank=True)
    amount = models.IntegerField(help_text='accomulate point', default=0)
    begin = models.IntegerField(help_text='score begin', default=0)
    balance = models.IntegerField(help_text='score balance', default=0)
    created_datetime = models.DateTimeField(auto_now_add=True) 
    creator = models.ForeignKey(User, related_name='score_item_user', on_delete=models.CASCADE,null=True,blank=True)

    def getString(self):

        if self.kind == 'SE':
            return "%s %s" % (self.get_kind_display(),self.bill_item.product.name)
        if self.kind == 'IR':
            return "%s" % (self.interest.get_status_display())
        if self.kind == 'SA':
            return "%s" % (self.get_kind_display())
        if self.kind == 'OR':
            return "%s" % (self.get_kind_display())

        return "xxxx"
    def clean(self, *args, **kwargs):

        if self.kind == "SE" and self.bill_item is None:
            raise ValidationError({'bill_item':"This field cannot be null."})
        # if self.kind == "SA" and self.saving is None:
        #     raise ValidationError({'saving':"This field cannot be null."})
        if self.kind == "IR" and self.interest is None:
            raise ValidationError({'interest':"This field cannot be null."})
        if self.kind == "OR" and self.redeem is None:
            raise ValidationError({'redeem':"This field cannot be null."})

        super(ScoreItem, self).clean()
    
    def getScore(customer):
        try:
            return Score.objects.get(customer=customer)
        except Score.DoesNotExist:
            return Score.objects.create(customer=customer)

    def saveRedeem(customer,redeem,amount):

        score = ScoreItem.getScore(customer)

        begin = score.total
        balance = score.total - amount
        o = ScoreItem(
            score = score,
            amount= amount,
            kind='OR',
            action='DEL',
            redeem=redeem,
            creator=None,
            balance=balance,
            begin=begin
        )
        o.full_clean()      
        o.save()

        score.total = balance
        #score.use = score.use + o.amount
        score.save()

        device_list = Device.objects.all().filter(customer_user__customer=customer.pk)
        
        title = "คุณใช้แต้ม %s คะแนน" % (amount)
        message = "แลกของรางวัล %s " % (redeem.reward.name)
        res = Device.sendNotiScoreItem(device_list,title,message,{
            'type': 5,
            'data': {
                'action' : o.get_action_display(),
                'kind' : o.get_kind_display(),
                'balance': o.balance,
                'begin': o.begin,
                'amount': o.amount,
                'id': o.pk,
                'created_datetime': str(o.created_datetime)
            }
        },o)
        print(res)
        print(title)
        print(message)

        return o

    def voidBillItem(creator,customer,bill_item,amount):

        score = ScoreItem.getScore(customer)
        begin = score.total
        balance = score.total - amount
        o = ScoreItem(
            score = score,
            amount= amount,
            kind='SE',
            action='DEL',
            bill_item=bill_item,
            creator=creator,
            balance=balance,
            begin=begin
        )
        o.full_clean()      
        o.save()

        score.total = balance
        score.sell = score.sell - o.amount
        score.save()

        return o

    def saveBillItem(creator,customer,bill_item,amount):

        score = ScoreItem.getScore(customer)
        begin = score.total
        balance = score.total + amount
        o = ScoreItem(
            score = score,
            amount= amount,
            kind='SE',
            action='ADD',
            bill_item=bill_item,
            creator=creator,
            balance=balance,
            begin=begin
        )
        o.full_clean()      
        o.save()

        score.total = balance
        score.sell = score.sell + o.amount
        score.save()

        device_list = Device.objects.all().filter(customer_user__customer=customer.pk)
        
        title = "คุณได้รับแต้ม %s คะแนน" % (amount)
        message = o.getString()
        res = Device.sendNotiScoreItem(device_list,title,message,{
            'type': 5,
            'data': {
                'action' : o.get_action_display(),
                'kind' : o.get_kind_display(),
                'balance': o.balance,
                'begin': o.begin,
                'amount': o.amount,
                'id': o.pk,
                'created_datetime': str(o.created_datetime)
            }
        },o)
        print(res)
        print(title)
        print(message)
        return o

    def saveSaving(creator,customer,saving,amount):
        score = ScoreItem.getScore(customer)
        begin = score.total
        balance = score.total + amount
        o = ScoreItem(
            score = score,
            amount=amount,
            kind='SA',
            action='ADD',
            saving=saving,
            creator=creator,
            balance=balance,
            begin=begin
        )        
        o.full_clean()
        o.save()

        score.total = balance
        score.saving = score.saving + o.amount
        score.save()

        device_list = Device.objects.all().filter(customer_user__customer=customer.pk)
        
        title = "คุณได้รับแต้ม %s คะแนน" % (amount)
        message = o.getString()
        res = Device.sendNotiScoreItem(device_list,title,message,{
            'type': 5,
            'data': {
                'action' : o.get_action_display(),
                'kind' : o.get_kind_display(),
                'balance': o.balance,
                'begin': o.begin,
                'amount': o.amount,
                'id': o.pk,
                'created_datetime': str(o.created_datetime)
            }
        },o)
        print(res)
        print(title)
        print(message)
        return o

    def saveInterest(creator,customer,interest,amount):
        score = ScoreItem.getScore(customer)
        begin = score.total
        balance = score.total + amount
        o = ScoreItem(
            score = score,
            amount=amount,
            kind='IR',
            action='ADD',
            interest=interest,
            creator=creator,
            balance=balance,
            begin=begin
        )     
        o.full_clean()   
        o.save()

        score.total = balance
        score.interest = score.interest + o.amount
        score.save()

        device_list = Device.objects.all().filter(customer_user__customer=customer.pk)
        
        title = "คุณได้รับแต้ม %s คะแนน" % (amount)
        message = o.getString()
        res = Device.sendNotiScoreItem(device_list,title,message,{
            'type': 5,
            'data': {
                'action' : o.get_action_display(),
                'kind' : o.get_kind_display(),
                'balance': o.balance,
                'begin': o.begin,
                'amount': o.amount,
                'id': o.pk,
                'created_datetime': str(o.created_datetime)
            }
        },o)
        print(res)
        print(title)
        print(message)

        return o