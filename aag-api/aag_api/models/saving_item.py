from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *
from datetime import date

KIND_SAVING_CHOICES = (
    ('I','ฝากเงิน'),
    ('O','ถอนเงิน')
)
class SavingItem(models.Model):
    saving = models.ForeignKey('Saving', related_name='saving_item', help_text='branch', on_delete=models.CASCADE)
    branch = models.ForeignKey('Branch', related_name='saving_item_branch', help_text='branch', on_delete=models.CASCADE)
    record_date = models.DateTimeField(auto_now_add=True, help_text='record_date')
    user = models.ForeignKey(
         User, related_name='saving_item_user', on_delete=models.CASCADE)
    staff = models.ForeignKey(
        'Staff', related_name='saving_item_staff', on_delete=models.CASCADE)
    gold = models.DecimalField(
        max_digits=20, decimal_places=2, default=0)
    amount = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    kind = models.CharField(max_length=1, choices=KIND_SAVING_CHOICES)
