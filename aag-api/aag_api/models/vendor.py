from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *

class Vendor(models.Model):
    name = models.CharField(max_length=255, help_text='Name',unique=True)
    phone = models.CharField(max_length=20, help_text='phone number', default='',blank=True,null=True)
    address = models.CharField(max_length=255, help_text='address', default='',blank=True,null=True)
    is_enabled = models.IntegerField(default=ENABLED,choices=IS_ENABLED)
    description = models.CharField(max_length=255, help_text='some description', default='',blank=True,null=True)

    def __str__(self):
        return self.name
