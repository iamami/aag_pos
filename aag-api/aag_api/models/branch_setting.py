from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *
from django.core.validators import ValidationError
from django.core.validators  import RegexValidator

validator_code = RegexValidator(r'^[A-Z]{1}[A-Z]{1}$','รูปแบบตัวอักษรไม่ถูกต้อง!')
class BranchSetting(models.Model):
    branch = models.OneToOneField('Branch', related_name='Branchetting_branch', help_text='branch', on_delete=models.CASCADE)
    
    code = models.CharField(max_length=2,validators = [validator_code],blank=True, null=True,)    
    counter = models.IntegerField(default=1)
    month = models.IntegerField(help_text='number of months to instalment', default=0)
    interest = models.DecimalField(max_digits=20, decimal_places=2, help_text='interest %', default=0)
    min_interest = models.DecimalField(
        max_digits=20, decimal_places=2, default=100)
    is_user_edit_interest =  models.IntegerField(choices=IS_USER_EDIT_INTEREST_CHOICES, default=0)
    calculate_date = models.IntegerField(choices=CALCULATE_DATE, default=1)
    tax_code = models.CharField(max_length=2,validators = [validator_code],blank=True, null=True,help_text='Char for tax number')
    tax_counter = models.IntegerField(help_text='tax counter', default=1)
    staff_code = models.CharField(max_length=1,validators = [RegexValidator(r'^[A-Z]{1}$','รูปแบบตัวอักษรไม่ถูกต้อง!')],blank=True, null=True,help_text='Char for staff number')
    staff_counter = models.IntegerField(help_text='staff counter', default=1)
    def genCode(self,id):
        char = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        s = char[int(id/26):int(id/26)+1] + char[(id%26):(id%26)+1]
        return s

    def save(self, *args, **kwargs):
       
        if self._state.adding and self.code is None:
            self.code = self.genCode(self.branch.id)
        if self._state.adding and self.tax_code is None:
            self.tax_code = self.genCode(self.branch.id)
            
        if self._state.adding and self.staff_code is None:
            self.staff_code = ''
        super().save(*args, **kwargs)