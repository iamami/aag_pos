from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *

class StockProductItem(models.Model):
    product = models.ForeignKey(
        'Product', related_name='stock_item_product', help_text='product', on_delete=models.CASCADE)
    stock = models.ForeignKey(
        'StockProduct', related_name='stock_item_stock', help_text='StockProduct', on_delete=models.CASCADE)
    branch = models.ForeignKey(
        'Branch', related_name='stock_item_branch', help_text='branch of this staff', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='stock_product_item_user', on_delete=models.CASCADE,default=1)
    object_id = models.CharField(max_length=15, help_text='invoicd or bill', default='')
    weight = models.DecimalField(
        max_digits=20, decimal_places=3, help_text='weight per bath in grams', default=0)
    weight_total = models.DecimalField(
        max_digits=20, decimal_places=3, help_text='Total weight per bath in grams', default=0)
    weight_real = models.DecimalField(
        max_digits=20, decimal_places=3, help_text='Real scales', default=0)
    amount = models.IntegerField(help_text='amount', default=1)
    before = models.IntegerField(help_text='amount before', default=1)
    after = models.IntegerField(help_text='amount after', default=1)
    record_date = models.DateTimeField(auto_now_add=True, help_text='record_date')
    kind = models.CharField(max_length=2, choices=TYPE_STOCK, default='I', help_text='stock kind')
    branch_to = models.IntegerField(help_text='branch to', default=0)
    branch_from = models.IntegerField(help_text='branch from', default=0)
    object_number = models.CharField(max_length=20, help_text='ref number', default='')
