from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.conf import settings
from django.db.models.signals import post_save

# @receiver(post_save, sender=settings.AUTH_USER_MODEL)
# def create_auth_token(sender, instance=None, created=False, **kwargs):
#     if created:
#         Token.objects.create(user=instance)

from aag_api.models.branch import *
from aag_api.models.staff import *
from aag_api.models.gold_price import *
from aag_api.models.stock_product import *
from aag_api.models.stock_product_item import *
from aag_api.models.invoice import *
from aag_api.models.invoice_item import *
from aag_api.models.vendor import *
from aag_api.models.category import *
from aag_api.models.user_profile import *
from aag_api.models.weight import *
from aag_api.models.style import *
from aag_api.models.product import *
from aag_api.models.product_type import *
from aag_api.models.product_name import *
from aag_api.models.bank_card import *
from aag_api.models.bank import *
from aag_api.models.bank_account import *
from aag_api.models.customer import *
from aag_api.models.bill import *
from aag_api.models.bill_item import *
from aag_api.models.stock_category import *
from aag_api.models.stock_category_item import *
from aag_api.models.ledger import *
from aag_api.models.ledger_category import *
from aag_api.models.bill_staff import *
from aag_api.models.bill_category import *
from aag_api.models.bill_category_item import *
from aag_api.models.saving import *
from aag_api.models.saving_item import *
from aag_api.models.lease import *
from aag_api.models.lease_product import *
from aag_api.models.lease_interest import *
from aag_api.models.lease_principle import *
from aag_api.models.system_setting import *
from aag_api.models.branch_setting import *
from aag_api.models.counter import *
from aag_api.models.score import *
from aag_api.models.score_item import *
from aag_api.models.reward import *
from aag_api.models.stock_reward import *
from aag_api.models.stock_reward_item import *
from aag_api.models.redeem import *