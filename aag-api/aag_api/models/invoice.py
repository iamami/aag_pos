from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *
from django.core.validators import ValidationError
from datetime import datetime 

def getBillNumber(kind,branch,date):
    current_month = date.month
    current_year = date.year

    pre = ''
    if kind == 'IM':
        pre = 'RE'
    elif kind == 'EX':
        pre = 'CU'
    elif kind == 'MV':
        pre = 'TR'

    l = len(Invoice.objects.all().filter(invoice_date__month=current_month,invoice_date__year=current_year,kind=kind))+1
    no = "%05d" % (l,)

    return "%s-%s%s" % (pre , date.strftime('%y%m') , no)

class Invoice(models.Model):
    old_number = models.CharField(max_length=20, blank=True, null=True)
    number = models.CharField(max_length=20, help_text='invoicd number', unique=True,blank=True, null=True,db_index=True)
    branch = models.ForeignKey(
        'Branch', related_name='Invoice_branch', help_text='branch of this staff', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='invoice_user', on_delete=models.CASCADE)
    vendor = models.ForeignKey('Vendor', related_name='invoice_vendor', on_delete=models.CASCADE,default='',blank=True, null=True)
    ref_number = models.CharField(max_length=15, help_text='invoicd ref number', default='',blank=True, null=True)
    record_date = models.DateTimeField(auto_now_add=True, help_text='record_date')
    invoice_date = models.DateField(default=datetime.now, help_text='bill date')
    update_date = models.DateTimeField(auto_now_add=True, help_text='stock_date', blank=True)
    status_bill = models.CharField(max_length=2, choices=STATUS_INVOICE, default='N', help_text='bill status')
    status_stock = models.CharField(max_length=2, choices=STATUS_STOCK, default='N', help_text='N ยังไม่อัพเดท,Y อัพเดทแล้ว')
    description = models.CharField(max_length=255, help_text='some description', default='',blank=True)
    is_enabled = models.IntegerField(default=ENABLED,choices=IS_ENABLED)
    kind = models.CharField(max_length=2, choices=TYPE_INVOICE, default='IM', help_text='invoice kind')
    branch_to = models.IntegerField(help_text='branch to', default=0,blank=True, null=True)

    def save(self, *args, **kwargs):

        # create new bill
        if self._state.adding:
            # set bill number
            self.full_clean()
            self.number = getBillNumber(self.kind ,self.branch.id,self.invoice_date)
        elif self.number is None:
            raise ValidationError({'number' : ["This field cannot be null."]})
    
        if self.kind == 'MV' and (self.branch_to is None or self.branch_to == 0):
            raise ValidationError({'branch_to' : ["This field cannot be null."]})

        super().save(*args, **kwargs)