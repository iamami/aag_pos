from django.db import models
from django.contrib.auth.models import User
from aag_api.models.customer import Customer
from django.core.validators  import RegexValidator
from django.utils.crypto import get_random_string
from aag_api.models.reward import Reward
from django.core.validators import ValidationError
from aag_api.models.counter import Counter
from aag_api.models.option import *
from django.core.validators import MinValueValidator,MaxValueValidator
STATUS_CHOICES = (
    ('1','ขอแลกของรางวัล'),
    ('2','ส่งของรางวัลแล้ว'),
    ('3','รับของรางวัลแล้ว'),
    ('4','ขอยกเลิก')
)

TYPE_CHOICES = (
    ('1','รับที่สาขา'),
    ('2','จัดส่ง')
)

# Create your models here.
class Redeem(models.Model):
    code = models.CharField(max_length=20)
    branch = models.ForeignKey('Branch', related_name='order_branch', on_delete=models.CASCADE,blank=True, null=True)
    customer = models.ForeignKey(Customer, related_name='order_customer', on_delete=models.CASCADE)
    reward = models.ForeignKey(Reward, related_name='order_reward', on_delete=models.CASCADE)
    amount = models.IntegerField(validators=[MinValueValidator(0)])
    created_datetime = models.DateTimeField(auto_now_add=True)
    receipt_date = models.DateField(blank=True, null=True)
    status = models.CharField(max_length=2, choices=STATUS_CHOICES)
    kind = models.CharField(max_length=2, choices=TYPE_CHOICES)
    confirmed = models.ForeignKey(User, related_name='order_confirmed', on_delete=models.CASCADE,blank=True, null=True)
    canceler = models.ForeignKey(User, related_name='order_canceler', on_delete=models.CASCADE,blank=True, null=True)
    staff = models.ForeignKey('Staff', related_name='order_staff', on_delete=models.CASCADE,blank=True, null=True)
    remrak = models.CharField(max_length=255, help_text='remrak', default='',blank=True)
    address = models.CharField(max_length=255,blank=True, null=True)
    status_stock = models.CharField(max_length=2, choices=STATUS_STOCK, default='N',blank=True)

    def __str__(self):
        return '%s %s' % (self.reward.name,self.customer.name)

    def save(self, *args, **kwargs):

        if self._state.adding and (self.code is None or self.code == ''):
            c = Counter.getObject('redeem')
            self.code = "D%06d" % (c.count)

        super().save(*args, **kwargs)