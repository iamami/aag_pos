from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *

def getCode():
    l = len(LedgerCategory.objects.all())+1
    return "%03d" % (l)
class LedgerCategory(models.Model):
    code = models.CharField(max_length=20, help_text='Ledger code', default='', unique=True)
    title = models.CharField(max_length=255, help_text='some description', default='')
    kind = models.CharField(max_length=2, choices=TYPE_LEDGER, default='IN', help_text='ledger kind')
    menu = models.CharField(max_length=255, help_text='menu', default='',blank=True)
    is_standard = models.BooleanField(default=False, help_text='ledger kind')
    is_enabled = models.IntegerField(default=ENABLED,choices=IS_ENABLED)

    def save(self, *args, **kwargs):
       
        # create new bill
        if self._state.adding:
            # set bill number
            self.code = getCode()
        super().save(*args, **kwargs)
    