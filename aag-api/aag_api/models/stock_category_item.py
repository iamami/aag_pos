from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *

class StockCategoryItem(models.Model):
    category = models.ForeignKey(
        'Category', related_name='stock_category_item_category', help_text='bill category', on_delete=models.CASCADE)
    stock = models.ForeignKey(
        'StockCategory', related_name='stock_category_item_stock', help_text='StockProduct', on_delete=models.CASCADE)
    branch = models.ForeignKey(
        'Branch', related_name='stock_category_item_branch', help_text='branch of this staff', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='stock_category_user', on_delete=models.CASCADE, default=1)
    object_id = models.CharField(max_length=15, help_text='invoicd or bill', default='')
    weight = models.DecimalField(
        max_digits=20, decimal_places=3, help_text='weight per bath in grams', default=0)
    record_date = models.DateTimeField(auto_now_add=True, help_text='record_date')
    kind = models.CharField(max_length=2, choices=TYPE_STOCK, default='I', help_text='stock kind')
