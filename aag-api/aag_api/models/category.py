from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *
from django.core.validators  import RegexValidator
from django.core.validators  import RegexValidator,MinValueValidator,MaxValueValidator
from decimal import Decimal

class Category(models.Model):
    name = models.CharField(max_length=100, unique=True, help_text='category name, eg. goldbar, 96.5%')
    code = models.CharField(max_length=5, help_text='category code, eg. 96.5% = 965', unique=True,db_index=True,validators=[RegexValidator(r'^[0-9]*$',"รูปแบบไม่ถูกต้อง")])
    weight = models.DecimalField(
        max_digits=20, decimal_places=3, help_text='weight per bath in grams', default=0,validators=[MinValueValidator(Decimal(0))])
    m_buy = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='multiplier buy', default=0,validators=[MinValueValidator(0)])
    m_sell = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='multiplier sell', default=0,validators=[MinValueValidator(0)])
    discount_buy = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='discount buy', default=0)
    description = models.CharField(max_length=255, help_text='some description', default='' ,blank=True, null=True)
    is_enabled = models.IntegerField(default=ENABLED,choices=IS_ENABLED)
    def __str__(self):
        return  "%s (%s)" % (self.name,self.code)