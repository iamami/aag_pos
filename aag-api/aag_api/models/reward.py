from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *
from aag_api.models.counter import Counter
from versatileimagefield.fields import VersatileImageField, PPOIField
from mapi.models.device import Device

class Reward(models.Model):
    name = models.CharField(max_length=100, unique=True,help_text='Name')
    code = models.CharField(max_length=20, help_text='Code', unique=True,db_index=True,validators=[RegexValidator(r'^[0-9A-Z]*$',"รูปแบบไม่ถูกต้อง")])
    detail = models.CharField(max_length=500, help_text='Detail')
    score = models.IntegerField(help_text='Amount score for exchange', default=0)
    image = VersatileImageField(max_length=255,upload_to='aag_api/reward/',default='',)
    created_datetime = models.DateTimeField(auto_now_add=True)
    is_enabled = models.IntegerField(default=ENABLED,choices=IS_ENABLED)
    class Meta:
        ordering = ['-code']
    def __str__(self):

        return "%s %s" % (self.code,self.name)

    def save(self, *args, **kwargs):
        is_add = False
        if self._state.adding and (self.code is None or self.code == ''):
            c = Counter.getObject('reward')
            self.code = "R%06d" % (c.count)
            is_add = True

        super().save(*args, **kwargs)
        if is_add :
           
            device_list = Device.objects.all().filter(customer_user__is_alert_newreward=True)
            if device_list.count() > 0:
                title = "มีของรางวัลใหม่ %s " % (self.name)
                message = self.detail
                res = Device.sendNotiRewardNew(device_list,title,message,{
                    'type': 3,
                    'data': {
                        'code': self.code,
                        'id': self.id,
                        'name': self.name,
                        'detail': self.detail,
                        'score': self.score,
                        'created_datetime': str(self.created_datetime),
                        'image': self.image.url,
                        'id': self.pk

                    }
                },Reward.objects.get(pk=self.pk))