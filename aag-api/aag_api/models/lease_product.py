from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *

class LeaseProduct(models.Model):
    lease = models.ForeignKey(
        'Lease', related_name='lease_prodcut', help_text='Lease', on_delete=models.CASCADE)
    category = models.ForeignKey(
        'Category', related_name='lease_product_category', help_text='product category', on_delete=models.CASCADE)
    name = models.CharField(max_length=255, help_text='name product', default='')
    user = models.ForeignKey(
         User, related_name='lease_user', on_delete=models.CASCADE)
    weight = models.DecimalField(
        max_digits=20, decimal_places=3, help_text='buy or sell', default=0)
    record_date = models.DateTimeField(auto_now_add=True, help_text='record_date')
    amount = models.IntegerField(help_text='amount', default=1)
    status = models.IntegerField(help_text='status', choices=STATUS_LEASE, default=1)
    is_enabled = models.IntegerField(default=ENABLED,choices=IS_ENABLED)
    status_stock = models.CharField(max_length=2, choices=STATUS_STOCK, default='N', help_text='N ยังไม่อัพเดท,Y อัพเดทแล้ว')

    def __str__(self):
        return "%s" % self.name
