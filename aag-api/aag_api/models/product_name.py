from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *

class ProductName(models.Model):
    name = models.CharField(max_length=255, unique=True,db_index=True, help_text='action kind  to principle')
    is_enabled = models.IntegerField(default=ENABLED,choices=IS_ENABLED)
