from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *
from django.core.validators import ValidationError
from django.utils.translation import ugettext_lazy as _

class BillItem(models.Model):

    kind_choices = (
        (BUY, 'ซื้อ'),
        (SELL, 'ขาย')
    )
    bill = models.ForeignKey(
        'Bill', related_name='bill_item_bill', help_text='bill', on_delete=models.CASCADE)
    category = models.ForeignKey(
        'Category', related_name='bil_item_category', help_text='bill category', on_delete=models.CASCADE)
    #product_id = models.IntegerField(help_text='product', default=1)
    product = models.ForeignKey(
        'Product', related_name='bill_item_bill', help_text='Product', on_delete=models.CASCADE,blank=True, null=True)
    weight = models.DecimalField(
        max_digits=20, decimal_places=3, help_text='buy or sell', default=0)
    amount = models.IntegerField(help_text='amount', default=1)
    cost = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='cost / item', default=0)
    sell = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='total price for sell', default=0)
    buy = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='total price for buy', default=0)
    gold_price = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='total price for gold', default=0)
    code_change = models.CharField(max_length=20, help_text='change code', default='',blank=True, null=True)
    exchange = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='total price for change', default=0)
    kind = models.CharField(max_length=5, choices=kind_choices, default=BUY, help_text='BILL CHOICES')
    status_stock = models.CharField(max_length=2, choices=STATUS_STOCK, default='N', help_text='bill status')
    product_name = models.CharField(max_length=255, help_text='name prodcut', default='',blank=True, null=True)
    record_date = models.DateTimeField(auto_now_add=True, help_text='record_date')

    def __str__(self):

        if self.product is not None:
            return self.product.name
        else: 
            return self.product_name

    def full_clean(self, *args, **kwargs):

        super().full_clean(*args, **kwargs)
        if self.kind == BUY:
            if self.product_name is None or self.product_name == '':
                raise ValidationError({'product_name': _('This field cannot be null.')})