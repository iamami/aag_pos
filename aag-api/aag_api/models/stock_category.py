from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *

class StockCategory(models.Model):
    category = models.ForeignKey(
        'Category', related_name='stock_category_category', help_text='bill category', on_delete=models.CASCADE)
    branch = models.ForeignKey(
        'Branch', related_name='stock_category_branch', help_text='branch of this staff', on_delete=models.CASCADE)
    weight = models.DecimalField(
        max_digits=20, decimal_places=3, help_text='buy or sell', default=0)
    in_date = models.DateTimeField(auto_now_add=True, help_text='in_date')
    out_date = models.DateTimeField(auto_now_add=True, help_text='out_date')
    total = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='total', default=0)
    class Meta:
        ordering = ['category__code']