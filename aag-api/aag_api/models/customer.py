from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *
from django.core.validators import ValidationError
from django.core.validators  import RegexValidator
from django.utils.crypto import get_random_string
from aag_api.models.counter import Counter
from versatileimagefield.fields import VersatileImageField
from aag_api.models.score import *

class Customer(models.Model):
    regex_code = RegexValidator(r'^C[0-9]{6}$',"รูปแบบรหัสลูกค้าไม่ถูกต้อง")

    name = models.CharField(max_length=100, help_text='customer full name', blank=False ,db_index=True)
    code = models.CharField(max_length=20, help_text='customer code', unique=True,db_index=True,validators=[regex_code],blank=True, null=True)
    nick_name = models.CharField(max_length=100, help_text='customer nick name', default='',blank=True, null=True)
    citizen_id = models.CharField(max_length=20, help_text='citizen or passport number', blank=True)

    address = models.CharField(max_length=100, help_text='customer address', blank=True)
    district = models.CharField(max_length=100, help_text='district, แขวง/เขต', blank=True)
    city = models.CharField(max_length=100, help_text='city, อำเภอ', blank=True)
    province = models.CharField(max_length=100, help_text='provice', blank=True)
    postal_code = models.CharField(max_length=10, help_text='postal code', blank=True)

    address2 = models.CharField(max_length=100, help_text='customer address', blank=True)
    district2 = models.CharField(max_length=100, help_text='district, แขวง/เขต', blank=True)
    city2 = models.CharField(max_length=100, help_text='city, อำเภอ', blank=True)
    province2 = models.CharField(max_length=100, help_text='provice', blank=True)
    postal_code2 = models.CharField(max_length=10, help_text='postal code', blank=True)

    address3 = models.CharField(max_length=100, help_text='customer address', blank=True)
    district3 = models.CharField(max_length=100, help_text='district, แขวง/เขต', blank=True)
    city3 = models.CharField(max_length=100, help_text='city, อำเภอ', blank=True)
    province3 = models.CharField(max_length=100, help_text='provice', blank=True)
    postal_code3 = models.CharField(max_length=10, help_text='postal code', blank=True)

    phone = models.CharField(max_length=20, help_text='customer phone number', blank=True, null=True)
    email = models.EmailField(max_length=100, help_text='email', blank=True, null=True, default=None, unique=True)
    birth_date = models.DateField(help_text='birthdate', blank=True,null=True)
    gender = models.CharField(max_length=2, choices=GENDER_CHOICES, default=NA, help_text='gender')
    nationality = models.CharField(max_length=100, help_text='nationality', default='',blank=True, null=True)
    ethnicity = models.CharField(max_length=100, help_text='ethnicity', default='',blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True, help_text='created date')
    description = models.CharField(max_length=255, help_text='some description', default='',blank=True, null=True)
    point = models.DecimalField(
        max_digits=10, decimal_places=2, help_text='accomulate point', default=0)
    mobile = models.CharField(max_length=20, help_text='customer mobile number', blank=True, null=True,validators=[RegexValidator(r'^0[0-9]{9}$','เบอร์มือถือไม่ถูกต้อง')], default=None, unique=True)
    is_enabled = models.IntegerField(default=ENABLED,choices=IS_ENABLED)
    secret = models.CharField(max_length=32,blank=True, null=True,validators=[RegexValidator(r'^[a-z0-9]{7}$','รูปแบบไม่ถูกต้อง')])
    image = VersatileImageField(max_length=255,upload_to='aag_api/customer/',default='',blank=True, null=True)
    is_application = models.BooleanField(default=True)
    address_primary = models.IntegerField(default=ENABLED,choices=((1,'Address 1'),(2,'Address 2'),(3,'Address 3')))

    def createCodeSecurity(self):
        self.secret = get_random_string(length=32, allowed_chars='abcdefghijklmnopqrstuvwxyz0123456789')  
        self.save()
        return self.secret
    
    def __str__(self):
        return self.name+' - '+self.code

    # def full_clean(self, *args, **kwargs):

    #     super().full_clean(*args, **kwargs)
    #     raise ValidationError({'name' : 'xxxx'})

    def save(self, *args, **kwargs):
        if self._state.adding and (self.code is None or self.code == ''):
            c = Counter.getObject('customer')
            self.code = "C%06d" % (c.count)
            
            
        is_adding = self._state.adding
        o = super().save(*args, **kwargs)

        if is_adding :
            Counter.nextCounter('customer')
            Score.objects.create(customer=self)