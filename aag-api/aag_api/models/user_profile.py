from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *


class UserProfile(models.Model):
    
    staff = models.ForeignKey(
        'Staff', related_name='user_profile_staff', on_delete=models.CASCADE,blank=True, null=True,default=None)
    user = models.OneToOneField(User, related_name='user_profile', on_delete=models.CASCADE)
    role = models.CharField(max_length=5, choices=USER_ROLE, default=USER, help_text='user [u=User,a=Admin]')
    branch = models.ForeignKey(
        'Branch', related_name='user_profile_branch', on_delete=models.CASCADE)

    def __str__(self):

        return "%s (%s)" % (self.user.username,self.branch.name)
    
    class Meta:
        ordering= ('id',)
