from django.db import models
from django.core.validators  import RegexValidator,MinValueValidator,MaxValueValidator
from django.contrib.auth.models import User
from aag_api.models.option import *

class Counter(models.Model):

    name = models.CharField(max_length=55,primary_key=True,unique=True,validators=[RegexValidator(r'^[a-z0-9]*$','Value has an invalid format. It must be in [a-z0-9]')])
    count = models.IntegerField(validators=[MinValueValidator(0)],default=0,blank=True)

    def nextCounter(name):
        try:
            c = Counter.objects.get(name=name)
            c.count = c.count + 1
            c.save()
            return c
        except Counter.DoesNotExist:
            c = Counter(name=name,count = 2)
            c.save()
            return c
    def getObject(name):
        
        try:
            c = Counter.objects.get(name=name)
            c.count = c.count
            return c
        except Counter.DoesNotExist:
            c = Counter(name=name,count = 1)
            c.save()
            return c

    def __str__(self):
        return '%s %s' % (self.name,self.count)