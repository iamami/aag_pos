from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *

TYPE_REWARD_STOCK = (
    ('I','นำเข้า'),
    ('O','นำออก'),
    ('E','แลกของรางวัล')
)
class StockRewardItem(models.Model):
    stock_reward = models.ForeignKey(
        'StockReward', related_name='stock_reward_item', on_delete=models.CASCADE)
    redeem = models.ForeignKey(
        'Redeem', related_name='redeem_stock_reward_item', on_delete=models.CASCADE,blank=True,null=True)
    amount = models.IntegerField(default=1)
    user = models.ForeignKey(User, related_name='stock_reward_item_user', on_delete=models.CASCADE)
    amount = models.IntegerField(help_text='amount', default=1)
    before = models.IntegerField(help_text='amount before', default=1)
    after = models.IntegerField(help_text='amount after', default=1)
    record_date = models.DateTimeField(auto_now_add=True, help_text='record_date')
    kind = models.CharField(max_length=2, choices=TYPE_REWARD_STOCK)
    description = models.CharField(max_length=255, help_text='description')
