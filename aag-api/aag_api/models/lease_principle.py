from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *

class LeasePrinciple(models.Model):
    lease = models.ForeignKey(
        'Lease', related_name='LeasePrinciple_lease', help_text='Lease', on_delete=models.CASCADE)
    user = models.ForeignKey(
         User, related_name='LeasePrinciple_user', on_delete=models.CASCADE)
    staff = models.ForeignKey(
         'Staff', related_name='LeasePrinciple_staff', on_delete=models.CASCADE)
    ledger = models.ForeignKey(
         'Ledger', related_name='LeasePrinciple_ledger', on_delete=models.CASCADE ,blank=True, null=True)
    record_date = models.DateTimeField(auto_now_add=True, help_text='record_date')
    total = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='totla', default=0)
    balance = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='totla', default=0)
    kind = models.CharField(max_length=2, choices=TYPE_PRINCIPLE, default='IC', help_text='action kind  to principle')
    description = models.CharField(max_length=255, help_text='some description', default='')
