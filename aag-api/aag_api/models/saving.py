from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *
from datetime import date

class Saving(models.Model):
    number = models.CharField(max_length=20, help_text='Saving number', unique=True,db_index=True)
    bill_date = models.DateField(default=None, help_text='bill_date',blank=True, null=True)
    branch = models.ForeignKey(
        'Branch', related_name='saving_branch', help_text='branch', on_delete=models.CASCADE)
    record_date = models.DateTimeField(auto_now_add=True, help_text='record_date')
    user = models.ForeignKey(
         User, related_name='user_saveing', on_delete=models.CASCADE)
    customer = models.ForeignKey(
        'Customer', related_name='customer_saveing', on_delete=models.CASCADE)
    amount = models.DecimalField(
        max_digits=20, decimal_places=2, default=0)
    is_enabled = models.BooleanField(default=True)

    def __str__(self):

        return "%s (%s)" % (self.number,self.customer.name)
