from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *

class StockProduct(models.Model):
    product = models.ForeignKey(
        'Product', related_name='stock_product',on_delete=models.CASCADE)
    branch = models.ForeignKey(
        'Branch', related_name='stock_branch', on_delete=models.CASCADE)
    amount = models.IntegerField(help_text='จำนวน', default=1)
    in_date = models.DateTimeField(auto_now_add=True, help_text='in_date')
    out_date = models.DateTimeField(auto_now_add=True, help_text='out_date')

    class Meta:
        unique_together = ('product', 'branch',)
        ordering = ['product__name',]

    def __str__(self):
        return "%s - %s (%s)" % (self.product.name,self.branch.name,self.amount)
