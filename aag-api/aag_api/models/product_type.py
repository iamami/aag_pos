
from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *
from django.core.validators  import RegexValidator

class ProductType(models.Model):
    code = models.CharField(max_length=5, help_text='type code, eg. Ring = R',  unique=True,db_index=True,validators=[RegexValidator(r'^[0-9A-Z]*$',"รูปแบบไม่ถูกต้อง")])
    name = models.CharField(max_length=100, help_text='type name, eg. neck, bracelet')
    description = models.CharField(max_length=255, help_text='some description', default='',blank=True, null=True)
    category_id = models.IntegerField(help_text='category only', default=0,blank=True,null=True)
    is_enabled = models.IntegerField(default=ENABLED,choices=IS_ENABLED)
    def __str__(self):
        return "%s (%s)" % (self.name,self.code)
