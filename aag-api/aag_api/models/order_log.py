from django.db import models
from django.contrib.auth.models import User
from aag_api.models.customer import Customer
from django.core.validators  import RegexValidator
from django.utils.crypto import get_random_string
from aag_api.models.reward import Reward


STATUS_CHOICES = (
    ('PD','Pending'),
    ('AP','Approval'),
    ('CC','Cancel')
)
# Create your models here.
class RedeemLog(models.Model):

    redeem = models.ForeignKey('Redeem', related_name='redeem_log', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='bill_user', on_delete=models.CASCADE, default=1)
    remrak = models.CharField(max_length=255, help_text='remrak', default='',blank=True)
    created_datetime = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=2, choices=STATUS_CHOICES)

    def __str__(self):
        return '%s' % (self.user.username)