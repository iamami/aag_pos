from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *
from aag_api.models.stock_reward_item import StockRewardItem
from django.core.validators import MinValueValidator,MaxValueValidator

class StockReward(models.Model):
    reward = models.OneToOneField(
        'Reward', related_name='stock_Reward', on_delete=models.CASCADE)
    amount = models.IntegerField(validators=[MinValueValidator(0)])
    updated_datetime = models.DateTimeField()

    def __str__(self):
        return "%s (%s)" % (self.reward.name,self.amount)