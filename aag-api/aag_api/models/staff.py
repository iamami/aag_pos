from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *
from aag_api.models.counter import Counter
from aag_api.models.branch_setting import *
class Staff(models.Model):

    regex_code = RegexValidator(r'^[A-Z]{1}[0-9]{3}$',"รูปแบบรหัสพนักงานไม่ถูกต้อง")

    name = models.CharField(max_length=255, help_text='Staff name', error_messages=my_error_messages)
    code = models.CharField(max_length=10, help_text='Staff code', unique=True,db_index=True, error_messages=my_error_messages,validators=[regex_code],blank=True, null=True)
    branch = models.ForeignKey(
        'Branch', related_name='staff_branch', help_text='branch of this staff', on_delete=models.CASCADE, error_messages=my_error_messages)
    phone = models.CharField(max_length=20, help_text='staff phone number', default='',blank=True, null=True)
    address = models.CharField(max_length=255, help_text='staff address', default='',blank=True, null=True)
    is_enabled = models.IntegerField(default=ENABLED,choices=IS_ENABLED)

    def genNumber(self,branch):

        o = BranchSetting.objects.get(branch=branch.pk)
        number = o.staff_code + "%03d" % (o.staff_counter)
        o.staff_counter = o.staff_counter + 1
        o.save()
        return number

    def getNextNumber(branch):
        o = BranchSetting.objects.get(branch=branch.pk)
        number = o.staff_code + "%03d" % (o.staff_counter)
        return number

    class Meta:
        ordering= ('branch','code',)
    def __str__(self):

        return "%s (%s)" % (self.name,self.code)

    def full_clean(self, *args, **kwargs):

        if self._state.adding and (self.code is None or self.code == ''):
            l = Staff.objects.all().count()
            self.code = "E%03d" % (l +1)

        super().full_clean(*args, **kwargs)

    def save(self, *args, **kwargs):

        if self._state.adding:
            c = Counter.getObject('staff')
            self.code = self.genNumber(self.branch)

        super().save(*args, **kwargs)