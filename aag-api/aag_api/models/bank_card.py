from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *

class BankCard(models.Model):
    kind = models.CharField(max_length=100, help_text='Card kind eg. Debit', default='',error_messages=my_error_messages)
    fee = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='fee', default=0,error_messages=my_error_messages)
    bank = models.ForeignKey(
        'Bank', related_name='card_bank', help_text='product category', on_delete=models.CASCADE,error_messages=my_error_messages)
    is_enabled = models.IntegerField(default=ENABLED,choices=IS_ENABLED)
    def __str__(self):
        return self.kind
