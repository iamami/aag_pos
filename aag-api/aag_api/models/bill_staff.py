from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *

class BillStaff(models.Model):
    bill = models.ForeignKey(
        'Bill', related_name='billstaff_bill', help_text='bill', on_delete=models.CASCADE)
    record_date = models.DateTimeField(auto_now_add=True, help_text='record_date')
    staff = models.ForeignKey(
        'Staff', related_name='billstaff_staff', help_text='staff', on_delete=models.CASCADE)
