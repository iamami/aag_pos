
from django.core.validators import ValidationError
from django.core.validators  import RegexValidator

my_error_messages = {
    'unique': 'ถูกใช้งานแล้ว',
    'blank': 'ต้องไม่เป็นค่าว่าง',
    'null': 'ต้องไม่เป็นค่าว่าง',
    'required': 'ต้องไม่เป็นค่าว่าง'
}


# Create your models here.
#CONSTRAINs
MALE = 'MA'
FEMALE = 'FE'
NA = 'NA'
BUY = 'BU'
SELL = 'SE'
EXCHANGE = 'XC'
OWNER = 'O'
ADMIN = 'A'
USER = 'U'
IN = 'I'
OUT = 'O'
VOID = 'V'
EDIT = 'E'
TIN = 'TI'
TOUT = 'TO'
EJECT = 'EJ'

PAY_CARD = 'CD'
PAY_CASH = 'CS'
PAY_CHECK = 'CH'
PAY_CARD_CASH = 'CC'
PAY_TRANSFER = 'TF'
PAYMENT = (
    (PAY_CARD,'บัตร'),
    (PAY_CASH,'เงินสด'),
    (PAY_CHECK,'เช็ค'),
    (PAY_CARD_CASH,'บัตร+เงินสด'),
    (PAY_TRANSFER,'โอน'),
)

GENDER_CHOICES = (
    (MALE, 'ผู้ชาย'),
    (FEMALE, 'ผู้หญิง'),
    (NA, 'ไม่ระบุ'),
)

BILL_CHOICES = (
    (BUY, 'ซื้อ'),
    (SELL, 'ขาย'),
    (EXCHANGE, 'เปลี่ยน')
)

USER_ROLE = (
    (ADMIN, 'ผู้ดูแลระบบ'),
    (USER, 'ผู้ใช้งาน')
)

TYPE_SALE = (
    (1, 'งานชั่ง'),
    (2, 'งานชิ้น')
)

TYPE_WEIGHT = (
    (1, 'บาท'),
    (2, 'กรัม')
)

TYPE_INVOICE = (
    ('IM','นำเข้า'),
    ('EX','นำออก'),
    ('MV','โอนสินค้า')
)

TYPE_PRINCIPLE = (
    ('IC','เพิ่ม'),
    ('DC','ลด')
)

STATUS_INVOICE = (
    ('N','ยังไม่เคลียร์บิล'),
    ('Y','เคลียร์บิลแล้ว')
)

STATUS_STOCK = (
    ('N','ยังไม่อัพเดทสต๊อก'),
    ('Y','อัพเดทสต๊อกแล้ว')
)

STATUS_BILL = (
    ('C','ยกเลิก'),
    ('O','ปกติ')
)

STATUS_LEASE = (
    (1,'ใหม่'),
    (2,'ต่อดอก'),
    (3,'ไถ่คืน'),
    (4,'คัดออก')
)

TYPE_STOCK = (
    (IN,'นำเข้า'),
    (OUT,'ขาย'),
    (VOID,'ยกเลิกบิล'),
    (TIN,'โอนเข้า'),
    (TOUT,'โอนออก'),
    (EJECT,'คัดออก')
)

TYPE_LEDGER = (
    ('IN','รายรับ'),
    ('EX','รายจ่าย')
)

CALCULATE_DATE = (
    (1,'คิดเป็นรายวัน'),
    (2,'1/4 เดือน(1-7)'),
    (3,'1/2 เดือน(8-15)'),
    (4,'3/4 เดือน(16-22)'),
    (5,'1 เดือน (23-31)'),
    (6,'ไม่คิด')
)

SETTING_KEY =(
    ('CUSF','ตั้งค่าข้อมูล ลูกค้า'),
    ('SCOR','ตัวคูณคะแนน (ค่าเริ่มต้น 10)'),
)

ENABLED = 1
DISABLED = 0
IS_ENABLED = (
    (ENABLED,'แสดง'),
    (DISABLED,'ซ่อน')
)

IS_USER_EDIT_INTEREST_CHOICES = (
    (0,'ไม่สามารถแก้ไขอัตราดอกเบี้ยได้'),
    (1,'สามารถแก้ไขอัตราดอกเบี้ยได้')
)