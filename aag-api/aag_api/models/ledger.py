from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *
from django.core.validators  import RegexValidator
from datetime import datetime 

def getBillNumber(kind,branch,date):

    current_month = date.month
    current_year = date.year

    pre = ''
    if kind == 'IN':
        pre = 'RE'
    elif kind == 'EX':
        pre = 'PA'

    l = len(Ledger.objects.all().filter(ledger_date__month=current_month,ledger_date__year=current_year,kind=kind,branch=branch))+1

    branch = str(branch)
    branch = ( '0' if int(branch) < 10 else '') + branch

    no = "%05d" % (l,)
    return "%s-%s%s%s" % (pre ,branch, date.strftime('%y%m') , no)

class Ledger(models.Model):
    branch = models.ForeignKey(
        'Branch', related_name='ledger_branch', help_text='ledger of branch', on_delete=models.CASCADE)
    object_id = models.IntegerField(help_text='object id', default=0,blank=True, null=True)
    staff = models.ForeignKey(
        'Staff', related_name='ledger_staff', on_delete=models.CASCADE,blank=True, null=True)
    customer = models.ForeignKey(
         'Customer', related_name='customer_ledger', on_delete=models.CASCADE,null=True,blank=True)
    user = models.ForeignKey(
         User, related_name='user_ledger', on_delete=models.CASCADE)
    old_number = models.CharField(max_length=20, blank=True, null=True)
    number = models.CharField(max_length=20, help_text='Bill number', unique=True,db_index=True,blank=True)
    object_number = models.CharField(max_length=20, help_text='object code', default='',blank=True, null=True)
    record_date = models.DateTimeField(auto_now_add=True, help_text='record_date')
    ledger_date = models.DateField(help_text='ledger_date',auto_now_add=True)
    kind = models.CharField(max_length=2, choices=TYPE_LEDGER, default='IN', help_text='ledger kind')
    total = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='total price for income or expenses', default=0)
    ledger_category = models.ForeignKey(
        'LedgerCategory', related_name='ledger_category', help_text='ledger category', on_delete=models.CASCADE)
    bankcard = models.IntegerField(help_text='card id', default=0)
    bank_account = models.IntegerField(help_text='bank accounts', default=0)
    cash = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='total price for cash', default=0)
    card = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='total price for card', default=0)
    check = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='total price for check', default=0)
    transfer = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='total price for cash', default=0)
    transfer_bank_account = models.ForeignKey(
        'BankAccount', related_name='bank_account', on_delete=models.CASCADE,blank=True, null=True , default=None)
    card_bank_card = models.ForeignKey(
        'BankCard', related_name='card_bank_card', on_delete=models.CASCADE,blank=True, null=True , default='')
    card_fee = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='fee for card', default=0)
    card_service = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='service for card', default=0)
    card_contract_number = models.CharField(max_length=255, help_text='card contract number', default='',blank=True, null=True)
    card_code = models.CharField(max_length=255, help_text='Card number', default='',blank=True, null=True, validators=[RegexValidator(r'^[0-9\-]*$','เฉพาะตัวเลขหรือ -')])
    card_period = models.IntegerField(help_text='card period', default=0)
    card_start = models.DateField(help_text='bill_date',blank=True, null=True)
    check_bank = models.IntegerField(help_text='id bank for check bank', default=0)
    check_code = models.CharField(max_length=25, help_text='Check number', default='',blank=True, null=True , validators=[RegexValidator(r'^[0-9\-]*$','เฉพาะตัวเลขและ -')])
    check_date = models.DateField(help_text='check date',blank=True, null=True)
    payment = models.CharField(max_length=5, choices=PAYMENT, default=PAY_CASH, help_text='payment')
    description = models.CharField(max_length=255, help_text='some description', default='',blank=True, null=True)
    status = models.IntegerField(help_text='ยกเลิก 0, ปกติ 1', default=1)

    def save(self, *args, **kwargs):
       
        # create new bill
        if self._state.adding:
            # set bill number
            self.number = getBillNumber(self.kind ,self.branch.id,datetime.now())
        super().save(*args, **kwargs)