from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *
from datetime import datetime 

def getBillNumber(branch,date):

    current_month = date.month
    current_year = date.year

    l = len(BillCategory.objects.all().filter(bill_date__month=current_month,bill_date__year=current_year,branch=branch))+1

    branch = str(branch)
    branch = ( '0' if int(branch) < 10 else '') + branch

    no = "%05d" % (l,)
    return "%s-%s%s%s" % ('OC' ,branch, date.strftime('%y%m') , no)

class BillCategory(models.Model):
    old_number = models.CharField(max_length=20, blank=True, null=True)
    bill_number = models.CharField(max_length=20, help_text='Bill number', unique=True,db_index=True,blank=True)
    branch = models.ForeignKey(
        'Branch', related_name='billcategory_branch', help_text='bill of branch', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='bill_category_user', on_delete=models.CASCADE)
    vendor = models.ForeignKey('Vendor', related_name='bill_category_vendor', on_delete=models.CASCADE,default='',blank=True, null=True)
    bill_date = models.DateField(default=datetime.now, help_text='bill_date')
    update_date = models.DateTimeField( help_text='stock_date', blank=True,default=None,null=True)
    weight = models.DecimalField(
        max_digits=20, decimal_places=3, help_text='buy or sell', default=0)
    total = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='total price', default=0)
    cost = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='cost price', default=0)
    gold_sell = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='gold price', default=0)
    net = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='total price', default=0)
    record_date = models.DateTimeField(auto_now_add=True, help_text='record_date')
    status_bill = models.CharField(max_length=2, choices=STATUS_INVOICE, default='N', help_text='bill status')
    status_stock = models.CharField(max_length=2, choices=STATUS_STOCK, default='N', help_text='N ยังไม่อัพเดท,Y อัพเดทแล้ว')
    description = models.CharField(max_length=255, help_text='some description', default='',blank=True, null=True)

    class Meta:
        ordering = ['bill_number']

    def save(self, *args, **kwargs):
       
        # create new bill
        if self._state.adding:
            # set bill 
            self.full_clean()
            self.bill_number = getBillNumber(self.branch.id,self.bill_date)
        super().save(*args, **kwargs)