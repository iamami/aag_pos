from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *
from django.core.validators import MaxValueValidator, MinValueValidator
from decimal import Decimal

class InvoiceItem(models.Model):
    invoice = models.ForeignKey(
        'Invoice', related_name='item_invoice', help_text='invoice', on_delete=models.CASCADE)
    product = models.ForeignKey(
        'Product', related_name='item_product', help_text='product', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='invoice_item_user', on_delete=models.CASCADE,default=1)
    record_date = models.DateTimeField(auto_now_add=True, help_text='record_date')
    status_stock = models.CharField(max_length=2, choices=STATUS_STOCK, default='N', help_text='bill status')
    update_date = models.DateTimeField(auto_now_add=True, help_text='stock_date', blank=True)
    weight = models.DecimalField(
        max_digits=20, decimal_places=3, help_text='weight per bath in grams', default=0,validators=[MinValueValidator(Decimal('0.0'),'ต้องมากกว่า 0')])
    weight_total = models.DecimalField(
        max_digits=20, decimal_places=3, help_text='Total weight per bath in grams', default=0,validators=[MinValueValidator(Decimal('0.0'),'ต้องมากกว่า 0')])
    weight_real = models.DecimalField(
        max_digits=20, decimal_places=3, help_text='Real scales', default=0,validators=[MinValueValidator(Decimal('0.0'),'ต้องมากกว่า 0')])
    amount = models.IntegerField(help_text='จำนวน', default=1,validators=[MinValueValidator(0,'ต้องมากกว่า 0')])
    is_enabled = models.IntegerField(default=ENABLED,choices=IS_ENABLED)

    class Meta:
        unique_together = ('invoice', 'product',)