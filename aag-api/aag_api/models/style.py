from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *

class Style(models.Model):
    name = models.CharField(max_length=100, help_text='style name, eg. เกรียวเชือก, เกร็ดดาว', default='')
    description = models.CharField(max_length=255, help_text='some description', default='')
    def __str__(self):
        return self.name
