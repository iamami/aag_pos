from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *
from datetime import datetime,date
from django.utils import timezone

def getBillNumber(kind,branch,date):

    current_month = date.month
    current_year = date.year

    pre = ''
    if kind == BUY:
        pre = 'BU'
    elif kind == SELL:
        pre = 'SE'
    elif kind == EXCHANGE:
        pre = 'CH'

    l = len(Bill.objects.all().filter(bill_date__month=current_month,bill_date__year=current_year,kind=kind,branch=branch))+1

    branch = str(branch)
    branch = ( '0' if int(branch) < 10 else '') + branch

    no = "%05d" % (l,)
    return "%s-%s%s%s" % (pre ,branch, date.strftime('%y%m') , no)


class Bill(models.Model):
    old_number = models.CharField(max_length=20, blank=True, null=True)
    bill_number = models.CharField(max_length=20, help_text='Bill number', unique=True,db_index=True,blank=True)
    tax_number = models.CharField(max_length=20, help_text='Tax number', default='',blank=True)
    branch = models.ForeignKey(
        'Branch', related_name='bill_branch', on_delete=models.CASCADE)
    customer = models.ForeignKey('Customer', related_name='bill_customer', on_delete=models.CASCADE,blank=True, default=1)
    user = models.ForeignKey(User, related_name='bill_user', on_delete=models.CASCADE, default=1)
    record_date = models.DateTimeField(auto_now_add=True, help_text='record_date')
    sell = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='total price for sell', default=0)
    buy = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='total price for buy', default=0)
    exchange = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='total price for exchange', default=0)
    total = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='total price', default=0)
    discount = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='discount', default=0)    
    amount = models.IntegerField(help_text='amount', default=1)
    status_stock = models.CharField(max_length=2, choices=STATUS_STOCK, default='N', help_text='bill status')
    status = models.CharField(max_length=5, choices=STATUS_BILL, default='O', help_text='O - Open , C - CANCEL')
    description = models.CharField(max_length=255, help_text='description', default='',blank=True)
    kind = models.CharField(max_length=5, choices=BILL_CHOICES, default=BUY, help_text='BILL CHOICES')
    canceler = models.IntegerField(help_text='staff id', default=0)
    is_void = models.IntegerField(help_text='1 = true 0 = false', default=0)
    bill_date = models.DateTimeField(default=datetime.now, help_text='bill date')
    weight_buy = models.DecimalField(
        max_digits=20, decimal_places=3, help_text='weight buy', default=0)
    payment = models.CharField(max_length=5, choices=PAYMENT, default=PAY_CASH, help_text='payment')
    staffs = models.CharField(max_length=255,  default='', help_text='staffs',blank=True)
    gold_price = models.ForeignKey(
        'GoldPrice', related_name='gold_price', help_text='GoldPrice', on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
       
        # create new bill
        if self._state.adding:
            # set bill number
            self.full_clean()
            self.bill_number = getBillNumber(self.kind ,self.branch.id,self.bill_date)
        super().save(*args, **kwargs)