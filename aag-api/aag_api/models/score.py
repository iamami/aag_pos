from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *


class Score(models.Model):

    customer = models.OneToOneField('Customer', related_name='score_customer', on_delete=models.CASCADE)
    sell = models.IntegerField(help_text='accomulate point from sell', default=0)
    saving = models.IntegerField(help_text='accomulate point from saving', default=0)
    interest = models.IntegerField(help_text='accomulate point from interrest', default=0)
    total = models.IntegerField(help_text='total accumulated point', default=0)
    use = models.IntegerField(help_text='accomulate point from exchange', default=0)
    created_datetime = models.DateTimeField(auto_now_add=True) 

    def __str__(self):

        return "%s (%s)" % (self.customer.name,self.total)