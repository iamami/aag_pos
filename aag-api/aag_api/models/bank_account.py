from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *

class BankAccount(models.Model):
    name = models.CharField(max_length=100, help_text='account name',  unique=True,db_index=True,error_messages=my_error_messages)
    number = models.CharField(max_length=14, help_text='account number',  unique=True,db_index=True,error_messages=my_error_messages)
    bank = models.ForeignKey(
        'Bank', related_name='bank_account', help_text='product category', on_delete=models.CASCADE)
    is_enabled = models.IntegerField(default=ENABLED,choices=IS_ENABLED)
    def __str__(self):
        return self.name
