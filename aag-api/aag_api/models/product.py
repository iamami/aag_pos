from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *
from django.core.validators  import RegexValidator
from django.core.validators import MaxValueValidator, MinValueValidator
from decimal import Decimal

class Product(models.Model):
    name = models.CharField(max_length=100, help_text='product name, eg. neck 2 baht')
    code = models.CharField(max_length=20, help_text='product code, eg. nt22b', unique=True,db_index=True,validators=[RegexValidator(r'^[0-9A-Z.]*$',"รูปแบบไม่ถูกต้อง")])
    category = models.ForeignKey(
        'Category', related_name='product_category', help_text='product category', on_delete=models.CASCADE)
    kind = models.ForeignKey(
        'ProductType', related_name='product_kind', help_text='product kind', on_delete=models.CASCADE)
    type_weight = models.IntegerField( choices=TYPE_WEIGHT, default=1, help_text='1 = บาท , 2 กรัม')
    weight = models.DecimalField(
        max_digits=20, decimal_places=3, help_text='1 bath ratio, eg. 1 บาท = 0.15.20 g', default=0)
    type_sale = models.IntegerField(choices=TYPE_SALE,default=1, help_text='1 = งานชั่ง , 2 งานชิ้น')
    price_tag = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='product price tag', default=0,validators=[MinValueValidator(Decimal('0.0'),'ต้องมากกว่า 0')])
    cost = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='product cost', default=0,validators=[MinValueValidator(Decimal('0.0'),'ต้องมากกว่า 0')])
    is_enabled = models.IntegerField(default=ENABLED,choices=IS_ENABLED)
    description = models.CharField(max_length=255, help_text='some description', default='',blank=True, null=True)
    score = models.DecimalField(max_digits=10, decimal_places=2, help_text='score', default=0,validators=[MinValueValidator(Decimal('0.0'),'ต้องมากกว่า 0')])
    class Meta:
        ordering = ['name']
    def __str__(self):
        return self.name+'('+self.code+')'
