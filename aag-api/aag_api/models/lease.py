from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *
from aag_api.models.ledger import *
from aag_api.models.branch_setting import *
from datetime import datetime 

class Lease(models.Model):
    number = models.CharField(max_length=20, help_text='Ledger number',db_index=True,blank=True, null=True)
    bill_date = models.DateField(auto_now_add=True, help_text='bill date')
    branch = models.ForeignKey(
        'Branch', related_name='Lease_branch', help_text='branch', on_delete=models.CASCADE)
    record_date = models.DateTimeField(auto_now_add=True, help_text='record date')
    user = models.ForeignKey(
         User, related_name='Lease_user', on_delete=models.CASCADE)
    staff = models.ForeignKey(
        'Staff', related_name='Lease_staff', help_text='staff', on_delete=models.CASCADE,blank=True, null=True)
    customer = models.ForeignKey(
        'Customer', related_name='Lease_customer', on_delete=models.CASCADE)
    citizen_id = models.CharField(max_length=20, help_text='customer citizen id', default='')
    phone = models.CharField(max_length=20, help_text='customer phone number', default='')
    amount = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='amount total', default=0)
    weight = models.DecimalField(
        max_digits=20, decimal_places=3, help_text='weight total', default=0)
    month = models.IntegerField(help_text='number of months to instalment', default=0)
    interest = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='interest %', default=0)
    start_date = models.DateField(default=datetime.now, help_text='นำเข้า')
    interest_date = models.DateField(default=datetime.now, help_text='วันที่ต่อดอกล่าสุด')
    end_date = models.DateField(default=datetime.now, help_text='วันครบกำหนด')
    close_date = models.DateField( help_text='ไถ่คืน',blank=True, null=True)
    out_date = models.DateField( help_text='คัดออก',blank=True, null=True)
    status = models.IntegerField(help_text='status', choices=STATUS_LEASE, default=1)
    total_interest = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='amount', default=0)    
    description = models.CharField(max_length=255, help_text='some description', default='',blank=True)
    is_enabled = models.IntegerField(default=ENABLED,choices=IS_ENABLED,help_text='0=ยกเลิก,1=ปกติ')
    status_stock = models.CharField(max_length=2, choices=STATUS_STOCK, default='N', help_text='N ยังไม่อัพเดท,Y อัพเดทแล้ว')

    def genNumber(self,branch):

        o = BranchSetting.objects.get(branch=branch.pk)
        number = o.code + "%05d" % (o.counter)
        o.counter = o.counter + 1
        o.save()
        return number

    def getNextNumber(branch):
        o = BranchSetting.objects.get(branch=branch.pk)
        number = o.code + "%05d" % (o.counter)
        return number

    def save(self, *args, **kwargs):
       
        if self.is_enabled == 0 :
            ledger = Ledger.objects.all().filter(object_id=self.id)
            if ledger.count()>0:
                for l in ledger:
                    l.status = 0
                    l.save()
        super().save(*args, **kwargs)