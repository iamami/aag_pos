from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *

class Bank(models.Model):
    name = models.CharField(max_length=255, help_text='some description',  unique=True,db_index=True,error_messages=my_error_messages)
    is_enabled = models.IntegerField(default=ENABLED,choices=IS_ENABLED)
    def __str__(self):
        return self.name
  