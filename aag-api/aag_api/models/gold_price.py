from django.db import models
from datetime import datetime

class GoldPrice(models.Model):
    #user = models.ForeignKey(User, related_name='GoldPrice_user', on_delete=models.CASCADE)
    record_date = models.DateTimeField(auto_now_add=True, help_text='record_date')
    gold_bar_buy = models.DecimalField(max_digits=20, decimal_places=2, help_text='gold_bar_buy', default=0)
    gold_bar_sell = models.DecimalField(max_digits=20, decimal_places=2, help_text='gold_bar_sell', default=0)
    gold_ornaments_buy = models.DecimalField(max_digits=20, decimal_places=2, help_text='gold_ornaments_buy', default=0)
    gold_ornaments_sell = models.DecimalField(max_digits=20, decimal_places=2, help_text='gold_ornaments_sell', default=0)
