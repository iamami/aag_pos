from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *
from aag_api.models.counter import Counter
from aag_api.models.branch_setting import *
from django.core.validators  import RegexValidator

char = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

class Branch(models.Model):
    name = models.CharField(max_length=100, help_text='branch name', unique=True , error_messages=my_error_messages)
    code = models.CharField(max_length=15, help_text='branch code', unique=True,db_index=True,error_messages=my_error_messages,validators=[RegexValidator(r'^[0-9\-]{3}$',"รูปแบบไม่ถูกต้อง")])
    description = models.CharField(max_length=255, help_text='some description', default='',blank=True,null=True)
    is_enabled = models.IntegerField(default=ENABLED,choices=IS_ENABLED)
    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):

        if self._state.adding:
            Counter.nextCounter('branch')
       
        # create new bill
        super().save(*args, **kwargs)
        l = BranchSetting.objects.all().filter(branch=self.pk)
        if l.count() == 0 :
            o = BranchSetting()
            o.branch = self
            o.counter = 1
            o.month = 3
            o.interest = 2
            o.min_interest = 100
            o.is_user_edit_interest = 1
            o.calculate_date = 1
            o.save()
        