from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *


class BillCategoryItem(models.Model):
    bill_category = models.ForeignKey(
        'BillCategory', related_name='billcategory_item', help_text='bill item of branch', on_delete=models.CASCADE)
    stock = models.ForeignKey(
        'StockCategory', related_name='billcategory_item_stock', help_text='StockProduct', on_delete=models.CASCADE)
    weight = models.DecimalField(
        max_digits=20, decimal_places=3, help_text='buy or sell', default=0)
    price = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='price', default=0)
    total = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='total price', default=0)
    branch = models.ForeignKey(
        'Branch', related_name='billcategory_item_branch', help_text='branch', on_delete=models.CASCADE)
    cost = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='cost price', default=0)
    average = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='gold average', default=0) 
    total_average = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='total gold average', default=0)   
    status_stock = models.CharField(max_length=2, choices=STATUS_STOCK, default='N', help_text='N ยังไม่อัพเดท,Y อัพเดทแล้ว')
    record_date = models.DateTimeField(auto_now_add=True, help_text='record_date')
    update_date = models.DateTimeField(auto_now_add=True, help_text='stock_date', blank=True)

    # class Meta:
    #     unique_together = [['bill_category', 'branch', 'stock']]