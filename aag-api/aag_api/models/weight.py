from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *

class Weight(models.Model):
    name = models.CharField(max_length=20, help_text='weight name, eg. 1 สลึง, 1 บาท', default='')
    ratio = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='1 bath ratio, eg. 1 สลึง = 0.25 บาท', default=0)

    def __str__(self):
        return self.name
