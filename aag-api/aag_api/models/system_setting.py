from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *


class SystemSetting(models.Model):
    setting = models.CharField(max_length=4, default='CUSF', choices=SETTING_KEY, help_text='action kind  to principle')
    value = models.TextField(help_text='customer field', default='')

    def getSettingScore():
        r = SystemSetting.objects.get(setting='SCOR')
        return r.value