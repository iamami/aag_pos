from django.db import models
from django.contrib.auth.models import User
from aag_api.models.option import *
from datetime import datetime 

class LeaseInterest(models.Model):
    lease = models.ForeignKey(
        'Lease', related_name='leaseInterest_lease', help_text='Lease', on_delete=models.CASCADE)
    user = models.ForeignKey(
         User, related_name='leaseinterest_user', on_delete=models.CASCADE)
    pay_date = models.DateField(help_text='pay_date',null=True,blank=True)
    interest_date = models.DateField(default=datetime.now, help_text='interest_date')
    ledger = models.ForeignKey(
         'Ledger', related_name='leaseinterest_ledger', on_delete=models.CASCADE,null=True,blank=True)
    record_date = models.DateTimeField(auto_now_add=True, help_text='record_date')
    total = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='totla', default=0)
    total_receive = models.DecimalField(
        max_digits=20, decimal_places=2, help_text='totla', default=0)
    status = models.IntegerField(help_text='status', choices=STATUS_LEASE, default=1)
    description = models.CharField(max_length=255, help_text='description', default='')
