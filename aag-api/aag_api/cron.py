from aag_api.models import *
import requests
from pyquery import PyQuery as pq
from datetime import datetime , date , time
from django.utils import timezone
from datetime import timedelta
from fcm_django.models import FCMDevice
from mapi.models.device import Device

def auto_submit_balance():
    
    branch = Branch.objects.all().filter(is_enabled=ENABLED)

    for b in branch:

        ledger = Ledger.objects.all().filter(ledger_date=date.today()+timedelta(days=-1),branch=b.pk,status=1)
        balance = 0
        for l in ledger:
            if l.kind == 'IN':
                balance += l.cash
            elif l.kind == 'EX':
                balance -= l.cash

        o = Ledger()
        o.ledger_date = date.today()
        o.branch = b
        o.object_id = None
        o.object_number = ''
        o.kind = 'IN'
        o.total = balance
        o.cash = balance
        o.ledger_category = LedgerCategory.objects.get(pk=12)
        o.user = User.objects.get(pk=1)
        o.description = 'auto save'
        o.full_clean()
        o.save()

def clenText(str):
    str = str.replace('n/a','0').replace(',','')
    return float(str)

def gold_price_log():
    data = {}
    r = requests.get('https://www.goldtraders.or.th/DailyPrices.aspx')
    d = pq(r.text)
    table = d('#DetailPlace_MainGridView')
    bar965_sell_baht = clenText(table.find('tr').eq(1).find('td').eq(3).text())
    bar965_buy_baht = clenText(table.find('tr').eq(1).find('td').eq(2).text())
    ornament965_sell_baht = clenText(table.find('tr').eq(2).find('td').eq(3).text())
    ornament965_buy_baht = clenText(table.find('tr').eq(2).find('td').eq(2).text())
    ornament90_sell_baht = clenText(table.find('tr').eq(3).find('td').eq(3).text())
    ornament90_buy_baht = clenText(table.find('tr').eq(3).find('td').eq(2).text())
    ornament9999_sell_baht = clenText(table.find('tr').eq(7).find('td').eq(3).text())
    ornament9999_buy_baht = clenText(table.find('tr').eq(7).find('td').eq(2).text())

    if bar965_sell_baht==0 or bar965_buy_baht ==0 or ornament965_sell_baht == 0 or ornament965_buy_baht == 0:
        return True 

    if len(GoldPrice.objects.all()) > 0 :
        last_goldprice = GoldPrice.objects.latest('id')
        if float(last_goldprice.gold_bar_buy) == bar965_buy_baht and ornament965_buy_baht == float(last_goldprice.gold_ornaments_buy) :
            print('no change')
            return True        

    price = GoldPrice()
    price.record_datetime = timezone.now()
    price.gold_bar_buy = bar965_buy_baht
    price.gold_bar_sell = bar965_sell_baht
    price.gold_ornaments_buy = ornament965_buy_baht
    price.gold_ornaments_sell= ornament965_sell_baht
    
    price.save()

    from_date = datetime.today()
    date_min = datetime.combine(from_date, time.min)
    date_max = datetime.combine(from_date, time.max)
    
    n = GoldPrice.objects.all().filter(record_date__range=(date_min, date_max)).count()

    # send notification to customer
    data = {
        "gold_bar_buy": price.gold_bar_buy,
        "gold_bar_sell": price.gold_bar_sell,
        "gold_bar_sell": price.gold_bar_sell,
        "gold_ornaments_buy": price.gold_ornaments_buy,
        "gold_ornaments_sell": price.gold_ornaments_sell,
        "record_datetime": str(price.record_datetime)
        }
    
    device_list = Device.objects.all().filter(customer_user__is_alert_gold_price=True)
    date = timezone.now().strftime("%d/%m/%Y")
    title = "ราคาทองเปลี่ยนแปลง"
    message = "ประจำวันที่ %s ครั้งที่ %s" % (date,n)
    res = Device.sendNotiGoldPrice(device_list,title,message,{  'type': 1,  'data': data  },price)
    return True