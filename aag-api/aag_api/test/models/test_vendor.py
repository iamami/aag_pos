from django.test import TestCase
from aag_api.models import *
from django.core.validators import ValidationError
from django.utils import timezone
from model_mommy import mommy

class VendorModelTest(TestCase):

    def test_model_vendor_valid(self):
        o = mommy.make(Vendor)
        o.full_clean()
        o.save()
        self.assertEqual(o.name,str(o))

    def test_model_vendor_none_fail(self):
        fields = ['name']
        for f in fields:
            Product.objects.all().delete()
            o = mommy.make(Product,code="N96B100")
            setattr(o, f, None)
            with self.assertRaisesMessage(ValidationError,"{'name': ['ฟิลด์นี้ไม่สารถปล่อยว่างได้']}"):
                o.full_clean()