from django.test import TestCase
from aag_api.models import *
from django.core.validators import ValidationError
from django.utils import timezone
from aag_api.test.helper import *
from model_mommy import mommy
class BankAccountModelTest(TestCase):

    def test_model_bank_account_valid(self):
        o = mommy.make(BankAccount)
        o.full_clean()
        o.save()
        self.assertEqual(o.name,str(o))
        self.assertEqual(len(o.number),14)
        self.assertEqual(o.is_enabled,1)

    def test_model_bank_account_none_fail(self):

        fields = ['kind','fee','bank']
        for f in fields:
            o = mommy.make(BankCard)
            setattr(o, f,None)
            with self.assertRaisesMessage(ValidationError,"ต้องไม่เป็นค่าว่าง"):
                o.full_clean()
