from django.test import TestCase
from aag_api.models import *
from django.core.validators import ValidationError
from django.utils import timezone
from aag_api.test.helper import *
from aag_api.models.option import *
from django.contrib.auth.models import User
from datetime import date

from django.test import TestCase
from aag_api.models import *
from django.core.validators import ValidationError
from django.utils import timezone
from aag_api.test.helper import *
from aag_api.models.option import *
from django.contrib.auth.models import User
from datetime import date

def createObject():
    p = createGoldPrice()
    bill = createBillEmpty()
    product = createProduct()

    o = BillItem()
    o.bill = bill
    o.category = product.category
    o.product = product
    o.weight = product.weight
    o.amount = 1
    o.cost = 0
    o.sell = 19000
    o.buy = 0
    o.gold_price = p.gold_bar_sell
    o.kind = SELL
    o.status_stock = 'Y'

    return o

class BillItemModelTest(TestCase):
    fixtures = ['group.json','user.json','oauth2_provider.json','gold_price.json','user.json','initial_data.json']
    def setUp(self):
        self.ym = date.today().strftime('%y%m')
        pass

    def test_model_bill_item_valid(self):
        o = createObject()
        o.full_clean()
        o.save()
        self.assertIsNotNone(o.id)
    
    def test_model_bill_item_none_fail(self):

        fields = ['bill','category','weight','amount','cost','sell','buy','gold_price','kind','status_stock']
        for f in fields:
            o = createObject()
            setattr(o, f, None)
            with self.assertRaisesMessage(ValidationError,"{'%s': ['ฟิลด์นี้ไม่สารถปล่อยว่างได้']}" % (f)):
                o.full_clean()

    def test_model_bill_item_product_name_none_fail(self):

        # if kind is BU the produt_name is not None
        o = createObject()
        o.kind = BUY
        o.product_name = ''
        
        with self.assertRaisesMessage(ValidationError,"{'product_name': ['ฟิลด์นี้ไม่สารถปล่อยว่างได้']}"):
            o.full_clean()