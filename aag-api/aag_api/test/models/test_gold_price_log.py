from django.test import TestCase
from aag_api.models import *
from django.core.validators import ValidationError
from django.utils import timezone

# def createObject():
#     o = GoldPriceLog()
#     o.ornament90_sell_baht = '20000.00'
#     o.ornament90_buy_baht = '20000.00'
#     o.ornament965_sell_baht = '20000.00'
#     o.ornament965_buy_baht = '20000.00'
#     o.ornament9999_sell_baht = '20000.00'
#     o.ornament9999_buy_baht = '20000.00'
#     o.bar965_sell_baht = '20000.00'
#     o.bar965_buy_baht = '20000.00'
#     o.bar9999_sell_baht = '20000.00'
#     o.bar9999_buy_baht = '20000.00'
#     o.association965_sell_baht = '20000.00'
#     o.association965_buy_baht = '20000.00'
#     o.association9999_sell_baht = '20000.00'
#     o.association9999_buy_baht = '20000.00'
#     o.bar9999_sell_baht = '20000.00'
#     o.bar9999_sell_baht = '20000.00'
#     o.bar9999_sell_baht = '20000.00'
#     o.bar9999_sell_baht = '20000.00'
#     o.price_datetime = timezone.now()
#     return o

# # class GoldPriceLogModelTest(TestCase):

#     def test_model_goldPriceLog_valid(self):

#         fields = GoldPriceLog._meta.get_fields()
#         o = createObject()
#         o.full_clean()
#         o.save()

#         for f in fields:
#             if f.name not in ('id','created_datetime','price_datetime'):
#                 self.assertEqual(str(getattr(o,f.name)),'20000.00')

#     def test_model_goldPriceLog_null_fail(self):

#         fields = GoldPriceLog._meta.get_fields()
#         for f in fields:
#             if f.name not in ('id','created_datetime','price_datetime'):
#                 o = createObject()
#                 setattr(o, f.name , None)
#                 with self.assertRaisesMessage(ValidationError,'This field cannot be null.'):
#                     o.full_clean()

#     def test_model_goldPriceLog_max_length(self):

#         fields = GoldPriceLog._meta.get_fields()
#         for f in fields:
#             if f.name not in ('id','created_datetime','price_datetime'):
#                 o = createObject()
#                 setattr(o, f.name , '100000000000000000.01')
#                 o.full_clean()
#                 o.save()
#                 self.assertEqual(str(getattr(o,f.name)),'100000000000000000.01')

#     def test_model_goldPriceLog_format_fail(self):

#         fields = GoldPriceLog._meta.get_fields()
#         for f in fields:
#             if f.name not in ('id','created_datetime','price_datetime'):
#                 o = createObject()
#                 setattr(o, f.name , 'test')
#                 with self.assertRaisesMessage(ValidationError,"'test' value must be a decimal number."):
#                     o.full_clean()

#     def test_model_goldPriceLog_created_datetime_auto_now_add(self):
#         o = createObject()
#         o.full_clean()
#         o.save()
#         self.assertIsNotNone(o.created_datetime)

#     def test_model_goldPriceLog_price_datetime_auto_now_add(self):
#         o = createObject()
#         o.price_datetime = None
#         with self.assertRaisesMessage(ValidationError,'This field cannot be null.'):
#             o.full_clean()