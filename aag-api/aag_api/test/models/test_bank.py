from django.test import TestCase
from aag_api.models import *
from django.core.validators import ValidationError
from django.utils import timezone
from model_mommy import mommy

def createObject():
    o = Bank()
    o.name = "Bank Name"

    return o

class BankModelTest(TestCase):

    def test_model_bank_valid(self):
        what = mommy.make(Bank)
        self.assertEqual(what.name,str(what))

    def test_model_bank_name_is_none_fail(self):
        o = createObject()
        o.name = None

        with self.assertRaisesMessage(ValidationError,"ต้องไม่เป็นค่าว่าง"):
            o.full_clean()

    def test_model_bank_name_unique_fail(self):
        o = createObject()
        o.full_clean()
        o.save()

        o = createObject()

        with self.assertRaisesMessage(ValidationError,"ถูกใช้งานแล้ว"):
            o.full_clean()

    def test_model_bank_str_(self):
        o = createObject()
        o.full_clean()
        o.save()
        self.assertEqual(str(o),"Bank Name")