from django.test import TestCase
from aag_api.models import *
from django.core.validators import ValidationError
from django.utils import timezone
from aag_api.test.helper import *
from aag_api.models.option import *
from django.contrib.auth.models import User
from datetime import date
from model_mommy import mommy

class UserProfileModelTest(TestCase):


    def test_model_user_profile_valid(self):
        staff = mommy.make(Staff)
        o = mommy.make(UserProfile,staff=staff)
        o.full_clean()
        o.save()
        _str = "%s (%s)" % (o.user.username,o.branch.name)
        self.assertEqual(_str,str(o))

    def test_model_user_profile_staff_is_none(self):

        o = mommy.make(UserProfile,staff=None)
        o.full_clean()
        o.save()
        self.assertIsNone(o.staff)