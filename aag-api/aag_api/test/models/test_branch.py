from django.test import TestCase
from aag_api.models import *
from django.core.validators import ValidationError
from django.utils import timezone

def createObject():
    o = Branch()
    o.name = "Branch"
    o.code = "001"
    o.description = "Detail branch"
    o.is_enabled = 1

    return o

class BranchModelNameTest(TestCase):

    def test_model_branch_name_valid(self):
        o = createObject()
        o.full_clean()
        o.save()
        self.assertEqual(o.name,"Branch")

    def test_model_branch_name_null_fail(self):
        o = createObject()
        o.name = None
        with self.assertRaisesMessage(ValidationError,'ต้องไม่เป็นค่าว่าง'):
            o.full_clean()

    def test_model_branch_name_blank_fail(self):
        o = createObject()
        o.name = ''

        with self.assertRaisesMessage(ValidationError,'ต้องไม่เป็นค่าว่าง'):
            o.full_clean()

    def test_model_branch_name_unique_fail(self):
        o = createObject()
        o.save()
        o = createObject()
        with self.assertRaisesMessage(ValidationError,'ถูกใช้งานแล้ว'):
            o.full_clean()

    def test_model_branch_name_max_length(self):
        o = createObject()
        name = "a" * 100
        o.name = name
        o.full_clean()
        o.save()

        self.assertEqual(o.name,name)

    def test_model_branch_name_max_length_fail(self):
        o = createObject()
        name = "a" * 101
        o.name = name
        with self.assertRaisesMessage(ValidationError,'Ensure this value has at most 100 characters (it has 101).'):
            o.full_clean()

class BranchModelCodeTest(TestCase):
    def test_model_branch_code_valid(self):
        o = createObject()
        o.full_clean()
        o.save()
        self.assertEqual(o.code,"001")


    def test_model_branch_code_null_fail(self):
        o = createObject()
        o.code = None
        with self.assertRaisesMessage(ValidationError,'ต้องไม่เป็นค่าว่าง'):
            o.full_clean()

    def test_model_branch_code_blank_fail(self):
        o = createObject()
        o.code = ''
        with self.assertRaisesMessage(ValidationError,'ต้องไม่เป็นค่าว่าง'):
            o.full_clean()

    def test_model_branch_code_unique_fail(self):
        o = createObject()
        o.save()
        o = createObject()
        o.name = "Name2"
        o.code = "001"
        with self.assertRaisesMessage(ValidationError,'ถูกใช้งานแล้ว'):
            o.full_clean()

    def test_model_branch_code_max_length(self):

        o = createObject()
        o.code = "001"
        o.full_clean()
        o.save()
        self.assertEqual(o.code,"001")

    def test_model_branch_code_max_length_fail(self):

        o = createObject()
        o.code = "0011"
        with self.assertRaisesMessage(ValidationError,'รูปแบบไม่ถูกต้อง'):
            o.full_clean()

    def test_model_branch_code_format_fail(self):

        o = createObject()
        o.code = "1"
        with self.assertRaisesMessage(ValidationError,'รูปแบบไม่ถูกต้อง'):
            o.full_clean()

class BranchModelDescriptionTest(TestCase):
    def test_model_branch_description_valid(self):
        o = createObject()
        o.description = "Detail for branch"
        o.full_clean()
        o.save()
        self.assertEqual(o.description,"Detail for branch")

    def test_model_branch_description_null(self):
        o = createObject()
        o.description = None
        o.full_clean()
        o.save()
        self.assertEqual(o.description,None)

    def test_model_branch_description_blank(self):
        o = createObject()
        o.description = ''
        o.full_clean()
        o.save()
        self.assertEqual(o.description,'')
    
    def test_model_branch_description_max_length(self):
        o = createObject()
        description = 'a' * 255
        o.description = description
        o.full_clean()
        o.save()
        self.assertEqual(o.description,description)

    def test_model_branch_description_max_length_fail(self):
        o = createObject()
        description = 'a' * 256
        o.description = description

        with self.assertRaisesMessage(ValidationError,'Ensure this value has at most 255 characters (it has 256).'):
            o.full_clean()

class BranchModelSaveTest(TestCase):

    def test_model_branch_save_create_seting(self):
        o = createObject()
        o.full_clean()
        o.save()

        s = BranchSetting.objects.all().filter(branch=o.pk)
        self.assertEqual(s.count(),1)
        self.assertEqual(s[0].branch,o)
        