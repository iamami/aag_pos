from django.test import TestCase
from aag_api.models import *
from django.core.validators import ValidationError
from django.utils import timezone
from aag_api.test.helper import *
from aag_api.models.option import *
from django.contrib.auth.models import User
from datetime import date
from model_mommy import mommy

class StaffModelTest(TestCase):

    fixtures = ['group.json','user.json','initial_data.json']
    def test_model_staff_valid(self):

        staff = mommy.make(Staff,branch=Branch.objects.get(pk=1))
        staff.full_clean()
        staff.save()
        _str = "%s (%s)" % (staff.name,staff.code)
        self.assertEqual(_str,str(staff))

    def test_model_staff_code_fail(self):

        staff = mommy.make(Staff)
        staff.code = "xxx"
        with self.assertRaisesMessage(ValidationError,"รูปแบบรหัสพนักงานไม่ถูกต้อง"):
            staff.full_clean()

    def test_model_staff_code_fail(self):

        staff = mommy.make(Staff)
        staff.code = "xxx"
        with self.assertRaisesMessage(ValidationError,"รูปแบบรหัสพนักงานไม่ถูกต้อง"):
            staff.full_clean()