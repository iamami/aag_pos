from django.test import TestCase
from aag_api.models import *
from django.core.validators import ValidationError
from django.utils import timezone
from aag_api.test.helper import *
from aag_api.models.option import *
from django.contrib.auth.models import User
from datetime import date
from model_mommy import mommy

def createObject():

    u = mommy.make(User)
    c = mommy.make(Customer)
    
    return  mommy.make(Bill,customer=c,user=u)

class BillModelTest(TestCase):
    #fixtures = ['user.json']
    def setUp(self):
        self.ym = date.today().strftime('%y%m')
        pass

    def test_model_bill_save_bill_number_kink_is_SELL(self):

        u = mommy.make(User)
        c = mommy.make(Customer)
        
        o =  mommy.make(Bill,customer=c,user=u,kind = SELL)

        branch = str(o.branch.id)
        branch = ( '0' if int(branch) < 10 else '') + branch

        self.assertEqual(o.bill_number,'SE-%s%s00001' % (branch,self.ym))

    def test_model_bill_save_bill_number_kink_is_BUY(self):

        o = createObject()
        o.kind = BUY
        o.full_clean()
        o.save()

        branch = str(o.branch.id)
        branch = ( '0' if int(branch) < 10 else '') + branch

        self.assertEqual(o.bill_number,'BU-%s%s00001' % (branch,self.ym))

    def test_model_bill_save_bill_number_kink_is_CHANGE(self):

        u = mommy.make(User)
        c = mommy.make(Customer)
        o =  mommy.make(Bill,customer=c,user=u,kind = EXCHANGE)

        branch = str(o.branch.id)
        branch = ( '0' if int(branch) < 10 else '') + branch

        self.assertEqual(o.bill_number,'CH-%s%s00001' % (branch,self.ym))
        