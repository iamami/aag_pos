from django.test import TestCase
from aag_api.models import *
from django.core.validators import ValidationError
from django.utils import timezone
from aag_api.test.helper import *
from aag_api.models.option import *
from django.contrib.auth.models import User
from datetime import date

def createObject():

    o = Invoice()
    o.branch = createBranch()
    o.bill_date = date.today()
    o.customer = createCustomer()
    o.user = User.objects.get(pk=1)
    o.vendor = Vendor.objects.get(pk=1)
    o.invoice_date = date.today()
    o.kind = 'IM'
    return o

class InvoiceModelTest(TestCase):
    fixtures = ['group.json','user.json','oauth2_provider.json','gold_price.json','user.json','initial_data.json','vendeor.json']
    def setUp(self):
        self.ym = date.today().strftime('%y%m')
        pass
    
    def test_model_invoice_save_valid(self):

        o = createObject()
        o.full_clean()
        o.save()

        self.assertIsNotNone(o.number)

    def test_model_invoice_none_fail(self):

        fields = ['branch','user','invoice_date','status_bill','status_stock','kind']
        for f in fields:
            o = createObject()
            setattr(o, f, None)
            with self.assertRaisesMessage(ValidationError,"{'%s': ['ฟิลด์นี้ไม่สารถปล่อยว่างได้']}" % (f)):
                o.full_clean()

    def test_model_invoice_number_none_fail(self):

        o = createObject()
        o.full_clean()
        o.save()

        o.number = None
        with self.assertRaisesMessage(ValidationError,"{'number': ['This field cannot be null.']}"):
            o.save()
    
    def test_model_invoice_number_kind_IM(self):

        o = createObject()
        o.full_clean()
        date = o.invoice_date
        current_month = date.month
        current_year = date.year
        l = len(Invoice.objects.all().filter(invoice_date__month=current_month,invoice_date__year=current_year,kind=o.kind))+1
        
        o.save()
        no = "%05d" % (l,)
        number = "%s-%s%s" % ('RE' , date.strftime('%y%m') , no)
        self.assertEqual(o.number,number)

    def test_model_invoice_number_kind_EX(self):

        o = createObject()
        o.kind = 'EX'
        o.full_clean()
        date = o.invoice_date
        current_month = date.month
        current_year = date.year
        l = len(Invoice.objects.all().filter(invoice_date__month=current_month,invoice_date__year=current_year,kind=o.kind))+1
        o.save()
        
        no = "%05d" % (l,)
        number = "%s-%s%s" % ('CU' , date.strftime('%y%m') , no)
        self.assertEqual(o.number,number)

    def test_model_invoice_number_kind_MV(self):
        
        o = createObject()
        o.kind = 'MV'
        o.branch_to = 1
        o.full_clean()
        date = o.invoice_date
        current_month = date.month
        current_year = date.year
        l = len(Invoice.objects.all().filter(invoice_date__month=current_month,invoice_date__year=current_year,kind=o.kind))+1

        o.save()
        
        no = "%05d" % (l,)
        number = "%s-%s%s" % ('TR' , date.strftime('%y%m') , no)
        self.assertEqual(o.number,number)

    def test_model_invoice_branch_to_none_save_fail(self):
        
        o = createObject()
        o.kind = 'MV'
        with self.assertRaisesMessage(ValidationError,"{'branch_to': ['This field cannot be null.']}"):
            o.save()