from django.test import TestCase
from aag_api.models import *
from django.core.validators import ValidationError
from django.utils import timezone
from aag_api.test.helper import *
from aag_api.models.option import *
from django.contrib.auth.models import User
from datetime import date
from model_mommy import mommy

class ProductTypeModelTest(TestCase):


    def test_model_product_type_valid(self):

        o = mommy.make(ProductType,code='101')
        o.full_clean()
        o.save()
        _str = "%s (%s)" % (o.name,o.code)
        self.assertEqual(_str,str(o))

    def test_model_product_type_code_invalid(self):

        o = mommy.make(ProductType,code='aa')
        with self.assertRaisesMessage(ValidationError,"รูปแบบไม่ถูกต้อง"):
            o.full_clean()