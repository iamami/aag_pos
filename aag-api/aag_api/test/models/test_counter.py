from django.test import TestCase
from aag_api.models import *
from django.core.validators import ValidationError
from django.utils import timezone

class CounterModelTest(TestCase):

    def test_model_counter_name_valid(self):
        count = Counter()
        count.name = 'keyname'
        self.assertIsNone(count.full_clean())
        self.assertIsNone(count.save())

    def test_model_counter_name_null_fail(self):
        count = Counter()
        count.count = 0
        with self.assertRaisesMessage(ValidationError,"{'name': ['ฟิลด์นี้เว้นว่างไม่ได้']}"):
            count.full_clean()
    
    def test_model_counter_count_valid(self):
        count = Counter()
        count.name = 'keyname'
        count.count = 0
        self.assertIsNone(count.full_clean())
        self.assertIsNone(count.save())

    def test_model_counter_count_null(self):
        count = Counter()
        count.name = 'keyname'
        self.assertIsNone(count.full_clean())
        self.assertIsNone(count.save())
        self.assertEqual(count.count,0)

    def test_model_counter_count_max(self):
        count = Counter()
        count.name = 'keyname'
        count.count = 2147483647
        self.assertIsNone(count.full_clean())
        self.assertIsNone(count.save())
        self.assertEqual(count.count,2147483647)

    def test_model_counter_count_max_fail(self):
        count = Counter()
        count.name = 'keyname'
        count.count = 2147483648
        with self.assertRaisesMessage(ValidationError,'ค่านี้ต้องน้อยกว่าหรือเท่ากับ 2147483647'):
            count.full_clean()

    def test_model_counter_count_min(self):
        count = Counter()
        count.name = 'keyname'
        count.count = 0
        self.assertIsNone(count.full_clean())
        self.assertIsNone(count.save())
        self.assertEqual(count.count,0)

    def test_model_counter_count_min_fail(self):
        count = Counter()
        count.name = 'keyname'
        count.count = -1
        with self.assertRaisesMessage(ValidationError,'ค่านี้ต้องมากกว่าหรือเท่ากับ 0'):
            count.full_clean()

    def test_model_counter_to_str(self):
        count = Counter()
        count.name = 'keyname'
        count.count = 1
        self.assertIsNone(count.full_clean())
        self.assertIsNone(count.save())
        self.assertEqual(str(count),"%s %s"%(count.name,count.count))

