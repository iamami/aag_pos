from django.test import TestCase
from aag_api.models import *
from django.core.validators import ValidationError
from django.utils import timezone
from model_mommy import mommy
from model_mommy.recipe import Recipe

class CustomerModelTest(TestCase):

    def test_model_customer_valid(self):
        customer = mommy.make(Customer,code='C000001')
        customer.full_clean()
        customer.save()

        self.assertEqual("%s - %s" % (customer.name,customer.code),str(customer))

    def test_model_customer_email_invalid(self):
        customer = mommy.make(Customer,email="test")
        with self.assertRaisesMessage(ValidationError,"{'email': ['ป้อนที่อยู่อีเมลที่ถูกต้อง']}"):
            customer.full_clean()

    def test_model_customer_code_invalid(self):
        customer = mommy.make(Customer,code="test")
        with self.assertRaisesMessage(ValidationError,'รูปแบบรหัสลูกค้าไม่ถูกต้อง'):
            customer.full_clean()

    def test_model_customer_code_none_auto_gen_on_full_clean(self):
        customer = Customer()
        customer.code = None
        customer.name = "Customer"
        customer.full_clean()
        customer.save()

        self.assertEqual(customer.code,'C000001')

    def test_model_customer_code_none_auto_gen_on_save(self):
        customer = Customer()
        customer.code = None
        customer.name = "Customer"
        customer.save()

        self.assertEqual(customer.code,'C000001')