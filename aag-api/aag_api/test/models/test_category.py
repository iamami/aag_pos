from django.test import TestCase
from aag_api.models import *
from django.core.validators import ValidationError
from django.utils import timezone
from aag_api.test.helper import *
from aag_api.models.option import *
from django.contrib.auth.models import User
from datetime import date
from model_mommy import mommy

class CategoryModelTest(TestCase):

    def test_model_category_valid(self):

        o = mommy.make(Category,code='123')
        o.full_clean()
        o.save()
        _str = "%s (%s)" % (o.name,o.code)
        self.assertEqual(_str,str(o))

    def test_model_category_code_invalid(self):

        o = mommy.make(Category,code='abc')
        with self.assertRaisesMessage(ValidationError,'รูปแบบไม่ถูกต้อง'):
            o.full_clean()