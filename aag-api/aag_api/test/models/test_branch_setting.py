from django.test import TestCase
from aag_api.models import *
from django.core.validators import ValidationError
from django.utils import timezone
from aag_api.test.helper import *

def createObject():
    b = createBranch()
    o = BranchSetting.objects.get(branch=b.pk)
    o.branch = b
    o.counter = 1
    o.month = 3
    o.interest = 2
    o.min_interest = 100
    o.is_user_edit_interest = 1
    o.calculate_date = 1
    o.is_enabled = 1
    return o

class BranchSettingModelTest(TestCase):

    def test_model_branch_setting_gen_code(self):
        o = createObject()
        char = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

        for i in range(0,26*26):
            id = i +1
            code = o.genCode(i+1)
            index_1 = id%26
            index_2 = int(id/26)
            s = char[index_2:index_2+1] + char[index_1:index_1+1]
            self.assertEqual(s,code)

    def test_model_branch_setting_code_valid(self):

        o = createObject()
        o.full_clean()
        o.save()
        id = o.branch.id
        char = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        index_1 = id%26
        index_2 = int(id/26)
        s = char[index_2:index_2+1] + char[index_1:index_1+1]
        self.assertEqual(s,o.code)
        
    def test_model_branch_setting_code_valid_fail(self):

        o = createObject()
        o.full_clean()
        o.save()

        # Edit 
        o.code = 'A1'
        with self.assertRaisesMessage(ValidationError,'รูปแบบตัวอักษรไม่ถูกต้อง'):
            o.full_clean()
            
    def test_model_branch_setting_branch_u(self):

        o = createObject()
        o.full_clean()
        o.save()

        # Edit 
        o.code = 'A1'
        with self.assertRaisesMessage(ValidationError,'รูปแบบตัวอักษรไม่ถูกต้อง'):
            o.full_clean()