from django.test import TestCase
from aag_api.models import *
from django.core.validators import ValidationError
from django.utils import timezone
from model_mommy import mommy

class ProductModelTest(TestCase):

    def test_model_product_valid(self):
        o = mommy.make(Product,code="N96B100")
        o.full_clean()
        o.save()
        self.assertEqual(o.name+'('+o.code+')',str(o))

    def test_model_product_none_fail(self):
        fields = ['name','code','category','kind','type_weight','type_sale','cost','price_tag']
        for f in fields:
            Product.objects.all().delete()
            o = mommy.make(Product,code="N96B100")
            setattr(o, f, None)
            with self.assertRaisesMessage(ValidationError,"{'%s': ['ฟิลด์นี้ไม่สารถปล่อยว่างได้']}" % (f)):
                o.full_clean()