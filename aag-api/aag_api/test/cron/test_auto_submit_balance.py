from django.test import TestCase
from aag_api.test.helper import *
from aag_api.models import *
from aag_api.cron import auto_submit_balance
from datetime import date,timedelta,datetime
from decimal import Decimal

def getLedgerCategory12():
    return Ledger.objects.all().filter(ledger_category=12,branch=1,
                                                ledger_date__year=date.today().year,
                                                ledger_date__month=date.today().month,
                                                ledger_date__day=date.today().day)
class GoldPriceLogCrontabTest(TestCase):
    fixtures = ['group.json','user.json','oauth2_provider.json','gold_price.json','user.json','initial_data.json']
        
    def test_crontab_auto_submit_balance_buy_cash(self):

        # create balance
        o = createLedgerCategory12()
        o.ledger_date = datetime.today() - timedelta(days=1)
        o.save()

        # create bill buy
        b = createLedgerBillBuy()
        b.ledger_date = datetime.today() - timedelta(days=1)
        b.save()

        ledger = getLedgerCategory12()
        self.assertEqual(ledger.count(),0)

        auto_submit_balance()

        ledger = getLedgerCategory12()
        l = Ledger.objects.get(pk=ledger[0].pk) 
        self.assertEqual(ledger.count(),1)
        self.assertEqual(l.total,Decimal('10000000')-Decimal('20000'))
        
    def test_crontab_auto_submit_balance_sell_cash(self):

        # create balance
        o = createLedgerCategory12()
        o.ledger_date = datetime.today() - timedelta(days=1)
        o.save()

        # create bill sell
        b = createLedgerBillBuy()
        b.kind = 'IN'
        b.ledger_date = datetime.today() - timedelta(days=1)
        b.save()

        ledger = getLedgerCategory12()
        self.assertEqual(ledger.count(),0)

        auto_submit_balance()

        ledger = getLedgerCategory12()
        l = Ledger.objects.get(pk=ledger[0].pk) 
        self.assertEqual(ledger.count(),1)
        self.assertEqual(l.total,Decimal('10000000')+Decimal('20000'))


    def test_crontab_auto_submit_balance_sell_card(self):

        # create balance
        o = createLedgerCategory12()
        o.ledger_date = datetime.today() - timedelta(days=1)
        o.save()

        # create bill sell
        b = createLedgerBillBuy()
        b.kind = 'IN'
        b.cash = '0'
        b.card = '20000'
        b.ledger_date = datetime.today() - timedelta(days=1)
        b.save()

        ledger = getLedgerCategory12()
        self.assertEqual(ledger.count(),0)

        auto_submit_balance()

        ledger = getLedgerCategory12()
        l = Ledger.objects.get(pk=ledger[0].pk) 
        self.assertEqual(ledger.count(),1)
        self.assertEqual(l.total,Decimal('10000000'))

    def test_crontab_auto_submit_balance_sell_check(self):

        # create balance
        o = createLedgerCategory12()
        o.ledger_date = datetime.today() - timedelta(days=1)
        o.save()

        # create bill sell
        b = createLedgerBillBuy()
        b.kind = 'IN'
        b.cash = '0'
        b.check = '20000'
        b.ledger_date = datetime.today() - timedelta(days=1)
        b.save()

        ledger = getLedgerCategory12()
        self.assertEqual(ledger.count(),0)

        auto_submit_balance()

        ledger = getLedgerCategory12()
        l = Ledger.objects.get(pk=ledger[0].pk) 
        self.assertEqual(ledger.count(),1)
        self.assertEqual(l.total,Decimal('10000000'))

    def test_crontab_auto_submit_balance_no_data(self):

        ledger = getLedgerCategory12()
        self.assertEqual(ledger.count(),0)

        auto_submit_balance()

        ledger = getLedgerCategory12()
        l = Ledger.objects.get(pk=ledger[0].pk) 
        self.assertEqual(ledger.count(),1)
        self.assertEqual(l.total,Decimal('0'))