from aag_api.models import *
from django.contrib.auth.models import User , Group
from django.contrib.auth import get_user_model
from django.utils import timezone
from django.shortcuts import get_object_or_404, render,redirect
def createGoldPrice():

    o = GoldPrice()
    o.gold_bar_buy = "20000"
    o.gold_bar_sell = "20000"
    o.gold_ornaments_buy = "20000"
    o.gold_ornaments_sell = "20000"
    o.full_clean()
    o.save()
    return o

def createBranch():
    if Branch.objects.all().count()==0:
        branch = Branch()
        branch.code = '001'
        branch.name = 'Branch Name'
        branch.description = "description for Branch"
        branch.full_clean()
        branch.save()
        return branch
    else:
        return Branch.objects.all()[0]

def createCustomer():
    if Customer.objects.all().count()==0:
        o = Customer()
        o.name = 'Customer'
        o.code = 'C000001'
        o.citizen_id = '1900899090987'
        o.phone = '0998998877'
        o.nick_name = 'cus'
        o.birth_date
        o.save()
        return 
    else:
        return Customer.objects.all()[0]


def createStaff():
    if Staff.objects.all().count()==0:
        o = Staff()
        o.name = "staff1"
        o.code = "E001"
        o.branch = createBranch()
        o.phone = "09988776655"
        o.address = "dddd"
        o.full_clean()
        o.save()
        return o
    else:
        return Staff.objects.all()[0]

def createUserStaff(email,password):

    user = User.objects.create(username=email)
    user.set_password(password)
    user.save()

    return user

def createGoldPriceList():

    price = GoldPrice()
    
    price.gold_bar_buy = '19650.00'
    price.gold_bar_sell = '19750.00'
    price.gold_ornaments_buy = '19303.20'
    price.gold_ornaments_sell= '20250.00'
    price.save()
    price.record_datetime = date.fromordinal(timezone.now().toordinal()-5)
    price.save()

    price = GoldPrice()
    
    price.gold_bar_buy = '19600.00'
    price.gold_bar_sell = '19700.00'
    price.gold_ornaments_buy = '19253.20'
    price.gold_ornaments_sell= '20200.00'
    price.save()
    price.record_datetime = date.fromordinal(timezone.now().toordinal()-4)
    price.save()

    price = GoldPrice()
    
    price.gold_bar_buy = '19400.00'
    price.gold_bar_sell = '19500.00'
    price.gold_ornaments_buy = '19053.20'
    price.gold_ornaments_sell= '20000.00'
    price.save()
    price.record_datetime = date.fromordinal(timezone.now().toordinal()-3)
    price.save()

    price = GoldPrice()
    
    price.gold_bar_buy = '19500.00'
    price.gold_bar_sell = '19400.00'
    price.gold_ornaments_buy = '19153.20'
    price.gold_ornaments_sell= '20100.00'
    price.save()
    price.record_datetime = date.fromordinal(timezone.now().toordinal()-2)
    price.save()

    price = GoldPrice()
    
    price.gold_bar_buy = '19650.00'
    price.gold_bar_sell = '19750.00'
    price.gold_ornaments_buy = '19393.20'
    price.gold_ornaments_sell= '20250.00'
    price.save()
    price.record_datetime = date.fromordinal(timezone.now().toordinal()-1)
    price.save()

    price = GoldPrice()
    
    price.gold_bar_buy = '19600.00'
    price.gold_bar_sell = '19700.00'
    price.gold_ornaments_buy = '19253.20'
    price.gold_ornaments_sell= '20200.00'
    price.save()
    price.record_datetime = date.fromordinal(timezone.now().toordinal())
    price.save()

def createProduct():
    o = None
    if Product.objects.all().count()==0:
        o = Product()
        o.code = 'N96B100'
        o.name = 'สร้อยคอ1บาท(96.5)'
        o.kind = ProductType.objects.get(pk=2)
        o.category = Category.objects.get(pk=2)
        o.weight = '15.20'
        o.description = 'test'
        o.type_sale = 1
        o.full_clean()
        o.save()
    else:
        o = Product.objects.all()[0]
    return o


def createLedgerObject():
    o = Ledger()
    o.branch = Branch.objects.get(pk=1)
    o.ledger_date = date.today()
    o.object_id = None
    o.staff = createStaff()
    o.object_number = ''
    o.kind = 'IN'
    o.total = '20000'
    o.cash = '20000'
    o.ledger_category = LedgerCategory.objects.get(pk=1)
    o.user = User.objects.get(pk=1)

    return o

def createLedgerCategory12():
    o = createLedgerObject()
    o.kind = 'IN'
    o.total = '10000000'
    o.cash = '10000000'
    o.ledger_category = LedgerCategory.objects.get(pk=12)
    o.full_clean()
    o.save()
    return o

def createLedger(object_id,object_number):
    o = createLedgerObject()
    o.object_id = object_id
    o.object_number = object_number
    o.full_clean()
    o.staff = createStaff()
    o.save()
    return o

def createLedgerBillBuy():
    b = createBillEmpty()
    o = createLedgerObject()
    o.object_id = b.id
    o.object_number = b.bill_number
    o.total = b.total
    o.kind = 'EX'
    o.card = b.total
    o.full_clean()
    o.save()
    return o

def createBill():
    p = createGoldPrice()
    product = createProduct()
    bill = Bill()
    bill.status_stock = 'Y'
    bill.kind = 'BU'
    bill.branch = Branch.objects.get(pk=1)
    bill.bill_date = date.fromordinal(date.today().toordinal())
    bill.total = 20000
    bill.gold_price = p
    bill.save()

    bill_item = BillItem()
    bill_item.bill = bill
    bill_item.product = product
    bill_item.category = product.category
    bill_item.gold_price = p.gold_bar_sell
    bill_item.kind = 'BU'
    bill_item.status_stock = 'Y'

    bill_item.save()

    ledger = createLedger(bill.id,bill.bill_number)

    return bill

def createBillEmpty():
    p = createGoldPrice()
    bill = Bill()
    bill.status_stock = 'Y'
    bill.kind = 'BU'
    bill.branch = Branch.objects.get(pk=1)
    bill.bill_date = date.fromordinal(date.today().toordinal())
    bill.total = 20000
    bill.gold_price = p
    bill.save()

    return bill

def createProductStock():
    product = createProduct()
    branch = createBranch()

    stock_prodcut = StockProduct()
    stock_prodcut.branch = branch
    stock_prodcut.product = product
    stock_prodcut.amount = 100
    stock_prodcut.save()
    return stock_prodcut