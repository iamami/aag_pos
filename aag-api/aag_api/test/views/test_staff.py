from rest_framework import status
from django_hosts.resolvers import reverse
from rest_framework.test import APITestCase
from django.contrib.auth.models import User, Group
from aag_api.models import *
from aag_api.test.helper import *
from datetime import datetime
from rest_framework.authtoken.models import Token
from model_mommy import mommy

class StaffViewTests(APITestCase):
    fixtures = ['group.json','user.json','oauth2_provider.json','gold_price.json','user.json','initial_data.json']

    def setUp(self):
        self.token = '6NrB82bztZVyN9fx6GXwO3vwXIxO4o'
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

    def test_api_staff_GET_all(self):

        staffs = mommy.make(Staff,branch=Branch.objects.get(pk=1),_quantity=10)
        url = reverse('staff-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(10,len(response.data))
        
    def test_api_staff_GET_is_enabled(self):
        
        staffs = mommy.make(Staff,branch=Branch.objects.get(pk=1) ,_quantity=10)
        o = staffs[0]
        o.is_enabled = 0
        o.save()
        url = reverse('staff-list')+"?is_enabled=1"
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(9,len(response.data))


    def test_api_staff_POST_valid(self):

        branch = mommy.make(Branch)
        
        data = {'branch': branch.id,'name':"staff1"}
        url = reverse('staff-list')

        response = self.client.post(url, data=data,format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


    def test_api_staff_DELETE(self):

        staff = mommy.make(Staff)
        url = reverse('staff-detail',kwargs={'pk':staff.pk})

        response = self.client.delete(url,format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_api_staff_DELETE_use_in_Bill_fail(self):

        o = mommy.make(BillStaff)
        url = reverse('staff-detail',kwargs={'pk':o.staff.pk})

        response = self.client.delete(url,format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_api_staff_DELETE_use_in_Lease_fail(self):

        s = mommy.make(Staff)
        o = mommy.make(Lease,staff=s )
        url = reverse('staff-detail',kwargs={'pk':o.staff.pk})

        response = self.client.delete(url,format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_api_staff_DELETE_use_in_Ledger_fail(self):
        staff = mommy.make(Staff)
        o = mommy.make(Ledger,staff=staff)
        url = reverse('staff-detail',kwargs={'pk':o.staff.pk})

        response = self.client.delete(url,format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_api_staff_DELETE_use_in_Ledger_fail(self):
        staff = mommy.make(Staff)
        o = mommy.make(Ledger,staff=staff)
        url = reverse('staff-detail',kwargs={'pk':o.staff.pk})

        response = self.client.delete(url,format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)