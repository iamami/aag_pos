from rest_framework import status
from django_hosts.resolvers import reverse
from rest_framework.test import APITestCase
from django.contrib.auth.models import User, Group
from aag_api.models import *
from aag_api.test.helper import *
from datetime import datetime
from rest_framework.authtoken.models import Token
from model_mommy import mommy

class VendorListTests(APITestCase):
    fixtures = ['group.json','user.json','oauth2_provider.json','gold_price.json','initial_data.json']
    
    def setUp(self):
        self.token = '6NrB82bztZVyN9fx6GXwO3vwXIxO4o'
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)
        self.vendor_list = mommy.make(Vendor,_quantity=10)

    def test_api_vendor_GET(self):

        
        url = reverse('vendor-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data),10)

    def test_api_vendor_POST(self):
        url = reverse('vendor-list')

        data = {"name": "Vendor Name"}
        response = self.client.post(url,data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['name'],"Vendor Name")

    def test_api_vendor_PUT(self):
       
        url = reverse('vendor-detail',kwargs={"pk" : self.vendor_list[0].pk})

        data = {"name": "Vendor Name"}
        response = self.client.put(url,data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['name'],"Vendor Name")

    def test_api_vendor_DELETE(self):
        url = reverse('vendor-detail',kwargs={"pk" : self.vendor_list[0].pk})

        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_api_vendor_use_in_Invoice_DELETE_fail(self):
        v = mommy.make(Vendor)
        o = mommy.make(Invoice,vendor=v)
        url = reverse('vendor-detail',kwargs={"pk" : v.id})
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_api_vendor_use_in_BillCategory_DELETE_fail(self):

        v = mommy.make(Vendor)
        o = mommy.make(Invoice,vendor=v)
        url = reverse('vendor-detail',kwargs={"pk" : v.id})
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)