from rest_framework import status
from django_hosts.resolvers import reverse
from rest_framework.test import APITestCase
from django.contrib.auth.models import User, Group
from aag_api.models import *
from aag_api.test.helper import *
from datetime import datetime
from rest_framework.authtoken.models import Token
from model_mommy import mommy

class BankListTests(APITestCase):
    fixtures = ['group.json','user.json','oauth2_provider.json','gold_price.json','user.json','initial_data.json']

    def setUp(self):
        self.token = '6NrB82bztZVyN9fx6GXwO3vwXIxO4o'
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)
        self.vendor_list = mommy.make(Bank,_quantity=10)

    def test_api_bank_GET(self):

        
        url = reverse('bank-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data),10)

    def test_api_bank_POST(self):
        url = reverse('bank-list')

        data = {"name": "Vendor Name"}
        response = self.client.post(url,data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['name'],"Vendor Name")

    def test_api_bank_PUT(self):
       
        url = reverse('bank-detail',kwargs={"pk" : self.vendor_list[0].pk})

        data = {"name": "Vendor Name"}
        response = self.client.put(url,data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['name'],"Vendor Name")

    def test_api_bank_DELETE(self):
        url = reverse('bank-detail',kwargs={"pk" : self.vendor_list[0].pk})

        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_api_bank_use_in_BankCard_DELETE_fail(self):
        b = mommy.make(Bank)
        o = mommy.make(BankCard,bank=b)
        url = reverse('bank-detail',kwargs={"pk" : b.id})
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_api_bank_use_in_BankAccount_DELETE_fail(self):

        b = mommy.make(Bank)
        o = mommy.make(BankAccount,bank=b)
        url = reverse('bank-detail',kwargs={"pk" : b.id})
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)