from rest_framework import status
from django_hosts.resolvers import reverse
from rest_framework.test import APITestCase
from django.contrib.auth.models import User, Group
from aag_api.models import *
from aag_api.test.helper import *
from datetime import datetime
from rest_framework.authtoken.models import Token
from model_mommy import mommy

class ProductTypeViewTests(APITestCase):
    fixtures = ['group.json','user.json','oauth2_provider.json','gold_price.json','user.json','initial_data.json']

    def setUp(self):
        self.token = '6NrB82bztZVyN9fx6GXwO3vwXIxO4o'
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

    def test_api_product_type_GET_all(self):

        url = reverse('product-type-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(12,len(response.data))
        
    def test_api_product_type_GET_is_enabled(self):

        o = ProductType.objects.get(pk=1)
        o.is_enabled = 0
        o.save()
        url = reverse('product-type-list')+"?is_enabled=1"
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(11,len(response.data))

    def test_api_product_type_POST_valid(self):

        data = {'code':'RRB','name': "แหวน2",'description':""}
        url = reverse('product-type-list')

        response = self.client.post(url, data=data,format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        

    def test_api_product_type_DELETE(self):

        o = mommy.make(ProductType)
        url = reverse('product-type-detail',kwargs={'pk':o.pk})

        response = self.client.delete(url,format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_api_product_type_DELETE_fail(self):

        # use in Product
        o = mommy.make(Product)
        url = reverse('product-type-detail',kwargs={'pk':o.kind.pk})
        response = self.client.delete(url,format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['error'],'ไม่สามารถลบรายการนี้ได้ เนื่องจากถูกใช้งานแล้ว')