from rest_framework import status
from django_hosts.resolvers import reverse
from rest_framework.test import APITestCase
from django.contrib.auth.models import User, Group
from aag_api.models import *
from aag_api.test.helper import *
from datetime import datetime
from rest_framework.authtoken.models import Token
from model_mommy import mommy

class CategoryViewTests(APITestCase):
    fixtures = ['group.json','user.json','oauth2_provider.json','gold_price.json','user.json','initial_data.json']

    def setUp(self):
        self.token = '6NrB82bztZVyN9fx6GXwO3vwXIxO4o'
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

    def test_api_category_GET_all(self):

        url = reverse('category-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(3,len(response.data))
        
    def test_api_category_GET_is_enabled(self):

        o = Category.objects.get(pk=1)
        o.is_enabled = 0
        o.save()
        url = reverse('category-list')+"?is_enabled=1"
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(2,len(response.data))


    def test_api_category_POST_valid(self):

        data = {'name': "99.99",'code':"9999",'weight': 15.23}
        url = reverse('category-list')

        response = self.client.post(url, data=data,format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


    def test_api_category_DELETE(self):

        o = mommy.make(Category)
        url = reverse('category-detail',kwargs={'pk':o.pk})

        response = self.client.delete(url,format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_api_category_DELETE_fail(self):

        # use in Product
        o = mommy.make(Product)
        url = reverse('category-detail',kwargs={'pk':o.category.pk})
        response = self.client.delete(url,format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['error'],'ไม่สามารถลบกลุ่มสินค้าได้ เนื่องจากถูกใช้งานแล้ว')

        # use in BillItem
        o = mommy.make(BillItem)
        url = reverse('category-detail',kwargs={'pk':o.category.pk})
        response = self.client.delete(url,format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['error'],'ไม่สามารถลบกลุ่มสินค้าได้ เนื่องจากถูกใช้งานแล้ว')

        # use in StockCategory
        o = mommy.make(StockCategory)
        url = reverse('category-detail',kwargs={'pk':o.category.pk})
        response = self.client.delete(url,format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['error'],'ไม่สามารถลบกลุ่มสินค้าได้ เนื่องจากถูกใช้งานแล้ว')