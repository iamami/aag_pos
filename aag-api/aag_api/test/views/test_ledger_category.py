from rest_framework import status
from django_hosts.resolvers import reverse
from rest_framework.test import APITestCase
from django.contrib.auth.models import User, Group
from aag_api.models import *
from aag_api.test.helper import *
from datetime import datetime
from rest_framework.authtoken.models import Token
from model_mommy import mommy

class LedgerCategoryListTests(APITestCase):
    fixtures = ['group.json','user.json','oauth2_provider.json','gold_price.json','user.json','initial_data.json']

    def setUp(self):
        self.token = '6NrB82bztZVyN9fx6GXwO3vwXIxO4o'
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

    def test_api_ledger_category_GET(self):

        url = reverse('ledger_category-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data),12)

    def test_api_ledger_category_POST(self):
        url = reverse('ledger_category-list')
        data = {"title": "Income","kind":"IN"}
        response = self.client.post(url,data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['title'],"Income")

    def test_api_ledger_category_PUT(self):
        
        c = mommy.make(LedgerCategory)
        url = reverse('ledger_category-detail',kwargs={"pk" : c.pk})
        data = {"title": "Income","kind":"IN"}
        response = self.client.put(url,data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['title'],"Income")

    def test_api_ledger_category_DELETE(self):
        c = mommy.make(LedgerCategory)
        url = reverse('ledger_category-detail',kwargs={"pk" : c.pk})

        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_api_ledger_category_use_in_Ledger_DELETE_fail(self):
        b = mommy.make(BankCard)
        o = mommy.make(Ledger,ledger_category=b)
        url = reverse('ledger_category-detail',kwargs={"pk" : b.id})
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_api_ledger_category_use_in_Ledger_DELETE_fail(self):
        b = mommy.make(LedgerCategory)
        o = mommy.make(Ledger,ledger_category=b)
        url = reverse('ledger_category-detail',kwargs={"pk" : b.id})
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    

    def test_api_ledger_category_is_standard_DELETE_fail(self):
        b = mommy.make(LedgerCategory)
        o = mommy.make(Ledger,ledger_category=b)
        url = reverse('ledger_category-detail',kwargs={"pk" : 1})
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)