from rest_framework import status
from django_hosts.resolvers import reverse
from rest_framework.test import APITestCase
from django.contrib.auth.models import User, Group
from aag_api.models import *
from aag_api.test.helper import *
from datetime import datetime
from rest_framework.authtoken.models import Token
from model_mommy import mommy

class BankAccountListTests(APITestCase):
    fixtures = ['group.json','user.json','oauth2_provider.json','gold_price.json','user.json','initial_data.json']

    def setUp(self):
        self.token = '6NrB82bztZVyN9fx6GXwO3vwXIxO4o'
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)
        self.list = mommy.make(BankAccount,_quantity=10)

    def test_api_bank_account_GET(self):

        url = reverse('bank_account-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data),10)

    def test_api_bank_account_POST(self):
        url = reverse('bank_account-list')

        b = mommy.make(Bank)
        data = {"name": "bank_account Name","number":"00000000000000","bank":b.pk}
        response = self.client.post(url,data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['name'],"bank_account Name")

    def test_api_bank_account_PUT(self):
       
        url = reverse('bank_account-detail',kwargs={"pk" : self.list[0].pk})
        b = mommy.make(Bank)
        data = {"name": "bank_account Name","number":"00000000000000","bank":b.pk}
        response = self.client.put(url,data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['name'],"bank_account Name")

    def test_api_bank_account_DELETE(self):
        url = reverse('bank_account-detail',kwargs={"pk" : self.list[0].pk})

        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_api_bank_account_use_in_Ledger_DELETE_fail(self):
        b = mommy.make(BankAccount)
        o = mommy.make(Ledger,transfer_bank_account=b)
        url = reverse('bank_account-detail',kwargs={"pk" : b.id})
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)