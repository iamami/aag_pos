from rest_framework import status

from django_hosts.resolvers import reverse
from rest_framework.test import APITestCase
from django.contrib.auth.models import User, Group
from aag_api.models import *
from aag_api.test.helper import *
from datetime import datetime
from rest_framework.test import APIClient

class GoldPriceTests(APITestCase):
    fixtures = ['group.json','user.json','oauth2_provider.json','gold_price.json']
    def setUp(self):
        self.token = '6NrB82bztZVyN9fx6GXwO3vwXIxO4o'
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

    def test_api_gold_all(self):
        url = reverse('gold_price-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data),5)

    # def test_api_gold_limit_1(self):
    #     url = reverse('gold_price-list') + '?limit=1'
    #     response = self.client.get(url, format='json')
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     self.assertEqual(len(response.data),1)

    # def test_api_gold_by_datetime(self):
    #     url = reverse('gold_price-list') + '?datetime=%s' % ('2018-06-28 00:00:00')
    #     response = self.client.get(url, format='json')
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     self.assertEqual(len(response.data),1)
    #     self.assertIn('2018-06-26T14:50:30.246000Z',str(response.data))

    # def test_api_gold_by_future_datetime(self):
    #     url = reverse('gold_price-list') + '?datetime=%s' % ('2018-07-28 00:00:00')

    #     response = self.client.get(url, format='json')
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     self.assertEqual(len(response.data),1)
    #     self.assertIn('2018-07-04T07:00:02.320000Z',str(response.data))