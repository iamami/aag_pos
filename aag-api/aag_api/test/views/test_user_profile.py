from rest_framework import status
from django_hosts.resolvers import reverse
from rest_framework.test import APITestCase
from django.contrib.auth.models import User, Group
from aag_api.models import *
from aag_api.test.helper import *
from datetime import datetime
from rest_framework.authtoken.models import Token
from model_mommy import mommy

class UserProfilesViewTests(APITestCase):
    fixtures = ['group.json','user.json','oauth2_provider.json','gold_price.json','user.json','initial_data.json']

    def setUp(self):
        self.token = '6NrB82bztZVyN9fx6GXwO3vwXIxO4o'
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

    def test_api_userprofiles_GET_all(self):

        users = mommy.make(UserProfile,_quantity=10)
        url = reverse('userprofiles-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(11,len(response.data))
        
    def test_api_userprofiles_GET_is_enabled(self):

        users = mommy.make(UserProfile,_quantity=10)
        user = users[0].user
        user.is_active = 0
        user.save()
        url = reverse('userprofiles-list')+"?is_active=1"
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(10,len(response.data))


    def test_api_userprofiles_POST_valid(self):
        staff = mommy.make(Staff)
        data = {
            "user": {
                "first_name": "test",
                "username": "test",
                "password": "112233",
                "is_active": True,
                "is_superuser": False
            },
            'staff': staff.id,
            'role': USER,
            'branch': staff.branch.id
        }
        url = reverse('userprofiles-list')

        response = self.client.post(url, data=data,format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        user_profiles = UserProfile.objects.all().filter(user__username='test')
        self.assertEqual(1,len(user_profiles))

    def test_api_userprofiles_PUT_valid(self):
        user_profile = mommy.make(UserProfile)
        data = {
            "user": {
                "first_name": "test",
                "username": "test",
                "password": "112233",
                "is_active": True,
                "is_superuser": False
            },
            'staff': None,
            'role': USER,
            'branch': user_profile.branch.id
        }
        url = reverse('userprofiles-detail',kwargs={'pk': user_profile.pk})

        response = self.client.put(url, data=data,format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        user_profiles = UserProfile.objects.all().filter(user__username='test')
        self.assertEqual(1,len(user_profiles))

    def test_api_staff_DELETE(self):

        user_profile = mommy.make(UserProfile)
        url = reverse('userprofiles-detail',kwargs={'pk':user_profile.pk})

        response = self.client.delete(url,format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_api_userprofile_DELETE_fail(self):

        o = mommy.make(Bill)
        url = reverse('userprofiles-detail',kwargs={'pk':o.user.pk})
        response = self.client.delete(url,format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        o = mommy.make(Lease)
        user_profile = mommy.make(UserProfile,user=o.user)
        url = reverse('userprofiles-detail',kwargs={'pk':user_profile.pk})
        response = self.client.delete(url,format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        o = mommy.make(Ledger)
        user_profile = mommy.make(UserProfile,user=o.user)
        url = reverse('userprofiles-detail',kwargs={'pk':user_profile.pk})
        response = self.client.delete(url,format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        o = mommy.make(Invoice)
        user_profile = mommy.make(UserProfile,user=o.user)
        url = reverse('userprofiles-detail',kwargs={'pk':user_profile.pk})
        response = self.client.delete(url,format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    
        o = mommy.make(BillCategory)
        user_profile = mommy.make(UserProfile,user=o.user)
        url = reverse('userprofiles-detail',kwargs={'pk':user_profile.pk})
        response = self.client.delete(url,format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)