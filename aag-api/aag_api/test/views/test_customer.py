from rest_framework import status
from django_hosts.resolvers import reverse
from rest_framework.test import APITestCase
from django.contrib.auth.models import User, Group
from aag_api.models import *
from aag_api.test.helper import *
from datetime import datetime
from rest_framework.authtoken.models import Token
from model_mommy import mommy

class CustomerViewTests(APITestCase):
    fixtures = ['group.json','user.json','oauth2_provider.json','gold_price.json','user.json','initial_data.json']

    def setUp(self):
        self.token = '6NrB82bztZVyN9fx6GXwO3vwXIxO4o'
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)
        
        self.customers = mommy.make(Customer,_quantity=1000)
        o = self.customers[0]
        o.is_enabled = 0
        o.save()

    def test_api_customer_GET_all(self):

        url = reverse('customer-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(1001,response.data['count'])

    def test_api_customer_GET_is_enabled_true(self):

        url = reverse('customer-list')+"?is_enabled=1"
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(1000,response.data['count'])

    def test_api_customer_GET_search_by_text(self):

        # search name by text
        c = mommy.make(Customer,name='name')
        url = reverse('customer-list')+"?text=name"
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(1,response.data['count'])

        # search code by text
        c = mommy.make(Customer)
        url = reverse('customer-list')+"?text="+c.code
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(1,response.data['count'])

        # search phone by text
        c = mommy.make(Customer,phone='0888888888')
        url = reverse('customer-list')+"?text=0888888888"
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(1,response.data['count'])

        # search citizen_id by text
        c = mommy.make(Customer,citizen_id='111111111111111')
        url = reverse('customer-list')+"?text=111111111111111"
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(1,response.data['count'])

    def test_api_customer_GET_page2(self):

        url = reverse('customer-list')+"?page=2"
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(1001,response.data['count'])

    def test_api_customer_POST_valid(self):
        
        data = {'name': "Customer","is_application": 1,"email": "aa@mail.com"}
        url = reverse('customer-list')

        response = self.client.post(url, data=data,format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_api_customer_DELETE(self):

        url = reverse('customer-detail',kwargs={'pk':self.customers[0].pk})

        response = self.client.delete(url,format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_api_customer_DELETE_fail(self):

        # create Bill
        c = mommy.make(Customer)
        mommy.make(Bill,customer=c)

        url = reverse('customer-detail',kwargs={'pk':c.pk})
        response = self.client.delete(url,format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # create Ledger
        c = mommy.make(Customer)
        mommy.make(Ledger,customer=c)

        # create Lease
        c = mommy.make(Customer)
        mommy.make(Lease,customer=c)

        url = reverse('customer-detail',kwargs={'pk':c.pk})
        response = self.client.delete(url,format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)