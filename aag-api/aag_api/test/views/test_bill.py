from rest_framework import status
from django_hosts.resolvers import reverse
from rest_framework.test import APITestCase
from django.contrib.auth.models import User, Group
from aag_api.models import *
from aag_api.test.helper import *
from datetime import datetime
from rest_framework.authtoken.models import Token
from model_mommy import mommy

class BillListTests(APITestCase):
    fixtures = ['group.json','user.json','oauth2_provider.json','gold_price.json','initial_data.json']

    def setUp(self):
        self.token = '6NrB82bztZVyN9fx6GXwO3vwXIxO4o'
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

    def test_api_bill_list_empty(self):
        url = reverse('bill-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data),0)

    def test_api_bill_list_10_item(self):

        bills = mommy.make(Bill,_quantity=10)

        # create 3 item
        url = reverse('bill-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data),10)

class VoidBillUpdateStockTests(APITestCase):
    fixtures =  ['group.json','user.json','oauth2_provider.json','gold_price.json','user.json','initial_data.json']

    def setUp(self):
        self.token = '6NrB82bztZVyN9fx6GXwO3vwXIxO4o'
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        self.bill = createBill()

    def test_api_bill_void_success(self):
        url = reverse('bill-void',kwargs={'bill': self.bill.id})
        response = self.client.post(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        
        bill = Bill.objects.get(pk=self.bill.id)
        self.assertEqual(bill.is_void,1)

        ledger = Ledger.objects.all()
        self.assertEqual(ledger.count(),1)
        self.assertEqual(ledger[0].status,0)
