from rest_framework import status
from django_hosts.resolvers import reverse
from rest_framework.test import APITestCase
from django.contrib.auth.models import User, Group
from aag_api.models import *
from aag_api.test.helper import *
from datetime import datetime
from rest_framework.authtoken.models import Token
from model_mommy import mommy

class BranchListTests(APITestCase):
    fixtures = ['group.json','user.json','oauth2_provider.json','gold_price.json','user.json','initial_data.json']

    def setUp(self):
        self.token = '6NrB82bztZVyN9fx6GXwO3vwXIxO4o'
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

    def test_api_branch_GET(self):
        url = reverse('branch-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data),1)

    def test_api_branch_POST(self):
        url = reverse('branch-list')

        data = {"code": "002","name": "Name Branch"}
        response = self.client.post(url,data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['code'],"002")

    def test_api_branch_PUT(self):
        url = reverse('branch-detail',kwargs={"pk" : 1})

        data = {"code": "002","name": "Name Branch"}
        response = self.client.put(url,data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['code'],"002")

    def test_api_branch_DELETE(self):
        url = reverse('branch-detail',kwargs={"pk" : 1})

        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_api_branch_use_in_Bill_DELETE_fail(self):

        o = mommy.make(Bill)
        url = reverse('branch-detail',kwargs={"pk" : o.branch.id})
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_api_branch_use_in_Invoice_DELETE_fail(self):

        o = mommy.make(Invoice)
        url = reverse('branch-detail',kwargs={"pk" : o.branch.id})
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_api_branch_use_in_Lease_DELETE_fail(self):

        o = mommy.make(Lease)
        url = reverse('branch-detail',kwargs={"pk" : o.branch.id})
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_api_branch_use_in_Ledger_DELETE_fail(self):

        o = mommy.make(Ledger)
        url = reverse('branch-detail',kwargs={"pk" : o.branch.id})
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)