from rest_framework import status
from django_hosts.resolvers import reverse
from rest_framework.test import APITestCase
from django.contrib.auth.models import User, Group
from aag_api.models import *
from aag_api.test.helper import *
from datetime import datetime
from rest_framework.authtoken.models import Token
from model_mommy import mommy

class ProductViewTests(APITestCase):
    fixtures = ['group.json','user.json','oauth2_provider.json','gold_price.json','user.json','initial_data.json']

    def setUp(self):
        self.token = '6NrB82bztZVyN9fx6GXwO3vwXIxO4o'
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

    def test_api_product_GET_all(self):

        products = mommy.make(Product,_quantity=10)
        url = reverse('product-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(10,len(response.data))
        
    def test_api_product_GET_is_enabled(self):

        products = mommy.make(Product,_quantity=10)
        product = products[0]
        product.is_enabled = 0
        product.save()
        url = reverse('product-list')+"?is_enabled=1"
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(9,len(response.data))


    def test_api_product_POST_valid(self):
        staff = mommy.make(Staff)
        data = {
            'name': "Product Name",
            'code': "NB001",
            'category': 1,
            'kind': 1,
            'type_weight': 1,
            'weight': 15.2,
            'type_sale': 1,
            'price_tag': 0,
            'cost': 0,
            'is_enabled': 1,

        }
        url = reverse('product-list')

        response = self.client.post(url, data=data,format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_api_product_PUT_valid(self):
        product = mommy.make(Product)
        data = {
            'name': "Product Name",
            'code': "NB001",
            'category': 1,
            'kind': 1,
            'type_weight': 1,
            'weight': 15.2,
            'type_sale': 1,
            'price_tag': 0,
            'cost': 0,
            'is_enabled': 1,

        }
        url = reverse('product-detail',kwargs={'pk':product.pk})

        response = self.client.put(url, data=data,format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        o = Product.objects.get(code='NB001')

    def test_api_product_DELETE(self):

        o = mommy.make(Product)
        url = reverse('product-detail',kwargs={'pk':o.pk})

        response = self.client.delete(url,format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_api_product_DELETE_fail(self):

        p = mommy.make(Product)
        o = mommy.make(BillItem,product=p)
        url = reverse('product-detail',kwargs={'pk':o.product.pk})
        response = self.client.delete(url,format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        o = mommy.make(StockProduct)
        url = reverse('product-detail',kwargs={'pk':o.product.pk})
        response = self.client.delete(url,format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        o = mommy.make(InvoiceItem)
        url = reverse('product-detail',kwargs={'pk':o.product.pk})
        response = self.client.delete(url,format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
