from rest_framework import permissions
from aag_api.models.user_profile import UserProfile
from aag_api.models.option import *


class IsUserReadOnly(permissions.BasePermission):

    def has_permission(self, request,view):
        if request.method=='PUT' or request.method=='PATCH'or request.method=='POST'or request.method=='DELETE':
            try:
                user_profile = UserProfile.objects.get(user=request.user.pk)
                return ADMIN in user_profile.role 
            except model.DoesNotExist:
                return False
        else:
            return True

class IsAdminOnly(permissions.BasePermission):

    def has_permission(self, request,view):
        try:
            user_profile = UserProfile.objects.get(user=request.user.pk)
            return ADMIN in user_profile.role 
        except model.DoesNotExist:
            return False

class IsAdminUpdateOnly(permissions.BasePermission):

    def has_permission(self, request,view):
        if request.method=='PUT' or request.method=='PATCH':
            try:
                user_profile = UserProfile.objects.get(user=request.user.pk)
                return ADMIN in user_profile.role 
            except model.DoesNotExist:
                return False
        else:
            return True

class IsAdminDeleteOnly(permissions.BasePermission):

    def has_permission(self, request,view):
        if request.method=='DELETE':
            try:
                user_profile = UserProfile.objects.get(user=request.user.pk)
                return ADMIN in user_profile.role 
            except model.DoesNotExist:
                return False
        else:
            return True