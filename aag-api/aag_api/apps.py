from django.apps import AppConfig


class AagApiConfig(AppConfig):
    name = 'aag_api'
