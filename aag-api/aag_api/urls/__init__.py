from aag_api.urls.api_v2 import urlpatterns as api_v2
from aag_api.urls.api_v1 import urlpatterns as api_v1
from aag_api.urls.urls import urlpatterns as urls

urlpatterns = urls
urlpatterns += api_v1
urlpatterns += api_v2