from django.conf.urls import url, include
from django.contrib import admin
from rest_framework.authtoken import views
from rest_framework.urlpatterns import format_suffix_patterns
from django.conf import settings
from django.conf.urls.static import static
from aag_api.views.index import IndexView
from aag_api.views.initdata import *
from rest_framework.documentation import include_docs_urls
admin.site.site_header = 'AA Gold - POS.'
admin.site.site_title = admin.site.site_header
admin.site.index_title = 'จัดการระบบ'

urlpatterns = [
    url(r'^$',IndexView,name='index'),
    url(r'^api-token-auth/', views.obtain_auth_token),
    url(r'^admin/', admin.site.urls,name='admin'),
    url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
]
if settings.IS_INITDATA:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns.append(url(r'^init-data/clear/',ClearData.as_view(),name='ClearData'))
    urlpatterns.append(url(r'^init-data/v1/',InitDataV1.as_view(),name='InitDataV1'))
    urlpatterns.append(url(r'^init-data/v1_admin/',InitDataV1_Admin.as_view(),name='InitDataV1_Admin'))
    urlpatterns.append(url(r'^init-data/v1_admin_report/',InitDataV1_Admin_Report.as_view(),name='InitDataV1_Admin_Report'))
    urlpatterns.append(url(r'^init-data/clearSettingCustomer/',ClearSettingCustomer.as_view(),name='ClearSettingCustomer'))
    urlpatterns.append(url(r'^init-data/searchCustomer/',SearchCustomer.as_view(),name='SearchCustomer'))
    urlpatterns.append(url(r'^init-data/infoCustomer/',InfoCustomer.as_view(),name='InfoCustomer'))
    urlpatterns.append(url(r'^init-data/clearInfoCustomer/',ClearInfoCustomer.as_view(),name='ClearInfoCustomer'))
    urlpatterns.append(url(r'^init-data/infoCustomeretc/',InfoCustomerETC.as_view(),name='InfoCustomerETC'))
    urlpatterns.append(url(r'^init-data/bill/',Bill.as_view(),name='Bill'))
    urlpatterns.append(url(r'^init-data/ledger/',Ledger.as_view(),name='Ledger'))
    urlpatterns.append(url(r'^init-data/lease/',Lease.as_view(),name='Lease'))
    urlpatterns.append(url(r'^init-data/score/',Score.as_view(),name='Score'))
    urlpatterns.append(url(r'^init-data/product/',Product.as_view(),name='Product'))
    urlpatterns.append(url(r'^init-data/pointCustomer/',PointCustomer.as_view(),name='PointCustomer'))
    urlpatterns.append(url(r'^init-data/branchSetting/',BranchSetting.as_view(),name='BranchSetting'))
    urlpatterns.append(url(r'^init-data/branch/',Branch.as_view(),name='Branch'))
    urlpatterns.append(url(r'^init-data/V11/',V11.as_view(),name='V11'))
    urlpatterns.append(url(r'^init-data/V12/',V12.as_view(),name='V12'))
    urlpatterns.append(url(r'^init-data/V13/',V13.as_view(),name='V13'))
    urlpatterns.append(url(r'^init-data/V14/',V14.as_view(),name='V14'))
    urlpatterns.append(url(r'^init-data/V15/',V15.as_view(),name='V15'))
    urlpatterns.append(url(r'^init-data/V18/',V18.as_view(),name='V18'))
    urlpatterns.append(url(r'^init-data/V19/',V19.as_view(),name='V19'))
    urlpatterns.append(url(r'^init-data/M1/',M1.as_view(),name='M1'))
    urlpatterns.append(url(r'^init-data/M2/',M2.as_view(),name='M2'))
    urlpatterns.append(url(r'^init-data/M3/',M3.as_view(),name='M3'))
    urlpatterns.append(url(r'^init-data/M4/',M4.as_view(),name='M4'))
    urlpatterns.append(url(r'^init-data/M5/',M5.as_view(),name='M5'))
    urlpatterns.append(url(r'^init-data/M6/',M6.as_view(),name='M6'))
    urlpatterns.append(url(r'^init-data/M7/',M7.as_view(),name='M7'))
    urlpatterns.append(url(r'^init-data/M8/',M8.as_view(),name='M8'))
    urlpatterns.append(url(r'^init-data/M9/',M9.as_view(),name='M9'))
    urlpatterns.append(url(r'^init-data/M10/',M10.as_view(),name='M10'))
    urlpatterns.append(url(r'^init-data/clearstaffproduct/',ClearStaff_Product.as_view(),name='ClearStaff_Product'))
    urlpatterns.append(url(r'^init-data/admin_1/',Admin_1.as_view(),name='Admin_1'))
    urlpatterns.append(url(r'^init-data/admin_2/',Admin_2.as_view(),name='Admin_2'))
    urlpatterns.append(url(r'^init-data/admin_3/',Admin_3.as_view(),name='Admin_3'))
    urlpatterns.append(url(r'^init-data/admin_4/',Admin_4.as_view(),name='Admin_4'))
    urlpatterns.append(url(r'^init-data/admin_5/',Admin_5.as_view(),name='Admin_5'))
    urlpatterns.append(url(r'^init-data/admin_6/',Admin_6.as_view(),name='Admin_6'))    
    urlpatterns.append(url(r'^init-data/admin_7/',Admin_7.as_view(),name='Admin_7'))
    urlpatterns.append(url(r'^init-data/admin_8/',Admin_8.as_view(),name='Admin_8'))
    urlpatterns.append(url(r'^init-data/admin_9/',Admin_9.as_view(),name='Admin_9'))
    urlpatterns.append(url(r'^init-data/admin_10/',Admin_10.as_view(),name='Admin_10'))
    urlpatterns.append(url(r'^init-data/admin_11/',Admin_11.as_view(),name='Admin_11'))
    urlpatterns.append(url(r'^init-data/admin_12/',Admin_12.as_view(),name='Admin_12'))
    urlpatterns.append(url(r'^init-data/admin_13/',Admin_13.as_view(),name='Admin_13'))
    urlpatterns.append(url(r'^init-data/admin_14/',Admin_14.as_view(),name='Admin_14'))
    urlpatterns.append(url(r'^init-data/admin_15/',Admin_15.as_view(),name='Admin_15'))
    urlpatterns.append(url(r'^init-data/admin_16/',Admin_16.as_view(),name='Admin_16'))
    urlpatterns.append(url(r'^init-data/admin_17/',Admin_17.as_view(),name='Admin_17'))
    urlpatterns.append(url(r'^init-data/admin_18/',Admin_18.as_view(),name='Admin_18'))
    urlpatterns.append(url(r'^init-data/admin_19/',Admin_19.as_view(),name='Admin_19'))
    urlpatterns.append(url(r'^init-data/admin_20/',Admin_20.as_view(),name='Admin_20'))
    urlpatterns.append(url(r'^init-data/admin_21/',Admin_21.as_view(),name='Admin_21'))
    urlpatterns.append(url(r'^init-data/admin_22/',Admin_22.as_view(),name='Admin_22'))
    urlpatterns.append(url(r'^init-data/admin_23/',Admin_23.as_view(),name='Admin_23'))
    urlpatterns.append(url(r'^init-data/admin_24/',Admin_24.as_view(),name='Admin_24'))
    urlpatterns.append(url(r'^init-data/admin_25/',Admin_25.as_view(),name='Admin_25'))
    urlpatterns.append(url(r'^init-data/admin_26/',Admin_26.as_view(),name='Admin_26'))
    urlpatterns.append(url(r'^init-data/admin_27/',Admin_27.as_view(),name='Admin_27'))
    urlpatterns.append(url(r'^init-data/admin_28/',Admin_28.as_view(),name='Admin_28'))
    urlpatterns.append(url(r'^init-data/admin_29/',Admin_29.as_view(),name='Admin_29'))
    urlpatterns.append(url(r'^init-data/admin_30/',Admin_30.as_view(),name='Admin_30'))
    urlpatterns.append(url(r'^init-data/admin_31/',Admin_31.as_view(),name='Admin_31'))
    urlpatterns.append(url(r'^init-data/admin_32/',Admin_32.as_view(),name='Admin_32'))
    urlpatterns.append(url(r'^init-data/admin_33/',Admin_33.as_view(),name='Admin_33'))
    urlpatterns.append(url(r'^init-data/admin_34/',Admin_34.as_view(),name='Admin_34'))
    urlpatterns.append(url(r'^init-data/admin_35/',Admin_35.as_view(),name='Admin_35'))
    urlpatterns.append(url(r'^init-data/admin_36/',Admin_36.as_view(),name='Admin_36'))
    urlpatterns.append(url(r'^init-data/admin_37/',Admin_37.as_view(),name='Admin_37'))
    urlpatterns.append(url(r'^init-data/admin_38/',Admin_38.as_view(),name='Admin_38'))
    urlpatterns.append(url(r'^init-data/admin_39/',Admin_39.as_view(),name='Admin_39'))
    urlpatterns.append(url(r'^init-data/admin_40/',Admin_40.as_view(),name='Admin_40'))
    urlpatterns.append(url(r'^init-data/admin_41/',Admin_41.as_view(),name='Admin_41'))
    urlpatterns.append(url(r'^init-data/admin_42/',Admin_42.as_view(),name='Admin_42'))
    urlpatterns.append(url(r'^init-data/report_1/',Report_1.as_view(),name='Report_1'))
    urlpatterns.append(url(r'^init-data/report_2/',Report_2.as_view(),name='Report_2'))
    urlpatterns.append(url(r'^init-data/report_3/',Report_3.as_view(),name='Report_3'))
    urlpatterns.append(url(r'^init-data/report_4/',Report_4.as_view(),name='Report_4'))
    urlpatterns.append(url(r'^init-data/report_5/',Report_5.as_view(),name='Report_5'))
    urlpatterns.append(url(r'^init-data/report_6/',Report_6.as_view(),name='Report_6'))
    urlpatterns.append(url(r'^init-data/report_7/',Report_7.as_view(),name='Report_7'))
    urlpatterns.append(url(r'^init-data/report_8/',Report_8.as_view(),name='Report_8'))
    urlpatterns.append(url(r'^init-data/report_9/',Report_9.as_view(),name='Report_9'))
    urlpatterns.append(url(r'^init-data/report_10/',Report_10.as_view(),name='Report_10'))
    urlpatterns.append(url(r'^init-data/report_11/',Report_11.as_view(),name='Report_11'))
    urlpatterns.append(url(r'^init-data/report_12/',Report_12.as_view(),name='Report_12'))
    urlpatterns.append(url(r'^init-data/report_13/',Report_13.as_view(),name='Report_13'))
    urlpatterns.append(url(r'^init-data/report_14/',Report_14.as_view(),name='Report_14'))
    urlpatterns.append(url(r'^init-data/report_15/',Report_15.as_view(),name='Report_15'))
    urlpatterns.append(url(r'^init-data/report_16/',Report_16.as_view(),name='Report_16'))
    urlpatterns.append(url(r'^init-data/report_17/',Report_17.as_view(),name='Report_17'))
    urlpatterns.append(url(r'^init-data/report_18/',Report_18.as_view(),name='Report_18'))
    urlpatterns.append(url(r'^init-data/report_19/',Report_19.as_view(),name='Report_19'))
    urlpatterns.append(url(r'^init-data/report_20/',Report_20.as_view(),name='Report_20'))
    urlpatterns.append(url(r'^init-data/report_21/',Report_21.as_view(),name='Report_21'))
    urlpatterns.append(url(r'^init-data/report_22/',Report_22.as_view(),name='Report_22'))
    urlpatterns.append(url(r'^init-data/report_23/',Report_23.as_view(),name='Report_23'))
    urlpatterns.append(url(r'^init-data/report_24/',Report_24.as_view(),name='Report_24'))
    urlpatterns.append(url(r'^init-data/report_25/',Report_25.as_view(),name='Report_25'))
    urlpatterns.append(url(r'^init-data/report_26/',Report_26.as_view(),name='Report_26'))
    urlpatterns.append(url(r'^init-data/report_27/',Report_27.as_view(),name='Report_27'))
    urlpatterns.append(url(r'^init-data/report_28/',Report_28.as_view(),name='Report_28'))
    urlpatterns.append(url(r'^init-data/report_29/',Report_29.as_view(),name='Report_29'))
    urlpatterns.append(url(r'^init-data/report_30/',Report_30.as_view(),name='Report_30'))
    urlpatterns.append(url(r'^init-data/report_31/',Report_31.as_view(),name='Report_31'))
    urlpatterns.append(url(r'^init-data/report_32/',Report_32.as_view(),name='Report_32'))
    urlpatterns.append(url(r'^init-data/report_33/',Report_33.as_view(),name='Report_33'))
    urlpatterns.append(url(r'^init-data/report_34/',Report_34.as_view(),name='Report_34'))
    urlpatterns.append(url(r'^init-data/report_35/',Report_35.as_view(),name='Report_35'))
    urlpatterns.append(url(r'^init-data/report_36/',Report_36.as_view(),name='Report_36'))
    urlpatterns.append(url(r'^init-data/report_37/',Report_37.as_view(),name='Report_37'))
    urlpatterns.append(url(r'^init-data/report_38/',Report_38.as_view(),name='Report_38'))
    urlpatterns.append(url(r'^init-data/report_39/',Report_39.as_view(),name='Report_39'))
    urlpatterns.append(url(r'^init-data/report_40/',Report_40.as_view(),name='Report_40'))
    urlpatterns.append(url(r'^init-data/report_41/',Report_41.as_view(),name='Report_41'))
    urlpatterns.append(url(r'^init-data/report_42/',Report_42.as_view(),name='Report_42'))
    
urlpatterns = format_suffix_patterns(urlpatterns)
