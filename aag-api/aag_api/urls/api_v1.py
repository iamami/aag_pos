from django.conf.urls import url
from django.contrib import admin
from rest_framework.authtoken import views
from rest_framework.urlpatterns import format_suffix_patterns
from aag_api.views.bill import *
from aag_api.views.bill_item import *
from aag_api.views.customer import *
from aag_api.views.gold_price import *
from aag_api.views.stock_product import *
from aag_api.views.invoice import *
from aag_api.views.bill_category import *
from aag_api.views.branch_setting import *
from aag_api.views.lease_principle import *
from aag_api.views.lease_interest import *
from aag_api.views.lease_product import *
from aag_api.views.product_name import *
from aag_api.views.lease import *
from aag_api.views.ledger_category import *
from aag_api.views.bill_category import *
from aag_api.views.user_profile import *
from aag_api.views.bill_staff import *
from aag_api.views.stock_category import *
from aag_api.views.bill import *
from aag_api.views.system_settng import *
from aag_api.views.vendor import *
from aag_api.views.category import *
from aag_api.views.product import *
from aag_api.views.product_type import *
from aag_api.views.ledger import *
from aag_api.views.branch import *
from aag_api.views.staff import *
from aag_api.views.user import *
from aag_api.views.bank import *
from aag_api.views.bank_card import *
from aag_api.views.bank_account import *
from aag_api.views.counter import *
from aag_api.views.reward import *
from aag_api.views.stock_reward import *
from aag_api.views.stock_reward_item import *
from aag_api.views.saving import *
from aag_api.views.saving_item import *
from aag_api.views.score import scoreView
from aag_api.views.score_item import scoreItemList,scoreItemCreateList
from aag_api.views.redeem import *

urlpatterns = [

    # bill
    url(r'^v1/bill/(?P<bill>[0-9]+)/tax/$', BillTax.as_view()),

    # invoice
    url(r'^v1/invoice_item/$', InvoiceItemList.as_view()),
    url(r'^v1/invoice_item/(?P<pk>[0-9]+)/$', InvoiceItemDetail.as_view()),
    url(r'^v1/invoices/$', InvoiceList.as_view()),
    url(r'^v1/invoices/(?P<pk>[0-9]+)/$', InvoiceDetail.as_view()),
    url(r'^v1/invoices/(?P<invoice>[0-9]+)/items/$', InvoiceItemViewList.as_view()),
    url(r'^v1/invoices/(?P<invoice>[0-9]+)/stock/$', InvoiceUpdateStock.as_view()),

    # system setting
    url(r'^v1/system_setting/$', SystemSettingList.as_view()),
    url(r'^v1/system_setting/(?P<pk>[0-9]+)/$', SystemSettingDetail.as_view()),

    # vendors
    url(r'^v1/vendors/$', VendorList.as_view(),name='vendor-list'),
    url(r'^v1/vendors/(?P<pk>[0-9]+)/$', VendorDetail.as_view(),name='vendor-detail'),

    # category
    url(r'^v1/categories/$', CategoryList.as_view(),name='category-list'),
    url(r'^v1/categories/(?P<pk>[0-9]+)/$', CategoryDetail.as_view(),name='category-detail'),

    # product type
    url(r'^v1/product_types/$', ProductTypeList.as_view(),name='product-type-list'),
    url(r'^v1/product_types/(?P<pk>[0-9]+)/$', ProductTypeDetail.as_view(),name='product-type-detail'),

    # product
    url(r'^v1/products_data/$', ProducrtDatatList.as_view()),
    url(r'^v1/products/$', ProductList.as_view(),name='product-list'),
    url(r'^v1/products/(?P<pk>[0-9]+)/$', ProductDetail.as_view(),name='product-detail'),

    # ledger
    url(r'^v1/ledger/$', LedgerList.as_view()),
    url(r'^v1/ledger/(?P<pk>[0-9]+)/$', LedgerDetail.as_view()),

    # branch
    url(r'^v1/branches/$', BranchList.as_view(),name="branch-list"),
    url(r'^v1/branches/(?P<pk>[0-9]+)/$', BranchDetail.as_view(),name="branch-detail"),

    # staff
    url(r'^v1/staffs/$', StaffList.as_view(),name='staff-list'),
    url(r'^v1/staffs/(?P<pk>[0-9]+)/$', StaffDetail.as_view(),name='staff-detail'),

    # customer
    url(r'^v1/customers/$', CustomerList.as_view(),name='customer-list'),
    url(r'^v1/customers/(?P<pk>[0-9]+)/$', CustomerDetail.as_view(),name='customer-detail'),
    url(r'^v1/customers/(?P<pk>[0-9]+)/(?P<secret>[a-z0-9]{7})/$', CustomerQrcodeView.as_view(),name='customer-qrcode'),
    url(r'^v1/customers/(?P<pk>[0-9]+)/bill_item/$', CustomerBillItemList.as_view(),name='customer-bill_item-list'),

    # bill (sell,buy,change)
    url(r'^v1/bills/$', BillList.as_view(),name='bill-list'),
    url(r'^v1/bills/(?P<pk>[0-9]+)/$', BillDetail.as_view()),
    url(r'^v1/bill/(?P<bill>[0-9]+)/stock_update/$', BillUpdateStock.as_view()),
    url(r'^v1/bill/(?P<bill>[0-9]+)/void/$', VoidBillUpdateStock.as_view(),name='bill-void'),

    # bill item
    url(r'^v1/bill/(?P<bill>[0-9]+)/items/$', BillItemList2.as_view()),
    url(r'^v1/bill_items/$', BillItemList.as_view()),
    url(r'^v1/bill/(?P<bill>[0-9]+)/items/(?P<pk>[0-9]+)/$', BillItemDetail.as_view()),

    # stock product
    url(r'^v1/stock_product/$', StockProductList.as_view(),name='stock-product-list'),
    url(r'^v1/stock_product/(?P<pk>[0-9]+)/$', StockProductDetail.as_view()),
    url(r'^v1/stock_product/(?P<stock>[0-9]+)/items$', StockProductItemList.as_view()),
    url(r'^v1/stock_product_buy_total/$', StockProductBuyTotal.as_view()),
    
    # stock category
    url(r'^v1/stock_category/$', StockCategoryList.as_view()),
    url(r'^v1/stock_category/(?P<pk>[0-9]+)/$', StockCategoryDetail.as_view()),

    # user
    url(r'^v1/users/$', UserList.as_view(),name='user-list'),
    url(r'^v1/users/(?P<pk>[0-9]+)/$', UserDetail.as_view(),name='user-detail'),

    # bank
    url(r'^v1/banks/$', BankList.as_view(),name='bank-list'),
    url(r'^v1/banks/(?P<pk>[0-9]+)/$', BankDetail.as_view(),name='bank-detail'),

    # bank card
    url(r'^v1/bank_cards/$', BankCardList.as_view(),name='bank_card-list'),
    url(r'^v1/bank_cards/(?P<pk>[0-9]+)/$', BankCardDetail.as_view(),name='bank_card-detail'),

    # bank account
    url(r'^v1/bank_account/$', BankAccountList.as_view(),name='bank_account-list'),
    url(r'^v1/bank_account/(?P<pk>[0-9]+)/$', BankAccountDetail.as_view(),name='bank_account-detail'),
    

    # counter
    url(r'^v1/counter/$', CounterDetail.as_view()),

    # bill staff
    url(r'^v1/bill_staff/$', BillStaffList.as_view()),
    url(r'^v1/bill_staff/(?P<pk>[0-9]+)/$', BillStaffDetail.as_view()),

    # user profile
    url(r'^v1/user_profile/$', UserProfileView.as_view(),name='user_profile'),
    url(r'^v1/userprofiles/$', UserProfileList.as_view(),name='userprofiles-list'),
    url(r'^v1/userprofiles/(?P<pk>[0-9]+)/$', UserProfileDetail.as_view(),name='userprofiles-detail'),
    

    # bill category
    url(r'^v1/bill_category/$', BillCategoryList.as_view()),
    url(r'^v1/bill_category/(?P<pk>[0-9]+)/$', BillCategoryDetail.as_view()),
    url(r'^v1/bill_category/(?P<bill_category>[0-9]+)/items/$', BillCategoryItemList.as_view()),
    url(r'^v1/bill_category/(?P<bill_category>[0-9]+)/items/(?P<pk>[0-9]+)/$',BillCategoryItemDetail.as_view()),
    url(r'^v1/bill_category/(?P<bill_category>[0-9]+)/stock/$', BillCategoryStock.as_view()),

    # ledger category
    url(r'^v1/ledger_category/$', LedgerCategoryList.as_view(),name='ledger_category-list'),
    url(r'^v1/ledger_category/(?P<pk>[0-9]+)/$', LedgerCategoryDetail.as_view(),name='ledger_category-detail'),  

    # saveing
    #url(r'^v1/saving/number/branch/(?P<branch>[0-9]+)/$', SavingNumberView.as_view()),  
    url(r'^v1/saving/$', SavingList.as_view()),
    url(r'^v1/saving/(?P<pk>[0-9]+)/$', SavingDetail.as_view()),
    url(r'^v1/saving/(?P<saving>[0-9]+)/item/$', SavingItemList.as_view()),
    url(r'^v1/saving/add_score/$', SavingAddScore.as_view()),
    

    # branch setting
    url(r'^v1/branch_setting/$', BranchSettingList.as_view()),
    url(r'^v1/branch_setting/(?P<pk>[0-9]+)/$', BranchSettingDetail.as_view()),

    # lease
    url(r'^v1/lease/$', LeaseList.as_view()),
    url(r'^v1/lease/(?P<pk>[0-9]+)/$', LeaseDetail.as_view()),
    url(r'^v1/lease_report/$', LeaseReport.as_view()),
    url(r'^v1/lease/(?P<lease>[0-9]+)/stock/$', LeaseUpdateStock.as_view()),

    # product name
    url(r'^v1/product_name/$', ProductNameList.as_view()),
    url(r'^v1/product_name/(?P<pk>[0-9]+)/$', ProductNameDetail.as_view()),

    # lease product
    url(r'^v1/lease/(?P<lease>[0-9]+)/product/$', LeaseProductList.as_view()),
    url(r'^v1/lease/(?P<lease>[0-9]+)/product/(?P<pk>[0-9]+)/$', LeaseProductDetail.as_view()),

    # lease interest
    url(r'^v1/lease/(?P<lease>[0-9]+)/interest/$', LeaseInterestList.as_view()),
    url(r'^v1/lease/(?P<lease>[0-9]+)/interest/(?P<pk>[0-9]+)/$', LeaseInterestDetail.as_view()),
    url(r'^v1/lease/interest/report/$', LeaseInterestReport.as_view()),

    # lease principle
    url(r'^v1/lease/(?P<lease>[0-9]+)/principle/$', LeasePrincipleList.as_view()),
    url(r'^v1/lease/(?P<lease>[0-9]+)/principle/(?P<pk>[0-9]+)/$', LeasePrincipleDetail.as_view()),

    # gold price
    url(r'^v1/gold_price/$', GoldPriceList.as_view(),name='gold_price-list'),
    url(r'^v1/gold_price/(?P<pk>[0-9]+)/$', GoldPriceDetail.as_view()),

    # reward
    url(r'^v1/reward/$', RewardList.as_view(),name='reward-list'),
    url(r'^v1/reward/(?P<pk>[0-9]+)/$', RewardDetail.as_view(),name='reward-detail'),

    # stock reward
    url(r'^v1/stock_reward/$', StockRewardList.as_view(),name='stock_reward-list'),
    url(r'^v1/stock_reward/(?P<pk>[0-9]+)/$', StockRewardDetail.as_view(),name='stock_reward-detail'),

    # stock reward item
    url(r'^v1/stock_reward_item/$', StockRewardItemList.as_view(),name='stock_reward_item-list'),
    url(r'^v1/stock_reward_item/(?P<pk>[0-9]+)/$', StockRewardItemDetail.as_view(),name='stock_reward_item-detail'),

    # score
    url(r'^v1/score/(?P<customer_id>[0-9]+)/$', scoreView.as_view(),name='score-detail'),
    url(r'^v1/score_item/(?P<customer_id>[0-9]+)/$', scoreItemList.as_view(),name='score-item-list'),
    
    # stock reward
    url(r'^v1/redeem/$', RedeemList.as_view(),name='redeem-list'),
    url(r'^v1/redeem/(?P<pk>[0-9]+)/$', RedeemDetail.as_view(),name='redeem-detail'),
]
urlpatterns = format_suffix_patterns(urlpatterns)
