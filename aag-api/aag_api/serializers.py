from rest_framework import serializers
from aag_api.models import *
from django.contrib.auth.models import User,Group
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from rest_framework.reverse import reverse
from versatileimagefield.serializers import VersatileImageFieldSerializer

serializers.CharField.default_error_messages['blank'] = '*ต้องไม่เป็นค่าว่าง'
serializers.CharField.default_error_messages['unique_together'] = '*ต้องไม่เป็นค่าว่าง'
serializers.IntegerField.default_error_messages['min_value'] = 'ต้องมากกว่า %(limit_value)s'

class SystemSettingSerializer(serializers.ModelSerializer):
    class Meta:
        model = SystemSetting
        fields = '__all__'
class InvoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Invoice
        fields = '__all__'
        read_only_fields = ('user',)
class BankSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bank
        fields = '__all__'

class BankCardFullSerializer(serializers.ModelSerializer):
    bank = BankSerializer()

    class Meta:
        model = BankCard
        fields = '__all__'

class BankCardSerializer(serializers.ModelSerializer):
    class Meta:
        model = BankCard
        fields = '__all__'

class BankAccountFullSerializer(serializers.ModelSerializer):
    bank = BankSerializer()

    class Meta:
        model = BankAccount
        fields = '__all__'

class BankAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = BankAccount
        fields = '__all__'

class InvoiceItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = InvoiceItem
        fields = '__all__'
        #extra_kwargs = {"non_field_errors": {"error_messages": {"unique": "Give yourself a username"}}}

class VendorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Vendor
        fields = '__all__'

class CategorySerializer(serializers.ModelSerializer):

    description = serializers.CharField(required=False, max_length=255, allow_blank=True)
    class Meta:
        model = Category
        fields = '__all__'


class ProductTypeSerializer(serializers.ModelSerializer):
    description = serializers.CharField(required=False, max_length=255, allow_blank=True)
    class Meta:
        model = ProductType
        fields = '__all__'


class WeightSerializer(serializers.ModelSerializer):

    class Meta:
        model = Weight
        fields = '__all__'


class StyleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Style
        fields = '__all__'

class LedgerSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Ledger
        fields = '__all__'
        read_only_fields = ['user','ledger_date',]

class ProductSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Product
        fields = '__all__'

class ProductFullSerializer(serializers.ModelSerializer):
    category = CategorySerializer()
    kind = ProductTypeSerializer()
    class Meta:
        model = Product
        fields = '__all__'
        
class BranchShortSerializer(serializers.ModelSerializer):

    class Meta:
        model = Branch
        fields = ('id','name')

class BranchSerializer(serializers.ModelSerializer):

    description = serializers.CharField(required=False, max_length=255, allow_blank=True,default='')
    class Meta:
        model = Branch
        fields = '__all__'

class StaffFullSerializer(serializers.ModelSerializer):
    branch = BranchSerializer()
    class Meta:
        model = Staff
        fields = '__all__'

class StaffSerializer(serializers.ModelSerializer):

    address = serializers.CharField(required=False, max_length=255, allow_blank=True,default='')
    phone = serializers.CharField(required=False, max_length=20, allow_blank=True,default='')
    class Meta:
        model = Staff
        fields = '__all__'

class StaffShortSerializer(serializers.ModelSerializer):

    class Meta:
        model = Staff
        fields = ('code','name','id')

class CustomerSerializer(serializers.ModelSerializer):

    def validate_email(self,value):
        return value or None
    def validate_mobile(self,value):
        return value or None
        
    def validate(self, data):
        if 'email' not in data or data['email'] == '':
            data['email'] = None
        if 'mobile' not in data or data['mobile'] == '':
            data['mobile'] = None

        if data['is_application'] and data['email'] == None and data['mobile'] == None:
            raise serializers.ValidationError({'error' : 'เบอร์มือถือหรือ อีเมล ต้องไม่เป็นค่าว่าง'})
        
        return data

    
    class Meta:
        model = Customer
        fields = '__all__'
        read_only_fields = ('code',)

class CustomerGetSerializer(serializers.ModelSerializer):

    def validate_email(self,value):
        return value or None
    def validate_mobile(self,value):
        return value or None
        
    def validate(self, data):
        if 'email' not in data or data['email'] == '':
            data['email'] = None
        if 'mobile' not in data or data['mobile'] == '':
            data['mobile'] = None

        if data['is_application'] and data['email'] == None and data['mobile'] == None:
            raise serializers.ValidationError({'error' : 'เบอร์มือถือหรือ อีเมล ต้องไม่เป็นค่าว่าง'})
        
        return data

    image = VersatileImageFieldSerializer(
        sizes=[
            ('full_size', 'url'),
            ('thumbnail', 'thumbnail__300x300'),
            ('crop', 'crop__300x300')
        ]
    )
    
    class Meta:
        model = Customer
        fields = '__all__'
        read_only_fields = ('code','image',)

class CustomerShortSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = ('code','name','id')

class GoldPriceSerializer(serializers.ModelSerializer):
    class Meta:
        model = GoldPrice
        fields = '__all__'

class BillFullSerializer(serializers.ModelSerializer):
    branch = BranchSerializer()
    description = serializers.CharField(required=False, max_length=255, allow_blank=True)
    customer = CustomerSerializer()
    gold_price = GoldPriceSerializer()
    kind_display = serializers.CharField(source='get_kind_display')
    status_stock_display = serializers.CharField(source='get_status_stock_display')
    payment_display = serializers.CharField(source='get_payment_display')
    ledger = serializers.SerializerMethodField()
    class Meta:
        model = Bill
        fields = '__all__'
    def get_ledger(self,object):

        l = Ledger.objects.all().filter(object_id=object.id)
        if len(l)>0:
            serializer = LedgerSerializer(l[0])
            return serializer.data
        else:
            return ""


class BillSerializer(serializers.ModelSerializer):
    description = serializers.CharField(required=False, max_length=255, allow_blank=True)
    class Meta:
        model = Bill
        fields = '__all__'

class BillItemSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = BillItem
        fields = '__all__'

class BillItemCreateMultiSerializer(serializers.ModelSerializer):
    class Meta:
        model = BillItem
        fields = '__all__'

class BillItemFullSerializer(serializers.ModelSerializer):
    bill = BillFullSerializer()
    category = CategorySerializer()
    product = ProductFullSerializer()
    status_stock_display = serializers.CharField(source='get_status_stock_display')
    kind_display = serializers.CharField(source='get_kind_display')
    class Meta:
        model = BillItem
        fields = '__all__'

class BillCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = BillCategory
        fields = '__all__'
        read_only_fields = ('user',)


class BillCategoryItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = BillCategoryItem
        fields = '__all__'

class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(required=False, max_length=128, allow_blank=True)
    username = serializers.CharField(required=False, max_length=150, allow_blank=True)
    class Meta:
        model = User
        fields = ('id','username', 'is_staff','is_superuser','password','is_active')
        extra_kwargs = {'password': {'write_only': True,'is_superuser':True}}
class UserGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id','username', 'is_staff','is_superuser','password','is_active')
        extra_kwargs = {'password': {'write_only': True}}

class CategoryViewSerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ('id','name','weight','m_buy','m_sell','discount_buy')

class StaffViewSerializer(serializers.ModelSerializer):

    class Meta:
        model = Staff
        fields = ('id','name')
class ProductTypeViewSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductType
        fields = ('id','name')

class ProducrtDatatSerializer(serializers.ModelSerializer):

    category = CategoryViewSerializer()
    kind = ProductTypeViewSerializer()
    class Meta:
        model = Product
        fields = ('id','name','code','category','kind','price_tag','cost','type_sale','weight')

class BranchViewSerializer(serializers.ModelSerializer):

    class Meta:
        model = Branch
        fields = ('id','name','code')

class InvoiceItemViewSerializer(serializers.ModelSerializer):
    #user = UserInfoSerializer()
    product = ProducrtDatatSerializer()
    invoice = InvoiceSerializer()
    class Meta:
        model = InvoiceItem
        fields = '__all__'
        read_only_fields = ['user']

class StockProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = StockProduct
        fields = '__all__'
        
class StockProductGetSerializer(serializers.ModelSerializer):
    product = ProducrtDatatSerializer()
    branch = BranchViewSerializer()
    class Meta:
        model = StockProduct
        fields = '__all__'
        
class StockCategorySerializer(serializers.ModelSerializer):
    branch = BranchViewSerializer()
    category = CategorySerializer()
    class Meta:
        model = StockCategory
        fields = '__all__'

class UserInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id','first_name','username', 'email','is_staff','is_superuser','is_active')


class UserProfileFullSerializer(serializers.ModelSerializer):
    user = UserInfoSerializer()
    staff = StaffSerializer()
    branch = BranchSerializer()
    class Meta:
        model = UserProfile
        fields = '__all__'

class UserProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    class Meta:
        model = UserProfile
        fields = '__all__'

    def update(self, instance, validated_data):

        user = instance.user
        user.username=validated_data['user']['username']
        user.is_superuser=validated_data['user']['is_superuser']
        user.is_active=validated_data['user']['is_active']

        if 'password' in validated_data['user']:
            user.set_password(validated_data['user']['password'])
        user.save()

        instance.user = user
        instance.branch = validated_data['branch']
        instance.role = validated_data['role']

        if 'staff' in validated_data:
            instance.staff = validated_data['staff']
        instance.save()
        return instance
    
    def create(self, validated_data):

        group = Group.objects.get(name='mgmt-sys')
        user = get_user_model().objects.create(
            username=validated_data['user']['username'],
            is_superuser=validated_data['user']['is_superuser'],
            is_active=validated_data['user']['is_active']
        )

        user.set_password(validated_data['user']['password'])
        user.save()
        group.user_set.add(user)
        user_profile = UserProfile.objects.create(
            user=user,
            branch=validated_data['branch'],
            role=validated_data['role']
        )
        if 'staff' in validated_data:
            user_profile.staff = validated_data['staff']

        return user_profile

class StockProductItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = StockProductItem
        fields = '__all__'
class StockProductItemFullSerializer(serializers.ModelSerializer):
    branch = BranchSerializer()
    product = ProductFullSerializer()
    kind_display = serializers.SerializerMethodField()
    class Meta:
        model = StockProductItem
        fields = '__all__'
    def get_kind_display(self,obj):
        return obj.get_kind_display()
    
class BillStaffSerializer(serializers.ModelSerializer):

    class Meta:
        model = BillStaff
        fields = '__all__'
class BillStaffFullSerializer(serializers.ModelSerializer):
    bill = BillSerializer()
    staff = StaffSerializer()
    class Meta:
        model = BillStaff
        fields = '__all__'

class BillCategoryItemFullSerializer(serializers.ModelSerializer):
    stock =  StockCategorySerializer()
    bill_category =  BillCategorySerializer()
    class Meta:
        model = BillCategoryItem
        fields = '__all__'

class LedgerCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = LedgerCategory
        fields = '__all__'

class LedgerFullSerializer(serializers.ModelSerializer):
    branch = BranchSerializer()
    customer = CustomerSerializer()
    customer_name = serializers.SerializerMethodField()
    staff_name = serializers.SerializerMethodField()
    object_lease = serializers.SerializerMethodField()
    object_bill = serializers.SerializerMethodField()
    ledger_category = LedgerCategorySerializer()

    class Meta:
        model = Ledger
        fields = '__all__'

    def get_customer_name(self, obj):
        return "" if obj.customer is None or obj.customer == '' else obj.customer.name
    def get_staff_name(self, obj):
        return "" if obj.staff is None or obj.staff == '' else obj.staff.name
    def get_object_lease(self, obj):
        if obj.ledger_category.id ==10:
            return LeaseSerializer(Lease.objects.get(pk=obj.object_id)).data
        else:
            return ""
    
    def get_object_bill(self, obj):
        if obj.ledger_category.id in [1,2,3]:
            try:
                return BillSerializer(Bill.objects.get(pk=obj.object_id)).data
            except Bill.DoesNotExist:
                return ""
        else:
            return ""

class SavingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Saving
        fields = '__all__'

class SavingItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = SavingItem
        fields = '__all__'

class SavingItemCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = SavingItem
        fields = ('branch','staff','price','kind')

class SavingItemFullSerializer(serializers.ModelSerializer):
    branch = BranchSerializer()
    staff = StaffSerializer()
    kind_display = serializers.CharField(source='get_kind_display')
    class Meta:
        model = SavingItem
        fields = '__all__'

class BranchSettingSerializer(serializers.ModelSerializer):
    class Meta:
        model = BranchSetting
        fields = '__all__'

class SavingFullSerializer(serializers.ModelSerializer):
    branch = BranchSerializer()
    customer = CustomerShortSerializer()
    staff = StaffShortSerializer()
    class Meta:
        model = Saving
        fields = '__all__'
class ProductNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductName
        fields = '__all__'

class LeaseSerializer(serializers.ModelSerializer):

    number = serializers.CharField(required=False, allow_blank=True)
    class Meta:
        model = Lease
        fields = '__all__'
    
class LeaseCreateSerializer(serializers.ModelSerializer):

    number = serializers.CharField(required=False, allow_blank=True)
    class Meta:
        model = Lease
        fields = '__all__'
        read_only_fields = ('user',)
    
    def validate(self, data):

        if 'number' not in data :
            o = BranchSetting.objects.get(branch=data['branch'])
            number = o.code + "%05d" % (o.counter)
            
            if len(Lease.objects.all().filter(number=number,branch=data['branch']))>0:
                raise serializers.ValidationError({'number':['กรุณาตั้งค่าเลขที่ขายฝากสาขา']})
            data['number'] = number
            o.counter = o.counter + 1
            o.save()
        return data

class LeaseUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lease
        fields = ('customer','citizen_id','phone','start_date','end_date')
        
class LeaseFullSerializer(serializers.ModelSerializer):
    branch = BranchSerializer()
    customer = CustomerSerializer()
    staff = StaffSerializer()
    lease_prodcut = serializers.StringRelatedField(many=True)
    class Meta:
        model = Lease
        fields = '__all__'

class LeaseProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = LeaseProduct
        fields = '__all__'
        read_only_fields = ('user',)

class LeaseFullProductSerializer(serializers.ModelSerializer):
    lease = LeaseFullSerializer()
    category = CategorySerializer()
    class Meta:
        model = LeaseProduct
        fields = '__all__'

class LeaseInterestSerializer(serializers.ModelSerializer):
    class Meta:
        model = LeaseInterest
        fields = '__all__'
        read_only_fields = ('user',)

class LeaseInterestFullSerializer(serializers.ModelSerializer):

    ledger = LedgerFullSerializer()
    lease = LeaseFullSerializer()
    class Meta:
        model = LeaseInterest
        fields = '__all__'
class LeasePrincipleSerializer(serializers.ModelSerializer):
    class Meta:
        model = LeasePrinciple
        fields = '__all__'

class LeasePrincipleFullSerializer(serializers.ModelSerializer):
    lease = LeaseFullSerializer()
    class Meta:
        model = LeasePrinciple
        fields = '__all__'

class ParameterisedHyperlinkedIdentityField(serializers.HyperlinkedIdentityField):

    lookup_fields = (('pk', 'secret'),)

    def __init__(self, *args, **kwargs):
        self.lookup_fields = kwargs.pop('lookup_fields', self.lookup_fields)
        super(ParameterisedHyperlinkedIdentityField, self).__init__(*args, **kwargs)

    def get_url(self, obj, view_name, request, format):
        kwargs = {}
        for model_field, url_param in self.lookup_fields:
            attr = obj
            for field in model_field.split('.'):
                attr = getattr(attr,field)
            kwargs[url_param] = attr

        return reverse(view_name, kwargs=kwargs, request=request, format=format)

class CustomerCodeSecuritySerializer(serializers.HyperlinkedModelSerializer):
    customer_qrcode = ParameterisedHyperlinkedIdentityField(view_name='customer-qrcode', lookup_fields=(('pk', 'pk'), ('secret', 'secret')), read_only=True)

    class Meta:
        model = Customer
        fields = ('customer_qrcode','url','name','code','citizen_id','id','secret')
        read_only_fields = ('code',)

class RewardShortSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reward
        fields = ('id','code','name')

class RewardCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Reward
        fields = '__all__'
        read_only_fields = ('code',)

class RewardSerializer(serializers.ModelSerializer):
    is_enabled_display = serializers.CharField(source='get_is_enabled_display',)
    image = VersatileImageFieldSerializer(
        sizes=[
            ('full_size', 'url'),
            ('thumbnail', 'thumbnail__300x300'),
            ('crop', 'crop__300x300'),
            ('small_square_crop', 'crop__100x100')
        ]
    )
    class Meta:
        model = Reward
        fields = ('code','name','image','score','created_datetime','detail','id','is_enabled_display','is_enabled')
        read_only_fields = ('code','is_enabled_display',)
        
class RewardListSerializer(serializers.ModelSerializer):
    image = VersatileImageFieldSerializer(
        sizes=[
            ('full_size', 'url'),
            ('crop', 'crop__300x300')
        ]
    )
    class Meta:
        model = Reward
        fields = ('code','name','image','score','created_datetime','id',)
        read_only_fields = ('code',)

class StockRewardSerializer(serializers.ModelSerializer):
    class Meta:
        model = StockReward
        fields = '__all__'

class StockRewardCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = StockReward
        fields = ('reward','amount')

class RedeemSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Redeem
        fields = '__all__'

class RedeemFullSerializer(serializers.ModelSerializer):
    kind_display = serializers.CharField(source='get_kind_display')
    status_display = serializers.CharField(source='get_status_display')
    reward = RewardSerializer()
    customer = CustomerSerializer()
    branch = BranchShortSerializer()
    staff = StaffShortSerializer()
    class Meta:
        model = Redeem
        fields = '__all__'

class RedeemCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Redeem
        fields = ('branch','customer','reward','amount','status','kind','remrak','address','receipt_date','staff')

class StockRewardFullSerializer(serializers.ModelSerializer):

    reward = RewardSerializer()
    class Meta:
        model = StockReward
        fields = '__all__'

class StockRewardItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = StockRewardItem
        fields = '__all__'

class StockRewardItemFullSerializer(serializers.ModelSerializer):
    stock_reward = StockRewardFullSerializer()
    user = UserSerializer()
    kind_display = serializers.CharField(source='get_kind_display')
    redeem = RedeemSerializer()
    class Meta:
        model = StockRewardItem
        fields = '__all__'

class StockRewardItemSubmitSerializer(serializers.ModelSerializer):
    class Meta:
        model = StockRewardItem
        fields = ('stock_reward','amount','kind','description')


class ScoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Score
        fields = '__all__'
class ScoreItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = ScoreItem
        fields = '__all__'

class ScoreItemFullSerializer(serializers.ModelSerializer):

    action_display = serializers.CharField(source='get_action_display')
    kind_display = serializers.CharField(source='getString')
    creator = UserSerializer()
    bill_item = BillItemFullSerializer()
    class Meta:
        model = ScoreItem
        fields = '__all__'


class SavingAddScoreSerializer(serializers.Serializer):

    amount = serializers.DecimalField(max_digits=20, decimal_places=2)
    score = serializers.IntegerField(min_value=1)
    customer = serializers.IntegerField()

class CustomerProfileSerializer(serializers.HyperlinkedModelSerializer):
    image = VersatileImageFieldSerializer(
        sizes=[
            ('full_size', 'url'),
            ('thumbnail', 'thumbnail__300x300'),
            ('crop', 'crop__300x300')
        ]
    )
    class Meta:
        model = Customer
        fields = ('name','code','citizen_id','id','nick_name','address','city','province','postal_code','address2','city2','province2','postal_code2','address3','city3','province3','postal_code3','phone','birth_date','nationality','email','image','mobile','district','district2','district3','address_primary')
        read_only_fields = ('code',)

