from django.conf import settings
from django_hosts import patterns, host

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False
IS_INITDATA = False
# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'aagold-mgmt-sys',
        'USER': 'aagold-mgmt-sys',
        'PASSWORD' : 'ExceptSuddenMexicoFollowSunday48081',
        'HOST' : '192.168.55.3',
        'PORT' : '5432',
    }
}

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = '/static/'

MEDIA_URL = '/media/'
MEDIA_ROOT = '/media/'

PARENT_HOST = 'aagold-th.com'

host_patterns = patterns('',
    host(r'mgmt-sys', 'aag_api.urls', name='aag_api', scheme='http:', port='8000'),
    host(r'mapi', 'mapi.urls', name='mapi',scheme='http:', port='8000'),
)