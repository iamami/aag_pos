# -*- coding: utf-8 -*-
'''
Contact is used to fill Basic information

'''

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from page_object.basepage import BasePage, BasePageLocator
from page_object.data import BaseData, FileData, FileUserData
from page_object.file.file import FilePage, FilePageLocator


class FileUser(FilePage):


    base_url = None
    button_add = None
    button_edit_1 = None
    button_delete_1 = None

    text_branch = None
    text_user_name = None
    text_user_password = None
    text_user_confirm_password = None
    text_staff = None
    text_level = None
    table_cells = None


    data = None

    def __init__(self, browser):
        self.base_url = super(FileUser, self).base_url
        super(FileUser, self).__init__(browser)

        self.data = FileUserData()
        '''
        # someting addition on class __init__()
        '''

    def initElement(self):
        # Base Elements of Admin
        super(FileUser, self).initElement()


    def initPageElement(self):
        self.waitLoadingDisappeared() #content area loading
        self.button_add = self.browser.find_element(*FileUserPageLocator.BUTTON_ADD)

    def initTableElement(self):
        self.button_edit_1 = self.browser.find_element(*FileUserPageLocator.BUTTON_EDIT_1)
        self.button_delete_1 = self.browser.find_element(*FileUserPageLocator.BUTTON_DELETE_1)

    def initFormElement(self):
        #self.text_user_code = self.browser.find_element(*FileUserPageLocator.TEXT_USER_CODE)
        self.text_user_name = self.browser.find_element(*FileUserPageLocator.TEXT_USER_NAME)
        self.text_user_password = self.browser.find_element(*FileUserPageLocator.TEXT_USER_PASSWORD)
        self.text_user_confirm_password = self.browser.find_element(*FileUserPageLocator.TEXT_USER_CONFIRM_PASSWORD)
        self.text_branch = self.browser.find_element(*FileUserPageLocator.TEXT_BRANCH)
        self.text_staff = self.browser.find_element(*FileUserPageLocator.TEXT_STAFF)
        #self.text_level = self.wait.until(EC.presence_of_element_located(FileUserPageLocator.TEXT_LEVEL))
        self.text_level = self.browser.find_element(*FileUserPageLocator.TEXT_LEVEL)


    def navigateFileUser(self):
        self.initHeadMenuElement()
        self.navigateFile()
        self.initLeftMenuElement()
        self.navigateUser()




    def clearForm(self):
        self.text_staff.clear()
        self.text_level.clear()
        self.text_user_name.clear()
        self.text_user_password.clear()
        self.text_user_confirm_password.clear()



class FileUserPageLocator(FilePageLocator):
    BUTTON_ADD = (By.XPATH, '//*[@id="content-body"]/div[1]/form[1]/div[1]/div[2]/button[1]')
    BUTTON_EDIT_1 = (By.XPATH, '//*[@id="content-body"]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/center[1]/a[1]/i[1]')
    BUTTON_DELETE_1 = (By.XPATH, '//*[@id="content-body"]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/center[1]/a[2]/i[1]')

    TEXT_BRANCH = (By.XPATH, '//label[contains(text(),"*สาขา")]/following-sibling::div[1]/input[1]')
    TEXT_LEVEL = (By.XPATH, '//label[contains(text(),"*ระดับ")]/following-sibling::div[1]/input[1]')
    TEXT_STAFF = (By.XPATH, '//label[contains(text(),"พนักงาน")]/following-sibling::div[1]/input[1]')
    TEXT_USER_NAME = (By.XPATH, '//div[@class="description"]/form/div[2]/div[1]/input')
    TEXT_USER_PASSWORD = (By.XPATH, '//div[@class="description"]/form/div[3]/div[1]/input')
    TEXT_USER_CONFIRM_PASSWORD = (By.XPATH, '//div[@class="description"]/form/div[4]/div[1]/input')
