# -*- coding: utf-8 -*-
'''
Contact is used to fill Basic information

'''

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from page_object.basepage import BasePage, BasePageLocator
from page_object.data import BaseData, FileData


class FilePage(BasePage):


    base_url = None
    link_file_branch = None
    link_file_staff = None
    link_file_user = None
    link_file_product_group = None

    def __init__(self, browser):
        self.base_url = super(FilePage, self).base_url
        super(FilePage, self).__init__(browser)
        '''
        # someting addition on class __init__()
        '''


    def initElement(self):
        # Base Elements of Admin
        super(FilePage, self).initElement()


    def initLeftMenuElement(self):
        self.link_file_branch = self.browser.find_element(*FilePageLocator.LINK_FILE_BRANCH)
        self.link_file_staff = self.browser.find_element(*FilePageLocator.LINK_FILE_STAFF)
        self.link_file_user = self.browser.find_element(*FilePageLocator.LINK_FILE_USER)
        self.link_file_product_group = self.browser.find_element(*FilePageLocator.LINK_FILE_PRODUCT_GROUP)

    def navigateBranch(self):
        self.link_file_branch.click()

    def navigateStaff(self):
        self.link_file_staff.click()

    def navigateUser(self):
        self.link_file_user.click()

    def navigateProductGroup(self):
        self.link_file_product_group.click()




class FilePageLocator(BasePageLocator):
    LINK_FILE_BRANCH = (By.XPATH, '//div[@class="ui secondary vertical menu-left menu"]/a[1]')
    LINK_FILE_STAFF = (By.XPATH, '//div[@class="ui secondary vertical menu-left menu"]/a[2]')
    LINK_FILE_USER = (By.XPATH, '//div[@class="ui secondary vertical menu-left menu"]/a[3]')
    LINK_FILE_PRODUCT_GROUP = (By.XPATH, '//div[@class="ui secondary vertical menu-left menu"]/a[4]')
