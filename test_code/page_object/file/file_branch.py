# -*- coding: utf-8 -*-
'''
Contact is used to fill Basic information

'''

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from page_object.basepage import BasePage, BasePageLocator
from page_object.data import BaseData, FileData, BranchData, FileBranchData
from page_object.file.file import FilePage, FilePageLocator


class FileBranch(FilePage):


    base_url = None
    button_add = None
    button_edit_1 = None
    button_delete_1 = None

    text_branch_code = None
    text_branch_name = None
    text_branch_note = None
    button_generate_branch_code = None
    table_cells = None


    data = None

    def __init__(self, browser):
        self.base_url = super(FileBranch, self).base_url
        super(FileBranch, self).__init__(browser)

        self.data = FileBranchData()
        '''
        # someting addition on class __init__()
        '''

    def initElement(self):
        # Base Elements of Admin
        super(FileBranch, self).initElement()


    def initPageElement(self):
        self.waitLoadingDisappeared() #content area loading
        self.button_add = self.browser.find_element(*FileBranchPageLocator.BUTTON_ADD)

    def initTableElement(self):
        self.button_edit_1 = self.browser.find_element(*FileBranchPageLocator.BUTTON_EDIT_1)
        self.button_delete_1 = self.browser.find_element(*FileBranchPageLocator.BUTTON_DELETE_1)

    def initFormElement(self):
        self.text_branch_code = self.browser.find_element(*FileBranchPageLocator.TEXT_BRANCH_CODE)
        self.text_branch_name = self.browser.find_element(*FileBranchPageLocator.TEXT_BRANCH_NAME)
        self.text_branch_note = self.browser.find_element(*FileBranchPageLocator.TEXT_BRANCH_NOTE)
        self.button_generate_branch_code = self.browser.find_element(*FileBranchPageLocator.BUTTON_GENERATE_BRANCH_CODE)


    def navigateFileBranch(self):
        self.initHeadMenuElement()
        self.navigateFile()
        self.initLeftMenuElement()
        self.navigateBranch()




    def clearForm(self):
        self.text_branch_code.clear()
        self.text_branch_name.clear()
        self.text_branch_note.clear()



class FileBranchPageLocator(FilePageLocator):
    BUTTON_ADD = (By.XPATH, '//*[@id="content-body"]/div[1]/form[1]/div[1]/div[2]/button[1]')
    BUTTON_EDIT_1 = (By.XPATH, '//*[@id="content-body"]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/center[1]/a[1]/i[1]')
    BUTTON_DELETE_1 = (By.XPATH, '//*[@id="content-body"]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/center[1]/a[2]/i[1]')

    TEXT_BRANCH_CODE = (By.XPATH, '//div[@class="description"]/form/div[1]/div[1]/input')
    BUTTON_GENERATE_BRANCH_CODE = (By.XPATH, '//div[@class="description"]/form/div[1]/div[1]/button')
    TEXT_BRANCH_NAME = (By.XPATH, '//div[@class="description"]/form/div[2]/div[1]/input')
    TEXT_BRANCH_NOTE = (By.XPATH, '//div[@class="description"]/form/div[3]/textarea')
