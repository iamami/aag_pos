# -*- coding: utf-8 -*-
'''
Contact is used to fill Basic information

'''

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from page_object.basepage import BasePage, BasePageLocator
from page_object.data import BaseData, FileData, FileProductGroupData
from page_object.file.file import FilePage, FilePageLocator


class FileProductGroup(FilePage):


    base_url = None
    button_add = None

    text_code = None
    text_name = None
    text_weight = None
    text_multiplier_buy = None
    text_multiplier_sell = None
    text_buy_deduct = None
    text_note = None


    data = None

    def __init__(self, browser):
        self.base_url = super(FileProductGroup, self).base_url
        super(FileProductGroup, self).__init__(browser)

        self.data = FileProductGroupData()
        '''
        # someting addition on class __init__()
        '''

    def initElement(self):
        # Base Elements of Admin
        super(FileProductGroup, self).initElement()


    def initPageElement(self):
        self.waitLoadingDisappeared() #content area loading
        self.button_add = self.browser.find_element(*FileProductGroupPageLocator.BUTTON_ADD)

    def initFormElement(self):

        self.text_code = self.browser.find_element(*FileProductGroupPageLocator.TEXT_CODE)
        self.text_name = self.browser.find_element(*FileProductGroupPageLocator.TEXT_NAME)
        self.text_weight = self.browser.find_element(*FileProductGroupPageLocator.TEXT_WEIGHT)
        self.text_multiplier_buy = self.browser.find_element(*FileProductGroupPageLocator.TEXT_MULTIPLIER_BUY)
        self.text_multiplier_sell = self.browser.find_element(*FileProductGroupPageLocator.TEXT_MULTIPLIER_SELL)
        self.text_buy_deduct = self.browser.find_element(*FileProductGroupPageLocator.TEXT_BUY_DEDUCT)
        self.text_note = self.browser.find_element(*FileProductGroupPageLocator.TEXT_NOTE)


    def navigateFileProductGroup(self):
        self.initHeadMenuElement()
        self.navigateFile()
        self.initLeftMenuElement()
        self.navigateProductGroup()



    def clearForm(self):
        self.text_staff.clear()
        self.text_level.clear()
        self.text_user_name.clear()
        self.text_user_password.clear()
        self.text_user_confirm_password.clear()



class FileProductGroupPageLocator(FilePageLocator):
    BUTTON_ADD = (By.XPATH, '//*[@id="content-body"]/div[1]/form[1]/div[1]/div[2]/button[1]')

    TEXT_CODE = (By.XPATH, '//label[contains(text(),"*รหัส")]/following-sibling::div[1]/input[1]')
    TEXT_NAME = (By.XPATH, '//label[contains(text(),"*ชื่อ")]/following-sibling::div[1]/input[1]')
    TEXT_WEIGHT = (By.XPATH, '//label[contains(text(),"น้ำหนัก(กรัม)/1บาท")]/following-sibling::div[1]/input[1]')
    TEXT_MULTIPLIER_BUY = (By.XPATH, '//label[contains(text(),"ตัวคูณซื้อทอง")]/following-sibling::div[1]/input[1]')
    TEXT_MULTIPLIER_SELL = (By.XPATH, '//label[contains(text(),"ตัวคูณขายทอง")]/following-sibling::div[1]/input[1]')
    TEXT_BUY_DEDUCT = (By.XPATH, '//label[contains(text(),"ค่าหักรับซื้อทองเก่า")]/following-sibling::div[1]/input[1]')
    TEXT_NOTE = (By.XPATH, '//div[@class="description"]/form/div[7]/textarea')
