# -*- coding: utf-8 -*-
'''
Contact is used to fill Basic information

'''

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from page_object.basepage import BasePage, BasePageLocator
from page_object.data import BaseData, FileData, FileStaffData
from page_object.file.file import FilePage, FilePageLocator


class FileStaff(FilePage):


    base_url = None
    button_add = None
    button_edit_1 = None
    button_delete_1 = None

    text_staff_code = None
    text_staff_name = None
    text_staff_address = None
    text_staff_phone = None
    text_select_branch = None
    button_generate_staff_code = None
    table_cells = None

    data = None

    def __init__(self, browser):
        self.base_url = super(FileStaff, self).base_url
        super(FileStaff, self).__init__(browser)

        self.data = FileStaffData()
        '''
        # someting addition on class __init__()
        '''

    def initElement(self):
        # Base Elements of Admin
        super(FileStaff, self).initElement()


    def initPageElement(self):
        self.waitLoadingDisappeared() #content area loading
        self.button_add = self.browser.find_element(*FileStaffPageLocator.BUTTON_ADD)

    def initTableElement(self):
        self.button_edit_1 = self.browser.find_element(*FileStaffPageLocator.BUTTON_EDIT_1)
        self.button_delete_1 = self.browser.find_element(*FileStaffPageLocator.BUTTON_DELETE_1)

    def initFormElement(self):
        self.text_staff_code = self.browser.find_element(*FileStaffPageLocator.TEXT_STAFF_CODE)
        self.text_staff_name = self.browser.find_element(*FileStaffPageLocator.TEXT_STAFF_NAME)
        self.text_staff_address = self.browser.find_element(*FileStaffPageLocator.TEXT_STAFF_ADDRESS)
        self.text_staff_phone = self.browser.find_element(*FileStaffPageLocator.TEXT_STAFF_PHONE)
        self.button_generate_staff_code = self.browser.find_element(*FileStaffPageLocator.BUTTON_GENERATE_STAFF_CODE)
        self.text_select_branch = self.browser.find_element(*FileStaffPageLocator.TEXT_SELECT_BRANCH)

    def navigateFileStaff(self):
        self.initHeadMenuElement()
        self.navigateFile()
        self.initLeftMenuElement()
        self.navigateStaff()




    def clearForm(self):
        self.text_staff_code.clear()
        self.text_staff_name.clear()
        self.text_staff_address.clear()
        self.text_staff_phone.clear()



class FileStaffPageLocator(FilePageLocator):
    BUTTON_ADD = (By.XPATH, '//*[@id="content-body"]/div[1]/form[1]/div[1]/div[2]/button[1]')
    BUTTON_EDIT_1 = (By.XPATH, '//*[@id="content-body"]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/center[1]/a[1]/i[1]')
    BUTTON_DELETE_1 = (By.XPATH, '//*[@id="content-body"]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/center[1]/a[2]/i[1]')

    TEXT_SELECT_BRANCH = (By.XPATH, '//div[@class="description"]/form/div[2]/div[1]/input')
    TEXT_STAFF_CODE = (By.XPATH, '//div[@class="description"]/form/div[1]/div[1]/input')
    BUTTON_GENERATE_STAFF_CODE = (By.XPATH, '//div[@class="description"]/form/div[1]/div[1]/button')
    TEXT_STAFF_NAME = (By.XPATH, '//div[@class="description"]/form/div[3]/div[1]/input')
    TEXT_STAFF_ADDRESS = (By.XPATH, '//div[@class="description"]/form/div[4]/div[1]/input')
    TEXT_STAFF_PHONE = (By.XPATH, '//div[@class="description"]/form/div[5]/div[1]/input')
