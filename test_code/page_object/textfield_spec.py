# -*- coding: utf-8 -*-

import time
import datetime


class File_Branch_BranchCode(object):
    #unique_combiantion = 1 # make value unique for each test success checking
    name = "File - Branch - text branch code"
    individual_form_error_xpath = None
    is_key = True

    is_accept_numeric = True
    is_accept_alphabet = False
    is_accept_decimal = False


    max_length_str = 3
    min_length_str = 3

    max_numeric_value = 999
    min_numeric_value = 1
    mid_numeric = 500

    allowed_special_char = ""

    is_accept_foreign_language_char = False
    is_injection_allow = False
    is_accept_space_bar = False
    is_null_allowed = False


class File_Branch_BranchName(object):
    name = "File - Branch - text branch name"
    #unique_combiantion = "fbnm"
    individual_form_error_xpath = None
    is_key = False

    is_accept_numeric = True
    is_accept_alphabet = True
    is_accept_decimal = False

    max_length_str = 50
    min_length_str = 1

    max_numeric_value = None
    min_numeric_value = None
    mid_numeric = 500

    allowed_special_char = "(),-/"

    is_accept_foreign_language_char = True
    is_injection_allow = False
    is_accept_space_bar = True
    is_null_allowed = False


class File_Branch_BranchNote(object):
    name = "File - Branch - text branch note"
    #unique_combiantion = "fbnt"
    individual_form_error_xpath = None
    is_accept_numeric = True
    is_accept_alphabet = True
    is_accept_decimal = False
    is_key = False

    max_length_str = 50
    min_length_str = 1

    max_numeric_value = None
    min_numeric_value = None
    mid_numeric = 500

    allowed_special_char = "!#$%&'()*+,-./:;<=>?@[\]^_`{|}~"

    is_accept_foreign_language_char = True
    is_injection_allow = False
    is_accept_space_bar = True
    is_null_allowed = True
