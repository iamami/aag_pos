# -*- coding: utf-8 -*-
'''
== UI mapping ==
base page  hader / footer / element of menu, ...
check all JS/CSS/base image exists


== method ==
check response header code
check title
check javascript error

'''

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from page_object.data import BaseData
import urllib
import ast
import time

class BasePage(object):

    #default_wait_time = 10
    base_url = "http://10.11.3.9"
    body = None
    title = None

    text_username = None
    text_password = None
    button_submit = None
    button_close_price_modal = None

    head_link_dashboard = None
    head_link_file = None
    head_link_stock = None
    head_link_customer = None
    head_link_pos = None
    head_link_repurchase = None
    head_link_transaction = None
    head_link_report = None

    modal_button_ok = None
    modal_button_cancel = None

    confirm_button_ok = None
    confirm_button_cancel = None
    form_error = None

    base_data = None

    def __init__(self, browser):
        self.browser = browser
        self.wait = WebDriverWait(self.browser, 10)
        self.base_data = BaseData()

    def loadPage(self):
        self.browser.get(self.base_url)

    def initLoginElement(self):
        self.text_username = self.browser.find_element(*BasePageLocator.TEXT_USERNAME)
        self.text_password = self.browser.find_element(*BasePageLocator.TEXT_PASSWORD)
        self.button_submit = self.browser.find_element(*BasePageLocator.BUTTON_SUBMIT)

    def initFirstModalElement(self):
        #time.sleep(3)
        self.wait.until(EC.presence_of_element_located(BasePageLocator.DIV_LOADING))
        self.wait.until(EC.invisibility_of_element_located(BasePageLocator.DIV_LOADING))
        self.wait.until(EC.visibility_of_element_located(BasePageLocator.BUTTON_CLOSE_PRICE_MODAL)).click()
        #self.button_close_price_modal = self.browser.find_element(*BasePageLocator.BUTTON_CLOSE_PRICE_MODAL)


    def waitLoadingDisappeared(self):
        self.wait.until(EC.presence_of_element_located(BasePageLocator.DIV_CONTENT_LOADING))
        self.wait.until(EC.invisibility_of_element_located(BasePageLocator.DIV_CONTENT_LOADING))

    def login(self):
        self.initLoginElement()
        self.text_username.send_keys(self.base_data.login_username)
        self.text_password.send_keys(self.base_data.login_password)
        self.button_submit.click()
        self.initFirstModalElement()
        #self.button_close_price_modal.click()

    def initBasicModalElement(self):
        self.modal_button_ok = self.wait.until(EC.presence_of_element_located(BasePageLocator.MODAL_BUTTON_OK))
        self.modal_button_cancel = self.modal_button_ok = self.wait.until(EC.presence_of_element_located(BasePageLocator.MODAL_BUTTON_OK))



    def initElement(self):
        # Base Elements of http://staging.the-im.com/careers
        self.title = self.wait.until(EC.presence_of_element_located(BasePageLocator.TITLE))
        self.browser.find_element(*BasePageLocator.TITLE)
        self.body = self.browser.find_element(*BasePageLocator.BODY)

    def initHeadMenuElement(self):
        self.head_link_dashboard = self.browser.find_element(*BasePageLocator.HEAD_LINK_DASHBOARD)
        self.head_link_file = self.browser.find_element(*BasePageLocator.HEAD_LINK_FILE)
        self.head_link_stock = self.browser.find_element(*BasePageLocator.HEAD_LINK_STOCK)
        self.head_link_customer = self.browser.find_element(*BasePageLocator.HEAD_LINK_CUSTOMER)
        self.head_link_pos = self.browser.find_element(*BasePageLocator.HEAD_LINK_POS)
        self.head_link_repurchase = self.browser.find_element(*BasePageLocator.HEAD_LINK_REPURCHASE)
        self.head_link_transaction = self.browser.find_element(*BasePageLocator.HEAD_LINK_DASHBOARD)
        self.head_link_report = self.browser.find_element(*BasePageLocator.HEAD_LINK_REPORT)


    def initConfirmBoxDelete(self):
        self.confirm_button_ok = self.browser.find_element(*BasePageLocator.CONFIRM_BUTTON_OK)
        self.confirm_button_cancel = self.browser.find_element(*BasePageLocator.CONFIRM_BUTTON_CANCEL)

    def initConfirmBoxEdit(self):
        self.initBasicModalElement()

    def intiFormError(self):
        self.form_error = self.wait.until(EC.presence_of_element_located(BasePageLocator.FORM_ERROR))

    def isFormErrorDisplayed(self):
        self.intiFormError()
        return self.isElementVisible(self.form_error)


    # Dim after modal
    def waitDimDisappeared(self):
        self.wait.until(EC.presence_of_element_located(BasePageLocator.DIV_DIM))
        self.wait.until(EC.invisibility_of_element_located(BasePageLocator.DIV_DIM))

    def waitDimDisappearedHalf(self):
        self.wait.until(EC.invisibility_of_element_located(BasePageLocator.DIV_DIM))

    def getNthRowTable(self, n):
        nth_row = self.browser.find_element_by_xpath('(//div[@class="fixedDataTableRowLayout_rowWrapper"])['+ str(n) +']')
        return nth_row


    def getNthRowObject(self, n, element_type):
        n += 1
        element = self.getNthRowTable(n)
        if element_type == 'edit':
            elem = element.find_element_by_xpath('.//i[@class="edit icon"]')
        elif element_type == 'delete':
            elem = element.find_element_by_xpath('.//i[@class="window close outline icon"]')
        return elem

    def selectOptionInsideModal(self, row_idx, text_value):
        # row_idx = index(row) of select box related to modal form
        # text_value = text inside select box you want to select.
        I_SELECT = (By.XPATH, '//div[@class="description"]/form/div['+ str(row_idx) +']/div/i')
        DIV_LIST_CONTAINER = (By.XPATH, '//div[@class="description"]/form/div['+ str(row_idx) +']/div/div[@role="listbox"]')
        EXPECTED_DIV_LIST = (By.XPATH, '//div[@class="description"]/form/div['+ str(row_idx) +']/div/div[@role="listbox"]/div/span[contains(text(), "'+ text_value +'")]')

        i_select = self.wait.until(EC.presence_of_element_located(I_SELECT))
        i_select.click()
        div_list_container = self.wait.until(EC.visibility_of_element_located(DIV_LIST_CONTAINER))
        expected_div_list  = self.wait.until(EC.visibility_of_element_located(EXPECTED_DIV_LIST))
        expected_div_list.click()

    def selectOptionInsideModalByIndex(self, row_idx, idx):
        # row_idx = index(row) of select box related to modal form
        # text_value = text inside select box you want to select.
        I_SELECT = (By.XPATH, '//div[@class="description"]/form/div['+ str(row_idx) +']/div/i')
        DIV_LIST_CONTAINER = (By.XPATH, '//div[@class="description"]/form/div['+ str(row_idx) +']/div/div[@role="listbox"]')
        EXPECTED_DIV_LIST = (By.XPATH, '//div[@class="description"]/form/div['+ str(row_idx) +']/div/div[@role="listbox"]/div/span['+ idx +']')

        i_select = self.wait.until(EC.presence_of_element_located(I_SELECT))
        i_select.click()
        div_list_container = self.wait.until(EC.visibility_of_element_located(DIV_LIST_CONTAINER))
        expected_div_list  = self.wait.until(EC.visibility_of_element_located(EXPECTED_DIV_LIST))
        expected_div_list.click()

    def getTableNthRowValue(self, n_row, n_col):
        n_row += 1 # offset table head
        n_col += 1 # offset table edit/delete column
        element = self.getNthRowTable(n_row)
        elem = element.find_element_by_xpath('.//div[@class="fixedDataTableCellLayout_main public_fixedDataTableCell_main"]['+ str(n_col+1) +']/div/div/div/div/div/div')
        return elem.get_attribute('innerHTML')

    '''
    newer function to search value in table by specifying search column
    '''
    def searchText2(self, n_col, search_text):
        rows = self.browser.find_elements_by_xpath('(//div[@class="fixedDataTableRowLayout_rowWrapper"])')
        i = 1
        #print("search for : "+ search_text)

        for row in rows:
            if i > 1:  #skip 1 which is table head
                elem = row.find_element_by_xpath('.//div[@class="fixedDataTableCellLayout_main public_fixedDataTableCell_main"]['+ str(n_col+1) +']/div/div/div/div/div/div')
                if elem.get_attribute('innerHTML') == search_text:
                    return i - 1
            i += 1
        return 0















    def navigateDashboard(self):
        self.initHeadMenuElement()
        self.head_link_dashboard.click()

    def navigateFile(self):
        self.initHeadMenuElement()
        self.head_link_file.click()

    def navigateStock(self):
        self.initHeadMenuElement()
        self.head_link_stock.click()

    def navigateCustomer(self):
        self.initHeadMenuElement()
        self.head_link_stock.click()

    def navigatePos(self):
        self.initHeadMenuElement()
        self.head_link_pos.click()

    def navigateRepurchase(self):
        self.initHeadMenuElement()
        self.head_link_repurchase.click()

    def navigateTransaction(self):
        self.initHeadMenuElement()
        self.head_link_transaction.click()

    def navigateReport(self):
        self.initHeadMenuElement()
        self.head_link_report.click()











    def checkBrokenLink(self):
        self.links = self.browser.find_elements_by_tag_name("a")

        for link in links:
            if link.is_displayed():
                url_open = self.site_base_url + link.get_attribute("href")
                content = urllib.urlopen(url_open)
                self.assertEqual(content.getcode(), 200)


    def isTitleContain(self, text):
        title = self.wait.until(EC.presence_of_element_located(BasePageLocator.TITLE))
        title_text = title.get_attribute('text')
        if title_text.find(text) != -1:
            return True
        else:
            return False

    # load page by URL
    def loadPageByUrl(self, url):
        self.browser.get(url)


    def checkBrokenImage(self):
        imgs = self.browser.find_elements_by_tag_name("img")
        for imgs in img:
            if img.is_displayed():
                url_open = self.site_base_url + link.get_attribute("src")
                content = urllib.urlopen(url_open)
                self.assertEqual(content.getcode(), 200)

    def scrollToElement(self, find_by, element_locator_string):
        if(find_by == 'id'):
            by = By.ID
        elif(find_by == 'xpath'):
            by = By.XPATH
        elif(find_by == 'tagname'):
            by = By.TAG_NAME
        the_element = WebDriverWait(self.browser, 10).until(EC.visibility_of_element_located((by, element_locator_string)))
        self.browser.execute_script("arguments[0].scrollIntoView();", the_element)

    def navigateLink(self, url):
        link_element = WebDriverWait(self.browser, 10).until(EC.presence_of_element_located((By.XPATH, "//a[@href='" + url + "']")))
        link_element.click()

    def isJavascriptErrorOnDomReady(self):
        '''
        at page load
        if javascript load, we have to make one element present when DOM ready and wait until presence of the element, then check javascript error
        <domready />

        '''
        element = WebDriverWait(self.browser, 10).until(
            EC.presence_of_element_located((By.TAG_NAME, "domready"))
        )
        return self.isJavascriptError(self)

    def isJavascriptError(self):
        '''
        return true if detect javascript error on body tag raised from windows.onerror event
        <script type="text/javascript">
            window.onerror=function(msg){
                $("body").attr("error_message",msg);
            }
        </script>
        '''
        error_message = self.body.get_attribute("error_message")
        if error_message.length > 0:
            return error_message
        else:
            return False

    def fillTextBoxById(self, textbox_id, value):
        textbox = self.browser.find_element_by_id(textbox_id)
        textbox.send_keys(value)

    def fillTextBoxByName(self, textbox_name, value):
        textbox = self.browser.find_element_by_name(textbox_name)
        textbox.send_keys(value)


    def cleanUpForm(self):
        a = 10

    def getErrorLabelByFormId(self, form_id):
        error_label = self.browser.find_element_by_xpath("//label[@for='"+ form_id +"']")
        return error_label
    '''
    def isFormErrorDisplayed(self, form_id):
        label = self.getErrorLabelByFormId(form_id)
        return label.is_displayed()
    '''
    def isElementHidden(self, element):
        try:
            if element.is_displayed():
                return False
            else:
                return True
        except:
            return True

    def isElementVisible(self, element):
        try:
            if element.is_displayed():
                return True
            else:
                return False
        except:
            return False


    def getElementStyle(self, element):
        location = element.location
        size = element.size
        color = element.value_of_css_property('color')
        background_color = element.value_of_css_property('background-color')

        ret = {"width": size["width"],
                "height": size["height"],
                "x": location["x"],
                "y": location["y"],
                "color": self.rgbConvert(color),
                "background_color": self.rgbConvert(background_color)
                }
        return ret


    def isVerticalAlign(self, element1, element2):
        element1_style = self.getElementStyle(element1)
        element2_style = self.getElementStyle(element2)
        if element1_style["y"] + element1_style["height"] <= element2_style["y"] or element2_style["y"] + element2_style["height"] <= element1_style["y"]:
            return True
        else:
            return False

    def isHorizontalAlign(self, element1, element2):
        element1_style = self.getElementStyle(element1)
        element2_style = self.getElementStyle(element2)
        if element1_style["x"] + element1_style["width"] <= element2_style["x"] or element2_style["x"] + element2_style["width"] <= element1_style["x"]:
            return True
        else:
            return False


    def rgbConvert(self, rgb):
        r, g, b, alpha = ast.literal_eval(rgb.strip("rgba"))
        hex_value = '#%02x%02x%02x' % (r, g, b)
        return hex_value


    def initTable(self):
        self.table_cells = self.browser.find_elements(*BasePageLocator.TABLE_CELLS)


    def searchText(self, column_search, column_all, text):
        self.initTable()
        col_count = 1
        row_count = 1
        #print(text)
        #print('-----')
        for cell in self.table_cells:
            #print cell on interested column
            if col_count == column_search:
                #print(cell.text)
                if cell.text == text:
                    return row_count
            if col_count == column_all:
                row_count += 1
                col_count = 1
            else:
                col_count += 1

        return 0



'''
    def isFormErrorExistByName(self, form_name):
        error_span = self.browser.find_element_by_name(form_name)


    def isFormErrorExistByXpath(self, form_xpath):
        error_span = self.browser.find_element_by_xpath(form_xpath)

    def getNextButton(self, pageNo):
        #There are multiple next buttons, choose visible one to work with
        all_next_button = self.browser.find_elements_by_xpath("//input[@name='next']")
        for next_button in all_next_button:
            if next_button.is_displayed():
                return next_button

    def getPreviousButton(self):
        #There are multiple previous buttons, choose visible one to work with
        all_previous_button = self.browser.find_elements_by_xpath("//input[@name='previous']")
        for previous_button in all_previous_button:
            if previous_button.is_displayed():
                return previous_button

    def chooseRadioByValue(self, radio_name, value):
'''


class BasePageLocator(object):

    TITLE = (By.TAG_NAME, "title")
    BODY = (By.TAG_NAME, "body")

    HEAD_LINK_DASHBOARD = (By.XPATH, '//div[@class="ui pointing secondary menu"]/a[1]')
    HEAD_LINK_FILE = (By.XPATH, '//div[@class="ui pointing secondary menu"]/a[2]')
    HEAD_LINK_STOCK = (By.XPATH, '//div[@class="ui pointing secondary menu"]/a[3]')
    HEAD_LINK_CUSTOMER = (By.XPATH, '//div[@class="ui pointing secondary menu"]/a[4]')
    HEAD_LINK_POS = (By.XPATH, '//div[@class="ui pointing secondary menu"]/a[5]')
    HEAD_LINK_REPURCHASE = (By.XPATH, '//div[@class="ui pointing secondary menu"]/a[6]')
    HEAD_LINK_TRANSACTION = (By.XPATH, '//div[@class="ui pointing secondary menu"]/a[7]')
    HEAD_LINK_REPORT = (By.XPATH, '//div[@class="ui pointing secondary menu"]/a[8]')

    TEXT_USERNAME = (By.XPATH, '//*[@id="root"]/div[1]/div[1]/div[1]/form[1]/div[2]/div[1]/input[1]')
    TEXT_PASSWORD = (By.XPATH, '//*[@id="root"]/div[1]/div[1]/div[1]/form[1]/div[3]/div[1]/input[1]')
    BUTTON_SUBMIT = (By.XPATH, '//*[@id="root"]/div[1]/div[1]/div[1]/form[1]/center[1]/button[1]')
    BUTTON_CLOSE_PRICE_MODAL = (By.XPATH, '//button[contains(text(), "ปิด")]')
    DIV_LOADING = (By.XPATH, '//div[@class="ui inverted dimmer"]')
    DIV_CONTENT_LOADING = (By.XPATH, '//*[@id="content-body"]/div/div[1]/div/div/div')
    DIV_DIM = (By.XPATH, '//body/div[2]')
    MODAL_BUTTON_OK = (By.XPATH, '//div[@class="actions"]/button[contains(text(), "บันทึก")]')
    MODAL_BUTTON_CANCEL = (By.XPATH, '//div[@class="actions"]/button[contains(text(), "ยกเลิก")]')

    CONFIRM_BUTTON_OK = (By.XPATH, '//div[@class="actions"]/button[contains(text(), "OK")]')
    CONFIRM_BUTTON_CANCEL = (By.XPATH, '//div[@class="actions"]/button[contains(text(), "Cancel")]')

    TABLE_CELLS = (By.XPATH, '//div[@class="fixedDataTableCellLayout_main public_fixedDataTableCell_main"]/div/div/div/div/div/div')
    FORM_ERROR = (By.XPATH, '//div[@class="error field"]')

class BaseCounter:
    int_counter = 1
