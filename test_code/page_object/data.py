# -*- coding: utf-8 -*-

import time
import datetime

class BaseData(object):
    title_prefix = "AAGold"
    login_username = 'admin'
    login_password = '***aag***'
    #login_username = 'aag'
    #login_password = '112233'


class FileData(object):
    select_position = {"valid":"Other", "invalid":"0", "empty":""}

class FileBranchData(object):
    millis = round(time.time())
    my_datetime = datetime.datetime.fromtimestamp(millis).strftime('%d%H%M%S')

    branch_name = {"valid":"ราม 02".decode("utf-8", "replace") + my_datetime, "valid2": "ราม 02 ทดสอบ".decode("utf-8", "replace"), "invalid": "ช่ื่อทดสอบช่ื่อทดสอบช่ื่อทดสอบช่ื่อทดสอบช่ื่อทดสอบช่ื่อทดสอบช่ื่อทดสอบช่ื่อทดสอบช่ื่อทดสอบช่ื่อทดสอบ123".decode("utf-8", "replace"), "empty":""}
    note = {"valid":"ทดสอบ ….".decode("utf-8", "replace"), "invalid": "", "empty":""}
    branch_code = {"valid":"x" + my_datetime, "valid2":"a" + my_datetime, "invalid": "001", "invalid2": "xxxxyyyy123", "empty":""}

class FileStaffData(object):
    millis = round(time.time())
    my_datetime = datetime.datetime.fromtimestamp(millis).strftime('%d%H%M%S')

    branch_name = {"valid" : "สำนักงานใหญ่".decode("utf-8", "replace")}
    staff_name = {"valid" : my_datetime + "ขยัน ทำกิน".decode("utf-8", "replace"), "valid2": "ขยันแก้ไข ทำกินแก้ไข".decode("utf-8", "replace") , "invalid" : "as xcmdnvsdasdfsdvn mxdvhdsi fhwklascnczxbvjsxvbkszjcvsodfychadsoflkjasdk skdjfhasjkfdnafas sdfjkadshfnkajsdhfkajdsfnkjadsrynfshu acjksahfcsajkdc hfwecg zxjkashdcjkas asdfasdjashc sahdbnaskj fadbjfkcahdsijfkawhe32g qwebdiautr qwgDBJKASGDJAKShdfjasdnakvasdfjdskasfaewsrewqr"}
    staff_address = {"valid" : "ที่อยู่ yyyy".decode("utf-8", "replace"), "valid2": my_datetime + "ที่อยู่ yyyy แก้ไข".decode("utf-8", "replace"), "valid3": ""}
    staff_phone = {"valid" : "092-222-yyyy", "valid2":"092-xxx-6666", "valid3": ""}
    staff_code = {"valid" : "t"+my_datetime, "invalid": "t00144441111", "invalid2" : "c001"}

class FileUserData(object):
    millis = round(time.time())
    my_datetime = datetime.datetime.fromtimestamp(millis).strftime('%d%H%M%S')

    branch_name = {"valid" : "สำนักงานใหญ่".decode("utf-8", "replace"), "valid2" : "สาขาที่ สอง".decode("utf-8", "replace")}
    user_name = {"valid" : my_datetime + "ad0073".decode("utf-8", "replace"), 'valid2': my_datetime + "ed0071", "invalid" : "admin"}
    password = {"valid" : "12341234", "invalid":"abcd1234", "invalid2": "123"}
    confirm_password = {"valid" : "12341234", "invalid":"abcd1234", "invalid2": "123"}
    user_level = {"valid" : "Admin", "valid2": "User"}
    staff_name = {"valid" : "เอเอ".decode("utf-8", "replace"), "valid2" : "เอเอ".decode("utf-8", "replace"), "invalid": ""}

class FileProductGroupData(object):
    millis = round(time.time())
    my_datetime = datetime.datetime.fromtimestamp(millis).strftime('%d%H%M%S')

    text_code = {"valid" : "b01", "valid2" : "b02", "invalid" : ""}
    text_name = {"valid" : "test1","valid2" : "test2", "invalid" : ""}
    text_weight = {"valid" : "1.52", "invalid" : ""}
    text_multiplier_buy = {"valid" : "0.95", "invalid" : ""}
    text_multiplier_sell = {"valid" : "0.95", "invalid" : ""}
    text_buy_deduct = {"valid" : "-200", "invalid" : ""}
    text_note = {"valid" : "ข้อความทดสอบ ….".decode("utf-8", "replace"), "invalid" : ""}






#============= Data ====================
class ErrorMessage(object):
    textbox_accept_numeric_only = "กรุณาป้อนข้อมูลเฉพาะตัวเลขเท่านั้น".decode("utf-8", "replace")
    #textbox_not_accept_character = "กรุณาเว้นการกรอกข้อมูลตัวอักษร"
    textbox_not_accept_numeric = "กรุณาเว้นการกรอกข้อมูลตัวเลข".decode("utf-8", "replace")
    textbox_not_accept_special_char = "กรุณายกเว้นการป้อนข้อมูลที่เป็นอักขระพิเศษ".decode("utf-8", "replace")
    textbox_not_accept_foreign_char = "กรุณาป้อนข้อมูลเป็นภาษาอังกฤษเท่านั้น".decode("utf-8", "replace")
    textbox_not_accept_space_bar = "กรุณายกเว้นการป้อนเครื่องหมายเว้นวรรค (Space bar)".decode("utf-8", "replace")
    textbox_max_val_error = "กรุณาป้อนข้อมูลตัวเลขที่มีค่าน้อยกว่า".decode("utf-8", "replace")
    textbox_min_val_error = "กรุณาป้อนข้อมูลตัวเลขที่มีค่ามากกว่า".decode("utf-8", "replace")
    textbox_max_length_error = "กรุณาป้อนข้อมูลที่มีความยาวไม่เกิน".decode("utf-8", "replace")
    textbox_min_length_error = "กรุณาป้อนข้อมูลที่มีความยาวอย่างน้อย".decode("utf-8", "replace")
    textbox_not_accept_decimal_point = "กรุณาเว้นการกรอก ทศนิยม".decode("utf-8", "replace")
    textbox_empty_error = "ต้องไม่เป็นค่าว่าง".decode("utf-8", "replace")
    textbox_duplicate_error = "ถูกใช้งานแล้ว".decode("utf-8", "replace")

class TestErrorDescription(object):
    temp_method_error_message = ""
    expect_error_message_not_found = " Expected error message not found "
    select_box_cannot_select = "Select box cannot select"
    insert_data_not_found_in_table = " Insertion Fail "


class BranchData(object):
    branch_code = {"valid1" : "001", "invalid1": "A01"}

class StaffData(object):
    select_data = "สำนักงานใหญ่".decode("utf-8", "replace")
    filter_data = "สำนัก".decode("utf-8", "replace")
