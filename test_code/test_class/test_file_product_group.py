# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import unittest

# Create your tests here.
#from selenium.common.exceptions import *

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import *
from selenium.webdriver.chrome.options import Options
import time

from test_class.basetest import BaseTest
from page_object.file.file_product_group import FileProductGroup


class TestFileProductGroup(BaseTest):

    page_file_product_group = None #Page Object

    browser = None
    temp_row = []
    #temp_data = []
    temp_duplicate = None



    def setUp(self): #test class initialize
        super(TestFileProductGroup, self).setUp()
        #page1 setup
        self.page_file_product_group = FileProductGroup(self.browser)
        self.page_file_product_group.loadPage()
        self.page_file_product_group.login()
    '''
    # ========================= TEST CASE AG019 - FILE ADD SUCCESS ================================
    def test_ag019_001_data_file_product_group_add_success(self):
        self.page_file_product_group.navigateFileProductGroup()
        self.page_file_product_group.initPageElement()
        self.page_file_product_group.button_add.click()

        self.page_file_product_group.initFormElement()
        self.page_file_product_group.initBasicModalElement()
        #self.page_file_product_group.clearForm()

        code = self.page_file_product_group.data.text_code["valid"]
        self.page_file_product_group.text_code.send_keys(code)
        self.page_file_product_group.text_name.send_keys(self.page_file_product_group.data.text_name["valid"])

        self.page_file_product_group.modal_button_ok.click()
        time.sleep(self.short_wait)
        #self.page_file_product_group.waitDimDisappeared()

        search_result = self.page_file_product_group.searchText2(1, code)
        self.assertTrue(search_result > 0, "Could not find added data in Staff table")

        self.temp_row.append(search_result)
        #self.temp_data.append(code)

        time.sleep(self.sleep)

    def test_ag019_002_data_file_product_group_add_success(self):
        self.page_file_product_group.navigateFileProductGroup()
        self.page_file_product_group.initPageElement()
        self.page_file_product_group.button_add.click()

        self.page_file_product_group.initFormElement()
        self.page_file_product_group.initBasicModalElement()
        #self.page_file_product_group.clearForm()

        code = self.page_file_product_group.data.text_code["valid2"]
        self.page_file_product_group.text_code.send_keys(code)
        self.page_file_product_group.text_name.send_keys(self.page_file_product_group.data.text_name["valid2"])
        self.page_file_product_group.text_weight.send_keys(self.page_file_product_group.data.text_weight["valid"])
        self.page_file_product_group.text_multiplier_buy.send_keys(self.page_file_product_group.data.text_multiplier_buy["valid"])
        self.page_file_product_group.text_multiplier_sell.send_keys(self.page_file_product_group.data.text_multiplier_sell["valid"])
        self.page_file_product_group.text_buy_deduct.send_keys(self.page_file_product_group.data.text_buy_deduct["valid"])
        self.page_file_product_group.text_note.send_keys(self.page_file_product_group.data.text_note["valid"])

        self.page_file_product_group.modal_button_ok.click()
        time.sleep(self.short_wait)
        #self.page_file_product_group.waitDimDisappeared()

        search_result = self.page_file_product_group.searchText2(1, code)
        self.assertTrue(search_result > 0, "Could not find added data in Staff table")

        self.temp_row.append(search_result)
        #self.temp_data.append(code)

        time.sleep(self.sleep)

    '''

    # ========================= TEST CASE AG013 - FILE ADD FAIL ================================
    def test_ag020_001_data_file_product_group_add_fail(self):
        self.page_file_product_group.navigateFileProductGroup()
        self.page_file_product_group.initPageElement()
        self.page_file_product_group.button_add.click()

        self.page_file_product_group.initFormElement()
        self.page_file_product_group.initBasicModalElement()
        #self.page_file_product_group.clearForm()
        user_name = self.page_file_product_group.data.user_name["valid"]
        #self.page_file_product_group.text_code.send_keys(self.page_file_product_group.data.text_code["valid"])
        self.page_file_product_group.text_name.send_keys(self.page_file_product_group.data.text_name["valid"])
        self.page_file_product_group.text_weight.send_keys(self.page_file_product_group.data.text_weight["valid"])
        self.page_file_product_group.text_multiplier_buy.send_keys(self.page_file_product_group.data.text_multiplier_buy["valid"])
        self.page_file_product_group.text_multiplier_sell.send_keys(self.page_file_product_group.data.text_multiplier_sell["valid"])
        self.page_file_product_group.text_buy_deduct.send_keys(self.page_file_product_group.data.text_buy_deduct["valid"])
        self.page_file_product_group.text_note.send_keys(self.page_file_product_group.data.text_note["valid"])

        self.page_file_product_group.modal_button_ok.click()
        self.assertTrue(self.page_file_product_group.isFormErrorDisplayed(), "Form error should be displayed")

        time.sleep(self.sleep)

    '''
    def test_ag013_002_data_file_product_group_add_fail(self):
        self.page_file_product_group.navigateFileProductGroup()
        self.page_file_product_group.initPageElement()
        self.page_file_product_group.button_add.click()

        self.page_file_product_group.initFormElement()
        self.page_file_product_group.initBasicModalElement()
        #self.page_file_product_group.clearForm()
        user_name = self.page_file_product_group.data.user_name["valid"]
        #self.page_file_product_group.text_user_name.send_keys(user_name)
        self.page_file_product_group.text_user_password.send_keys(self.page_file_product_group.data.password["valid"])
        self.page_file_product_group.text_user_confirm_password.send_keys(self.page_file_product_group.data.confirm_password["valid"])
        self.page_file_product_group.selectOptionInsideModal(1, self.page_file_product_group.data.branch_name["valid"])
        self.page_file_product_group.selectOptionInsideModal(5, self.page_file_product_group.data.user_level["valid"])
        self.page_file_product_group.modal_button_ok.click()
        self.assertTrue(self.page_file_product_group.isFormErrorDisplayed(), "Form error should be displayed")

        time.sleep(self.sleep)

    def test_ag013_003_data_file_product_group_add_fail(self):
        self.page_file_product_group.navigateFileProductGroup()
        self.page_file_product_group.initPageElement()
        self.page_file_product_group.button_add.click()

        self.page_file_product_group.initFormElement()
        self.page_file_product_group.initBasicModalElement()
        self.page_file_product_group.clearForm()

        self.page_file_product_group.text_user_password.send_keys(self.page_file_product_group.data.password["valid"])
        self.page_file_product_group.text_user_confirm_password.send_keys(self.page_file_product_group.data.confirm_password["valid"])
        self.page_file_product_group.selectOptionInsideModal(1, self.page_file_product_group.data.branch_name["valid"])
        self.page_file_product_group.selectOptionInsideModal(5, self.page_file_product_group.data.user_level["valid"])
        user_name = self.page_file_product_group.data.user_name["invalid"]
        self.page_file_product_group.text_user_name.send_keys(user_name)
        self.page_file_product_group.modal_button_ok.click()
        self.assertTrue(self.page_file_product_group.isFormErrorDisplayed(), "Form error should be displayed")

        time.sleep(self.sleep)



    def test_ag013_004_data_file_product_group_add_fail(self):
        self.page_file_product_group.navigateFileProductGroup()
        self.page_file_product_group.initPageElement()
        self.page_file_product_group.button_add.click()

        self.page_file_product_group.initFormElement()
        self.page_file_product_group.initBasicModalElement()
        #self.page_file_product_group.clearForm()
        user_name = self.page_file_product_group.data.user_name["valid"]
        self.page_file_product_group.text_user_name.send_keys(user_name)
        #self.page_file_product_group.text_user_password.send_keys(self.page_file_product_group.data.password["valid"])
        self.page_file_product_group.text_user_confirm_password.send_keys(self.page_file_product_group.data.confirm_password["valid"])
        self.page_file_product_group.selectOptionInsideModal(1, self.page_file_product_group.data.branch_name["valid"])
        self.page_file_product_group.selectOptionInsideModal(5, self.page_file_product_group.data.user_level["valid"])
        self.page_file_product_group.modal_button_ok.click()
        self.assertTrue(self.page_file_product_group.isFormErrorDisplayed(), "Form error should be displayed")

        time.sleep(self.sleep)




    def test_ag013_005_data_file_product_group_add_fail(self):
        self.page_file_product_group.navigateFileProductGroup()
        self.page_file_product_group.initPageElement()
        self.page_file_product_group.button_add.click()

        self.page_file_product_group.initFormElement()
        self.page_file_product_group.initBasicModalElement()
        #self.page_file_product_group.clearForm()
        user_name = self.page_file_product_group.data.user_name["valid"]
        self.page_file_product_group.text_user_name.send_keys(user_name)
        self.page_file_product_group.text_user_password.send_keys(self.page_file_product_group.data.password["invalid2"])
        self.page_file_product_group.text_user_confirm_password.send_keys(self.page_file_product_group.data.confirm_password["invalid2"])
        self.page_file_product_group.selectOptionInsideModal(1, self.page_file_product_group.data.branch_name["valid"])
        self.page_file_product_group.selectOptionInsideModal(5, self.page_file_product_group.data.user_level["valid"])
        self.page_file_product_group.modal_button_ok.click()
        self.assertTrue(self.page_file_product_group.isFormErrorDisplayed(), "Form error should be displayed")

        time.sleep(self.sleep)



    def test_ag013_006_data_file_product_group_add_fail(self):
        self.page_file_product_group.navigateFileProductGroup()
        self.page_file_product_group.initPageElement()
        self.page_file_product_group.button_add.click()

        self.page_file_product_group.initFormElement()
        self.page_file_product_group.initBasicModalElement()
        #self.page_file_product_group.clearForm()
        user_name = self.page_file_product_group.data.user_name["valid"]
        self.page_file_product_group.text_user_name.send_keys(user_name)
        self.page_file_product_group.text_user_password.send_keys(self.page_file_product_group.data.password["valid"])
        self.page_file_product_group.text_user_confirm_password.send_keys(self.page_file_product_group.data.confirm_password["invalid"])
        self.page_file_product_group.selectOptionInsideModal(1, self.page_file_product_group.data.branch_name["valid"])
        self.page_file_product_group.selectOptionInsideModal(5, self.page_file_product_group.data.user_level["valid"])
        self.page_file_product_group.modal_button_ok.click()
        self.assertTrue(self.page_file_product_group.isFormErrorDisplayed(), "Form error should be displayed")
        time.sleep(self.sleep)




    def test_ag013_007_data_file_product_group_add_fail(self):
        self.page_file_product_group.navigateFileProductGroup()
        self.page_file_product_group.initPageElement()
        self.page_file_product_group.button_add.click()

        self.page_file_product_group.initFormElement()
        self.page_file_product_group.initBasicModalElement()
        #self.page_file_product_group.clearForm()

        user_name = self.page_file_product_group.data.user_name["valid"]
        self.page_file_product_group.text_user_name.send_keys(user_name)
        self.page_file_product_group.text_user_password.send_keys(self.page_file_product_group.data.password["valid"])
        self.page_file_product_group.text_user_confirm_password.send_keys(self.page_file_product_group.data.confirm_password["valid"])
        self.page_file_product_group.text_branch.send_keys(self.page_file_product_group.data.branch_name["valid"])
        #self.page_file_product_group.selectOptionInsideModal(1, self.page_file_product_group.data.branch_name["valid"])
        #self.page_file_product_group.selectOptionInsideModal(5, self.page_file_product_group.data.user_level["valid"])
        self.page_file_product_group.modal_button_ok.click()
        self.assertTrue(self.page_file_product_group.isFormErrorDisplayed(), "Form error should be displayed")

        time.sleep(self.sleep)



    # ========================= TEST CASE AG014 - FILE EDIT SUCCESS ================================
    def test_ag014_001_data_file_product_group_edit_success(self):
        self.page_file_product_group.navigateFileProductGroup()
        self.page_file_product_group.initPageElement()
        if not self.temp_row:
            row1 = 2
        else:
            row1 = self.temp_row[0]
        edit_button = self.page_file_product_group.getNthRowObject(row1, 'edit')
        edit_button.click()
        self.page_file_product_group.initFormElement()
        edit_user_branch_data = self.page_file_product_group.data.branch_name["valid2"]
        self.page_file_product_group.text_branch.clear()
        self.page_file_product_group.text_branch.send_keys(edit_user_branch_data)
        self.page_file_product_group.initConfirmBoxEdit()
        self.page_file_product_group.modal_button_ok.click()
        #self.page_file_product_group.waitDimDisappeared()
        time.sleep(self.short_wait)
        search_result = self.page_file_product_group.searchText2(2, edit_user_branch_data)
        self.assertTrue(search_result > 0, "Could not find added data in Branch table")
        time.sleep(self.sleep)


    def test_ag014_002_data_file_product_group_edit_success(self):
        self.page_file_product_group.navigateFileProductGroup()
        self.page_file_product_group.initPageElement()
        if not self.temp_row:
            row1 = 3
        else:
            row1 = self.temp_row[0]
        edit_button = self.page_file_product_group.getNthRowObject(row1, 'edit')
        edit_button.click()
        self.page_file_product_group.initFormElement()
        edit_user_name_data = self.page_file_product_group.data.user_name["valid2"]
        self.page_file_product_group.text_user_name.clear()
        self.page_file_product_group.text_user_name.send_keys(edit_user_name_data)
        self.page_file_product_group.initConfirmBoxEdit()
        self.page_file_product_group.modal_button_ok.click()
        #self.page_file_product_group.waitDimDisappeared()
        time.sleep(self.short_wait)
        search_result = self.page_file_product_group.searchText2(3, edit_user_name_data)
        self.assertTrue(search_result > 0, "Could not find added data in Branch table")
        time.sleep(self.sleep)



    def test_ag014_003_data_file_product_group_edit_success(self):
        self.page_file_product_group.navigateFileProductGroup()
        self.page_file_product_group.initPageElement()
        if not self.temp_row:
            row1 = 3
        else:
            row1 = self.temp_row[0]
        edit_button = self.page_file_product_group.getNthRowObject(row1, 'edit')
        edit_button.click()
        self.page_file_product_group.initFormElement()
        edit_user_password_data = self.page_file_product_group.data.user_name["valid2"]
        self.page_file_product_group.text_user_name.clear()
        self.page_file_product_group.text_user_name.send_keys(edit_user_password_data)
        self.page_file_product_group.initConfirmBoxEdit()
        self.page_file_product_group.modal_button_ok.click()
        #self.page_file_product_group.waitDimDisappeared()
        time.sleep(self.short_wait)
        #search_result = self.page_file_product_group.searchText(3, 5, edit_user_name_data)
        #self.assertTrue(search_result > 0, "Could not find added data in Branch table")
        # @todo ==== logout and login again
        time.sleep(self.sleep)


    def test_ag014_004_data_file_product_group_edit_success(self):
        self.page_file_product_group.navigateFileProductGroup()
        self.page_file_product_group.initPageElement()
        if not self.temp_row:
            row1 = 4
        else:
            row1 = self.temp_row[0]
        edit_button = self.page_file_product_group.getNthRowObject(row1, 'edit')
        edit_button.click()
        self.page_file_product_group.initFormElement()
        edit_user_level_data = self.page_file_product_group.data.user_level["valid2"]
        self.page_file_product_group.text_level.clear()
        self.page_file_product_group.text_level.send_keys(edit_user_level_data)
        self.page_file_product_group.initConfirmBoxEdit()
        self.page_file_product_group.modal_button_ok.click()
        #self.page_file_product_group.waitDimDisappeared()
        time.sleep(self.short_wait)
        self.page_file_product_group.getTableNthRowValue(row1, 3)
        search_result = self.page_file_product_group.searchText2(5, edit_user_level_data)
        self.assertTrue(search_result > 0, "Could not find added data in Branch table")
        time.sleep(self.sleep)



    def test_ag014_005_data_file_product_group_edit_success(self):
        self.page_file_product_group.navigateFileProductGroup()
        self.page_file_product_group.initPageElement()
        if not self.temp_row:
            row1 = 2
        else:
            row1 = self.temp_row[0]
        edit_button = self.page_file_product_group.getNthRowObject(row1, 'edit')
        edit_button.click()
        self.page_file_product_group.initFormElement()
        edit_staff_name_data = self.page_file_product_group.data.staff_name["valid2"]
        self.page_file_product_group.text_staff.clear()
        self.page_file_product_group.text_staff.send_keys(edit_staff_name_data)
        self.page_file_product_group.initConfirmBoxEdit()
        self.page_file_product_group.modal_button_ok.click()
        #self.page_file_product_group.waitDimDisappeared()
        time.sleep(self.short_wait)
        search_result = self.page_file_product_group.searchText2(4, edit_staff_name_data)
        self.assertTrue(search_result > 0, "Could not find added data in Branch table")
        time.sleep(self.sleep)



    # ========================= TEST CASE AG015 - FILE EDIT SUCCESS ================================

    def test_ag015_001_data_file_product_group_edit_fail(self):
        # @todo - can not clear text field of name
        self.page_file_product_group.navigateFileProductGroup()
        self.page_file_product_group.initPageElement()
        if not self.temp_row:
            row1 = 3
        else:
            row1 = self.temp_row[0]
        edit_button = self.page_file_product_group.getNthRowObject(row1, 'edit')
        edit_button.click()
        self.page_file_product_group.initFormElement()
        self.page_file_product_group.text_user_name.clear()
        #self.page_file_product_group.text_user_name.send_keys(Keys.BACKSPACE)

        self.page_file_product_group.initConfirmBoxEdit()
        self.page_file_product_group.text_user_name.clear()
        self.page_file_product_group.modal_button_ok.click()

        #self.page_file_product_group.waitDimDisappeared()
        self.assertTrue(self.page_file_product_group.isFormErrorDisplayed(), "Form error should be displayed")
        time.sleep(self.sleep)



    def test_ag015_002_data_file_product_group_edit_fail(self):
        # @todo - can not clear text field of name
        self.page_file_product_group.navigateFileProductGroup()
        self.page_file_product_group.initPageElement()
        if not self.temp_row:
            row1 = 3
        else:
            row1 = self.temp_row[0]
        edit_button = self.page_file_product_group.getNthRowObject(row1, 'edit')
        edit_button.click()
        self.page_file_product_group.initFormElement()
        self.page_file_product_group.text_user_name.clear()
        self.page_file_product_group.text_user_name.send_keys(self.page_file_product_group.data.user_name["invalid"])
        self.page_file_product_group.initConfirmBoxEdit()
        self.page_file_product_group.modal_button_ok.click()
        time.sleep(self.short_wait)
        #self.page_file_product_group.waitDimDisappeared()
        self.assertTrue(self.page_file_product_group.isFormErrorDisplayed(), "Form error should be displayed")
        time.sleep(self.sleep)



    def test_ag015_003_data_file_product_group_edit_fail(self):
        # @todo - can not clear text field of name
        self.page_file_product_group.navigateFileProductGroup()
        self.page_file_product_group.initPageElement()
        if not self.temp_row:
            row1 = 3
        else:
            row1 = self.temp_row[0]
        edit_button = self.page_file_product_group.getNthRowObject(row1, 'edit')
        edit_button.click()
        self.page_file_product_group.initFormElement()
        self.page_file_product_group.text_user_password.clear()
        self.page_file_product_group.text_user_confirm_password.clear()
        self.page_file_product_group.text_user_password.send_keys(self.page_file_product_group.data.password["valid"])
        self.page_file_product_group.text_user_confirm_password.send_keys(self.page_file_product_group.data.password["invalid"])
        self.page_file_product_group.initConfirmBoxEdit()
        self.page_file_product_group.modal_button_ok.click()

        #self.page_file_product_group.waitDimDisappeared()
        self.assertTrue(self.page_file_product_group.isFormErrorDisplayed(), "Form error should be displayed")
        time.sleep(self.sleep)






    # ========================= TEST CASE AG016 ========================
    def test_ag022_001_data_file_product_group_delete_success(self):
        self.page_file_product_group.navigateFileProductGroup()
        self.page_file_product_group.initPageElement()
        data_user_code = self.temp_data[0]
        search_result = self.page_file_product_group.searchText2(3, data_user_code)

        self.assertTrue(search_result > 0, "removed data still appear in Branch table")
        row1 = search_result
        delete_button = self.page_file_product_group.getNthRowObject(row1, 'delete')
        delete_button.click()
        self.page_file_product_group.initConfirmBoxDelete()
        self.page_file_product_group.confirm_button_ok.click()
        time.sleep(self.short_wait)
        search_result = self.page_file_product_group.searchText2(3, data_user_code)

        self.assertTrue(search_result == 0, "removed data still appear in Branch table")
        time.sleep(self.sleep)
    '''
