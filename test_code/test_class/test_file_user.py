# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import unittest

# Create your tests here.
#from selenium.common.exceptions import *

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import *
from selenium.webdriver.chrome.options import Options
import time

from test_class.basetest import BaseTest
from page_object.file.file_user import FileUser


class TestFileUser(BaseTest):

    page_file_user = None #Page Object

    browser = None
    temp_row = []
    temp_data = []
    temp_duplicate = None


    def setUp(self): #test class initialize
        super(TestFileUser, self).setUp()
        #page1 setup
        self.page_file_user = FileUser(self.browser)
        self.page_file_user.loadPage()
        self.page_file_user.login()

    '''
    @classmethod
    def setUpClass(self):
        self.browser = webdriver.Firefox()
        self.ajax_wait = 10
        self.page_file_user = FileUser(self.browser)
        self.page_file_user.loadPage()
        self.page_file_user.login()

    @classmethod
    def tearDownClass(self):
        time.sleep(1)
        self.browser.close()
    '''

    # ========================= TEST CASE AG012 - FILE ADD SUCCESS ================================
    def test_ag012_001_data_file_user_add_success(self):
        self.page_file_user.navigateFileUser()
        self.page_file_user.initPageElement()
        self.page_file_user.button_add.click()

        self.page_file_user.initFormElement()
        self.page_file_user.initBasicModalElement()
        #self.page_file_user.clearForm()
        user_name = self.page_file_user.data.user_name["valid"]
        self.page_file_user.text_user_name.send_keys(user_name)
        self.page_file_user.text_user_password.send_keys(self.page_file_user.data.password["valid"])
        self.page_file_user.text_user_confirm_password.send_keys(self.page_file_user.data.confirm_password["valid"])
        self.page_file_user.selectOptionInsideModal(1, self.page_file_user.data.branch_name["valid"])
        self.page_file_user.selectOptionInsideModal(5, self.page_file_user.data.user_level["valid"])
        self.page_file_user.modal_button_ok.click()
        time.sleep(self.short_wait)
        search_result = self.page_file_user.searchText2(3, user_name)
        self.assertTrue(search_result > 0, "Could not find added data in Staff table")

        self.temp_row.append(search_result)
        self.temp_data.append(user_name)

        time.sleep(self.sleep)


    '''

    # ========================= TEST CASE AG013 - FILE ADD FAIL ================================
    def test_ag013_001_data_file_user_add_fail(self):
        self.page_file_user.navigateFileUser()
        self.page_file_user.initPageElement()
        self.page_file_user.button_add.click()

        self.page_file_user.initFormElement()
        self.page_file_user.initBasicModalElement()
        #self.page_file_user.clearForm()
        user_name = self.page_file_user.data.user_name["valid"]
        self.page_file_user.text_user_name.send_keys(user_name)
        self.page_file_user.text_user_password.send_keys(self.page_file_user.data.password["valid"])
        self.page_file_user.text_user_confirm_password.send_keys(self.page_file_user.data.confirm_password["valid"])
        #self.page_file_user.selectOptionInsideModal(1, self.page_file_user.data.branch_name["valid"])
        self.page_file_user.selectOptionInsideModal(5, self.page_file_user.data.user_level["valid"])
        self.page_file_user.modal_button_ok.click()
        self.assertTrue(self.page_file_user.isFormErrorDisplayed(), "Form error should be displayed")

        time.sleep(self.sleep)


    def test_ag013_002_data_file_user_add_fail(self):
        self.page_file_user.navigateFileUser()
        self.page_file_user.initPageElement()
        self.page_file_user.button_add.click()

        self.page_file_user.initFormElement()
        self.page_file_user.initBasicModalElement()
        #self.page_file_user.clearForm()
        user_name = self.page_file_user.data.user_name["valid"]
        #self.page_file_user.text_user_name.send_keys(user_name)
        self.page_file_user.text_user_password.send_keys(self.page_file_user.data.password["valid"])
        self.page_file_user.text_user_confirm_password.send_keys(self.page_file_user.data.confirm_password["valid"])
        self.page_file_user.selectOptionInsideModal(1, self.page_file_user.data.branch_name["valid"])
        self.page_file_user.selectOptionInsideModal(5, self.page_file_user.data.user_level["valid"])
        self.page_file_user.modal_button_ok.click()
        self.assertTrue(self.page_file_user.isFormErrorDisplayed(), "Form error should be displayed")

        time.sleep(self.sleep)

    def test_ag013_003_data_file_user_add_fail(self):
        self.page_file_user.navigateFileUser()
        self.page_file_user.initPageElement()
        self.page_file_user.button_add.click()

        self.page_file_user.initFormElement()
        self.page_file_user.initBasicModalElement()
        self.page_file_user.clearForm()

        self.page_file_user.text_user_password.send_keys(self.page_file_user.data.password["valid"])
        self.page_file_user.text_user_confirm_password.send_keys(self.page_file_user.data.confirm_password["valid"])
        self.page_file_user.selectOptionInsideModal(1, self.page_file_user.data.branch_name["valid"])
        self.page_file_user.selectOptionInsideModal(5, self.page_file_user.data.user_level["valid"])
        user_name = self.page_file_user.data.user_name["invalid"]
        self.page_file_user.text_user_name.send_keys(user_name)
        self.page_file_user.modal_button_ok.click()
        self.assertTrue(self.page_file_user.isFormErrorDisplayed(), "Form error should be displayed")

        time.sleep(self.sleep)



    def test_ag013_004_data_file_user_add_fail(self):
        self.page_file_user.navigateFileUser()
        self.page_file_user.initPageElement()
        self.page_file_user.button_add.click()

        self.page_file_user.initFormElement()
        self.page_file_user.initBasicModalElement()
        #self.page_file_user.clearForm()
        user_name = self.page_file_user.data.user_name["valid"]
        self.page_file_user.text_user_name.send_keys(user_name)
        #self.page_file_user.text_user_password.send_keys(self.page_file_user.data.password["valid"])
        self.page_file_user.text_user_confirm_password.send_keys(self.page_file_user.data.confirm_password["valid"])
        self.page_file_user.selectOptionInsideModal(1, self.page_file_user.data.branch_name["valid"])
        self.page_file_user.selectOptionInsideModal(5, self.page_file_user.data.user_level["valid"])
        self.page_file_user.modal_button_ok.click()
        self.assertTrue(self.page_file_user.isFormErrorDisplayed(), "Form error should be displayed")

        time.sleep(self.sleep)




    def test_ag013_005_data_file_user_add_fail(self):
        self.page_file_user.navigateFileUser()
        self.page_file_user.initPageElement()
        self.page_file_user.button_add.click()

        self.page_file_user.initFormElement()
        self.page_file_user.initBasicModalElement()
        #self.page_file_user.clearForm()
        user_name = self.page_file_user.data.user_name["valid"]
        self.page_file_user.text_user_name.send_keys(user_name)
        self.page_file_user.text_user_password.send_keys(self.page_file_user.data.password["invalid2"])
        self.page_file_user.text_user_confirm_password.send_keys(self.page_file_user.data.confirm_password["invalid2"])
        self.page_file_user.selectOptionInsideModal(1, self.page_file_user.data.branch_name["valid"])
        self.page_file_user.selectOptionInsideModal(5, self.page_file_user.data.user_level["valid"])
        self.page_file_user.modal_button_ok.click()
        self.assertTrue(self.page_file_user.isFormErrorDisplayed(), "Form error should be displayed")

        time.sleep(self.sleep)



    def test_ag013_006_data_file_user_add_fail(self):
        self.page_file_user.navigateFileUser()
        self.page_file_user.initPageElement()
        self.page_file_user.button_add.click()

        self.page_file_user.initFormElement()
        self.page_file_user.initBasicModalElement()
        #self.page_file_user.clearForm()
        user_name = self.page_file_user.data.user_name["valid"]
        self.page_file_user.text_user_name.send_keys(user_name)
        self.page_file_user.text_user_password.send_keys(self.page_file_user.data.password["valid"])
        self.page_file_user.text_user_confirm_password.send_keys(self.page_file_user.data.confirm_password["invalid"])
        self.page_file_user.selectOptionInsideModal(1, self.page_file_user.data.branch_name["valid"])
        self.page_file_user.selectOptionInsideModal(5, self.page_file_user.data.user_level["valid"])
        self.page_file_user.modal_button_ok.click()
        self.assertTrue(self.page_file_user.isFormErrorDisplayed(), "Form error should be displayed")
        time.sleep(self.sleep)




    def test_ag013_007_data_file_user_add_fail(self):
        self.page_file_user.navigateFileUser()
        self.page_file_user.initPageElement()
        self.page_file_user.button_add.click()

        self.page_file_user.initFormElement()
        self.page_file_user.initBasicModalElement()
        #self.page_file_user.clearForm()

        user_name = self.page_file_user.data.user_name["valid"]
        self.page_file_user.text_user_name.send_keys(user_name)
        self.page_file_user.text_user_password.send_keys(self.page_file_user.data.password["valid"])
        self.page_file_user.text_user_confirm_password.send_keys(self.page_file_user.data.confirm_password["valid"])
        self.page_file_user.text_branch.send_keys(self.page_file_user.data.branch_name["valid"])
        #self.page_file_user.selectOptionInsideModal(1, self.page_file_user.data.branch_name["valid"])
        #self.page_file_user.selectOptionInsideModal(5, self.page_file_user.data.user_level["valid"])
        self.page_file_user.modal_button_ok.click()
        self.assertTrue(self.page_file_user.isFormErrorDisplayed(), "Form error should be displayed")

        time.sleep(self.sleep)




    # ========================= TEST CASE AG014 - FILE EDIT SUCCESS ================================
    def test_ag014_001_data_file_user_edit_success(self):
        self.page_file_user.navigateFileUser()
        self.page_file_user.initPageElement()
        if not self.temp_row:
            row1 = 2
        else:
            row1 = self.temp_row[0]
        edit_button = self.page_file_user.getNthRowObject(row1, 'edit')
        edit_button.click()
        self.page_file_user.initFormElement()
        edit_user_branch_data = self.page_file_user.data.branch_name["valid2"]
        self.page_file_user.text_branch.clear()
        self.page_file_user.text_branch.send_keys(edit_user_branch_data)
        self.page_file_user.initConfirmBoxEdit()
        self.page_file_user.modal_button_ok.click()
        #self.page_file_user.waitDimDisappeared()
        time.sleep(self.short_wait)
        search_result = self.page_file_user.searchText2(2, edit_user_branch_data)
        self.assertTrue(search_result > 0, "Could not find added data in user table")
        time.sleep(self.sleep)


    def test_ag014_002_data_file_user_edit_success(self):
        self.page_file_user.navigateFileUser()
        self.page_file_user.initPageElement()
        if not self.temp_row:
            row1 = 3
        else:
            row1 = self.temp_row[0]
        edit_button = self.page_file_user.getNthRowObject(row1, 'edit')
        edit_button.click()
        self.page_file_user.initFormElement()
        edit_user_name_data = self.page_file_user.data.user_name["valid2"]
        self.page_file_user.text_user_name.clear()
        self.page_file_user.text_user_name.send_keys(edit_user_name_data)
        self.page_file_user.initConfirmBoxEdit()
        self.page_file_user.modal_button_ok.click()
        #self.page_file_user.waitDimDisappeared()
        time.sleep(self.short_wait)
        search_result = self.page_file_user.searchText2(3, edit_user_name_data)

        self.temp_data[0] = edit_user_name_data
        self.assertTrue(search_result > 0, "Could not find added data in User table")
        time.sleep(self.sleep)



    def test_ag014_003_data_file_user_edit_success(self):
        self.page_file_user.navigateFileUser()
        self.page_file_user.initPageElement()
        if not self.temp_row:
            row1 = 3
        else:
            row1 = self.temp_row[0]
        edit_button = self.page_file_user.getNthRowObject(row1, 'edit')
        edit_button.click()
        self.page_file_user.initFormElement()
        edit_user_password_data = self.page_file_user.data.user_name["valid2"]
        self.page_file_user.text_user_name.clear()
        self.page_file_user.text_user_name.send_keys(edit_user_password_data)
        self.page_file_user.initConfirmBoxEdit()
        self.page_file_user.modal_button_ok.click()
        #self.page_file_user.waitDimDisappeared()
        time.sleep(self.short_wait)
        #search_result = self.page_file_user.searchText(3, 5, edit_user_name_data)
        #self.assertTrue(search_result > 0, "Could not find added data in User table")
        # @todo ==== logout and login again
        time.sleep(self.sleep)


    def test_ag014_004_data_file_user_edit_success(self):
        self.page_file_user.navigateFileUser()
        self.page_file_user.initPageElement()
        if not self.temp_row:
            row1 = 4
        else:
            row1 = self.temp_row[0]
        edit_button = self.page_file_user.getNthRowObject(row1, 'edit')
        edit_button.click()
        self.page_file_user.initFormElement()
        edit_user_level_data = self.page_file_user.data.user_level["valid2"]
        self.page_file_user.text_level.clear()
        self.page_file_user.text_level.send_keys(edit_user_level_data)
        self.page_file_user.initConfirmBoxEdit()
        self.page_file_user.modal_button_ok.click()
        #self.page_file_user.waitDimDisappeared()
        time.sleep(self.short_wait)
        self.page_file_user.getTableNthRowValue(row1, 3)
        search_result = self.page_file_user.searchText2(5, edit_user_level_data)
        self.assertTrue(search_result > 0, "Could not find edied data in User table")
        time.sleep(self.sleep)




    def test_ag014_005_data_file_user_edit_success(self):
        self.page_file_user.navigateFileUser()
        self.page_file_user.initPageElement()
        if not self.temp_row:
            row1 = 2
        else:
            row1 = self.temp_row[0]
        edit_button = self.page_file_user.getNthRowObject(row1, 'edit')
        edit_button.click()
        self.page_file_user.initFormElement()
        edit_staff_name_data = self.page_file_user.data.staff_name["valid2"]
        self.page_file_user.text_staff.clear()
        self.page_file_user.text_staff.send_keys(edit_staff_name_data)
        self.page_file_user.initConfirmBoxEdit()
        self.page_file_user.modal_button_ok.click()
        #self.page_file_user.waitDimDisappeared()
        time.sleep(self.short_wait)
        search_result = self.page_file_user.searchText2(4, edit_staff_name_data)
        self.assertTrue(search_result > 0, "Could not find edited data in user table")
        time.sleep(self.sleep)




    # ========================= TEST CASE AG015 - FILE EDIT SUCCESS ================================

    def test_ag015_001_data_file_user_edit_fail(self):
        # @todo - can not clear text field of name
        self.page_file_user.navigateFileUser()
        self.page_file_user.initPageElement()
        if not self.temp_row:
            row1 = 3
        else:
            row1 = self.temp_row[0]
        edit_button = self.page_file_user.getNthRowObject(row1, 'edit')
        edit_button.click()
        self.page_file_user.initFormElement()
        self.page_file_user.text_user_name.clear()
        #self.page_file_user.text_user_name.send_keys(Keys.BACKSPACE)

        self.page_file_user.initConfirmBoxEdit()
        self.page_file_user.text_user_name.clear()
        self.page_file_user.modal_button_ok.click()

        #self.page_file_user.waitDimDisappeared()
        self.assertTrue(self.page_file_user.isFormErrorDisplayed(), "Form error should be displayed")
        time.sleep(self.sleep)



    def test_ag015_002_data_file_user_edit_fail(self):
        # @todo - can not clear text field of name
        self.page_file_user.navigateFileUser()
        self.page_file_user.initPageElement()
        if not self.temp_row:
            row1 = 3
        else:
            row1 = self.temp_row[0]
        edit_button = self.page_file_user.getNthRowObject(row1, 'edit')
        edit_button.click()
        self.page_file_user.initFormElement()
        self.page_file_user.text_user_name.clear()
        self.page_file_user.text_user_name.send_keys(self.page_file_user.data.user_name["invalid"])
        self.page_file_user.initConfirmBoxEdit()
        self.page_file_user.modal_button_ok.click()
        time.sleep(self.short_wait)
        #self.page_file_user.waitDimDisappeared()
        self.assertTrue(self.page_file_user.isFormErrorDisplayed(), "Form error should be displayed")
        time.sleep(self.sleep)



    def test_ag015_003_data_file_user_edit_fail(self):
        # @todo - can not clear text field of name
        self.page_file_user.navigateFileUser()
        self.page_file_user.initPageElement()
        if not self.temp_row:
            row1 = 3
        else:
            row1 = self.temp_row[0]
        edit_button = self.page_file_user.getNthRowObject(row1, 'edit')
        edit_button.click()
        self.page_file_user.initFormElement()
        self.page_file_user.text_user_password.clear()
        self.page_file_user.text_user_confirm_password.clear()
        self.page_file_user.text_user_password.send_keys(self.page_file_user.data.password["valid"])
        self.page_file_user.text_user_confirm_password.send_keys(self.page_file_user.data.password["invalid"])
        self.page_file_user.initConfirmBoxEdit()
        self.page_file_user.modal_button_ok.click()

        #self.page_file_user.waitDimDisappeared()
        self.assertTrue(self.page_file_user.isFormErrorDisplayed(), "Form error should be displayed")
        time.sleep(self.sleep)


    '''



    # ========================= TEST CASE AG016 ========================
    def test_ag016_001_data_file_user_delete_success(self):
        self.page_file_user.navigateFileUser()
        self.page_file_user.initPageElement()
        data_user_code = self.temp_data[0]
        search_result = self.page_file_user.searchText2(3, data_user_code)

        self.assertTrue(search_result > 0, "Data should be in table before remove")
        row1 = search_result
        delete_button = self.page_file_user.getNthRowObject(row1, 'delete')
        delete_button.click()
        self.page_file_user.initConfirmBoxDelete()
        self.page_file_user.confirm_button_ok.click()
        time.sleep(self.short_wait)
        search_result = self.page_file_user.searchText2(3, data_user_code)

        self.assertTrue(search_result == 0, "removed data still appear in User table")
        time.sleep(self.sleep)
    
