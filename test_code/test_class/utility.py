# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import unittest

# Create your tests here.
#from selenium.common.exceptions import *

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import *
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import *
from math import *
from page_object.data import ErrorMessage, TestErrorDescription

from page_object.basepage import BasePageLocator, BaseCounter

import time


class TextboxUtility(object):



    def __init__(self, browser, textfield_object, button_submit, textfield_spec):
        self.browser = browser
        self.textfield_object = textfield_object
        self.textfield_spec = textfield_spec
        self.wait = WebDriverWait(self.browser, 10)
        #self.valid_input = self.getValidData()
        self.button_submit = button_submit
        self.is_edit = False



    '''
    Create valid data for textbox according to textbox spec object
    '''
    def getValidData(self):
        # accept numeric value only
        if self.textfield_spec.is_accept_numeric and  (not self.textfield_spec.is_accept_alphabet):

            if self.textfield_spec.max_numeric_value == None and self.textfield_spec.min_numeric_value == None:
                self.textfield_spec.min_numeric_value = 1
                pow_plus_1 = self.textfield_spec.max_length_str + 1
                self.textfield_spec.max_numeric_value = pow(10,pow_plus_1) - 1

            mid_val = (self.textfield_spec.max_numeric_value + self.textfield_spec.min_numeric_value) / 2

            if self.textfield_spec.is_accept_decimal:
                if ceil(mid_val) == mid_val: #check is decimal digit
                    mid_val += 0.1
            else:
                if ceil(mid_val) != mid_val:
                    mid_val = ceil(mid_val)
            if self.textfield_spec.is_key == True:
                BaseCounter.int_counter += 1

            mid_val = mid_val + BaseCounter.int_counter
            if self.textfield_spec.min_length_str == self.textfield_spec.max_length_str:
                if len(str(mid_val)) < self.textfield_spec.min_length_str:
                    mid_val = str(mid_val).zfill(self.textfield_spec.min_length_str)

            input_str = mid_val




        # accept both numeric and alphabet
        elif (not self.textfield_spec.is_accept_numeric) and self.textfield_spec.is_accept_alphabet:
            if self.textfield_spec.max_length_str == self.textfield_spec.min_length_str:
                str_length = self.textfield_spec.min_length_str
            else:
                str_length = ceil((self.textfield_spec.min_length_str + self.textfield_spec.max_length_str) / 2)

            input_str = "A"

            variant_str = "abcdefghijklmnopqrstuvwxyz"
            variant_char = list(variant_str)
            if self.textfield_spec.is_key == True:
                BaseCounter.int_counter += 1
                idx = BaseCounter.int_counter % 26
                additional_char = variant_char[idx]
                input_str = input_str + additional_char

            if self.textfield_spec.is_accept_space_bar:
                input_str = input_str + " "
            if self.textfield_spec.is_accept_foreign_language_char:
                input_str = input_str + "ก"

            input_str = input_str + self.textfield_spec.allowed_special_char
            input_length = (self.textfield_spec.max_length_str + self.textfield_spec.min_length_str)/2

            input_str = input_str.ljust(input_length, "A")


        # accept both numeric and alphabet
        elif self.textfield_spec.is_accept_numeric and self.textfield_spec.is_accept_alphabet:
            if self.textfield_spec.max_length_str == self.textfield_spec.min_length_str:
                str_length = self.textfield_spec.min_length_str
            else:
                str_length = ceil((self.textfield_spec.min_length_str + self.textfield_spec.max_length_str) / 2)

            input_str = "E"

            if self.textfield_spec.is_accept_space_bar:
                input_str = input_str + " "
            if self.textfield_spec.is_accept_foreign_language_char:
                input_str = input_str + "ก"

            input_str = input_str + self.textfield_spec.allowed_special_char

            if self.textfield_spec.is_key == True:
                BaseCounter.int_counter += 1
            input_str = input_str + str(BaseCounter.int_counter)
            input_length = (self.textfield_spec.max_length_str + self.textfield_spec.min_length_str)/2
            input_str = input_str.ljust(input_length, "A")


        return input_str

    '''
    waiting for teble cell
    '''
    def hasTableCellContain(self, contain_data):
        try:
            if type(contain_data) is int:
                contain_data = str(contain_data)

            self.inserted_cell = self.wait.until(EC.presence_of_element_located((By.XPATH, '//div[@class="fixedDataTableCellLayout_main public_fixedDataTableCell_main"]/div/div/div/div/div/div[contains(text(), "' + contain_data + '")]')))
        except (NoSuchElementException, TimeoutException) as e:
            TestErrorDescription.temp_method_error_message = "Not found value of in data table"
            return False
        return True


    '''
    check textbox act in correct way when input prohibited special char, error message must show
    '''
    def checkProhibitedSpecialChar(self):

        allowed_char = self.textfield_spec.allowed_special_char
        all_special_char = "!#$%&'()*+,-./:;<=>?@[\]^_`{|}~"
        all_is_ok = False
        ok_counter = 0
        expected_ok_counter = len(all_special_char)
        all_special_char_list = list(all_special_char)
        #not_allow_special_char = ""
        if allowed_char != "":
            allowed_char_list = list(allowed_char)
            for ac in allowed_char_list:
                all_special_char_list.remove(ac)
            prohibited_special_char = all_special_char_list
        else:
            prohibited_special_char = all_special_char_list


        valid_data = self.getValidData()
        if type(valid_data) is int:
            valid_data = str(valid_data)
        ln = len(valid_data) - 1
        i = 0
        try:
            for ac in all_special_char_list:
                input_data = valid_data[:ln] + prohibited_special_char[i]
                i += 1
                self.textfield_object.clear()
                self.textfield_object.send_keys(input_data)
                self.button_submit.click()
                if self.hasFormError(ErrorMessage.textbox_not_accept_special_char):
                    ok_counter += 1

            # return True if all prohibited special characters generate error message.
            if ok_counter == expected_ok_counter:
                return True
            else:
                TestErrorDescription.temp_method_error_message = "Not work correctly on checking SPECIAL CHAR condition "
                return False
        except:
            TestErrorDescription.temp_method_error_message = "Not work correctly on checking SPECIAL CHAR condition "
            return False



    '''
    Fill data to the text box.
    '''
    def fillValidData(self):
        ret = self.getValidData()
        self.textfield_object.send_keys(ret)
        return ret

    '''
    init form error
    '''
    def hasFormError(self, search_text = "กรุณา", input_object = None):
        try:
            if input_object == None:
                input_object = self.textfield_object

            if type(search_text) is int:
                contain_data = str(search_text)
            try:
                #input textbox error
                error_div = input_object.find_element_by_xpath("../../label[1]/span/span[contains(text(), '"+ search_text +"')]")
            except NoSuchElementException as e:
                #text area error
                error_div = input_object.find_element_by_xpath("../label[1]/span/span[contains(text(), '"+ search_text +"')]")

            error_message = error_div.get_attribute('innerHTML')

            if error_message.find(search_text) != -1:
                return True
        except (StaleElementReferenceException, NoSuchElementException, TimeoutException) as e:
            return False
        return False

    '''
    Mix valid input with insterested value and replace in the textbox
    '''
    def replaceTailValidInput(self, tail_input, valid_input = None):
        if valid_input == None:
            if self.textfield_object.get_attribute('value') != "":
                valid_input = self.textfield_object.get_attribute('value')
            else:
                valid_input = self.getValidData()
        if type(tail_input) is int:
            tail_input = str(tail_input)
        tail_input_length = len(tail_input)
        strip_input_length = -1 * tail_input_length
        if type(valid_input) is int:
            valid_input = str(valid_input)
        valid_input = valid_input[:strip_input_length]
        new_input = valid_input + tail_input
        self.textfield_object.clear()
        if self.is_edit:
            new_input = self.replaceEditData(new_input)
        self.textfield_object.send_keys(new_input)
        return new_input

    '''
    clear textfield object
    '''
    def clear(self):
        self.textfield_object.clear()


    def clickSubmitButton(self):
        self.button_submit.click()


    def getName(self):
        return self.textfield_spec.name


    def initTable(self):
        self.table_cells = self.browser.find_elements(*BasePageLocator.TABLE_CELLS)


    '''
    return list of value of specified column
    '''
    def getTableColumnValue(self, column_search, column_all):
        self.initTable()
        col_count = 1
        row_count = 1
        element_list = []
        #print(text)
        #print('-----')
        for cell in self.table_cells:
            #print cell on interested column
            if col_count == column_search:
                element_list.append(cell.text)
            if col_count == column_all:
                row_count += 1
                col_count = 1
            else:
                col_count += 1

        return element_list


    def getAllSelectFieldElement(self, textbox = None):
        if textbox == None:
            textbox = self.textfield_object

        val_list = []
        select_list = textbox.find_elements_by_xpath("../div[@role='listbox']/div/span")
        i = 1
        for elem in select_list:
            if i > 1:
                val = elem.get_attribute('innerHTML')
                val_list.append(val)
            i += 1
        return val_list




    def selectList(self, text_value, textfield_object = None):
        if textfield_object == None:
            textfield_object = self.textfield_object
        select_icon = textfield_object.find_element_by_xpath("../i")
        select_icon.click()
        select_list = textfield_object.find_element_by_xpath("../div[@role='listbox']/div/span[contains(text(), '"+ text_value +"')]")
        select_list.click()
        value_div = textfield_object.find_element_by_xpath("./following-sibling::div")
        ret = value_div.get_attribute('innerHTML')

        return ret

    def isSelectListFilterWork(self, text_value, textfield_object = None):
        if textfield_object == None:
            textfield_object = self.textfield_object
        textfield_object.send_keys(text_value)

        select_list = textfield_object.find_elements_by_xpath("../div[@role='listbox']/div/span")
        i = 1
        for list_val in select_list:
            val = list_val.get_attribute('innerHTML')
            if val.find(text_value) == -1:
                return False
            i += 1
        return True








    #================= updated  Utility functions  ======================================


    '''
    check that textbox should act as it be with alphabet
    '''
    def checkAlphabetCondition(self, search_key = None):
        input_str = "A"
        input_val = self.replaceTailValidInput(input_str)
        self.textfield_object.clear()

        self.textfield_object.send_keys(input_val)
        self.button_submit.click()

        if self.textfield_spec.is_accept_alphabet:
            return self.hasTableCellContain(search_key)
        else:
            ret = self.hasFormError(ErrorMessage.textbox_accept_numeric_only)
            TestErrorDescription.temp_method_error_message = "Not work correctly on checking ALPHABET condition "
            return ret


    '''
    check if the textbox act as it should for empty condition
    '''
    def checkEmptyCondition(self, search_key = None):
        self.textfield_object.clear()
        self.button_submit.click()
        if self.textfield_spec.is_null_allowed:

            return self.hasTableCellContain(search_key)
        else:
            ret = self.hasFormError(ErrorMessage.textbox_empty_error)
            if ret == False:
                TestErrorDescription.temp_method_error_message = "Not work correctly on checking EMPTY condition "

            return ret


    def checkLengthBelowMinCondition(self, search_key = None):
        self.textfield_object.clear()

        if self.textfield_spec.min_length_str != None:
            valid_input_val = self.getValidData()
            if type(valid_input_val) is int:
                valid_input_val = str(valid_input_val)
            valid_input_val_length = len(valid_input_val)
            input_length = self.textfield_spec.min_length_str - 1
            input_length2 = (valid_input_val_length - input_length) * -1
            input_val = valid_input_val[:input_length2]
            if self.is_edit:
                input_val = self.replaceEditData(input_val)
            self.textfield_object.send_keys(input_val)
            self.button_submit.click()

            ret = self.hasFormError(ErrorMessage.textbox_min_length_error)
            if ret == False:
                TestErrorDescription.temp_method_error_message = "Not work correctly on checking BELOW MIN LENGTH condition "

            # it should error
            return ret

    #==== close on min length point ======
    def checkLengthMinCondition(self, search_key = None):

        if self.textfield_spec.min_length_str != None:
            valid_input_val = self.getValidData()
            if type(valid_input_val) is int:
                valid_input_val = str(valid_input_val)
            valid_input_val_length = len(valid_input_val)
            input_length = self.textfield_spec.min_length_str
            input_length2 = (valid_input_val_length - input_length) * -1
            input_val = valid_input_val[:input_length2]
            self.textfield_object.clear()
            if self.is_edit:
                input_val = self.replaceEditData(input_val)
            self.textfield_object.send_keys(input_val)
            self.button_submit.click()
            # check if the input texfield is primary key
            if search_key == None:
                search_key = input_val
            #it should pass
            return self.hasTableCellContain(search_key)

    def checkLengthMaxCondition(self, search_key = None):
        self.textfield_object.clear()
        if self.textfield_spec.max_length_str != None:
            valid_input_val = self.getValidData()
            if type(valid_input_val) is int:
                valid_input_val = str(valid_input_val)
            valid_input_val_length = len(valid_input_val)
            input_length = self.textfield_spec.max_length_str

            repeat = input_length
            c = valid_input_val[-1:]
            input_val = valid_input_val.ljust(repeat, c)
            if self.is_edit:
                input_val = self.replaceEditData(input_val)
            self.textfield_object.send_keys(input_val)
            self.button_submit.click()
            # check if the input texfield is primary key
            if search_key == None:
                search_key = input_val
            return self.hasTableCellContain(search_key)

    def checkLengthOverMaxCondition(self):
        self.textfield_object.clear()
        if self.textfield_spec.max_length_str != None:
            valid_input_val = self.getValidData()
            if type(valid_input_val) is int:
                valid_input_val = str(valid_input_val)
            valid_input_val_length = len(valid_input_val)
            input_length = self.textfield_spec.max_length_str + 1

            repeat = input_length
            c = valid_input_val[-1:]
            input_val = valid_input_val.ljust(repeat, c)
            if self.is_edit:
                input_val = self.replaceEditData(input_val)
            self.textfield_object.send_keys(input_val)
            # if javascript limit length, length never over limit, return True
            if len(self.textfield_object.get_attribute('value')) <= self.textfield_spec.max_length_str:
                return True
            self.button_submit.click()
            ret = self.hasFormError(ErrorMessage.textbox_max_length_error)
            if ret == False:
                TestErrorDescription.temp_method_error_message = "Not work correctly on checking OVER MAX LENGTH condition "
            return ret
    #def checkLengthMinConditionFalse() -- For open min bound Length
    #def checkLengthMaxConditionFalse() -- for open max bound Length

    def checkForeignChar(self, search_key = None):

        if self.textfield_object.get_attribute('value') != "":
            valid_input = self.textfield_object.get_attribute('value')
        else:
            valid_input = self.getValidData()
        if type(valid_input) is int:
            valid_input = str(valid_input)
        new_str = valid_input[:-1]
        new_str = new_str + "ก"
        self.textfield_object.clear()
        if self.is_edit:
            input_val = self.replaceEditData(input_val)
        self.textfield_object.send_keys(new_str)
        #self.replaceTailValidInput(a)
        self.button_submit.click()

        if self.textfield_spec.is_accept_foreign_language_char:
            return self.hasTableCellContain(search_key)
        else:
            ret = self.hasFormError(ErrorMessage.textbox_not_accept_foreign_char)
            if ret == False:
                TestErrorDescription.temp_method_error_message = "Not work correctly on checking FOREIGN CHAR condition "
            return ret


    '''
    check if textbox response in correct way when space bar input
    '''
    def checkSpaceBarCondition(self, search_key = None):
        self.replaceTailValidInput(' ')
        self.button_submit.click()
        if self.textfield_spec.is_accept_space_bar:
            return self.hasTableCellContain(search_key)
        else:
            ret = self.hasFormError(ErrorMessage.textbox_not_accept_space_bar)
            if ret == False:
                TestErrorDescription.temp_method_error_message = "Not work correctly on checking SPACE BAR condition "
            return ret

    def checkDuplicateCondition(self, duplicate_value):
        self.textfield_object.clear()
        self.textfield_object.send_keys(duplicate_value)
        self.button_submit.click()
        ret = self.hasFormError(ErrorMessage.textbox_duplicate_error)
        if ret == False:
            TestErrorDescription.temp_method_error_message = "Not work correctly on checking DATA DUPLICATE condition "
        return ret


    def replaceEditData(self, data, textfield_object = None):
        if textfield_object == None:
            textfield_object = self.textfield_object
        temp_data1 = data
        data1 = data
        if type(data1) is int:
            return data1 + 50
        data1 = data1.replace("ก", "ข")
        data1 = data1.replace("E", "D")
        data1 = data1.replace("A", "B")
        data1 = data1.replace("0", "1")
        self.is_edit = False

        return data1

        # get text field spec and replace value according to textfield_spec

        # place edited value

        # check edit pass

        # check edit error


    '''
    output_type : 'success' or 'fail'
    '''
    def check(self, input_str, output_type, output_message = None):
        if type(input_str) is int:
            input_str = str(input_str)
        self.textfield_object.clear()
        self.textfield_object.send_keys(input_str)
        self.button_submit.click()
        if output_type == 'success':
            ret = self.hasTableCellContain(input_str)
            if ret == False:
                TestErrorDescription.temp_method_error_message = "Data : "+ input_str +" expect success insertion but it is not."

        else:
            ret = self.hasFormError(output_message)
            if ret == False:
                TestErrorDescription.temp_method_error_message = "Data : "+ input_str +" expect error message but it is not."

        return ret
























    #
    # '''
    # check if valid data has completely inserted and appear in table
    # '''
    # def isValidDataPass(self, search_key):
    #
    #     input_data = self.getValidData()
    #     self.textfield_object.clear()
    #     self.textfield_object.send_keys(input_data)
    #     self.button_submit.click()
    #
    #     if self.hasTableCellContain(search_key):
    #         return True
    #     else:
    #         return False
    #
    #
    #
    #
    # '''
    # check textbox should act as it be with number
    # '''
    # def checkNumberCondition(self, search_key = None):
    #     self.textfield_object.clear()
    #     input_str = "1"
    #     input_length = (self.textfield_spec.max_length_str + self.textfield_spec.min_length_str)/2
    #     input_str = input_str.ljust(input_length, "0")
    #
    #     self.textfield_object.send_keys(input_str)
    #     self.button_submit.click()
    #
    #     if self.textfield_spec.is_accept_numeric:
    #         return self.hasTableCellContain(search_key)
    #     else:
    #         return self.hasFormError(ErrorMessage.textbox_not_accept_numeric)
    #
    #
    #
    #
    #
    #
    # '''
    # check if textbox response in correct way when numeric with decimal point input
    # '''
    # def checkDecimalPoint(self, search_key = None):
    #     self.textfield_object.clear()
    #     self.textfield_object.send_keys('1.1')
    #     self.button_submit.click()
    #     if self.textfield_spec.is_accept_numeric:
    #         if self.textfield_spec.is_accept_decimal:
    #             return self.hasTableCellContain(search_key)
    #         else:
    #             return self.hasFormError(ErrorMessage.textbox_not_accept_decimal_point)
    #
    # '''
    # check if textbox response in correct way when foreign char input
    # '''
    # # ========================= NUMERIC RANGE CHECK =======================
    # '''
    # check if textbox response in correct way when has numeric input lower than min
    # BELOW MIN ==> False
    # MIN ==> True
    # MAX ==> True
    # OVER MAX ==> False
    # '''
    # def checkNumericBelowMinCondition(self):
    #     self.textfield_object.clear()
    #     if self.textfield_spec.min_numeric_value != None:
    #         input_val = self.textfield_spec.min_numeric_value - 1
    #         self.textfield_object.send_keys(input_val)
    #         self.button_submit.click()
    #         # it should error
    #         return self.hasFormError(ErrorMessage.textbox_min_val_error)
    #
    # def checkNumericMinCondition(self, search_key = None):
    #     self.textfield_object.clear()
    #     if self.textfield_spec.min_numeric_value != None:
    #         self.textfield_object.send_keys(self.textfield_spec.min_numeric_value)
    #         self.button_submit.click()
    #         # check if the input texfield is primary key
    #         if search_key == None:
    #             search_key = self.textfield_spec.min_numeric_value
    #         #it should pass
    #         return self.hasTableCellContain(search_key)
    #
    # def checkNumericMaxCondition(self, search_key = None):
    #     self.textfield_object.clear()
    #     if self.textfield_spec.max_numeric_value != None:
    #         self.textfield_object.send_keys(self.textfield_spec.max_numeric_value)
    #         self.button_submit.click()
    #         # check if the input texfield is primary key
    #         if search_key == None:
    #             search_key = self.textfield_spec.max_numeric_value
    #         #it should pass
    #         return self.hasTableCellContain(search_key)
    #
    # def checkNumericOverMaxCondition(self):
    #     self.textfield_object.clear()
    #     if self.textfield_spec.max_numeric_value != None:
    #         input_val = self.textfield_spec.max_numeric_value + 1
    #         self.textfield_object.send_keys(input_val)
    #
    #         #if javascript already limit max value on text field, return True
    #         if self.textfield_object.get_attribute('value') <= self.textfield_spec.max_numeric_value:
    #             return True
    #
    #         self.button_submit.click()
    #         #it should error
    #         return self.hasFormError(ErrorMessage.textbox_max_val_error)
    #
    # #def checkNumericMinConditionFalse() -- For open min bound
    # #def checkNumericMaxConditionFalse() -- for open max bound
    #
    # '''
    # check if textbox response in correct way when it has various length
    # BELOW MIN ==> False
    # MIN ==> True
    # MAX ==> True
    # OVER MAX ==> False
    # '''




































    '''
    not allow char
    check that textbox should act as it be with not allowed special char

    space bar condition

    decimal point condition

    foreign char condition


    numeric : below min
    numeric : min
    numeric : max
    numeric : over max


    alphabet : below min length
    alphabet : min length
    alphabet : max length
    alphabet : over max length



    '''
