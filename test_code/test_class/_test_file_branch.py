# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import unittest

# Create your tests here.
#from selenium.common.exceptions import *

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import *
from selenium.webdriver.chrome.options import Options
import time

from test_class.basetest import BaseTest
from test_class.utility import TextboxUtility
from page_object.file.file_branch import FileBranch
from page_object.file.file_staff import FileStaff
from page_object.textfield_spec import *
from page_object.data import StaffData, BranchData, TestErrorDescription, ErrorMessage


class TestFileBranch(BaseTest):

    page_file_branch = None #Page Object

    browser = None
    temp_row = []
    temp_data = []
    temp_duplicate = None
    text_file_branch_branchcode = None
    text_file_branch_branchname = None
    text_file_branch_branchnote = None
    search_key = 0


    def setUp(self): #test class initialize
        super(TestFileBranch, self).setUp()
        #page1 setup
        self.page_file_branch = FileBranch(self.browser)
        self.page_file_branch.loadPage()
        self.page_file_branch.login()
        self.page_file_branch.navigateFileBranch()
        self.page_file_branch.initPageElement()
        self.page_file_branch.button_add.click()
        self.page_file_branch.initFormElement()
        self.page_file_branch.initBasicModalElement()

        self.text_file_branch_branchcode_spec = File_Branch_BranchCode()
        self.text_file_branch_branchname_spec = File_Branch_BranchName()
        self.text_file_branch_branchnote_spec = File_Branch_BranchNote()

        self.text_file_branch_branchcode = TextboxUtility(self.browser, self.page_file_branch.text_branch_code, self.page_file_branch.modal_button_ok, self.text_file_branch_branchcode_spec)
        self.text_file_branch_branchname = TextboxUtility(self.browser, self.page_file_branch.text_branch_name, self.page_file_branch.modal_button_ok, self.text_file_branch_branchname_spec)
        self.text_file_branch_branchnote = TextboxUtility(self.browser, self.page_file_branch.text_branch_note, self.page_file_branch.modal_button_ok, self.text_file_branch_branchnote_spec)

        self.search_key = self.text_file_branch_branchcode.fillValidData()
        self.text_file_branch_branchname.fillValidData()
        self.text_file_branch_branchnote.fillValidData()
        self.validation_obj_name = "textbox branch code "


    '''
    # ========================= textbox branch code validation ================================
    '''
    '''
    def test_ag002_001_text_branch_code_valid(self):
        #self.search_key += 1
        #self.assertTrue(self.text_file_branch_branchcode.isValidDataPass(self.search_key), "Valid data should completely insert record but it doesn't")
        self.page_file_branch.modal_button_ok.click()
        ret = self.text_file_branch_branchcode.hasTableCellContain(self.search_key)
        self.assertTrue(ret,  self.validation_obj_name +" : " + TestErrorDescription.insert_data_not_found_in_table)




    def test_ag002_007_text_branch_code_empty(self):

        self.text_file_branch_branchcode.clear()
        self.text_file_branch_branchcode.clickSubmitButton()

        ret = self.text_file_branch_branchcode.hasFormError(ErrorMessage.textbox_empty_error)
        self.assertTrue(ret, self.validation_obj_name +" : " + TestErrorDescription.expect_error_message_not_found)
    '''



    #test select box, check data from table and compare with select box list
    def test_ag003_001_staff_select_box_list_check(self):

        self.page_file_branch.modal_button_cancel.click()
        self.page_file_branch.waitDimDisappearedHalf()

        self.page_file_branch.navigateBranch()
        list_branch_name = self.text_file_branch_branchcode.getTableColumnValue(2, 3)

        self.page_file_branch.navigateStaff()
        self.page_file_staff = FileStaff(self.browser)
        self.page_file_staff.initPageElement()
        self.page_file_staff.button_add.click()
        self.page_file_staff.initFormElement()
        self.page_file_staff.initBasicModalElement()
        branch_select = self.page_file_staff.text_select_branch
        branch_select_element_list = self.text_file_branch_branchnote.getAllSelectFieldElement(branch_select)
        self.assertTrue(set(list_branch_name) == set(branch_select_element_list), "Error")
        #print("============")
        #print(list_branch_name)
        #print("============")
        #print(branch_select_element_list)



    #test select
    def test_ag003_002_staff_select_box_select(self):
        self.page_file_branch.modal_button_cancel.click()
        self.page_file_branch.waitDimDisappearedHalf()
        self.page_file_branch.navigateStaff()

        self.page_file_staff = FileStaff(self.browser)
        self.page_file_staff.initPageElement()
        self.page_file_staff.button_add.click()
        self.page_file_staff.initFormElement()
        self.page_file_staff.initBasicModalElement()
        select_data = StaffData.select_data

        #use utility object to use selectList method
        textbox_value = self.text_file_branch_branchcode.selectList(select_data, self.page_file_staff.text_select_branch)

        self.assertTrue(textbox_value == select_data, TestErrorDescription.select_box_cannot_select)



    #test filter --- has pre-condition, use prepared test data 
    def test_ag003_003_staff_select_box_select(self):
        self.page_file_branch.modal_button_cancel.click()
        self.page_file_branch.waitDimDisappearedHalf()
        self.page_file_branch.navigateStaff()

        self.page_file_staff = FileStaff(self.browser)
        self.page_file_staff.initPageElement()
        self.page_file_staff.button_add.click()
        self.page_file_staff.initFormElement()
        self.page_file_staff.initBasicModalElement()
        filter_data = StaffData.filter_data

        #use utility object to use selectList method
        filter_ok = self.text_file_branch_branchcode.isSelectListFilterWork(filter_data, self.page_file_staff.text_select_branch)

        self.assertTrue(filter_ok, TestErrorDescription.select_box_cannot_select)




    '''
    def test_ag001_002_text_branch_code_over_max(self):
        self.assertTrue(self.text_file_branch_branchcode.checkNumericOverMaxCondition(), "branch code - checkNumericOverMaxCondition wrong")


    def test_ag001_003_text_branch_code_below_min(self):
        self.assertTrue(self.text_file_branch_branchcode.checkNumericBelowMinCondition(), "branch code - checkNumericBelowMinCondition wrong")


    def test_ag001_004_text_branch_code_min(self):
        self.assertTrue(self.text_file_branch_branchcode.checkNumericMinCondition(), "branch code - checkNumericMinCondition wrong")


    def test_ag001_005_text_branch_code_max(self):
        self.assertTrue(self.text_file_branch_branchcode.checkNumericMaxCondition(), "branch code - checkNumericMaxCondition wrong")


    def test_ag001_006_text_branch_code_invalid_alphabet(self):
        self.assertTrue(self.text_file_branch_branchcode.checkAlphabetCondition(), "branch code - checkAlphabetCondition wrong")


    def test_ag001_008_text_branch_code_invalid_alphabet(self):
        self.assertTrue(self.text_file_branch_branchcode.checkProhibitedSpecialChar(), "branch code - checkProhibitedSpecialChar wrong")

    '''
