# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import unittest

# Create your tests here.
#from selenium.common.exceptions import *

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import *
from selenium.webdriver.chrome.options import Options
import time

from test_class.basetest import BaseTest
from page_object.file.file_staff import FileStaff


class TestFileStaff(BaseTest):

    page_file_staff = None #Page Object

    browser = None
    temp_row = []
    temp_data = []
    temp_duplicate = None


    def setUp(self): #test class initialize
        super(TestFileStaff, self).setUp()
        #page1 setup
        self.page_file_staff = FileStaff(self.browser)
        self.page_file_staff.loadPage()
        self.page_file_staff.login()


    # ========================= TEST CASE AG006 - FILE ADD SUCCESS ================================
    def test_ag006_001_data_file_employee_add_success(self):
        self.page_file_staff.navigateFileStaff()
        self.page_file_staff.initPageElement()
        self.page_file_staff.button_add.click()

        self.page_file_staff.initFormElement()
        self.page_file_staff.initBasicModalElement()
        self.page_file_staff.clearForm()

        self.page_file_staff.button_generate_staff_code.click()
        time.sleep(self.short_wait)
        staff_code = self.page_file_staff.text_staff_code.get_attribute('value')
        self.page_file_staff.text_staff_name.send_keys(self.page_file_staff.data.staff_name["valid"])
        self.page_file_staff.text_staff_address.send_keys(self.page_file_staff.data.staff_address["valid"])
        self.page_file_staff.text_staff_phone.send_keys(self.page_file_staff.data.staff_phone["valid"])
        self.page_file_staff.selectOptionInsideModal(2, self.page_file_staff.data.branch_name["valid"])
        self.page_file_staff.modal_button_ok.click()
        self.page_file_staff.waitDimDisappeared()
        search_result = self.page_file_staff.searchText(1, 5, staff_code)
        self.assertTrue(search_result > 0, "Could not find added data in Staff table")

        self.temp_row.append(search_result)
        self.temp_data.append(staff_code)

        time.sleep(self.sleep)



    def test_ag006_002_data_file_employee_add_success(self):

        self.page_file_staff.navigateFileStaff()
        self.page_file_staff.initPageElement()
        self.page_file_staff.button_add.click()

        self.page_file_staff.initFormElement()
        self.page_file_staff.initBasicModalElement()
        self.page_file_staff.clearForm()

        staff_code = self.page_file_staff.data.staff_code["valid"]
        self.page_file_staff.text_staff_code.send_keys(staff_code)
        self.page_file_staff.text_staff_name.send_keys(self.page_file_staff.data.staff_name["valid"])
        self.page_file_staff.text_staff_address.send_keys(self.page_file_staff.data.staff_address["valid"])
        self.page_file_staff.text_staff_phone.send_keys(self.page_file_staff.data.staff_phone["valid"])
        self.page_file_staff.selectOptionInsideModal(2, self.page_file_staff.data.branch_name["valid"])
        self.page_file_staff.modal_button_ok.click()
        self.page_file_staff.waitDimDisappeared()
        search_result = self.page_file_staff.searchText(1, 5, staff_code)
        self.assertTrue(search_result > 0, "Could not find added data in Staff table")

        self.temp_row.append(search_result)
        self.temp_data.append(staff_code)

        time.sleep(self.sleep)

    '''

    def test_ag007_001_data_file_employee_add_fail(self):

        self.page_file_staff.navigateFileStaff()
        self.page_file_staff.initPageElement()
        self.page_file_staff.button_add.click()

        self.page_file_staff.initFormElement()
        self.page_file_staff.initBasicModalElement()
        self.page_file_staff.clearForm()

        staff_code = self.page_file_staff.data.staff_code["valid"]
        #self.page_file_staff.text_staff_code.send_keys(staff_code)
        self.page_file_staff.text_staff_name.send_keys(self.page_file_staff.data.staff_name["valid"])
        self.page_file_staff.text_staff_address.send_keys(self.page_file_staff.data.staff_address["valid"])
        self.page_file_staff.text_staff_phone.send_keys(self.page_file_staff.data.staff_phone["valid"])
        self.page_file_staff.selectOptionInsideModal(2, self.page_file_staff.data.branch_name["valid"])
        self.page_file_staff.modal_button_ok.click()
        #self.page_file_staff.waitDimDisappeared()
        self.assertTrue(self.page_file_staff.isFormErrorDisplayed(), "Form error should be displayed")

        time.sleep(self.sleep)


    def test_ag007_002_data_file_employee_add_fail(self):

        self.page_file_staff.navigateFileStaff()
        self.page_file_staff.initPageElement()
        self.page_file_staff.button_add.click()

        self.page_file_staff.initFormElement()
        self.page_file_staff.initBasicModalElement()
        self.page_file_staff.clearForm()

        staff_code = self.page_file_staff.data.staff_code["invalid"]
        self.page_file_staff.text_staff_code.send_keys(staff_code)
        self.page_file_staff.text_staff_name.send_keys(self.page_file_staff.data.staff_name["valid"])
        self.page_file_staff.text_staff_address.send_keys(self.page_file_staff.data.staff_address["valid"])
        self.page_file_staff.text_staff_phone.send_keys(self.page_file_staff.data.staff_phone["valid"])
        self.page_file_staff.selectOptionInsideModal(2, self.page_file_staff.data.branch_name["valid"])
        self.page_file_staff.modal_button_ok.click()
        #self.page_file_staff.waitDimDisappeared()
        self.assertTrue(self.page_file_staff.isFormErrorDisplayed(), "Form error should be displayed")

        time.sleep(self.sleep)

    def test_ag007_003_data_file_employee_add_fail(self):

        self.page_file_staff.navigateFileStaff()
        self.page_file_staff.initPageElement()
        self.page_file_staff.button_add.click()

        self.page_file_staff.initFormElement()
        self.page_file_staff.initBasicModalElement()
        self.page_file_staff.clearForm()

        staff_code = self.page_file_staff.data.staff_code["invalid2"]
        self.page_file_staff.text_staff_code.send_keys(staff_code)
        self.page_file_staff.text_staff_name.send_keys(self.page_file_staff.data.staff_name["valid"])
        self.page_file_staff.text_staff_address.send_keys(self.page_file_staff.data.staff_address["valid"])
        self.page_file_staff.text_staff_phone.send_keys(self.page_file_staff.data.staff_phone["valid"])
        self.page_file_staff.selectOptionInsideModal(2, self.page_file_staff.data.branch_name["valid"])
        self.page_file_staff.modal_button_ok.click()
        #self.page_file_staff.waitDimDisappeared()
        self.assertTrue(self.page_file_staff.isFormErrorDisplayed(), "Form error should be displayed")

        time.sleep(self.sleep)



    def test_ag007_004_data_file_employee_add_fail(self):

        self.page_file_staff.navigateFileStaff()
        self.page_file_staff.initPageElement()
        self.page_file_staff.button_add.click()

        self.page_file_staff.initFormElement()
        self.page_file_staff.initBasicModalElement()
        self.page_file_staff.clearForm()

        staff_code = self.page_file_staff.data.staff_code["valid"]
        self.page_file_staff.text_staff_code.send_keys(staff_code)
        self.page_file_staff.text_staff_name.send_keys(self.page_file_staff.data.staff_name["valid"])
        self.page_file_staff.text_staff_address.send_keys(self.page_file_staff.data.staff_address["valid"])
        self.page_file_staff.text_staff_phone.send_keys(self.page_file_staff.data.staff_phone["valid"])
        #self.page_file_staff.selectOptionInsideModal(2, self.page_file_staff.data.branch_name["valid"])
        self.page_file_staff.modal_button_ok.click()
        #self.page_file_staff.waitDimDisappeared()
        self.assertTrue(self.page_file_staff.isFormErrorDisplayed(), "Form error should be displayed")

        time.sleep(self.sleep)

    def test_ag007_005_data_file_employee_add_fail(self):

        self.page_file_staff.navigateFileStaff()
        self.page_file_staff.initPageElement()
        self.page_file_staff.button_add.click()

        self.page_file_staff.initFormElement()
        self.page_file_staff.initBasicModalElement()
        self.page_file_staff.clearForm()

        staff_code = self.page_file_staff.data.staff_code["valid"]
        self.page_file_staff.text_staff_code.send_keys(staff_code)
        #self.page_file_staff.text_staff_name.send_keys(self.page_file_staff.data.staff_name["valid"])
        self.page_file_staff.text_staff_address.send_keys(self.page_file_staff.data.staff_address["valid"])
        self.page_file_staff.text_staff_phone.send_keys(self.page_file_staff.data.staff_phone["valid"])
        self.page_file_staff.selectOptionInsideModal(2, self.page_file_staff.data.branch_name["valid"])
        self.page_file_staff.modal_button_ok.click()
        #self.page_file_staff.waitDimDisappeared()
        self.assertTrue(self.page_file_staff.isFormErrorDisplayed(), "Form error should be displayed")

        time.sleep(self.sleep)

    def test_ag007_006_data_file_employee_add_fail(self):

        self.page_file_staff.navigateFileStaff()
        self.page_file_staff.initPageElement()
        self.page_file_staff.button_add.click()

        self.page_file_staff.initFormElement()
        self.page_file_staff.initBasicModalElement()
        self.page_file_staff.clearForm()

        staff_code = self.page_file_staff.data.staff_code["valid"]
        self.page_file_staff.text_staff_code.send_keys(staff_code)
        #self.page_file_staff.text_staff_name.send_keys(self.page_file_staff.data.staff_name["valid"])
        self.page_file_staff.text_staff_address.send_keys(self.page_file_staff.data.staff_address["valid"])
        self.page_file_staff.text_staff_phone.send_keys(self.page_file_staff.data.staff_phone["valid"])
        self.page_file_staff.selectOptionInsideModal(2, self.page_file_staff.data.branch_name["valid"])
        self.page_file_staff.modal_button_ok.click()
        #self.page_file_staff.waitDimDisappeared()
        self.assertTrue(self.page_file_staff.isFormErrorDisplayed(), "Form error should be displayed")

        time.sleep(self.sleep)

    def test_ag007_007_data_file_employee_add_fail(self):

        self.page_file_staff.navigateFileStaff()
        self.page_file_staff.initPageElement()
        self.page_file_staff.button_add.click()

        self.page_file_staff.initFormElement()
        self.page_file_staff.initBasicModalElement()
        self.page_file_staff.clearForm()

        staff_code = self.page_file_staff.data.staff_code["valid"]
        self.page_file_staff.text_staff_code.send_keys(staff_code)
        self.page_file_staff.text_staff_name.send_keys(self.page_file_staff.data.staff_name["invalid"])
        self.page_file_staff.text_staff_address.send_keys(self.page_file_staff.data.staff_address["valid"])
        self.page_file_staff.text_staff_phone.send_keys(self.page_file_staff.data.staff_phone["valid"])
        self.page_file_staff.selectOptionInsideModal(2, self.page_file_staff.data.branch_name["valid"])
        self.page_file_staff.modal_button_ok.click()
        #self.page_file_staff.waitDimDisappeared()
        self.assertTrue(self.page_file_staff.isFormErrorDisplayed(), "Form error should be displayed")

        time.sleep(self.sleep)



    # ========================= TEST CASE AG008 - FILE EDIT SUCCESS ================================

    def test_ag008_001_data_file_employee_edit_success(self):
        self.page_file_staff.navigateFileStaff()
        self.page_file_staff.initPageElement()
        if not self.temp_row:
            row1 = 2
        else:
            row1 = self.temp_row[0]
        edit_button = self.page_file_staff.getNthRowObject(row1, 'edit')
        edit_button.click()
        self.page_file_staff.initFormElement()
        edit_staff_name_data = self.page_file_staff.data.staff_name["valid2"]
        self.page_file_staff.text_staff_name.clear()
        self.page_file_staff.text_staff_name.send_keys(edit_staff_name_data)

        self.page_file_staff.initConfirmBoxEdit()
        self.page_file_staff.modal_button_ok.click()

        self.page_file_staff.waitDimDisappeared()
        search_result = self.page_file_staff.searchText(3, 5, edit_staff_name_data)
        self.assertTrue(search_result > 0, "Could not find added data in Branch table")
        time.sleep(self.sleep)

    def test_ag008_002_data_file_employee_edit_success(self):
        self.page_file_staff.navigateFileStaff()
        self.page_file_staff.initPageElement()
        if not self.temp_row:
            row1 = 2
        else:
            row1 = self.temp_row[0]
        edit_button = self.page_file_staff.getNthRowObject(row1, 'edit')
        edit_button.click()
        self.page_file_staff.initFormElement()

        #self.page_file_staff.text_staff_phone.clear()
        address = self.page_file_staff.data.staff_address["valid3"]
        phone = self.page_file_staff.data.staff_phone["valid3"]

        self.page_file_staff.text_staff_address.clear()
        self.page_file_staff.text_staff_address.send_keys(address)
        self.page_file_staff.text_staff_phone.clear()
        self.page_file_staff.text_staff_phone.send_keys(phone)


        self.page_file_staff.initConfirmBoxEdit()
        self.page_file_staff.modal_button_ok.click()

        self.page_file_staff.waitDimDisappeared()
        search_result = self.page_file_staff.searchText(5, 5, phone)
        self.assertTrue(search_result > 0, "Could not find added data in Branch table")
        time.sleep(self.sleep)


    # ========================= TEST CASE AG009 - FILE EDIT FAIL ================================
    def test_ag009_001_data_file_employee_edit_fail(self):
        self.page_file_staff.navigateFileStaff()
        self.page_file_staff.initPageElement()
        if not self.temp_row:
            row1 = 2
        else:
            row1 = self.temp_row[0]
        edit_button = self.page_file_staff.getNthRowObject(row1, 'edit')
        edit_button.click()
        self.page_file_staff.initFormElement()
        self.page_file_staff.text_staff_name.clear()
        self.page_file_staff.initConfirmBoxEdit()
        self.page_file_staff.modal_button_ok.click()
        self.page_file_staff.waitDimDisappeared()
        self.assertTrue(self.page_file_staff.isFormErrorDisplayed(), "Form error should be displayed")
        time.sleep(self.sleep)



    def test_ag009_002_data_file_employee_edit_fail(self):
        self.page_file_staff.navigateFileStaff()
        self.page_file_staff.initPageElement()
        if not self.temp_row:
            row1 = 2
        else:
            row1 = self.temp_row[0]
        edit_button = self.page_file_staff.getNthRowObject(row1, 'edit')
        edit_button.click()
        self.page_file_staff.initFormElement()
        self.page_file_staff.text_staff_name.clear()
        self.page_file_staff.text_staff_name.send_keys(self.page_file_staff.data.staff_name["invalid"])
        self.page_file_staff.initConfirmBoxEdit()
        self.page_file_staff.modal_button_ok.click()
        self.page_file_staff.waitDimDisappeared()
        self.assertTrue(self.page_file_staff.isFormErrorDisplayed(), "Form error should be displayed")
        time.sleep(self.sleep)

    '''

    # ========================= TEST CASE AG010
    def test_ag010_001_data_file_employee_add_success(self):
        self.page_file_staff.navigateFileStaff()
        self.page_file_staff.initPageElement()
        data_staff_code = self.temp_data[0]
        search_result = self.page_file_staff.searchText(1, 5, data_staff_code)

        self.assertTrue(search_result > 0, "removed data still appear in Branch table")
        row1 = search_result + 1
        delete_button = self.page_file_staff.getNthRowObject(row1, 'delete')
        delete_button.click()
        self.page_file_staff.initConfirmBoxDelete()
        self.page_file_staff.confirm_button_ok.click()
        time.sleep(self.short_wait)
        search_result = self.page_file_staff.searchText(1, 5, data_staff_code)

        self.assertTrue(search_result == 0, "removed data still appear in Branch table")

        data_staff_code = self.temp_data[1]
        search_result = self.page_file_staff.searchText(1, 5, data_staff_code)

        self.assertTrue(search_result > 0, "removed data still appear in Branch table")
        row2 = search_result + 1
        delete_button = self.page_file_staff.getNthRowObject(row2, 'delete')
        delete_button.click()
        self.page_file_staff.initConfirmBoxDelete()
        self.page_file_staff.confirm_button_ok.click()
        time.sleep(self.short_wait)
        search_result = self.page_file_staff.searchText(1, 3, data_staff_code)
        self.assertTrue(search_result == 0, "removed data still appear in Branch table")
        time.sleep(self.sleep)
