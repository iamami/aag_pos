# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import unittest
from selenium import webdriver
import time
from subprocess import PIPE,Popen
import shlex
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

# Base of test function
class BaseTest(unittest.TestCase):
    sleep = 4
    short_wait = 1
    class_sleep = 10

    conn_db_host = "10.11.3.9"
    conn_db_name = "postgres"
    conn_db_password = "112233"
    conn_db_user = "aagold"

    '''
    bak_db_old_name
    bak_db_new_name

    local_file_name
    '''



    @classmethod
    def setUpClass(self):


        conn = psycopg2.connect("dbname='postgres' user='aagold' host='10.11.3.9' password='112233'")

        cur = conn.cursor()
        cur.execute("""ALTER DATABASE aagold RENAME TO _aagold""")

        conn.commit()
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        cur.execute("""CREATE DATABASE aagold""")
        conn.close()

        host_name = '10.11.3.9'
        database_name = 'aagold'
        user_name = 'aagold'
        database_password = '112233'
        command = 'pg_restore -h {0} -d {1} -U {2} /home/nites/testing/aagold/aag_test_case/data/aagold3.backup'.format(host_name,database_name,user_name)
        command = shlex.split(command)
        p = Popen(command,shell=False,stdin=PIPE,stdout=PIPE,stderr=PIPE)
        ret = p.communicate('{}\n'.format(database_password))
        #time.sleep(10)
        return ret

    @classmethod
    def tearDownClass(self):
        conn = psycopg2.connect("dbname='postgres' user='aagold' host='10.11.3.9' password='112233'")
        cur = conn.cursor()
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        cur.execute("""DROP DATABASE aagold""")
        cur.execute("""ALTER DATABASE _aagold RENAME TO aagold""")
        conn.close()


    def setUp(self):
        self.browser = webdriver.Firefox()
        self.ajax_wait = 10


    def tearDown(self):
        time.sleep(3)
        self.browser.close()

    '''
    def renameDatabase(self):
        conn = psycopg2.connect("dbname='postgres' user='aagold' host='10.11.3.9' password='112233'")
        cur = conn.cursor()
        cur.execute("""ALTER DATABASE aagold RENAME TO _aagold""")
        conn.commit()
        conn.close()

    def renameBackDatabase(self):
        conn = psycopg2.connect("dbname='postgres' user='aagold' host='10.11.3.9' password='112233'")
        cur = conn.cursor()
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        cur.execute("""DROP DATABASE aagold""")
        cur.execute("""ALTER DATABASE _aagold RENAME TO aagold""")
        conn.close()

    def restore_table(self):
        conn = psycopg2.connect("dbname='postgres' user='aagold' host='10.11.3.9' password='112233'")
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        cur = conn.cursor()
        cur.execute("""CREATE DATABASE aagold""")
        conn.close()
        host_name = '10.11.3.9'
        database_name = 'aagold'
        user_name = 'aagold'
        command = 'pg_restore -h {0} -d {1} -U {2} /home/nites/testing/aagold/aag_test_case/data/aagold3.backup'.format(host_name,database_name,user_name)
        command = shlex.split(command)
        p = Popen(command,shell=False,stdin=PIPE,stdout=PIPE,stderr=PIPE)
        return p.communicate('{}\n'.format(database_password))
    '''
