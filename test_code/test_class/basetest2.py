# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import unittest
from selenium import webdriver
import time
from subprocess import PIPE,Popen
import shlex
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

# Base of test function
class BaseTest2(unittest.TestCase):
    sleep = 4
    short_wait = 1


    @classmethod
    def setUpClass(self):
        self.browser = webdriver.Firefox()
        self.ajax_wait = 10
        self.page_file_user = FileUser(self.browser)
        self.page_file_user.loadPage()
        self.page_file_user.login()

    @classmethod
    def tearDownClass(self):
        time.sleep(1)
        self.browser.close()




    def renameDatabae(self):
        try:
            conn = psycopg2.connect("dbname='posgres' user='aagold' host='10.11.3.9' password='112233'")
        except:
            print "I am unable to connect to the database"
        cur = conn.cursor()
        cur.execute("""ALTER DATABASE aagold RENAME TO _aagold""")
        conn.commit()
        conn.close()

    def renameBackDatabae(self):
        try:
            conn = psycopg2.connect("dbname='posgres' user='aagold' host='10.11.3.9' password='112233'")
        except:
            print "I am unable to connect to the database"
        cur = conn.cursor()
        cur.execute("""ALTER DATABASE _aagold RENAME TO aagold""")
        conn.commit()
        conn.close()


    def restore_table(host_name,database_name,user_name,database_password):

        try:
            conn = psycopg2.connect("dbname='posgres' user='aagold' host='10.11.3.9' password='112233'")
        except:
            print "I am unable to connect to the database"

        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        cur = conn.cursor()
        cur.execute("""CREATE DATABASE aagold""")
        conn.close()


        #Remove the '<' from the pg_restore command.
        command = 'pg_restore -h {0} -d {1} -U {2} /home/nites/testing/aagold/aag_test_case/data/aagold3.backup'.format(host_name,database_name,user_name)

        #Use shlex to use a list of parameters in Popen instead of using the
        #command as is.
        command = shlex.split(command)

        #Let the shell out of this (i.e. shell=False)
        p = Popen(command,shell=False,stdin=PIPE,stdout=PIPE,stderr=PIPE)

        return p.communicate('{}\n'.format(database_password))
