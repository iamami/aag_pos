# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import unittest


# Create your tests here.
#from selenium.common.exceptions import *

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import *
from selenium.webdriver.chrome.options import Options
import time

from test_class.basetest import BaseTest
from test_class.utility import TextboxUtility
from page_object.file.file_branch import FileBranch
from page_object.file.file_staff import FileStaff
from page_object.textfield_spec import *
from page_object.data import StaffData, BranchData, TestErrorDescription, ErrorMessage


class TestFileBranch(BaseTest):

    page_file_branch = None #Page Object

    browser = None
    temp_row = []
    temp_data = []
    temp_duplicate = None
    text_file_branch_branchcode = None
    text_file_branch_branchname = None
    text_file_branch_branchnote = None
    search_key = 0


    def setUp(self): #test class initialize
        super(TestFileBranch, self).setUp()
        #page1 setup
        self.page_file_branch = FileBranch(self.browser)
        self.page_file_branch.loadPage()
        self.page_file_branch.login()
        self.page_file_branch.navigateFileBranch()
        self.page_file_branch.initPageElement()
        self.page_file_branch.button_add.click()
        self.page_file_branch.initFormElement()
        self.page_file_branch.initBasicModalElement()

        self.text_file_branch_branchcode_spec = File_Branch_BranchCode()
        self.text_file_branch_branchname_spec = File_Branch_BranchName()
        self.text_file_branch_branchnote_spec = File_Branch_BranchNote()

        self.text_file_branch_branchcode = TextboxUtility(self.browser, self.page_file_branch.text_branch_code, self.page_file_branch.modal_button_ok, self.text_file_branch_branchcode_spec)
        self.text_file_branch_branchname = TextboxUtility(self.browser, self.page_file_branch.text_branch_name, self.page_file_branch.modal_button_ok, self.text_file_branch_branchname_spec)
        self.text_file_branch_branchnote = TextboxUtility(self.browser, self.page_file_branch.text_branch_note, self.page_file_branch.modal_button_ok, self.text_file_branch_branchnote_spec)

        self.search_key = self.text_file_branch_branchcode.fillValidData()
        self.text_file_branch_branchname.fillValidData()
        self.text_file_branch_branchnote.fillValidData()
        self.validation_input1_name = "textbox branch code "
        self.validation_input2_name = "textbox branch name "
        self.validation_input3_name = "textbox branch note "



    '''
    # ========================= textbox branch code validation ================================
    '''

    def test_ag001_001_text_branch_code_valid(self):
        self.page_file_branch.modal_button_ok.click()
        ret = self.text_file_branch_branchcode.hasTableCellContain(self.search_key)
        self.assertTrue(ret,  self.validation_input1_name + " : " + TestErrorDescription.temp_method_error_message)

    def test_ag001_002_text_branch_code_invalid_below_min(self):
        data = "000"
        #self.assertTrue(self.text_file_branch_branchcode.isValidDataPass(self.search_key), "Valid data should completely insert record but it doesn't")
        self.page_file_branch.text_branch_code.clear()
        self.page_file_branch.text_branch_code.send_keys(data)
        self.page_file_branch.modal_button_ok.click()

        ret = self.text_file_branch_branchcode.hasFormError(ErrorMessage.textbox_min_val_error)
        self.assertTrue(ret,  self.validation_input1_name +" : " + "not work properly on BELOW MIN condition")



    def test_ag001_003_text_branch_code_invalid_alphabet(self):
        ret = self.text_file_branch_branchcode.checkAlphabetCondition()
        self.assertTrue(ret, self.validation_input1_name +" : " + TestErrorDescription.temp_method_error_message)


    def test_ag001_004_text_branch_code_invalid_over_max_length(self):
        ret = self.text_file_branch_branchcode.checkLengthOverMaxCondition()
        self.assertTrue(ret, self.validation_input1_name +" : " + TestErrorDescription.temp_method_error_message)



    def test_ag001_005_text_branch_code_invalid_below_min_length(self):
        ret = self.text_file_branch_branchcode.checkLengthBelowMinCondition()
        self.assertTrue(ret, self.validation_input1_name +" : " + TestErrorDescription.temp_method_error_message)




    def test_ag001_006_text_branch_code_invalid_special_char(self):
        ret = self.text_file_branch_branchcode.checkProhibitedSpecialChar()
        self.assertTrue(ret, self.validation_input1_name +" : " + TestErrorDescription.temp_method_error_message)


    def test_ag001_007_text_branch_code_invalid_empty(self):
        ret = self.text_file_branch_branchcode.checkEmptyCondition()
        self.assertTrue(ret, self.validation_input1_name +" : " + TestErrorDescription.temp_method_error_message)


    def test_ag001_008_text_branch_code_invalid_foreign_char(self):
        ret = self.text_file_branch_branchcode.checkForeignChar()
        self.assertTrue(ret, self.validation_input1_name +" : " + TestErrorDescription.temp_method_error_message)



    def test_ag001_009_text_branch_code_invalid_space_bar(self):
        ret = self.text_file_branch_branchcode.checkSpaceBarCondition()
        self.assertTrue(ret, self.validation_input1_name +" : " + TestErrorDescription.temp_method_error_message)



    def test_ag001_010_text_branch_code_duplicate_branch_code(self):
        ret = self.text_file_branch_branchcode.checkDuplicateCondition('001')
        self.assertTrue(ret, self.validation_input1_name +" : " + TestErrorDescription.temp_method_error_message)




    def test_ag001_011_text_branch_name_valid(self):
        data = "สาขาเพื่อทดสอบ (1/9-33,จ )" #new data to check
        self.page_file_branch.text_branch_name.clear()
        self.page_file_branch.text_branch_name.send_keys(data)

        self.page_file_branch.modal_button_ok.click()
        ret = self.text_file_branch_branchname.hasTableCellContain(data)
        self.assertTrue(ret,  self.validation_input2_name + " : " + TestErrorDescription.temp_method_error_message)
        #time.sleep(10)




    def test_ag001_012_text_branch_name_min_length(self):
        ret = self.text_file_branch_branchname.checkLengthMinCondition()
        self.assertTrue(ret, self.validation_input2_name +" : " + TestErrorDescription.temp_method_error_message)

    def test_ag001_013_text_branch_name_max_length(self):
        ret = self.text_file_branch_branchname.checkLengthMaxCondition()
        self.assertTrue(ret, self.validation_input2_name +" : " + TestErrorDescription.temp_method_error_message)


    def test_ag001_014_text_branch_name_over_max_length(self):
        ret = self.text_file_branch_branchname.checkLengthOverMaxCondition()
        self.assertTrue(ret, self.validation_input2_name +" : " + TestErrorDescription.temp_method_error_message)

    def test_ag001_014_text_branch_name_invalid_include_with_special_character(self):
        ret = self.text_file_branch_branchname.checkProhibitedSpecialChar()
        self.assertTrue(ret, self.validation_input2_name +" : " + TestErrorDescription.temp_method_error_message)

    def test_ag001_016_text_branch_name_invalid_empty(self):
        ret = self.text_file_branch_branchname.checkEmptyCondition()
        self.assertTrue(ret, self.validation_input2_name +" : " + TestErrorDescription.temp_method_error_message)

    def test_ag001_017_text_branch_name_duplicate(self):
        ret = self.text_file_branch_branchname.checkDuplicateCondition("สำนักงานใหญ่")
        self.assertTrue(ret, self.validation_input2_name +" : " + TestErrorDescription.temp_method_error_message)



    def test_ag001_018_text_note_valid(self):
        data = "!#$%&'()*+,-./:;<=>?@[\]^_`{|}~ ทดสอบ Test" #new data to check
        self.page_file_branch.text_branch_note.clear()
        self.page_file_branch.text_branch_note.send_keys(data)

        self.page_file_branch.modal_button_ok.click()
        ret = self.text_file_branch_branchnote.hasTableCellContain(data)
        self.assertTrue(ret,  self.validation_input3_name + " : " + TestErrorDescription.temp_method_error_message)

    def test_ag001_019_text_note_valid_empty(self):
        ret = self.text_file_branch_branchnote.checkEmptyCondition('')
        self.assertTrue(ret, self.validation_input3_name +" : " + TestErrorDescription.temp_method_error_message)



    def test_ag001_020_text_note_valid_min_length(self):
        ret = self.text_file_branch_branchnote.checkLengthMinCondition()
        self.assertTrue(ret, self.validation_input3_name +" : " + TestErrorDescription.temp_method_error_message)

    def test_ag001_021_text_note_valid_max_length(self):
        ret = self.text_file_branch_branchnote.checkLengthMaxCondition()
        self.assertTrue(ret, self.validation_input3_name +" : " + TestErrorDescription.temp_method_error_message)

    def test_ag001_022_text_note_invalid_long_over_max_length(self):
        ret = self.text_file_branch_branchnote.checkLengthOverMaxCondition()
        self.assertTrue(ret, self.validation_input3_name +" : " + TestErrorDescription.temp_method_error_message)


    def test_ag001_023_button_auto_generate_branch_code(self):
        self.page_file_branch.button_generate_branch_code.click()
        value = self.page_file_branch.text_branch_code.get_attribute('value')
        value = str(value)
        self.assertEqual(len(value), 3, self.validation_input1_name +" length is not valid")
        attr1 = self.page_file_branch.text_branch_code.get_attribute('disabled')
        self.assertEqual(attr1, "disabled", self.validation_input1_name + " should be disabled")
        attr2 = self.page_file_branch.button_generate_branch_code.get_attribute('disabled')
        self.assertEqual(attr2, "disabled", self.validation_input1_name + " should be disabled")



class TestFileBranchEdit(BaseTest):

    page_file_branch = None #Page Object

    browser = None
    temp_row = []
    temp_data = []
    temp_duplicate = None
    text_file_branch_branchcode = None
    text_file_branch_branchname = None
    text_file_branch_branchnote = None
    search_key = 0


    def setUp(self): #test class initialize
        super(TestFileBranchEdit, self).setUp()
        #page1 setup
        self.page_file_branch = FileBranch(self.browser)
        self.page_file_branch.loadPage()
        self.page_file_branch.login()
        self.page_file_branch.navigateFileBranch()
        self.page_file_branch.initPageElement()

        row = self.page_file_branch.searchText2(1, '003')
        edit_icon = self.page_file_branch.getNthRowObject(row, 'edit')
        edit_icon.click()
        self.page_file_branch.initFormElement()
        self.page_file_branch.initBasicModalElement()

        self.text_file_branch_branchcode_spec = File_Branch_BranchCode()
        self.text_file_branch_branchname_spec = File_Branch_BranchName()
        self.text_file_branch_branchnote_spec = File_Branch_BranchNote()

        self.text_file_branch_branchcode = TextboxUtility(self.browser, self.page_file_branch.text_branch_code, self.page_file_branch.modal_button_ok, self.text_file_branch_branchcode_spec)
        self.text_file_branch_branchname = TextboxUtility(self.browser, self.page_file_branch.text_branch_name, self.page_file_branch.modal_button_ok, self.text_file_branch_branchname_spec)
        self.text_file_branch_branchnote = TextboxUtility(self.browser, self.page_file_branch.text_branch_note, self.page_file_branch.modal_button_ok, self.text_file_branch_branchnote_spec)

        self.validation_input1_name = "textbox branch code "
        self.validation_input2_name = "textbox branch name "
        self.validation_input3_name = "textbox branch note "

    def test_ag002_001_text_branch_code_disabled_status(self):
        dis_status = self.page_file_branch.text_branch_code.get_attribute('disabled')
        self.assertEqual(dis_status, "disabled", self.validation_input1_name + " should be disabled but it is not.")


    
    def test_ag002_002_branch_name_edit_valid(self):
        self.text_file_branch_branchname.is_edit = True
        data = "สาขาเพื่อทดสอบ2 (1/9-33,จ )"
        ret = self.text_file_branch_branchname.check(data, 'success')
        self.assertTrue(ret, self.validation_input2_name +" : " + TestErrorDescription.temp_method_error_message)

    def test_ag002_003_branch_name_edit_valid_min(self):
        self.text_file_branch_branchname.is_edit = True
        ret = self.text_file_branch_branchname.checkLengthMinCondition()
        self.assertTrue(ret, self.validation_input2_name +" : " + TestErrorDescription.temp_method_error_message)

    def test_ag002_004_branch_name_edit_valid_max(self):
        self.text_file_branch_branchname.is_edit = True
        ret = self.text_file_branch_branchname.checkLengthMaxCondition()
        self.assertTrue(ret, self.validation_input2_name +" : " + TestErrorDescription.temp_method_error_message)

    def test_ag002_005_branch_name_edit_invalid_over_max(self):
        self.text_file_branch_branchname.is_edit = True
        ret = self.text_file_branch_branchname.checkLengthOverMaxCondition()
        self.assertTrue(ret, self.validation_input2_name +" : " + TestErrorDescription.temp_method_error_message)

    def test_ag002_006_branch_name_edit_invalid_prohibit_special_char(self):
        self.text_file_branch_branchname.is_edit = True
        ret = self.text_file_branch_branchname.checkProhibitedSpecialChar()
        self.assertTrue(ret, self.validation_input2_name +" : " + TestErrorDescription.temp_method_error_message)

    def test_ag002_007_branch_name_edit_invalid_empty(self):
        self.text_file_branch_branchname.is_edit = True
        ret = self.text_file_branch_branchname.checkEmptyCondition('')
        self.assertTrue(ret, self.validation_input2_name +" : " + TestErrorDescription.temp_method_error_message)



    def test_ag002_008_text_note_valid(self):
        self.text_file_branch_branchnote.is_edit = True
        data = "!#$%&'()*+,-./:;<=>?@[\]^_`{|}~ทดสอบ Test"
        ret = self.text_file_branch_branchnote.check(data, 'success')
        self.assertTrue(ret, self.validation_input3_name +" : " + TestErrorDescription.temp_method_error_message)

    def test_ag002_009_text_note_valid_empty(self):
        self.text_file_branch_branchnote.is_edit = True
        ret = self.text_file_branch_branchnote.checkEmptyCondition('')
        self.assertTrue(ret, self.validation_input3_name +" : " + TestErrorDescription.temp_method_error_message)

    def test_ag002_010_text_note_valid_min_length(self):
        self.text_file_branch_branchnote.is_edit = True
        ret = self.text_file_branch_branchnote.checkLengthMinCondition()
        self.assertTrue(ret, self.validation_input3_name +" : " + TestErrorDescription.temp_method_error_message)

    def test_ag002_011_text_note_valid_max_length(self):
        self.text_file_branch_branchnote.is_edit = True
        ret = self.text_file_branch_branchnote.checkLengthMaxCondition()
        self.assertTrue(ret, self.validation_input3_name +" : " + TestErrorDescription.temp_method_error_message)

    def test_ag002_012_text_note_invalid_long_over_max_length(self):
        self.text_file_branch_branchnote.is_edit = True
        ret = self.text_file_branch_branchnote.checkLengthOverMaxCondition()
        self.assertTrue(ret, self.validation_input3_name +" : " + TestErrorDescription.temp_method_error_message)
        time.sleep(10)
