# -*- coding: utf-8 -*-
'''
== Test case ==
include all page and design test case base on page object


override driver method thai "find element" and take wait inside as optional for Ajax Wait.
'''
from __future__ import unicode_literals

import unittest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import *
from selenium.webdriver.chrome.options import Options

import time
import ast

from test_class.test_file_branch import TestFileBranch, TestFileBranchEdit
#from test_class.test_file_staff import TestFileStaff
#from test_class.test_file_user import TestFileUser
#from test_class.test_file_product_group import TestFileProductGroup
#from test_class.test_test import TestTest

if __name__ == "__main__":
    unittest.main()
