*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบAdminและinit_data  
Test Setup        เข้าหน้าแฟ้มข้อมูล
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data_admin
*** Variables ***

*** Test cases ***
ตั้งค่าพนักงาน
    Sleep                               2
    Wait Until Page Contains Element    //*[@id="setting_branch"]                               300
    Sleep                               3
    Click Element                       //*[@id="setting_branch"]
    Sleep                               2
    Input Text                          id:IDstaffstart                                                                                                    12
    Sleep                               2
    Wait Until Page Contains Element    id:btnsave_setting                              300
    Sleep                               3
    Click Element                       id:btnsave_setting
    Sleep                               1
    ตรวจพบข้อความบน Alert               บันทึกสำเร็จ
    Sleep                               2

Test1 ทดสอบการเพิ่มพนักงานไม่สำเร็จเมื่อกดปุ่มกากบาท
    เข้าหน้าพนักงาน
    Sleep                               3
    กดสร้างพนักงาน
    Sleep                               1
    เลือกสาขาพนักงาน
    Sleep                               1
    กดปุ่มกากบาทหน้าสร้างพนักงาน
    Sleep                               3
    ค้นหาพนักงานด้วยรหัสพนักงาน         A012
    Sleep                               3
    กดปุ่มค้นหาพนักงาน
    Sleep                               3
    ไม่พบข้อความ                        A012

Test2 ทดสอบการเพิ่มพนักงานไม่สำเร็จเมื่อกดปุ่มยกเลิก
    เข้าหน้าพนักงาน
    Sleep                               3
    กดสร้างพนักงาน
    Sleep                               1
    เลือกสาขาพนักงาน
    Sleep                               1
    กดปุ่มยกเลิกสร้างพนักงาน
    Sleep                               3
    ค้นหาพนักงานด้วยรหัสพนักงาน         A012
    Sleep                               3
    กดปุ่มค้นหาพนักงาน
    Sleep                               3
    ไม่พบข้อความ                        A012

Test3 ทดสอบการเพิ่มพนักงานไม่สำเร็จเมื่อไม่เลือกสาขา
    เข้าหน้าพนักงาน
    Sleep                               3
    กดสร้างพนักงาน
    Sleep                               2
    กดปุ่มบันทึกสร้างพนักงาน
    Sleep                               2
    พบข้อความ                           This field may not be null.
    Sleep                               3
    กดปุ่มกากบาทหน้าสร้างพนักงาน

Test4 ทดสอบการเพิ่มพนักงานไม่สำเร็จเมื่อไม่กรอกชื่อพนักงาน
    เข้าหน้าพนักงาน
    Sleep                               2
    กดสร้างพนักงาน
    Sleep                               2
    กดปุ่มบันทึกสร้างพนักงาน
    Sleep                               2
    พบข้อความ                           *ชื่อพนักงาน *ต้องไม่เป็นค่าว่าง
    Sleep                               3
    กดปุ่มกากบาทหน้าสร้างพนักงาน

Test5 ทดสอบการเพิ่มพนักงานสำเร็จเมื่อไม่กรอกที่อยู่แล้วกดบันทึก
    เข้าหน้าพนักงาน
    Sleep                               2
    กดสร้างพนักงาน
    Sleep                               2
    เลือกสาขาพนักงาน
    Sleep                               2
    กรอกชื่อพนักงาน
    Sleep                               2
    กรอกเบอร์โทรศัพท์พนักงาน
    Sleep                               2
    กดปุ่มบันทึกสร้างพนักงาน
    Sleep                               5
    ค้นหาพนักงานด้วยรหัสพนักงาน         A012
    Sleep                               3
    กดปุ่มค้นหาพนักงาน
    Sleep                               3
    พบข้อความ                           A012
                # ไม่พบที่อยู่

Test6 ทดสอบการเพิ่มพนักงานสำเร็จเมื่อไม่กรอกเบอร์โทรศัพท์แล้วกดบันทึก
    เข้าหน้าพนักงาน
    Sleep                               1
    กดสร้างพนักงาน
    Sleep                               1
    เลือกสาขาพนักงาน
    Sleep                               1
    กรอกชื่อพนักงาน
    Sleep                               1
    กรอกที่อยู่พนักงาน
    Sleep                               1
    กดปุ่มบันทึกสร้างพนักงาน
    Sleep                               3
    ค้นหาพนักงานด้วยรหัสพนักงาน         A013
    Sleep                               3
    กดปุ่มค้นหาพนักงาน
    Sleep                               3
    พบข้อความ                           A013
                #ไม่พบเบอร์โทรศัพท์

Test7 ทดสอบการเพิ่มพนักงานสำเร็จเมื่อกรอกข้อมูลครบแล้วกดบันทึก
    เข้าหน้าพนักงาน
    Sleep                               1
    กดสร้างพนักงาน
    Sleep                               1
    เลือกสาขาพนักงาน
    Sleep                               1
    กรอกชื่อพนักงาน
    Sleep                               1
    กรอกที่อยู่พนักงาน
    Sleep                               1
    กรอกเบอร์โทรศัพท์พนักงาน
    Sleep                               1
    กดปุ่มบันทึกสร้างพนักงาน
    Sleep                               3
    ค้นหาพนักงานด้วยรหัสพนักงาน         A014
    Sleep                               3
    กดปุ่มค้นหาพนักงาน
    Sleep                               3
    พบข้อความ                           A014
                # พบทุกอย่าง

#ค้นหา
# Test8 ทดสอบการแสดงพนักงานเมื่อกดปุ่มทั้งหมด
#                 เข้าหน้าพนักงาน
#                 Sleep                                         2
#                 Wait Until Page Contains Element              id:allStaff
#                 Click Element                                 id:allStaff
#                 Sleep                                         5
#                 # พบพนักงานทั้งหมด
#                 keyword.พบข้อความ                             ลาดกระบัง
#                 keyword.พบข้อความ                             กิ่งแก้ว
#                 # ${count} =                                  Get Element Count                                                                                               id:table_idstaff
#                 # Should Be Equal                             ${count}                                                                                                        0

Test9 ทดสอบการแสดงพนักงานเมื่อกรอกรหัสพนักงาน
    เข้าหน้าพนักงาน
    Sleep                               2
    Input Text                          id:searchStaff                                  A001
    Sleep                               3                                                                                                  
    กดปุ่มค้นหาพนักงาน
                # พบพนักงาน
    Sleep                               3
    พบข้อความ                           ลาดกระบัง
    Sleep                               3
    ${count} =                          Get Element Count                                                                                                  id:table_idstaff
    ${count}=                           Convert To String                                                                                                  ${count}
    Should Be Equal                     ${count}                                                                                                           1

Test10 ทดสอบการแสดงพนักงานเมื่อเลือกสาขา
    เข้าหน้าพนักงาน
    Sleep                               2
    Wait Until Page Contains Element    //*[@id="searchBranch"]/i                                                                                            300
    Sleep                               3
    Click Element                       //*[@id="searchBranch"]/i
    Sleep                               2
    Wait Until Element Is Visible       //*[@id="searchBranch"]/div[2]/div[3]/span                                                                         300
    Sleep                               3
    Click Element                       //*[@id="searchBranch"]/div[2]/div[3]/span
    Sleep                               2
    กดปุ่มค้นหาพนักงาน
                # พบพนักงาน
    Sleep                               2
    ${count} =                          Get Element Count                                                                                                  id:table_idstaff
    ${count}=                           Convert To String                                                                                                  ${count}
    Should Be Equal                     ${count}                                                                                                           1

Test11 ทดสอบการแสดงพนักงานเมื่อกรอกชื่อพนักงานและเลือกสาขาตามที่พนักงานคนนั้นอยู่
    เข้าหน้าพนักงาน
    Sleep                               2
    Input Text                          id:searchStaff                                                                                                     A001
    Sleep                               2
    Wait Until Page Contains Element    //*[@id="searchBranch"]/i                                                                                          300
    Sleep                               3
    Click Element                       //*[@id="searchBranch"]/i
    Sleep                               2
    Wait Until Element Is Visible       //*[@id="searchBranch"]/div[2]/div[2]/span                                                                         300
    Sleep                               3
    Click Element                       //*[@id="searchBranch"]/div[2]/div[2]/span
    Sleep                               2
    กดปุ่มค้นหาพนักงาน
                # พบพนักงานทั้งหมด
    Sleep                               2
    ${count} =                          Get Element Count                                                                                                  id:table_idstaff
    ${count}=                           Convert To String                                                                                                  ${count}
    Should Be Equal                     ${count}                                                                                                           1

Test12 ทดสอบการแสดงพนักงานเมื่อกรอกชื่อพนักงานและเลือกสาขาตามที่พนักงานคนนั้นไม่ได้อยู่
    เข้าหน้าพนักงาน
    Sleep                               2
    Input Text                          id:searchStaff                                                                                                     A001
    Sleep                               2
    Wait Until Page Contains Element    //*[@id="searchBranch"]/i                                                                                          300
    Sleep                               3
    Click Element                       //*[@id="searchBranch"]/i
    Sleep                               2
    Wait Until Element Is Visible       //*[@id="searchBranch"]/div[2]/div[3]/span                                                                         300
    Sleep                               3
    Click Element                       //*[@id="searchBranch"]/div[2]/div[3]/span
    Sleep                               2
    กดปุ่มค้นหาพนักงาน
                # พบพนักงานทั้งหมด
    Sleep                               2
    ${count} =                          Get Element Count                                                                                                  id:table_idstaff
    ${count}=                           Convert To String                                                                                                  ${count}
    Should Be Equal                     ${count}                                                                                                           0

#แก้ไขพนักงาน
Test13 ทดสอบการแสดงข้อมูลตรงกับพนักงานที่ต้องการแก้ไข
    เข้าหน้าพนักงาน
    Sleep                               1
    ค้นหาพนักงานด้วยรหัสพนักงาน         A001
    Sleep                               3
    กดปุ่มค้นหาพนักงาน
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="content-body"]/div/div[2]/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]/i                        300
    Sleep                               3
    Click Element                       //*[@id="content-body"]/div/div[2]/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]/i
    Sleep                               3
    ${value}                            get value                                                                                                          //*[@id="IDstaff"]
    Should Be Equal                     A001                                                                                                               ${value}
    Sleep                               3
    กดปุ่มกากบาทหน้าสร้างพนักงาน

Test14 ทดสอบการไม่สามารถแก้ไขรหัสพนักงานได้
    เข้าหน้าพนักงาน
    Sleep                               1
    ค้นหาพนักงานด้วยรหัสพนักงาน         A001
    Sleep                               3
    กดปุ่มค้นหาพนักงาน
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="content-body"]/div/div[2]/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]/i                            300
    Sleep                               3
    Click Element                       //*[@id="content-body"]/div/div[2]/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]/i
    Sleep                               3
    ${value} =                          Get Element Attribute                                                                                              //*[@id="IDstaff"]      readOnly
    Sleep                               3
    กดปุ่มกากบาทหน้าสร้างพนักงาน

Test15 ทดสอบการแก้ไขพนักงานไม่สำเร็จเมื่อกดยกเลิก
    เข้าหน้าพนักงาน
    Sleep                               1
    ค้นหาพนักงานด้วยรหัสพนักงาน         A001
    Sleep                               3
    กดปุ่มค้นหาพนักงาน
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="content-body"]/div/div[2]/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]/i                    300
    Sleep                               3
    Click Element                       //*[@id="content-body"]/div/div[2]/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]/i
    Sleep                               3
    กรอกชื่อพนักงาน
    Sleep                               3
    กดปุ่มยกเลิกสร้างพนักงาน
    Sleep                               3
    ค้นหาพนักงานด้วยรหัสพนักงาน         A001
    Sleep                               3
    กดปุ่มค้นหาพนักงาน
    Sleep                               1
    ไม่พบข้อความ                        daniel

Test16 ทดสอบการแก้ไขพนักงานไม่สำเร็จเมื่อกดกากบาท
    เข้าหน้าพนักงาน
    Sleep                               1
    ค้นหาพนักงานด้วยรหัสพนักงาน         A001
    Sleep                               3
    กดปุ่มค้นหาพนักงาน
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="content-body"]/div/div[2]/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]/i                            300
    Sleep                               3
    Click Element                       //*[@id="content-body"]/div/div[2]/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]/i
    Sleep                               3
    กรอกชื่อพนักงาน
    Sleep                               3
    กดปุ่มกากบาทหน้าสร้างพนักงาน
    Sleep                               3
    ค้นหาพนักงานด้วยรหัสพนักงาน         A001
    Sleep                               3
    กดปุ่มค้นหาพนักงาน
    Sleep                               1
    ไม่พบข้อความ                        daniel

Test17 ทดสอบการแก้ไขพนักงานไม่สำเร็จเมื่อเลือกสาขาทั้งหมด
    เข้าหน้าพนักงาน
    Sleep                               1
    ค้นหาพนักงานด้วยรหัสพนักงาน         A001
    Sleep                               3
    กดปุ่มค้นหาพนักงาน
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="content-body"]/div/div[2]/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]/i                            300
    Sleep                               3
    Click Element                       //*[@id="content-body"]/div/div[2]/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]/i
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="branch"]/i                                                                                                300
    Sleep                               3
    Click Element                       //*[@id="branch"]/i
    Sleep                               2
    Wait Until Element Is Visible       //*[@id="branch"]/div[2]/div[1]/span                                                                               300
    Sleep                               3
    Click Element                       //*[@id="branch"]/div[2]/div[1]/span
    Sleep                               3
    กดปุ่มบันทึกสร้างพนักงาน
    Sleep                               1
    พบข้อความ                           *สาขา Invalid pk "0" - object does not exist.
    Sleep                               3
    กดปุ่มกากบาทหน้าสร้างพนักงาน

# Test18 ทดสอบการแก้ไขพนักงานไม่สำเร็จเมื่อแก้ชื่อพนักงานเป็นค่าว่าง
#                 เข้าหน้าพนักงาน
#                 Sleep                                         1
#                 ค้นหาพนักงานด้วยรหัสพนักงาน                   A001
#                 กดปุ่มค้นหาพนักงาน
#                 Sleep                                         3
#                 Wait Until Element Is Visible                 //*[@id="content-body"]/div/div[2]/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]/i
#                 Click Element                                 //*[@id="content-body"]/div/div[2]/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]/i
#                 Sleep                                         3
#                 Wait Until Element Is Visible                 //*[@id="nameStaff"]                                                                                            300
#                 Input Text                                    //*[@id="nameStaff"]                                                                                            ${EMPTY}
#                 Sleep                                         3
#                 กดปุ่มบันทึกสร้างพนักงาน
#                 Sleep                                         1
#                 พบข้อความ                                     *ชื่อพนักงาน *ต้องไม่เป็นค่าว่าง
#                 กดปุ่มกากบาทหน้าสร้างพนักงาน

Test19 ทดสอบการแก้ไขพนักงานสำเร็จเมื่อเปลี่ยนแปลงสาขาแล้วกดบันทึก
    เข้าหน้าพนักงาน
    Sleep                               1
    ค้นหาพนักงานด้วยรหัสพนักงาน         B001
    Sleep                               3
    กดปุ่มค้นหาพนักงาน
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="content-body"]/div/div[2]/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]/i                                300
    Sleep                               3
    Click Element                       //*[@id="content-body"]/div/div[2]/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]/i
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="branch"]/i                                                                                                300
    Sleep                               3
    Click Element                       //*[@id="branch"]/i
    Sleep                               2
    Wait Until Element Is Visible       //*[@id="branch"]/div[2]/div[2]/span                                                                               300
    Sleep                               3
    Click Element                       //*[@id="branch"]/div[2]/div[2]/span
    Sleep                               3
    ${name}=                            Get Value                                                                                                          //*[@id="nameStaff"]
    กดปุ่มบันทึกสร้างพนักงาน
    Sleep                               1
    ค้นหาพนักงานด้วยรหัสพนักงาน         ${name}
    Sleep                               3
    กดปุ่มค้นหาพนักงาน
    Sleep                               3
    พบข้อความ                           A015

Test20 ทดสอบการแก้ไขพนักงานสำเร็จเมื่อเปลี่ยนแปลงชื่อพนักงานแล้วกดบันทึก
    เข้าหน้าพนักงาน
    Sleep                               1
    ค้นหาพนักงานด้วยรหัสพนักงาน         A013
    Sleep                               3
    กดปุ่มค้นหาพนักงาน
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="content-body"]/div/div[2]/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]/i                            300
    Sleep                               3
    Click Element                       //*[@id="content-body"]/div/div[2]/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]/i
    Sleep                               3
    Input Text                          //*[@id="nameStaff"]                                                                                               john
    Sleep                               3
    กดปุ่มบันทึกสร้างพนักงาน
    Sleep                               1
    ค้นหาพนักงานด้วยรหัสพนักงาน         A013
    Sleep                               3
    กดปุ่มค้นหาพนักงาน
    Sleep                               3
    พบข้อความ                           john

Test21 ทดสอบการแก้ไขพนักงานสำเร็จเมื่อเปลี่ยนสถานะเป็นซ่อนแล้วกดบันทึก
    เข้าหน้าพนักงาน
    Sleep                               1
    ค้นหาพนักงานด้วยรหัสพนักงาน         A013
    Sleep                               3
    กดปุ่มค้นหาพนักงาน
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="content-body"]/div/div[2]/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]/i                            300
    Sleep                               3
    Click Element                       //*[@id="content-body"]/div/div[2]/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]/i
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="statusStaff"]/i                                                                                           300
    Sleep                               3
    Click Element                       //*[@id="statusStaff"]/i
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="statusStaff"]/div[2]/div[2]/span                                                                          300
    Sleep                               3
    Click Element                       //*[@id="statusStaff"]/div[2]/div[2]/span
    กดปุ่มบันทึกสร้างพนักงาน
    Sleep                               1
    ค้นหาพนักงานด้วยรหัสพนักงาน         A013
    Sleep                               3
    กดปุ่มค้นหาพนักงาน
    Sleep                               3
    ไม่พบข้อความ                        A013

Test22 ทดสอบการลบพนักงานไม่สำเร็จเมื่อกดCANCEL
    เข้าหน้าพนักงาน
    Sleep                               1
    ค้นหาพนักงานด้วยรหัสพนักงาน         A014
    Sleep                               3
    กดปุ่มค้นหาพนักงาน
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="content-body"]/div/div[2]/div[1]/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[2]/i                         300
    Sleep                               3
    Click Element                       //*[@id="content-body"]/div/div[2]/div[1]/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[2]/i
    Sleep                               3
    ตรวจพบข้อความบน Alertแล้วCancel     ยืนยันลบพนักงานนี้
    Sleep                               3
    ค้นหาพนักงานด้วยรหัสพนักงาน         A014
    Sleep                               3
    กดปุ่มค้นหาพนักงาน
    Sleep                               3
    พบข้อความ                           A014

Test23 ทดสอบการลบพนักงานไม่สำเร็จเมื่อกดOK
    เข้าหน้าพนักงาน
    Sleep                               1
    ค้นหาพนักงานด้วยรหัสพนักงาน         A014
    Sleep                               3
    กดปุ่มค้นหาพนักงาน
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="content-body"]/div/div[2]/div[1]/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[2]/i                         300
    Sleep                               3
    Click Element                       //*[@id="content-body"]/div/div[2]/div[1]/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[2]/i
    Sleep                               3
    ตรวจพบข้อความบน Alert               ยืนยันลบพนักงานนี้
    Sleep                               3
    ค้นหาพนักงานด้วยรหัสพนักงาน         A014
    Sleep                               3
    กดปุ่มค้นหาพนักงาน
    Sleep                               3
    ไม่พบข้อความ                        A014

*** Keywords ***
เข้าหน้าพนักงาน
    Wait Until Page Contains Element    id:staff                                                                                                           300
    Click Element                       id:staff

กดสร้างพนักงาน
    Wait Until Page Contains Element    //*[@id="addstaff"]                                                                                                300
    Click Element                       //*[@id="addstaff"]

กดปุ่มกากบาทหน้าสร้างพนักงาน
    Wait Until Element Is Visible       id:closeAddstaff                                                                                                   300
    Click Element                       id:closeAddstaff

กดปุ่มบันทึกสร้างพนักงาน
    Wait Until Element Is Visible       id:submitAddstaff                                                                                                  300
    Click Element                       id:submitAddstaff

กดปุ่มยกเลิกสร้างพนักงาน
    Wait Until Element Is Visible       id:cancelAddstaff                                                                                                  300
    Click Element                       id:cancelAddstaff

เลือกสาขาพนักงาน
    Sleep                               2
    Wait Until Element Is Visible       //*[@id="branch"]/i                                                                                                300
    Click Element                       //*[@id="branch"]/i
    Sleep                               2
    Wait Until Element Is Visible       //*[@id="branch"]/div[2]/div[2]/span                                                                               300
    Click Element                       //*[@id="branch"]/div[2]/div[2]/span

กรอกสาขาพนักงาน
    Wait Until Element Is Visible       //*[@id="branch"]/input                                                                                            300
    Input Text                          //*[@id="branch"]/input                                                                                            ลาดกระบัง

กรอกชื่อพนักงาน
    Wait Until Element Is Visible       //*[@id="nameStaff"]                                                                                               300
    Input Text                          //*[@id="nameStaff"]                                                                                               daniel

กรอกที่อยู่พนักงาน
    Wait Until Element Is Visible       //*[@id="addressStaff"]                                                                                            300
    Input Text                          //*[@id="addressStaff"]                                                                                            bangkok

กรอกเบอร์โทรศัพท์พนักงาน
    Wait Until Element Is Visible       //*[@id="phoneStaff"]                                                                                              300
    Input Text                          //*[@id="phoneStaff"]                                                                                              0888888888

กดปุ่มค้นหาพนักงาน
    Sleep                               2
    Wait Until Page Contains Element    id:btnsearchStaff                                                                                                  300
    Click Element                       id:btnsearchStaff

ค้นหาพนักงานด้วยรหัสพนักงาน
    [Arguments]                         ${value}
    Sleep                               2
    Input Text                          id:searchStaff                                                                                                     ${value}
    Sleep                               2
