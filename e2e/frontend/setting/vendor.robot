*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบAdminและinit_data  
Test Setup        เข้าหน้าแฟ้มข้อมูล
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data_admin
*** Variables ***

*** Test cases ***
Test1 ทดสอบการเพิ่มโรงงานไม่สำเร็จเมื่อกดปุ่มกากบาท
    เข้าหน้าโรงงาน
    กดปุ่มเพิ่มโรงงาน
    กรอกชื่อโรงงาน/ร้านส่ง                              vender2
    กรอกเบอร์โทร                                        0811111110
    กรอกที่อยู่                                         address
    กดปุ่มกากบาทโรงงาน
    Sleep                                               3
    พบจำนวนแถวโรงงาน                                    1
    ไม่พบข้อความ                                        vender2
    #ไม่พบรายการที่เพิ่มล่าสุด

Test2 ทดสอบการเพิ่มโรงงานไม่สำเร็จเมื่อกดปุ่มยกเลิก
    เข้าหน้าโรงงาน
    กดปุ่มเพิ่มโรงงาน
    กรอกชื่อโรงงาน/ร้านส่ง                              vender2
    กรอกเบอร์โทร                                        0811111110
    กรอกที่อยู่                                         address
    กดปุ่มยกเลิกโรงงาน
    Sleep                                               3
    พบจำนวนแถวโรงงาน                                    1
    ไม่พบข้อความ                                        vender2
    #ไม่พบรายการที่เพิ่มล่าสุด

Test3 ทดสอบการเพิ่มโรงงานไม่สำเร็จเมื่อไม่กรอกชื่อโรงงาน
    เข้าหน้าโรงงาน
    กดปุ่มเพิ่มโรงงาน
    กรอกเบอร์โทร                                        0811111110
    กรอกที่อยู่                                         address
    กดปุ่มบันทึกโรงงาน
    พบข้อความ                                           *ชื่อโรงงาน/ร้านส่ง *ต้องไม่เป็นค่าว่าง
    กดปุ่มกากบาทโรงงาน
    Sleep                                               3
    พบจำนวนแถวโรงงาน                                    1
    ไม่พบข้อความ                                        0811111110
    #พบข้อความ *ชื่อโรงงาน/ร้านส่ง *ต้องไม่เป็นค่าว่าง

Test4 ทดสอบการเพิ่มโรงงานสำเร็จเมื่อกรอกข้อมูลครบ
    เข้าหน้าโรงงาน
    กดปุ่มเพิ่มโรงงาน
    กรอกชื่อโรงงาน/ร้านส่ง                              vender2
    กรอกเบอร์โทร                                        0811111110
    กรอกที่อยู่                                         address
    กดปุ่มบันทึกโรงงาน
    Sleep                                               3
    #ไม่พบ popupโรงงาน
    พบจำนวนแถวโรงงาน                                    2
    พบข้อความ                                           vender2
    พบข้อความ                                           0811111110
    พบข้อความ                                           address
    #พบรายการโรงงานที่เพิ่มเข้ามา

Test5 ทดสอบการแก้ไขโรงงานไม่สำเร็จเมื่อกดปุ่มกากบาท
    เข้าหน้าโรงงาน
    กดปุ่มแก้ไขโรงงาน2
    กรอกชื่อโรงงาน/ร้านส่ง                              vender4
    กรอกเบอร์โทร                                        0811111114
    กรอกที่อยู่                                         address4
    กดปุ่มกากบาทโรงงาน
    Sleep                                               3
    พบจำนวนแถวโรงงาน                                    2
    พบข้อความ                                           vender2
    พบข้อความ                                           0811111110
    พบข้อความ                                           address
    #ไม่พบความเปลี่ยนแปลงของโรงงานที่ทำการแก้ไข

Test6 ทดสอบการแก้ไขโรงงานไม่สำเร็จเมื่อกดปุ่มยกเลิก
    เข้าหน้าโรงงาน
    กดปุ่มแก้ไขโรงงาน2
    กรอกชื่อโรงงาน/ร้านส่ง                              vender4
    กรอกเบอร์โทร                                        0811111114
    กรอกที่อยู่                                         address4
    กดปุ่มยกเลิกโรงงาน
    Sleep                                               3
    พบจำนวนแถวโรงงาน                                    2
    พบข้อความ                                           vender2
    พบข้อความ                                           0811111110
    พบข้อความ                                           address
    #ไม่พบความเปลี่ยนแปลงของโรงงานที่ทำการแก้ไข

# Test7 ทดสอบการแก้ไขโรงงานไม่สำเร็จเมื่อกรอกชื่อโรงงานเป็นค่าว่าง
#     เข้าหน้าโรงงาน
#     กดปุ่มแก้ไขโรงงาน2
#     กรอกชื่อโรงงาน/ร้านส่งด้วยค่าว่าง
#     กรอกเบอร์โทร                                        0811111114
#     กรอกที่อยู่                                         address4
#     กดปุ่มบันทึกโรงงาน
#     พบข้อความ                                           *ชื่อโรงงาน/ร้านส่ง *ต้องไม่เป็นค่าว่าง
#     #พบข้อความ *ชื่อโรงงาน/ร้านส่ง *ต้องไม่เป็นค่าว่าง
#     กดปุ่มกากบาทโรงงาน

Test8 ทดสอบการแก้ไขโรงงานสำเร็จเมื่อกรอกข้อมูลครบ
    เข้าหน้าโรงงาน
    กดปุ่มแก้ไขโรงงาน2
    กรอกชื่อโรงงาน/ร้านส่ง                              vender4
    กรอกเบอร์โทร                                        0811111114
    กรอกที่อยู่                                         address4
    กดปุ่มบันทึกโรงงาน
    Sleep                                               3
    #ไม่พบ popup โรงงาน
    พบจำนวนแถวโรงงาน                                    2
    พบข้อความ                                           vender4
    พบข้อความ                                           0811111114
    พบข้อความ                                           address4
    ไม่พบข้อความ                                        vender2
    ไม่พบข้อความ                                        0811111110
    ไม่พบข้อความ                                        address
    #พบความเปลี่ยนแปลงของโรงงานที่ทำการแก้ไข

Test9 ทดสอบการยกเลิกโรงงานไม่สำเร็จเมื่อปฏิเสธการยืนยันการลบ
    เข้าหน้าโรงงาน
    กดปุ่มลบโรงงาน2
    Sleep                                               5
    พบpopupยืนยันรายการลบ
    ปฏิเสธลบรายการโรงงาน
    พบจำนวนแถวโรงงาน                                    2
    พบข้อความ                                           vender4
    พบข้อความ                                           0811111114
    พบข้อความ                                           address4
    #รายการที่ต้องการลบยังคงอยู่

Test10 ทดสอบการยกเลิกโรงงานสำเร็จเมื่อตกลงการยืนยันการลบ
    เข้าหน้าโรงงาน
    กดปุ่มลบโรงงาน2
    Sleep                                               5
    พบpopupยืนยันรายการลบ
    ยืนยันลบรายการโรงงาน
    Sleep                                   3
    พบจำนวนแถวโรงงาน                                    1
    ไม่พบข้อความ                                           vender4
    ไม่พบข้อความ                                           0811111114
    ไม่พบข้อความ                                           address4
    #รายการที่ต้องการลบหายไป

*** Keywords ***
เข้าหน้าโรงงาน
    Sleep                                               2
    Wait Until Page Contains Element                    id:vendors                                        300
    Click Element                                       id:vendors

กดปุ่มเพิ่มโรงงาน
    Wait Until Page Contains Element                    id:btnAddVendor                                   300
    Sleep                                               1
    Click Element                                       id:btnAddVendor

กดปุ่มกากบาทโรงงาน
    Sleep                                               2
    Wait Until Page Contains Element                    id:btnCloseVendor                                 300
    Click Element                                       id:btnCloseVendor

กดปุ่มยกเลิกโรงงาน
    Sleep                                               2
    Wait Until Page Contains Element                    id:btnCancelVendor                                300
    Click Element                                       id:btnCancelVendor

กดปุ่มบันทึกโรงงาน
    Sleep                                               2
    Wait Until Page Contains Element                    id:btnSaveVendor                                  300
    Click Element                                       id:btnSaveVendor

กรอกชื่อโรงงาน/ร้านส่ง
    [Arguments]                                         ${value}
    Sleep                                               2
    Wait Until Element Is Visible                       id:inputVendorName
    Input Text                                          id:inputVendorName                                ${value}

กรอกชื่อโรงงาน/ร้านส่งด้วยค่าว่าง
    Wait Until Element Is Visible                       id:inputVendorName                                300
    Sleep                                               1
    Input Text                                          id:inputVendorName                                ${Empty}

กรอกเบอร์โทร
    [Arguments]                                         ${value}
    Sleep                                               2
    Wait Until Element Is Visible                       id:inputVendorTel
    Input Text                                          id:inputVendorTel                                 ${value}

กรอกที่อยู่
    [Arguments]                                         ${value}
    Sleep                                               2
    Wait Until Element Is Visible                       id:inputVendorAddr
    Input Text                                          id:inputVendorAddr                                ${value}

พบชื่อโรงงาน/ร้านส่ง
    [Arguments]                                         ${value}
    ${text} =                                           Get Value                                         id:inputVendorName
    Should Be Equal                                     ${text}                                           ${value}

พบเบอร์โทร
    [Arguments]                                         ${value}
    ${text} =                                           Get Value                                         id:inputVendorTel
    Should Be Equal                                     ${text}                                           ${value}

พบที่อยู่
    [Arguments]                                         ${value}
    ${text} =                                           Get Value                                         id:inputVendorAddr
    Should Be Equal                                     ${text}                                           ${value}

พบจำนวนแถวโรงงาน
    [Arguments]                                         ${value}
    ${count} =                                          Get Element Count                                 id:branchID
    ${count_str} =                                      Convert To String                                 ${count}
    Should Be Equal                                     ${count_str}                                      ${value}

กดปุ่มแก้ไขโรงงาน1
    Wait Until Page Contains Element                    id:0_btnEdit                                      300
    Sleep                                               1
    Click Element                                       id:0_btnEdit

กดปุ่มแก้ไขโรงงาน2
    Wait Until Page Contains Element                    id:1_btnEdit                                      300
    Sleep                                               1
    Click Element                                       id:1_btnEdit

กดปุ่มลบโรงงาน1
    Wait Until Page Contains Element                    id:0_btnDelete                                    300
    Sleep                                               1
    Click Element                                       id:0_btnDelete

กดปุ่มลบโรงงาน2
    Wait Until Page Contains Element                    id:1_btnDelete                                    300
    Sleep                                               1
    Click Element                                       id:1_btnDelete

พบpopupยืนยันรายการลบ
    Wait Until Element Is Visible                       //*[@id="modalConfirmDelete"]                     300
    Wait Until Element Is Visible                       //*[@id="modalConfirmDelete"]/div[2]/button[1]    300
    Wait Until Element Is Visible                       //*[@id="modalConfirmDelete"]/div[2]/button[2]    300

ยืนยันลบรายการโรงงาน
    Wait Until Element Is Visible                       //*[@id="modalConfirmDelete"]/div[2]/button[2]    300
    Click Element                       //*[@id="modalConfirmDelete"]/div[2]/button[2] 

ปฏิเสธลบรายการโรงงาน
    Wait Until Element Is Visible                       //*[@id="modalConfirmDelete"]/div[2]/button[1]    300
    Click Element                       //*[@id="modalConfirmDelete"]/div[2]/button[1] 