*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบAdminและinit_data 
Test Setup        เข้าหน้าแฟ้มข้อมูล
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data_admin
*** Variables ***

*** Test cases ***
# ค้นหาสินค้า
Test1 ทดสอบการแสดงรายการสินค้าเมื่อกดปุ่มทั้งหมด
    เข้าหน้าสินค้า
    Sleep                               3
    กดปุ่มเพิ่มสินค้า
    Sleep                               3
    เลือกกลุ่มสินค้า
    Sleep                               3
    เลือกประเภทสินค้า
    Sleep                               3
    กรอกน.น./ชิ้น (บาท)                 1
    Sleep                               3
    เจนรหัสสินค้า
    Sleep                               3
    กดยืนยันหน้าเพิ่มสินค้า
    Sleep                               3
    กดปุ่มค้นหาสินค้าทั้งหมด
    Sleep                               3
    ตรวจสอบจำนวนสินค้า                  2

Test2 ทดสอบการแสดงรายการสินค้าเมื่อเลือกกลุ่มสินค้าแล้วกดปุ่มค้นหา
    เข้าหน้าสินค้า
    Sleep                               3
    ค้นหาสินค้าด้วยกลุ่มสินค้า
    Sleep                               3
    กดปุ่มค้นหาสินค้า
    Sleep                               3
    พบข้อความ                           90
    Sleep                               3
    ไม่พบข้อความ                        96.5
    Sleep                               3
    ตรวจสอบจำนวนสินค้า                  1


Test3 ทดสอบการแสดงรายการสินค้าเมื่อเลือกประเภทสินค้าแล้วกดปุ่มค้นหา
    เข้าหน้าสินค้า
    Sleep                               3
    ค้นหาสินค้าด้วยประเภทสินค้า
    Sleep                               3
    ${value}=                           Get Text                                              //*[@id="searchproducttype"]/div[1]
    Sleep                               3
    พบข้อความ                           ${value}
    Sleep                               3
    กดปุ่มค้นหาสินค้า
    Sleep                               3
    ตรวจสอบจำนวนสินค้า                  2

Test4 ทดสอบการแสดงรายการสินค้าเมื่อเลือกรหัสสินค้าแล้วกดปุ่มค้นหา
    เข้าหน้าสินค้า
    Sleep                               3
    ค้นหาสินค้าด้วยรหัสสินค้า
    Sleep                               3
    ${value}=                           Get Text                                              //*[@id="searchproductid"]/div[1]
    Sleep                               3
    พบข้อความ                           ${value}
    Sleep                               3
    กดปุ่มค้นหาสินค้า
    Sleep                               3
    ตรวจสอบจำนวนสินค้า                  1

Test5 ทดสอบการแสดงรายการสินค้าเมื่อเลือกชื่อสินค้าแล้วกดปุ่มค้นหา
    เข้าหน้าสินค้า
    Sleep                               3
    ค้นหาสินค้าด้วยชื่อสินค้า
    Sleep                               3
    ${value}=                           Get Text                                              //*[@id="searchproductname"]/div[1]
    Sleep                               3
    พบข้อความ                           ${value}
    Sleep                               3
    กดปุ่มค้นหาสินค้า
    Sleep                               3
    ตรวจสอบจำนวนสินค้า                  1

Test6 ทดสอบการแสดงรหัสสินค้าถูกต้องเมื่อเลือกชื่อสินค้า
    เข้าหน้าสินค้า
    Sleep                               3
    ค้นหาสินค้าด้วยรหัสสินค้า
    Sleep                               3
    ${value}=                           Get Text                                              //*[@id="searchproductname"]/div[1]
    Should Be Equal                     ${value}                                              แหวน1บาท(90)

Test7 ทดสอบการแสดงรายการสินค้าเมื่อเลือกประเภทงานขายแล้วกดปุ่มค้นหา
    เข้าหน้าสินค้า
    Sleep                               3
    ค้นหาสินค้าด้วยชื่อประเภทงานขาย
    Sleep                               3
    ${value}=                           Get Text                                              //*[@id="searchproductsell"]/div[1]
    Sleep                               3
    พบข้อความ                           ${value}
    Sleep                               3
    กดปุ่มค้นหาสินค้า
    Sleep                               3
    ตรวจสอบจำนวนสินค้า                  2

Test8 ทดสอบการแสดงรายการสินค้าเมื่อเลือกน้ำหนัก บาทเป็น1/2สลึงแล้วกดปุ่มค้นหา
    เข้าหน้าสินค้า
    Sleep                               3
    ค้นหาสินค้าด้วยน้ำหนักบาท
    Sleep                               3
    ${weightg}                          get value                                             //*[@id="searchproductweightg"]
    Should Be Equal                     ${weightg}                                            15.2
    ${value}=                           Get Text                                              //*[@id="searchproductsell"]/div[1]
    Sleep                               3
    พบข้อความ                           ${value}
    Sleep                               3
    กดปุ่มค้นหาสินค้า
    Sleep                               3
    ตรวจสอบจำนวนสินค้า                  1

Test9 ทดสอบการแสดงรายการสินค้าเมื่อเลือกน้ำหนัก กรัมแล้วกดปุ่มค้นหา
    เข้าหน้าสินค้า
    Sleep                               3
    ค้นหาสินค้าด้วยน้ำหนักกรัม
    Sleep                               3
    ${value}=                           Get value                                             //*[@id="searchproductweightg"]
    Sleep                               3
    พบข้อความ                           ${value}
    Sleep                               3
    กดปุ่มค้นหาสินค้า
    Sleep                               3
    ตรวจสอบจำนวนสินค้า                  1

                # เพิ่มสินค้า

Test10 ทดสอบการเพิ่มสินค้าไม่สำเร็จเมื่อกดปุ่มกากบาท
    เข้าหน้าสินค้า
    Sleep                               3
    กดปุ่มเพิ่มสินค้า
    Sleep                               3
    เลือกกลุ่มสินค้า
    Sleep                               3
    เลือกประเภทสินค้า
    Sleep                               3
    กรอกน.น./ชิ้น (บาท)                 2
    Sleep                               3
    เจนรหัสสินค้า
    Sleep                               3
    กดกากบาทหน้าเพิ่มสินค้า
    Sleep                               3
    ไม่พบข้อความ                        R90B200

Test12 ทดสอบการไม่สามารถแก้ไขราคาป้ายชิ้นละ(บาท) เมื่อเลือกประเภทงานขายเป็นงานชั่ง
    เข้าหน้าสินค้า
    Sleep                               3
    กดปุ่มเพิ่มสินค้า
    Sleep                               3
    ${value} =                          Get Element Attribute                                 //*[@id="pricetag"]                    disabled
    Sleep                               3
    กดกากบาทหน้าเพิ่มสินค้า

Test13 ทดสอบการไม่สามารถแก้ไข*ต้นทุนชิ้นละ(บาท)ได้เมื่อเลือกประเภทงานขายเป็นงานชั่ง
    เข้าหน้าสินค้า
    Sleep                               3
    กดปุ่มเพิ่มสินค้า
    Sleep                               3
    ${value} =                          Get Element Attribute                                 //*[@id="cost"]                        disabled
    Sleep                               3
    กดกากบาทหน้าเพิ่มสินค้า

Test14 ทดสอบการสามารถแก้ไขราคาป้ายชิ้นละ(บาท) เมื่อเลือกประเภทงานขายเป็นงานชิ้น
    เข้าหน้าสินค้า
    Sleep                               3
    กดปุ่มเพิ่มสินค้า
    Sleep                               3
    เลือกประเภทงานขายเป็นงานชิ้น
    Sleep                               3
    กรอกราคาป้าย                        300
    Sleep                               3
    ${value}                            Get Value                                             //*[@id="pricetag"]
    Should Be Equal                     ${value}                                              300
    Sleep                               3
    กดกากบาทหน้าเพิ่มสินค้า

Test15 ทดสอบการเพิ่มสินค้าไม่สำเร็จเมื่อไม่เลือกกลุ่มสินค้า
    เข้าหน้าสินค้า
    Sleep                               3
    กดปุ่มเพิ่มสินค้า
    Sleep                               3
    เลือกประเภทงานขายเป็นงานชิ้น
    Sleep                               3
    กรอกราคาต้นทุน                      300
    Sleep                               3
    ${value}                            Get Value                                             //*[@id="cost"]
    Should Be Equal                     ${value}                                              300
    Sleep                               3
    กดกากบาทหน้าเพิ่มสินค้า

Test16 ทดสอบการสามารถแก้ไข*ต้นทุนชิ้นละ(บาท)ได้เมื่อเลือกประเภทงานขายเป็นงานชิ้น
    เข้าหน้าสินค้า
    Sleep                               3
    กดปุ่มเพิ่มสินค้า
    Sleep                               3
    เลือกประเภทสินค้า
    Sleep                               3
    กรอกน.น./ชิ้น (บาท)                 2
    Sleep                               3
    เจนรหัสสินค้า
    Sleep                               3
    กดยืนยันหน้าเพิ่มสินค้า
    Sleep                               3
    พบข้อความ                           *กลุ่มสินค้า*เลือกกลุ่มสินค้า
    Sleep                               3
    กดกากบาทหน้าเพิ่มสินค้า

Test17 ทดสอบการเพิ่มสินค้าไม่สำเร็จเมื่อไม่เลือกประเภทสินค้าสินค้า
    เข้าหน้าสินค้า
    Sleep                               3
    กดปุ่มเพิ่มสินค้า
    Sleep                               3
    เลือกกลุ่มสินค้า
    Sleep                               3
    กรอกน.น./ชิ้น (บาท)                 2
    Sleep                               3
    เจนรหัสสินค้า
    Sleep                               3
    กดยืนยันหน้าเพิ่มสินค้า
    Sleep                               3
    พบข้อความ                           *ประเภทสินค้า*เลือกประเภทสินค้า
    Sleep                               3
    กดกากบาทหน้าเพิ่มสินค้า

Test18 ทดสอบการเพิ่มสินค้าไม่สำเร็จเมื่อเลือกประเภทนน.เป็นบาท แล้วน.น./ชิ้น (บาท)* เป็นค่าว่าง
    เข้าหน้าสินค้า
    Sleep                               3
    กดปุ่มเพิ่มสินค้า
    Sleep                               3
    เลือกกลุ่มสินค้า
    Sleep                               3
    เลือกประเภทสินค้า
    Sleep                               3
    เจนรหัสสินค้า
    Sleep                               3
    กดยืนยันหน้าเพิ่มสินค้า
    Sleep                               3
    พบข้อความ                           *น.น./ชิ้น (บาท)*ต้องไม่เป็นค่าว่าง
    Sleep                               3
    กดกากบาทหน้าเพิ่มสินค้า

Test19 ทดสอบการเพิ่มสินค้าไม่สำเร็จเมื่อเลือกประเภทนน.เป็นกรัม แล้วน.น./ชิ้น (กรัม)* เป็นค่าว่าง
    เข้าหน้าสินค้า
    Sleep                               3
    กดปุ่มเพิ่มสินค้า
    Sleep                               3
    เลือกกลุ่มสินค้า
    Sleep                               3
    เลือกประเภทสินค้า
    Sleep                               3
    เลือกประเภทประเภทน.น.เป็นกรัม
    Sleep                               3
    เจนรหัสสินค้า
    Sleep                               3
    กดยืนยันหน้าเพิ่มสินค้า
    Sleep                               3
    พบข้อความ                           *น.น./ชิ้น (กรัม)*ต้องไม่เป็นค่าว่าง
    Sleep                               3
    กดกากบาทหน้าเพิ่มสินค้า

Test20 ทดสอบการเพิ่มสินค้าไม่สำเร็จเมื่อกรอกรหัสเป็นค่าว่าง
    เข้าหน้าสินค้า
    Sleep                               3
    กดปุ่มเพิ่มสินค้า
    Sleep                               3
    เลือกกลุ่มสินค้า
    Sleep                               3
    เลือกประเภทสินค้า
    Sleep                               3
    กรอกน.น./ชิ้น (บาท)                 2
    Sleep                               3
    กดยืนยันหน้าเพิ่มสินค้า
    Sleep                               3
    พบข้อความ                           *รหัสสินค้า*ต้องไม่เป็นค่าว่าง
    Sleep                               3
    กดกากบาทหน้าเพิ่มสินค้า

Test21 ทดสอบการเพิ่มสินค้าไม่สำเร็จเมื่อกรอกชื่อเป็นค่าว่าง
    เข้าหน้าสินค้า
    Sleep                               3
    กดปุ่มเพิ่มสินค้า
    Sleep                               3
    เลือกกลุ่มสินค้า
    Sleep                               3
    เลือกประเภทสินค้า
    Sleep                               3
    กรอกน.น./ชิ้น (บาท)                 2
    Sleep                               3
    กดยืนยันหน้าเพิ่มสินค้า
    Sleep                               3
    พบข้อความ                           *ชื่อสินค้า*ต้องไม่เป็นค่าว่าง
    Sleep                               3
    กดกากบาทหน้าเพิ่มสินค้า

Test22 ทดสอบการเพิ่มสินค้าไม่สำเร็จเมื่อเลือกสถานะเป็นซ่อน
    เข้าหน้าสินค้า
    Sleep                               3
    กดปุ่มเพิ่มสินค้า
    Sleep                               3
    เลือกกลุ่มสินค้า
    Sleep                               3
    เลือกประเภทสินค้า
    Sleep                               3
    กรอกน.น./ชิ้น (บาท)                 2
    Sleep                               3
    เจนรหัสสินค้า
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="productstatus"]/i                            300
    Sleep                               3
    Click Element                       //*[@id="productstatus"]/i
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="productstatus"]/div[2]/div[2]/span           300
    Sleep                               3
    Click Element                       //*[@id="productstatus"]/div[2]/div[2]/span
    Sleep                               3
    กดยืนยันหน้าเพิ่มสินค้า
    Sleep                               3
    ไม่พบข้อความ                        R90B200

Test23 ทดสอบการเพิ่มสินค้าสำเร็จเมื่อกรอกข้อมูลครบ
    เข้าหน้าสินค้า
    Sleep                               3
    กดปุ่มเพิ่มสินค้า
    Sleep                               3
    เลือกกลุ่มสินค้า
    Sleep                               3
    เลือกประเภทสินค้า
    Sleep                               3
    กรอกน.น./ชิ้น (บาท)                 3
    Sleep                               3
    เจนรหัสสินค้า
    Sleep                               3
    กดยืนยันหน้าเพิ่มสินค้า
    Sleep                               3
    พบข้อความ                           R90B300

# คำนวณสินค้า
Test24 ทดสอบการคำนวณน.น./ชิ้น (บาท) เมื่อเลือกเป็น 1/2สลึง
    เข้าหน้าสินค้า
    Sleep                               3
    กดปุ่มเพิ่มสินค้า
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="wb"]                                         300
    Sleep                               3
    Click Element                       //*[@id="wb"]
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="wb"]/div[2]/div[1]/span                      300
    Sleep                               3
    Click Element                       //*[@id="wb"]/div[2]/div[1]/span
    Sleep                               3
    ${value}                            Get Value                                             //*[@id="weightbath"]
    Should Be Equal                     ${value}                                              0.125
    Sleep                               3
    กดกากบาทหน้าเพิ่มสินค้า

Test25 ทดการสร้างรหัสสินค้าประเภทงานชั่งเมื่อกรอกข้อมูลครบ
    เข้าหน้าสินค้า
    Sleep                               3
    กดปุ่มเพิ่มสินค้า
    Sleep                               3
    เลือกกลุ่มสินค้า
    Sleep                               3
    เลือกประเภทสินค้า
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="wb"]                                         300
    Sleep                               3
    Click Element                       //*[@id="wb"]
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="wb"]/div[2]/div[1]/span                      300
    Sleep                               3
    Click Element                       //*[@id="wb"]/div[2]/div[1]/span
    Sleep                               3
    เจนรหัสสินค้า
    Sleep                               3
    ${value}                            Get Value                                             //*[@id="codeproduct"]
    Should Be Equal                     ${value}                                              R90B12
    Sleep                               3
    กดกากบาทหน้าเพิ่มสินค้า

Test26 ทดการสร้างรหัสสินค้าประเภทงานชิ้นเมื่อกรอกข้อมูลครบ
    เข้าหน้าสินค้า
    Sleep                               3
    กดปุ่มเพิ่มสินค้า
    Sleep                               3
    เลือกกลุ่มสินค้า
    Sleep                               3
    เลือกประเภทสินค้า
    Sleep                               3
    กรอกน.น./ชิ้น (บาท)                 3
    Sleep                               3
    เลือกประเภทงานขายเป็นงานชิ้น
    Sleep                               3
    กรอกราคาป้าย                        300
    Sleep                               3
    เจนรหัสสินค้า
    ${value}                            Get Value                                             //*[@id="codeproduct"]
    Should Be Equal                     ${value}                                              R90F300
    Sleep                               3
    กดกากบาทหน้าเพิ่มสินค้า

    # แก้ไขสินค้า
Test27 ทดสอบการแก้ไขสินค้าไม่สำเร็จเมื่อกดปุ่มกากบาท
    เข้าหน้าสินค้า
    Sleep                               3
    Wait Until Page Contains Element    //*[@id="0_btnEdit"]                                  300
    Sleep                               3
    Click Element                       //*[@id="0_btnEdit"]
    Sleep                               3
    กรอกน.น./ชิ้น (บาท)                 0.125
    Sleep                               3
    เจนรหัสสินค้า
    Sleep                               3
    กดกากบาทหน้าเพิ่มสินค้า
    Sleep                               3
    ไม่พบข้อความ                        R96G190

Test32 ทดสอบการแก้ไขสินค้าไม่สำเร็จเมื่อเลือกสถานะเป็นซ่อน
    เข้าหน้าสินค้า
    Sleep                               3
    Wait Until Page Contains Element    //*[@id="0_btnEdit"]                                  300
    Sleep                               3
    Click Element                       //*[@id="0_btnEdit"]
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="productstatus"]/i                            300
    Sleep                               3
    Click Element                       //*[@id="productstatus"]/i
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="productstatus"]/div[2]/div[2]/span           300
    Sleep                               3
    Click Element                       //*[@id="productstatus"]/div[2]/div[2]/span
    Sleep                               3
    กดยืนยันหน้าเพิ่มสินค้า
    Sleep                               3
    ไม่พบข้อความ                        R90B300

Test33 ทดสอบการแก้ไขสินค้าสำเร็จเมื่อกรอกข้อมูลครบ
    เข้าหน้าสินค้า
    Sleep                               3
    กดปุ่มเพิ่มสินค้า
    Sleep                               3
    เลือกกลุ่มสินค้า
    Sleep                               3
    เลือกประเภทสินค้า
    Sleep                               3
    กรอกน.น./ชิ้น (บาท)                 4
    Sleep                               3
    เจนรหัสสินค้า
    Sleep                               3
    กดยืนยันหน้าเพิ่มสินค้า
    Sleep                               3
    พบข้อความ                           R90B400
    Sleep                               3
    Wait Until Page Contains Element    //*[@id="0_btnEdit"]                                  300
    Sleep                               3
    Click Element                       //*[@id="0_btnEdit"]
    Sleep                               3
    กรอกน.น./ชิ้น (บาท)                 0.125
    Sleep                               3
    เจนรหัสสินค้า
    Sleep                               3
    กดยืนยันหน้าเพิ่มสินค้า
    Sleep                               3
    พบข้อความ                           R90B12

# ยกเลิกสินค้า
Test34 ทดสอบการยกเลิกประเภทสินค้าไม่สำเร็จเมื่อปฏิเสธการยืนยันการลบ
    เข้าหน้าสินค้า
    Sleep                               3
    Wait Until Page Contains Element    //*[@id="0_btnDelete"]                                300
    Sleep                               3
    Click Element                       //*[@id="0_btnDelete"]
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="deleteproduct"]/div[2]/button[1]             300
    Sleep                               3
    Click Element                       //*[@id="deleteproduct"]/div[2]/button[1]
    Sleep                               3
    พบข้อความ                           R90B12

Test35 ทดสอบการยกเลิกประเภทสินค้าสำเร็จเมื่อตกลงการยืนยันการลบ
    เข้าหน้าสินค้า
    Sleep                               3
    Wait Until Page Contains Element    //*[@id="0_btnDelete"]                                300
    Sleep                               3
    Click Element                       //*[@id="0_btnDelete"]
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="deleteproduct"]/div[2]/button[2]             300
    Sleep                               3
    Click Element                       //*[@id="deleteproduct"]/div[2]/button[2]
    Sleep                               3
    ไม่พบข้อความ                        R90B12


*** Keywords ***
เข้าหน้าสินค้า
    Sleep                               2
    Wait Until Page Contains Element    //*[@id="product"]                                    300
    Click Element                       //*[@id="product"]

กดปุ่มค้นหาสินค้า
    Sleep                               2
    Wait Until Page Contains Element    //*[@id="searchproduct"]                              300
    Click Element                       //*[@id="searchproduct"]

กดปุ่มค้นหาสินค้าทั้งหมด
    Sleep                               2
    Wait Until Page Contains Element    //*[@id="searchallproduct"]                           300
    Click Element                       //*[@id="searchallproduct"]

กดปุ่มเพิ่มสินค้า
    Sleep                               2
    Wait Until Page Contains Element    //*[@id="addproduct"]                                 300
    Click Element                       //*[@id="addproduct"]

กดกากบาทหน้าเพิ่มสินค้า
    Sleep                               2
    Wait Until Page Contains Element    //*[@id="closeaddProduct"]                            300
    Click Element                       //*[@id="closeaddProduct"]

กดยืนยันหน้าเพิ่มสินค้า
    Sleep                               2
    Wait Until Page Contains Element    //*[@id="confirmproduct"]                             300
    Click Element                       //*[@id="confirmproduct"]

เลือกกลุ่มสินค้า
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="productgroup"]/i                             300
    Click Element                       //*[@id="productgroup"]/i
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="productgroup"]/div[2]/div[1]/span            300
    Click Element                       //*[@id="productgroup"]/div[2]/div[1]/span

เลือกประเภทสินค้า
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="producttype"]/i                              300
    Click Element                       //*[@id="producttype"]/i
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="producttype"]/div[2]/div/span                300
    Click Element                       //*[@id="producttype"]/div[2]/div/span

เลือกประเภทประเภทน.น.เป็นกรัม
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="weightproduct"]/i                            300
    Click Element                       //*[@id="weightproduct"]/i
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="weightproduct"]/div[2]/div[2]/span           300
    Click Element                       //*[@id="weightproduct"]/div[2]/div[2]/span

กรอกน.น./ชิ้น (บาท)
    [Arguments]                         ${value}
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="weightbath"]                                 300
    Input Text                          //*[@id="weightbath"]                                 ${value}

กรอกน.น./ชิ้น (กรัม)
    [Arguments]                         ${value}
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="weightg"]                                    300
    Input Text                          //*[@id="weightg"]                                    ${value}

เลือกประเภทงานขายเป็นงานชิ้น
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="typesell"]/i                                 300
    Click Element                       //*[@id="typesell"]/i
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="typesell"]/div[2]/div[2]/span                300
    Click Element                       //*[@id="typesell"]/div[2]/div[2]/span

กรอกราคาป้าย
    [Arguments]                         ${value}
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="pricetag"]                                   300
    Input Text                          //*[@id="pricetag"]                                   ${value}

กรอกราคาต้นทุน
    [Arguments]                         ${value}
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="cost"]                                       300
    Input Text                          //*[@id="cost"]                                       ${value}

เจนรหัสสินค้า
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="codeid"]                                     300
    Click Element                       //*[@id="codeid"]

ค้นหาสินค้าด้วยกลุ่มสินค้า
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="searchproductgroup"]/i                       300
    Click Element                       //*[@id="searchproductgroup"]/i
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="searchproductgroup"]/div[2]/div[1]/span      300
    Click Element                       //*[@id="searchproductgroup"]/div[2]/div[1]/span

ค้นหาสินค้าด้วยประเภทสินค้า
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="searchproducttype"]/i                        300
    Click Element                       //*[@id="searchproducttype"]/i
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="searchproducttype"]/div[2]/div/span          300
    Click Element                       //*[@id="searchproducttype"]/div[2]/div/span

ค้นหาสินค้าด้วยรหัสสินค้า
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="searchproductid"]/i                          300
    Click Element                       //*[@id="searchproductid"]/i
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="searchproductid"]/div[2]/div/span            300
    Click Element                       //*[@id="searchproductid"]/div[2]/div/span

ค้นหาสินค้าด้วยชื่อสินค้า
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="searchproductname"]/i                        300
    Click Element                       //*[@id="searchproductname"]/i
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="searchproductname"]/div[2]/div/span          300
    Click Element                       //*[@id="searchproductname"]/div[2]/div/span

ค้นหาสินค้าด้วยชื่อประเภทงานขาย
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="searchproductsell"]/i                        300
    Click Element                       //*[@id="searchproductsell"]/i
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="searchproductsell"]/div[2]/div/span          300
    Click Element                       //*[@id="searchproductsell"]/div[2]/div/span

ค้นหาสินค้าด้วยน้ำหนักบาท
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="searchproductweightb"]/i                     300
    Click Element                       //*[@id="searchproductweightb"]/i
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="searchproductweightb"]/div[2]/div[6]/span    300
    Click Element                       //*[@id="searchproductweightb"]/div[2]/div[6]/span

ค้นหาสินค้าด้วยน้ำหนักกรัม
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="searchproductweightg"]                       300
    Input Text                          //*[@id="searchproductweightg"]                       15.2


ตรวจสอบจำนวนสินค้า
    [Arguments]                         ${value}
    Sleep                               1
    ${count}=                           Get Element Count                                     id:tableproductid
    ${count}=                           Convert To String                                     ${count}
    Should Be Equal                     ${count}                                              ${value}