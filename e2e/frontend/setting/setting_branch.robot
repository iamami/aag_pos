*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบAdminและinit_data
Test Setup        เข้าหน้าแฟ้มข้อมูล
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data_admin
*** Variables ***

*** Test cases ***
Test1 ทดสอบการแสดงข้อมูลเมื่อเปลี่ยนสาขา
    เข้าหน้าตั้งค่าสาขา
    Sleep                                        3
    เลือกสาขากิ่งแก้ว
    Sleep                                        3
    พบหมวดเริ่มต้นใบกำกับภาษี                    AB
    Sleep                                        3
    พบเลขที่เริ่มต้นใบกำกับภาษี                  5
    Sleep                                        3
    พบหมวดเริ่มต้นรหัสพนักงาน                    B
    Sleep                                        3
    พบเลขที่เริ่มต้นรหัสพนักงาน                  2
    Sleep                                        3
    พบหมวดเริ่มต้นขายฝาก                         AB
    Sleep                                        3
    พบเลขที่เริ่มต้นขายฝาก                       2
    Sleep                                        3
    พบจำนวนเดือนขายฝาก                           3
    Sleep                                        3
    พบอัตราดอกเบี้ยขายฝาก                        2.00
    Sleep                                        3
    พบดอกเบี้ยขายฝากขั้นต่ำ                      100.00
    #พบรายละเอียดของสาขาที่เลือก

Test2 ทดสอบการแสดงเลขที่เริ่มต้นใบขายฝาก เมื่อสร้างใบขายฝากใบใหม่
    เข้าหน้าขายฝากหลัก
    Sleep                                        1
    กดปุ่มสร้างรายการขายฝาก
    Sleep                                        1
    พบPopupขายฝาก
    Sleep                                        1
    กรอกชื่อลูกค้า                               linda
    Sleep                                        1
    กรอกจำนวนเงิน                                10000
    กดปุ่มเพิ่มรายการสินค้า
    Sleep                                        1
    พบPopupเลือกรายการสินค้า
    Sleep                                        1
    เลือก % ทอง
    ใส่ชื่อสินค้า
    ใส่จำนวน
    Sleep                                        1
    ใส่น้ำหนัก
    กดปุ่มเพิ่มสินค้า
    Sleep                                        1
    พบPopupขายฝาก
    Sleep                                        2
    กดปุ่มสร้าง
    Sleep                                        1
    พบPopupจ่ายเงิน
    Sleep                                        1
    เลือกพนักงานเพื่อเพิ่ม
    Sleep                                        1
    กดปุ่มบันทึกการจ่ายเงิน
    Sleep                                        10
    ตรวจพบข้อความบน Alert                        บันทึกข้อมูลสำเร็จ
    #พบalert บันทึกข้อมูลสำเร็จ
    Sleep                                        1
    กดปุ่มพิมพ์สัญญา
    Sleep                                        1
    พบPopup Preview Lease
    Sleep                                        1
    พบข้อความ                                    Preview
    ตรวจสอบเลขที่บิลขายฝาก                       AA00002
    ตรวจสอบชื่อลูกค้า                            linda
    ตรวจสอบจำนวนเงินที่จ่าย                      10,000.00
    กดปุ่มปิดPopup Preview Lease
    Sleep                                        1
    ปิดPopupขายฝาก
    Sleep                                        1
    เข้าหน้าแฟ้มข้อมูล
    Sleep                                        1
    เข้าหน้าตั้งค่าสาขา
    Sleep                                        1
    พบหมวดเริ่มต้นขายฝาก                         AA
    Sleep                                        1
    พบเลขที่เริ่มต้นขายฝาก                       3
    #พบเลขที่เริ่มต้นใบขายฝากล่าสุด +1

Test3 ทดสอบการแสดงเลขที่เริ่มต้นใบกำกับภาษี เมื่อสร้างใบกำกับภาษีใหม่
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                                        1
    เข้าหน้าขายทอง
    Sleep                                        1
    พบPopup POS
    Sleep                                        1
    เลือกพนักงานคนที่                            1
    Sleep                                        1
    กดเพิ่มรายการสินค้า
    Sleep                                        1
    Wait Until Element Is Visible                //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select              300
    Sleep                                        3
    Click Element                                //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select
    Sleep                                        1
    Wait Until Element Is Visible                //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select/option[2]    300
    Sleep                                        3
    Click Element                                //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select/option[2]
    Sleep                                        1
    กดปุ่มลงรายการ
    Sleep                                        1
    #pop up เลือกรายการสินค้า ถูกปิด
    พบPopup POS
    #แสดง pop up ขายทอง
    Sleep                                        1
    Sleep                                        1
    กดปุ่มบันทึกและชำระเงิน
    Sleep                                        1
    พบPopup ชำระเงิน
    Sleep                                        1
    กรอกจำนวนรับเงินสด                           12408
    Sleep                                        5
    กดปุ่มบันทึกชำระเงิน
    Sleep                                        5
    ตรวจพบข้อความบน Alert                        ยืนยันการชำระเงิน
    Sleep                                        5
    ตรวจพบข้อความบน Alert                        บันทึกบิลและชำระเงินสำเร็จ
    Sleep                                        1
    Wait Until Element Is Visible                id:textCost                                                                                   300
    กดปุ่มVat
    Sleep                                        10
    พบยอดเงินรับสุทธิ                            12,408.00
    Sleep                                        5
    กดปุ่มปิดPopup Preview
    Sleep                                        1
    ปิดpopupPOS
    Sleep                                        1
    เข้าหน้าแฟ้มข้อมูล
    Sleep                                        1
    เข้าหน้าตั้งค่าสาขา
    Sleep                                        2
    พบหมวดเริ่มต้นใบกำกับภาษี                    AA
    Sleep                                        3
    พบเลขที่เริ่มต้นใบกำกับภาษี                  6
    #พบเลขที่เริ่มต้นใบกำกับภาษีฝากล่าสุด +1

Test4 ทดสอบการแสดงเลขที่เริ่มต้นพนักงาน เมื่อสร้างพนักงานคนใหม่
    เข้าหน้าพนักงาน
    Sleep                                        1
    กดสร้างพนักงาน
    Sleep                                        1
    เลือกสาขาพนักงาน
    Sleep                                        1
    กรอกชื่อพนักงาน
    Sleep                                        1
    กรอกที่อยู่พนักงาน
    Sleep                                        1
    กรอกเบอร์โทรศัพท์พนักงาน
    Sleep                                        1
    กดปุ่มบันทึกสร้างพนักงาน
    Sleep                                        1
    พบข้อความ                                    A002
    Sleep                                        1
    เข้าหน้าแฟ้มข้อมูล
    Sleep                                        1
    เข้าหน้าตั้งค่าสาขา
    Sleep                                        2
    พบหมวดเริ่มต้นรหัสพนักงาน                    A
    Sleep                                        1
    พบเลขที่เริ่มต้นรหัสพนักงาน                  3

Test5 ทดสอบการแก้ไขตั้งค่าสาขาไม่สำเร็จ เมื่อกรอกหมวดเริ่มต้นขายฝาก 1 ตัวอักษร
    เข้าหน้าตั้งค่าสาขา
    Sleep                                        3
    เลือกสาขากิ่งแก้ว
    Sleep                                        3
    กรอกหมวดเริ่มต้นขายฝาก                       j
    Sleep                                        3
    กดปุ่มบันทึกการตั้งค่า
    Sleep                                        3
    พบข้อความ                                    หมวดเริ่มต้น รูปแบบตัวอักษรไม่ถูกต้อง!
    #พบข้อความ หมวดเริ่มต้น รูปแบบตัวอักษรไม่ถูกต้อง!

Test6 ทดสอบการแก้ไขตั้งค่าสาขาไม่สำเร็จ เมื่อกรอกหมวดเริ่มต้นใบกำกับภาษี 1 ตัวอักษร
    เข้าหน้าตั้งค่าสาขา
    Sleep                                        3
    เลือกสาขากิ่งแก้ว
    Sleep                                        3
    กรอกหมวดเริ่มต้นใบกำกับภาษี                  j
    Sleep                                        3
    กดปุ่มบันทึกการตั้งค่า
    Sleep                                        3
    พบข้อความ                                    หมวดเริ่มต้น รูปแบบตัวอักษรไม่ถูกต้อง!
    #พบข้อความ หมวดเริ่มต้น รูปแบบตัวอักษรไม่ถูกต้อง!

Test7 ทดสอบการแก้ไขตั้งค่าสาขาไม่สำเร็จ เมื่อกรอกหมวดเริ่มต้นรหัสพนักงานมากกว่า 1 ตัวอักษร
    เข้าหน้าตั้งค่าสาขา
    Sleep                                        3
    เลือกสาขากิ่งแก้ว
    Sleep                                        3
    กรอกหมวดเริ่มต้นรหัสพนักงาน                  jj
    Sleep                                        3
    กดปุ่มบันทึกการตั้งค่า
    Sleep                                        3
    พบข้อความ                                    หมวดเริ่มต้น รูปแบบตัวอักษรไม่ถูกต้อง!
    #พบข้อความ หมวดเริ่มต้น รูปแบบตัวอักษรไม่ถูกต้อง!

Test8 ทดสอบการแก้ไขตั้งค่าสาขาสำเร็จ เมื่อแก้ไขข้อมูลทุกส่วน
    เข้าหน้าตั้งค่าสาขา
    Sleep                                        3
    กรอกหมวดเริ่มต้นขายฝาก                       JJ
    Sleep                                        3
    กรอกหมวดเริ่มต้นใบกำกับภาษี                  JJ
    Sleep                                        3
    กรอกหมวดเริ่มต้นรหัสพนักงาน                  J
    Sleep                                        3
    กรอกเลขขายฝาก                                12
    Sleep                                        3
    กรอกเลขใบกำกับภาษี                           15
    Sleep                                        3
    กรอกเลขรหัสพนักงาน                           12
    Sleep                                        3
    กรอกดอกเบี้ยขายฝากขั้นต่ำ                    1000
    Sleep                                        3
    กรอกอัตราดอกเบี้ยขายฝาก (%)                  3.00
    Sleep                                        3
    กรอกจำนวนเดือนขายฝาก                         1
    Sleep                                        3
    Wait Until Page Contains Element             id:radio3                                                                                     300
    Sleep                                        3
    Click Element                                id:radio3
    Sleep                                        3
    กดปุ่มบันทึกการตั้งค่า
    Sleep                                        3
    ตรวจพบข้อความบน Alert                        บันทึกสำเร็จ
    Sleep                                        3
    #พบalert บันทึกข้อมูลสำเร็จ
    ###lease###
    Go To                                       ${BASE_URL}
    Sleep                                        3
    เข้าหน้าขายฝากหลัก
    Sleep                                        3
    กดปุ่มสร้างรายการขายฝาก
    Sleep                                        1
    พบPopupขายฝาก
    Sleep                                        1
    กรอกชื่อลูกค้า                               linda
    Sleep                                        1
    กรอกจำนวนเงิน                                10000
    พบระยะเวลา(เดือน)                            1
    พบอัตราดอกเบี้ย(%)                           3.00
    กดปุ่มเพิ่มรายการสินค้า
    Sleep                                        1
    พบPopupเลือกรายการสินค้า
    Sleep                                        1
    เลือก % ทอง
    ใส่ชื่อสินค้า
    ใส่จำนวน
    Sleep                                        1
    ใส่น้ำหนัก
    กดปุ่มเพิ่มสินค้า
    Sleep                                        1
    พบPopupขายฝาก
    Sleep                                        2
    กดปุ่มสร้าง
    Sleep                                        1
    พบPopupจ่ายเงิน
    Sleep                                        1
    เลือกพนักงานเพื่อเพิ่ม
    Sleep                                        1
    กดปุ่มบันทึกการจ่ายเงิน
    Sleep                                        10
    ตรวจพบข้อความบน Alert                        บันทึกข้อมูลสำเร็จ
    #พบalert บันทึกข้อมูลสำเร็จ
    กดปุ่มเพิ่มรายการดอกเบี้ยรับ
    Sleep                                        1
    พบดอกเบี้ยคำนวณ                              1,000.00
    Sleep                                        1
    Wait Until Element Is Visible                //*[@id="modalInterest"]/div[2]/form/div[2]/div[6]/label/span                                 300
    Sleep                                        1
    พบข้อความ                                    **ดอกเบี้ยขั้นต่ำ
    Sleep                                        1
    ปิดpopupต่อดอกไถ่คืน
    Sleep                                        1
    กดปุ่มพิมพ์สัญญา
    Sleep                                        1
    พบPopup Preview Lease
    Sleep                                        1
    พบข้อความ                                    Preview
    ตรวจสอบเลขที่บิลขายฝาก                       JJ00012
    ตรวจสอบชื่อลูกค้า                            linda
    ตรวจสอบจำนวนเงินที่จ่าย                      10,000.00
    กดปุ่มปิดPopup Preview Lease
    Sleep                                        1
    ปิดPopupขายฝาก
    Sleep                                        1
    เข้าหน้าแฟ้มข้อมูล
    Sleep                                        1
    เข้าหน้าตั้งค่าสาขา
    Sleep                                        1
    พบหมวดเริ่มต้นขายฝาก                         JJ
    Sleep                                        1
    พบเลขที่เริ่มต้นขายฝาก                       13
    #พบข้อมูลใบขายฝากถูกต้องเมื่อสร้ารายการใหม่

    ###POS###
    Go To                                       ${BASE_URL}
    Sleep                                        3
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                                        1
    เข้าหน้าขายทอง
    Sleep                                        1
    พบPopup POS
    Sleep                                        1
    เลือกพนักงานคนที่                            1
    Sleep                                        1
    กดเพิ่มรายการสินค้า
    Sleep                                        1
    Wait Until Element Is Visible                //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select              300
    Sleep                                        3
    Click Element                                //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select
    Sleep                                        1
    Wait Until Element Is Visible                //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select/option[2]    300
    Sleep                                        3
    Click Element                                //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select/option[2]
    Sleep                                        1
    กดปุ่มลงรายการ
    Sleep                                        1
    #pop up เลือกรายการสินค้า ถูกปิด
    พบPopup POS
    #แสดง pop up ขายทอง
    Sleep                                        1
    Sleep                                        1
    กดปุ่มบันทึกและชำระเงิน
    Sleep                                        1
    พบPopup ชำระเงิน
    Sleep                                        1
    กรอกจำนวนรับเงินสด                           12408
    Sleep                                        5
    กดปุ่มบันทึกชำระเงิน
    Sleep                                        5
    ตรวจพบข้อความบน Alert                        ยืนยันการชำระเงิน
    Sleep                                        5
    ตรวจพบข้อความบน Alert                        บันทึกบิลและชำระเงินสำเร็จ
    Sleep                                        1
    Wait Until Element Is Visible                id:textCost                                                                                   300
    กดปุ่มVat
    Sleep                                        10
    พบยอดเงินรับสุทธิ                            12,408.00
    Sleep                                        5
    กดปุ่มปิดPopup Preview
    Sleep                                        5
    ปิดpopupPOS
    Sleep                                        5
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                                        5
    กดปุ่มค้นหาบิล
    Sleep                                        5
    พบข้อความ                                    JJ000000015
    Sleep                                        1
    เข้าหน้าแฟ้มข้อมูล
    Sleep                                        1
    เข้าหน้าตั้งค่าสาขา
    Sleep                                        2
    พบหมวดเริ่มต้นใบกำกับภาษี                    JJ
    Sleep                                        3
    พบเลขที่เริ่มต้นใบกำกับภาษี                  16
    #พบข้อมูลใบกำกับภาษีถูกต้องเมื่อสร้ารายการใหม่

    ###Staff###
    Go To                                       ${BASE_URL}
    Sleep                                        3
    เข้าหน้าแฟ้มข้อมูล
    Sleep                                        3
    เข้าหน้าพนักงาน
    Sleep                                        1
    กดสร้างพนักงาน
    Sleep                                        1
    เลือกสาขาพนักงาน
    Sleep                                        1
    กรอกชื่อพนักงาน
    Sleep                                        1
    กรอกที่อยู่พนักงาน
    Sleep                                        1
    กรอกเบอร์โทรศัพท์พนักงาน
    Sleep                                        1
    กดปุ่มบันทึกสร้างพนักงาน
    Sleep                                        1
    พบข้อความ                                    J012
    Sleep                                        1
    เข้าหน้าแฟ้มข้อมูล
    Sleep                                        1
    เข้าหน้าตั้งค่าสาขา
    Sleep                                        2
    พบหมวดเริ่มต้นรหัสพนักงาน                    J
    Sleep                                        1
    พบเลขที่เริ่มต้นรหัสพนักงาน                  13
    #พบข้อมูลรหัสพนักงานถูกต้องเมื่อสร้ารายการใหม่

Test9 ทดสอบการอนุญาตให้userสามารถแก้ไขดอกเบี้ยได้สำเร็จ
    เข้าหน้าตั้งค่าสาขา
    Sleep                                        1
    เลือกอนุญาติให้ยูสเซอร์แก้ไขอัตราดอกเบี้ย
    Sleep                                        3
    กดปุ่มบันทึกการตั้งค่า
    Sleep                                        3
    ตรวจพบข้อความบน Alert                        บันทึกสำเร็จ
    Sleep                                        3
    ###lease###
    Go To                                       ${BASE_URL}
    Sleep                                        3
    เข้าหน้าขายฝากหลัก
    Sleep                                        1
    กดปุ่มสร้างรายการขายฝาก
    Sleep                                        1
    พบPopupขายฝาก
    Sleep                                        1
    กรอกชื่อลูกค้า                               linda
    Sleep                                        1
    กรอกจำนวนเงิน                                10000
    Sleep                                        1
    กรอกอัตราดอกเบี้ย(%)                         3.00
    Sleep                                        1
    พบอัตราดอกเบี้ย(%)                           3.00
    ปิดpopupPOS

Test10 ทดสอบการไม่อนุญาตให้userสามารถแก้ไขดอกเบี้ยได้สำเร็จ
    เข้าหน้าตั้งค่าสาขา
    Sleep                                        1
    เลือกสาขากิ่งแก้ว
    # เลือกอนุญาติให้ยูสเซอร์แก้ไขอัตราดอกเบี้ย
    # กดปุ่มบันทึกการตั้งค่า
    # ตรวจพบข้อความบน Alert               บันทึกสำเร็จ
    ###lease###
    Go To                                       ${BASE_URL}
    Sleep                                        3
    เข้าหน้าขายฝากหลัก
    Sleep                                        3
    Wait Until Page Contains Element             //*[@id="root-content"]/div/form/div/div/h3/div[2]/div                                        300
    Sleep                                        3
    Click Element                                //*[@id="root-content"]/div/form/div/div/h3/div[2]/div
    Sleep                                        3
    Wait Until Element Is Visible                //*[@id="root-content"]/div/form/div/div/h3/div[2]/div/div[2]/div[2]                          300
    Sleep                                        3
    Click Element                                //*[@id="root-content"]/div/form/div/div/h3/div[2]/div/div[2]/div[2]
    กดปุ่มสร้างรายการขายฝาก
    Sleep                                        1
    พบPopupขายฝาก
    Sleep                                        1
    กรอกชื่อลูกค้า                               linda
    Sleep                                        1
    กรอกจำนวนเงิน                                10000
    Sleep                                        5
    กรอกอัตราดอกเบี้ย(%)                         3.00
    Sleep                                        5
    พบอัตราดอกเบี้ย(%)                           2.00
    ปิดpopupPOS

*** Keywords ***
เข้าหน้าตั้งค่าสาขา
    Sleep                                        2
    Wait Until Page Contains Element             id:setting_branch                                                                             300
    Click Element                                id:setting_branch

เลือกสาขาลาดกระบัง
    Wait Until Element Is Visible                id:dropDownBranch                                                                             300
    Click Element                                id:dropDownBranch
    Wait Until Element Is Visible                //*[@id="dropDownBranch"]/div[2]/div[1]                                                       300
    Click Element                                //*[@id="dropDownBranch"]/div[2]/div[1]

เลือกสาขากิ่งแก้ว
    Wait Until Element Is Visible                id:dropDownBranch                                                                             300
    Click Element                                id:dropDownBranch
    Wait Until Element Is Visible                //*[@id="dropDownBranch"]/div[2]/div[2]                                                       300
    Click Element                                //*[@id="dropDownBranch"]/div[2]/div[2]

พบหมวดเริ่มต้นใบกำกับภาษี
    [Arguments]                                  ${value}
    ${text} =                                    Get Value                                                                                     id:inputCatagoryVat
    Should Be Equal                              ${text}                                                                                       ${value}

พบหมวดเริ่มต้นรหัสพนักงาน
    [Arguments]                                  ${value}
    ${text} =                                    Get Value                                                                                     id:inputCatagoryStaff
    Should Be Equal                              ${text}                                                                                       ${value}

พบหมวดเริ่มต้นขายฝาก
    [Arguments]                                  ${value}
    ${text} =                                    Get Value                                                                                     id:inputCatagoryLease
    Should Be Equal                              ${text}                                                                                       ${value}

พบเลขที่เริ่มต้นใบกำกับภาษี
    [Arguments]                                  ${value}
    ${text} =                                    Get Value                                                                                     id:inputStartIDVat
    Should Be Equal                              ${text}                                                                                       ${value}

พบเลขที่เริ่มต้นรหัสพนักงาน
    [Arguments]                                  ${value}
    ${text} =                                    Get Value                                                                                     id:IDstaffstart
    Should Be Equal                              ${text}                                                                                       ${value}

พบเลขที่เริ่มต้นขายฝาก
    [Arguments]                                  ${value}
    ${text} =                                    Get Value                                                                                     id:inputStartIDLease
    Should Be Equal                              ${text}                                                                                       ${value}

พบจำนวนเดือนขายฝาก
    [Arguments]                                  ${value}
    ${text} =                                    Get Value                                                                                     id:inputMonthLease
    Should Be Equal                              ${text}                                                                                       ${value}

พบอัตราดอกเบี้ยขายฝาก
    [Arguments]                                  ${value}
    ${text} =                                    Get Value                                                                                     id:inputRateLease
    Should Be Equal                              ${text}                                                                                       ${value}

พบดอกเบี้ยขายฝากขั้นต่ำ
    [Arguments]                                  ${value}
    ${text} =                                    Get Value                                                                                     id:inputMinRateLease
    Should Be Equal                              ${text}                                                                                       ${value}

พบยอดเงินรับสุทธิ
    [Arguments]                                  ${value}
    Wait Until Element Is Visible                //*[@id="table-to-xls"]/tbody[5]/tr[6]/td[3]                                                  300
    ${text} =                                    Get Text                                                                                      //*[@id="table-to-xls"]/tbody[5]/tr[6]/td[3]
    Should Be Equal                              ${text}                                                                                       ${value}

เข้าหน้าพนักงาน
    Wait Until Page Contains Element             id:staff                                                                                      300
    Click Element                                id:staff

กดสร้างพนักงาน
    Wait Until Page Contains Element             //*[@id="addstaff"]                                                                           300
    Click Element                                //*[@id="addstaff"]\

กดปุ่มบันทึกสร้างพนักงาน
    Wait Until Element Is Visible                id:submitAddstaff                                                                             300
    Click Element                                id:submitAddstaff

เลือกสาขาพนักงาน
    Sleep                                        2
    Wait Until Element Is Visible                //*[@id="branch"]/i                                                                           300
    Click Element                                //*[@id="branch"]/i
    Sleep                                        2
    Wait Until Element Is Visible                //*[@id="branch"]/div[2]/div[2]/span                                                          300
    Click Element                                //*[@id="branch"]/div[2]/div[2]/span

กรอกสาขาพนักงาน
    Wait Until Element Is Visible                //*[@id="branch"]/input                                                                       300
    Input Text                                   //*[@id="branch"]/input                                                                       ลาดกระบัง

กรอกชื่อพนักงาน
    Wait Until Element Is Visible                //*[@id="nameStaff"]                                                                          300
    Input Text                                   //*[@id="nameStaff"]                                                                          daniel

กรอกที่อยู่พนักงาน
    Wait Until Element Is Visible                //*[@id="addressStaff"]                                                                       300
    Input Text                                   //*[@id="addressStaff"]                                                                       bangkok

กรอกเบอร์โทรศัพท์พนักงาน
    Wait Until Element Is Visible                //*[@id="phoneStaff"]                                                                         300
    Input Text                                   //*[@id="phoneStaff"]                                                                         0888888888

กรอกหมวดเริ่มต้นใบกำกับภาษี
    [Arguments]                                  ${value}
    Sleep                                        1
    Wait Until Element Is Visible                //*[@id="inputCatagoryVat"]                                                                   300
    Sleep                                        1
    Input Text                                   //*[@id="inputCatagoryVat"]                                                                   ${value}

กรอกหมวดเริ่มต้นขายฝาก
    [Arguments]                                  ${value}
    Sleep                                        1
    Wait Until Element Is Visible                //*[@id="inputCatagoryLease"]                                                                 300
    Sleep                                        1
    Input Text                                   //*[@id="inputCatagoryLease"]                                                                 ${value}

กรอกหมวดเริ่มต้นรหัสพนักงาน
    [Arguments]                                  ${value}
    Sleep                                        1
    Wait Until Element Is Visible                //*[@id="inputCatagoryStaff"]                                                                 300
    Sleep                                        1
    Input Text                                   //*[@id="inputCatagoryStaff"]                                                                 ${value}

กรอกเลขใบกำกับภาษี
    [Arguments]                                  ${value}
    Sleep                                        1
    Wait Until Element Is Visible                //*[@id="inputStartIDVat"]                                                                    300
    Sleep                                        1
    Input Text                                   //*[@id="inputStartIDVat"]                                                                    ${value}

กรอกเลขขายฝาก
    [Arguments]                                  ${value}
    Sleep                                        1
    Wait Until Element Is Visible                //*[@id="inputStartIDLease"]                                                                  300
    Sleep                                        1
    Input Text                                   //*[@id="inputStartIDLease"]                                                                  ${value}

กรอกเลขรหัสพนักงาน
    [Arguments]                                  ${value}
    Sleep                                        1
    Wait Until Element Is Visible                //*[@id="IDstaffstart"]                                                                       300
    Sleep                                        1
    Input Text                                   //*[@id="IDstaffstart"]                                                                       ${value}

กรอกดอกเบี้ยขายฝากขั้นต่ำ
    [Arguments]                                  ${value}
    Sleep                                        1
    Wait Until Element Is Visible                //*[@id="inputMinRateLease"]                                                                  300
    Sleep                                        1
    Input Text                                   //*[@id="inputMinRateLease"]                                                                  ${value}

กรอกอัตราดอกเบี้ยขายฝาก (%)
    [Arguments]                                  ${value}
    Sleep                                        1
    Wait Until Element Is Visible                //*[@id="inputRateLease"]                                                                     300
    Sleep                                        1
    Input Text                                   //*[@id="inputRateLease"]                                                                     ${value}

กรอกจำนวนเดือนขายฝาก
    [Arguments]                                  ${value}
    Sleep                                        1
    Wait Until Element Is Visible                //*[@id="inputMonthLease"]                                                                    300
    Sleep                                        1
    Input Text                                   //*[@id="inputMonthLease"]                                                                    ${value}

กรอกอัตราดอกเบี้ย(%)
    [Arguments]                                  ${value}
    Sleep                                        1
    Wait Until Element Is Visible                //*[@id="inputInterestRatio"]                                                                 300
    Sleep                                        1
    Input Text                                   //*[@id="inputInterestRatio"]                                                                 ${value}

พบระยะเวลา(เดือน)
    [Arguments]                                  ${value}
    Wait Until Element Is Visible                id:inputTime                                                                                  300
    ${text} =                                    Get Value                                                                                     id:inputTime
    Should Be Equal                              ${text}                                                                                       ${value}

พบอัตราดอกเบี้ย(%)
    [Arguments]                                  ${value}
    Wait Until Element Is Visible                id:inputInterestRatio                                                                         300
    ${text} =                                    Get Value                                                                                     id:inputInterestRatio
    Should Be Equal                              ${text}                                                                                       ${value}

พบดอกเบี้ยคำนวณ
    [Arguments]                                  ${value}
    Wait Until Element Is Visible                id:InterestTotal                                                                              300
    ${text} =                                    Get Value                                                                                     id:InterestTotal
    Should Be Equal                              ${text}                                                                                       ${value}

กดปุ่มบันทึกการตั้งค่า
    Wait Until Page Contains Element             //*[@id="btnsave_setting"]                                                                    300
    Click Element                                //*[@id="btnsave_setting"]

เลือกอนุญาติให้ยูสเซอร์แก้ไขอัตราดอกเบี้ย
    Wait Until Page Contains Element             //*[@id="content-body"]/div/form[2]/div[2]/div[2]/div/div[2]/div[2]/div                       300
    Click Element                                //*[@id="content-body"]/div/form[2]/div[2]/div[2]/div/div[2]/div[2]/div
