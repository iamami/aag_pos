*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบAdminและinit_data 
Test Setup        เข้าหน้าแฟ้มข้อมูล
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data_admin
*** Variables ***

*** Test cases ***
Test1 ทดสอบการเพิ่มผู้ใช้งานไม่สำเร็จเมื่อกดปุ่มกากบาท
    เข้าหน้าผู้ใช้งาน
    กดปุ่มเพิ่มผู้ใช้
    กดปุ่มกากบาทผู้ใช้
    Sleep                                3
    พบจำนวนแถวผุ้ใช้งาน                  2

Test2 ทดสอบการเพิ่มผู้ใช้งานไม่สำเร็จเมื่อกดปุ่มยกเลิก
    เข้าหน้าผู้ใช้งาน
    กดปุ่มเพิ่มผู้ใช้
    กดปุ่มยกเลิกผู้ใช้
    Sleep                                3
    พบจำนวนแถวผุ้ใช้งาน                  2

Test3 ทดสอบการเพิ่มผู้ใช้งานไม่สำเร็จเมื่อไม่เลือกสาขาแล้วกดบันทึก
    เข้าหน้าผู้ใช้งาน
    กดปุ่มเพิ่มผู้ใช้
    กรอกชื่อผู้ใช้                       test
    กรอกรหัสผ่าน                         112233
    กรอกยืนยันรหัสผ่าน                   112233
    เลือกระดับuser
    เลือกสถานะซ่อน
    กดปุ่มบันทึกผู้ใช้
    พบข้อความ                            *กรุณาเลือกสาขา
    #พบข้อความ *กรุณาเลือกสาขา
    กดปุ่มกากบาทผู้ใช้
    Sleep                                3
    พบจำนวนแถวผุ้ใช้งาน                  2

Test4 ทดสอบการเพิ่มผู้ใช้งานไม่สำเร็จเมื่อไม่กรอกชื่อผู้ใช้แล้วกดบันทึก
    เข้าหน้าผู้ใช้งาน
    กดปุ่มเพิ่มผู้ใช้
    เลือกสาขาที่                         1
    กรอกรหัสผ่าน                         112233
    กรอกยืนยันรหัสผ่าน                   112233
    เลือกระดับuser
    เลือกพนักงาน
    เลือกสถานะซ่อน
    กดปุ่มบันทึกผู้ใช้
    พบข้อความ                            *ชื่อผู้ใช้ *กรุณาตั้งชื่อผู้ใช้งาน
    #พบข้อความ *ชื่อผู้ใช้ *กรุณาตั้งชื่อผู้ใช้งาน
    กดปุ่มกากบาทผู้ใช้
    Sleep                                3
    พบจำนวนแถวผุ้ใช้งาน                  2

Test5 ทดสอบการเพิ่มผู้ใช้งานไม่สำเร็จเมื่อกรอกชื่อผู้ใช้ซ้ำ
    เข้าหน้าผู้ใช้งาน
    กดปุ่มเพิ่มผู้ใช้
    เลือกสาขาที่                         1
    กรอกชื่อผู้ใช้                       admin
    กรอกรหัสผ่าน                         112233
    กรอกยืนยันรหัสผ่าน                   112233
    เลือกระดับuser
    เลือกพนักงาน
    เลือกสถานะซ่อน
    กดปุ่มบันทึกผู้ใช้
    พบข้อความ                            *ชื้อผู้ใช้ซ้ำ
    #พบข้อความ *ชื้อผู้ใช้ซ้ำ
    กดปุ่มกากบาทผู้ใช้
    Sleep                                3
    พบจำนวนแถวผุ้ใช้งาน                  2

Test6 ทดสอบการเพิ่มผู้ใช้งานไม่สำเร็จเมื่อไม่กรอกรหัสผ่านแล้วกดบันทึก
    เข้าหน้าผู้ใช้งาน
    กดปุ่มเพิ่มผู้ใช้
    เลือกสาขาที่                         1
    กรอกชื่อผู้ใช้                       test
    กรอกยืนยันรหัสผ่าน                   112233
    เลือกระดับuser
    เลือกพนักงาน
    เลือกสถานะซ่อน
    กดปุ่มบันทึกผู้ใช้
    พบข้อความ                            *กรุณาตั้งรหัสผ่าน
    #พบข้อความ *กรุณาตั้งรหัสผ่าน
    กดปุ่มกากบาทผู้ใช้
    Sleep                                3
    พบจำนวนแถวผุ้ใช้งาน                  2

Test7 ทดสอบการเพิ่มผู้ใช้งานไม่สำเร็จเมื่อกรอกรหัสผ่านน้อยกว่า4ตัวอักษร
    เข้าหน้าผู้ใช้งาน
    กดปุ่มเพิ่มผู้ใช้
    เลือกสาขาที่                         1
    กรอกชื่อผู้ใช้                       test
    กรอกรหัสผ่าน                         112
    กรอกยืนยันรหัสผ่าน                   112233
    เลือกระดับuser
    เลือกพนักงาน
    เลือกสถานะซ่อน
    กดปุ่มบันทึกผู้ใช้
    พบข้อความ                            *รหัสผ่าน ความยาวอย่างน้อย 4 ตัวอักษร
    #พบข้อความ *รหัสผ่าน ความยาวอย่างน้อย 4 ตัวอักษร
    กดปุ่มกากบาทผู้ใช้
    Sleep                                3
    พบจำนวนแถวผุ้ใช้งาน                  2

Test8 ทดสอบการเพิ่มผู้ใช้งานไม่สำเร็จเมื่อไม่กรอกยืนยันรหัสผ่านแล้วกดบันทึก
    เข้าหน้าผู้ใช้งาน
    กดปุ่มเพิ่มผู้ใช้
    เลือกสาขาที่                         1
    กรอกชื่อผู้ใช้                       test
    กรอกรหัสผ่าน                         112233
    เลือกระดับuser
    เลือกพนักงาน
    เลือกสถานะซ่อน
    กดปุ่มบันทึกผู้ใช้
    พบข้อความ                            *ยืนยันรหัสผ่าน *กรุณายืนยันรหัสผ่าน
    #พบข้อความ *ยืนยันรหัสผ่าน *กรุณายืนยันรหัสผ่าน
    กดปุ่มกากบาทผู้ใช้
    Sleep                                3
    พบจำนวนแถวผุ้ใช้งาน                  2

Test9 ทดสอบการเพิ่มผู้ใช้งานไม่สำเร็จเมื่อกรอกยืนยันรหัสผ่านไม่ตรงกับรหัสผ่านแล้วกดบันทึก
    เข้าหน้าผู้ใช้งาน
    กดปุ่มเพิ่มผู้ใช้
    เลือกสาขาที่                         1
    กรอกชื่อผู้ใช้                       test
    กรอกรหัสผ่าน                         112233
    กรอกยืนยันรหัสผ่าน                   112234
    เลือกระดับuser
    เลือกพนักงาน
    เลือกสถานะซ่อน
    กดปุ่มบันทึกผู้ใช้
    พบข้อความ                            *ยืนยันรหัสผ่าน *กรุณายืนยันรหัสผ่านให้ตรงกัน
    #พบข้อความ *ยืนยันรหัสผ่าน *กรุณายืนยันรหัสผ่าน
    กดปุ่มกากบาทผู้ใช้
    Sleep                                3
    พบจำนวนแถวผุ้ใช้งาน                  2

Test10 ทดสอบการเพิ่มผู้ใช้งานไม่สำเร็จเมื่อไม่กรอกยืนยันรหัสผ่านแล้วกดบันทึก
    เข้าหน้าผู้ใช้งาน
    กดปุ่มเพิ่มผู้ใช้
    เลือกสาขาที่                         1
    กรอกชื่อผู้ใช้                       test
    กรอกรหัสผ่าน                         112233
    กรอกยืนยันรหัสผ่าน                   112233
    เลือกพนักงาน
    เลือกสถานะซ่อน
    กดปุ่มบันทึกผู้ใช้
    พบข้อความ                            *ระดับ *กรุณาเลือกระดับผู้ใช้งาน
    #พบข้อความ *ระดับ *กรุณาเลือกระดับผู้ใช้งาน
    กดปุ่มกากบาทผู้ใช้
    Sleep                                3
    พบจำนวนแถวผุ้ใช้งาน                  2

Test11 ทดสอบการเพิ่มผู้ใช้งานสำเร็จเมื่อเลือกระดับเป็น user
    เข้าหน้าผู้ใช้งาน
    กดปุ่มเพิ่มผู้ใช้
    เลือกสาขาที่                         1
    กรอกชื่อผู้ใช้                       test
    กรอกรหัสผ่าน                         112233
    กรอกยืนยันรหัสผ่าน                   112233
    เลือกระดับuser
    เลือกพนักงาน
    กดปุ่มบันทึกผู้ใช้
    Sleep                                3
    พบจำนวนแถวผุ้ใช้งาน                  3
    กดปุ่มแก้ไขผู้ใช้คนที่ 3
    กดปุ่มกากบาทผู้ใช้
    #พบรายการที่เพิ่มในระดับuser

Test12 ทดสอบการเพิ่มผู้ใช้งานสำเร็จเมื่อเลือกระดับเป็น admin
    เข้าหน้าผู้ใช้งาน
    กดปุ่มเพิ่มผู้ใช้
    เลือกสาขาที่                         2
    กรอกชื่อผู้ใช้                       test2
    กรอกรหัสผ่าน                         112233
    กรอกยืนยันรหัสผ่าน                   112233
    เลือกระดับadmin
    เลือกพนักงาน
    กดปุ่มบันทึกผู้ใช้
    Sleep                                3
    พบจำนวนแถวผุ้ใช้งาน                  4
    กดปุ่มแก้ไขผู้ใช้คนที่ 4
    กดปุ่มกากบาทผู้ใช้
    #พบรายการที่เพิ่มในระดับadmin

Test13 ทดสอบการแสดงรายการผู้ใช้งานเมื่อกดปุ่มทั้งหมด
    เข้าหน้าผู้ใช้งาน
    กดปุ่มค้นหาผู้ใช้ทั้งหมด
    Sleep                                3
    พบจำนวนแถวผุ้ใช้งาน                  4
    #พบรายการผู้ใช้งานทั้งหมด

Test14 ทดสอบการแสดงรายการผู้ใช้งานเมื่อกรอกชื่อผู้ใช้แล้วกดปุ่มค้นหา
    เข้าหน้าผู้ใช้งาน
    กรอกชื่อผู้ใช้เพื่อค้นหา             test
    กดปุ่มค้นหาผู้ใช้
    Sleep                                3
    พบจำนวนแถวผุ้ใช้งาน                  1
    #พบรายการ 1 รายการ

Test15 ทดสอบการแสดงรายการผู้ใช้งานเมื่อเลือกสาขาแล้วกดปุ่มค้นหา
    เข้าหน้าผู้ใช้งาน
    เลือกสาขาลาดกระบังเพื่อค้นหา
    กดปุ่มค้นหาผู้ใช้
    Sleep                                3
    พบจำนวนแถวผุ้ใช้งาน                  3
    #พบผู้ใช้งานเฉพาะสาขาที่เลือก

Test16 ทดสอบการแสดงผู้ใช้งานเมื่อกรอกชื่อผู้ใช้และเลือกสาขาตามที่ผู้ใช้งานคนนั้นอยู่
    เข้าหน้าผู้ใช้งาน
    กรอกชื่อผู้ใช้เพื่อค้นหา             test2
    เลือกสาขากิ่งแก้วเพื่อค้นหา
    กดปุ่มค้นหาผู้ใช้
    Sleep                                3
    พบจำนวนแถวผุ้ใช้งาน                  1
    #พบรายการ 1 รายการ

Test17 ทดสอบการแสดงผู้ใช้งานเมื่อกรอกชื่อผู้ใช้และเลือกสาขาตามที่ผู้ใช้งานคนนั้นไม่ได้อยู่
    เข้าหน้าผู้ใช้งาน
    กรอกชื่อผู้ใช้เพื่อค้นหา             test
    เลือกสาขากิ่งแก้วเพื่อค้นหา
    กดปุ่มค้นหาผู้ใช้
    Sleep                                3
    พบจำนวนแถวผุ้ใช้งาน                  0
    #พบรายการ 0 รายการ

Test18 ทดสอบการแก้ไขไม่สำเร็จเมื่อกดปุ่มกากบาท
    เข้าหน้าผู้ใช้งาน
    กดปุ่มแก้ไขผู้ใช้คนที่ 4
    กรอกชื่อผู้ใช้                       test3
    กดปุ่มกากบาทผู้ใช้
    พบข้อความ                            test2
    ไม่พบข้อความ                         test2
    #พบรายการไม่ถูกเปลี่ยนแปลง

Test19 ทดสอบการแก้ไขไม่่สำเร็จเมื่อกดปุ่มยกเลิก
    เข้าหน้าผู้ใช้งาน
    กดปุ่มแก้ไขผู้ใช้คนที่ 4
    กรอกชื่อผู้ใช้                       test3
    กดปุ่มยกเลิกผู้ใช้
    พบข้อความ                            test2
    ไม่พบข้อความ                         test2
    #พบรายการไม่ถูกเปลี่ยนแปลง

# Test20 ทดสอบการแก้ไขไม่สำเร็จเมื่อกรอกชื่อผู้ใช้เป็นค่าว่าง
#                 เข้าหน้าผู้ใช้งาน
#                 กดปุ่มแก้ไขผู้ใช้คนที่ 4
#                 กรอกชื่อผู้ใช้ด้วยค่าว่าง
#                 กรอกรหัสผ่าน                                  112233
#                 กรอกยืนยันรหัสผ่าน                            112233
#                 กดปุ่มบันทึกผู้ใช้
#                 พบข้อความ                                     *ชื่อผู้ใช้ *กรุณาตั้งชื่อผู้ใช้งาน
#                 กดปุ่มกากบาทผู้ใช้
#                 #พบข้อความ *ชื่อผู้ใช้ *กรุณาตั้งชื่อผู้ใช้งาน

Test21 ทดสอบการแก้ไขสำเร็จเมื่อแก้ไขสาขา
    เข้าหน้าผู้ใช้งาน
    กดปุ่มแก้ไขผู้ใช้คนที่ 3
    เลือกสาขาที่                         2
    กรอกรหัสผ่าน                         112233
    กรอกยืนยันรหัสผ่าน                   112233
    กดปุ่มบันทึกผู้ใช้
    Sleep                                3
    เลือกสาขากิ่งแก้วเพื่อค้นหา
    กดปุ่มค้นหาผู้ใช้
    Sleep                                3
    พบจำนวนแถวผุ้ใช้งาน                  2
    #พบรายละเอียดตรงสาขาเป็นสาขาที่ทำการเปลี่ยนแปลง

Test22 ทดสอบการแก้ไขสำเร็จเมื่อแก้ไขชื่อผู้ใช้
    เข้าหน้าผู้ใช้งาน
    กดปุ่มแก้ไขผู้ใช้คนที่ 3
    กรอกชื่อผู้ใช้                       test3
    กรอกรหัสผ่าน                         112233
    กรอกยืนยันรหัสผ่าน                   112233
    กดปุ่มบันทึกผู้ใช้
    Sleep                                3
    กรอกชื่อผู้ใช้เพื่อค้นหา             test3
    กดปุ่มค้นหาผู้ใช้
    Sleep                                3
    พบจำนวนแถวผุ้ใช้งาน                  1
    #พบชื่อผู้ใช้งานที่ทำการเปลี่ยนแปลง

Test23 ทดสอบการแก้ไขสำเร็จเมื่อแก้ไขรหัสผ่าน
    เข้าหน้าผู้ใช้งาน
    กดปุ่มแก้ไขผู้ใช้คนที่ 3
    กรอกรหัสผ่าน                         112234
    กรอกยืนยันรหัสผ่าน                   112234
    กดปุ่มบันทึกผู้ใช้
    Sleep                                5
    ออกจากระบบ
    Wait Until Page Contains Element     id:userInput                                      300
    Sleep                                1
    Input Text                           id:userInput                                      test3
    Sleep                                1
    Wait Until Page Contains Element     id:passwordInput                                  300
    Sleep                                1
    Input Text                           id:passwordInput                                  112234
    Sleep                                1
    Wait Until Page Contains Element     id:btnLogin                                       300
    Sleep                                0.5
    Click Element                        id:btnLogin
    พบข้อความ                            test3
    #สามารถเข้าสู่ระบบด้วยรหัสผ่านใหม่
    ออกจากระบบ
    เข้าสู่ระบบด้วยAdmin

Test24 ทดสอบการแก้ไขสำเร็จเมื่อแก้ไขระดับuserคนอื่นที่ไม่ตัวเอง เป็น admin
    เข้าหน้าผู้ใช้งาน
    กดปุ่มแก้ไขผู้ใช้คนที่ 3
    เลือกระดับadmin
    กรอกรหัสผ่าน                         112233
    กรอกยืนยันรหัสผ่าน                   112233
    กดปุ่มบันทึกผู้ใช้
    Sleep                                3
    กรอกชื่อผู้ใช้เพื่อค้นหา             test3
    กดปุ่มค้นหาผู้ใช้
    Sleep                                3
    พบจำนวนแถวผุ้ใช้งาน                  1
    พบข้อความ                            Admin
    #พบรายการที่เปลี่ยนแปลงอยู่ในระดับ admin
    ออกจากระบบ
    Wait Until Page Contains Element     id:userInput                                      300
    Sleep                                1
    Input Text                           id:userInput                                      test3
    Sleep                                1
    Wait Until Page Contains Element     id:passwordInput                                  300
    Sleep                                1
    Input Text                           id:passwordInput                                  112233
    Sleep                                1
    Wait Until Page Contains Element     id:btnLogin                                       300
    Sleep                                0.5
    Click Element                        id:btnLogin
    เข้าหน้าแฟ้มข้อมูล
    #สามารถเข้าสู่ระบบในสิทธิ์admin
    ออกจากระบบ
    เข้าสู่ระบบด้วยAdmin

Test25 ทดสอบการแก้ไขสำเร็จเมื่อแก้ไขระดับuserคนอื่นที่ไม่ตัวเอง เป็น user
    เข้าหน้าผู้ใช้งาน
    กดปุ่มแก้ไขผู้ใช้คนที่ 3
    เลือกระดับuser
    กรอกรหัสผ่าน                         112233
    กรอกยืนยันรหัสผ่าน                   112233
    กดปุ่มบันทึกผู้ใช้
    Sleep                                3
    กรอกชื่อผู้ใช้เพื่อค้นหา             test3
    กดปุ่มค้นหาผู้ใช้
    Sleep                                3
    พบจำนวนแถวผุ้ใช้งาน                  1
    พบข้อความ                            User
    #พบรายการที่เปลี่ยนแปลงอยู่ในระดับ user
    ออกจากระบบ
    Wait Until Page Contains Element     id:userInput                                      300
    Sleep                                1
    Input Text                           id:userInput                                      test3
    Sleep                                1
    Wait Until Page Contains Element     id:passwordInput                                  300
    Sleep                                1
    Input Text                           id:passwordInput                                  112233
    Sleep                                1
    Wait Until Page Contains Element     id:btnLogin                                       300
    Sleep                                0.5
    Click Element                        id:btnLogin
    Wait Until Element Is Not Visible                   id:btnSetting               300
    #ไม่สามารถเข้าสู่ระบบในสิทธิ์admin
    ออกจากระบบ
    เข้าสู่ระบบด้วยAdmin
    
Test25.1 ทดสอบการไม่แสดงตัวเลือกระดับใน pop up แก้ไขผู้ใช้งานเมื่อเลือกแก้ไขuserตัวเอง
    เข้าหน้าผู้ใช้งาน
    กดปุ่มแก้ไขผู้ใช้คนที่ 1
    Wait Until Element Is Not Visible    id:dropDownPosition
    กดปุ่มกากบาทผู้ใช้
    #ไม่พบตัวเลือกระดับ

Test26 ทดสอบการแก้ไขสำเร็จเมื่อแก้ไขสถานะเป็นInactive
    เข้าหน้าผู้ใช้งาน
    กดปุ่มแก้ไขผู้ใช้คนที่ 3
    กรอกชื่อผู้ใช้                       test3
    กรอกรหัสผ่าน                         112233
    กรอกยืนยันรหัสผ่าน                   112233
    Wait Until Element Is Visible        //*[@id="dropDownStatus"]                         300
    Click Element                        //*[@id="dropDownStatus"]
    Wait Until Element Is Visible        //*[@id="dropDownStatus"]/div[2]/div[2]           300
    Click Element                        //*[@id="dropDownStatus"]/div[2]/div[2]
    กดปุ่มบันทึกผู้ใช้
    Sleep                                3
    เลือกสาขากิ่งแก้วเพื่อค้นหา
    กดปุ่มค้นหาผู้ใช้
    Sleep                                3
    พบจำนวนแถวผุ้ใช้งาน                  1
    #ไม่พบรายการที่ทำการซ่อน
    ออกจากระบบ
    Wait Until Page Contains Element     id:userInput                                      300
    Sleep                                1
    Input Text                           id:userInput                                      test3
    Sleep                                1
    Wait Until Page Contains Element     id:passwordInput                                  300
    Sleep                                1
    Input Text                           id:passwordInput                                  112233
    Sleep                                1
    Wait Until Page Contains Element     id:btnLogin                                       300
    Sleep                                0.5
    Click Element                        id:btnLogin
    พบข้อความ                            ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง!
    #loginใหม่ไม่ได้
    เข้าสู่ระบบด้วยAdmin

Test27 ทดสอบการยกเลิกผู้ใช้งานไม่สำเร็จเมื่อปฏิเสธการยืนยันการลบ
    เข้าหน้าผู้ใช้งาน
    กดปุ่มลบผู้ใช้คนที่ 2
    ตรวจพบข้อความบน Alertแล้วCancel      ยืนยันลบผู้ใช้งานนี้
    Sleep                                3
    พบจำนวนแถวผุ้ใช้งาน                  3
    พบข้อความ                            user
    #รายการที่ต้องการลบยังคงอยู่

Test28 ทดสอบการยกเลิกผู้ใช้งานสำเร็จเมื่อตกลงการยืนยันการลบ
    เข้าหน้าผู้ใช้งาน
    กดปุ่มลบผู้ใช้คนที่ 2
    ตรวจพบข้อความบน Alert                ยืนยันลบผู้ใช้งานนี้
    Sleep                                3
    พบจำนวนแถวผุ้ใช้งาน                  2
    ไม่พบข้อความ                         user
    #รายการที่ต้องการลบหายไป

*** Keywords ***
เข้าหน้าผู้ใช้งาน
    Wait Until Page Contains Element     id:users                                          300                  300
    Click Element                        id:users

กดปุ่มเพิ่มผู้ใช้
    Wait Until Page Contains Element     id:btnAddUsers                                    300
    Sleep                                1
    Click Element                        id:btnAddUsers

กดปุ่มค้นหาผู้ใช้
    Wait Until Page Contains Element     id:btnSearch                                      300
    Sleep                                1
    Click Element                        id:btnSearch

กดปุ่มค้นหาผู้ใช้ทั้งหมด
    Wait Until Page Contains Element     id:btnSearchAll                                   300
    Sleep                                1
    Click Element                        id:btnSearchAll

กดปุ่มแก้ไขผู้ใช้คนที่ 1
    Wait Until Page Contains Element     id:0_btnEdit                                      300
    Sleep                                1
    Click Element                        id:0_btnEdit

กดปุ่มแก้ไขผู้ใช้คนที่ 4
    Wait Until Page Contains Element     id:3_btnEdit                                      300
    Sleep                                1
    Click Element                        id:3_btnEdit

กดปุ่มแก้ไขผู้ใช้คนที่ 3
    Wait Until Page Contains Element     id:2_btnEdit                                      300
    Sleep                                1
    Click Element                        id:2_btnEdit

กดปุ่มลบผู้ใช้คนที่ 2
    Wait Until Page Contains Element     id:1_btnDelete                                    300
    Sleep                                1
    Click Element                        id:1_btnDelete

กดปุ่มกากบาทผู้ใช้
    Wait Until Page Contains Element     id:btnClose                                       300
    Sleep                                1
    Click Element                        id:btnClose

กดปุ่มยกเลิกผู้ใช้
    Wait Until Page Contains Element     id:btnCancelUsers                                 300
    Sleep                                1
    Click Element                        id:btnCancelUsers

กดปุ่มบันทึกผู้ใช้
    Wait Until Page Contains Element     id:btnSaveUsers                                   300
    Sleep                                1
    Click Element                        id:btnSaveUsers

พบจำนวนแถวผุ้ใช้งาน
    [Arguments]                          ${value}
    ${count} =                           Get Element Count                                 id:ID
    ${count_str} =                       Convert To String                                 ${count}
    Should Be Equal                      ${count_str}                                      ${value}

พบชื่อสาขา
    [Arguments]                          ${value}
    ${text} =                            Get Value                                         id:dropDownBranch
    Should Be Equal                      ${text}                                           ${value}


เลือกสาขาที่
    [Arguments]                          ${value}
    Wait Until Element Is Visible        id:dropDownBranch                                 300
    Click Element                        id:dropDownBranch
    Wait Until Element Is Visible        //*[@id="dropDownBranch"]/div[2]/div[${value}]    300
    Click Element                        //*[@id="dropDownBranch"]/div[2]/div[${value}]

เลือกสาขากิ่งแก้วเพื่อค้นหา
    Wait Until Element Is Visible        id:InputSearchBranch                              300
    Click Element                        id:InputSearchBranch
    Wait Until Element Is Visible        //*[@id="InputSearchBranch"]/div[2]/div[2]        300
    Click Element                        //*[@id="InputSearchBranch"]/div[2]/div[2]

เลือกสาขาลาดกระบังเพื่อค้นหา
    Wait Until Element Is Visible        id:InputSearchBranch                              300
    Click Element                        id:InputSearchBranch
    Wait Until Element Is Visible        //*[@id="InputSearchBranch"]/div[2]/div[1]        300
    Click Element                        //*[@id="InputSearchBranch"]/div[2]/div[1]

กรอกชื่อผู้ใช้ด้วยค่าว่าง
    Wait Until Element Is Visible        id:InputUserName                                  300
    Sleep                                1
    Input Text                           id:InputUserName                                  ${Empty}

กรอกชื่อผู้ใช้
    [Arguments]                          ${value}
    Wait Until Element Is Visible        id:InputUserName                                  300
    Sleep                                1
    Input Text                           id:InputUserName                                  ${value}

กรอกชื่อผู้ใช้เพื่อค้นหา
    [Arguments]                          ${value}
    Wait Until Element Is Visible        id:InputSearchUsers                               300
    Sleep                                1
    Input Text                           id:InputSearchUsers                               ${value}

กรอกรหัสผ่านด้วยค่าว่าง
    Wait Until Element Is Visible        id:InputPassword                                  300
    Sleep                                1
    Input Text                           id:InputPassword                                  ${Empty}

กรอกรหัสผ่าน
    [Arguments]                          ${value}
    Wait Until Element Is Visible        id:InputPassword                                  300
    Sleep                                1
    Input Text                           id:InputPassword                                  ${value}

กรอกยืนยันรหัสผ่านด้วยค่าว่าง
    Wait Until Element Is Visible        id:InputPasswordAgain                             300
    Sleep                                1
    Input Text                           id:InputPasswordAgain                             ${Empty}

กรอกยืนยันรหัสผ่าน
    [Arguments]                          ${value}
    Wait Until Element Is Visible        id:InputPasswordAgain                             300
    Sleep                                1
    Input Text                           id:InputPasswordAgain                             ${value}

เลือกระดับadmin
    Wait Until Element Is Visible        id:dropDownPosition                               300
    Click Element                        id:dropDownPosition
    Wait Until Element Is Visible        //*[@id="dropDownPosition"]/div[2]/div[1]         300
    Click Element                        //*[@id="dropDownPosition"]/div[2]/div[1]

เลือกระดับuser
    Wait Until Element Is Visible        id:dropDownPosition                               300
    Click Element                        id:dropDownPosition
    Wait Until Element Is Visible        //*[@id="dropDownPosition"]/div[2]/div[2]         300
    Click Element                        //*[@id="dropDownPosition"]/div[2]/div[2]

เลือกพนักงาน
    Wait Until Element Is Visible        id:dropDownStaff                                  300
    Click Element                        id:dropDownStaff
    Wait Until Element Is Visible        //*[@id="dropDownStaff"]/div[2]/div[2]            300
    Click Element                        //*[@id="dropDownStaff"]/div[2]/div[2]

เลือกสถานะซ่อน
    Wait Until Element Is Visible        id:dropDownStatus                                 300
    Click Element                        id:dropDownStatus
    Wait Until Element Is Visible        //*[@id="dropDownStatus"]/div[2]/div[2]           300
    Click Element                        //*[@id="dropDownStatus"]/div[2]/div[2]




