*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบAdminและinit_data  
Test Setup        เข้าหน้าแฟ้มข้อมูล
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data_admin
*** Variables ***

*** Test cases ***
Test1 ทดสอบการเพิ่มธนาคารไม่สำเร็จเมื่อกดปุ่มกากบาท
    เข้าหน้าธนาคาร
    กดปุ่มเพิ่มธนาคาร
    กรอกชื่อธนาคาร              bank
    กดปุ่มกากบาทธนาคาร
    พบจำนวนแถวธนาคาร                   4
    #ไม่พบรายการที่เพิ่มล่าสุด

Test2 ทดสอบการเพิ่มธนาคารไม่สำเร็จเมื่อกดปุ่มยกเลิก
    เข้าหน้าธนาคาร
    กดปุ่มเพิ่มธนาคาร
    กรอกชื่อธนาคาร              bank
    กดปุ่มยกเลิกธนาคาร
    Sleep                               3
    พบจำนวนแถวธนาคาร                  4
    #ไม่พบรายการที่เพิ่มล่าสุด

Test3 ทดสอบการเพิ่มธนาคารไม่สำเร็จเมื่อไม่กรอกชื่อธนาคาร
    เข้าหน้าธนาคาร
    กดปุ่มเพิ่มธนาคาร
    กดปุ่มบันทึกธนาคาร
    พบข้อความ                          *ชื่อธนาคาร *ต้องไม่เป็นค่าว่าง
    กดปุ่มกากบาทธนาคาร
    Sleep                               3
    พบจำนวนแถวธนาคาร                   4
    #พบข้อความ *ชื่อธนาคาร *ต้องไม่เป็นค่าว่าง

Test4 ทดสอบการเพิ่มธนาคารสำเร็จเมื่อกรอกข้อมูลครบ
    เข้าหน้าธนาคาร
    กดปุ่มเพิ่มธนาคาร
    กรอกชื่อธนาคาร              bank
    กดปุ่มบันทึกธนาคาร
    Sleep                               3
    #ไม่พบ popupธนาคาร
    พบจำนวนแถวธนาคาร                   5
    พบข้อความ                           bank
    #พบรายการธนาคารที่เพิ่มเข้ามา

Test5 ทดสอบการแก้ไขธนาคารไม่สำเร็จเมื่อกดปุ่มกากบาท
    เข้าหน้าธนาคาร
    กดปุ่มแก้ไขธนาคาร2
    กรอกชื่อธนาคาร              bank2
    กดปุ่มกากบาทธนาคาร
    Sleep                               3
    พบจำนวนแถวธนาคาร                    5
    พบข้อความ                           bank
    #ไม่พบความเปลี่ยนแปลงของโรงงานที่ทำการแก้ไข

Test6 ทดสอบการแก้ไขธนาคารไม่สำเร็จเมื่อกดปุ่มยกเลิก
    เข้าหน้าธนาคาร
    กดปุ่มแก้ไขธนาคาร2
    กรอกชื่อธนาคาร              bank2
    กดปุ่มยกเลิกธนาคาร
    Sleep                               3
    พบจำนวนแถวธนาคาร                    5
    พบข้อความ                           bank
    #ไม่พบความเปลี่ยนแปลงของโรงงานที่ทำการแก้ไข

# Test7 ทดสอบการแก้ไขธนาคารไม่สำเร็จเมื่อกรอกชื่อธนาคารเป็นค่าวาง
#                 เข้าหน้าธนาคาร 
#                 กดปุ่มแก้ไขธนาคาร 2
#                 กรอกชื่อธนาคาร ด้วยค่าว่าง
#                 กดปุ่มบันทึกธนาคาร
#                 พบข้อความ                                     *ชื่อธนาคาร *ต้องไม่เป็นค่าว่าง
#                 #พบข้อความ *ชื่อธนาคาร *ต้องไม่เป็นค่าว่าง
#                 กดปุ่มกากบาทธนาคาร

Test8 ทดสอบการแก้ไขธนาคารสำเร็จเมื่อกรอกข้อมูลครบ
    เข้าหน้าธนาคาร
    กดปุ่มแก้ไขธนาคาร2
    กรอกชื่อธนาคาร              bank2
    กดปุ่มบันทึกธนาคาร  
    Sleep                               3
    #ไม่พบ popup ธนาคาร  
    พบจำนวนแถวธนาคาร                    5
    พบข้อความ                           bank2
    #พบความเปลี่ยนแปลงของโรงงานที่ทำการแก้ไข

Test9 ทดสอบการยกเลิกธนาคารไม่สำเร็จเมื่อปฏิเสธการยืนยันการลบ
    เข้าหน้าธนาคาร
    กดปุ่มลบธนาคาร2
    Sleep                               5
    ตรวจพบข้อความบน Alertแล้วCancel     ยืนยันลบ
    พบจำนวนแถวธนาคาร                   5
    พบข้อความ                           bank2
    #รายการที่ต้องการลบยังคงอยู่

Test10 ทดสอบการยกเลิกธนาคารสำเร็จเมื่อตกลงการยืนยันการลบ
    เข้าหน้าธนาคาร
    กดปุ่มลบธนาคาร2
    Sleep                               5
    ตรวจพบข้อความบน Alert               ยืนยันลบ
    Sleep                               3
    พบจำนวนแถวธนาคาร               4
    ไม่พบข้อความ                        bank2
    #รายการที่ต้องการลบหายไป

*** Keywords ***
เข้าหน้าธนาคาร
    Sleep                               2
    Wait Until Page Contains Element    id:bank                                        300
    Click Element                       id:bank

กดปุ่มเพิ่มธนาคาร
    Wait Until Page Contains Element    id:btnAddBank                                   300
    Sleep                               1
    Click Element                       id:btnAddBank

กดปุ่มกากบาทธนาคาร
    Sleep                               2
    Wait Until Page Contains Element    id:btnCloseBank                                 300
    Click Element                       id:btnCloseBank

กดปุ่มยกเลิกธนาคาร
    Sleep                               2
    Wait Until Page Contains Element    id:btnCancelBank                                300
    Click Element                       id:btnCancelBank

กดปุ่มบันทึกธนาคาร
    Sleep                               2
    Wait Until Page Contains Element    id:btnSaveBank                                  300
    Click Element                       id:btnSaveBank

กรอกชื่อธนาคาร
    [Arguments]                         ${value}
    Sleep                               2
    Wait Until Element Is Visible       id:inputBankName
    Input Text                          id:inputBankName                                ${value}

กรอกชื่อธนาคารด้วยค่าว่าง
    Wait Until Element Is Visible       id:inputBankName                                300
    Sleep                               1
    Input Text                          id:inputBankName                                ${Empty}

พบชื่อธนาคาร
    [Arguments]                         ${value}
    ${text} =                           Get Value                                         id:inputBankName
    Should Be Equal                     ${text}                                           ${value}

พบจำนวนแถวธนาคาร
    [Arguments]                         ${value}
    ${count} =                          Get Element Count                                 id:branchID
    ${count_str} =                      Convert To String                                 ${count}
    Should Be Equal                     ${count_str}                                      ${value}

กดปุ่มแก้ไขธนาคาร1
    Wait Until Page Contains Element    id:3_btnEdit                                      300
    Sleep                               1
    Click Element                       id:3_btnEdit

กดปุ่มแก้ไขธนาคาร2
    Wait Until Page Contains Element    id:4_btnEdit                                      300
    Sleep                               1
    Click Element                       id:4_btnEdit

กดปุ่มลบธนาคาร1
    Wait Until Page Contains Element    id:3_btnDelete                                    300
    Sleep                               1
    Click Element                       id:3_btnDelete

กดปุ่มลบธนาคาร2
    Wait Until Page Contains Element    id:4_btnDelete                                    300
    Sleep                               1
    Click Element                       id:4_btnDelete
