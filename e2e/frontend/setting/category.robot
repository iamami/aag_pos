*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบAdminและinit_data  
Test Setup        เข้าหน้าแฟ้มข้อมูล
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data_admin
*** Variables ***

*** Test cases ***
Test1 ทดสอบการเพิ่มกลุ่มสินค้าไม่สำเร็จเมื่อกดปุ่มกากบาท
    เข้าหน้ากลุ่มสินค้า
    Sleep                               3
    กดปุ่มเพิ่มสินค้า
    Sleep                               3
    กรอกรหัสสินค้า                      99
    Sleep                               3
    กดกากบาทหน้าเพิ่มกลุ่มสินค้า
    Sleep                               3
    ไม่พบข้อความ                        99

Test2 ทดสอบการเพิ่มกลุ่มสินค้าไม่สำเร็จเมื่อกดปุ่มยกเลิก
    เข้าหน้ากลุ่มสินค้า
    Sleep                               3
    กดปุ่มเพิ่มสินค้า
    Sleep                               3
    กรอกรหัสสินค้า                      99
    Sleep                               3
    กดยกเลิกหน้าเพิ่มกลุ่มสินค้า
    Sleep                               3
    ไม่พบข้อความ                        99

Test3 ทดสอบการเพิ่มกลุ่มสินค้าไม่สำเร็จเมื่อไม่กรอกรหัส
    เข้าหน้ากลุ่มสินค้า
    Sleep                               3
    กดปุ่มเพิ่มสินค้า
    Sleep                               3
    กรอกชื่อสินค้าหน้าเพิ่มสินค้า       99
    Sleep                               3
    กรอกน้ำหนัก(กรัม)/1บาท              15
    Sleep                               3
    กรอกตัวคูณซื้อทอง                   1
    Sleep                               3
    กรอกตัวคูณขายทอง                    1
    Sleep                               3
    กรอกค่าหักรับซื้อทองเก่า            0
    Sleep                               3
    กดยืนยันหน้าเพิ่มกลุ่มสินค้า
    Sleep                               3
    พบข้อความ                           *รหัส ต้องไม่เป็นค่าว่าง
    Sleep                               3
    กดกากบาทหน้าเพิ่มกลุ่มสินค้า

Test4 ทดสอบการเพิ่มกลุ่มสินค้าไม่สำเร็จเมื่อไม่กรอกชื่อ
    เข้าหน้ากลุ่มสินค้า
    Sleep                               3
    กดปุ่มเพิ่มสินค้า
    Sleep                               3
    กรอกรหัสสินค้า                      99
    Sleep                               3
    กรอกน้ำหนัก(กรัม)/1บาท              15
    Sleep                               3
    กรอกตัวคูณซื้อทอง                   1
    Sleep                               3
    กรอกตัวคูณขายทอง                    1
    Sleep                               3
    กรอกค่าหักรับซื้อทองเก่า            0
    Sleep                               3
    กดยืนยันหน้าเพิ่มกลุ่มสินค้า
    Sleep                               3
    พบข้อความ                           *ชื่อ ต้องไม่เป็นค่าว่าง
    Sleep                               3
    กดกากบาทหน้าเพิ่มกลุ่มสินค้า

Test5 ทดสอบการเพิ่มกลุ่มสินค้าไม่สำเร็จเมื่อกรอกรหัสซ้ำ
    เข้าหน้ากลุ่มสินค้า
    Sleep                               3
    กดปุ่มเพิ่มสินค้า
    Sleep                               3
    กรอกรหัสสินค้า                      90
    Sleep                               3
    กดยืนยันหน้าเพิ่มกลุ่มสินค้า
    Sleep                               3
    พบข้อความ                           *รหัส category และ code มีอยู่แล้ว
    Sleep                               3
    กดกากบาทหน้าเพิ่มกลุ่มสินค้า

Test6 ทดสอบการเพิ่มกลุ่มสินค้าไม่สำเร็จเมื่อกรอกชื่อซ้ำ
    เข้าหน้ากลุ่มสินค้า
    Sleep                               3
    กดปุ่มเพิ่มสินค้า
    Sleep                               3
    กรอกชื่อสินค้าหน้าเพิ่มสินค้า       90
    Sleep                               3
    กดยืนยันหน้าเพิ่มกลุ่มสินค้า
    Sleep                               3
    พบข้อความ                           *ชื่อ category และ name มีอยู่แล้ว
    Sleep                               3
    กดกากบาทหน้าเพิ่มกลุ่มสินค้า

Test7 ทดสอบการเพิ่มกลุ่มสินค้าไม่สำเร็จเมื่อกรอกน้ำหนัก(กรัม)/1บาทติดลบ
    เข้าหน้ากลุ่มสินค้า
    Sleep                               3
    กดปุ่มเพิ่มสินค้า
    Sleep                               3
    กรอกน้ำหนัก(กรัม)/1บาท              -1
    Sleep                               3
    กดยืนยันหน้าเพิ่มกลุ่มสินค้า
    Sleep                               3
    พบข้อความ                           น้ำหนัก(กรัม)/1บาท ต้องไม่ติดลบ
    Sleep                               3
    กดกากบาทหน้าเพิ่มกลุ่มสินค้า

Test8 ทดสอบการเพิ่มกลุ่มสินค้าไม่สำเร็จเมื่อกรอกตัวคูณซื้อทอง ติดลบ
    เข้าหน้ากลุ่มสินค้า
    Sleep                               3
    กดปุ่มเพิ่มสินค้า
    Sleep                               3
    กรอกตัวคูณซื้อทอง                   -1
    Sleep                               3
    กดยืนยันหน้าเพิ่มกลุ่มสินค้า
    Sleep                               3
    พบข้อความ                           ตัวคูณซื้อทอง ต้องไม่ติดลบ
    Sleep                               3
    กดกากบาทหน้าเพิ่มกลุ่มสินค้า

Test9 ทดสอบการเพิ่มกลุ่มสินค้าไม่สำเร็จเมื่อตัวคูณขายทอง ติดลบ
    เข้าหน้ากลุ่มสินค้า
    Sleep                               3
    กดปุ่มเพิ่มสินค้า
    Sleep                               3
    กรอกตัวคูณขายทอง                    -1
    Sleep                               3
    กดยืนยันหน้าเพิ่มกลุ่มสินค้า
    Sleep                               3
    พบข้อความ                           ตัวคูณขายทอง ต้องไม่ติดลบ
    Sleep                               3
    กดกากบาทหน้าเพิ่มกลุ่มสินค้า

Test10 ทดสอบการเพิ่มกลุ่มสินค้าไม่สำเร็จเมื่อกรอกรหัสมากกว่า5ตัวอักษร
    เข้าหน้ากลุ่มสินค้า
    Sleep                               3
    กดปุ่มเพิ่มสินค้า
    Sleep                               3
    กรอกรหัสสินค้า                      999999
    Sleep                               3
    กดยืนยันหน้าเพิ่มกลุ่มสินค้า
    Sleep                               3
    พบข้อความ                           *รหัส ความยาวต้องไม่มากกว่า5ตัวอักษร
    Sleep                               3
    กดกากบาทหน้าเพิ่มกลุ่มสินค้า

Test11 ทดสอบการเพิ่มกลุ่มสินค้าไม่สำเร็จเมื่อไม่กรอกชื่อ
    เข้าหน้ากลุ่มสินค้า
    Sleep                               3
    กดปุ่มเพิ่มสินค้า
    Sleep                               3
    กรอกชื่อสินค้าหน้าเพิ่มสินค้า       98
    Sleep                               3
    กรอกรหัสสินค้า                      98
    Sleep                               3
    กรอกน้ำหนัก(กรัม)/1บาท              15
    Sleep                               3
    กรอกตัวคูณซื้อทอง                   1
    Sleep                               3
    กรอกตัวคูณขายทอง                    1
    Sleep                               3
    กรอกค่าหักรับซื้อทองเก่า            0
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="status_category"]/i                300
    Sleep                               3
    Click Element                       //*[@id="status_category"]/i
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="status_category"]/div[2]/div[2]/span                   300
    Sleep                               3
    Click Element                       //*[@id="status_category"]/div[2]/div[2]/span
    Sleep                               3
    กดยืนยันหน้าเพิ่มกลุ่มสินค้า
    Sleep                               3
    ไม่พบข้อความ                        98

Test13 ทดสอบการเพิ่มกลุ่มสินค้าสำเร็จเมื่อกรอกข้อมูลครบ
    เข้าหน้ากลุ่มสินค้า
    Sleep                               3
    กดปุ่มเพิ่มสินค้า
    Sleep                               3
    กรอกชื่อสินค้าหน้าเพิ่มสินค้า       99
    Sleep                               3
    กรอกรหัสสินค้า                      99
    Sleep                               3
    กรอกน้ำหนัก(กรัม)/1บาท              15
    Sleep                               3
    กรอกตัวคูณซื้อทอง                   1
    Sleep                               3
    กรอกตัวคูณขายทอง                    1
    Sleep                               3
    กรอกค่าหักรับซื้อทองเก่า            0
    Sleep                               3
    กดยืนยันหน้าเพิ่มกลุ่มสินค้า
    Sleep                               3
    พบข้อความ                           99

# แก้ไขกลุ่มสินค้า
Test14 ทดสอบการแก้ไขกลุ่มสินค้าไม่สำเร็จเมื่อกดปุ่มกากบาท
    เข้าหน้ากลุ่มสินค้า
    Sleep                               3
    กดแก้ไขสินค้า
    Sleep                               3
    กรอกชื่อสินค้าหน้าเพิ่มสินค้า       100
    Sleep                               3
    กดกากบาทหน้าเพิ่มกลุ่มสินค้า
    Sleep                               3
    ไม่พบข้อความ                        300

Test15 ทดสอบการแก้ไขกลุ่มสินค้าไม่สำเร็จเมื่อกดปุ่มยกเลิก
    เข้าหน้ากลุ่มสินค้า
    Sleep                               3
    กดแก้ไขสินค้า
    Sleep                               3
    กรอกชื่อสินค้าหน้าเพิ่มสินค้า       100
    Sleep                               3
    กดยกเลิกหน้าเพิ่มกลุ่มสินค้า
    Sleep                               3
    ไม่พบข้อความ                        300

Test18 ทดสอบการแก้ไขกลุ่มสินค้าไม่สำเร็จเมื่อกรอกตัวคูณซื้อทอง ติดลบ
    เข้าหน้ากลุ่มสินค้า
    Sleep                               3
    กดแก้ไขสินค้า
    Sleep                               3
    กรอกน้ำหนัก(กรัม)/1บาท              -1
    Sleep                               3
    กดยืนยันหน้าเพิ่มกลุ่มสินค้า
    Sleep                               3
    พบข้อความ                           น้ำหนัก(กรัม)/1บาท ต้องไม่ติดลบ
    Sleep                               3
    กดกากบาทหน้าเพิ่มกลุ่มสินค้า

Test19 ทดสอบการแก้ไขกลุ่มสินค้าไม่สำเร็จเมื่อกรอกตัวคูณซื้อทอง ติดลบ
    เข้าหน้ากลุ่มสินค้า
    Sleep                               3
    กดแก้ไขสินค้า
    Sleep                               3
    กรอกตัวคูณซื้อทอง                   -1
    Sleep                               3
    กดยืนยันหน้าเพิ่มกลุ่มสินค้า
    Sleep                               3
    พบข้อความ                           ตัวคูณซื้อทอง ต้องไม่ติดลบ
    Sleep                               3
    กดกากบาทหน้าเพิ่มกลุ่มสินค้า

Test20 ทดสอบการแก้ไขกลุ่มสินค้าไม่สำเร็จเมื่อตัวคูณขายทอง ติดลบ
    เข้าหน้ากลุ่มสินค้า
    Sleep                               3
    กดแก้ไขสินค้า
    Sleep                               3
    กรอกตัวคูณขายทอง                    -1
    Sleep                               3
    กดยืนยันหน้าเพิ่มกลุ่มสินค้า
    Sleep                               3
    พบข้อความ                           ตัวคูณขายทอง ต้องไม่ติดลบ
    Sleep                               3
    กดกากบาทหน้าเพิ่มกลุ่มสินค้า

Test21 ทดสอบการแก้ไขกลุ่มสินค้าสำเร็จเมื่อเลือกสถานะเป็นซ่อน
    เข้าหน้ากลุ่มสินค้า
    Sleep                               3
    กดแก้ไขสินค้า
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="status_category"]/i                        300
    Sleep                               3
    Click Element                       //*[@id="status_category"]/i
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="status_category"]/div[2]/div[2]/span                               300
    Sleep                               3
    Click Element                       //*[@id="status_category"]/div[2]/div[2]/span
    Sleep                               3
    กดยืนยันหน้าเพิ่มกลุ่มสินค้า
    Sleep                               3
    ไม่พบข้อความ                        90

# ลบรายการสินค้า
Test22 ทดสอบการยกเลิกกลุ่มสินค้าไม่สำเร็จเมื่อปฏิเสธการยืนยันการลบ
    เข้าหน้ากลุ่มสินค้า
    Sleep                               3
    กดยกเลิกสินค้า
    Sleep                               3
    ตรวจพบข้อความบน Alertแล้วCancel     ยืนยันลบ
    Sleep                               3
    พบข้อความ                           96

Test23 ทดสอบการยกเลิกกลุ่มสินค้าไม่สำเร็จเมื่อกดยกเลิกกลุ่มสินค้าที่ถูกใช้งานอยู่
    เข้าหน้ากลุ่มสินค้า
    Sleep                               3
    กดยกเลิกสินค้า
    Sleep                               3
    ตรวจพบข้อความบน Alert               ยืนยันลบ
    Sleep                               3
    ตรวจพบข้อความบน Alert               ไม่สามารถลบกลุ่มสินค้าได้ เนื่องจากถูกใช้งานแล้ว
    Sleep                               3
    พบข้อความ                           96

Test24 ทดสอบการยกเลิกกลุ่มสินค้าสำเร็จเมื่อตกลงการยืนยันการลบ
    เข้าหน้ากลุ่มสินค้า
    Sleep                               3
    Wait Until Page Contains Element    //*[@id="content-body"]/div/div[2]/div/div[3]/div[3]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[2]                              300
    Sleep                               3
    Click Element                       //*[@id="content-body"]/div/div[2]/div/div[3]/div[3]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[2]
    Sleep                               3
    ตรวจพบข้อความบน Alert               ยืนยันลบ
    Sleep                               3
    ไม่พบข้อความ                        99


*** Keywords ***
เข้าหน้ากลุ่มสินค้า
    Sleep                               2
    Wait Until Page Contains Element    id:category                                                                                                      300
    Click Element                       id:category

กดกากบาทหน้าเพิ่มกลุ่มสินค้า
    Sleep                               2
    Wait Until Page Contains Element    id:close_catagory                                                                                                300
    Click Element                       id:close_catagory

กดยกเลิกหน้าเพิ่มกลุ่มสินค้า
    Sleep                               2
    Wait Until Page Contains Element    id:cancel_category                                                                                               300
    Click Element                       id:cancel_category

กดยืนยันหน้าเพิ่มกลุ่มสินค้า
    Sleep                               2
    Wait Until Page Contains Element    id:confirm_category                                                                                              300
    Click Element                       id:confirm_category

กดปุ่มเพิ่มสินค้า
    Sleep                               2
    Wait Until Page Contains Element    id:addCategory                                                                                                   300
    Click Element                       id:addCategory

กรอกรหัสสินค้า
    [Arguments]                         ${value}
    Sleep                               2
    Wait Until Element Is Visible       id:categoryID                                                                                                    300
    Input Text                          id:categoryID                                                                                                    ${value}

กรอกชื่อสินค้าหน้าเพิ่มสินค้า
    [Arguments]                         ${value}
    Sleep                               2
    Wait Until Element Is Visible       id:name_category
    Input Text                          id:name_category                                                                                                 ${value}

กรอกน้ำหนัก(กรัม)/1บาท
    [Arguments]                         ${value}
    Sleep                               2
    Wait Until Element Is Visible       id:weight_category
    Input Text                          id:weight_category                                                                                               ${value}

กรอกตัวคูณซื้อทอง
    [Arguments]                         ${value}
    Sleep                               2
    Wait Until Element Is Visible       id:m_buy_category
    Input Text                          id:m_buy_category                                                                                                ${value}

กรอกตัวคูณขายทอง
    [Arguments]                         ${value}
    Sleep                               2
    Wait Until Element Is Visible       id:m_sell_category
    Input Text                          id:m_sell_category                                                                                               ${value}

กรอกค่าหักรับซื้อทองเก่า
    [Arguments]                         ${value}
    Sleep                               2
    Wait Until Element Is Visible       id:discount_buy_category
    Input Text                          id:discount_buy_category                                                                                         ${value}

กดแก้ไขสินค้า
    Sleep                               2
    Wait Until Page Contains Element    //*[@id="content-body"]/div/div[2]/div[1]/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]
    Click Element                       //*[@id="content-body"]/div/div[2]/div[1]/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]

กดยกเลิกสินค้า
    Sleep                               2
    Wait Until Page Contains Element    //*[@id="content-body"]/div/div[2]/div[1]/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[2]
    Click Element                       //*[@id="content-body"]/div/div[2]/div[1]/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[2]
