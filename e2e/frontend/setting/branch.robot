*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบAdminและinit_data         
Test Setup        เข้าหน้าแฟ้มข้อมูล
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data_admin
*** Variables ***

*** Test cases ***
Test1 ทดสอบการแสดงรหัสสาขาเมื่อสร้างรหัสสาขา
    Sleep                               3
    กดปุ่มเพิ่มสาขา
    Sleep                               3
    กดปุ่มสร้างสาขา
    Sleep                               3
    พบรหัสสาขา                          003
    Sleep                               3
    กดปุ่มยกเลิกสาขา
    #พบเลขรหัสล่าสุด (12)

Test2 ทดสอบการแสดงรหัสสาขาเมื่อสร้างสาขาแล้วกดปิดโดยไม่บันทึก แล้วกดสร้างสาขาใหม่อีกครั้ง
    Sleep                               3
    กดปุ่มเพิ่มสาขา
    Sleep                               3
    กดปุ่มสร้างสาขา
    Sleep                               3
    กดปุ่มกากบาทสาขา
    Sleep                               3
    กดปุ่มเพิ่มสาขา
    Sleep                               3
    กดปุ่มสร้างสาขา
    Sleep                               3
    พบรหัสสาขา                          003
    Sleep                               3
    กดปุ่มยกเลิกสาขา
    #พบเลขรหัสล่าสุด (12)

Test3 ทดสอบการสร้างสาขาไม่สำเร็จเมื่อไม่กรอกรหัสสาขา
    Sleep                               3
    กดปุ่มเพิ่มสาขา
    Sleep                               3
    กรอกชื่อสาขา                        ลาดกระบัง2
    Sleep                               3
    กดปุ่มบันทึกสาขา
    Sleep                               3
    พบข้อความ                           *รหัสสาขา *ต้องไม่เป็นค่าว่าง
    Sleep                               3
    #พบข้อความ *รหัสสาขา *ต้องไม่เป็นค่าว่าง
    กดปุ่มกากบาทสาขา

Test4 ทดสอบการสร้างสาขาไม่สำเร็จเมื่อไม่กรอกชื่อสาขา
    Sleep                               3
    กดปุ่มเพิ่มสาขา
    Sleep                               3
    กดปุ่มสร้างสาขา
    Sleep                               3
    กดปุ่มบันทึกสาขา
    Sleep                               3
    พบข้อความ                           *ชื่อสาขา *ต้องไม่เป็นค่าว่าง
    Sleep                               3
    #พบข้อความ *ชื่อสาขา  *ต้องไม่เป็นค่าว่าง
    Sleep                               3
    กดปุ่มกากบาทสาขา

Test5 ทดสอบการสร้างสาขาไม่สำเร็จเมื่อกดยกเลิก
    Sleep                               3
    กดปุ่มเพิ่มสาขา
    Sleep                               3
    กดปุ่มยกเลิกสาขา
    Sleep                               3
    พบหน้าสาขา
    Sleep                               3
    #พบหน้าสาขา
    พบจำนวนแถวสาขา                      2
    #ไม่พบสาขาที่ทำรายการ

Test6 ทดสอบการสร้างสาขาไม่สำเร็จเมื่อกดกากบาท
    Sleep                               3
    กดปุ่มเพิ่มสาขา
    Sleep                               3
    กดปุ่มกากบาทสาขา
    Sleep                               3
    พบหน้าสาขา
    Sleep                               3
    #พบหน้าสาขา
    พบจำนวนแถวสาขา                      2
    #ไม่พบสาขาที่ทำรายการ

Test7 ทดสอบการสร้างสาขาสำเร็จเมื่อกดบันทึก
    Sleep                               3
    กดปุ่มเพิ่มสาขา
    Sleep                               3
    กดปุ่มสร้างสาขา
    Sleep                               3
    กรอกชื่อสาขา                        ลาดกระบัง3
    Sleep                               3
    กดปุ่มบันทึกสาขา
    Sleep                               3
    พบหน้าสาขา
    Sleep                               3
    #พบหน้าสาขา
    พบจำนวนแถวสาขา                      3
    #พบสาขาที่ทำรายการ

Test8 ทดสอบการ importไม่สำเร็จเมื่อกดปุ่มกากบาท
    Sleep                               3
    กดปุ่มimport
    Sleep                               3
    กดปุ่มกากบาทimport
    Sleep                               3
    พบหน้าสาขา
    Sleep                               3
    #พบหน้าสาขา
    พบจำนวนแถวสาขา                      3
    #ไม่มีข้อมูลสาขาเพิ่ม

Test9 ทดสอบการ importไม่สำเร็จเมื่อกดปุ่มกากบาท
    Sleep                               3
    กดปุ่มimport
    Sleep                               3
    กดปุ่มบันทึกimport
    Sleep                               3
    พบข้อความ                           Import CSV
    Sleep                               3
    #หน้า pop up Import CSV ยังคงอยู่
    กดปุ่มกากบาทimport
    Sleep                               3
    พบจำนวนแถวสาขา                      3
    #ไม่มีข้อมูลสาขาเพิ่ม

Test15 ทดสอบการแสดงข้อมูลตรงกับสาขาที่ต้องการแก้ไข
    Sleep                               3
    กดปุ่มแก้ไขสาขา1
    Sleep                               3
    พบรหัสสาขา                          001
    Sleep                               3
    พบชื่อสาขา                          ลาดกระบัง
    Sleep                               3
    กดปุ่มกากบาทสาขา
    #พบรายละเอียดสาขาถูกต้อง

Test16 ทดสอบการไม่สามารถแก้ไขรหัสสาขาได้
    Sleep                               3
    กดปุ่มแก้ไขสาขา1
    Sleep                               3
    ${text} =                           Get Element Attribute                         //*[@id="inputBranchID"]    readonly
    Should Be Equal As Strings          ${text}                                       true
    #ไม่สามารถแก้ไขรหัสสาขาได้
    Sleep                               3
    กดปุ่มกากบาทสาขา

Test17 ทดสอบการแก้ไขสาขาไม่สำเร็จเมื่อกดยกเลิก
    Sleep                               3
    กดปุ่มแก้ไขสาขา1
    Sleep                               3
    กรอกชื่อสาขา                        ลาดกระบังแก้ไข
    Sleep                               3
    กดปุ่มยกเลิกแก้ไขสาขา
    Sleep                               3
    พบข้อความ                           001
    Sleep                               3
    พบข้อความ                           ลาดกระบัง
    #พบข้อมูลรายการที่ทำการแก้ไขไม่ถูกเปลี่ยนแปลง

Test18 ทดสอบการแก้ไขสาขาไม่สำเร็จเมื่อกดกากบาท
    Sleep                               3
    กดปุ่มแก้ไขสาขา1
    Sleep                               3
    กรอกชื่อสาขา                        ลาดกระบังแก้ไข
    Sleep                               3
    กดปุ่มกากบาทแก้ไขสาขา
    Sleep                               3
    พบข้อความ                           001
    Sleep                               3
    พบข้อความ                           ลาดกระบัง
    #พบข้อมูลรายการที่ทำการแก้ไขไม่ถูกเปลี่ยนแปลง

# Test19 ทดสอบการแก้ไขสาขาไม่สำเร็จเมื่อกรอกชื่อสาขาเป็นค่าว่าง
#     กดปุ่มแก้ไขสาขา1
#     กรอกชื่อสาขาด้วยค่าว่าง
#     กดปุ่มบันทึกแก้ไขสาขา
#     พบข้อความ                           *ชื่อสาขา *ต้องไม่เป็นค่าว่าง
#     กดปุ่มกากบาทแก้ไขสาขา
#     #พบข้อความ *ชื่อสาขา *ต้องไม่เป็นค่าว่าง

Test20 ทดสอบการแก้ไขสาขาสำเร็จเมื่อแก้ไขชื่อสาขาแล้วกดบันทึก
    Sleep                               3
    กดปุ่มแก้ไขสาขา1
    Sleep                               3
    กรอกชื่อสาขา                        ลาดกระบังแก้ไข
    Sleep                               3
    กดปุ่มบันทึกแก้ไขสาขา
    Sleep                               3
    พบข้อความ                           ลาดกระบังแก้ไข
    Sleep                               3
    ไม่พบข้อความ                        ลาดกระบัง
    #พบข้อมูลรายการที่ทำการแก้ไขถูกเปลี่ยนแปลง

Test21 ทดสอบการแก้ไขสาขาสำเร็จเมื่อเปลี่ยนสถานะเป็นซ่อนแล้วกดบันทึก
    Sleep                               3
    กดปุ่มแก้ไขสาขา1
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="inputBranchStatus"]                  300
    Sleep                               3
    Click Element                       //*[@id="inputBranchStatus"]  
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="inputBranchStatus"]/div[2]/div[2]    300
    Sleep                               3
    Click Element                       //*[@id="inputBranchStatus"]/div[2]/div[2]
    Sleep                               3
    กดปุ่มบันทึกแก้ไขสาขา
    Sleep                               3
    ไม่พบข้อความ                        ลาดกระบังแก้ไข
    Sleep                               3
    ไม่พบข้อความ                        001
    #พบข้อมูลรายการที่ทำการแก้ไขถูกเปลี่ยนแปลง

Test22 ทดสอบการลบสาขาไม่สำเร็จเมื่อกดปฏิเสธยืนยันการลบ
    Sleep                               3
    กดปุ่มลบสาขา1
    Sleep                               5
    ตรวจพบข้อความบน Alertแล้วCancel     ยืนยันลบ
    Sleep                               3
    พบข้อความ                           กิ่งแก้ว
    Sleep                               5
    พบข้อความ                           002
    #พบสาขาที่ทำรายการ

Test23 ทดสอบการลบสาขาไม่สำเร็จเมื่อกดตกลงยืนยันการลบ สาขาที่ถูกใช้งานไปแล้ว
    Sleep                               3
    กดปุ่มลบสาขา1
    Sleep                               5
    ตรวจพบข้อความบน Alert               ยืนยันลบ
    Sleep                               5
    ตรวจพบข้อความบน Alert               ไม่สามารถลบสาขาได้ เนื่องจากถูกใช้งานแล้ว
    Sleep                               3
    #พบ alert ไม่สามารถลบสาขาได้ เนื่องจากถูกใช้งานแล้ว
    พบข้อความ                           กิ่งแก้ว
    Sleep                               3
    พบข้อความ                           002

Test24 ทดสอบการลบสาขาสำเร็จเมื่อกดตกลงยืนยันการลบ สาขาที่ยังไม่ได้ใช้งาน
    Sleep                               3
    กดปุ่มลบสาขา3
    Sleep                               5
    ตรวจพบข้อความบน Alert               ยืนยันลบ
    Sleep                               5
    ไม่พบข้อความ                        ลาดกระบัง2
    Sleep                               3
    ไม่พบข้อความ                        003
    #ไม่พบสาขาที่ทำรายการ


*** Keywords ***
กดปุ่มเพิ่มสาขา
    Wait Until Page Contains Element    id:addBranch                                  300
    Sleep                               1
    Click Element                       id:addBranch

กดปุ่มสร้างสาขา
    Wait Until Page Contains Element    id:btnGenID                                   300
    Sleep                               1
    Click Element                       id:btnGenID

กดปุ่มบันทึกสาขา
    Wait Until Page Contains Element    id:btnSave                                    300
    Sleep                               1
    Click Element                       id:btnSave

กดปุ่มบันทึกแก้ไขสาขา
    Wait Until Element Is Visible       id:btnSave                                    300
    Sleep                               1
    Click Element                       id:btnSave

กดปุ่มยกเลิกสาขา
    Wait Until Page Contains Element    id:btnCancel                                  300
    Sleep                               1
    Click Element                       id:btnCancel

กดปุ่มยกเลิกแก้ไขสาขา
    Wait Until Element Is Visible       id:btnCancel                                  300
    Sleep                               1
    Click Element                       id:btnCancel

กดปุ่มกากบาทสาขา
    Wait Until Page Contains Element    id:btnClose                                   300
    Sleep                               1
    Click Element                       id:btnClose

กดปุ่มกากบาทแก้ไขสาขา
    Wait Until Element Is Visible       id:btnClose                                   300
    Sleep                               1
    Click Element                       id:btnClose

กดปุ่มimport
    Wait Until Page Contains Element    id:importBranch                               300
    Sleep                               1
    Click Element                       id:importBranch

กดปุ่มกากบาทimport
    Wait Until Page Contains Element    id:btnCloseCSV                                300
    Sleep                               1
    Click Element                       id:btnCloseCSV

กดปุ่มบันทึกimport
    Wait Until Page Contains Element    id:btnSaveCSV                                 300
    Sleep                               1
    Click Element                       id:btnSaveCSV

กดปุ่มแก้ไขสาขา3
    Wait Until Page Contains Element    id:2_btnEdit                                 300
    Sleep                               1
    Click Element                       id:2_btnEdit

กดปุ่มลบสาขา3
    Wait Until Page Contains Element    id:1_btnDelete                               300
    Sleep                               1
    Click Element                       id:1_btnDelete

กดปุ่มลบสาขา1
    Wait Until Page Contains Element    id:0_btnDelete                               300
    Sleep                               3
    Click Element                       id:0_btnDelete

กดปุ่มแก้ไขสาขา1
    Wait Until Page Contains Element    id:0_btnEdit                                 300
    Sleep                               1
    Click Element                       id:0_btnEdit

พบจำนวนแถวสาขา
    [Arguments]                         ${value}
    ${count} =                          Get Element Count                             id:branchID
    ${count_str} =                      Convert To String                               ${count}
    Should Be Equal                     ${count_str}                                      ${value}

พบหน้าสาขา
    พบข้อความ                           รหัสสาขา
    พบข้อความ                           ชื่อสาขา
    พบข้อความ                           หมายเหตุ

พบรหัสสาขา
    [Arguments]                         ${value}
    ${text} =                           Get Value                                     id:inputBranchID
    Should Be Equal                     ${text}                                       ${value}

พบชื่อสาขา
    [Arguments]                         ${value}
    ${text} =                           Get Value                                     id:inputBranchName
    Should Be Equal                     ${text}                                       ${value}

กรอกชื่อสาขาด้วยค่าว่าง
    Wait Until Element Is Visible       id:inputBranchName                            300
    Sleep                               1
    Input Text                          id:inputBranchName                            ${Empty}

กรอกชื่อสาขา
    [Arguments]                         ${value}
    Wait Until Element Is Visible       id:inputBranchName                            300
    Sleep                               1
    Input Text                          id:inputBranchName                            ${value}