*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบAdminและinit_data
Test Setup        เข้าหน้าแฟ้มข้อมูล
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data_admin
*** Variables ***

*** Test cases ***
Test1 ทดสอบการเพิ่มประเภทสินค้าไม่สำเร็จเมื่อกดปุ่มกากบาท
    เข้าหน้าประเภทสินค้า
    Sleep                               3
    กดปุ่มเพิ่มประเภทสินค้า
    Sleep                               3
    กรอกรหัสประเภทสินค้า                Z
    Sleep                               3
    กรอกชื่อประเภทสินค้า                สร้อย
    Sleep                               3
    กดกากบาทหน้าประเภทสินค้า
    Sleep                               3
    ไม่พบข้อความ                        Z

Test2 ทดสอบการเพิ่มประเภทสินค้าไม่สำเร็จเมื่อกดปุ่มยกเลิก
    เข้าหน้าประเภทสินค้า
    Sleep                               3
    กดปุ่มเพิ่มประเภทสินค้า
    Sleep                               3
    กรอกรหัสประเภทสินค้า                Z
    Sleep                               3
    กรอกชื่อประเภทสินค้า                สร้อย
    Sleep                               3
    กดยกเลิกหน้าประเภทสินค้า
    Sleep                               3
    ไม่พบข้อความ                        Z

Test3 ทดสอบการเพิ่มประเภทสินค้าไม่สำเร็จเมื่อกรอกรหัสเป็นค่าว่าง
    เข้าหน้าประเภทสินค้า
    Sleep                               3
    กดปุ่มเพิ่มประเภทสินค้า
    Sleep                               3
    กรอกชื่อประเภทสินค้า                สร้อย
    Sleep                               3
    กดยืนยันหน้าประเภทสินค้า
    Sleep                               3
    พบข้อความ                           รหัส *ต้องไม่เป็นค่าว่าง
    Sleep                               3
    กดกากบาทหน้าประเภทสินค้า

Test4 ทดสอบการเพิ่มประเภทสินค้าไม่สำเร็จเมื่อกรอกชื่อเป็นค่าว่าง
    เข้าหน้าประเภทสินค้า
    Sleep                               3
    กดปุ่มเพิ่มประเภทสินค้า
    Sleep                               3
    กรอกรหัสประเภทสินค้า                Z
    Sleep                               3
    กดยืนยันหน้าประเภทสินค้า
    Sleep                               3
    พบข้อความ                           ชื่อ *ต้องไม่เป็นค่าว่าง
    Sleep                               3
    กดกากบาทหน้าประเภทสินค้า

Test5 ทดสอบการเพิ่มประเภทสินค้าไม่สำเร็จเมื่อกรอกรหัสที่มีอยู่แล้ว
    เข้าหน้าประเภทสินค้า
    Sleep                               3
    กดปุ่มเพิ่มประเภทสินค้า
    Sleep                               3
    กรอกรหัสประเภทสินค้า                R
    Sleep                               3
    กรอกชื่อประเภทสินค้า                สร้อย
    Sleep                               3
    กดยืนยันหน้าประเภทสินค้า
    Sleep                               3
    พบข้อความ                           รหัส product type และ code มีอยู่แล้ว
    Sleep                               3
    กดกากบาทหน้าประเภทสินค้า

Test6 ทดสอบการเพิ่มประเภทสินค้าไม่สำเร็จเมื่อกรอกรหัสมากกว่า5ตัวอักษร
    เข้าหน้าประเภทสินค้า
    Sleep                               3
    กดปุ่มเพิ่มประเภทสินค้า
    Sleep                               3
    กรอกรหัสประเภทสินค้า                RRRRRR
    Sleep                               3
    กรอกชื่อประเภทสินค้า                สร้อย
    Sleep                               3
    กดยืนยันหน้าประเภทสินค้า
    Sleep                               3
    พบข้อความ                           รหัส *ต้องไม่มากกว่า5ตัวอักษร
    Sleep                               3
    กดกากบาทหน้าประเภทสินค้า

Test7 ทดสอบการเพิ่มประเภทสินค้าไม่สำเร็จเมื่อกรอกรหัสเป็นค่่าที่ไม่ใช่ภาษาอังกฤษตัวพิมพ์ใหญ่หรือตัวเลข
    เข้าหน้าประเภทสินค้า
    Sleep                               3
    กดปุ่มเพิ่มประเภทสินค้า
    Sleep                               3
    กรอกรหัสประเภทสินค้า                r
    Sleep                               3
    กรอกชื่อประเภทสินค้า                สร้อย
    Sleep                               3
    กดยืนยันหน้าประเภทสินค้า
    Sleep                               3
    พบข้อความ                           รหัส รูปแบบไม่ถูกต้อง
    Sleep                               3
    กดกากบาทหน้าประเภทสินค้า

Test8 ทดสอบการเพิ่มประเภทสินค้าไม่สำเร็จเมื่อเลือกสถานะเป็นซ่อน
    เข้าหน้าประเภทสินค้า
    Sleep                               3
    กดปุ่มเพิ่มประเภทสินค้า
    Sleep                               3
    กรอกรหัสประเภทสินค้า                AAAAA
    Sleep                               3
    กรอกชื่อประเภทสินค้า                สร้อย
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="productTypestatus"]/i                                                                                300
    Sleep                               3
    Click Element                       //*[@id="productTypestatus"]/i
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="productTypestatus"]/div[2]/div[2]/span                                                               300
    Sleep                               3
    Click Element                       //*[@id="productTypestatus"]/div[2]/div[2]/span
    Sleep                               3
    กดยืนยันหน้าประเภทสินค้า
    Sleep                               3
    ไม่พบข้อความ                        AAAAA

Test9 ทดสอบการเพิ่มประเภทสินค้าสำเร็จเมื่อกรอกข้อมูลครบ
    เข้าหน้าประเภทสินค้า
    Sleep                               3
    กดปุ่มเพิ่มประเภทสินค้า
    Sleep                               3
    กรอกรหัสประเภทสินค้า                AAAAB
    Sleep                               3
    กรอกชื่อประเภทสินค้า                ต่างหู
    Sleep                               3
    กดยืนยันหน้าประเภทสินค้า
    Sleep                               3
    พบข้อความ                           AAAAB

# แก้ประเภทสินค้า
Test10 ทดสอบการแก้ไขประเภทสินค้าไม่สำเร็จเมื่อกดปุ่มกากบาท
    เข้าหน้าประเภทสินค้า
    Sleep                               3
    กดปุ่มแก้ไขประเภทสินค้า
    Sleep                               3
    กรอกรหัสประเภทสินค้า                BBBBB
    Sleep                               3
    กดกากบาทหน้าประเภทสินค้า
    Sleep                               3
    ไม่พบข้อความ                        BBBBB

Test11 ทดสอบการแก้ไขประเภทสินค้าไม่สำเร็จเมื่อกดปุ่มยกเลิก
    เข้าหน้าประเภทสินค้า
    Sleep                               3
    กดปุ่มแก้ไขประเภทสินค้า
    Sleep                               3
    กรอกรหัสประเภทสินค้า                BBBBB
    Sleep                               3
    กดยกเลิกหน้าประเภทสินค้า
    Sleep                               3
    ไม่พบข้อความ                        BBBBB

# Test12 ทดสอบการแก้ไขประเภทสินค้าไม่สำเร็จเมื่อกรอกรหัสเป็นค่าว่าง
#                 เข้าหน้าประเภทสินค้า
#                 กดปุ่มแก้ไขประเภทสินค้า
#                 กรอกรหัสประเภทสินค้า                          ${EMPTY}
#                 กดยืนยันหน้าประเภทสินค้า
#                 พบข้อความ                                     รหัส *ต้องไม่เป็นค่าว่าง
#                 กดกากบาทหน้าประเภทสินค้า

Test14 ทดสอบการแก้ไขประเภทสินค้าไม่สำเร็จเมื่อกรอกรหัสที่มีอยู่แล้ว
    เข้าหน้าประเภทสินค้า
    Sleep                               3
    กดปุ่มแก้ไขประเภทสินค้า
    Sleep                               3
    กรอกรหัสประเภทสินค้า                R
    Sleep                               3
    กดยืนยันหน้าประเภทสินค้า
    Sleep                               3
    พบข้อความ                           รหัส product type และ code มีอยู่แล้ว
    Sleep                               3
    กดกากบาทหน้าประเภทสินค้า

Test15 ทดสอบการแก้ไขประเภทสินค้าไม่สำเร็จเมื่อกรอกรหัสมากกว่า5ตัวอักษร
    เข้าหน้าประเภทสินค้า
    Sleep                               3
    กดปุ่มแก้ไขประเภทสินค้า
    Sleep                               3
    กรอกรหัสประเภทสินค้า                RRRRRR
    Sleep                               3
    กดยืนยันหน้าประเภทสินค้า
    Sleep                               3
    พบข้อความ                           รหัส *ต้องไม่มากกว่า5ตัวอักษร
    Sleep                               3
    กดกากบาทหน้าประเภทสินค้า

Test16 ทดสอบการแก้ไขประเภทสินค้าไม่สำเร็จเมื่อกรอกรหัสเป็นค่่าที่ไม่ใช่ภาษาอังกฤษตัวพิมพ์ใหญ่หรือตัวเลข
    เข้าหน้าประเภทสินค้า
    Sleep                               3
    กดปุ่มแก้ไขประเภทสินค้า
    Sleep                               3
    กรอกรหัสประเภทสินค้า                r
    Sleep                               3
    กดยืนยันหน้าประเภทสินค้า
    Sleep                               3
    พบข้อความ                           รหัส รูปแบบไม่ถูกต้อง
    Sleep                               3
    กดกากบาทหน้าประเภทสินค้า

Test17 ทดสอบการแก้ไขประเภทสินค้าสำเร็จเมื่อเลือกสถานะเป็นซ่อน
    เข้าหน้าประเภทสินค้า
    Sleep                               3
    # กดปุ่มแก้ไขประเภทสินค้า
    Wait Until Page Contains Element    //*[@id="content-body"]/div/div[2]/div/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]    300
    Sleep                               3
    Click Element                       //*[@id="content-body"]/div/div[2]/div/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="productTypestatus"]/i                                                                                300
    Sleep                               3
    Click Element                       //*[@id="productTypestatus"]/i
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="productTypestatus"]/div[2]/div[2]/span                                                               300
    Sleep                               3
    Click Element                       //*[@id="productTypestatus"]/div[2]/div[2]/span
    Sleep                               3
    กดยืนยันหน้าประเภทสินค้า
    Sleep                               3
    ไม่พบข้อความ                        R

Test18 ทดสอบการแก้ไขประเภทสินค้าสำเร็จเมื่อกรอกข้อมูลครบ
    เข้าหน้าประเภทสินค้า
    # กดปุ่มแก้ไขประเภทสินค้า
    Sleep                               3
    Wait Until Page Contains Element    //*[@id="content-body"]/div/div[2]/div/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]    300
    Sleep                               3
    Click Element                       //*[@id="content-body"]/div/div[2]/div/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]
    Sleep                               3
    กรอกรหัสประเภทสินค้า                B
    Sleep                               3
    กดยืนยันหน้าประเภทสินค้า
    Sleep                               3
    พบข้อความ                           B

# ยกเลิกประเภทสินค้า
Test19 ทดสอบการยกเลิกประเภทสินค้าไม่สำเร็จเมื่อปฏิเสธการยืนยันการลบ
    เข้าหน้าประเภทสินค้า
    Sleep                               3
    Wait Until Page Contains Element    //*[@id="content-body"]/div/div[2]/div/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[2]    300
    Sleep                               3
    Click Element                       //*[@id="content-body"]/div/div[2]/div/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[2]
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="deleteproducttype"]/div[2]/button[1]                                                                 300
    Sleep                               3
    Click Element                       //*[@id="deleteproducttype"]/div[2]/button[1]
    Sleep                               3
    พบข้อความ                           B

Test20 ทดสอบการยกเลิกประเภทสินค้าไม่สำเร็จเมื่อกดยกเลิกประเภทสินค้าที่ถูกใช้งานอยู่
    เข้าหน้าประเภทสินค้า
    Sleep                               3
    Wait Until Page Contains Element    //*[@id="content-body"]/div/div[2]/div/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]    300
    Sleep                               3
    Click Element                       //*[@id="content-body"]/div/div[2]/div/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]
    Sleep                               3
    Wait Until Page Contains Element    //*[@id="productType"]/i                                                                                      300
    Sleep                               3
    Click Element                       //*[@id="productType"]/i
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="productType"]/div[2]/div[2]/span                                                                     300
    Sleep                               3
    Click Element                       //*[@id="productType"]/div[2]/div[2]/span
    Sleep                               3
    กดยืนยันหน้าประเภทสินค้า
    # เพิ่มสินค้า
    Sleep                               3
    Wait Until Page Contains Element    //*[@id="product"]                                                                                            300
    Sleep                               3
    Click Element                       //*[@id="product"]
    Sleep                               3
    Wait Until Page Contains Element    //*[@id="addproduct"]                                                                                         300
    Sleep                               3
    Click Element                       //*[@id="addproduct"]
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="productgroup"]/i                                                                                     300
    Sleep                               3
    Click Element                       //*[@id="productgroup"]/i
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="productgroup"]/div[2]/div[1]/span                                                                    300
    Sleep                               3
    Click Element                       //*[@id="productgroup"]/div[2]/div[1]/span
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="producttype"]/i                                                                                      300
    Sleep                               3
    Click Element                       //*[@id="producttype"]/i
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="producttype"]/div[2]/div/span                                                                        300
    Sleep                               3
    Click Element                       //*[@id="producttype"]/div[2]/div/span
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="weightbath"]                                                                                         300
    Sleep                               3
    Input Text                          //*[@id="weightbath"]                                                                                         0.125
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="codeid"]                                                                                             300
    Sleep                               3
    Click Element                       //*[@id="codeid"]
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="confirmproduct"]                                                                                     300
    Sleep                               3
    Click Element                       //*[@id="confirmproduct"]
    Sleep                               3
    เข้าหน้าประเภทสินค้า
    Sleep                               3
    Wait Until Page Contains Element    //*[@id="content-body"]/div/div[2]/div/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[2]    300
    Sleep                               3
    Click Element                       //*[@id="content-body"]/div/div[2]/div/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[2]
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="deleteproducttype"]/div[2]/button[2]                                                                 300
    Sleep                               3
    Click Element                       //*[@id="deleteproducttype"]/div[2]/button[2]
    Sleep                               1
    ตรวจพบข้อความบน Alert               ไม่สามารถลบรายการนี้ได้ เนื่องจากถูกใช้งานแล้ว
    Wait Until Element Is Visible       //*[@id="deleteproducttype"]/div[2]/button[1]                                                                 300
    Sleep                               3
    Click Element                       //*[@id="deleteproducttype"]/div[2]/button[1]
    Sleep                               3

Test21 ทดสอบการยกเลิกประเภทสินค้าที่ยังไม่ได้ใช้งานสำเร็จสำเร็จเมื่อตกลงการยืนยันการลบ
    เข้าหน้าประเภทสินค้า
    Sleep                               3
    กดปุ่มเพิ่มประเภทสินค้า
    Sleep                               3
    กรอกรหัสประเภทสินค้า                BB
    Sleep                               3
    กรอกชื่อประเภทสินค้า                สร้อยข้อมือ
    Sleep                               3
    กดยืนยันหน้าประเภทสินค้า
    Sleep                               3
    Wait Until Page Contains Element    //*[@id="content-body"]/div/div[2]/div/div[3]/div[2]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[2]    300
    Sleep                               3
    Click Element                       //*[@id="content-body"]/div/div[2]/div/div[3]/div[2]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[2]
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="deleteproducttype"]/div[2]/button[2]                                                                 300
    Sleep                               3
    Click Element                       //*[@id="deleteproducttype"]/div[2]/button[2]
    Sleep                               3
    ไม่พบข้อความ                        BB



*** Keywords ***
เข้าหน้าประเภทสินค้า
    Sleep                               2
    Wait Until Page Contains Element    id:productType                                                                                                300
    Click Element                       id:productType

กดปุ่มเพิ่มประเภทสินค้า
    Sleep                               2
    Wait Until Page Contains Element    id:addProductType                                                                                             300
    Click Element                       id:addProductType

กดกากบาทหน้าประเภทสินค้า
    Sleep                               2
    Wait Until Page Contains Element    id:closeProductType                                                                                           300
    Click Element                       id:closeProductType

กดยกเลิกหน้าประเภทสินค้า
    Sleep                               2
    Wait Until Page Contains Element    id:cancelProductType                                                                                          300
    Click Element                       id:cancelProductType

กดยืนยันหน้าประเภทสินค้า
    Sleep                               2
    Wait Until Page Contains Element    id:confirmProductType                                                                                         300
    Click Element                       id:confirmProductType

กรอกรหัสประเภทสินค้า
    [Arguments]                         ${value}
    Sleep                               2
    Wait Until Element Is Visible       id:productTypeID
    Input Text                          id:productTypeID                                                                                              ${value}

กรอกชื่อประเภทสินค้า
    [Arguments]                         ${value}
    Sleep                               2
    Wait Until Element Is Visible       id:productTypename
    Input Text                          id:productTypename                                                                                            ${value}

กดปุ่มแก้ไขประเภทสินค้า
    Sleep                               2
    Wait Until Page Contains Element    //*[@id="content-body"]/div/div[2]/div/div[3]/div[2]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]    300
    Click Element                       //*[@id="content-body"]/div/div[2]/div/div[3]/div[2]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]