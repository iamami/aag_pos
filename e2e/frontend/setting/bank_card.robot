*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบAdminและinit_data  
Test Setup        เข้าหน้าแฟ้มข้อมูล
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data_admin     
*** Variables ***

*** Test cases ***
Test1 ทดสอบการเพิ่มชนิดบัตรไม่สำเร็จเมื่อกดปุ่มกากบาท
    เข้าหน้าชนิดบัตร
    กดปุ่มเพิ่มชนิดบัตร
    กรอกชื่อประเภทบัตร                                Card
    กรอก%ค่าธรรมเนียม                                     10
    เลือกธนาคารกรุงศรีอยุธยา
    กดปุ่มกากบาทชนิดบัตร
    Sleep                                               3
    พบจำนวนแถวชนิดบัตร                                   5
    ไม่พบข้อความ                                        Card
    #ไม่พบรายการที่เพิ่มล่าสุด

Test2 ทดสอบการชนิดบัตรไม่สำเร็จเมื่อกดปุ่มยกเลิก
    เข้าหน้าชนิดบัตร
    กดปุ่มเพิ่มชนิดบัตร
    กรอกชื่อประเภทบัตร                                Card
    กรอก%ค่าธรรมเนียม                                     10
    เลือกธนาคารกรุงศรีอยุธยา
    กดปุ่มยกเลิกชนิดบัตร
    Sleep                                               3
    พบจำนวนแถวชนิดบัตร                                   5
    ไม่พบข้อความ                                        Card
    #ไม่พบรายการที่เพิ่มล่าสุด

Test3 ทดสอบการเพิ่มชนิดบัตรไม่สำเร็จเมื่อไม่กรอกประเภทบัตร
    เข้าหน้าชนิดบัตร
    กดปุ่มเพิ่มชนิดบัตร
    กรอก%ค่าธรรมเนียม                                     10
    เลือกธนาคารกรุงศรีอยุธยา
    กดปุ่มบันทึกชนิดบัตร
    พบข้อความ                                           ประเภทบัตร *ต้องไม่เป็นค่าว่าง
    กดปุ่มกากบาทชนิดบัตร
    Sleep                                               3
    พบจำนวนแถวชนิดบัตร                                    5
    ไม่พบข้อความ                                        กรุงศรีอยุธยา
    #พบข้อความ *ประเภทบัตร *ต้องไม่เป็นค่าว่าง

Test4 ทดสอบการเพิ่มชนิดบัตรไม่สำเร็จเมื่อไม่กรอก%ค่าธรรมเนียม
    เข้าหน้าชนิดบัตร
    กดปุ่มเพิ่มชนิดบัตร
    กรอกชื่อประเภทบัตร                                Card
    เลือกธนาคารกรุงศรีอยุธยา
    กดปุ่มบันทึกชนิดบัตร
    พบข้อความ                                           %ค่าธรรมเนียม *ต้องไม่เป็นค่าว่าง & *ต้องไม่มีตัวอักษร
    กดปุ่มกากบาทชนิดบัตร
    Sleep                                               3
    พบจำนวนแถวชนิดบัตร                                    5
    ไม่พบข้อความ                                        Card
    ไม่พบข้อความ                                        กรุงศรีอยุธยา
    #พบข้อความ %ค่าธรรมเนียม *ต้องไม่เป็นค่าว่าง & *ต้องไม่มีตัวอักษร

Test5 ทดสอบการเพิ่มชนิดบัตรไม่สำเร็จเมื่อไม่เลือกชื่อธนาคาร
    เข้าหน้าชนิดบัตร
    กดปุ่มเพิ่มชนิดบัตร
    กรอกชื่อประเภทบัตร                                Card
    กรอก%ค่าธรรมเนียม                                     10
    กดปุ่มบันทึกชนิดบัตร
    พบข้อความ                                           ชื่อธนาคาร *เลือกธนาคาร
    กดปุ่มกากบาทชนิดบัตร
    Sleep                                               3
    พบจำนวนแถวชนิดบัตร                                    5
    ไม่พบข้อความ                                        Card
    ไม่พบข้อความ                                        10.00
    #พบข้อความ ชื่อธนาคาร *เลือกธนาคาร

Test6 ทดสอบการเพิ่มชนิดบัตรสำเร็จเมื่อกรอกข้อมูลครบ
    เข้าหน้าชนิดบัตร
    กดปุ่มเพิ่มชนิดบัตร
    กรอกชื่อประเภทบัตร                                Card
    กรอก%ค่าธรรมเนียม                                     10
    เลือกธนาคารกรุงศรีอยุธยา
    กดปุ่มบันทึกชนิดบัตร
    Sleep                                               3
    พบจำนวนแถวชนิดบัตร                                     6
    พบข้อความ                                           Card
    พบข้อความ                                           10.00
    พบข้อความ                                           กรุงศรีอยุธยา
    #พบรายการที่เพิ่ม

Test7 ทดสอบการแก้ไขชนิดบัตรไม่สำเร็จเมื่อกดปุ่มกากบาท
    เข้าหน้าชนิดบัตร
    กดปุ่มแก้ไขชนิดบัตร6
    กรอกชื่อประเภทบัตร                                Card2
    กรอก%ค่าธรรมเนียม                                     15
    เลือกธนาคารAeon
    กดปุ่มกากบาทชนิดบัตร
    Sleep                                               3
    พบจำนวนแถวชนิดบัตร                                   6
    ไม่พบข้อความ                                        Card2
    ไม่พบข้อความ                                        15.00
    #ไม่พบความเปลี่ยนแปลงของชนิดบัตรที่ทำการแก้ไข

Test8 ทดสอบการแก้ไขชนิดบัตรไม่สำเร็จเมื่อกดปุ่มยกเลิก
    เข้าหน้าชนิดบัตร
    กดปุ่มแก้ไขชนิดบัตร6
    กรอกชื่อประเภทบัตร                                Card2
    กรอก%ค่าธรรมเนียม                                     15
    เลือกธนาคารAeon
    กดปุ่มยกเลิกชนิดบัตร
    Sleep                                               3
    พบจำนวนแถวชนิดบัตร                                   6
    ไม่พบข้อความ                                        Card2
    ไม่พบข้อความ                                        15.00
    #ไม่พบความเปลี่ยนแปลงของชนิดบัตรที่ทำการแก้ไข

# Test9 ทดสอบการแก้ไขชนิดบัตรไม่สำเร็จเมื่อแก้ไขประเภทบัตรเป็นค่าว่าง
#     เข้าหน้าชนิดบัตร
#     กดปุ่มแก้ไขชนิดบัตร6
#     กรอกชื่อประเภทบัตรด้วยค่าว่าง
#     กรอก%ค่าธรรมเนียม                                     15
#     เลือกธนาคารAeon
#     กดปุ่มบันทึกชนิดบัตร
#     พบข้อความ                                           ประเภทบัตร *ต้องไม่เป็นค่าว่าง
#     กดปุ่มกากบาทชนิดบัตร
#     Sleep                                               3
#     พบจำนวนแถวชนิดบัตร                                   6
#     พบข้อความ                                        Card
#     พบข้อความ                                        10.00
#     #พบข้อความ *ประเภทบัตร *ต้องไม่เป็นค่าว่าง

# Test10 ทดสอบการแก้ไขชนิดบัตรไม่สำเร็จเมื่อแก้ไข%ค่าธรรมเนียมเป็นค่าว่าง
#     เข้าหน้าชนิดบัตร
#     กดปุ่มแก้ไขชนิดบัตร6
#     กรอกชื่อประเภทบัตร                                Card2
#     กรอก%ค่าธรรมเนียมด้วยค่าว่าง        
#     เลือกธนาคารAeon
#     กดปุ่มบันทึกชนิดบัตร
#     พบข้อความ                                           %ค่าธรรมเนียม *ต้องไม่เป็นค่าว่าง & *ต้องไม่มีตัวอักษร
#     กดปุ่มกากบาทชนิดบัตร
#     Sleep                                               3
#     พบจำนวนแถวชนิดบัตร                                   6
#     พบข้อความ                                        Card
#     พบข้อความ                                        10.00
#     #พบข้อความ %ค่าธรรมเนียม *ต้องไม่เป็นค่าว่าง & *ต้องไม่มีตัวอักษร

Test11 ทดสอบการแก้ไขชนิดบัตรสำเร็จเมื่อแก้ไขข้อมูล
    เข้าหน้าชนิดบัตร
    กดปุ่มแก้ไขชนิดบัตร6
    กรอกชื่อประเภทบัตร                                Card2
    กรอก%ค่าธรรมเนียม                                     15
    เลือกธนาคารAeon
    กดปุ่มบันทึกชนิดบัตร
    Sleep                                               3
    พบจำนวนแถวชนิดบัตร                                   6
    #ไม่พบ popup ชนิดบัตร
    พบข้อความ                                        Card2
    พบข้อความ                                        15.00
    #พบความเปลี่ยนแปลงของชนิดบัตรที่ทำการแก้ไข

Test12 ทดสอบการยกเลิกชนิดบัตรไม่สำเร็จเมื่อปฏิเสธการยืนยันการลบ
    เข้าหน้าชนิดบัตร  
    กดปุ่มลบชนิดบัตร6
    Sleep                                               5
    พบpopupยืนยันรายการลบชนิดบัตร
    ปฏิเสธลบรายการชนิดบัตร
    พบจำนวนแถวชนิดบัตร                                    6
    พบข้อความ                                           Card2
    พบข้อความ                                           15.00
    #รายการที่ต้องการลบยังคงอยู่

Test13 ทดสอบการยกเลิกชนิดบัตรสำเร็จเมื่อตกลงการยืนยันการลบ
    เข้าหน้าชนิดบัตร  
    กดปุ่มลบชนิดบัตร6
    Sleep                                               5
    พบpopupยืนยันรายการลบชนิดบัตร
    ยืนยันลบรายการชนิดบัตร
    Sleep                                   3
    พบจำนวนแถวชนิดบัตร                                    5
    ไม่พบข้อความ                                           Card2
    ไม่พบข้อความ                                           15.00
    #รายการที่ต้องการลบหายไป

*** Keywords ***
เข้าหน้าชนิดบัตร
    Sleep                                               2
    Wait Until Page Contains Element                    id:bank_card                                        300
    Click Element                                       id:bank_card

กดปุ่มเพิ่มชนิดบัตร
    Wait Until Page Contains Element                    id:btnAddCard                                   300
    Sleep                                               1
    Click Element                                       id:btnAddCard

กดปุ่มกากบาทชนิดบัตร
    Sleep                                               2
    Wait Until Page Contains Element                    id:btnCloseCard                                 300
    Click Element                                       id:btnCloseCard

กดปุ่มยกเลิกชนิดบัตร
    Sleep                                               2
    Wait Until Page Contains Element                    id:btnCancelCard                                300
    Click Element                                       id:btnCancelCard

กดปุ่มบันทึกชนิดบัตร
    Sleep                                               2
    Wait Until Page Contains Element                    id:btnSaveCard                                  300
    Click Element                                       id:btnSaveCard

กรอกชื่อประเภทบัตร
    [Arguments]                                         ${value}
    Sleep                                               2
    Wait Until Element Is Visible                       id:inputCardType
    Input Text                                          id:inputCardType                                ${value}

กรอกชื่อประเภทบัตรด้วยค่าว่าง
    Wait Until Element Is Visible                       id:inputCardType                                300
    Sleep                                               1
    Input Text                                          id:inputCardType                                ${Empty}

กรอก%ค่าธรรมเนียม
    [Arguments]                                         ${value}
    Sleep                                               2
    Wait Until Element Is Visible                       id:inputCardFree
    Input Text                                          id:inputCardFree                                 ${value}

กรอก%ค่าธรรมเนียมด้วยค่าว่าง
    Wait Until Element Is Visible       id:inputCardFree                                300
    Sleep                               1
    Input Text                          id:inputCardFree                                ${Empty}

เลือกธนาคารกรุงศรีอยุธยา
    Wait Until Element Is Visible        id:dorpDownBankName                               300
    Click Element                        id:dorpDownBankName
    Wait Until Element Is Visible        //*[@id="dorpDownBankName"]/div[2]/div[1]         300
    Click Element                        //*[@id="dorpDownBankName"]/div[2]/div[1]

เลือกธนาคารAeon
    Wait Until Element Is Visible        id:dorpDownBankName                               300
    Click Element                        id:dorpDownBankName
    Wait Until Element Is Visible        //*[@id="dorpDownBankName"]/div[2]/div[2]         300
    Click Element                        //*[@id="dorpDownBankName"]/div[2]/div[2]

พบชื่อประเภทบัตร
    [Arguments]                                         ${value}
    ${text} =                                           Get Value                                         id:inputCardType
    Should Be Equal                                     ${text}                                           ${value}

พบ%ค่าธรรมเนียม
    [Arguments]                                         ${value}
    ${text} =                                           Get Value                                         id:inputCardFree
    Should Be Equal                                     ${text}                                           ${value}

พบจำนวนแถวชนิดบัตร
    [Arguments]                                         ${value}
    ${count} =                                          Get Element Count                                 id:cardType
    ${count_str} =                                      Convert To String                                 ${count}
    Should Be Equal                                     ${count_str}                                      ${value}

กดปุ่มแก้ไขชนิดบัตร1
    Wait Until Page Contains Element                    id:0_btnEdit                                      300
    Sleep                                               1
    Click Element                                       id:0_btnEdit

กดปุ่มแก้ไขชนิดบัตร6
    Wait Until Page Contains Element                    id:5_btnEdit                                      300
    Sleep                                               1
    Click Element                                       id:5_btnEdit

กดปุ่มลบชนิดบัตร1
    Wait Until Page Contains Element                    id:0_btnDelete                                    300
    Sleep                                               1
    Click Element                                       id:0_btnDelete

กดปุ่มลบชนิดบัตร6
    Wait Until Page Contains Element                    id:5_btnDelete                                    300
    Sleep                                               1
    Click Element                                       id:5_btnDelete

พบpopupยืนยันรายการลบชนิดบัตร
    Wait Until Element Is Visible                       //*[@id="modalConfirmDeleteCard"]                     300
    Wait Until Element Is Visible                       //*[@id="modalConfirmDeleteCard"]/div[2]/button[1]    300
    Wait Until Element Is Visible                       //*[@id="modalConfirmDeleteCard"]/div[2]/button[2]    300

ยืนยันลบรายการชนิดบัตร
    Wait Until Element Is Visible                       //*[@id="modalConfirmDeleteCard"]/div[2]/button[2]    300
    Click Element                       //*[@id="modalConfirmDeleteCard"]/div[2]/button[2] 

ปฏิเสธลบรายการชนิดบัตร
    Wait Until Element Is Visible                       //*[@id="modalConfirmDeleteCard"]/div[2]/button[1]    300
    Click Element                       //*[@id="modalConfirmDeleteCard"]/div[2]/button[1] 