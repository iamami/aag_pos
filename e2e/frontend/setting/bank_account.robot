*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบAdminและinit_data
Test Setup        เข้าหน้าแฟ้มข้อมูล
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data_admin                    
*** Variables ***

*** Test cases ***
Test1 ทดสอบการเพิ่มบัญชีธนาคารไม่สำเร็จเมื่อกดปุ่มกากบาท
    เข้าหน้าบัญชี
    กดปุ่มเพิ่มบัญชี
    กรอกชื่อบัญชี                                Account
    กรอกเลขที่บัญชี                                      1111111111
    เลือกธนาคารกรุงศรีอยุธยา
    กดปุ่มกากบาทบัญชี
    Sleep                                               3
    พบจำนวนแถวบัญชี                                   1
    ไม่พบข้อความ                                        Account
    #ไม่พบรายการที่เพิ่มล่าสุด

Test2 ทดสอบการเพิ่มบัญชีธนาคารไม่สำเร็จเมื่อกดปุ่มยกเลิก
    เข้าหน้าบัญชี
    กดปุ่มเพิ่มบัญชี
    กรอกชื่อบัญชี                                Account
    กรอกเลขที่บัญชี                                      1111111111
    เลือกธนาคารกรุงศรีอยุธยา
    กดปุ่มยกเลิกบัญชี
    Sleep                                               3
    พบจำนวนแถวบัญชี                                   1
    ไม่พบข้อความ                                        Account
    #ไม่พบรายการที่เพิ่มล่าสุด

Test3 ทดสอบการเพิ่มบัญชีธนาคารไม่สำเร็จเมื่อไม่กรอกชื่อบัญชี
    เข้าหน้าบัญชี
    กดปุ่มเพิ่มบัญชี
    กรอกเลขที่บัญชี                                      1111111111
    เลือกธนาคารกรุงศรีอยุธยา
    กดปุ่มบันทึกบัญชี 
    พบข้อความ                                           ชื่อบัญชี *ต้องไม่เป็นค่าว่าง
    กดปุ่มกากบาทบัญชี
    Sleep                                               3
    พบจำนวนแถวบัญชี                                    1
    ไม่พบข้อความ                                        1111111111
    #พบข้อความ ชื่อบัญชี *ต้องไม่เป็นค่าว่าง

Test4 ทดสอบการเพิ่มบัญชีธนาคารไม่สำเร็จเมื่อไม่กรอกเลขที่บัญชี
    เข้าหน้าบัญชี
    กดปุ่มเพิ่มบัญชี
    กรอกชื่อบัญชี                                Account
    เลือกธนาคารกรุงศรีอยุธยา
    กดปุ่มบันทึกบัญชี 
    พบข้อความ                                           เลขที่บัญชี *ต้องไม่เป็นค่าว่าง
    กดปุ่มกากบาทบัญชี
    Sleep                                               3
    พบจำนวนแถวบัญชี                                    1
    ไม่พบข้อความ                                        Account
    #พบข้อความ เลขที่บัญชี *ต้องไม่เป็นค่าว่าง

Test5 ทดสอบการเพิ่มบัญชีธนาคารไม่สำเร็จเมื่อไม่เลือกธนาคาร
    เข้าหน้าบัญชี
    กดปุ่มเพิ่มบัญชี
    กรอกชื่อบัญชี                                Account
    กรอกเลขที่บัญชี                                      1111111111
    กดปุ่มบันทึกบัญชี 
    พบข้อความ                                           ธนาคาร *เลือกธนาคาร
    กดปุ่มกากบาทบัญชี
    Sleep                                               3
    พบจำนวนแถวบัญชี                                    1
    ไม่พบข้อความ                                        Account
    ไม่พบข้อความ                                        กรุงศรีอยุธยา
    #พบข้อความ ธนาคาร *เลือกธนาคาร

Test6 ทดสอบการเพิ่มบัญชีธนาคารไม่สำเร็จเมื่อกรอกเลขที่บัญชีไม่ถูกต้อง
    เข้าหน้าบัญชี
    กดปุ่มเพิ่มบัญชี
    กรอกชื่อบัญชี                                Account
    กรอกเลขที่บัญชี                                      111
    เลือกธนาคารกรุงศรีอยุธยา
    กดปุ่มบันทึกบัญชี 
    พบข้อความ                                           เลขที่บัญชี *เลขที่บัญชีไม่ถูกต้อง
    กดปุ่มกากบาทบัญชี
    Sleep                                               3
    พบจำนวนแถวบัญชี                                    1
    ไม่พบข้อความ                                        Account
    ไม่พบข้อความ                                        กรุงศรีอยุธยา
    #พบข้อความ เลขที่บัญชี *เลขที่บัญชีไม่ถูกต้อง


Test7 ทดสอบการเพิ่มบัญชีธนาคารสำเร็จเมื่อกรอกข้อมูลครบ
    เข้าหน้าบัญชี
    กดปุ่มเพิ่มบัญชี
    กรอกชื่อบัญชี                                Account
    กรอกเลขที่บัญชี                                      1111111111
    เลือกธนาคารกรุงศรีอยุธยา
    กดปุ่มบันทึกบัญชี 
    Sleep                                               3
    พบจำนวนแถวบัญชี                                    2
    พบข้อความ                                        Account
    พบข้อความ                                        1111111111
    พบข้อความ                                        กรุงศรีอยุธยา
    #พบรายการที่เพิ่ม

Test8 ทดสอบการแก้ไขบัญชีธนาคารไม่สำเร็จเมื่อกดปุ่มกากบาท
    เข้าหน้าบัญชี
    กดปุ่มแก้ไขบัญชี2
    กรอกชื่อบัญชี                                Account2
    กรอกเลขที่บัญชี                                      1111111112
    เลือกธนาคารAeon
    กดปุ่มกากบาทบัญชี
    Sleep                                               3
    พบจำนวนแถวบัญชี                                   2
    ไม่พบข้อความ                                        Account2
    ไม่พบข้อความ                                        1111111112
    #ไม่พบความเปลี่ยนแปลงของชนิดบัตรที่ทำการแก้ไข

Test9 ทดสอบการแก้ไขบัญชีธนาคารไม่สำเร็จเมื่อกดปุ่มยกเลิก
    เข้าหน้าบัญชี
    กดปุ่มแก้ไขบัญชี2
    กรอกชื่อบัญชี                                Account2
    กรอกเลขที่บัญชี                                      1111111112
    เลือกธนาคารAeon
    กดปุ่มยกเลิกบัญชี
    Sleep                                               3
    พบจำนวนแถวบัญชี                                   2
    ไม่พบข้อความ                                        Account2
    ไม่พบข้อความ                                        1111111112
    #ไม่พบความเปลี่ยนแปลงของชนิดบัตรที่ทำการแก้ไข

# Test10 ทดสอบการแก้ไขบัญชีธนาคารไม่สำเร็จเมื่อแก้ไขชื่อบัญชีเป็นค่าว่าง
#     เข้าหน้าบัญชี
#     กดปุ่มแก้ไขบัญชี2
#     กรอกชื่อบัญชีด้วยค่าว่าง
#     กรอกเลขที่บัญชี                                      1111111112
#     เลือกธนาคารAeon
#     กดปุ่มบันทึกบัญชี
#     พบข้อความ                                           ชื่อบัญชี *ต้องไม่เป็นค่าว่าง
#     กดปุ่มกากบาทบัญชี
#     Sleep                                               3
#     พบจำนวนแถวบัญชี                                  2
#     พบข้อความ                                        Account
#     พบข้อความ                                        1111111111
#     #พบข้อความ ชื่อบัญชี *ต้องไม่เป็นค่าว่าง

# Test11 ทดสอบการแก้ไขบัญชีธนาคารไม่สำเร็จเมื่อแก้ไขเลขที่บัญชีเป็นค่าว่าง
    # เข้าหน้าบัญชี
    # กดปุ่มแก้ไขบัญชี2
    # กรอกชื่อบัญชี                                Account2
    # กรอกเลขที่บัญชีด้วยค่าว่าง
    # เลือกธนาคารAeon
    # กดปุ่มบันทึกบัญชี
    # พบข้อความ                                           เลขที่บัญชี *ต้องไม่เป็นค่าว่าง
    # กดปุ่มกากบาทบัญชี
    # Sleep                                               3
    # พบจำนวนแถวบัญชี                                  2
    # พบข้อความ                                        Account
    # พบข้อความ                                        1111111111
    # #พบข้อความ เลขที่บัญชี *ต้องไม่เป็นค่าว่าง

Test12 ทดสอบการแก้ไขบัญชีธนาคารไม่สำเร็จเมื่อกรอกเลขที่บัญชีไม่ถูกต้อง
    เข้าหน้าบัญชี
    กดปุ่มแก้ไขบัญชี2
    กรอกชื่อบัญชี                                Account2
    กรอกเลขที่บัญชี                                      111
    เลือกธนาคารAeon
    กดปุ่มบันทึกบัญชี
    พบข้อความ                                           เลขที่บัญชี *เลขที่บัญชีไม่ถูกต้อง
    กดปุ่มกากบาทบัญชี
    Sleep                                               3
    พบจำนวนแถวบัญชี                                  2
    พบข้อความ                                        Account
    พบข้อความ                                        1111111111
    #พบข้อความ เลขที่บัญชี *เลขที่บัญชีไม่ถูกต้อง

Test13 ทดสอบการแก้ไขบัญชีธนาคารสำเร็จเมื่อแก้ไขข้อมูล
    เข้าหน้าบัญชี
    กดปุ่มแก้ไขบัญชี2
    กรอกชื่อบัญชี                                Account2
    กรอกเลขที่บัญชี                                      1111111112
    เลือกธนาคารAeon
    กดปุ่มบันทึกบัญชี
    Sleep                                               3
    พบจำนวนแถวบัญชี                                  2
    พบข้อความ                                        Account2
    พบข้อความ                                        1111111112
    พบข้อความ                                        Aeon
    ไม่พบข้อความ                                        Account
    ไม่พบข้อความ                                        1111111111
    ไม่พบข้อความ                                        กรุงศรีอยุธยา
    #พบข้อความ เลขที่บัญชี *เลขที่บัญชีไม่ถูกต้อง

Test14 ทดสอบการยกเลิกบัญชีไม่สำเร็จเมื่อปฏิเสธการยืนยันการลบ
    เข้าหน้าบัญชี
    กดปุ่มลบบัญชี2
    Sleep                                               5
    พบpopupยืนยันรายการลบบัญชี
    ปฏิเสธลบรายการบัญชี
    Sleep                                   3
    พบจำนวนแถวบัญชี                                   2
    พบข้อความ                                           Account2
    พบข้อความ                                           1111111112
    พบข้อความ                                        Aeon
    #รายการที่ต้องการลบยังคงอยู่

Test15 ทดสอบการยกเลิกบัญชีสำเร็จเมื่อตกลงการยืนยันการลบ
    เข้าหน้าบัญชี
    กดปุ่มลบบัญชี2
    Sleep                                               5
    พบpopupยืนยันรายการลบบัญชี
    ยืนยันลบรายการบัญชี
    Sleep                                   3
    พบจำนวนแถวบัญชี                                   1
    ไม่พบข้อความ                                           Account2
    ไม่พบข้อความ                                           1111111112
    ไม่พบข้อความ                                        Aeon
    #รายการที่ต้องการลบยังคงอยู่

*** Keywords ***
เข้าหน้าบัญชี
    Sleep                                               2
    Wait Until Page Contains Element                    id:bank_account                                      300
    Click Element                                       id:bank_account

กดปุ่มเพิ่มบัญชี
    Wait Until Page Contains Element                    id:btnAddAccount                                 300
    Sleep                                               1
    Click Element                                       id:btnAddAccount

กดปุ่มกากบาทบัญชี
    Sleep                                               2
    Wait Until Page Contains Element                    id:btnCloseAccount                                 300
    Click Element                                       id:btnCloseAccount

กดปุ่มยกเลิกบัญชี
    Sleep                                               2
    Wait Until Page Contains Element                    id:btnCancelAccount                               300
    Click Element                                       id:btnCancelAccount

กดปุ่มบันทึกบัญชี
    Sleep                                               2
    Wait Until Page Contains Element                    id:btnSaveAccount                                 300
    Click Element                                       id:btnSaveAccount

กรอกชื่อบัญชี
    [Arguments]                                         ${value}
    Sleep                                               2
    Wait Until Element Is Visible                       id:inputAccountName
    Input Text                                          id:inputAccountName                                ${value}

กรอกชื่อบัญชีด้วยค่าว่าง
    Wait Until Element Is Visible                       id:inputAccountName                                300
    Sleep                                               1
    Input Text                                          id:inputAccountName                                ${Empty}

กรอกเลขที่บัญชี
    [Arguments]                                         ${value}
    Sleep                                               2
    Wait Until Element Is Visible                       id:inputAccountID
    Input Text                                          id:inputAccountID                                 ${value}

กรอกเลขที่บัญชีด้วยค่าว่าง
    Wait Until Element Is Visible       id:inputAccountID                                300
    Sleep                               1
    Input Text                          id:inputAccountID                                ${Empty}

เลือกธนาคารกรุงศรีอยุธยา
    Wait Until Element Is Visible        id:dorpDownBankName                               300
    Click Element                        id:dorpDownBankName
    Wait Until Element Is Visible        //*[@id="dorpDownBankName"]/div[2]/div[1]         300
    Click Element                        //*[@id="dorpDownBankName"]/div[2]/div[1]

เลือกธนาคารAeon
    Wait Until Element Is Visible        id:dorpDownBankName                               300
    Click Element                        id:dorpDownBankName
    Wait Until Element Is Visible        //*[@id="dorpDownBankName"]/div[2]/div[2]         300
    Click Element                        //*[@id="dorpDownBankName"]/div[2]/div[2]

พบจำนวนแถวบัญชี
    [Arguments]                                         ${value}
    ${count} =                                          Get Element Count                                 id:AccountName
    ${count_str} =                                      Convert To String                                 ${count}
    Should Be Equal                                     ${count_str}                                      ${value}

กดปุ่มแก้ไขบัญชี1
    Wait Until Page Contains Element                    id:0_btnEdit                                      300
    Sleep                                               1
    Click Element                                       id:0_btnEdit

กดปุ่มแก้ไขบัญชี2
    Wait Until Page Contains Element                    id:1_btnEdit                                      300
    Sleep                                               1
    Click Element                                       id:1_btnEdit

กดปุ่มลบบัญชี1
    Wait Until Page Contains Element                    id:0_btnDelete                                    300
    Sleep                                               1
    Click Element                                       id:0_btnDelete

กดปุ่มลบบัญชี2
    Wait Until Page Contains Element                    id:1_btnDelete                                    300
    Sleep                                               1
    Click Element                                       id:1_btnDelete

พบpopupยืนยันรายการลบบัญชี
    Wait Until Element Is Visible                       //*[@id="modalConfirmDeleteAccount"]                     300
    Wait Until Element Is Visible                       //*[@id="modalConfirmDeleteAccount"]/div[2]/button[1]    300
    Wait Until Element Is Visible                       //*[@id="modalConfirmDeleteAccount"]/div[2]/button[2]    300

ยืนยันลบรายการบัญชี
    Wait Until Element Is Visible                       //*[@id="modalConfirmDeleteAccount"]/div[2]/button[2]    300
    Click Element                       //*[@id="modalConfirmDeleteAccount"]/div[2]/button[2] 

ปฏิเสธลบรายการบัญชี
    Wait Until Element Is Visible                       //*[@id="modalConfirmDeleteAccount"]/div[2]/button[1]    300
    Click Element                       //*[@id="modalConfirmDeleteAccount"]/div[2]/button[1] 