*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบAdminและinit_data  
Test Setup        เข้าหน้าแฟ้มข้อมูล
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data_admin
*** Variables ***

*** Test cases ***
Test1 ทดสอบการเพิ่มรายรับรายจ่ายไม่สำเร็จเมื่อกดปุ่มกากบาท
    เข้าหน้ารายการรายรับรายจ่าย
    Sleep                               3
    กดปุ่มเพิ่มรายการรายรับรายจ่าย
    Sleep                               3
    กรอกรายการรายรับรายจ่าย                 ออมทอง
    Sleep                               3
    เลือกประเภทรายรับรายจ่าย
    Sleep                               3
    กดปุ่มกากบาทเพิ่มรายการรายรับรายจ่าย
    Sleep                               3
    ไม่พบข้อความ                            ออมทอง

Test2 ทดสอบการเพิ่มรายรับรายจ่ายไม่สำเร็จเมื่อไม่กรอกรายการ
    เข้าหน้ารายการรายรับรายจ่าย
    Sleep                               3
    กดปุ่มเพิ่มรายการรายรับรายจ่าย
    Sleep                               3
    กรอกรายการรายรับรายจ่าย                 ออมทอง
    Sleep                               3
    เลือกประเภทรายรับรายจ่าย
    Sleep                               3
    กดปุ่มยกเลิกเพิ่มรายการรายรับรายจ่าย
    Sleep                               3
    ไม่พบข้อความ                            ออมทอง

Test3 ทดสอบการเพิ่มรายรับรายจ่ายไม่สำเร็จเมื่อไม่กรอกรายการ
    เข้าหน้ารายการรายรับรายจ่าย
    Sleep                               3
    กดปุ่มเพิ่มรายการรายรับรายจ่าย
    Sleep                               3
    เลือกประเภทรายรับรายจ่าย
    Sleep                               3
    กดปุ่มยืนยันเพิ่มรายการรายรับรายจ่าย
    Sleep                               3
    พบข้อความ                               *รายการ *ต้องไม่เป็นค่าว่าง
    Sleep                               3
    กดปุ่มกากบาทเพิ่มรายการรายรับรายจ่าย

Test4 ทดสอบการเพิ่มรายรับรายจ่ายไม่สำเร็จเมื่อไม่เลือกประเภท
    เข้าหน้ารายการรายรับรายจ่าย
    Sleep                               3
    กดปุ่มเพิ่มรายการรายรับรายจ่าย
    Sleep                               3
    กรอกรายการรายรับรายจ่าย                 ออมทอง
    Sleep                               3
    กดปุ่มยืนยันเพิ่มรายการรายรับรายจ่าย
    Sleep                               3
    พบข้อความ                               *ประเภท *ต้องไม่เป็นค่าว่าง
    Sleep                               3
    กดปุ่มกากบาทเพิ่มรายการรายรับรายจ่าย

Test5 ทดสอบการเพิ่มรายรับรายจ่ายสำเร็จเมื่อกรอกข้อมูลครบ
    เข้าหน้ารายการรายรับรายจ่าย
    Sleep                               3
    กดปุ่มเพิ่มรายการรายรับรายจ่าย
    Sleep                               3
    กรอกรายการรายรับรายจ่าย                 ออมทอง
    Sleep                               3
    เลือกประเภทรายรับรายจ่าย
    Sleep                               3
    กดปุ่มยืนยันเพิ่มรายการรายรับรายจ่าย
    Sleep                               3
    พบข้อความ                               ออมทอง

# แก้ไขรายรับรายจ่าย
Test6 ทดสอบการแก้ไขรายรับรายจ่ายไม่สำเร็จเมื่อกดปุ่มแก้ไข เฉพาะรายการรหัส 001-005
    เข้าหน้ารายการรายรับรายจ่าย
    Sleep                               3
    Wait Until Page Contains Element        //*[@id="content-body"]/div/div[2]/div/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]    300
    Sleep                               3
    Click Element                           //*[@id="content-body"]/div/div[2]/div/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]
    Sleep                               3
    Wait Until Element Is Not Visible       //*[@id="menuinout"]

Test7 ทดสอบการแก้ไขรายรับรายจ่ายไม่สำเร็จเมื่อกดปุ่มกากบาท
    เข้าหน้ารายการรายรับรายจ่าย
    Sleep                               3
    Wait Until Page Contains Element        //*[@id="content-body"]/div/div[2]/div/div[3]/div[6]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]    300
    Sleep                               3
    Click Element                           //*[@id="content-body"]/div/div[2]/div/div[3]/div[6]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]
    Sleep                               3
    กรอกรายการรายรับรายจ่าย                 ออมทอง1
    Sleep                               3
    กดปุ่มกากบาทเพิ่มรายการรายรับรายจ่าย
    Sleep                               3
    ไม่พบข้อความ                            ออมทอง1

Test8 ทดสอบการแก้ไขรายรับรายจ่ายไม่สำเร็จเมื่อกดปุ่มยกเลิก
    เข้าหน้ารายการรายรับรายจ่าย
    Sleep                               3
    Wait Until Page Contains Element        //*[@id="content-body"]/div/div[2]/div/div[3]/div[6]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]    300
    Sleep                               3
    Click Element                           //*[@id="content-body"]/div/div[2]/div/div[3]/div[6]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]
    Sleep                               3
    กรอกรายการรายรับรายจ่าย                 ออมทอง1
    Sleep                               3
    กดปุ่มยกเลิกเพิ่มรายการรายรับรายจ่าย
    Sleep                               3
    ไม่พบข้อความ                            ออมทอง1

Test10 ทดสอบการแก้ไขรายรับรายจ่ายสำเร็จเมื่อกรอกข้อมูลครบ
    เข้าหน้ารายการรายรับรายจ่าย
    Sleep                               3
    Wait Until Page Contains Element        //*[@id="content-body"]/div/div[2]/div/div[3]/div[6]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]    300
    Sleep                               3
    Click Element                           //*[@id="content-body"]/div/div[2]/div/div[3]/div[6]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]
    Sleep                               3
    กรอกรายการรายรับรายจ่าย                 ออมทอง1
    Sleep                               3
    กดปุ่มยืนยันเพิ่มรายการรายรับรายจ่าย
    Sleep                               3
    พบข้อความ                               ออมทอง1

# ยกเลิก
Test11 ทดสอบการยกเลิกรายรับรายจ่ายไม่สำเร็จเมื่อกดปุ่มยกเลิก เฉพาะรายการรหัส 001-012
    เข้าหน้ารายการรายรับรายจ่าย
    Sleep                               3
    Wait Until Page Contains Element        //*[@id="content-body"]/div/div[2]/div/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[2]    300
    Sleep                               3
    Click Element                           //*[@id="content-body"]/div/div[2]/div/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[2]
    Sleep                               3
    Wait Until Element Is Not Visible       //*[@id="deleteinout"]

Test12 ทดสอบการยกเลิกรายรับรายจ่ายไม่สำเร็จเมื่อปฏิเสธการยืนยันการลบ
    เข้าหน้ารายการรายรับรายจ่าย
    Sleep                               3
    Wait Until Page Contains Element        //*[@id="content-body"]/div/div[2]/div/div[3]/div[6]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[2]    300
    Sleep                               3
    Click Element                           //*[@id="content-body"]/div/div[2]/div/div[3]/div[6]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[2]
    Sleep                               3
    Wait Until Element Is Visible           //*[@id="deleteinout"]/div[2]/button[1]
    Sleep                               3
    Click Element                           //*[@id="deleteinout"]/div[2]/button[1]
    Sleep                               3
    พบข้อความ                               ออมทอง1

Test13 ทดสอบการยกเลิกรายรับรายจ่ายสำเร็จเมื่อตกลงการยืนยันการลบ
    เข้าหน้ารายการรายรับรายจ่าย
    Sleep                               3
    Wait Until Page Contains Element        //*[@id="content-body"]/div/div[2]/div/div[3]/div[6]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[2]    300
    Sleep                               3
    Click Element                           //*[@id="content-body"]/div/div[2]/div/div[3]/div[6]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[2]
    Sleep                               3
    Wait Until Element Is Visible           //*[@id="deleteinout"]/div[2]/button[2]
    Sleep                               3
    Click Element                           //*[@id="deleteinout"]/div[2]/button[2]
    Sleep                               3
    ไม่พบข้อความ                            ออมทอง1

*** Keywords ***
เข้าหน้ารายการรายรับรายจ่าย
    Sleep                                   2
    Wait Until Page Contains Element        //*[@id="ledger_category"]                                                                                    300
    Click Element                           //*[@id="ledger_category"]

กดปุ่มเพิ่มรายการรายรับรายจ่าย
    Sleep                                   2
    Wait Until Page Contains Element        //*[@id="addinout"]                                                                                           300
    Click Element                           //*[@id="addinout"]

กดปุ่มกากบาทเพิ่มรายการรายรับรายจ่าย
    Sleep                                   2
    Wait Until Page Contains Element        //*[@id="closeaddinout"]                                                                                      300
    Click Element                           //*[@id="closeaddinout"]

กดปุ่มยกเลิกเพิ่มรายการรายรับรายจ่าย
    Sleep                                   2
    Wait Until Page Contains Element        //*[@id="cancelinout"]                                                                                        300
    Click Element                           //*[@id="cancelinout"]

กดปุ่มยืนยันเพิ่มรายการรายรับรายจ่าย
    Sleep                                   2
    Wait Until Page Contains Element        //*[@id="confirminout"]                                                                                       300
    Click Element                           //*[@id="confirminout"]

กรอกรายการรายรับรายจ่าย
    [Arguments]                             ${value}
    Sleep                                   2
    Wait Until Element Is Visible           //*[@id="menuinout"]                                                                                          300
    Input Text                              //*[@id="menuinout"]                                                                                          ${value}

เลือกประเภทรายรับรายจ่าย
    Sleep                                   1
    Wait Until Element Is Visible           //*[@id="typeinout"]/i                                                                                        300
    Click Element                           //*[@id="typeinout"]/i
    Sleep                                   1
    Wait Until Element Is Visible           //*[@id="typeinout"]/div[2]/div[1]/div/i
    Click Element                           //*[@id="typeinout"]/div[2]/div[1]/div/i