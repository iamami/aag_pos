*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบและinit_data Infocustomer
Test Setup        เข้าหน้ารายรับรายจ่าย
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data Infocustomer
*** Variables ***

*** Test cases ***
Test23 ทดสอบการเข้าหน้าpreviewสำเร็จ
    Sleep                               1
    Maximize Browser Window
    Sleep                               5
    กดปุ่มเพิ่มรายการรายรับรายจ่าย
    Sleep                               5
    เลือกรายการรายรับรายจ่าย
    Sleep                               5
    เลือกประเภทการชำระรายรับรายจ่าย     2
    Sleep                               1
    Wait Until Element Is Visible       id:staff                                                     300
    Sleep                               5
    Click Element                       id:staff
    Wait Until Element Is Visible       //*[@id="staff"]/div[2]/div[1]/span                          300
    Sleep                               5
    Click Element                       //*[@id="staff"]/div[2]/div[1]/span
    Sleep                               1
    Input Text                          id:inputcashIn-Out                                           5000
    Sleep                               1
    Input Text                          id:ex                                                        ลูกค้าทั่วไป
    Sleep                               1
    Wait Until Element Is Visible       id:confirmIn-Out                                             300
    Sleep                               5
    Click Element                       id:confirmIn-Out
    Sleep                               5
    ตรวจพบข้อความบน Alert               บันทึกข้อมูลสำเร็จ!
    Sleep                               5
    กดปุ่มพิมพ์รายรับรายจ่าย
    Sleep                               5
    พบข้อความ                           5,000.00
    Sleep                               5
    พบข้อความ                           RE-01191100001
    พบข้อความ                           เงินสด ณ ต้นวัน
    พบข้อความ                           ลูกค้าทั่วไป
    กดปุ่มปิดPopupPreview

Test24 ทดสอบการยกเลิกการพิมพ์เมื่อกดปุ่มปิด
    Sleep                                1
    กดปุ่มพิมพ์รายรับรายจ่าย
    Sleep                               1
    กดปุ่มปิดPopupPreview
    Sleep                               5
    พบปุ่มเพิ่มรายการ
    Sleep                               5
    พบข้อความ                           เลขที่ใบสำคัญ
    #พบหน้ารายรับ รายจ่าย
*** Keywords ***
กดปุ่มพิมพ์รายรับรายจ่าย
    Wait Until Page Contains Element    //*[@id="root-content"]/div/div/form[2]/div[2]/div/button    300
    Sleep                               5
    Click Button                        //*[@id="root-content"]/div/div/form[2]/div[2]/div/button

กดปุ่มปิดPopupPreview
    Sleep                               5
    Click Element                       //*[@id="btnPrint"]

พบปุ่มเพิ่มรายการ
    Wait Until Element Is Visible       id:addIn-Out                                                 300