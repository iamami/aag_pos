*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Library           String
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบและinit_data Infocustomer
Test Setup        เข้าหน้ารายรับรายจ่าย
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data Infocustomer
*** Variables ***

*** Test cases ***
Test1 ทดสอบการคำนวณรายรับ รายจ่าย คงเหลือเมื่อไม่มีรายการ
    Sleep                               1
    Wait Until Page Contains Element    id:btnSearch                300
    Click Element                       id:btnSearch
        #ตรวจสอบรายรับ
    Sleep                                1
    ตรวจสอบรายรับ                       0
        #ตรวจสอบรายจ่าย
    Sleep                                1
    ตรวจสอบรายจ่าย                      0
        #ตรวจสอบยอดคงเหลือ
    Sleep                                1
    ตรวจสอบยอดคงเหลือ                   0

Test2 ทดสอบการคำนวณรายรับ รายจ่าย คงเหลือเมื่อมีรายการ
    Maximize Browser Window
    Sleep                               1
    #เตรียมข้อมูล - ขายทอง
    เข้าหน้าPos ซื้อ-ขายทอง
    # ########
    Sleep                                1
    เข้าหน้าขายทอง
    Sleep                               3
    เลือกพนักงานคนที่                   3
    Sleep                               1
    กดเพิ่มรายการสินค้า
    Sleep                               1
    เลือกชื่อสินค้า
    Sleep                               1
    กดปุ่มลงรายการ
    Sleep                               1
    กดปุ่มบันทึกและชำระเงิน
    Sleep                               1
    ${SE_temp}                          Get Value                     id:cash
    ${SE_temp}=                         Remove String                 ${SE_temp}                    ,    .00
    ${SE}                               Convert To Number             ${SE_temp}
    Sleep                               1
    กรอกจำนวนรับเงินสด                  ${SE_temp}
    Sleep                               1
    กดปุ่มบันทึกชำระเงิน
    Sleep                               1
    ตรวจพบข้อความบน Alert               ยืนยันการชำระเงิน
    Sleep                               1
    ตรวจพบข้อความบน Alert               บันทึกบิลและชำระเงินสำเร็จ
    Sleep                               1
    ปิดpopupPOS
    ####
    Sleep                                1
    เข้าหน้าขายทอง
    Sleep                               1
    เลือกพนักงานคนที่                   3
    Sleep                               1
    กดเพิ่มรายการสินค้า
    Sleep                               1
    เลือกชื่อสินค้า
    Sleep                               1
    กดปุ่มลงรายการ
    Sleep                               1
    กดปุ่มบันทึกและชำระเงิน
    Sleep                               1
    ${SE2_temp}                         Get Value                     id:cash
    ${SE2_temp}=                        Remove String                 ${SE2_temp}                   ,    .00
    ${SE2}                              Convert To Number             ${SE2_temp}
    Sleep                               1
    กรอกจำนวนรับเงินสด                  ${SE2_temp}
    Sleep                               1
    กดปุ่มบันทึกชำระเงิน
    Sleep                               1
    ตรวจพบข้อความบน Alert               ยืนยันการชำระเงิน
    Sleep                               1
    ตรวจพบข้อความบน Alert               บันทึกบิลและชำระเงินสำเร็จ
    Sleep                               1
    ปิดpopupPOS
    #####
    Sleep                                1
    เข้าหน้าซื้อทอง
    พบPopup POS
    Sleep                               1
    กดเพิ่มรายการสินค้า
    Sleep                               1
    เลือกเปอร์เซ็นต์ทอง90เปอร์เซ็นต์
    Sleep                               1
    กรอกชื่อสินค้า                      สร้อยคอ
    Sleep                               1
    กรอกนน.(ก)                          3
    Sleep                               1
    ${BU_temp}                          Get Value                     //*[@id="money"]/div/input
    ${BU_temp}=                         Remove String                 ${BU_temp}                    ,    .00
    ${BU}                               Convert To Number             ${BU_temp}
    กดปุ่มลงรายการ
    Sleep                               1
    กดปุ่มบันทึกบิล
    Sleep                               1
    ตรวจพบข้อความบน Alert               บันทึกบิลและชำระเงินสำเร็จ
    ปิดpopupPOS
    #####
    Sleep                                1
    เข้าหน้าซื้อทอง
    พบPopup POS
    Sleep                               1
    กดเพิ่มรายการสินค้า
    Sleep                               1
    เลือกเปอร์เซ็นต์ทอง90เปอร์เซ็นต์
    Sleep                               1
    กรอกชื่อสินค้า                      สร้อยข้อมือ
    Sleep                               1
    กรอกนน.(ก)                          2
    Sleep                               1
    ${BU2_temp}                         Get Value                     //*[@id="money"]/div/input
    ${BU2_temp}=                        Remove String                 ${BU2_temp}                   ,    .00
    ${BU2}                              Convert To Number             ${BU2_temp}
    กดปุ่มลงรายการ
    Sleep                               1
    กดปุ่มบันทึกบิล
    Sleep                               1
    ตรวจพบข้อความบน Alert               บันทึกบิลและชำระเงินสำเร็จ
    Sleep                               5
    ปิดpopupPOS
    #####
    Sleep                               5
    เข้าหน้ารายรับรายจ่าย
    Sleep                               2
    #ตรวจสอบรายรับ
    Sleep                                1
    ${Total_SE}                         Evaluate                      ${SE}+${SE2}
    ตรวจสอบรายรับ                       ${Total_SE}
    #ตรวจสอบรายจ่าย
    Sleep                                1
    ${Total_BU}                         Evaluate                      ${BU}+${BU2}
    ตรวจสอบรายจ่าย                      ${Total_BU}
    # ตรวจสอบยอดคงเหลือ
    Sleep                                1
    ${TOTALS}                           Evaluate                      ${Total_SE}-${Total_BU}
    ตรวจสอบยอดคงเหลือ                   ${TOTALS}

*** Keywords ***
