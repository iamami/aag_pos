*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบและinit_data Infocustomer
Test Setup        เข้าหน้ารายรับรายจ่าย
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data Infocustomer
*** Variables ***

*** Test cases ***
เตรียมข้อมูล
    Sleep                                1
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                                1
    เข้าหน้าขายทอง
    Sleep                                               1
    พบPopup POS
    Sleep                                               5
    เลือกพนักงานคนที่                                   3
    Sleep                                               1
    กดเพิ่มรายการสินค้า
    Sleep                                               1
    พบPopup เลือกสินค้าเพื่อขายทอง
    Sleep                                               5
    เลือกชื่อสินค้า
    Sleep                                               1
    กดปุ่มลงรายการ
    Sleep                                               1
    พบPopup POS
    Sleep                                               5
    กดปุ่มบันทึกและชำระเงิน
    Sleep                                               1
    พบPopup ชำระเงิน
    Sleep                                               1
    กรอกจำนวนรับเงินสด                                  10250
    Sleep                                               1
    กดปุ่มบันทึกชำระเงิน
    Sleep                                               5
    ตรวจพบข้อความบน Alert                               ยืนยันการชำระเงิน
    Sleep                                               5
    ตรวจพบข้อความบน Alert                               บันทึกบิลและชำระเงินสำเร็จ
    Sleep                                               1
    กดปุ่มVat
    Sleep                                               1
    กดปุ่มปิดPopup Preview
    Sleep                                               1
    ปิดpopupPOS
    Sleep                                               1
    #ขายทอง1
    เข้าหน้าขายทอง
    Sleep                                               1
    พบPopup POS
    Sleep                                               5
    เลือกพนักงานคนที่                                   3
    Sleep                                               1
    กดเพิ่มรายการสินค้า
    Sleep                                               1
    พบPopup เลือกสินค้าเพื่อขายทอง
    Sleep                                               1
    เลือกชื่อสินค้า
    Sleep                                               1
    กดปุ่มลงรายการ
    Sleep                                               1
    พบPopup POS
    Sleep                                               5
    กดปุ่มบันทึกและชำระเงิน
    Sleep                                               1
    พบPopup ชำระเงิน
    Sleep                                               1
    กรอกจำนวนรับเงินสด                                  10250
    Sleep                                               1
    กดปุ่มบันทึกชำระเงิน
    Sleep                                               5
    ตรวจพบข้อความบน Alert                               ยืนยันการชำระเงิน
    Sleep                                               6
    ตรวจพบข้อความบน Alert                               บันทึกบิลและชำระเงินสำเร็จ
    Sleep                                               1
    กดปุ่มVat
    Sleep                                               1
    กดปุ่มปิดPopup Preview
    Sleep                                               1
    ปิดpopupPOS
    Sleep                                               1
    #ขายทอง2
    เข้าหน้าซื้อทอง
    Sleep                                               1
    พบPopup POS
    Sleep                                               5
    กดเพิ่มรายการสินค้า
    Sleep                                               1
    เลือกเปอร์เซ็นต์ทอง90เปอร์เซ็นต์
    Sleep                                               1
    กรอกชื่อสินค้า                                      สร้อยคอ
    Sleep                                               1
    กรอกนน.(ก)                                          9
    Sleep                                               1
    กดปุ่มลงรายการ
    Sleep                                               1
    พบPopup POS
    Sleep                                               1
    กดปุ่มบันทึกและชำระเงิน
    Sleep                                               7
    ตรวจพบข้อความบน Alert                               บันทึกบิลและชำระเงินสำเร็จ
    Sleep                                               1
    กดปุ่มVat
    Sleep                                               1
    กดปุ่มปิดPopup Preview
    Sleep                                               1
    ปิดpopupPOS
    Sleep                                               1
    #ซื้อทอง1
    เข้าหน้าซื้อทอง
    Sleep                                               1
    พบPopup POS
    Sleep                                               5
    กดเพิ่มรายการสินค้า
    Sleep                                               1
    เลือกเปอร์เซ็นต์ทอง90เปอร์เซ็นต์
    Sleep                                               1
    กรอกชื่อสินค้า                                      สร้อยคอ
    Sleep                                               1
    กรอกนน.(ก)                                          9
    Sleep                                               1
    กดปุ่มลงรายการ
    Sleep                                               1
    พบPopup POS
    Sleep                                               5
    กดปุ่มบันทึกและชำระเงิน
    Sleep                                               7
    ตรวจพบข้อความบน Alert                               บันทึกบิลและชำระเงินสำเร็จ
    Sleep                                               1
    กดปุ่มVat
    Sleep                                               1
    กดปุ่มปิดPopup Preview
    Sleep                                               1
    ปิดpopupPOS
    Sleep                                               1
    #ซื้อทอง2
    เข้าหน้าเปลี่ยนทอง
    Sleep                                               1
    พบPopup POS
    Sleep                                               5
    กดเพิ่มรายการสินค้า
    Sleep                                               1
    กดเพิ่มรายการเปลี่ยนทอง
    Sleep                                               1
    กดปุ่มลงรายการ
    Sleep                                               1
    พบPopup POS
    Sleep                                               5
    กดปุ่มบันทึกและชำระเงิน
    Sleep                                               1    
    พบPopup ชำระเงิน
    Sleep                                               5
    กรอกจำนวนรับเงินสด                                      200000
    Sleep                                               1
    กดปุ่มบันทึกชำระเงิน
    Sleep                                               5
    ตรวจพบข้อความบน Alert                               ยืนยันการชำระเงิน
    Sleep                                               5
    ตรวจพบข้อความบน Alert                               บันทึกบิลและชำระเงินสำเร็จ
    Sleep                                               1
    กดปุ่มVat
    Sleep                                               1
    กดปุ่มปิดPopup Preview
    Sleep                                               1
    ปิดpopupPOS
    #เปลี่ยนทอง
    เข้าหน้าขายฝากหลัก
    Sleep                                               12
    Wait Until Page Contains Element                    //*[@id="client_width"]/div[1]/form/div[3]/div[1]/p[3]/div               300
    Sleep                                               1
    Click Element                                       //*[@id="client_width"]/div[1]/form/div[3]/div[1]/p[3]/div
    Sleep                                               1
    กดปุ่มค้นหารายการขายฝาก
    Sleep                                               1
    กดปุ่มแก้ไข
    Sleep                                               1
    กดแถบเพิ่ม/ลด
    Sleep                                               1
    กดปุ่มเพิ่ม
    Sleep                                               1
    Wait Until Element Is Visible                       id:btnDecease               300
    Sleep                                               1
    Click Element                                       id:btnDecease
    Sleep                                               5
    เลือกพนักงาน
    Sleep                                               1
    Wait Until Element Is Visible                       id:inputLedgerTotal               300
    Sleep                                               1
    Input Text                                          id:inputLedgerTotal                                                                                   3000
    Sleep                                               1
    กดบันทึกเพิ่ม/ลด
    Sleep                                               5
    Wait Until Element Is Visible                       id:inputCash               300
    Sleep                                               1
    Input Text                                          id:inputCash                                                                                          3000
    Sleep                                               1
    Wait Until Element Is Visible                       id:btnSave               300
    Sleep                                               5
    Click Element                                       id:btnSave
    Sleep                                               5
    ตรวจพบข้อความบน Alert                               ยืนยันการชำระเงิน
    Sleep                                               5
    ตรวจพบข้อความบน Alert                               บันทึกข้อมูลสำเร็จ
    Sleep                                               1
    #ลดเงินต้น
    Sleep                                               1
    Wait Until Page Contains Element                    //*[@id="tab2"]               300
    Sleep                                               1
    Click Element                                       //*[@id="tab2"]
    Sleep                                               2
    กดปุ่มเพิ่มรายการดอกเบี้ยรับ
    Sleep                                               1
    เลือกจำนวนเดือน
    Sleep                                               1
    Wait Until Element Is Visible                       //*[@id="btnSaveInterest"]               300
    Sleep                                               1
    Click Element                                       //*[@id="btnSaveInterest"]
    Sleep                                               1
    พบPopup ชำระเงิน
    Sleep                                               1
    กรอกจำนวนรับเงินสด                                  300
    Sleep                                               1
    กดปุ่มบันทึกชำระเงิน
    Sleep                                               5
    ตรวจพบข้อความบน Alert                               ยืนยันการชำระเงิน
    Sleep                                               5
    ตรวจพบข้อความบน Alert                               บันทึกข้อมูลสำเร็จ
    Sleep                                               1
    ปิดPopupขาย/ฝาก
    #ต่อดอก
    Sleep                                               12
    Wait Until Page Contains Element                    //*[@id="client_width"]/div[1]/form/div[3]/div[1]/p[3]/div               300
    Sleep                                               1
    Click Element                                      //*[@id="client_width"]/div[1]/form/div[3]/div[1]/p[3]/div
    Sleep                                               1
    กดปุ่มค้นหารายการขายฝาก
    Sleep                                               1
    กดปุ่มแก้ไข
    Sleep                                               1
    กดแถบเพิ่ม/ลด
    Sleep                                               1
    กดปุ่มเพิ่ม
    Sleep                                               1
    เลือกพนักงาน
    Sleep                                               1
    กรอกจำนวนเงินเพิ่ม/ลด
    Sleep                                               1
    กดบันทึกเพิ่ม/ลด
    Sleep                                               5
    ตรวจพบข้อความบน Alert                               บันทึกข้อมูลสำเร็จ
    Sleep                                               1
    ปิดPopupขาย/ฝาก
    #เพิ่มเงินต้น
    Sleep                                               7
    กรอกเลขที่ใบขายฝาก                                  AA01000
    Sleep                                               1
    กดปุ่มค้นหารายการขายฝาก
    Sleep                                               1
    กดปุ่มแก้ไข
    Sleep                                               1
    พบPopupขายฝาก
    Sleep                                               5
    กดปุ่มเพิ่มรายการดอกเบี้ยรับ
    Sleep                                               5
    กดปุ่มไถ่คืนในหน้าPopupต่อดอกไถ่คืน
    Sleep                                               1
    พบ popup ไถ่คืน
    Sleep                                               5
    พบข้อความ                                           ไถ่คืน
    Sleep                                               1
    กดปุ่มบันทึกไถ่คืน/ต่อดอก
    Sleep                                               1
    พบPopup ชำระเงิน
    Sleep                                               5
    กรอกจำนวนรับเงินสด                                  6000
    Sleep                                               1
    กดปุ่มบันทึกชำระเงิน
    Sleep                                               7
    ตรวจพบข้อความบน Alert                               ยืนยันการชำระเงิน
    Sleep                                               5
    ตรวจพบข้อความบน Alert                               บันทึกข้อมูลสำเร็จ
    Sleep                                               2
    ปิดPopupขายฝาก
    #ไถ่คืน
    กดปุ่มสร้างรายการขายฝาก
    Sleep                                               1
    พบPopupขายฝาก
    Sleep                                               1
    กรอกชื่อลูกค้า                                      linda
    Sleep                                               1
    กรอกจำนวนเงิน                                       30000
    กดปุ่มเพิ่มรายการสินค้า
    Sleep                                               1
    พบPopupเลือกรายการสินค้า
    Sleep                                               5
    เลือก % ทอง
    Sleep                                               1
    ใส่ชื่อสินค้า
    Sleep                                               1
    ใส่จำนวน
    Sleep                                               1
    ใส่น้ำหนัก
    Sleep                                               1
    กดปุ่มเพิ่มสินค้า
    Sleep                                               5
    พบPopupขายฝาก
    Sleep                                               5
    กดปุ่มสร้าง
    Sleep                                               1
    พบPopupจ่ายเงิน
    Sleep                                               5
    เลือกพนักงานเพื่อเพิ่ม
    Sleep                                               1
    กดปุ่มบันทึกการจ่ายเงิน
    Sleep                                               5
    ตรวจพบข้อความบน Alert                               บันทึกข้อมูลสำเร็จ
    #พบalert บันทึกข้อมูลสำเร็จ
    Sleep                                               5
    กดปุ่มพิมพ์สัญญา
    Sleep                                               1
    พบPopup Preview Lease
    Sleep                                               5
    กดปุ่มปิดPopup Preview Lease
    Sleep                                               2
    ปิดPopupขายฝาก
    #ขายฝาก
    เข้าหน้ารายรับรายจ่าย
    Sleep                                               1
    กดปุ่มเพิ่มรายการรายรับรายจ่าย
    Sleep                                               1
    เลือกรายการรายรับรายจ่าย
    Sleep                                               1
    เลือกประเภทการชำระรายรับรายจ่าย                     2
    Sleep                                               1
    เลือกพนักงานหน้ารายรับรายจ่าย
    Sleep                                               1
    กรอกชำระเงินสด                                      5000
    Sleep                                               1
    กรอกหมายเหตุ                                        ลูกค้าทั่วไป
    Sleep                                               1
    กดปุ่มบันทึกรายรับรายจ่าย
    Sleep                                               10
    ตรวจพบข้อความบน Alert                               บันทึกข้อมูลสำเร็จ!
    #เงินสดต้นวัน


Test25 ทดสอบการค้นหาเมื่อกรอกเลขที่ใบสำคัญเป็น RE-01190800005
    Sleep                                               1
    กรอกเลขที่ใบสำคัญ                                   RE-01190800005
    Sleep                                               2
    กดปุ่มค้นหารายรับรายจ่าย
    Sleep                                               10
    พบจำนวนรายการรายรับรายจ่าย                          1

Test26 ทดสอบการค้นหาเมื่อประเภทรายรับ
    Sleep                                               1
    เลือกประเภทรายรับ
    Sleep                                               1
    กดปุ่มค้นหารายรับรายจ่าย
    Sleep                                               10
    พบจำนวนรายการรายรับรายจ่าย                          7

Test27 ทดสอบการค้นหาเมื่อประเภทรายจ่าย
    Sleep                                               1
    เลือกประเภทรายจ่าย
    Sleep                                               1
    กดปุ่มค้นหารายรับรายจ่าย
    Sleep                                               10
    พบจำนวนรายการรายรับรายจ่าย                          4

Test28 ทดสอบการค้นหาเมื่อเลือกประเภทรายรับและรายการทั้งหมด
    Sleep                                               1
    เลือกประเภทรายรับ
    Sleep                                               1
    เลือกรายการทั้งหมดเมื่อเลือกประเภทรายรับ/รายจ่าย
    Sleep                                               1
    กดปุ่มค้นหารายรับรายจ่าย
    Sleep                                               10
    พบจำนวนรายการรายรับรายจ่าย                          7

Test29 ทดสอบการค้นหาเมื่อเลือกประเภทรายรับและรายการขายทอง
    Sleep                                               1
    เลือกประเภทรายรับ
    Sleep                                               1
    เลือกรายการขาย
    Sleep                                               1
    กดปุ่มค้นหารายรับรายจ่าย
    Sleep                                               10
    พบจำนวนรายการรายรับรายจ่าย                          2

Test30 ทดสอบการค้นหาเมื่อเลือกประเภทรายรับและรายการเปลี่ยนทอง
    Sleep                                               1
    เลือกประเภทรายรับ
    Sleep                                               1
    เลือกรายการเปลี่ยน
    Sleep                                               1
    กดปุ่มค้นหารายรับรายจ่าย
    Sleep                                               10
    พบจำนวนรายการรายรับรายจ่าย                          1

Test32 ทดสอบการค้นหาเมื่อเลือกประเภทรายรับและรายการค่าดอกเบี้ย
    Sleep                                               1
    เลือกประเภทรายรับ
    Sleep                                               1
    เลือกรายการค่าดอกเบี้ย
    Sleep                                               1
    กดปุ่มค้นหารายรับรายจ่าย
    Sleep                                               10
    พบจำนวนรายการรายรับรายจ่าย                          1

Test33 ทดสอบการค้นหาเมื่อเลือกประเภทรายรับและรายการลดเงินต้น
    Sleep                                               1
    เลือกประเภทรายรับ
    Sleep                                               1
    เลือกรายการลดเงินต้น
    Sleep                                               1
    กดปุ่มค้นหารายรับรายจ่าย
    Sleep                                               10
    พบจำนวนรายการรายรับรายจ่าย                          1

Test34 ทดสอบการค้นหาเมื่อเลือกประเภทรายรับและรายการเงินต้น+ดอกเบี้ยไถ่คืน
    Sleep                                               1
    เลือกประเภทรายรับ
    Sleep                                               1
    เลือกรายการเงินต้น+ดอกเบี้ยไถ่คืน
    Sleep                                               1
    กดปุ่มค้นหารายรับรายจ่าย
    Sleep                                               10
    พบจำนวนรายการรายรับรายจ่าย                          1

Test35 ทดสอบการค้นหาเมื่อเลือกประเภทรายรับและรายการเงินสด ณ ต้นวัน
    Sleep                                               1
    เลือกประเภทรายรับ
    Sleep                                               1
    เลือกรายการเงินสด ณ ต้นวัน
    Sleep                                               1
    กดปุ่มค้นหารายรับรายจ่าย
    Sleep                                               10
    พบจำนวนรายการรายรับรายจ่าย                          1


Test36 ทดสอบการค้นหาเมื่อเลือกประเภททั้งหมดและรายการทั้งหมด
    Sleep                                               1
    เลือกประเภททั้งหมด
    Sleep                                               1
    เลือกรายการทั้งหมดเมื่อเลือกประเภททั้งหมด
    Sleep                                               1
    กดปุ่มค้นหารายรับรายจ่าย
    Sleep                                               10
    พบจำนวนรายการรายรับรายจ่าย                          11

Test37 ทดสอบการค้นหาเมื่อเลือกประเภทรายจ่ายและรายการทั้งหมด
    Sleep                                               1
    เลือกประเภทรายจ่าย
    Sleep                                               1
    เลือกรายการทั้งหมดเมื่อเลือกประเภทรายรับ/รายจ่าย
    Sleep                                               1
    กดปุ่มค้นหารายรับรายจ่าย
    Sleep                                               10
    พบจำนวนรายการรายรับรายจ่าย                          4

Test38 ทดสอบการค้นหาเมื่อเลือกประเภทรายจ่ายและรายการซื้อทอง
    Sleep                                               1
    เลือกประเภทรายจ่าย
    Sleep                                               1
    เลือกรายการซื้อ
    Sleep                                               1
    กดปุ่มค้นหารายรับรายจ่าย
    Sleep                                               10
    พบจำนวนรายการรายรับรายจ่าย                          2

Test39 ทดสอบการค้นหาเมื่อเลือกประเภทรายจ่ายและรายการขายฝาก
    Sleep                                               1
    เลือกประเภทรายจ่าย
    Sleep                                               1
    เลือกรายการขายฝาก
    Sleep                                               1
    กดปุ่มค้นหารายรับรายจ่าย
    Sleep                                               10
    พบจำนวนรายการรายรับรายจ่าย                          1

Test40 ทดสอบการค้นหาเมื่อเลือกประเภทรายจ่ายและรายการเพิ่มเงินต้น
    Sleep                                               1
    เลือกประเภทรายจ่าย
    Sleep                                               1
    เลือกรายการเพิ่มเงินต้น
    Sleep                                               1
    กดปุ่มค้นหารายรับรายจ่าย
    Sleep                                               10
    พบจำนวนรายการรายรับรายจ่าย                          1

Test41 ทดสอบการค้นหาเมื่อกรอกชื่อลูกค้าเป็นlinda
    Sleep                                               1
    กรอกชื่อลูกค้าเพื่อหารายรับรายจ่าย                  linda
    Sleep                                               2
    กดปุ่มค้นหารายรับรายจ่าย
    Sleep                                               10
    พบจำนวนรายการรายรับรายจ่าย                          3

Test42 ทดสอบการค้นหาเมื่อกรอกเลขที่อ้างอิงเป็น CH-01190800001
    Sleep                                               1
    กรอกเลขที่อ้างอิง                                   CH-01190800001
    Sleep                                               2
    กดปุ่มค้นหารายรับรายจ่าย
    Sleep                                               10
    พบจำนวนรายการรายรับรายจ่าย                          1

Test43 ทดสอบการค้นหาเมื่อกดปุ่มทั้งหมด
    Sleep                                               1
    กดปุ่มค้นหารายรับรายจ่าย
    Sleep                                               10
    พบจำนวนรายการรายรับรายจ่าย                          11

Test44 ทดสอบการลบรายการซื้อทอง ขายทอง เปลี่ยนทองไม่สำเร็จ
    Sleep                                               1
    กดปุ่มค้นหารายรับรายจ่าย
    Sleep                                               1
    Wait Until Element Is Visible                       //*[@id="table_width"]/div/div[1]/div[3]/div[7]/div/div/div[2]/div/div[1]/div/div/div/div/center/a               300
    Sleep                                               1
    Click Element                                       //*[@id="table_width"]/div/div[1]/div[3]/div[7]/div/div/div[2]/div/div[1]/div/div/div/div/center/a
    Sleep                                               5
    ตรวจพบข้อความบน Alert                               กรุณายกเลิกบิลก่อน
    Sleep                                               10
    พบจำนวนรายการรายรับรายจ่าย                          11

Test45 ทดสอบการลบรายการขายฝากไม่สำเร็จ
    Sleep                                               1
    กดปุ่มค้นหารายรับรายจ่าย
    Sleep                                               1
    Wait Until Element Is Visible                       //*[@id="table_width"]/div/div[1]/div[3]/div[5]/div/div/div[2]/div/div[1]/div/div/div/div/center/a               300
    Sleep                                               1
    Click Element                                       //*[@id="table_width"]/div/div[1]/div[3]/div[5]/div/div/div[2]/div/div[1]/div/div/div/div/center/a
    Sleep                                               5
    ตรวจพบข้อความบน Alert                               ยืนยันลบรายการนี้
    Sleep                                               5
    ตรวจพบข้อความบน Alert                               undefined
    Sleep                                               10
    พบจำนวนรายการรายรับรายจ่าย                          11



*** Keywords ***