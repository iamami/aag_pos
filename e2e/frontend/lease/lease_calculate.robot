*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Library           String
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบและinit_data Infocustomer
Test Setup        เข้าหน้าขายฝากหลัก
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data Infocustomer
*** Variables ***

*** Test cases ***
Test83 ทดสอบการแสดงข้อมูลใน popup คำนวณ เมื่อเลือกค้นหาด้วยรายการไถ่คืนทั้งหมด
    Sleep                               7
    Wait Until Page Contains Element    //*[@id="client_width"]/div[1]/form/div[3]/div[2]/p[3]/div    300
    Sleep                               1
    Click Element                       //*[@id="client_width"]/div[1]/form/div[3]/div[2]/p[3]/div
    Sleep                               1
    Wait Until Page Contains Element    id:btnSearch                                                  300
    Sleep                               2
    Click Element                       id:btnSearch
    เข้าหน้าคำนวณ
    Sleep                               1
    #ตรวจสอบน้ำหนักรวม
    ${totalweight_temp}                 Get Text                                                      id:totalweight
    ${totalweight_t}                    Remove String                                                 ${totalweight_temp}                                                                          .00
    # ${totalweight}                      Convert To Integer                                            ${totalweight_t}
    ${w1_temp}                          Get Text                                                      //*[@id="total-box2"]/div/div/div[3]/div/div/div/div[2]/div/div[2]/div/div/div/div/div
    ${w1_t}                             Remove String                                                 ${w1_temp}                                                                                   .000
    # ${w1}                               Convert To Integer                                            ${w1_t}
    # ${weight}                           Evaluate                                                      ${w1}
    Should Be Equal                     ${w1_t}                                                    ${totalweight_t}
    # ตรวจสอบยอดขายฝาก
    ${b1_temp}                          Get Text                                                      //*[@id="least"]
    ${b1_t}                             Remove String                                                 ${b1_temp}                                                                                ,    .00
    # ${b1}                               Convert To Integer                                            ${b1_t}
    ${totalinterest_temp}               Get Text                                                      id:least
    ${totalinterest_t}                  Remove String                                                 ${totalinterest_temp}                                                                     ,    .00
    # ${totalinterest}                    Convert To Integer                                            ${totalinterest_t}
    # ${total_b}                          Evaluate                                                      ${b1}
    Should Be Equal                     ${b1_t}                                                    ${totalinterest_t}
    Sleep                               1
    ปิดหน้าคำนวณ

Test84 ทดสอบการแสดงข้อมูลใน popup คำนวณ เมื่อเลือกค้นหาด้วยรายการทั้งหมด
    เข้าหน้าคำนวณ
    Sleep                               1
    ${increse}                          Get Text                                                      id:increse
    Should Be Equal                     0.00                                                          ${increse}
    ${decrese}                          Get Text                                                      id:decrese
    Should Be Equal                     0.00                                                          ${decrese}
    ${total}                            Get Text                                                      id:total
    Should Be Equal                     0.00                                                          ${total}
    Sleep                               1
    ปิดหน้าคำนวณ


*** Keywords ***
เข้าหน้าคำนวณ
    Sleep                               1
    Wait Until Page Contains Element    id:calculate                                                  300
    Sleep                               1
    Click Element                       id:calculate

ปิดหน้าคำนวณ
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="modalCal"]/i                                         300
    Sleep                               1
    Click Element                       //*[@id="modalCal"]/i