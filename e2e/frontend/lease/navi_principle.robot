*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบและinit_data Infocustomer
Test Setup        เข้าหน้าขายฝากหลัก
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data Infocustomer
*** Variables ***

*** Test cases ***
Test45 ทดสอบเข้าหน้าเพิ่มลดเงินต้นไม่สำเร็จ เมื่อค้นหาด้วยค่าว่าง
    กดปุ่มเพิ่มลดเงินต้น
    Sleep                                        5
    พบข้อความ                 เพิ่มลดเงินต้น
    Sleep                                        5
    กดปุ่มค้นหาบิล
    Sleep                                        5
    ตรวจพบข้อความบน Alert     ไม่พบข้อมูลขายฝากนี้
    Sleep                                        5
    #พบalert ไม่พบข้อมูลขายฝากนี้

Test46 ทดสอบเข้าหน้าเพิ่มลดเงินต้นไม่สำเร็จ เมื่อค้นหาด้วยบิลที่ถูกไถ่คืนไปแล้ว
    กดปุ่มเพิ่มลดเงินต้น
    Sleep                                        5
    พบข้อความ                 เพิ่มลดเงินต้น
    Sleep                                        5
    Input Text                id:inputBillNum           AA01003
    Sleep                                        5
    กดปุ่มค้นหาบิล
    Sleep                                        5
    ตรวจพบข้อความบน Alert     ใบขายฝากถูกไถ่คืนแล้ว!
    #พบalert ไม่พบข้อมูลขายฝากนี้

Test47 ทดสอบเข้าหน้าเพิ่มลดเงินต้นสำเร็จ
    กดปุ่มเพิ่มลดเงินต้น
    Sleep                                        5
    พบข้อความ                 เพิ่มลดเงินต้น
    Sleep                                        5
    Input Text                id:inputBillNum           AA01001
    Sleep                                        5
    กดปุ่มค้นหาบิล
    Sleep                                        5
    พบpopupเพิ่มลดเงินต้น
    Sleep                                        5
    ปิดpopupเพิ่มลดเงินต้น
    Sleep                                        5
    Sleep                     1
    ปิดPopupขายฝาก

*** Keywords ***
