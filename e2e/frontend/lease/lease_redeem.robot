*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Library           String
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบและinit_data Infocustomer
Test Setup        เข้าหน้าขายฝากหลัก
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data Infocustomer

*** Variables ***


*** Test cases ***
Test26 ทดสอบการคำนวณยอดจ่ายเงินสดถูกต้อง
    Sleep                                      7
    กรอกเลขที่ใบขายฝาก                         AA01000
    Sleep                               5
    กดปุ่มค้นหารายการขายฝาก
    Sleep                               5
    กดปุ่มแก้ไข
    Sleep                               5
    พบPopupขายฝาก
    Sleep                               5
    กดปุ่มเพิ่มรายการดอกเบี้ยรับ
    Sleep                                      5
    กดปุ่มไถ่คืนในหน้าPopupต่อดอกไถ่คืน
    Sleep                               10
    พบ popup ไถ่คืน
    Sleep                               5
    พบข้อความ                                  ไถ่คืน
    Sleep                               5
    จำนวนเดือน                                 2
    Sleep                               5
    ยอดจ่ายเงินสดเท่ากับยอดดอกเบี้ย+เงินต้น
    Sleep                               5
    #พบยอดดอกเบี้ย+เงินต้น
    ปิดpopup ชำระเงิน
    Sleep                               5
    ปิดpopupต่อดอกไถ่คืน
    Sleep                               5
    ปิดPopupขายฝาก


Test27 ทดสอบการไถ่คืนสำเร็จ
    Sleep                                      7
    กรอกเลขที่ใบขายฝาก                         AA01000
    Sleep                               5
    กดปุ่มค้นหารายการขายฝาก
    Sleep                               5
    กดปุ่มแก้ไข
    Sleep                               5
    พบPopupขายฝาก
    Sleep                               5
    กดปุ่มเพิ่มรายการดอกเบี้ยรับ
    Sleep                               5
    กดปุ่มไถ่คืนในหน้าPopupต่อดอกไถ่คืน
    Sleep                               5
    พบ popup ไถ่คืน
    Sleep                               5
    พบข้อความ                                  ไถ่คืน
    Sleep                               5
    กดปุ่มบันทึกไถ่คืน/ต่อดอก
    Sleep                               5
    พบPopup ชำระเงิน
    Sleep                               5
    กรอกจำนวนรับเงินสด                         10600
    Sleep                               5
    กดปุ่มบันทึกชำระเงิน
    Sleep                               5
    ตรวจพบข้อความบน Alert                      ยืนยันการชำระเงิน
    Sleep                               5
    ตรวจพบข้อความบน Alert                      บันทึกข้อมูลสำเร็จ
    Sleep                               5
    ปิดPopupขายฝาก
    Sleep                               5
    กรอกเลขที่ใบขายฝาก                         AA01000
    Sleep                               5
    กดปุ่มค้นหารายการขายฝาก
    Sleep                               5
    กดปุ่มแก้ไข
    Sleep                               5
    พบข้อความ                                  สถานะ ไถ่คืน
    Sleep                               5
    #พบสถานะไถ่คืนที่หน้าแก้ไข
    ปิดPopupขายฝาก


*** Keywords ***


จำนวนเดือน
    [Arguments]                                ${value}
    Wait Until Element Is Visible              //*[@id="selectMonth"]                                                  300
    Sleep                               5
    Click Element                              //*[@id="selectMonth"]
    Sleep                               5
    Wait Until Element Is Visible              //*[@id="selectMonth"]/div[2]/div[${value}]/span                                                  300
    Sleep                               5
    Click Element                              //*[@id="selectMonth"]/div[2]/div[${value}]/span

ยอดจ่ายเงินสดเท่ากับยอดดอกเบี้ย+เงินต้น
    ${value1}                                  Get Value                                                                                                  id:inputAmount
    ${c_value1}                                Remove String                                                                                              ${value1}                                                                   ,    .00
    ${money_amount}                            Convert To Integer                                                                                         ${c_value1}
    ${value2}                                  Get Value                                                                                                  id:inputInterestPay
    ${c_value2}                                Remove String                                                                                              ${value2}                                                                   ,    .00
    ${interest_pay}                            Convert To Integer                                                                                         ${c_value2}
    ${sum}                                     Evaluate                                                                                                   ${interest_pay}+${money_amount}
    ${sum1}                                    Convert To Integer                                                                                         ${sum}
    กดปุ่มบันทึกไถ่คืน/ต่อดอก
    Sleep                               5
    พบPopup ชำระเงิน
    Sleep                               5
    ${value3}                                  Get Value                                                                                                  //*[@id="descriptModalPayment"]/div/div/div/div[1]/form[1]/div/div/input
    ${c_value3}                                Remove String                                                                                              ${value3}                                                                   ,    .00
    ${sum2}                                    Convert To String                                                                                          ${c_value3}
    Should Be Equal As Strings                 ${sum1}                                                                                                    ${sum2}

กดปุ่มแก้ไข
    Wait Until Page Contains Element           //*[@id="client_width"]/div[2]/div[1]/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]                                                  300
    Sleep                               5
    Click Element           //*[@id="client_width"]/div[2]/div[1]/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]   