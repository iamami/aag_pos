*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบและinit_data Infocustomer
Test Setup        เข้าหน้าขายฝากหลัก
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data Infocustomer
*** Variables ***

*** Test cases ***
Test21 ทดสอบเข้าหน้าไถ่คืนไม่สำเร็จ เมื่อค้นหาด้วยค่าว่าง
    กดปุ่มไถ่คืน
    Sleep                                        5
    พบข้อความ                  ไถ่คืน
    Sleep                                        5
    กดปุ่มค้นหาบิล
    Sleep                                        5
    ตรวจพบข้อความบน Alert      ไม่พบข้อมูลขายฝากนี้
    #พบalert ไม่พบข้อมูลขายฝากนี้

Test22 ทดสอบเข้าหน้าไถ่คืนไม่สำเร็จ เมื่อค้นหาด้วยค่าว่า invalid
    กดปุ่มไถ่คืน
    Sleep                                        5
    พบข้อความ                  ไถ่คืน
    Sleep                                        5
    กรอกเลขที่บิลเพื่อค้นหา    AA0000
    Sleep                                        5
    กดปุ่มค้นหาบิล
    Sleep                                        5
    ตรวจพบข้อความบน Alert      ไม่พบข้อมูลขายฝากนี้
    #พบalert ใบขายฝากถูกไถ่คืนแล้ว!

Test23 ทดสอบเข้าหน้าไถ่คืนไม่สำเร็จ เมื่อค้นหาด้วยบิลที่ถูกไถ่คืนไปแล้ว
    กดปุ่มไถ่คืน
    Sleep                                        5
    พบข้อความ                  ไถ่คืน
    Sleep                                        5
    กรอกเลขที่บิลเพื่อค้นหา    AA01003
    Sleep                                        5
    กดปุ่มค้นหาบิล
    Sleep                                        5
    ตรวจพบข้อความบน Alert      ใบขายฝากถูกไถ่คืนแล้ว!
    #พบalert ใบขายฝากถูกไถ่คืนแล้ว!

Test24 ทดสอบเข้าหน้าไถ่คืนสำเร็จ
    กดปุ่มไถ่คืน
    Sleep                                        5
    พบข้อความ                  ไถ่คืน
    Sleep                                        5
    กรอกเลขที่บิลเพื่อค้นหา    AA01001
    Sleep                                        5
    กดปุ่มค้นหาบิล
    Sleep                                        5
    พบ popup ไถ่คืน
    #พบ popup ต่อดอก
    Sleep                                        5
    ปิดpopupต่อดอกไถ่คืน
    Sleep                                        5
    ปิดPopupขายฝาก

Test25 ทดสอบเข้าหน้าไถ่คืนสำเร็จเมื่อกด Enter
    กดปุ่มไถ่คืน
    Sleep                                        5
    พบข้อความ                  ไถ่คืน
    Sleep                                        5
    กรอกเลขที่บิลเพื่อค้นหา    AA01001
    Sleep                                        5
    Press Key                  id:btnSearch              \\13
    Sleep                                        5
    พบ popup ไถ่คืน
    Sleep                                        5
    #พบ popup ต่อดอก
    ปิดpopupต่อดอกไถ่คืน
    Sleep                                        5
    ปิดPopupขายฝาก

*** Keywords ***



