*** Settings ***
Library           Selenium2Library
Library           DateTime
Library           String
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบและinit_data Infocustomer
Test Setup        เข้าหน้าขายฝากหลัก
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data Infocustomer
*** Variables ***

*** Test cases ***
Test31 ทดสอบการแสดงผล คิดเป็นเวลา ถูกต้อง
    Sleep                               13
    Wait Until Page Contains Element    //*[@id="btnCreateLease"]
    Click Element                       //*[@id="btnCreateLease"]
    Sleep                               1
    กรอกชื่อลูกค้า                      linda
    Sleep                               5
    กรอกจำนวนเงิน                       12345
    Sleep                               1
    กดเพิ่มรายการวันนี้
    Sleep                               1
    กดปุ่มเพิ่มรายการดอกเบี้ยรับ
    Sleep                               1
    เลือกจำนวนเดือน
    Sleep                               1
    ${monthselect}                      Get Text                                                                                              //*[@id="selectMonth"]/div[1]
    ${month}                            Get Value                                                                                             //*[@id="inputMonth"]
    Should Be Equal                     ${monthselect}                                                                                        ${month}
    Sleep                               1
    ปิดpopupต่อดอก
    Sleep                               1
    ปิดPopupขาย/ฝาก

Test32 ทดสอบการแสดงจำนวนเดือนและจำนวนวันเมื่อเลือกวันที่
    Sleep                               13
    กดค้นหารายการวันนี้
    Sleep                               1
    กดปุ่มแก้ไข
    Sleep                               1
    กดปุ่มเพิ่มรายการดอกเบี้ยรับ
    Sleep                               1
    เลือกจำนวนเดือน
    Sleep                               1
    ${monthselect}                      Get Text                                                                                              //*[@id="selectMonth"]/div[1]
    ${month}                            Get Value                                                                                             //*[@id="inputMonth"]
    Should Be Equal                     ${monthselect}                                                                                        ${month}
    ${end_m1}                           Get Value                                                                                             //*[@id="inputDate"]
    ${end_m2}                           Get Value                                                                                             //*[@id="inputDate"]
    Should Be Equal                     ${end_m1}                                                                                             ${end_m2}
    Sleep                               1
    ปิดpopupต่อดอก
    Sleep                               1
    ปิดPopupขาย/ฝาก

Test33 ทดสอบการดอกเบั้ยเมื่อเลือกคิดเป็นรายวัน
    Sleep                               13
    กดค้นหารายการวันนี้
    Sleep                               1
    กดปุ่มแก้ไข
    Sleep                               1
    กดปุ่มเพิ่มรายการดอกเบี้ยรับ
    Sleep                               1
    เลือกจำนวนเดือน
    Sleep                               1
    เลือกenddate
    Sleep                               5
    ${ans_temp}                         Get Value                                                                                             //*[@id="inputInterestMonth"]
    ${ans}                              Convert To Number                                                                                     ${ans_temp}
                #ตรวจดอกเบี้ย
    ${cash_temp}                        Get Value                                                                                             //*[@id="Amountm"]
    ${cash_t}                           Remove String                                                                                         ${cash_temp}                         ,
    ${cash}                             Convert To Number                                                                                     ${cash_t}
    ${interest_temp}                    Get Value                                                                                             //*[@id="InterestRatio"]
    ${interest_t}                       Convert To Number                                                                                     ${interest_temp}
    ${interest}                         Evaluate                                                                                              ${interest_t}/100
    ${result_temp}                      Evaluate                                                                                              ${cash}*${interest}*${ans}
    ${result}                           Convert To Integer                                                                                    ${result_temp}
    ${result}=                          Convert To Number                                                                                     ${result}
    ${result}=                          Evaluate                                                                                              ${result}+1
    ${interestcal_temp}                 Get Value                                                                                             //*[@id="InterestTotal"]
    ${interestcal}                      Convert To Number                                                                                     ${interestcal_temp}
    Should Be Equal                     ${result}                                                                                             ${interestcal}
    Sleep                               1
    ปิดpopupต่อดอก
    Sleep                               1
    ปิดPopupขาย/ฝาก

Test34 ทดสอบการดอกเบั้ยเมื่อเลือกคิดเป็น1/4
    Sleep                               13
    กดค้นหารายการวันนี้
    Sleep                               1
    กดปุ่มแก้ไข
    Sleep                               1
    กดปุ่มเพิ่มรายการดอกเบี้ยรับ
    Sleep                               1
    เลือกจำนวนเดือน
    Sleep                               1
    เลือกenddate
    Sleep                               5
    ${inr}                              Convert To Number                                                                                     0.25
                # เลือก1/4ของเดือน
    Wait Until Element Is Visible       //*[@id="radio2"]/div
    Sleep                               1
    Click Element                       //*[@id="radio2"]/div
    ${monthtemp}                        Get Value                                                                                             //*[@id="inputMonth"]
    ${month}                            Convert To Number                                                                                     ${monthtemp}
    ${daytemp}                          Get Value                                                                                             //*[@id="inputDate"]
    ${amountdate}                       Evaluate                                                                                              ${month}+${inr}
    ${amountdate}=                      Evaluate                                                                                              "%.2f" % ${amountdate}
    ${amountdate}=                      Convert To Number                                                                                     ${amountdate}
    ${ans_temp}                         Get Value                                                                                             //*[@id="inputInterestMonth"]
    ${ans}                              Convert To Number                                                                                     ${ans_temp}
                #ตรวจจำนวนวัน
    Should Be Equal                     ${amountdate}                                                                                         ${ans}
                #ตรวจดอกเบี้ย
    ${cash_temp}                        Get Value                                                                                             //*[@id="Amountm"]
    ${cash_t}                           Remove String                                                                                         ${cash_temp}                         ,
    ${cash}                             Convert To Number                                                                                     ${cash_t}
    ${interest_temp}                    Get Value                                                                                             //*[@id="InterestRatio"]
    ${interest_t}                       Convert To Number                                                                                     ${interest_temp}
    ${interest}                         Evaluate                                                                                              ${interest_t}/100
    ${result_temp}                      Evaluate                                                                                              ${cash}*${interest}*${amountdate}
    ${result}                           Convert To Number                                                                                     ${result_temp}
    ${result}=                          Convert To Integer                                                                                    ${result}
    ${result}=                          Evaluate                                                                                              ${result}+1
    ${interestcal_temp}                 Get Value                                                                                             //*[@id="InterestTotal"]
    ${interestcal}                      Convert To Number                                                                                     ${interestcal_temp}
    Should Be Equal                     ${result}                                                                                             ${interestcal}
    Sleep                               1
    ปิดpopupต่อดอก
    Sleep                               1
    ปิดPopupขาย/ฝาก

Test35 ทดสอบการดอกเบั้ยเมื่อเลือกคิดเป็น1/2
    Sleep                               13
    กดค้นหารายการวันนี้
    Sleep                               1
    กดปุ่มแก้ไข
    Sleep                               1
    กดปุ่มเพิ่มรายการดอกเบี้ยรับ
    Sleep                               1
    เลือกจำนวนเดือน
    Sleep                               1
    เลือกenddate
    Sleep                               5
    ${inr}                              Convert To Number                                                                                     0.5
                # เลือก1/2ของเดือน
    Wait Until Element Is Visible       //*[@id="radio3"]/div
    Sleep                               1
    Click Element                       //*[@id="radio3"]/div
    ${monthtemp}                        Get Value                                                                                             //*[@id="inputMonth"]
    ${month}                            Convert To Number                                                                                     ${monthtemp}
    ${daytemp}                          Get Value                                                                                             //*[@id="inputDate"]
    ${amountdate}                       Evaluate                                                                                              ${month}+${inr}
    ${amountdate}=                      Evaluate                                                                                              "%.2f" % ${amountdate}
    ${amountdate}=                      Convert To Number                                                                                     ${amountdate}
    ${ans_temp}                         Get Value                                                                                             //*[@id="inputInterestMonth"]
    ${ans}                              Convert To Number                                                                                     ${ans_temp}
                #ตรวจจำนวนวัน
    Should Be Equal                     ${amountdate}                                                                                         ${ans}
                #ตรวจดอกเบี้ย
    ${cash_temp}                        Get Value                                                                                             //*[@id="Amountm"]
    ${cash_t}                           Remove String                                                                                         ${cash_temp}                         ,
    ${cash}                             Convert To Number                                                                                     ${cash_t}
    ${interest_temp}                    Get Value                                                                                             //*[@id="InterestRatio"]
    ${interest_t}                       Convert To Number                                                                                     ${interest_temp}
    ${interest}                         Evaluate                                                                                              ${interest_t}/100
    ${result_temp}                      Evaluate                                                                                              ${cash}*${interest}*${amountdate}
    ${result}                           Convert To Number                                                                                     ${result_temp}
    ${result}=                          Convert To Integer                                                                                    ${result}
    ${result}=                          Evaluate                                                                                              ${result}+1
    ${interestcal_temp}                 Get Value                                                                                             //*[@id="InterestTotal"]
    ${interestcal}                      Convert To Number                                                                                     ${interestcal_temp}
    Should Be Equal                     ${result}                                                                                             ${interestcal}
    Sleep                               1
    ปิดpopupต่อดอก
    Sleep                               1
    ปิดPopupขาย/ฝาก

Test36 ทดสอบการดอกเบั้ยเมื่อเลือกคิดเป็น3/4
    Sleep                               13
    กดค้นหารายการวันนี้
    Sleep                               1
    กดปุ่มแก้ไข
    Sleep                               1
    กดปุ่มเพิ่มรายการดอกเบี้ยรับ
    Sleep                               1
    เลือกจำนวนเดือน
    Sleep                               1
    เลือกenddate
    Sleep                               5
    ${inr}                              Convert To Number                                                                                     0.75
                # เลือก3/4ของเดือน
    Wait Until Element Is Visible       //*[@id="radio4"]/div
    Sleep                               5
    Click Element                       //*[@id="radio4"]/div
    ${monthtemp}                        Get Value                                                                                             //*[@id="inputMonth"]
    ${month}                            Convert To Number                                                                                     ${monthtemp}
    ${daytemp}                          Get Value                                                                                             //*[@id="inputDate"]
    ${amountdate}                       Evaluate                                                                                              ${month}+${inr}
    ${amountdate}=                      Evaluate                                                                                              "%.2f" % ${amountdate}
    ${amountdate}=                      Convert To Number                                                                                     ${amountdate}
    ${ans_temp}                         Get Value                                                                                             //*[@id="inputInterestMonth"]
    ${ans}                              Convert To Number                                                                                     ${ans_temp}
                #ตรวจจำนวนวัน
    Should Be Equal                     ${amountdate}                                                                                         ${ans}
                #ตรวจดอกเบี้ย
    ${cash_temp}                        Get Value                                                                                             //*[@id="Amountm"]
    ${cash_t}                           Remove String                                                                                         ${cash_temp}                         ,
    ${cash}                             Convert To Number                                                                                     ${cash_t}
    ${interest_temp}                    Get Value                                                                                             //*[@id="InterestRatio"]
    ${interest_t}                       Convert To Number                                                                                     ${interest_temp}
    ${interest}                         Evaluate                                                                                              ${interest_t}/100
    ${result_temp}                      Evaluate                                                                                              ${cash}*${interest}*${amountdate}
    ${result}                           Convert To Number                                                                                     ${result_temp}
    ${result}=                          Convert To Integer                                                                                    ${result}
    ${result}=                          Evaluate                                                                                              ${result}+1
    ${interestcal_temp}                 Get Value                                                                                             //*[@id="InterestTotal"]
    ${interestcal}                      Convert To Number                                                                                     ${interestcal_temp}
    Should Be Equal                     ${result}                                                                                             ${interestcal}
    Sleep                               1
    ปิดpopupต่อดอก
    Sleep                               1
    ปิดPopupขาย/ฝาก

Test37 ทดสอบการดอกเบั้ยเมื่อเลือกคิดเป็น1เดือน
    Sleep                               13
    กดค้นหารายการวันนี้
    Sleep                               1
    กดปุ่มแก้ไข
    Sleep                               2
    กดปุ่มเพิ่มรายการดอกเบี้ยรับ
    Sleep                               1
    เลือกจำนวนเดือน
    Sleep                               1
    เลือกenddate
    Sleep                               1
    # เลือก1เดือน
    Wait Until Element Is Visible       //*[@id="radio5"]/div
    Sleep                               1
    Click Element                       //*[@id="radio5"]/div
    ${monthtemp}                        Get Value                                                                                             //*[@id="inputMonth"]
    ${month}                            Convert To Number                                                                                     ${monthtemp}
    ${amountdate}                       Evaluate                                                                                              "%.2f" % ${month}
    ${amountdate}=                      Convert To Number                                                                                     ${amountdate}
    ${ans_temp}                         Get Value                                                                                             //*[@id="inputInterestMonth"]
    ${ans}                              Convert To Number                                                                                     ${ans_temp}
    #ตรวจจำนวนวัน
    Sleep                               3
    Should Be Equal                     ${amountdate}                                                                                         ${ans}
    #ตรวจดอกเบี้ย
    ${cash_temp}                        Get Value                                                                                             //*[@id="Amountm"]
    ${cash_t}                           Remove String                                                                                         ${cash_temp}                         ,
    ${cash}                             Convert To Number                                                                                     ${cash_t}
    ${interest_temp}                    Get Value                                                                                             //*[@id="InterestRatio"]
    ${interest_t}                       Convert To Number                                                                                     ${interest_temp}
    ${interest}                         Evaluate                                                                                              ${interest_t}/100
    ${result_temp}                      Evaluate                                                                                              ${cash}*${interest}*${amountdate}
    ${result}                           Convert To Number                                                                                     ${result_temp}
    ${result}=                          Convert To Integer                                                                                    ${result}
    ${result}=                          Evaluate                                                                                              ${result}+1
    ${interestcal_temp}                 Get Value                                                                                             //*[@id="InterestTotal"]
    ${interestcal}                      Convert To Number                                                                                     ${interestcal_temp}
    Should Be Equal                     ${result}                                                                                             ${interestcal}
    Sleep                               1
    ปิดpopupต่อดอก
    Sleep                               1
    ปิดPopupขาย/ฝาก

Test38 ทดสอบการดอกเบั้ยเมื่อเลือกไม่คิดดอกเบี้ย
    Sleep                               13
    กดค้นหารายการวันนี้
    Sleep                               1
    กดปุ่มแก้ไข
    Sleep                               1
    กดปุ่มเพิ่มรายการดอกเบี้ยรับ
    Sleep                               1
    เลือกจำนวนเดือน
    Sleep                               1
    เลือกenddate
    Sleep                               5
                # เลือกไม่คิดดอกเบี้ย
    Wait Until Element Is Visible       //*[@id="radio6"]/div
    Sleep                               5
    Click Element                       //*[@id="radio6"]/div
    ${monthtemp}                        Get Value                                                                                             //*[@id="inputMonth"]
    ${month}                            Convert To Number                                                                                     ${monthtemp}
    ${amountdate}                       Evaluate                                                                                              "%.2f" % ${month}
    ${amountdate}=                      Convert To Number                                                                                     ${amountdate}
    ${ans_temp}                         Get Value                                                                                             //*[@id="inputInterestMonth"]
    ${ans}                              Convert To Number                                                                                     ${ans_temp}
                #ตรวจจำนวนวัน
    Sleep                               3
    Should Be Equal                     ${amountdate}                                                                                         ${ans}
                #ตรวจดอกเบี้ย
    ${cash_temp}                        Get Value                                                                                             //*[@id="Amountm"]
    ${cash_t}                           Remove String                                                                                         ${cash_temp}                         ,
    ${cash}                             Convert To Number                                                                                     ${cash_t}
    ${interest_temp}                    Get Value                                                                                             //*[@id="InterestRatio"]
    ${interest_t}                       Convert To Number                                                                                     ${interest_temp}
    ${interest}                         Evaluate                                                                                              ${interest_t}/100
    ${result_temp}                      Evaluate                                                                                              ${cash}*${interest}*${amountdate}
    ${result}                           Convert To Number                                                                                     ${result_temp}
    ${result}=                          Convert To Integer                                                                                    ${result}
    ${result}=                          Evaluate                                                                                              ${result}+1
    ${interestcal_temp}                 Get Value                                                                                             //*[@id="InterestTotal"]
    ${interestcal}                      Convert To Number                                                                                     ${interestcal_temp}
    Should Be Equal                     ${result}                                                                                             ${interestcal}
    Sleep                               1
    ปิดpopupต่อดอก
    Sleep                               1
    ปิดPopupขาย/ฝาก

Test39 ทดสอบการบันทึกการต่อดอกสำเร็จ เมื่อไม่ระบุจำนวนเดือน
    Sleep                               13
    กดค้นหารายการวันนี้
    Sleep                               1
    กดปุ่มแก้ไข
    Sleep                               1
    กดปุ่มเพิ่มรายการดอกเบี้ยรับ
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="btnSaveInterest"]                                                  300
    Sleep                               5
    Click Element                       //*[@id="btnSaveInterest"]
    Sleep                               5
    พบข้อความ                           ชำระเงิน
    Wait Until Element Is Visible       id:closePaymentModal                                                  300
    Sleep                               5
    Click Element                       id:closePaymentModal
    Sleep                               1
    ปิดpopupต่อดอก
    Sleep                               1
    ปิดPopupขาย/ฝาก

Test40 ทดสอบการบันทึกการต่อดอกสำเร็จ เมื่อระบุจำนวนเดือน
    Sleep                               13
    กดค้นหารายการวันนี้
    Sleep                               1
    กดปุ่มแก้ไข
    Sleep                               1
    กดปุ่มเพิ่มรายการดอกเบี้ยรับ
    Sleep                               1
    เลือกจำนวนเดือน
    Sleep                               5
    Wait Until Element Is Visible       //*[@id="btnSaveInterest"]                                                  300
    Sleep                               5
    Click Element                       //*[@id="btnSaveInterest"]
    Sleep                               5
    พบข้อความ                           ชำระเงิน
    Wait Until Element Is Visible       id:closePaymentModal                                                  300
    Sleep                               5
    Click Element                       id:closePaymentModal
    Sleep                               1
    ปิดpopupต่อดอก
    Sleep                               1
    ปิดPopupขาย/ฝาก

Test41 ทดสอบการไม่สามารถแก้ไขจำนวนเงิน ในหน้าต่อดอก
    Sleep                               13
    กดค้นหารายการวันนี้
    Sleep                               1
    กดปุ่มแก้ไข
    Sleep                               1
    กดปุ่มเพิ่มรายการดอกเบี้ยรับ
    Sleep                               5
    Wait Until Page Contains Element    id:Amountm                                                  300
    ${value} =                          Get Element Attribute                                                                                 id:Amountm                           readOnly
    Should Be Equal As Strings          ${value}                                                                                              true
    Sleep                               5
    ปิดpopupต่อดอก
    Sleep                               1
    ปิดPopupขาย/ฝาก

Test42 ทดสอบการไม่สามารถแก้ไขอัตราดอกเบี้ย (%) ในหน้าต่อดอก
    Sleep                               13
    กดค้นหารายการวันนี้
    Sleep                               1
    กดปุ่มแก้ไข
    Sleep                               1
    กดปุ่มเพิ่มรายการดอกเบี้ยรับ
    Sleep                               5
    Wait Until Page Contains Element    id:InterestRatio                                                  300
    ${value} =                          Get Element Attribute                                                                                 id:InterestRatio                     readOnly
    Should Be Equal As Strings          ${value}                                                                                              true
    Sleep                               5
    ปิดpopupต่อดอก
    Sleep                               1
    ปิดPopupขาย/ฝาก

Test44 ทดสอบการไม่สามารถแก้ไขดอกเบี้ยคำนวณ ในหน้าต่อดอก
    Sleep                               13
    กดค้นหารายการวันนี้
    Sleep                               1
    กดปุ่มแก้ไข
    Sleep                               1
    กดปุ่มเพิ่มรายการดอกเบี้ยรับ
    Sleep                               5
    Wait Until Page Contains Element    id:InterestTotal                                                  300
    ${value} =                          Get Element Attribute                                                                                 id:InterestTotal                     readOnly
    Should Be Equal As Strings          ${value}                                                                                              true
    Sleep                               5
    ปิดpopupต่อดอก
    Sleep                               1
    ปิดPopupขาย/ฝาก

*** Keywords ***
กดปุ่มแก้ไข
    Sleep                               10
    Wait Until Page Contains Element    //*[@id="client_width"]/div[2]/div[1]/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]                                                 300
    Sleep                               5
    Click Element                       //*[@id="client_width"]/div[2]/div[1]/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]

ปิดpopupต่อดอก
    Sleep                               1
    Wait Until Element Is Visible       id:btnCloseInterest                                                  300
    Sleep                               5
    Click Element                       id:btnCloseInterest

เลือกenddate
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="modalInterest"]/div[2]/form/table/tr[2]/td[4]/div[1]/div/input                                                 300
    Sleep                               5
    Click Element                       //*[@id="modalInterest"]/div[2]/form/table/tr[2]/td[4]/div[1]/div/input
    Wait Until Element Is Visible       //*[@id="modalInterest"]/div[2]/form/table/tr[2]/td[4]/div[2]/div/div[2]/div[2]/div[5]/div[4]                                                 300
    Sleep                               5
    Click Element                       //*[@id="modalInterest"]/div[2]/form/table/tr[2]/td[4]/div[2]/div/div[2]/div[2]/div[5]/div[4]

กดเพิ่มรายการวันนี้
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="inputAmount"]                                                  300
    Sleep                               5
    Input Text                          //*[@id="inputAmount"]                                                                                12345
    Sleep                               5
    Wait Until Element Is Visible       //*[@id="content-body"]/form/div/div[2]/button                                                  300
    Sleep                               5
    Click Element                       //*[@id="content-body"]/form/div/div[2]/button
    Sleep                               5
    Wait Until Element Is Visible       //*[@id="contentModalAddProduct"]/form/div[1]/select                                                  300
    Sleep                               5
    Click Element                       //*[@id="contentModalAddProduct"]/form/div[1]/select
    Sleep                               5
    Wait Until Element Is Visible       //*[@id="contentModalAddProduct"]/form/div[1]/select/option[3]                                                  300
    Sleep                               5
    Click Element                       //*[@id="contentModalAddProduct"]/form/div[1]/select/option[3]
    Sleep                               5
    Wait Until Element Is Visible       //*[@id="inputNameProduct"]                                                  300
    Sleep                               5
    Input Text                          //*[@id="inputNameProduct"]                                                                           สร้อยคอ
    Sleep                               5
    Wait Until Element Is Visible       //*[@id="inputWeight"]                                                  300
    Sleep                               5
    Input Text                          //*[@id="inputWeight"]                                                                                1
    Sleep                               5
    Wait Until Element Is Visible       //*[@id="inputNumber"]                                                  300
    Sleep                               5
    Input Text                          //*[@id="inputNumber"]                                                                                1
    Sleep                               5
    Wait Until Element Is Visible       id:btnAdd                                                  300
    Sleep                               5
    Click Element                       id:btnAdd
    Sleep                               5
    Wait Until Element Is Visible       //*[@id="btnSaveLease"]                                                  300
    Sleep                               5
    Click Element                       //*[@id="btnSaveLease"]
    Sleep                               5
    เลือกพนักงานเพื่อเพิ่ม
    Sleep                               5
    Wait Until Element Is Visible       //*[@id="btnSavePayLease"]                                                  300
    Sleep                               5
    Click Element                       //*[@id="btnSavePayLease"]
    Sleep                               5
    ตรวจพบข้อความบน Alert               บันทึกข้อมูลสำเร็จ

กรอกชื่อลูกค้า
    [Arguments]                         ${name}
    Wait Until Element Is Visible       //*[@id="searchNameCus2"]/input                                                  300
    Sleep                               5
    Input Text                          //*[@id="searchNameCus2"]/input                                                                       ${name}

กรอกเลขบัตรประชาชน
    [Arguments]                         ${value}
    Wait Until Element Is Visible       //*[@id="inputID"]                                                  300
    Sleep                               5
    Click Element                       //*[@id="inputID"]
    Sleep                               5
    Input Text                          //*[@id="inputID"]                                                                                    ${value}

กรอกเบอร์โทรศัพท์
    [Arguments]                         ${value}
    Wait Until Element Is Visible       //*[@id="inputPhone"]                                                  300
    Sleep                               5
    Input Text                          //*[@id="inputPhone"]                                                                                 ${value}

กรอกจำนวนเงิน
    [Arguments]                         ${value}
    Wait Until Element Is Visible       id:inputAmount                                                  300
    Sleep                               5
    Click Element                       id:inputAmount
    Sleep                               5
    Input Text                          id:inputAmount                                                                                        ${value}

เลือกพนักงาน
    Wait Until Element Is Visible       //*[@id="dropDownEmp"]/i                                                  300
    Sleep                               5
    Click Element                       //*[@id="dropDownEmp"]/i
    Sleep                               5
    Wait Until Element Is Visible       //*[@id="dropDownEmp"]/div[2]/div[3]                                                  300
    Sleep                               5
    Click Element                       //*[@id="dropDownEmp"]/div[2]/div[3]
    Sleep                               1

กดค้นหารายการวันนี้
    Sleep                               3
    Wait Until Page Contains Element    //*[@id="client_width"]/div[1]/form/div[3]/div[1]/p[1]/div                            300
    Sleep                               5
    Click Element                       //*[@id="client_width"]/div[1]/form/div[3]/div[1]/p[1]/div
    Sleep                               5
    Wait Until Page Contains Element    id:btnSearch
    Sleep                               5
    Click Element                       id:btnSearch
    Sleep                               1