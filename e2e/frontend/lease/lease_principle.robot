*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบและinit_data Infocustomer
Test Setup        เข้าหน้าขายฝากหลัก
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data Infocustomer
*** Variables ***

*** Test cases ***
Test49 ทดสอบการไม่สามารถแก้ไขเลขที่ขายฝาก ในหน้า popup เพิ่ม
    Sleep                               13
    Wait Until Page Contains Element    //*[@id="btnCreateLease"]                                                  300
    Sleep                               5
    Click Element                       //*[@id="btnCreateLease"]
    Sleep                               1
    กรอกชื่อลูกค้า                      linda
    Sleep                               5
    กรอกจำนวนเงิน                       12345
    Sleep                               5
    กดเพิ่มรายการวันนี้
    Sleep                               5
    กดแถบเพิ่ม/ลด
    Sleep                               1
    กดปุ่มเพิ่ม
    Sleep                               1
    Wait Until Page Contains Element    id:leaseNum                                                  300
    Sleep                               5
    ${value} =                          Get Element Attribute                                                                                 id:leaseNum                                                                                        readOnly
    Should Be Equal As Strings          ${value}                                                                                             true
    กดปิดpopupเพิ่ม/ลด
    ปิดpopupขาย/ฝาก

Test50 ทดสอบการไม่สามารถแก้ไขจำนวนเงินต้น ในหน้า popup เพิ่ม
    Sleep                               13
    กดค้นหารายการวันนี้
    Sleep                               1
    กดปุ่มแก้ไข
    Sleep                               1
    กดแถบเพิ่ม/ลด
    Sleep                               1
    กดปุ่มเพิ่ม
    Sleep                               1
    Wait Until Page Contains Element    id:inputLeaseAmount                                                  300
    Sleep                               5
    ${value} =                          Get Element Attribute                                                                                 id:inputLeaseAmount                                                                                readOnly
    Should Be Equal As Strings          ${value}                                                                                true
    กดปิดpopupเพิ่ม/ลด
    ปิดpopupขาย/ฝาก

Test51 ทดสอบการบันทึกการเพิ่มเงินต้นไม่สำเร็จ เมื่อไม่กรอกข้อมูลพนักงาน
    Sleep                               13
    กดค้นหารายการวันนี้
    Sleep                               1
    กดปุ่มแก้ไข
    Sleep                               1
    กดแถบเพิ่ม/ลด
    Sleep                               1
    กดปุ่มเพิ่ม
    Sleep                               1
    กดบันทึกเพิ่ม/ลด
    Sleep                               5
    พบข้อความ                           เลือกพนักงาน
    Sleep                               5
    กดปิดpopupเพิ่ม/ลด
    ปิดpopupขาย/ฝาก

Test52 ทดสอบการบันทึกการเพิ่มเงินต้นไม่สำเร็จ เมื่อกรอกข้อมูลเป็นค่าว่าง
    Sleep                               13
    กดค้นหารายการวันนี้
    Sleep                               1
    กดปุ่มแก้ไข
    Sleep                               1
    กดแถบเพิ่ม/ลด
    Sleep                               1
    กดปุ่มเพิ่ม
    Sleep                               1
    เลือกพนักงาน
    Sleep                               5
    กดบันทึกเพิ่ม/ลด
    Sleep                               5
    พบข้อความ                           ระบุยอดจำนวนเงินเพิ่ม/ลด
    Sleep                               5
    กดปิดpopupเพิ่ม/ลด
    ปิดpopupขาย/ฝาก

Test53 ทดสอบการเพิ่มเงินต้นสำเร็จ
    Sleep                               13
    กดค้นหารายการวันนี้
    Sleep                               1
    กดปุ่มแก้ไข
    Sleep                               1
    กดแถบเพิ่ม/ลด
    Sleep                               1
    กดปุ่มเพิ่ม
    Sleep                               1
    เลือกพนักงาน
    Sleep                               5
    กรอกจำนวนเงินเพิ่ม/ลด
    Sleep                               5
    กดบันทึกเพิ่ม/ลด
    Sleep                               5
    ตรวจพบข้อความบน Alert               บันทึกข้อมูลสำเร็จ
    Sleep                               5
    กดแถบเพิ่ม/ลด
    Sleep                               5
    Wait Until Element Is Visible       //*[@id="content-body2"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[4]/div/div/div/div/div                                                  300
    ${value}                            Get Text                                                                                              //*[@id="content-body2"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[4]/div/div/div/div/div
    Should Be Equal                     ${value}                                                                                              5,000.00
    ปิดpopupขาย/ฝาก

Test54 ทดสอบการลดเงินต้นสำเร็จ
    Sleep                               13
    กดค้นหารายการวันนี้
    Sleep                               1
    กดปุ่มแก้ไข
    Sleep                               1
    กดแถบเพิ่ม/ลด
    Sleep                               1
    กดปุ่มเพิ่ม
    Sleep                               1
    Wait Until Element Is Visible       id:btnDecease                                                  300
    Sleep                               5
    Click Element                       id:btnDecease
    เลือกพนักงาน
    กรอกจำนวนเงินเพิ่ม/ลด
    กดบันทึกเพิ่ม/ลด
    Wait Until Element Is Visible       id:inputCash                                                  300
    Sleep                               5
    Input Text                          id:inputCash                                                                                          5000
    Sleep                               1
    Wait Until Element Is Visible       id:btnSave                                                  300
    Sleep                               5
    Click Element                       id:btnSave
    Sleep                               5
    ตรวจพบข้อความบน Alert               ยืนยันการชำระเงิน
    Sleep                               5
    ตรวจพบข้อความบน Alert               บันทึกข้อมูลสำเร็จ
    Sleep                               5
    กดแถบเพิ่ม/ลด
    Sleep                               5
    Wait Until Element Is Visible       //*[@id="content-body2"]/div/div[1]/div[3]/div[2]/div/div/div[2]/div/div[5]/div/div/div/div/div                                                  300
    ${value}                            Get Text                                                                                              //*[@id="content-body2"]/div/div[1]/div[3]/div[2]/div/div/div[2]/div/div[5]/div/div/div/div/div
    Should Be Equal                     ${value}                                                                                              5,000.00
    ปิดpopupขาย/ฝาก

*** Keywords ***
กดปิดpopupเพิ่ม/ลด
    Wait Until Element Is Visible       id:btnCloseInDe                                                  300
    Sleep                               5
    Click Element                       id:btnCloseInDe

ปิดpopupขาย/ฝาก
    Wait Until Element Is Visible       id:btnClose                                                  300
    Sleep                               5
    Click Element                       id:btnClose

กดเพิ่มรายการวันนี้
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="inputAmount"]                                                  300
    Sleep                               5
    Input Text                          //*[@id="inputAmount"]                                                                                12345
    Sleep                               5
    Wait Until Element Is Visible       //*[@id="content-body"]/form/div/div[2]/button                                                  300
    Sleep                               5
    Click Element                       //*[@id="content-body"]/form/div/div[2]/button
    Sleep                               5
    Wait Until Element Is Visible       //*[@id="contentModalAddProduct"]/form/div[1]/select                                                  300
    Sleep                               5
    Click Element                       //*[@id="contentModalAddProduct"]/form/div[1]/select
    Sleep                               5
    Wait Until Element Is Visible       //*[@id="contentModalAddProduct"]/form/div[1]/select/option[3]                                                  300
    Sleep                               5
    Click Element                       //*[@id="contentModalAddProduct"]/form/div[1]/select/option[3]
    Sleep                               5
    Wait Until Element Is Visible       //*[@id="inputNameProduct"]                                                  300
    Sleep                               5
    Input Text                          //*[@id="inputNameProduct"]                                                                           สร้อยคอ
    Sleep                               5
    Wait Until Element Is Visible       //*[@id="inputWeight"]                                                  300
    Sleep                               5
    Input Text                          //*[@id="inputWeight"]                                                                                1
    Sleep                               5
    Wait Until Element Is Visible       //*[@id="inputNumber"]                                                  300
    Sleep                               5
    Input Text                          //*[@id="inputNumber"]                                                                                1
    Sleep                               5
    Wait Until Element Is Visible       id:btnAdd                                                  300
    Sleep                               5
    Click Element                       id:btnAdd
    Sleep                               5
    Wait Until Element Is Visible       //*[@id="btnSaveLease"]                                                  300
    Sleep                               5
    Click Element                       //*[@id="btnSaveLease"]
    Sleep                               5
    เลือกพนักงานขาย
    Sleep                               5
    Wait Until Element Is Visible       //*[@id="btnSavePayLease"]                                                  300
    Sleep                               5
    Click Element                       //*[@id="btnSavePayLease"]
    Sleep                               5
    ตรวจพบข้อความบน Alert               บันทึกข้อมูลสำเร็จ

กรอกชื่อลูกค้า
    [Arguments]                         ${name}
    Wait Until Element Is Visible       //*[@id="searchNameCus2"]/input                                                  300
    Sleep                               1
    Input Text                          //*[@id="searchNameCus2"]/input                                                                       ${name}

กรอกเลขบัตรประชาชน
    [Arguments]                         ${value}
    Wait Until Element Is Visible       //*[@id="inputID"]                                                  300
    Sleep                               1
    Click Element                       //*[@id="inputID"]
    Sleep                               5
    Input Text                          //*[@id="inputID"]                                                                                    ${value}

กรอกเบอร์โทรศัพท์
    [Arguments]                         ${value}
    Wait Until Element Is Visible       //*[@id="inputPhone"]                                                  300
    Sleep                               1
    Input Text                          //*[@id="inputPhone"]                                                                                 ${value}

กรอกจำนวนเงิน
    [Arguments]                         ${value}
    Wait Until Element Is Visible       id:inputAmount                                                  300
    Sleep                               1
    Click Element                       id:inputAmount
    Sleep                               5
    Input Text                          id:inputAmount                                                                                        ${value}

เลือกพนักงานขาย
    Wait Until Element Is Visible       //*[@id="dropDownEmp"]/i                                                  300
    Sleep                               5
    Click Element                       //*[@id="dropDownEmp"]/i
    Sleep                               5
    Wait Until Element Is Visible       //*[@id="dropDownEmp"]/div[2]/div[3]                                                  300
    Sleep                               5
    Click Element                       //*[@id="dropDownEmp"]/div[2]/div[3]
    Sleep                               1

กดค้นหารายการวันนี้
    Sleep                               3
    Wait Until Page Contains Element    //*[@id="client_width"]/div[1]/form/div[3]/div[1]/p[1]/div                                                 300
    Sleep                               1
    Click Element                       //*[@id="client_width"]/div[1]/form/div[3]/div[1]/p[1]/div
    Sleep                               1
    Wait Until Page Contains Element    id:btnSearch
    Sleep                               5
    Click Element                       id:btnSearch
    Sleep                               1