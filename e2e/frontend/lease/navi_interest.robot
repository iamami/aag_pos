*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบและinit_data Infocustomer
Test Setup        เข้าหน้าขายฝากหลัก
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data Infocustomer
*** Variables ***

*** Test cases ***
Test28 ทดสอบเข้าหน้าต่อดอกไม่สำเร็จ เมื่อค้นหาด้วยค่าว่าง
    กดปุ่มต่อดอก
    Sleep                            5
    พบข้อความ                        ต่อดอก
    Sleep                            5
    กดปุ่มค้นหาบิล
    Sleep                            5
    ตรวจพบข้อความบน Alert            ไม่พบข้อมูลขายฝากนี้
    #พบalert ไม่พบข้อมูลขายฝากนี้

Test29 ทดสอบเข้าหน้าต่อดอกไม่สำเร็จ เมื่อค้นหาด้วยบิลที่ถูกไถ่คืนไปแล้ว
    กดปุ่มต่อดอก
    Sleep                            5
    พบข้อความ                        ต่อดอก
    Sleep                            5
    กรอกเลขที่บิลเพื่อค้นหา          AA01003
    Sleep                            5
    กดปุ่มค้นหาบิล
    Sleep                            5
    ตรวจพบข้อความบน Alert            ใบขายฝากถูกไถ่คืนแล้ว!
    #พบalert ใบขายฝากถูกไถ่คืนแล้ว!

Test30 ทดสอบเข้าหน้าต่อดอกสำเร็จ
    กดปุ่มต่อดอก
    Sleep                            5
    พบข้อความ                        ต่อดอก
    Sleep                            5
    กรอกเลขที่บิลเพื่อค้นหา          AA01001
    Sleep                            5
    กดปุ่มค้นหาบิล
    Sleep                            5
    พบ popup ต่อดอก
    Sleep                            5
    #พบ popup ต่อดอก
    Sleep                            5
    ปิดpopupต่อดอกไถ่คืน
    Sleep                            5
    ปิดPopupขายฝาก

*** Keywords ***
พบ popup ต่อดอก
    พบข้อความ                        ต่อดอก
    Wait Until Element Is Visible    id:modalInterest               300
    Wait Until Element Is Visible    id:btnClose                    300
    Wait Until Element Is Visible    id:selectMonth                 300
    Wait Until Element Is Visible    id:radio1                      300
    Wait Until Element Is Visible    id:InterestTotal               300
    Wait Until Element Is Visible    id:btnCal                      300
    Wait Until Element Is Visible    id:btnSaveInterest             300