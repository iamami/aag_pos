*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบและinit_data Infocustomer
Test Setup        เข้าหน้าขายฝากหลัก
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data Infocustomer
*** Variables ***

*** Test cases ***
Test55 ทดสอบการยกเลิกการ Import เมื่อกดปุ่มกากบาท
    Sleep                               5
    กดปุ่มmport
    Sleep                               5
    พบpopup_import
    Sleep                               5
    ปิดpopup_import
    Sleep                               5
    พบข้อความ                           รายการขายฝาก

Test56 ทดสอบการยกเลิกการ Import CSV เมื่อกดปุ่มกากบาท
    กดปุ่มmport
    Sleep                               5
    กดimportรายการขายฝาก
    Sleep                               5
    ปิดpopupCSV
    Sleep                               5
    พบpopup_import
    Sleep                               5
    ปิดpopup_import
    Sleep                               5
    พบข้อความ                           รายการขายฝาก
*** Keywords ***
กดปุ่มmport
    Sleep                               1
    Wait Until Page Contains Element    id:btnImport                                                  300
    Sleep                               5
    Click Element                       id:btnImport
    Sleep                               1

ปิดpopup_import
    Sleep                               1
    Wait Until Page Contains Element    id:btnCloseModalImport                                                  300
    Sleep                               5
    Click Element                       id:btnCloseModalImport
    Sleep                               1

กดimportรายการขายฝาก
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="menuImport"]/a[1]                                                  300
    Sleep                               5
    Click Element                       //*[@id="menuImport"]/a[1]
    Sleep                               1

ปิดpopupCSV
    Sleep                               1
    Wait Until Element Is Visible       id:btnCloseCSV                                                  300
    Sleep                               5
    Click Element                       id:btnCloseCSV
    Sleep                               1

พบpopup_import
    Sleep                               5
    พบข้อความ                           Import
    Sleep                               5
    Wait Until Element Is Visible       id:btnCloseModalImport                                                  300