*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบและinit_data Infocustomer
Test Setup        เข้าหน้าขายฝากหลัก
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data Infocustomer
*** Variables ***

*** Test cases ***
Test1 ทดสอบการยกเลิกการสร้างบิลขายฝาก เมื่อกดกากบาท
    Sleep                                  12
    กดปุ่มสร้างรายการขายฝาก
    Sleep                                  10
    พบPopupขายฝาก
    Sleep                                  2
    ปิดPopupขายฝาก
    Sleep                                  1
    พบข้อความ                              ขายฝาก
    Wait Until Page Contains Element       id:btnCreateLease                                                                      300
                #กลับมาหน้าขายฝาก
    ไม่พบข้อความ                           AA00019

Test2 ทดสอบการเคลียร์ข้อมูลในหน้าสร้างบิลขายฝาก
    กดปุ่มสร้างรายการขายฝาก
    Sleep                                  1
    พบPopupขายฝาก
    Sleep                                  1
    กรอกชื่อลูกค้า                         linda
    กดปุ่มสร้างใหม่
    Sleep                                  1
    พบPopupขายฝาก
    ไม่พบข้อความ                           linda
    ไม่พบข้อความ                           1111111111112
    ไม่พบข้อความ                           0802222229
                #ไม่พบข้อมูลบนหน้าสร้างบิลขายฝาก
    ปิดPopupขายฝาก

Test3 ทดสอบการไม่สามารถพิมพ์สัญญาได้ เมื่อยังไม่สร้างบิลขายฝาก
    กดปุ่มสร้างรายการขายฝาก
    Sleep                                  1
    พบPopupขายฝาก
    Sleep                                  1
    ตรวจสอบไม่สามารถกดปุ่มพิมพ์สัญญาได้
                #ไม่สามารถกดปุ่มพิมพ์สัญญาได้
    ปิดPopupขายฝาก

Test4 ทดสอบการสร้างบิลขายฝากไม่สำเร็จ เมื่อไม่กรอกข้อมูลลูกค้า
    กดปุ่มสร้างรายการขายฝาก
    Sleep                                  3
    พบPopupขายฝาก
    Sleep                                  3
    กรอกจำนวนเงิน                          30000
    Sleep                                  3
    กดปุ่มสร้าง
    Sleep                                  7
    ตรวจพบข้อความบน Alert                  กรุณาระบุลูกค้า
                #พบalert กรุณาระบุลูกค้า
    ปิดPopupขายฝาก

# Test5 ทดสอบการสร้างบิลขายฝากไม่สำเร็จ เมื่อไม่กรอกข้อมูลเลขบัตรประชาชน
#                 กดปุ่มสร้างรายการขายฝาก
#                 Sleep                                     1
#                 พบPopupขายฝาก
#                 Sleep                                     1
#                 กดปุ่มสร้างลูกค้า
#                 Wait Until Element Is Visible             //*[@id="AddCustomerModal"]/div[2]/div/form/div[4]/div/div[2]/div/input
#                 Sleep                                     1
#                 Input Text                                //*[@id="AddCustomerModal"]/div[2]/div/form/div[4]/div/div[2]/div/input                               Mai
#                 Sleep                                     1
#                 Wait Until Element Is Visible             //*[@id="AddCustomerModal"]/div[2]/div/form/div[4]/div/div[3]/div/input
#                 Sleep                                     1
#                 Input Text                                //*[@id="AddCustomerModal"]/div[2]/div/form/div[4]/div/div[3]/div/input                               0811111567
#                 Sleep                                     10
#                 Wait Until Element Is Visible             //*[@id="AddConfirm"]
#                 Sleep                                     15
#                 Click Element                             //*[@id="AddConfirm"]
#                 Sleep                                     30
#                 ตรวจพบข้อความบน Alert                     บันทึกข้อมูลสำเร็จ
#                 Sleep                                     2
#                 กรอกจำนวนเงิน                             30000
#                 Sleep                                     2
#                 กดปุ่มสร้าง
#                 Sleep                                     10
#                 ตรวจพบข้อความบน Alert                     กรุณาระบุเลขประจำตัวประชาชน
#                 #พบalert กรุณาระบุเลขประจำตัวประชาชน
#                 ปิดPopupขายฝาก

# Test6 ทดสอบการสร้างบิลขายฝากไม่สำเร็จ เมื่อไม่กรอกข้อมูลเบอร์โทรศัพท์
#                 กดปุ่มสร้างรายการขายฝาก
#                 Sleep                                     1
#                 พบPopupขายฝาก
#                 Sleep                                     1
#                 กดปุ่มสร้างลูกค้า
#                 Wait Until Element Is Visible             //*[@id="AddCustomerModal"]/div[2]/div/form/div[4]/div/div[2]/div/input
#                 Sleep                                     1
#                 Input Text                                //*[@id="AddCustomerModal"]/div[2]/div/form/div[4]/div/div[2]/div/input                               Mai
#                 Sleep                                     1
#                 Wait Until Element Is Visible             //*[@id="AddCustomerModal"]/div[2]/div/form/div[4]/div/div[4]/div/input
#                 Sleep                                     1
#                 Input Text                                //*[@id="AddCustomerModal"]/div[2]/div/form/div[4]/div/div[4]/div/input                               dddd@gggg.kkk
#                 Sleep                                     1
#                 Wait Until Element Is Visible             //*[@id="AddCustomerModal"]/div[2]/div/form/div[4]/div/div[5]/div/input
#                 Sleep                                     1
#                 Input Text                                //*[@id="AddCustomerModal"]/div[2]/div/form/div[4]/div/div[5]/div/input                               1111111555779
#                 Sleep                                     10
#                 Wait Until Element Is Visible             //*[@id="AddConfirm"]
#                 Sleep                                     15
#                 Click Element                             //*[@id="AddConfirm"]
#                 Sleep                                     7
#                 ตรวจพบข้อความบน Alert                     บันทึกข้อมูลสำเร็จ
#                 Sleep                                     2
#                 กรอกจำนวนเงิน                             30000
#                 Sleep                                     2
#                 กดปุ่มสร้าง
#                 Sleep                                     7
#                 ตรวจพบข้อความบน Alert                     กรุณาระบุเบอร์โทร
#                 #พบalert กรุณาระบุเบอร์โทร
#                 ปิดPopupขายฝาก

Test7 ทดสอบการสร้างบิลขายฝากไม่สำเร็จ เมื่อกรอกจำนวนเงิน เป็น 0
    กดปุ่มสร้างรายการขายฝาก
    Sleep                                  5
    พบPopupขายฝาก
    Sleep                                  5
    กรอกชื่อลูกค้า                              linda
    Sleep                                  5
    กรอกจำนวนเงิน                          0
    Sleep                                  5
    กดปุ่มสร้าง
    Sleep                                  7
    ตรวจพบข้อความบน Alert                  กรุณาระบุจำนวนเงิน
                #พบalert กรุณาระบุจำนวนเงิน
    ปิดPopupขายฝาก

Test8 ทดสอบการสร้างบิลขายฝากไม่สำเร็จ เมื่อกรอกระยะเวลา เป็น 0
    กดปุ่มสร้างรายการขายฝาก
    Sleep                                  1
    พบPopupขายฝาก
    Sleep                                  1
    กรอกชื่อลูกค้า                         linda
    Sleep                                  1
    กรอกจำนวนเงิน                          30000
    Sleep                                  2
    Input Text                             //*[@id="inputTime"]                                                                   0
    Sleep                                  2
    กดปุ่มสร้าง
    Sleep                                  7
    ตรวจพบข้อความบน Alert                  กรุณาระบุระยะเวลา
                #พบalert กรุณาระบุเบอร์โทร
    ปิดPopupขายฝาก

# Test9 ทดสอบการสร้างบิลขายฝากไม่สำเร็จ เมื่อไม่กรอกข้อมูลวันนำเข้า
#                 กดปุ่มสร้างรายการขายฝาก
#                 Sleep                                     1
#                 พบPopupขายฝาก
#                 Sleep                                     1
#                 กรอกชื่อลูกค้า                            linda
#                 Sleep                                     1
#                 กรอกจำนวนเงิน                             30000
#                 Sleep                                     2
#                 Clear Element Text                        //*[@id="ModalLeaseForm"]/div[2]/div/div[2]/form/div/div/div[1]/div[7]/div[1]/div/div[1]/div/input
#                 Sleep                                     2
#                 กดปุ่มสร้าง
#                 Sleep                                     1
#                 ตรวจพบข้อความบน Alert                     กรุณาระบุวันนำเข้า
#                 #พบalert กรุณาระบุวันนำเข้า
#                 ปิดPopupขายฝาก

# Test10 ทดสอบการสร้างบิลขายฝากไม่สำเร็จ เมื่อไม่กรอกข้อมูลวันนำเข้า
#                 กดปุ่มสร้างรายการขายฝาก
#                 Sleep                                     1
#                 พบPopupขายฝาก
#                 Sleep                                     1
#                 กรอกชื่อลูกค้า                            linda
#                 Sleep                                     1
#                 กรอกจำนวนเงิน                             30000
#                 Sleep                                     2
#                 Clear Element Text                        //*[@id="ModalLeaseForm"]/div[2]/div/div[2]/form/div/div/div[1]/div[7]/div[2]/div/div[1]/div/input
#                 Sleep                                     2
#                 กดปุ่มสร้าง
#                 Sleep                                     1
#                 ตรวจพบข้อความบน Alert                     กรุณาระบุวันครบกำหนด
#                 #พบalert กรุณาระบุวันครบกำหนด
#                 ปิดPopupขายฝาก

Test11 ทดสอบการสร้างบิลขายฝากไม่สำเร็จ เมื่อไม่มีรายการสินค้า
    กดปุ่มสร้างรายการขายฝาก
    Sleep                                  1
    พบPopupขายฝาก
    Sleep                                  1
    กรอกชื่อลูกค้า                         linda
    Sleep                                  1
    กรอกจำนวนเงิน                          30000
    Sleep                                  2
    กดปุ่มสร้าง
    Sleep                                  5
    ตรวจพบข้อความบน Alert                  กรุณาเพิ่มรายการสินค้า
                #พบalert กรุณาเพิ่มรายการสินค้า
    ปิดPopupขายฝาก

Test12 ทดสอบการเพิ่มรายการสินค้าไม่สำเร็จ เมื่อไม่กรอกข้อมูล% ทอง
    กดปุ่มสร้างรายการขายฝาก
    Sleep                                  1
    พบPopupขายฝาก
    Sleep                                  1
    กดปุ่มเพิ่มรายการสินค้า
    Sleep                                  1
    พบPopupเลือกรายการสินค้า
    Sleep                                  1
    ใส่ชื่อสินค้า
    Sleep                                  1
    ใส่น้ำหนัก
    Sleep                                  1
    ใส่จำนวน
    Sleep                                  1
    กดปุ่มเพิ่มสินค้า
    Sleep                                  5
    ตรวจพบข้อความบน Alert                  กรุณาเลือก %ทอง
                #พบalert กรุณาเลือก %ทอง
    ปิดPopupเลือกรายการสินค้า
    ปิดPopupขายฝาก

Test13 ทดสอบการเพิ่มรายการสินค้าไม่สำเร็จ เมื่อไม่กรอกข้อมูลน้ำหนักชั่งรวม
    กดปุ่มสร้างรายการขายฝาก
    Sleep                                  1
    พบPopupขายฝาก
    Sleep                                  1
    กดปุ่มเพิ่มรายการสินค้า
    Sleep                                  1
    พบPopupเลือกรายการสินค้า
    Sleep                                  1
    เลือก % ทอง
    Sleep                                  1
    ใส่ชื่อสินค้า
    Sleep                                  1
    ใส่จำนวน
    Sleep                                  1
    กดปุ่มเพิ่มสินค้า
    Sleep                                  5
    ตรวจพบข้อความบน Alert                  กรุณากรอกน้ำหนัก
                #พบalert กรุณากรอกน้ำหนัก
    ปิดPopupเลือกรายการสินค้า
    ปิดPopupขายฝาก

Test14 ทดสอบการเพิ่มรายการสินค้าไม่สำเร็จ เมื่อไม่กรอกข้อมูลชื่อสินค้า
    กดปุ่มสร้างรายการขายฝาก
    Sleep                                  1
    พบPopupขายฝาก
    Sleep                                  1
    กดปุ่มเพิ่มรายการสินค้า
    Sleep                                  1
    พบPopupเลือกรายการสินค้า
    Sleep                                  1
    เลือก % ทอง
    Sleep                                  1
    ใส่น้ำหนัก
    Sleep                                  1
    ใส่จำนวน
    Sleep                                  1
    กดปุ่มเพิ่มสินค้า
    Sleep                                  5
    ตรวจพบข้อความบน Alert                  กรุณากรอกชื่อสินค้า
                #พบalert กรุณากรอกชื่อสินค้า
    ปิดPopupเลือกรายการสินค้า
    ปิดPopupขายฝาก

Test15 ทดสอบการเพิ่มรายการสินค้าไม่สำเร็จ เมื่อกรอกจำนวน(ชิ้น) เป็นinvalid input
    กดปุ่มสร้างรายการขายฝาก
    Sleep                                  1
    พบPopupขายฝาก
    Sleep                                  1
    กดปุ่มเพิ่มรายการสินค้า
    Sleep                                  1
    พบPopupเลือกรายการสินค้า
    Sleep                                  1
    เลือก % ทอง
    Sleep                                  1
    ใส่ชื่อสินค้า
    Sleep                                  1
    ใส่น้ำหนัก
    Sleep                                  1
    ใส่จำนวนinvalid
    Sleep                                  1
    กดปุ่มเพิ่มสินค้า
    Sleep                                  5
    ตรวจพบข้อความบน Alert                  กรุณากรอกจำนวน
                #พบalert กรุณากรอกชื่อสินค้า
    ปิดPopupเลือกรายการสินค้า
    ปิดPopupขายฝาก

Test16 ทดสอบการเพิ่มรายการสำเร็จเมื่อกรอกข้อมูลครบ
    กดปุ่มสร้างรายการขายฝาก
    Sleep                                  1
    พบPopupขายฝาก
    Sleep                                  1
    กดปุ่มเพิ่มรายการสินค้า
    Sleep                                  1
    พบPopupเลือกรายการสินค้า
    Sleep                                  1
    เลือก % ทอง
    Sleep                                  1
    ใส่ชื่อสินค้า
    Sleep                                  1
    ใส่จำนวน
    Sleep                                  1
    ใส่น้ำหนัก
    กดปุ่มเพิ่มสินค้า
    Sleep                                  1
    พบPopupขายฝาก
    Sleep                                  1
    #พบPopupขายฝาก
    จำนวนรายการสินค้า                      1
    ปิดPopupขายฝาก

Test17 ทดสอบการสร้างบิลขายฝากสำเร็จ เมื่อมีรายการสินค้า
    กดปุ่มสร้างรายการขายฝาก
    Sleep                                  1
    พบPopupขายฝาก
    Sleep                                  1
    กรอกชื่อลูกค้า                         linda
    Sleep                                  1
    กรอกจำนวนเงิน                          30000
    กดปุ่มเพิ่มรายการสินค้า
    Sleep                                  1
    พบPopupเลือกรายการสินค้า
    Sleep                                  1
    เลือก % ทอง
    Sleep                                  1
    ใส่ชื่อสินค้า
    Sleep                                  1
    ใส่จำนวน
    Sleep                                  1
    ใส่น้ำหนัก
    กดปุ่มเพิ่มสินค้า
    Sleep                                  1
    พบPopupขายฝาก
    Sleep                                  2
    กดปุ่มสร้าง
    Sleep                                  1
    พบPopupจ่ายเงิน
                #พบPopupจ่ายเงิน
    Sleep                                  1
    ปิดPopupจ่ายเงิน
    Sleep                                  1
    ปิดPopupขายฝาก

Test18 ทดสอบการไม่สามารถแก้ไขจำนวนเงินในpopupจ่ายเงินได้
    กดปุ่มสร้างรายการขายฝาก
    Sleep                                  1
    พบPopupขายฝาก
    Sleep                                  1
    กรอกชื่อลูกค้า                         linda
    Sleep                                  1
    กรอกจำนวนเงิน                          30000
    กดปุ่มเพิ่มรายการสินค้า
    Sleep                                  1
    พบPopupเลือกรายการสินค้า
    Sleep                                  1
    เลือก % ทอง
    ใส่ชื่อสินค้า
    ใส่จำนวน
    Sleep                                  1
    ใส่น้ำหนัก
    กดปุ่มเพิ่มสินค้า
    Sleep                                  1
    พบPopupขายฝาก
    Sleep                                  2
    กดปุ่มสร้าง
    Sleep                                  1
    พบPopupจ่ายเงิน
    Sleep                                  1
    ตรวจสอบไม่สามารถแก้ไขจำนวนเงินได้
                #ไม่สามารถแก้ไขข้อมูลได้
    ปิดPopupจ่ายเงิน
    Sleep                                  1
    ปิดPopupขายฝาก

Test19 ทดสอบการจ่ายเงินไม่สำเร็จเมื่อไม่กรอกชื่อพนักงาน
    กดปุ่มสร้างรายการขายฝาก
    Sleep                                  1
    พบPopupขายฝาก
    Sleep                                  1
    กรอกชื่อลูกค้า                         linda
    Sleep                                  1
    กรอกจำนวนเงิน                          30000
    กดปุ่มเพิ่มรายการสินค้า
    Sleep                                  1
    พบPopupเลือกรายการสินค้า
    Sleep                                  1
    เลือก % ทอง
    ใส่ชื่อสินค้า
    ใส่จำนวน
    Sleep                                  1
    ใส่น้ำหนัก
    กดปุ่มเพิ่มสินค้า
    Sleep                                  1
    พบPopupขายฝาก
    Sleep                                  2
    กดปุ่มสร้าง
    Sleep                                  1
    พบPopupจ่ายเงิน
    Sleep                                  1
    กดปุ่มบันทึกการจ่ายเงิน
    Sleep                                  1
    พบข้อความ                              กรุณาเลือกพนักงาน
    #พบข้อความ " กรุณาเลือกพนักงาน"
    ปิดPopupจ่ายเงิน
    Sleep                                  1
    ปิดPopupขายฝาก

Test20 ทดสอบการจ่ายเงินสำเร็จ เมื่อกรอกข้อมูลครบ
    กดปุ่มสร้างรายการขายฝาก
    Sleep                                  1
    พบPopupขายฝาก
    Sleep                                  1
    กรอกชื่อลูกค้า                         linda
    Sleep                                  1
    กรอกจำนวนเงิน                          10000
    กดปุ่มเพิ่มรายการสินค้า
    Sleep                                  1
    พบPopupเลือกรายการสินค้า
    Sleep                                  1
    เลือก % ทอง
    ใส่ชื่อสินค้า
    ใส่จำนวน
    Sleep                                  1
    ใส่น้ำหนัก
    กดปุ่มเพิ่มสินค้า
    Sleep                                  1
    พบPopupขายฝาก
    Sleep                                  2
    กดปุ่มสร้าง
    Sleep                                  1
    พบPopupจ่ายเงิน
    Sleep                                  1
    เลือกพนักงานเพื่อเพิ่ม
    Sleep                                  1
    กดปุ่มบันทึกการจ่ายเงิน
    Sleep                                  5
    ตรวจพบข้อความบน Alert                  บันทึกข้อมูลสำเร็จ
    #พบalert บันทึกข้อมูลสำเร็จ
    Sleep                                  1
    กดปุ่มพิมพ์สัญญา
    Sleep                                  1
    พบPopup Preview Lease
    Sleep                                  1
    พบข้อความ                              Preview
    ตรวจสอบเลขที่บิลขายฝาก                 AA00001
    ตรวจสอบชื่อลูกค้า                      linda
    ตรวจสอบจำนวนเงินที่จ่าย                10,000.00
    กดปุ่มปิดPopup Preview Lease
    Sleep                                  1
    ปิดPopupขายฝาก

*** Keywords ***