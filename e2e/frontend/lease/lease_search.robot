*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบและinit_data Infocustomer
Test Setup        เข้าหน้าขายฝากหลัก
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data Infocustomer
*** Variables ***

*** Test cases ***
Test57 ทดสอบการค้นหารายการขายฝากสำเร็จเมื่อไม่กรอกข้อมูล
    Sleep                                        12
    กดปุ่มค้นหารายการขายฝาก
    Sleep                                        5
    พบข้อความ                                    AA01000
    พบข้อความ                                    AA01001
    พบข้อความ                                    AA01002
    พบข้อความ                                    AA01003
    Sleep                                        5
    จำนวนรายการขายฝากที่ต้องการขายฝาก            4

Test58 ทดสอบการค้นหารายการขายฝากสำเร็จเมื่อกรอกเลขที่ AA01000 เพียงอย่างเดียว
    Sleep                                        12
    กรอกเลขที่บิลขายฝาก                          AA01000
    Sleep                                        5
    กดปุ่มค้นหารายการขายฝาก
    Sleep                                        5
    พบข้อความ                                    AA01000
    ไม่พบข้อความ                                 AA01001
    Sleep                                        5
    จำนวนรายการขายฝากที่ต้องการขายฝาก            1

Test59 ทดสอบการค้นหารายการขายฝากสำเร็จเมื่อกรอกเลขที่ AA01009 เพียงอย่างเดียว
    Sleep                                        12
    กรอกเลขที่บิลขายฝาก                          AA01009
    Sleep                                        5
    กดปุ่มค้นหารายการขายฝาก
    Sleep                                        5
    ไม่พบข้อความ                                 AA01009
    ไม่พบข้อความ                                 AA01002
    Sleep                                        5
    จำนวนรายการขายฝากที่ต้องการขายฝาก            0

# Test60 ทดสอบการค้นหารายการขายฝากสำเร็จเมื่อกรอกชื่อ John เพียงอย่างเดียว
#                 Sleep                                     12
#                 กรอกชื่อเพื่อค้นหาบิลขายฝาก               John
#                 กดปุ่มค้นหารายการขายฝาก
#                 ไม่พบข้อความ                              linda
#                 พบข้อความ                                 AA01002
#                 พบข้อความ                                 AA01001
#                 จำนวนรายการขายฝากที่ต้องการขายฝาก         2

# Test61 ทดสอบการค้นหารายการขายฝากสำเร็จเมื่อกรอกชื่อ lindaa เพียงอย่างเดียว
#                 Sleep                                     12
#                 กรอกชื่อเพื่อค้นหาบิลขายฝาก               lindaa
#                 กดปุ่มค้นหารายการขายฝาก
#                 ไม่พบข้อความ                              AA01002
#                 ไม่พบข้อความ                              AA01001
#                 ไม่พบข้อความ                              AA01000
#                 จำนวนรายการขายฝากที่ต้องการขายฝาก         0

Test62 ทดสอบการค้นหารายการขายฝากสำเร็จเมื่อกรอกเบอร์โทร 0802222222 เพียงอย่างเดียว
    Sleep                                        12
    กรอกเบอร์โทรเพื่อค้นหาบิลขายฝาก              0802222222
    Sleep                                        5
    กดปุ่มค้นหารายการขายฝาก
    Sleep                                        5
    พบข้อความ                                    AA01002
    พบข้อความ                                    AA01001
    ไม่พบข้อความ                                 linda
    Sleep                                        5
    จำนวนรายการขายฝากที่ต้องการขายฝาก            2

Test63 ทดสอบการค้นหารายการขายฝากสำเร็จเมื่อกรอกเบอร์โทร 0802222220 เพียงอย่างเดียว
    Sleep                                        12
    กรอกเบอร์โทรเพื่อค้นหาบิลขายฝาก              0802222220
    Sleep                                        5
    กดปุ่มค้นหารายการขายฝาก
    Sleep                                        5
    ไม่พบข้อความ                                 AA01001
    ไม่พบข้อความ                                 AA01002
    Sleep                                        5
    จำนวนรายการขายฝากที่ต้องการขายฝาก            0

Test64 ทดสอบการค้นหารายการขายฝากสำเร็จเมื่อกรอกเลขประจำตัวประชาชน 1111111111112 เพียงอย่างเดียว
    Sleep                                        12
    กรอกเลขประจำตัวประชาชนเพื่อค้นหาบิลขายฝาก    1111111111112
    Sleep                                        5
    กดปุ่มค้นหารายการขายฝาก
    Sleep                                        5
    พบข้อความ                                    AA01000
    พบข้อความ                                    AA01003
    ไม่พบข้อความ                                 John
    Sleep                                        5
    จำนวนรายการขายฝากที่ต้องการขายฝาก            2

Test65 ทดสอบการค้นหารายการขายฝากสำเร็จเมื่อกรอกเลขประจำตัวประชาชน 1111111111113 เพียงอย่างเดียว
    Sleep                                        12
    กรอกเลขประจำตัวประชาชนเพื่อค้นหาบิลขายฝาก    1111111111113
    Sleep                                        5
    กดปุ่มค้นหารายการขายฝาก
    Sleep                                        5
    ไม่พบข้อความ                                 AA01000
    ไม่พบข้อความ                                 AA01001
    Sleep                                        5
    จำนวนรายการขายฝากที่ต้องการขายฝาก            0

Test66 ทดสอบการค้นหารายการขายฝากสำเร็จเมื่อกรอกวันที่นำเข้า 20/05/2019 เพียงอย่างเดียว
    Sleep                                        12
    กรอกวันที่นำเข้าเพื่อค้นหาบิลขายฝาก          20/05/2019
    Sleep                                        5
    กดปุ่มค้นหารายการขายฝาก
    Sleep                                        5
    พบข้อความ                                    AA01003
    พบข้อความ                                    AA01002
    ไม่พบข้อความ                                 AA01000
    ไม่พบข้อความ                                 18/05/2019
    Sleep                                        5
    จำนวนรายการขายฝากที่ต้องการขายฝาก            2

Test67 ทดสอบการค้นหารายการขายฝากสำเร็จเมื่อกรอกจำนวนเงินตั้งแต่ 10000 เพียงอย่างเดียว
    Sleep                                        12
    กรอกจำนวนเงินตั้งแต่เพื่อค้นหาบิลขายฝาก      10000
    Sleep                                        5
    กดปุ่มค้นหารายการขายฝาก
    Sleep                                        5
    พบข้อความ                                    20,000.00
    พบข้อความ                                    AA01001
    พบข้อความ                                    AA01003
    พบข้อความ                                    AA01002
    ไม่พบข้อความ                                 AA01000
    ไม่พบข้อความ                                 4,000.00
    Sleep                                        5
    จำนวนรายการขายฝากที่ต้องการขายฝาก            3

Test68 ทดสอบการค้นหารายการขายฝากสำเร็จเมื่อกรอกจำนวนเงินตั้งแต่ 1000000 เพียงอย่างเดียว
    Sleep                                        12
    กรอกจำนวนเงินตั้งแต่เพื่อค้นหาบิลขายฝาก      1000000
    Sleep                                        5
    กดปุ่มค้นหารายการขายฝาก
    Sleep                                        5
    ไม่พบข้อความ                                 20,000.00
    ไม่พบข้อความ                                 AA01000
    ไม่พบข้อความ                                 AA01003
    ไม่พบข้อความ                                 4,000.00
    Sleep                                        5
    จำนวนรายการขายฝากที่ต้องการขายฝาก            0

Test68.1 ทดสอบการค้นหารายการขายฝากสำเร็จเมื่อกรอกจำนวนเงินตั้งแต่ 4000 ถึง 15000
    Sleep                                        12
    กรอกจำนวนเงินตั้งแต่เพื่อค้นหาบิลขายฝาก      4000
    Sleep                                        5
    กรอกจำนวนเงินถึงเพื่อค้นหาบิลขายฝาก          15000
    Sleep                                        5
    กดปุ่มค้นหารายการขายฝาก
    Sleep                                        5
    ไม่พบข้อความ                                 20,000.00
    พบข้อความ                                    AA01000
    พบข้อความ                                    AA01002
    ไม่พบข้อความ                                 AA01003
    ไม่พบข้อความ                                 AA01001
    พบข้อความ                                    4,000.00
    Sleep                                        5
    จำนวนรายการขายฝากที่ต้องการขายฝาก            2

Test69 ทดสอบการค้นหารายการขายฝาก เมื่อเลือกรายการที่ยังไม่ไถ่คืน
    Sleep                                        12
    Wait Until Page Contains Element             //*[@id="client_width"]/div[1]/form/div[3]/div[2]/p[4]/div                                                 300
    Sleep                                        5
    Click Element                                //*[@id="client_width"]/div[1]/form/div[3]/div[2]/p[4]/div
    Sleep                                        5
    กดปุ่มค้นหารายการขายฝาก
    Sleep                                        5
    พบข้อความ                                    AA01000
    พบข้อความ                                    AA01001
    ไม่พบข้อความ                                 AA01003
    ไม่พบข้อความ                                 AA01002
    Sleep                                        5
    จำนวนรายการขายฝากที่ต้องการขายฝาก            2

Test70 ทดสอบการค้นหารายการขายฝาก เมื่อเลือกรายการวันนี้
    กดปุ่มสร้างรายการขายฝาก
    Sleep                                        5
    พบPopupขายฝาก
    Sleep                                        5
    กรอกชื่อลูกค้า                               linda
    Sleep                                        5
    กรอกจำนวนเงิน                                10000
    Sleep                                        5
    กดปุ่มเพิ่มรายการสินค้า
    Sleep                                        5
    พบPopupเลือกรายการสินค้า
    Sleep                                        5
    เลือก % ทอง
    Sleep                                        5
    ใส่ชื่อสินค้า
    Sleep                                        5
    ใส่จำนวน
    Sleep                                        5
    ใส่น้ำหนัก
    Sleep                                        5
    กดปุ่มเพิ่มสินค้า
    Sleep                                        5
    พบPopupขายฝาก
    Sleep                                        5
    กดปุ่มสร้าง
    Sleep                                        5
    พบPopupจ่ายเงิน
    Sleep                                        5
    เลือกพนักงานเพื่อเพิ่ม
    Sleep                                        5
    กดปุ่มบันทึกการจ่ายเงิน
    Sleep                                        10
    ตรวจพบข้อความบน Alert                        บันทึกข้อมูลสำเร็จ
    #พบalert บันทึกข้อมูลสำเร็จ
    Sleep                                        1
    ปิดPopupขายฝาก
    Sleep                                        12
    Wait Until Page Contains Element             //*[@id="client_width"]/div[1]/form/div[3]/div[1]/p[1]/div/label                                                  300
    Sleep                                        5
    Click Element                                //*[@id="client_width"]/div[1]/form/div[3]/div[1]/p[1]/div/label
    Sleep                                        5
    กดปุ่มค้นหารายการขายฝาก
    Sleep                                        5
    ไม่พบข้อความ                                 AA01000
    พบข้อความ                                    AA00001
    Sleep                                        5
    จำนวนรายการขายฝากที่ต้องการขายฝาก            1

Test71 ทดสอบการค้นหารายการขายฝาก เมื่อเลือกรายการเดือนนี้
    Sleep                                        12
    Wait Until Page Contains Element             //*[@id="client_width"]/div[1]/form/div[3]/div[1]/p[2]/div                                                  300
    Sleep                                        5
    Click Element                                //*[@id="client_width"]/div[1]/form/div[3]/div[1]/p[2]/div
    Sleep                                        5
    กดปุ่มค้นหารายการขายฝาก
    Sleep                                        5
    ไม่พบข้อความ                                 AA01000
    พบข้อความ                                    AA00001
    Sleep                                        5
    จำนวนรายการขายฝากที่ต้องการขายฝาก            1


Test72 ทดสอบการค้นหารายการขายฝาก เมื่อเลือกรายการทั้งหมด
    Sleep                                        12
    Wait Until Page Contains Element             //*[@id="client_width"]/div[1]/form/div[3]/div[1]/p[3]/div                                                  300
    Sleep                                        5
    Click Element                                //*[@id="client_width"]/div[1]/form/div[3]/div[1]/p[3]/div
    Sleep                                        5
    กดปุ่มค้นหารายการขายฝาก
    Sleep                                        5
    พบข้อความ                                    AA01000
    พบข้อความ                                    AA01001
    พบข้อความ                                    AA01002
    พบข้อความ                                    AA01003
    Sleep                                        5
    จำนวนรายการขายฝากที่ต้องการขายฝาก            5

Test73 ทดสอบการค้นหารายการขายฝาก เมื่อเลือกรายการครบกำหนดวันนี้
    Sleep                                        12
    Wait Until Page Contains Element             //*[@id="client_width"]/div[1]/form/div[3]/div[1]/p[3]/div                                                 300
    Sleep                                        5
    Click Element                                //*[@id="client_width"]/div[1]/form/div[3]/div[1]/p[3]/div
    Sleep                                        5
    กดปุ่มค้นหารายการขายฝาก
    Sleep                                        5
    จำนวนรายการขายฝากที่ต้องการขายฝาก            5

# Test74 ทดสอบการค้นหารายการขายฝาก เมื่อเลือกรายการคัดออกวันนี้
#                 Sleep                                     12
#                 Wait Until Page Contains Element          //*[@id="invoice-box"]/div[1]/form/div[3]/div[6]/div
#                 Click Element                             //*[@id="invoice-box"]/div[1]/form/div[3]/div[6]/div
#                 กดปุ่มค้นหารายการขายฝาก
#                 ไม่พบข้อความ                              AA00008
#                 ไม่พบข้อความ                              AA00015
#                 จำนวนรายการขายฝากที่ต้องการขายฝาก         0

# Test75 ทดสอบการค้นหารายการขายฝาก เมื่อเลือกรายการยกเลิกบิล
#                 Sleep                                     12
#                 Wait Until Page Contains Element          //*[@id="invoice-box"]/div[1]/form/div[3]/div[7]/div
#                 Click Element                             //*[@id="invoice-box"]/div[1]/form/div[3]/div[7]/div
#                 กดปุ่มค้นหารายการขายฝาก
#                 พบข้อความ                                 AA00007
#                 พบข้อความ                                 linda
#                 จำนวนรายการขายฝากที่ต้องการขายฝาก         1

Test76 ทดสอบการค้นหารายการขายฝาก เมื่อเลือกรายการไถ่คืนเดือนนี้
    Sleep                                        12
    Wait Until Page Contains Element             //*[@id="client_width"]/div[1]/form/div[3]/div[1]/p[1]/div                                                   300
    Sleep                                        5
    Click Element                                //*[@id="client_width"]/div[1]/form/div[3]/div[1]/p[1]/div 
    Sleep                                        5
    กดปุ่มค้นหารายการขายฝาก
    Sleep                                        5
    #กดปุ่มรายการวันนี้
    Wait Until Element Is Visible                //*[@id="client_width"]/div[2]/div[1]/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a/i                                                  300
    Sleep                                        5
    Click Element                                //*[@id="client_width"]/div[2]/div[1]/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a/i
    Sleep                                        5
    พบPopupขายฝาก
    Sleep                                        5
    กดปุ่มเพิ่มรายการดอกเบี้ยรับ
    Sleep                                        5
    กดปุ่มไถ่คืนในหน้าPopupต่อดอกไถ่คืน
    Sleep                                        5
    กดปุ่มบันทึกไถ่คืน/ต่อดอก
    Sleep                                        5
    พบPopup ชำระเงิน
    Sleep                                        5
    กรอกจำนวนรับเงินสด                           31800
    Sleep                                        5
    กดปุ่มบันทึกชำระเงิน
    Sleep                                        5
    ตรวจพบข้อความบน Alert                        ยืนยันการชำระเงิน
    Sleep                                        5
    ตรวจพบข้อความบน Alert                        บันทึกข้อมูลสำเร็จ
    Sleep                                        5
    ปิดPopupขายฝาก
    Sleep                                        5
    Wait Until Page Contains Element             //*[@id="client_width"]/div[1]/form/div[3]/div[2]/p[2]/div                                                  300
    Sleep                                        5
    Click Element                                //*[@id="client_width"]/div[1]/form/div[3]/div[2]/p[2]/div
    Sleep                                        5
    กดปุ่มค้นหารายการขายฝาก
    Sleep                                        5
    พบข้อความ                                    AA00001
    พบข้อความ                                    10,000.00

Test77 ทดสอบการค้นหารายการขายฝาก เมื่อเลือกรายการไถ่คืนทั้งหมด
    Sleep                                        12
    Wait Until Page Contains Element             //*[@id="client_width"]/div[1]/form/div[3]/div[2]/p[3]/div                                                  300
    Sleep                                        5
    Click Element                                //*[@id="client_width"]/div[1]/form/div[3]/div[2]/p[3]/div
    Sleep                                        5
    กดปุ่มค้นหารายการขายฝาก
    Sleep                                        5
    พบข้อความ                                    AA00001
    พบข้อความ                                    AA01002
    พบข้อความ                                    AA01003
    Sleep                                        5
    จำนวนรายการขายฝากที่ต้องการขายฝาก            3

# Test78 ทดสอบการค้นหารายการขายฝาก เมื่อเลือกรายการคัดออกทั้งหมด
#                 Sleep                                     12
#                 Wait Until Page Contains Element          //*[@id="invoice-box"]/div[1]/form/div[3]/div[10]/div
#                 Click Element                             //*[@id="invoice-box"]/div[1]/form/div[3]/div[10]/div
#                 กดปุ่มค้นหารายการขายฝาก
#                 พบข้อความ                                 AA00001
#                 พบข้อความ                                 AA00012
#                 จำนวนรายการขายฝากที่ต้องการขายฝาก         2

Test79 ทดสอบการค้นหารายการขายฝาก เมื่อเลือกรายการเกินกำหนดทั้งหมด
    Sleep                                        12
    Wait Until Page Contains Element             //*[@id="client_width"]/div[1]/form/div[3]/div[4]/p[3]/div                                                 300
    Sleep                                        5
    Click Element                                //*[@id="client_width"]/div[1]/form/div[3]/div[4]/p[3]/div
    Sleep                                        5
    กดปุ่มค้นหารายการขายฝาก
    Sleep                                        5
    พบข้อความ                                    AA01000
    ไม่พบข้อความ                                 AA00001
    Sleep                                        5
    จำนวนรายการขายฝากที่ต้องการขายฝาก            1

*** Keywords ***
จำนวนรายการขายฝากที่ต้องการขายฝาก
    [Arguments]                                  ${value}
    ${count} =                                   Get Element Count                                                                                     id:leaseID
    ${count_str} =                               Convert To String                                                                                     ${count}
    Should Be Equal                              ${count_str}                                                                                          ${value}

กรอกเลขที่บิลขายฝาก
    [Arguments]                                  ${value}
    Wait Until Page Contains Element             id:inputLeaseNum                                                  300
    Sleep                               5
    Input Text                                   id:inputLeaseNum                                                                                      ${value}

กรอกชื่อเพื่อค้นหาบิลขายฝาก
    [Arguments]                                  ${name}
    Wait Until Page Contains Element             //*[@id="searchNameCus"]/input                                                  300
    Sleep                                        1
    Input Text                                   //*[@id="searchNameCus"]/input                                                                        ${name}

กรอกเบอร์โทรเพื่อค้นหาบิลขายฝาก
    [Arguments]                                  ${value}
    Wait Until Page Contains Element             id:inputPhone                                                  300
    Sleep                               5
    Input Text                                   id:inputPhone                                                                                         ${value}

กรอกเลขประจำตัวประชาชนเพื่อค้นหาบิลขายฝาก
    [Arguments]                                  ${value}
    Wait Until Page Contains Element             id:inputID                                                  300
    Sleep                               5
    Input Text                                   id:inputID                                                                                            ${value}

กรอกวันที่นำเข้าเพื่อค้นหาบิลขายฝาก
    [Arguments]                                  ${value}
    Wait Until Page Contains Element             //*[@id="client_width"]/div[1]/form/div[2]/div[1]/div/div/div[1]/div[1]/div/input                                                  300
    Sleep                               5
    Input Text                                   //*[@id="client_width"]/div[1]/form/div[2]/div[1]/div/div/div[1]/div[1]/div/input                 ${value}

กรอกจำนวนเงินตั้งแต่เพื่อค้นหาบิลขายฝาก
    [Arguments]                                  ${value}
    Wait Until Page Contains Element             id:inputStartPrice                                                  300
    Sleep                               5
    Input Text                                   id:inputStartPrice                                                                                    ${value}


กรอกจำนวนเงินถึงเพื่อค้นหาบิลขายฝาก
    [Arguments]                                  ${value}
    Wait Until Page Contains Element             id:inputEndPrice                                                  300
    Sleep                               5
    Input Text                                   id:inputEndPrice                                                                                      ${value}

