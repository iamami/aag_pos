*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบและinit_data Infocustomer
Test Setup        เข้าหน้าขายฝากหลัก
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data Infocustomer
*** Variables ***

*** Test cases ***
Test80 ทดสอบการเข้าหน้้าpop up preview เมื่อกดปุ่มพิมพ์
    Sleep                               2
    Wait Until Page Contains Element    //*[@id="client_width"]/div[1]/form/div[3]/div[1]/p[1]/div                                                  300
    Sleep                               5
    Click Element                       //*[@id="client_width"]/div[1]/form/div[3]/div[1]/p[1]/div
    Sleep                               5
    Wait Until Page Contains Element    id:btnSearch                                                  300
    Sleep                               5
    Click Element                       id:btnSearch
    Sleep                               5
    เข้าหน้าพิมพ์
    Sleep                               5
    #เข้าหน้าpreview
    พบข้อความ                           Preview
    พบข้อความ                           รายการขายฝาก
    พบข้อความ                           ลาดกระบัง
    Sleep                               5
    กดปุ่มปิด

Test81 ทดสอบการยกเลิกการพิมพ์ เมื่อกดปุ่มปิด
    เข้าหน้าพิมพ์
    Sleep                               5
    #เข้าหน้าpreview
    พบข้อความ                           Preview
    Sleep                               5
    กดปุ่มปิด
*** Keywords ***
เข้าหน้าพิมพ์
    Sleep                               1
    Wait Until Page Contains Element    //*[@id="print"]                                                  300
    Sleep                               1
    Click Element                       //*[@id="print"]

กดปุ่มปิด
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="btnClosePreview"]                                                  300
    Sleep                               1
    Click Element                       //*[@id="btnClosePreview"]