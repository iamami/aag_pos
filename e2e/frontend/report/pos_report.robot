*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบAdmin_IBและinit_data
Test Setup        เข้าหน้ารายงาน
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data_report 
*** Variables ***

*** Test cases ***
Test1 ทดสอบการแสดงรายงานขายทองรูปพรรณ/ทองแท่ง เมื่อเลือกกลุ่ม ทองรูปพรรณ
    เลือกรายการ                               //*[@id="root-content"]/div/div/div[1]/div/div[1]/div[2]/a[1]
    ไปลิงค์รายงานพร้อม start_date end_date    sell_group                                                       16/10/2019             17/10/2019
    Wait Until Page Contains                  เงือนไขค้นหา                                                     300
    Sleep                                     1
    # สาขา  พูนทรัพย์
    กรอกข้อมูลและตัวเลือก                     1                                                                8
    # กลุ่ม   ทองรูปพรรณ
    กรอกข้อมูลและตัวเลือก                     4                                                                1
    กดพิมพ์
    Sleep                                     1
    ตรวจสาขา                                  สาขา : พูนทรัพย์
    ตรวจเวลา                                    ตั้งแต่ : 16/10/2019 ถึง 17/10/2019
    ตรวจจำนวนรวมทั้งหมด                       4.00
    ตรวจน.น.รวมทั้งหมด                        21.00
    ตรวจน.น.รวม(บาท)ทั้งหมด                     1.38
    ตรวจรวมเป็นเงินทั้งหมด                      31,700.00
    ตรวจกำไรค่าแรงทั้งหมด                       2,184.00
    ตรวจกำไรสุทธิทั้งหมด                        2,184.00
    ปิด Preview

Test1.1 ทดสอบการแสดงรายงานขายทองรูปพรรณ/ทองแท่ง เมื่่อเลือกกลุ่มทองแท่ง
    เลือกรายการ                               //*[@id="root-content"]/div/div/div[1]/div/div[1]/div[2]/a[1]
    ไปลิงค์รายงานพร้อม start_date end_date    sell_group                                                       12/10/2019             12/10/2019
    Wait Until Page Contains                  เงือนไขค้นหา                                                     300
    Sleep                                     1
    # สาขา  ลาดกระบัง
    กรอกข้อมูลและตัวเลือก                     1                                                                1
    # กลุ่ม   ทองแท่ง
    กรอกข้อมูลและตัวเลือก                     4                                                                2
    กดพิมพ์
    Sleep                                     1
    ตรวจสาขา                                  สาขา : ลาดกระบัง
    ตรวจเวลา                                    ตั้งแต่ : 12/10/2019 ถึง 12/10/2019
    ตรวจจำนวนรวมทั้งหมด                       5.00
    ตรวจน.น.รวมทั้งหมด                        76.22
    ตรวจน.น.รวม(บาท)ทั้งหมด                     5.00
    ตรวจรวมเป็นเงินทั้งหมด                      108,500.00
    ตรวจกำไรค่าแรงทั้งหมด                       1,000.00
    ตรวจกำไรสุทธิทั้งหมด                        1,000.00
    ปิด Preview

Test2 ทดสอบการแสดงรายงานการขายทอง เมื่อเลือกประเภทแหวน
    เลือกรายการ                               //*[@id="root-content"]/div/div/div[1]/div/div[1]/div[2]/a[2]
    ไปลิงค์รายงานพร้อม start_date end_date    sell                                                       20/09/2019             20/09/2019
    Wait Until Page Contains                  เงือนไขค้นหา                                                     300
    Sleep                                     1
    # สาขา  บางพลี
    กรอกข้อมูลและตัวเลือก                     1                                                                6
    # ประเภท  แหวน
    กรอกข้อมูลและตัวเลือก                     4                                                                1
    กดพิมพ์
    Sleep                                     1
    ตรวจสาขา                                  สาขา : บางพลี
    ตรวจเวลา                                    ตั้งแต่ : 20/09/2019 ถึง 20/09/2019
    Sleep                                     1
    ตรวจจำนวนรวมทั้งหมด                       2.00
    ตรวจน.น.รวมทั้งหมด                        9.50
    ตรวจน.น.รวม(บาท)ทั้งหมด                     0.63
    ตรวจรวมเป็นเงินทั้งหมด                      14,250.00
    ตรวจกำไรค่าแรงทั้งหมด                       687.00
    ตรวจกำไรสุทธิทั้งหมด                        687.00
    ปิด Preview

Test2.1 ทดสอบการแสดงรายงานการขายทอง เมื่อเลือกประเภททองแท่ง
    เลือกรายการ                               //*[@id="root-content"]/div/div/div[1]/div/div[1]/div[2]/a[2]
    ไปลิงค์รายงานพร้อม start_date end_date    sell                                                       06/10/2019             07/10/2019
    Wait Until Page Contains                  เงือนไขค้นหา                                                     300
    Sleep                                     1
    # สาขา  ปากน้ำ
    กรอกข้อมูลและตัวเลือก                     1                                                                9
    # กลุ่ม   ทองแท่ง
    กรอกข้อมูลและตัวเลือก                     4                                                                12
    กดพิมพ์
    Sleep                                     1
    ตรวจสาขา                                  สาขา : ปากน้ำ
    ตรวจเวลา                                    ตั้งแต่ : 06/10/2019 ถึง 07/10/2019
    Sleep                                     1
    ตรวจจำนวนรวมทั้งหมด                       3.00
    ตรวจน.น.รวมทั้งหมด                        11.43
    ตรวจน.น.รวม(บาท)ทั้งหมด                     0.75
    ตรวจรวมเป็นเงินทั้งหมด                      17,010.00
    ตรวจกำไรค่าแรงทั้งหมด                       735.00
    ตรวจกำไรสุทธิทั้งหมด                        735.00
    ปิด Preview

Test3 ทดสอบการแสดงรายงานขายแยกตามประเภทและน้ำหนัก เมื่อเลือกประเภทสร้อยข้อมือ
    เลือกรายการ     //*[@id="root-content"]/div/div/div[1]/div/div[1]/div[2]/a[3]
    ไปลิงค์รายงานพร้อม start_date end_date          sell_type              01/09/2019          30/09/2019
   Wait Until Page Contains                  เงือนไขค้นหา                                                     300
    Sleep                                     1
    # สาขา  สำโรง
    กรอกข้อมูลและตัวเลือก                     1                                                                5
    # ประเภทสร้อยข้อมือ
    กรอกข้อมูลและตัวเลือก                     4                                                                4
    กดพิมพ์
    Sleep                                     1
    ตรวจสาขา                                  สาขา : สำโรง
    ตรวจเวลา                                    ตั้งแต่ : 01/09/2019 ถึง 30/09/2019
    Sleep                                     1
    ตรวจจำนวนรวมทั้งหมด                          7
    ตรวจน.น.รวม(g)ทั้งหมด                       45.60
    ตรวจน.น.รวม(บาท)ทั้งหมด                     3.00
    ตรวจรับเงินสุทธิทั้งหมด                         69,600.00
    ปิด Preview

Test3.1 ทดสอบการแสดงรายงานขายแยกตามประเภทและน้ำหนัก เมื่อเลือกประเภทจี้
    เลือกรายการ     //*[@id="root-content"]/div/div/div[1]/div/div[1]/div[2]/a[3]
    ไปลิงค์รายงานพร้อม start_date end_date          sell_type              01/10/2019          22/10/2019
   Wait Until Page Contains                  เงือนไขค้นหา                                                     300
    Sleep                                     1
    # สาขา  บางปู
    กรอกข้อมูลและตัวเลือก                     1                                                                10
    # ประเภทสร้อยข้อมือ
    กรอกข้อมูลและตัวเลือก                     4                                                                10
    กดพิมพ์
    Sleep                                     1
    ตรวจสาขา                                  สาขา : บางปู
    ตรวจเวลา                                    ตั้งแต่ : 01/10/2019 ถึง 22/10/2019
    Sleep                                     1
    ตรวจจำนวนรวมทั้งหมด                          1
    ตรวจน.น.รวม(g)ทั้งหมด                       3.80
    ตรวจน.น.รวม(บาท)ทั้งหมด                     0.25
    ตรวจรับเงินสุทธิทั้งหมด                          6,000.00
    ปิด Preview


Test4 ทดสอบการแสดงรายงานการซื้อทองเก่า 
    เลือกรายการ     //*[@id="root-content"]/div/div/div[1]/div/div[1]/div[2]/a[4]
    ไปลิงค์รายงานพร้อม start_date end_date          buy                     21/09/2019          21/09/2019
    Wait Until Page Contains                        เงือนไขค้นหา            300
    Sleep                                     1
    # สาขา  ลาดกระบัง2
    กรอกข้อมูลและตัวเลือก                     1                                                                11
    กดพิมพ์
    Sleep                                     1
    ตรวจสาขา                                  สาขา : ลาดกระบัง2
    ตรวจเวลา                                    ตั้งแต่ : 21/09/2019 ถึง 21/09/2019
    Sleep                                     1
    ตรวจน.น.รวม(g)ทั้งหมด                       66.500
    ตรวจราคาซื้อทั้งหมด                             89,700.00
    ปิด Preview

# Test5 ทดสอบการแสดงรายงานการเปลี่ยนทอง
#     เลือกรายการ     //*[@id="root-content"]/div/div/div[1]/div/div[1]/div[2]/a[5]
#     ไปลิงค์รายงานพร้อม start_date end_date          exc                     30/09/2019          01/10/2019
#     Wait Until Page Contains                        เงือนไขค้นหา            300
#     Sleep                                     1
#     # สาขา  สินสาคร
#     กรอกข้อมูลและตัวเลือก                     1                                                                7
#     กดพิมพ์
#     Sleep                                     1
#     ตรวจสาขา                                  สาขา : สินสาคร
#     ตรวจเวลา                                    ตั้งแต่ : 30/09/2019 ถึง 01/10/2019
#     Sleep                                     1
#     ตรวจรวม น.น.(g)ทองใหม่ทั้งหมด               76.000
#     ตรวจเป็นเงินขายทองใหม่ทั้งหมด                   107,950.00
#     ตรวจรวม น.น.(g)ทองเก่าทั้งหมด               75.20
#     ตรวจเป็นเงินซื้อทองเก่าทั้งหมด                  300,150.00
#     ตรวจค่าเปลี่ยนทองทั้งหมด                        7,800.00
#     ปิด Preview

Test6 ทดสอบการแสดงรายงานการเปลี่ยนทอง
    เลือกรายการ     //*[@id="root-content"]/div/div/div[1]/div/div[1]/div[2]/a[6]
    ไปลิงค์รายงานพร้อม start_date end_date          sell_type_amount        01/09/2019          30/09/2019
    Wait Until Page Contains                        เงือนไขค้นหา            300
    Sleep                                     1
    # สาขา  กิ่งแก้ว
    กรอกข้อมูลและตัวเลือก                     1                                                                2
    กดพิมพ์
    Sleep                                     1
    ตรวจสาขา                                  สาขา : กิ่งแก้ว
    ตรวจเวลา                                    ตั้งแต่ : 01/09/2019 ถึง 30/09/2019
    Sleep                                     1
    ตรวจจำนวนชิ้นรวมทั้งหมด                         38
    ปิด Preview

*** Keywords ***
ตรวจจำนวนรวมทั้งหมด
    [Arguments]                               ${value}
    Wait Until Element Is Visible             id:amount
    ${count} =                                Get Text                                                         //*[@id="amount"]
    ${count_str} =                            Convert To String                                                ${count}
    Should Be Equal                           ${count_str}                                                     ${value}

ตรวจน.น.รวมทั้งหมด
    [Arguments]                               ${value}
    Wait Until Element Is Visible             id:weight
    ${count} =                                Get Text                                                         //*[@id="weight"]
    ${count_str} =                            Convert To String                                                ${count}
    Should Be Equal                           ${count_str}                                                     ${value}

ตรวจน.น.รวม(บาท)ทั้งหมด
    [Arguments]                               ${value}
    Wait Until Element Is Visible             id:weight_b
    ${count} =                                Get Text                                                         //*[@id="weight_b"]
    ${count_str} =                            Convert To String                                                ${count}
    Should Be Equal                           ${count_str}                                                     ${value}

ตรวจรวมเป็นเงินทั้งหมด
    [Arguments]                               ${value}
    Wait Until Element Is Visible             id:sell
    ${count} =                                Get Text                                                         //*[@id="sell"]
    ${count_str} =                            Convert To String                                                ${count}
    Should Be Equal                           ${count_str}                                                     ${value}

ตรวจกำไรค่าแรงทั้งหมด
    [Arguments]                               ${value}
    Wait Until Element Is Visible             id:wage
    ${count} =                                Get Text                                                         //*[@id="wage"]
    ${count_str} =                            Convert To String                                                ${count}
    Should Be Equal                           ${count_str}                                                     ${value}

ตรวจกำไรสุทธิทั้งหมด
    [Arguments]                               ${value}
    Wait Until Element Is Visible             id:net
    ${count} =                                Get Text                                                         //*[@id="net"]
    ${count_str} =                            Convert To String                                                ${count}
    Should Be Equal                           ${count_str}                                                     ${value}

ตรวจสาขา
    [Arguments]                               ${value}
    Wait Until Element Is Visible             //*[@id="branch"]
    ${count} =                                Get Text                                                         //*[@id="branch"]
    ${count_str} =                            Convert To String                                                ${count}
    Should Be Equal                           ${count_str}                                                     ${value}

ตรวจเวลา
    [Arguments]                               ${value}
    Wait Until Element Is Visible             //*[@id="date"]
    ${count} =                                Get Text                                                         //*[@id="date"]
    ${count_str} =                            Convert To String                                                ${count}
    Should Be Equal                           ${count_str}                                                     ${value}

ตรวจน.น.รวม(g)ทั้งหมด
    [Arguments]                               ${value}
    Wait Until Element Is Visible             //*[@id="weight"]
    ${count} =                                Get Text                                                         //*[@id="weight"]
    ${count_str} =                            Convert To String                                                ${count}
    Should Be Equal                           ${count_str}                                                     ${value}

ตรวจรวม น.น.(g)ทองใหม่ทั้งหมด
    [Arguments]                               ${value}
    Wait Until Element Is Visible             //*[@id="weight_sell"]
    ${count} =                                Get Text                                                         //*[@id="weight_sell"]
    ${count_str} =                            Convert To String                                                ${count}
    Should Be Equal                           ${count_str}                                                     ${value}

ตรวจรวม น.น.(g)ทองเก่าทั้งหมด
    [Arguments]                               ${value}
    Wait Until Element Is Visible             //*[@id="weight_buy"]
    ${count} =                                Get Text                                                         //*[@id="weight_buy"]
    ${count_str} =                            Convert To String                                                ${count}
    Should Be Equal                           ${count_str}                                                     ${value}

ตรวจรับเงินสุทธิทั้งหมด
    [Arguments]                               ${value}
    Wait Until Element Is Visible             //*[@id="sell"]
    ${count} =                                Get Text                                                         //*[@id="sell"]
    ${count_str} =                            Convert To String                                                ${count}
    Should Be Equal                           ${count_str}                                                     ${value}

ตรวจราคาซื้อทั้งหมด
    [Arguments]                               ${value}
    Wait Until Element Is Visible             //*[@id="total"]
    ${count} =                                Get Text                                                         //*[@id="total"]
    ${count_str} =                            Convert To String                                                ${count}
    Should Be Equal                           ${count_str}                                                     ${value}

ตรวจเป็นเงินขายทองใหม่ทั้งหมด
    [Arguments]                               ${value}
    Wait Until Element Is Visible             //*[@id="total_sell"]
    ${count} =                                Get Text                                                         //*[@id="total_sell"]
    ${count_str} =                            Convert To String                                                ${count}
    Should Be Equal                           ${count_str}                                                     ${value}

ตรวจเป็นเงินซื้อทองเก่าทั้งหมด
    [Arguments]                               ${value}
    Wait Until Element Is Visible             //*[@id="total_buy"]
    ${count} =                                Get Text                                                         //*[@id="total_buy"]
    ${count_str} =                            Convert To String                                                ${count}
    Should Be Equal                           ${count_str}                                                     ${value}

ตรวจค่าเปลี่ยนทองทั้งหมด
    [Arguments]                               ${value}
    Wait Until Element Is Visible             //*[@id="total_change"]
    ${count} =                                Get Text                                                         //*[@id="total_change"]
    ${count_str} =                            Convert To String                                                ${count}
    Should Be Equal                           ${count_str}                                                     ${value}

ตรวจจำนวนชิ้นรวมทั้งหมด
    [Arguments]                               ${value}
    Wait Until Element Is Visible             //*[@id="sum_total"]
    ${count} =                                Get Text                                                         //*[@id="sum_total"]
    ${count_str} =                            Convert To String                                                ${count}
    Should Be Equal                           ${count_str}                                                     ${value}
