*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบAdmin_IBและinit_data
Test Setup        เข้าหน้ารายงาน
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data_report
*** Variables ***

*** Test cases ***
Test18 ทดสอบการแสดงรายงานยอดขายฝากเมื่อเลือกแสดงทั้งหมด และ เรียงตามวันที่นำเข้า
    เลือกรายการ                               //*[@id="root-content"]/div/div/div[1]/div/div[3]/div[2]/a[1]
    ไปลิงค์รายงานพร้อม start_date end_date    lease                                                                                                   26/09/2019                                   27/09/2019
    Wait Until Page Contains                  เงือนไขค้นหา                                                                                            300
    Sleep                                     1
    กรอกข้อมูลและตัวเลือก                     1                                                                                                       3
    กดพิมพ์
    Sleep                                           3
    เปรียบเทียบข้อความ                        //*[@id="table-to-xls"]/thead/tr[2]/th/center                                                           สาขา : อัญธานี
    เปรียบเทียบข้อความ                        //*[@id="table-to-xls"]/thead/tr[3]/th/center                                                           ตั้งแต่วันที่ : 26/09/2019 ถึง 27/09/2019
                #น้ำหนัก(รวม)=157.700
    เปรียบเทียบข้อความ                        //*[@id="weight"]                                                                                       157.700
                #จำนวนเงิน(รวม)=179,000.00
    เปรียบเทียบข้อความ                        //*[@id="amount"]                                                                                       179,000.00
    ปิด Preview

Test19 ทดสอบการแสดงรายงานยอดขายฝากเมื่อเลือกแสดงเฉพาะรายการที่ไม่ได้คัดออกหรือไม่ได้ไถ่คืน และ เรียงตามเลขที่ใบขายฝาก
    เลือกรายการ                               //*[@id="root-content"]/div/div/div[1]/div/div[3]/div[2]/a[1]
    ไปลิงค์รายงานพร้อม start_date end_date    lease                                                                                                   29/09/2019                                   30/09/2019
    Wait Until Page Contains                  เงือนไขค้นหา                                                                                            300
    Sleep                                     1
    กรอกข้อมูลและตัวเลือก                     1                                                                                                       6
                # แสดงเฉพาะรายการที่ไม่ได้คัดออกหรือไม่ได้ไถ่คืน
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/form/div[7]/label                                                   300
    Click Element                             //*[@id="content-body"]/div/div/div/form/div[7]/label
                # เรียงตามเลขที่ใบขายฝาก
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/form/div[10]/label                                                  300
    Click Element                             //*[@id="content-body"]/div/div/div/form/div[10]/label
    กดพิมพ์
    Sleep                                           3
    เปรียบเทียบข้อความ                        //*[@id="table-to-xls"]/thead/tr[2]/th/center                                                           สาขา : บางพลี
    เปรียบเทียบข้อความ                        //*[@id="table-to-xls"]/thead/tr[3]/th/center                                                           ตั้งแต่วันที่ : 29/09/2019 ถึง 30/09/2019
                #น้ำหนัก(รวม)=86.200
    เปรียบเทียบข้อความ                        //*[@id="weight"]                                                                                       86.200
                #จำนวนเงิน(รวม)=95,500.00
    เปรียบเทียบข้อความ                        //*[@id="amount"]                                                                                       95,500.00
    ปิด Preview

Test20 ทดสอบการแสดงรายงานยอดดอกเบี้ย เมื่อเลือกเรียงลำดับตามเลขที่ขายฝาก
    เลือกรายการ                               //*[@id="root-content"]/div/div/div[1]/div/div[3]/div[2]/a[2]
    ไปลิงค์รายงานพร้อม start_date end_date    lease_interest                                                                                          7/10/2019                                    7/10/2019
    Wait Until Page Contains                  เงือนไขค้นหา                                                                                            300
    Sleep                                     1
    กรอกข้อมูลและตัวเลือก                     1                                                                                                       2
    กดพิมพ์
    Sleep                                           3
    เปรียบเทียบข้อความ                        //*[@id="table-to-xls"]/thead/tr[2]/th/center                                                           สาขา : กิ่งแก้ว
    เปรียบเทียบข้อความ                        //*[@id="table-to-xls"]/thead/tr[3]/th/center                                                           ตั้งแต่วันที่ : 07/10/2019 ถึง 07/10/2019
                #นำ้หนัก(รวม)=60.800
    เปรียบเทียบข้อความ                        //*[@id="weight"]                                                                                       60.800
                #เงินต้น(รวม)=74,000.00
    เปรียบเทียบข้อความ                        //*[@id="amount"]                                                                                       74,000.00
                #ดอกเบี้ยคำนวน(รวม)=1,480.00
    เปรียบเทียบข้อความ                        //*[@id="total"]                                                                                        1,480.00
                #ดอกเบี้ย(รวม)=1,480.000
    เปรียบเทียบข้อความ                        //*[@id="total_receive"]                                                                                1,480.000
    ปิด Preview

Test21 ทดสอบการแสดงรายงานยอดดอกเบี้ย เมื่อเลือกเรียงลำดับตามวันที่,เลขที่ขายฝาก
    เลือกรายการ                               //*[@id="root-content"]/div/div/div[1]/div/div[3]/div[2]/a[2]
    ไปลิงค์รายงานพร้อม start_date end_date    lease_interest                                                                                          15/10/2019                                   17/10/2019
    Wait Until Page Contains                  เงือนไขค้นหา                                                                                            300
    Sleep                                     1
    กรอกข้อมูลและตัวเลือก                     1                                                                                                       11
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/form/div[6]/label                                                   300
    Click Element                             //*[@id="content-body"]/div/div/div/form/div[6]/label
    กดพิมพ์
    Sleep                                           3
    เปรียบเทียบข้อความ                        //*[@id="table-to-xls"]/thead/tr[2]/th/center                                                           สาขา : ลาดกระบัง2
    เปรียบเทียบข้อความ                        //*[@id="table-to-xls"]/thead/tr[3]/th/center                                                           ตั้งแต่วันที่ : 15/10/2019 ถึง 17/10/2019
                #นำ้หนัก(รวม)=573.500
    เปรียบเทียบข้อความ                        //*[@id="weight"]                                                                                       573.500
                #เงินต้น(รวม)=701,500.00
    เปรียบเทียบข้อความ                        //*[@id="amount"]                                                                                       701,500.00
                #ดอกเบี้ยคำนวน(รวม)=16,150.00
    เปรียบเทียบข้อความ                        //*[@id="total"]                                                                                        16,150.00
                #ดอกเบี้ย(รวม)=16,150.000
    เปรียบเทียบข้อความ                        //*[@id="total_receive"]                                                                                16,150.000
    ปิด Preview

Test22 ทดสอบการแสดงรายงานยอดไถ่คืน เมื่อเลือกเรียงลำดับตามเลขที่ขายฝาก
    เลือกรายการ                               //*[@id="root-content"]/div/div/div[1]/div/div[3]/div[2]/a[3]
    ไปลิงค์รายงานพร้อม start_date end_date    lease_redeem                                                                                            09/10/2019                                   10/10/2019
    Wait Until Page Contains                  เงือนไขค้นหา                                                                                            300
    Sleep                                     1
    กรอกข้อมูลและตัวเลือก                     1                                                                                                       9
    กดพิมพ์
    Sleep                                           3
    เปรียบเทียบข้อความ                        //*[@id="table-to-xls"]/thead/tr[2]/th/center                                                           สาขา : ปากน้ำ
    เปรียบเทียบข้อความ                        //*[@id="table-to-xls"]/thead/tr[3]/th/center                                                           ตั้งแต่วันที่ : 09/10/2019 ถึง 10/10/2019
                #นำ้หนัก(รวม)=132.800
    เปรียบเทียบข้อความ                        //*[@id="weight"]                                                                                       132.800
                #เงินต้น(รวม)=152,500.00
    เปรียบเทียบข้อความ                        //*[@id="amount"]                                                                                       152,500.00
                #ดอกเบี้ยคำนวน(รวม)=6,140.00
    เปรียบเทียบข้อความ                        //*[@id="total"]                                                                                        6,140.00
                #ดอกเบี้ย(รวม)=6,210.00
    เปรียบเทียบข้อความ                        //*[@id="total_receive"]                                                                                6,210.00
                #เงินต้น+ดอกเบีย(รวม)=158,710.000
    เปรียบเทียบข้อความ                        //*[@id="total_redeem"]                                                                                 158,710.000
    ปิด Preview

Test23 ทดสอบการแสดงรายงานยอดไถ่คืน เมื่อเลือกเรียงลำดับตามวันที่,เลขที่ขายฝาก
    เลือกรายการ                               //*[@id="root-content"]/div/div/div[1]/div/div[3]/div[2]/a[3]
    ไปลิงค์รายงานพร้อม start_date end_date    lease_redeem                                                                                            24/09/2019                                   25/09/2019
    Wait Until Page Contains                  เงือนไขค้นหา                                                                                            300
    Sleep                                     1
    กรอกข้อมูลและตัวเลือก                     1                                                                                                       8
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/form/div[6]/label                                                   300
    Click Element                             //*[@id="content-body"]/div/div/div/form/div[6]/label
    กดพิมพ์
    Sleep                                           3
    เปรียบเทียบข้อความ                        //*[@id="table-to-xls"]/thead/tr[2]/th/center                                                           สาขา : พูนทรัพย์
    เปรียบเทียบข้อความ                        //*[@id="table-to-xls"]/thead/tr[3]/th/center                                                           ตั้งแต่วันที่ : 24/09/2019 ถึง 25/09/2019
                #นำ้หนัก(รวม)=123.400
    เปรียบเทียบข้อความ                        //*[@id="weight"]                                                                                       123.400
                #เงินต้น(รวม)=116,500.00
    เปรียบเทียบข้อความ                        //*[@id="amount"]                                                                                       116,500.00
                #ดอกเบี้ยคำนวน(รวม)=1,516.00
    เปรียบเทียบข้อความ                        //*[@id="total"]                                                                                        1,516.00
                #ดอกเบี้ย(รวม)=1,650.00
    เปรียบเทียบข้อความ                        //*[@id="total_receive"]                                                                                1,650.00
                #เงินต้น+ดอกเบีย(รวม)=118,150.000
    เปรียบเทียบข้อความ                        //*[@id="total_redeem"]                                                                                 118,150.000
    ปิด Preview

# Test24 ทดสอบการแสดงรายงานยอดคัดออก เมื่อกรอกจำนวนเงิน 1000-5000 แล้วเลือกเรียงลำดับตามเลขที่ขายฝาก
#     เข้าหน้าขายฝากหลัก
#     เข้าหน้าคัดออก
#     เลือกสาขาสินสาคร
#     Input Text                                //*[@id="root-content"]/div/div/div[2]/div/div/div[2]/div/div[1]/form/table/tr[2]/td[2]/div[1]/input    1000
#     Sleep                                     2
#     Input Text                                //*[@id="root-content"]/div/div/div[2]/div/div/div[2]/div/div[1]/form/table/tr[2]/td[2]/div[2]/input    5000
#     Sleep                                     3
#     Wait Until Element Is Visible             //*[@id="root-content"]/div/div/div[2]/div/div/div[2]/div/div[1]/form/button                            300
#     Sleep                                     3
#     Click Element                             //*[@id="root-content"]/div/div/div[2]/div/div/div[2]/div/div[1]/form/button
#     กดปุ่มเลือกรายการคัดออกทั้งหมด
#     Sleep                                     3
#     กดปุ่มคัดออก
#     Sleep                                     10
#     ตรวจพบข้อความบน Alert                     ยืนยันคัดออก
#     Sleep                                     10
#     ตรวจพบข้อความบน Alert                     ต้องการบันทึกประเภทและน้ำหนัก ไปยังสต็อกทองเก่าหรือไม่?
#     Sleep                                     10
#     ตรวจพบข้อความบน Alert                     คัดออกสำเร็จ
#     Sleep                                     3
#     เข้าหน้ารายงาน
#     เลือกรายการ                               //*[@id="root-content"]/div/div/div[1]/div/div[3]/div[2]/a[4]
#     Wait Until Page Contains                  เงือนไขค้นหา                                                                                            300
#     Sleep                                     1
#                 # สาขา                                           กิ่งแก้ว
#     กรอกข้อมูลและตัวเลือก                     1                                                                                                       7
#                 # จำนวนเงิน                                      2000
#     กรอกข้อมูลและจำนวน                        4                                                                                                       3000
#                 # ถึงจำนวนเงิน                                   4000
#     กรอกข้อมูลและจำนวน                        5                                                                                                       5000
#     Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/form/div[6]/label                                                   300
#     Sleep                                     3
#     Click Element                             //*[@id="content-body"]/div/div/div/form/div[6]/label
#     กดพิมพ์
#     เปรียบเทียบข้อความ                        //*[@id="table-to-xls"]/thead/tr[2]/th/center                                                           สาขา : สินสาคร
#                 #น้ำหนัก(รวม) = 1,100.100
#     เปรียบเทียบข้อความ                        //*[@id="weight"]                                                                                       1,100.100
#                 #จำนวนเงิน(รวม) = 1,098,000.00
#     เปรียบเทียบข้อความ                        //*[@id="amount"]                                                                                       1,098,000.00
#                 #ดอกเบี้ยรับ(รวม) = 32,085.00
#     เปรียบเทียบข้อความ                        //*[@id="total_interest"]                                                                               32,085.00
#     ปิด Preview

# Test25 ทดสอบการแสดงรายงานยอดคัดออก เมื่อกรอกจำนวนเงิน 10000-15000 แล้วเลือกเรียงลำดับตามวันที่
#     เข้าหน้าขายฝากหลัก
#     เข้าหน้าคัดออก
#     เลือกสาขาสะแกงาม
#     Input Text                                //*[@id="root-content"]/div/div/div[2]/div/div/div[2]/div/div[1]/form/table/tr[2]/td[2]/div[1]/input    10000
#     Sleep                                     2
#     Input Text                                //*[@id="root-content"]/div/div/div[2]/div/div/div[2]/div/div[1]/form/table/tr[2]/td[2]/div[2]/input    15000
#     Sleep                                     3
#     Wait Until Element Is Visible             //*[@id="root-content"]/div/div/div[2]/div/div/div[2]/div/div[1]/form/button                            300
#     Sleep                                     3
#     Click Element                             //*[@id="root-content"]/div/div/div[2]/div/div/div[2]/div/div[1]/form/button
#     กดปุ่มเลือกรายการคัดออกทั้งหมด
#     Sleep                                     3
#     กดปุ่มคัดออก
#     Sleep                                     10
#     ตรวจพบข้อความบน Alert                     ยืนยันคัดออก
#     Sleep                                     10
#     ตรวจพบข้อความบน Alert                     ต้องการบันทึกประเภทและน้ำหนัก ไปยังสต็อกทองเก่าหรือไม่?
#     Sleep                                     10
#     ตรวจพบข้อความบน Alert                     คัดออกสำเร็จ
#     Sleep                                     3
#     เข้าหน้ารายงาน
#     เลือกรายการ                               //*[@id="root-content"]/div/div/div[1]/div/div[3]/div[2]/a[4]
#     Wait Until Page Contains                  เงือนไขค้นหา                                                                                            300
#     Sleep                                     1
#                 # สาขา                                           กิ่งแก้ว
#     กรอกข้อมูลและตัวเลือก                     1                                                                                                       4
#                 # จำนวนเงิน                                      2000
#     กรอกข้อมูลและจำนวน                        4                                                                                                       30000
#                 # ถึงจำนวนเงิน                                   4000
#     กรอกข้อมูลและจำนวน                        5                                                                                                       15000
#     กดพิมพ์
#     เปรียบเทียบข้อความ                        //*[@id="table-to-xls"]/thead/tr[2]/th/center                                                           สาขา : สะแกงาม
#                 #น้ำหนัก(รวม) = 1,100.100
#     เปรียบเทียบข้อความ                        //*[@id="weight"]                                                                                       370.800
#                 #จำนวนเงิน(รวม) = 1,098,000.00
#     เปรียบเทียบข้อความ                        //*[@id="amount"]                                                                                       344,000.00
#                 #ดอกเบี้ยรับ(รวม) = 32,085.00
#     เปรียบเทียบข้อความ                        //*[@id="total_interest"]                                                                               15,045.00
#     ปิด Preview

Test26 ทดสอบการแสดงรายงานยอดครบกำหนด เมื่อเลือกเรียงลำดับตามเลขที่ขายฝาก
    เลือกรายการ                               //*[@id="root-content"]/div/div/div[1]/div/div[3]/div[2]/a[5]
    ไปลิงค์รายงานพร้อม start_date end_date    lease_due                                                                                               01/08/2019                                   31/08/2019
    Wait Until Page Contains                  เงือนไขค้นหา                                                                                            300
    Sleep                                     1
                # สาขา                                           กิ่งแก้ว
    กรอกข้อมูลและตัวเลือก                     1                                                                                                       10
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/form/div[4]/label                                                   300
    Click Element                             //*[@id="content-body"]/div/div/div/form/div[4]/label
    กดพิมพ์
    Sleep                                           3
    เปรียบเทียบข้อความ                        //*[@id="table-to-xls"]/thead/tr[2]/th/center                                                           สาขา : บางปู
    เปรียบเทียบข้อความ                        //*[@id="table-to-xls"]/thead/tr[3]/th/center                                                           ตั้งแต่วันที่ : 01/08/2019 ถึง 31/08/2019
                #น้ำหนัก(รวม)=223.100
    เปรียบเทียบข้อความ                        //*[@id="weight"]                                                                                       223.100
                #จำนวนเงิน(รวม)=265,500.00
    เปรียบเทียบข้อความ                        //*[@id="amount"]                                                                                       265,500.00
                #ดอกเบี้ยรับ(รวม)=1,510.00
    เปรียบเทียบข้อความ                        //*[@id="total_interest"]                                                                               1,510.00
    ปิด Preview

Test27 ทดสอบการแสดงรายงานยอดครบกำหนด เมื่อเลือกเรียงลำดับตามวันที่
    เลือกรายการ                               //*[@id="root-content"]/div/div/div[1]/div/div[3]/div[2]/a[5]
    ไปลิงค์รายงานพร้อม start_date end_date    lease_due                                                                                               01/09/2019                                   30/09/2019
    Wait Until Page Contains                  เงือนไขค้นหา                                                                                            300
    Sleep                                     1
    กรอกข้อมูลและตัวเลือก                     1                                                                                                       1
    กดพิมพ์
    Sleep                                           3
    เปรียบเทียบข้อความ                        //*[@id="table-to-xls"]/thead/tr[2]/th/center                                                           สาขา : ลาดกระบัง
    เปรียบเทียบข้อความ                        //*[@id="table-to-xls"]/thead/tr[3]/th/center                                                           ตั้งแต่วันที่ : 01/09/2019 ถึง 30/09/2019
                #น้ำหนัก(รวม)=900.500
    เปรียบเทียบข้อความ                        //*[@id="weight"]                                                                                       900.500
                #จำนวนเงิน(รวม)=1,093,500.00
    เปรียบเทียบข้อความ                        //*[@id="amount"]                                                                                       1,093,500.00
                #ดอกเบี้ยรับ(รวม)=24,200.00
    เปรียบเทียบข้อความ                        //*[@id="total_interest"]                                                                               24,200.00
    ปิด Preview

Test28 ทดสอบการแสดงรายงานเกินกำหนด เมื่อเลือกเรียงลำดับตามเลขที่ขายฝาก เมื่อกรอกจำนวนวัน 0 วัน
    เลือกรายการ                               //*[@id="root-content"]/div/div/div[1]/div/div[3]/div[2]/a[6]
    กรอกข้อมูลและตัวเลือก                     1                                                                                                       5
    กรอกข้อมูลและจำนวน                        2                                                                                                       0
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/form/div[3]/label                                                   300
    Click Element                             //*[@id="content-body"]/div/div/div/form/div[3]/label
    กดพิมพ์
    Sleep                                           3
    เปรียบเทียบข้อความ                        //*[@id="table-to-xls"]/thead/tr[2]/th/center                                                           สาขา : สำโรง
                #น้ำหนัก(รวม)=481.900
    เปรียบเทียบข้อความ                        //*[@id="weight"]                                                                                       4,934.100
                #จำนวนเงิน(รวม)=569,000.00
    เปรียบเทียบข้อความ                        //*[@id="amount"]                                                                                       5,696,000.00
    ปิด Preview

Test29 ทดสอบการแสดงรายงานเกินกำหนด เมื่อเลือกเรียงลำดับตามวันที่ เมื่อกรอกจำนวนวัน 0 วัน
    เลือกรายการ                               //*[@id="root-content"]/div/div/div[1]/div/div[3]/div[2]/a[6]
    กรอกข้อมูลและตัวเลือก                     1                                                                                                       3
    กรอกข้อมูลและจำนวน                        2                                                                                                       0
    กดพิมพ์
    Sleep                                           3
    เปรียบเทียบข้อความ                        //*[@id="table-to-xls"]/thead/tr[2]/th/center                                                           สาขา : อัญธานี
                #น้ำหนัก(รวม)=2,102.400
    เปรียบเทียบข้อความ                        //*[@id="weight"]                                                                                       10,475.000
                #จำนวนเงิน(รวม)=2,490,500.00
    เปรียบเทียบข้อความ                        //*[@id="amount"]                                                                                       12,581,000.00
    ปิด Preview

Test30 ทดสอบการแสดงรายงานยอดขายฝาก รายเดือนตามปีที่ระบุ เมื่อเลือกเดือน 12 ปี 2018
    เลือกรายการ                               //*[@id="root-content"]/div/div/div[1]/div/div[3]/div[2]/a[7]
    กรอกข้อมูลและตัวเลือก                     2                                                                                                       12
    กรอกข้อมูลและตัวเลือก                     3                                                                                                       3
    กรอกข้อมูลและตัวเลือก                     4                                                                                                       6
    กดพิมพ์
    Sleep                                           3
    เปรียบเทียบข้อความ                        //*[@id="table-to-xls"]/thead/tr[2]/th/center                                                           สาขา : บางพลี
    เปรียบเทียบข้อความ                        //*[@id="date"]                                                                                         ประจำเดือน : 12 ปี 2018
                #จำนวนเงิน(รวม)=53,500.00
    เปรียบเทียบข้อความ                        //*[@id="amount"]                                                                                       53,500.00
    ปิด Preview

Test31 ทดสอบการแสดงรายงานยอดดอกเบี้ย รายเดือนตามปีที่ระบุ เมื่อเลือกเดือน 5 ปี 2019
    เลือกรายการ                               //*[@id="root-content"]/div/div/div[1]/div/div[3]/div[2]/a[7]
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/form/div[1]/div/div[2]/div/label                                    300
    Click Element                             //*[@id="content-body"]/div/div/div/form/div[1]/div/div[2]/div/label
    กรอกข้อมูลและตัวเลือก                     2                                                                                                       5
    กรอกข้อมูลและตัวเลือก                     3                                                                                                       4
    กรอกข้อมูลและตัวเลือก                     4                                                                                                       2
    กดพิมพ์
    Sleep                                           3
    เปรียบเทียบข้อความ                        //*[@id="table-to-xls"]/thead/tr[2]/th/center                                                           สาขา : กิ่งแก้ว
    เปรียบเทียบข้อความ                        //*[@id="table-to-xls"]/thead/tr[3]/th/center                                                           ประจำเดือน : 5 ปี 2019
                #จำนวนเงิน(รวม)=66,050.00
    เปรียบเทียบข้อความ                        //*[@id="total_interest"]                                                                               66,050.00
    ปิด Preview

Test32 ทดสอบการแสดงรายงานยอดขายฝาก,ดอกเบี้ย,รายเดือน เมื่อกรอกยอดตั้งต้น 1000
    เลือกรายการ                               //*[@id="root-content"]/div/div/div[1]/div/div[3]/div[2]/a[8]
    กรอกข้อมูลและตัวเลือก                     1                                                                                                       9
    กรอกข้อมูลและจำนวน                        2                                                                                                       3000
    กดพิมพ์
    Sleep                                           3
    เปรียบเทียบข้อความ                        //*[@id="table-to-xls"]/thead/tr[2]/th/center                                                           สาขา : ปากน้ำ
                #ยอดเงินขายฝาก(รวม)=6,379,500.00
    เปรียบเทียบข้อความ                        //*[@id="total"]                                                                                        6,379,500.00
                #ยอดดอกเบี้ยรับ (รวม)=303,590.00
    เปรียบเทียบข้อความ                        //*[@id="total_interest"]                                                                               303,590.00
    ปิด Preview

Test33 ทดสอบการแสดงรายงานสรุปขายฝากทั้งหมด เมื่อเลือกสรุปรายวัน
    เลือกรายการ                               //*[@id="root-content"]/div/div/div[1]/div/div[3]/div[2]/a[9]
    ไปลิงค์รายงานพร้อม start_date end_date    lease_all                                                                                               23/09/2019                                   23/09/2019
    เลือกสรุปรายวัน
    Wait Until Page Contains                  สรุปรายวัน                                                                                              300
    Sleep                                     1
                # เลือกสาขา
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[1]/div                                      300
    Click Element                             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[1]/div
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[1]/div/div[2]/div[11]                       300
    Click Element                             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[1]/div/div[2]/div[11]
                # กดพิมพ์
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/button                                          300
    Click Element                             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/button
    Wait Until Page Does Not Contain          Loading                                                                                                 300
    Sleep                                           3
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="amount_1"]                                                                                     12
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_1"]                                                                                      168,000.00
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_interest_1"]                                                                             0.00
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="amount_2"]                                                                                     2
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_2"]                                                                                      13,000.00
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_interest_2"]                                                                             100.00
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="amount_3"]                                                                                     0
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_3"]                                                                                      0.00
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_interest_3"]                                                                             0.00
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="amount_all"]                                                                                   14
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_all"]                                                                                    100.00
    Sleep                                     1
    Wait Until Element Is Visible             //*[@id="btnPrintmodal"]                                                                                300
    Click Element                             //*[@id="btnPrintmodal"]
    Sleep                                     1
    เปรียบเทียบข้อความ                        //*[@id="table-to-xls"]/thead/tr/th/center                                                              สรุปรายงานขายฝากวันที่ :23/09/2019
    เปรียบเทียบข้อความ                        //*[@id="amount_lease"]                                                                                 12
    เปรียบเทียบข้อความ                        //*[@id="total_lease"]                                                                                  168,000.00
    เปรียบเทียบข้อความ                        //*[@id="total_interest_lease"]                                                                         0.00
    เปรียบเทียบข้อความ                        //*[@id="amount_redeem"]                                                                                2
    เปรียบเทียบข้อความ                        //*[@id="total_redeem"]                                                                                 13,000.00
    เปรียบเทียบข้อความ                        //*[@id="total_interest_redeem"]                                                                        100.00
    เปรียบเทียบข้อความ                        //*[@id="amount_eject"]                                                                                 0
    เปรียบเทียบข้อความ                        //*[@id="total_eject"]                                                                                  0.00
    เปรียบเทียบข้อความ                        //*[@id="total_interest_eject"]                                                                         0.00
    เปรียบเทียบข้อความ                        //*[@id="amount_all"]                                                                                   14
    เปรียบเทียบข้อความ                        //*[@id="total_all"]                                                                                    100.00
    ปิด Preview

Test34 ทดสอบการแสดงรายงานสรุปขายฝากทั้งหมด เมื่อเลือกสรุปรายเดือน เมื่อเลือก เดือน 1 ปี 2019
    เลือกรายการ                               //*[@id="root-content"]/div/div/div[1]/div/div[3]/div[2]/a[9]
    เลือกสรุปรายเดือน
    Wait Until Page Contains                  สรุปรายเดือน                                                                                            300
    Sleep                                     1
    # เลือกสาขา
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[1]/div                                      300
    Click Element                             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[1]/div
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[1]/div/div[2]/div[8]                        300
    Click Element                             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[1]/div/div[2]/div[8]

    # เลือกเดือน
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[2]/div/input                                300
    Click Element                             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[2]/div/input
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[2]/div/div[2]/div[1]                        300
    Click Element                             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[2]/div/div[2]/div[1]


    # เลือกปี
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[3]/div/input                                300
    Click Element                             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[3]/div/input
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[3]/div/div[2]/div[4]                        300
    Click Element                             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[3]/div/div[2]/div[4]


    # กดพิมพ์
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/button                                          300
    Click Element                             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/button
    Wait Until Page Does Not Contain          Loading                                                                                                 300
    Sleep                                           3
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="amount_1"]                                                                                     27
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_1"]                                                                                      234,900.00
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_interest_1"]                                                                             35,512.00
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="amount_2"]                                                                                     5
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_2"]                                                                                      68,500.00
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_interest_2"]                                                                             11,580.00
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="amount_3"]                                                                                     0
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_3"]                                                                                      0.00
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_interest_3"]                                                                             0.00
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="amount_all"]                                                                                   32
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_all"]                                                                                    47,092.00
    Sleep                                     1
    Wait Until Element Is Visible             //*[@id="btnPrintmodal"]                                                                                300
    Click Element                             //*[@id="btnPrintmodal"]
    Sleep                                     1
    เปรียบเทียบข้อความ                        //*[@id="amount_lease"]                                                                                 27
    เปรียบเทียบข้อความ                        //*[@id="total_lease"]                                                                                  234,900.00
    เปรียบเทียบข้อความ                        //*[@id="total_interest_lease"]                                                                         35,512.00
    เปรียบเทียบข้อความ                        //*[@id="amount_redeem"]                                                                                5
    เปรียบเทียบข้อความ                        //*[@id="total_redeem"]                                                                                 68,500.00
    เปรียบเทียบข้อความ                        //*[@id="total_interest_redeem"]                                                                        11,580.00
    เปรียบเทียบข้อความ                        //*[@id="amount_eject"]                                                                                 0
    เปรียบเทียบข้อความ                        //*[@id="total_eject"]                                                                                  0.00
    เปรียบเทียบข้อความ                        //*[@id="total_interest_eject"]                                                                         0.00
    เปรียบเทียบข้อความ                        //*[@id="amount_all"]                                                                                   32
    เปรียบเทียบข้อความ                        //*[@id="total_all"]                                                                                    47,092.00
    ปิด Preview

Test35 ทดสอบการแสดงรายงานสรุปขายฝากทั้งหมด เมื่อเลือกสรุปรายปี เมื่อเลือกปี 2018
    เลือกรายการ                               //*[@id="root-content"]/div/div/div[1]/div/div[3]/div[2]/a[9]
    เลือกสรุปรายปี
    Wait Until Page Contains                  สรุปรายปี                                                                                               300
    Sleep                                     1
                # เลือกสาขา
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[1]/div                                      300
    Click Element                             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[1]/div
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[1]/div/div[2]/div[7]                        300
    Click Element                             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[1]/div/div[2]/div[7]


                # เลือกปี
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[2]/div/input                                300
    Click Element                             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[2]/div/input
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[2]/div/div[2]/div[3]                        300
    Click Element                             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[2]/div/div[2]/div[3]


                # กดพิมพ์
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/button                                          300
    Click Element                             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/button
    Wait Until Page Does Not Contain          Loading                                                                                                 300
    Sleep                                           3
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="amount_1"]                                                                                     13
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_1"]                                                                                      244,800.00
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_interest_1"]                                                                             61,148.00
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="amount_2"]                                                                                     1
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_2"]                                                                                      15,000.00
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_interest_2"]                                                                             4,530.00
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="amount_3"]                                                                                     0
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_3"]                                                                                      0.00
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_interest_3"]                                                                             0.00
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="amount_all"]                                                                                   14
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_all"]                                                                                    65,678.00
    Sleep                                     1
    Wait Until Element Is Visible             //*[@id="btnPrintmodal"]                                                                                300
    Click Element                             //*[@id="btnPrintmodal"]
    Sleep                                     1
    เปรียบเทียบข้อความ                        //*[@id="table-to-xls"]/thead/tr/th/center                                                              สรุปรายงานขายฝากปี 2018
    เปรียบเทียบข้อความ                        //*[@id="amount_lease"]                                                                                 13
    เปรียบเทียบข้อความ                        //*[@id="total_lease"]                                                                                  244,800.00
    เปรียบเทียบข้อความ                        //*[@id="total_interest_lease"]                                                                         61,148.00
    เปรียบเทียบข้อความ                        //*[@id="amount_redeem"]                                                                                1
    เปรียบเทียบข้อความ                        //*[@id="total_redeem"]                                                                                 15,000.00
    เปรียบเทียบข้อความ                        //*[@id="total_interest_redeem"]                                                                        4,530.00
    เปรียบเทียบข้อความ                        //*[@id="amount_eject"]                                                                                 0
    เปรียบเทียบข้อความ                        //*[@id="total_eject"]                                                                                  0.00
    เปรียบเทียบข้อความ                        //*[@id="total_interest_eject"]                                                                         0.00
    เปรียบเทียบข้อความ                        //*[@id="amount_all"]                                                                                   14
    เปรียบเทียบข้อความ                        //*[@id="total_all"]                                                                                    65,678.00
    ปิด Preview

Test36 ทดสอบการแสดงรายงานสรุปขายฝากทั้งหมด เมื่อเลือกสรุปตามช่วงวันที่
    เลือกรายการ                               //*[@id="root-content"]/div/div/div[1]/div/div[3]/div[2]/a[9]
    ไปลิงค์รายงานพร้อม start_date end_date    lease_all                                                                                               01/08/2019                                   01/09/2019
    เลือกสรุปตามช่วงวันที่
    Wait Until Page Contains                  สรุปตามช่วงวันที่                                                                                       300
    Sleep                                     1
                # เลือกสาขา
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[1]/div                                      300
    Click Element                             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[1]/div
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[1]/div/div[2]/div[4]                        300
    Click Element                             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[1]/div/div[2]/div[4]
                # กดพิมพ์
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/button                                          300
    Click Element                             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/button
    Wait Until Page Does Not Contain          Loading                                                                                                 300
    Sleep                                           3
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="amount_1"]                                                                                     94
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_1"]                                                                                      1,174,000.00
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_interest_1"]                                                                             13,776.00
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="amount_2"]                                                                                     43
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_2"]                                                                                      670,500.00
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_interest_2"]                                                                             13,666.00
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="amount_3"]                                                                                     0
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_3"]                                                                                      0.00
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_interest_3"]                                                                             0.00
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="amount_all"]                                                                                   137
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_all"]                                                                                    27,442.00
    Sleep                                     1
    Wait Until Element Is Visible             //*[@id="btnPrintmodal"]                                                                                300
    Click Element                             //*[@id="btnPrintmodal"]
    Sleep                                     1
    เปรียบเทียบข้อความ                        //*[@id="amount_lease"]                                                                                 94
    เปรียบเทียบข้อความ                        //*[@id="total_lease"]                                                                                  1,174,000.00
    เปรียบเทียบข้อความ                        //*[@id="total_interest_lease"]                                                                         13,776.00
    เปรียบเทียบข้อความ                        //*[@id="amount_redeem"]                                                                                43
    เปรียบเทียบข้อความ                        //*[@id="total_redeem"]                                                                                 670,500.00
    เปรียบเทียบข้อความ                        //*[@id="total_interest_redeem"]                                                                        13,666.00
    เปรียบเทียบข้อความ                        //*[@id="amount_eject"]                                                                                 0
    เปรียบเทียบข้อความ                        //*[@id="total_eject"]                                                                                  0.00
    เปรียบเทียบข้อความ                        //*[@id="total_interest_eject"]                                                                         0.00
    เปรียบเทียบข้อความ                        //*[@id="amount_all"]                                                                                   137
    เปรียบเทียบข้อความ                        //*[@id="total_all"]                                                                                    27,442.00
    ปิด Preview

Test37 ทดสอบการแสดงรายงานสรุปขายฝากทั้งหมด เมื่อเลือกสรุปทั้งหมด
    เลือกรายการ                               //*[@id="root-content"]/div/div/div[1]/div/div[3]/div[2]/a[9]
    เลือกสรุปทั้งหมด
    Wait Until Page Contains                  สรุปทั้งหมด                                                                                             300
    Sleep                                     1
                # เลือกสาขา
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[1]/div                                      300
    Click Element                             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[1]/div
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[1]/div/div[2]/div[10]                       300
    Click Element                             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/div[1]/div/div[2]/div[10]
                # กดพิมพ์
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/center/button                                   300
    Click Element                             //*[@id="content-body"]/div/div/div/div[1]/div/div/form/center/button
    Wait Until Page Does Not Contain          Loading                                                                                                 10000
    Sleep                                           10
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="amount_1"]                                                                                     10446
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_1"]                                                                                      129,763,299.00
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_interest_1"]                                                                             5,538,252.00
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="amount_2"]                                                                                     3086
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_2"]                                                                                      37,708,340.00
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_interest_2"]                                                                             1,720,120.00
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="amount_3"]                                                                                     0
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_3"]                                                                                      0.00
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_interest_3"]                                                                             0.00
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="amount_all"]                                                                                   13532
    เปรียบเทียบจำนวนก่อนกดพิมพ์               //*[@id="total_all"]                                                                                    7,258,372.00
    Sleep                                     1
    Wait Until Element Is Visible             //*[@id="btnPrintmodal"]                                                                                300
    Click Element                             //*[@id="btnPrintmodal"]
    Sleep                                     1
    เปรียบเทียบข้อความ                        //*[@id="table-to-xls"]/thead/tr/th/center                                                              สรุปรายงานขายฝากทั้งหมด
    เปรียบเทียบข้อความ                        //*[@id="amount_lease"]                                                                                 10446
    เปรียบเทียบข้อความ                        //*[@id="total_lease"]                                                                                  129,763,299.00
    เปรียบเทียบข้อความ                        //*[@id="total_interest_lease"]                                                                         5,538,252.00
    เปรียบเทียบข้อความ                        //*[@id="amount_redeem"]                                                                                3086
    เปรียบเทียบข้อความ                        //*[@id="total_redeem"]                                                                                 37,708,340.00
    เปรียบเทียบข้อความ                        //*[@id="total_interest_redeem"]                                                                        1,720,120.00
    เปรียบเทียบข้อความ                        //*[@id="amount_eject"]                                                                                 0
    เปรียบเทียบข้อความ                        //*[@id="total_eject"]                                                                                  0.00
    เปรียบเทียบข้อความ                        //*[@id="total_interest_eject"]                                                                         0.00
    เปรียบเทียบข้อความ                        //*[@id="amount_all"]                                                                                   13532
    เปรียบเทียบข้อความ                        //*[@id="total_all"]                                                                                    7,258,372.00
    ปิด Preview

Test38 ทดสอบการแสดงรายงานสินค้้าขายฝาก
    เลือกรายการ                               //*[@id="root-content"]/div/div/div[1]/div/div[3]/div[2]/a[10]
    ไปลิงค์รายงานพร้อม start_date end_date    lease_stock                                                                                             21/09/2019                                   21/09/2019
    Wait Until Page Contains                  เงือนไขค้นหา                                                                                            300
    Sleep                                     5
    # สาขา  ลาดกระบัง
    กรอกข้อมูลและตัวเลือก                     1                                                                                                       1
    กดพิมพ์
    Sleep                                           3
    ตรวจสาขา                                  สาขา : ลาดกระบัง
    ตรวจเวลา                                  ตั้งแต่วันที่ : 21/09/2019 ถึง 21/09/2019
    ตรวจน้ำหนักรวม                            111.200
    ปิด Preview

Test39 ทดสอบการแสดงรายงานปริะวัติการเพิ่มลดเงินต้น
    เลือกรายการ                               //*[@id="root-content"]/div/div/div[1]/div/div[3]/div[2]/a[11]
    ไปลิงค์รายงานพร้อม start_date end_date    lease_principle                                                                                         01/10/2019                                   15/10/2019
    Wait Until Page Contains                  เงือนไขค้นหา                                                                                            300
    Sleep                                     5
    # สาขา  สำโรง
    กรอกข้อมูลและตัวเลือก                     1                                                                                                       5
    กดพิมพ์
    Sleep                                     5
    ตรวจสาขา                                  สาขา : สำโรง
    ตรวจเวลา                                  ตั้งแต่วันที่ : 01/10/2019 ถึง 15/10/2019
    ตรวจเพิ่มเงินต้นรวม                       2,498.10
    ตรวจลดเงินต้นรวม                          0.00
    ปิด Preview

Test40 ทดสอบการแสดงรายงานประวัติการต่อดอก
    เลือกรายการ                               //*[@id="root-content"]/div/div/div[1]/div/div[3]/div[2]/a[12]
    ไปลิงค์รายงานพร้อม start_date end_date    lease_history                                                                                           22/09/2019                                   28/09/2019
    Wait Until Page Contains                  เงือนไขค้นหา                                                                                            300
    Sleep                                     5
    # สาขา  อัญธานี
    กรอกข้อมูลและตัวเลือก                     1                                                                                                       3
    กดพิมพ์
    Sleep                                           3
    ตรวจสาขา                                  สาขา : อัญธานี
    # ตรวจเวลา                                  ตั้งแต่วันที่ : 22/09/2019 ถึง 28/09/2019
    ตรวจลำดับที่                              //*[@id="อัญธานี_3"]/tr[1]/td[1]                                                                        4
    ตรวจเลขขายฝาก                             //*[@id="อัญธานี_3"]/tr[1]/td[2]                                                                        TY08304
    ตรวจสาขาขายฝาก                            //*[@id="อัญธานี_3"]/tr[1]/td[3]                                                                        อัญธานี
    ตรวจน้ำหนักขายฝาก                         //*[@id="อัญธานี_3"]/tr[1]/td[5]                                                                        3.800
    ตรวจดอกเบี้ยขายฝาก                        //*[@id="อัญธานี_3"]/tr[1]/td[6]                                                                        800.00
    ตรวจวันที่จ่ายดอกเบี้ยขายฝาก              //*[@id="อัญธานี_3"]/tr[3]/td[2]                                                                        22/09/2019
    ตรวจดอกเบี้ยรับขายฝาก                     //*[@id="อัญธานี_3"]/tr[3]/td[3]                                                                        80.00
    ตรวจวันครบกำหนดขายฝาก                     //*[@id="อัญธานี_3"]/tr[3]/td[4]                                                                        17/11/2019
    ตรวจต่อดอกถึงวันที่                       //*[@id="อัญธานี_3"]/tr[3]/td[5]                                                                        17/12/2018
    ตรวจจำนวนเดือนขายฝาก                      //*[@id="อัญธานี_3"]/tr[3]/td[6]                                                                        1
    ปิด Preview

Test40.1 ทดสอบการแสดงรายงานประวัติการต่อดอก เมื่อระบุเลขที่ขายฝากเป็น PN00917
    เลือกรายการ                               //*[@id="root-content"]/div/div/div[1]/div/div[3]/div[2]/a[12]
    ไปลิงค์รายงานพร้อม start_date end_date    lease_history                                                                                           01/09/2016                                   20/10/2019
    Wait Until Page Contains                  เงือนไขค้นหา                                                                                            300
    Sleep                                     5
    # สาขา  ปากน้ำ
    กรอกข้อมูลและตัวเลือก                     1                                                                                                       9
    กรอกเลขที่ขายฝากPN00917
    กดพิมพ์
    Sleep                                           3
    ตรวจสาขา                                  สาขา : ปากน้ำ
    # ตรวจเวลา                                  ตั้งแต่วันที่ : 01/09/2016 ถึง 20/10/2019
    ตรวจลำดับที่                              //*[@id="ปากน้ำ_0"]/tr[1]/td[1]                                                                         1
    ตรวจเลขขายฝาก                             //*[@id="ปากน้ำ_0"]/tr[1]/td[2]                                                                         PN00917
    ตรวจสาขาขายฝาก                            //*[@id="ปากน้ำ_0"]/tr[1]/td[3]                                                                         ปากน้ำ
    ตรวจน้ำหนักขายฝาก                         //*[@id="ปากน้ำ_0"]/tr[1]/td[5]                                                                         1.900
    ตรวจดอกเบี้ยขายฝาก                        //*[@id="ปากน้ำ_0"]/tr[1]/td[6]                                                                         760.00
    #ต่อดอกครั้งที่ 1
    ตรวจต่อดอกครั้งที่                        //*[@id="ปากน้ำ_0"]/tr[3]/td[1]                                                                         1
    ตรวจวันที่จ่ายดอกเบี้ยขายฝาก              //*[@id="ปากน้ำ_0"]/tr[3]/td[2]                                                                         11/02/2018
    ตรวจดอกเบี้ยรับขายฝาก                     //*[@id="ปากน้ำ_0"]/tr[3]/td[3]                                                                         40.00
    ตรวจวันครบกำหนดขายฝาก                     //*[@id="ปากน้ำ_0"]/tr[3]/td[4]                                                                         16/11/2019
    ตรวจต่อดอกถึงวันที่                       //*[@id="ปากน้ำ_0"]/tr[3]/td[5]                                                                         11/03/2018
    #ต่อดอกครั้งที่ 2
    ตรวจต่อดอกครั้งที่                        //*[@id="ปากน้ำ_0"]/tr[4]/td[1]                                                                         2
    ตรวจวันที่จ่ายดอกเบี้ยขายฝาก              //*[@id="ปากน้ำ_0"]/tr[4]/td[2]                                                                         20/09/2019
    ตรวจดอกเบี้ยรับขายฝาก                     //*[@id="ปากน้ำ_0"]/tr[4]/td[3]                                                                         42.00
    ตรวจวันครบกำหนดขายฝาก                     //*[@id="ปากน้ำ_0"]/tr[4]/td[4]                                                                         16/11/2019
    ตรวจต่อดอกถึงวันที่                       //*[@id="ปากน้ำ_0"]/tr[4]/td[5]                                                                         11/03/2018
    ปิด Preview

*** Keywords ***
เข้าหน้าคัดออก
    Go To                                     http://localhost:3000/lease/out
เลือกสาขาสินสาคร
    Wait Until Element Is Visible             //*[@id="root-content"]/div/form/div/div/h3/div[2]/div
    Click Element                             //*[@id="root-content"]/div/form/div/div/h3/div[2]/div
    Wait Until Element Is Visible             //*[@id="root-content"]/div/form/div/div/h3/div[2]/div/div[2]/div[7]                                    300
    Click Element                             //*[@id="root-content"]/div/form/div/div/h3/div[2]/div/div[2]/div[7]
เลือกสาขาสะแกงาม
    Wait Until Element Is Visible             //*[@id="root-content"]/div/form/div/div/h3/div[2]/div
    Click Element                             //*[@id="root-content"]/div/form/div/div/h3/div[2]/div
    Wait Until Element Is Visible             //*[@id="root-content"]/div/form/div/div/h3/div[2]/div/div[2]/div[4]                                    300
    Click Element                             //*[@id="root-content"]/div/form/div/div/h3/div[2]/div/div[2]/div[4]
กดปุ่มเลือกรายการคัดออกทั้งหมด
    Sleep                                     3
    Wait Until Element Is Visible             //*[@id="root-content"]/div/div/div[2]/div/div/div[2]/div/div[2]/button[1]                              300
    Sleep                                     3
    Click Element                             //*[@id="root-content"]/div/div/div[2]/div/div/div[2]/div/div[2]/button[1]
กดปุ่มคัดออก
    Wait Until Element Is Visible             //*[@id="root-content"]/div/div/div[2]/div/div/right/button                                             300
    Click Element                             //*[@id="root-content"]/div/div/div[2]/div/div/right/button

เปรียบเทียบจำนวนก่อนกดพิมพ์
    [Arguments]                               ${loc}                                                                                                  ${expected}
    Wait Until Element Is Visible             ${loc}                                                                                                  300
    ${Value}=                                 Get Value                                                                                               ${loc}
    Should Be Equal As Strings                ${Value}                                                                                                ${expected}

เลือกสรุปรายวัน
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/div[2]/button[1]                                                    300
    Click Element                             //*[@id="content-body"]/div/div/div/div[2]/button[1]
ตรวจน้ำหนักรวม
    [Arguments]                               ${value}
    Wait Until Element Is Visible             id:weight
    ${count} =                                Get Text                                                                                                //*[@id="weight"]
    ${count_str} =                            Convert To String                                                                                       ${count}
    Should Be Equal                           ${count_str}                                                                                            ${value}

เลือกสรุปรายเดือน
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/div[2]/button[2]                                                    300
    Click Element                             //*[@id="content-body"]/div/div/div/div[2]/button[2]
ตรวจสาขา
    [Arguments]                               ${value}
    Wait Until Element Is Visible             //*[@id="branch"]                                                                                       300
    ${count} =                                Get Text                                                                                                //*[@id="branch"]
    ${count_str} =                            Convert To String                                                                                       ${count}
    Should Be Equal                           ${count_str}                                                                                            ${value}

เลือกสรุปรายปี
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/div[2]/button[3]                                                    300
    Click Element                             //*[@id="content-body"]/div/div/div/div[2]/button[3]

เลือกสรุปตามช่วงวันที่
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/div[2]/button[4]                                                    300
    Click Element                             //*[@id="content-body"]/div/div/div/div[2]/button[4]

เลือกสรุปทั้งหมด
    Wait Until Element Is Visible             //*[@id="content-body"]/div/div/div/div[2]/button[5]                                                    300
    Click Element                             //*[@id="content-body"]/div/div/div/div[2]/button[5]



ตรวจเวลา
    [Arguments]                               ${value}
    Wait Until Element Is Visible             //*[@id="date"]                                                                                         300
    ${count} =                                Get Text                                                                                                //*[@id="date"]
    ${count_str} =                            Convert To String                                                                                       ${count}
    Should Be Equal                           ${count_str}                                                                                            ${value}

ตรวจเพิ่มเงินต้นรวม
    [Arguments]                               ${value}
    Wait Until Element Is Visible             id:add                                                                                                  300
    ${count} =                                Get Text                                                                                                //*[@id="add"]
    ${count_str} =                            Convert To String                                                                                       ${count}
    Should Be Equal                           ${count_str}                                                                                            ${value}

ตรวจลดเงินต้นรวม
    [Arguments]                               ${value}
    Wait Until Element Is Visible             id:del                                                                                                  300
    ${count} =                                Get Text                                                                                                //*[@id="del"]
    ${count_str} =                            Convert To String                                                                                       ${count}
    Should Be Equal                           ${count_str}                                                                                            ${value}

ตรวจลำดับที่
    [Arguments]                               ${id}                                                                                                   ${value}
    Wait Until Element Is Visible             ${id}                                                                                                   300
    ${count} =                                Get Text                                                                                                ${id}
    ${count_str} =                            Convert To String                                                                                       ${count}
    Should Be Equal                           ${count_str}                                                                                            ${value}

ตรวจเลขขายฝาก
    [Arguments]                               ${id}                                                                                                   ${value}
    Wait Until Element Is Visible             ${id}                                                                                                   300
    ${count} =                                Get Text                                                                                                ${id}
    ${count_str} =                            Convert To String                                                                                       ${count}
    Should Be Equal                           ${count_str}                                                                                            ${value}

ตรวจสาขาขายฝาก
    [Arguments]                               ${id}                                                                                                   ${value}
    Wait Until Element Is Visible             ${id}                                                                                                   300
    ${count} =                                Get Text                                                                                                ${id}
    ${count_str} =                            Convert To String                                                                                       ${count}
    Should Be Equal                           ${count_str}                                                                                            ${value}

ตรวจน้ำหนักขายฝาก
    [Arguments]                               ${id}                                                                                                   ${value}
    Wait Until Element Is Visible             ${id}                                                                                                   300
    ${count} =                                Get Text                                                                                                ${id}
    ${count_str} =                            Convert To String                                                                                       ${count}
    Should Be Equal                           ${count_str}                                                                                            ${value}

ตรวจดอกเบี้ยขายฝาก
    [Arguments]                               ${id}                                                                                                   ${value}
    Wait Until Element Is Visible             ${id}                                                                                                   300
    ${count} =                                Get Text                                                                                                ${id}
    ${count_str} =                            Convert To String                                                                                       ${count}
    Should Be Equal                           ${count_str}                                                                                            ${value}

ตรวจวันที่จ่ายดอกเบี้ยขายฝาก
    [Arguments]                               ${id}                                                                                                   ${value}
    Wait Until Element Is Visible             ${id}                                                                                                   300
    ${count} =                                Get Text                                                                                                ${id}
    ${count_str} =                            Convert To String                                                                                       ${count}
    Should Be Equal                           ${count_str}                                                                                            ${value}

ตรวจดอกเบี้ยรับขายฝาก
    [Arguments]                               ${id}                                                                                                   ${value}
    Wait Until Element Is Visible             ${id}                                                                                                   300
    ${count} =                                Get Text                                                                                                ${id}
    ${count_str} =                            Convert To String                                                                                       ${count}
    Should Be Equal                           ${count_str}                                                                                            ${value}

ตรวจวันครบกำหนดขายฝาก
    [Arguments]                               ${id}                                                                                                   ${value}
    Wait Until Element Is Visible             ${id}                                                                                                   300
    ${count} =                                Get Text                                                                                                ${id}
    ${count_str} =                            Convert To String                                                                                       ${count}
    Should Be Equal                           ${count_str}                                                                                            ${value}

ตรวจต่อดอกถึงวันที่
    [Arguments]                               ${id}                                                                                                   ${value}
    Wait Until Element Is Visible             ${id}                                                                                                   300
    ${count} =                                Get Text                                                                                                ${id}
    ${count_str} =                            Convert To String                                                                                       ${count}
    Should Be Equal                           ${count_str}                                                                                            ${value}

ตรวจจำนวนเดือนขายฝาก
    [Arguments]                               ${id}                                                                                                   ${value}
    Wait Until Element Is Visible             ${id}                                                                                                   300
    ${count} =                                Get Text                                                                                                ${id}
    ${count_str} =                            Convert To String                                                                                       ${count}
    Should Be Equal                           ${count_str}                                                                                            ${value}

ตรวจต่อดอกครั้งที่
    [Arguments]                               ${id}                                                                                                   ${value}
    Wait Until Element Is Visible             ${id}                                                                                                   300
    ${count} =                                Get Text                                                                                                ${id}
    ${count_str} =                            Convert To String                                                                                       ${count}
    Should Be Equal                           ${count_str}                                                                                            ${value}

กรอกเลขที่ขายฝากPN00917
    Wait Until Element Is Visible             //*[@id="searchLease"]                                                                                  300
    Click Element                             //*[@id="searchLease"]
    Sleep                                     5
    Wait Until Element Is Visible             //*[@id="searchLease"]/div[2]/div                                                                       300
    Click Element                             //*[@id="searchLease"]/div[2]/div
