*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบAdmin_IBและinit_data                    
Test Setup        เข้าหน้ารายงาน
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data_report          
*** Variables ***

*** Test cases ***
Test41 ทดสอบการแสดงรายงานสรุุปประจำวัน (รายจ่าย - รายรับ)	
    เลือกรายการ     //*[@id="root-content"]/div/div/div[1]/div/div[4]/div[2]/a[1]
    
    ไปลิงค์รายงานพร้อม start_date end_date          daily                   15/10/2019          15/10/2019
    Wait Until Page Contains                        เงือนไขค้นหา            300
    Sleep                                           3
    # สาขา  บางพลี
    Wait Until Element Is Visible           //*[@id="content-body"]/div/div/div/form[1]/div/div[1]/div
    Sleep                                           3
    Click Element                           //*[@id="content-body"]/div/div/div/form[1]/div/div[1]/div
    Sleep                                           3
    Wait Until Element Is Visible           //*[@id="content-body"]/div/div/div/form[1]/div/div[1]/div/div[2]/div[6]
    Sleep                                           3
    Click Element                           //*[@id="content-body"]/div/div/div/form[1]/div/div[1]/div/div[2]/div[6]
    Sleep                                           3
    # กดค้นหา
    Wait Until Element Is Visible           //*[@id="content-body"]/div/div/div/form[1]/div/div[4]/button
    Sleep                                           3
    Click Element                           //*[@id="content-body"]/div/div/div/form[1]/div/div[4]/button
    Sleep                                           3
    Wait Until Page Does Not Contain        Loading                     300

    Sleep                                           3
    # ยอดจ่ายเงิน 3,300.00
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/table/tbody/tr[2]/td[2]/div/input       3,300.00
    # ยอดเงินสด ณ ต้นวัน 374,564.00
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/table/tbody/tr[2]/td[4]/div/input       374,564.00
    # ยอดรับเงิน เงินสด 201,300.00
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/table/tbody/tr[3]/td[3]/div/input       201,300.00
    # ยอดรับเงิน เครดิต 0.00
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/table/tbody/tr[3]/td[4]/div/input       0.00

    # พิมพ์
    Wait Until Element Is Visible           //*[@id="content-body"]/div/div/div/form[2]/div/div/div[2]/button[1]                          300
    Sleep                                           3
    Click Element                           //*[@id="content-body"]/div/div/div/form[2]/div/div/div[2]/button[1]
    Wait Until Page Does Not Contain        Loading                     300

    Sleep                                           3
    # ยอดจ่ายเงิน 3,300.00
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[1]/tr[2]/td[2]       3,300.00
    # ยอดเงินสด ณ ต้นวัน 374,564.00
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[1]/tr[2]/td[4]       374,564.00
    # ยอดรับเงิน เงินสด 201,300.00
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[1]/tr[3]/td[3]       201,300.00
    # ยอดรับเงิน เครดิต 0.00
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[1]/tr[3]/td[4]       0.00

    ปิด Preview

Test41.1 ทดสอบการแสดงรายงานสรุุปประจำวัน (ขายฝาก)	
    เลือกรายการ     //*[@id="root-content"]/div/div/div[1]/div/div[4]/div[2]/a[1]
    
    ไปลิงค์รายงานพร้อม start_date end_date          daily                   15/10/2019          15/10/2019
    Wait Until Page Contains                        เงือนไขค้นหา            300
    Sleep                                           1
    # สาขา  บางพลี
    Wait Until Element Is Visible           //*[@id="content-body"]/div/div/div/form[1]/div/div[1]/div
    Click Element                           //*[@id="content-body"]/div/div/div/form[1]/div/div[1]/div
    Wait Until Element Is Visible           //*[@id="content-body"]/div/div/div/form[1]/div/div[1]/div/div[2]/div[6]
    Click Element                           //*[@id="content-body"]/div/div/div/form[1]/div/div[1]/div/div[2]/div[6]
    # กดค้นหา
    Wait Until Element Is Visible           //*[@id="content-body"]/div/div/div/form[1]/div/div[4]/button
    Click Element                           //*[@id="content-body"]/div/div/div/form[1]/div/div[4]/button
    Wait Until Page Does Not Contain        Loading                     300
    Sleep                           1
    # ยอดขายฝาก = 66,000.00
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/table/tbody/tr[5]/td[2]/div/input           66,000.00
    # จ.น. = 9
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/table/tbody/tr[6]/td[1]/div/div/div/input   9
    # น.น. รวม =  123.50
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/table/tbody/tr[6]/td[2]/div/input           123.50
    # จำนวนรายการขายฝาก = 9
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/table/tbody/tr[7]/td[2]/div/input           9
    # ยอดขายฝากคงเหลือ = 66,000.00
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/table/tbody/tr[8]/td[2]/div/input           66,000.00
    # เพิ่มเงินต้น = 0.00
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/table/tbody/tr[9]/td[2]/div/input           0.00
    # ดอกเบี้ยต่อดอก(เงินสด) = 450.00
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/table/tbody/tr[5]/td[4]/div/input           450.00
    # ดอกเบี้ยต่อดอก(เครดิต) = 0.00
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/table/tbody/tr[5]/td[5]/div/input           0.00
    # ดอกเบี้ยต่อดอก(เช็ค) = 0.00
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/table/tbody/tr[5]/td[6]/div/input           0.00
    # เงินต้น+ดอกเบียไถ่คืน(เงินสด) = 9,710.00
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/table/tbody/tr[6]/td[4]/div/input           9,710.00
    # เงินต้น+ดอกเบียไถ่คืน(เครดิต) = 0.00
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/table/tbody/tr[6]/td[5]/div/input           0.00
    # เงินต้น+ดอกเบียไถ่คืน(เช็ค) = 0.00
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/table/tbody/tr[6]/td[6]/div/input           0.00
    # ดอกเบี้ยไถ่คืน(เงินสด) = 210.00
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/table/tbody/tr[7]/td[4]/div/input           210.00
    # ยอดไถ่คืน(เงินต้น)(เงินสด) = 9,500.00
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/table/tbody/tr[8]/td[4]/div/input           9,500.00
    # ลดเงินต้น(เงินสด) = 0.00
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/table/tbody/tr[9]/td[4]/div/input           0.00

    # พิมพ์
    Wait Until Element Is Visible           //*[@id="content-body"]/div/div/div/form[2]/div/div/div[2]/button[1]                          300
    Sleep                                           3
    Click Element                           //*[@id="content-body"]/div/div/div/form[2]/div/div/div[2]/button[1]
    Wait Until Page Does Not Contain        Loading                     300

    Sleep                                           3
    # ยอดขายฝาก = 66,000.00
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[1]/tr[5]/td[2]           66,000.00
    # จ.น. = 9
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[1]/tr[6]/td[1]           จ.น.9 น.น.รวม
    # น.น. รวม =  123.50
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[1]/tr[6]/td[2]           123.50 กรัม	
    # จำนวนรายการขายฝาก = 9
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[1]/tr[7]/td[2]           9
    # ยอดขายฝากคงเหลือ = 66,000.00
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[1]/tr[8]/td[2]           66,000.00
    # เพิ่มเงินต้น = 0.00
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[1]/tr[9]/td[2]           0.00
    # ดอกเบี้ยต่อดอก(เงินสด) = 450.00
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[1]/tr[5]/td[4]           450.00
    # ดอกเบี้ยต่อดอก(เครดิต) = 0.00
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[1]/tr[5]/td[5]           0.00
    # ดอกเบี้ยต่อดอก(เช็ค) = 0.00
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[1]/tr[5]/td[6]           0.00
    # เงินต้น+ดอกเบียไถ่คืน(เงินสด) = 9,710.00
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[1]/tr[6]/td[4]           9,710.00
    # เงินต้น+ดอกเบียไถ่คืน(เครดิต) = 0.00
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[1]/tr[6]/td[5]           0.00
    # เงินต้น+ดอกเบียไถ่คืน(เช็ค) = 0.00
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[1]/tr[6]/td[6]           0.00
    # ดอกเบี้ยไถ่คืน(เงินสด) = 210.00
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[1]/tr[7]/td[4]           210.00
    # ยอดไถ่คืน(เงินต้น)(เงินสด) = 9,500.00
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[1]/tr[8]/td[4]           9,500.00
    # ลดเงินต้น(เงินสด) = 0.00
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[1]/tr[9]/td[4]           0.00

    ปิด Preview

Test41.2 ทดสอบการแสดงรายงานสรุุปประจำวัน (ซื้อ/ขายทอง)		
    เลือกรายการ     //*[@id="root-content"]/div/div/div[1]/div/div[4]/div[2]/a[1]
    
    ไปลิงค์รายงานพร้อม start_date end_date          daily                   15/10/2019          15/10/2019
    Wait Until Page Contains                        เงือนไขค้นหา            300
    Sleep                                           1
    # สาขา  บางพลี
    Wait Until Element Is Visible           //*[@id="content-body"]/div/div/div/form[1]/div/div[1]/div
    Click Element                           //*[@id="content-body"]/div/div/div/form[1]/div/div[1]/div
    Wait Until Element Is Visible           //*[@id="content-body"]/div/div/div/form[1]/div/div[1]/div/div[2]/div[6]
    Click Element                           //*[@id="content-body"]/div/div/div/form[1]/div/div[1]/div/div[2]/div[6]
    # กดค้นหา
    Wait Until Element Is Visible           //*[@id="content-body"]/div/div/div/form[1]/div/div[4]/button
    Click Element                           //*[@id="content-body"]/div/div/div/form[1]/div/div[4]/button
    Wait Until Page Does Not Contain        Loading                     300
    Sleep                           5
    # ยอดซื้อ = 10,200.00
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/table/tbody/tr[11]/td[2]/div/input          10,200.00
    # ราคาเฉลี่ย/ บาท = 20,400.00
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/table/tbody/tr[12]/td[2]/div/input          20,400.00
    # ยอดขาย(เงินสด)  = 21,950.00
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/table/tbody/tr[11]/td[4]/div/input          21,950.00
    # ยอดขาย(เครดิต)  = 0.00
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/table/tbody/tr[11]/td[5]/div/input          0.00
    # ยอดขาย(เช็ค) = 0.00
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/table/tbody/tr[11]/td[6]/div/input          0.00       
    # ราคาขายเฉลี่ย/บาท(เงินสด) = 21,950.00
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/table/tbody/tr[12]/td[4]/div/input          21,950.00

    Sleep                                           3
    # พิมพ์
    Wait Until Element Is Visible           //*[@id="content-body"]/div/div/div/form[2]/div/div/div[2]/button[1]                          300
    Sleep                                           3
    Click Element                           //*[@id="content-body"]/div/div/div/form[2]/div/div/div[2]/button[1]
    Wait Until Page Does Not Contain        Loading                     300

    Sleep                                           3
    # ยอดซื้อ = 10,200.00
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[1]/tr[11]/td[2]          10,200.00
    # ราคาเฉลี่ย/ บาท = 20,400.00
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[1]/tr[12]/td[2]          20,400.00
    # ยอดขาย(เงินสด)  = 21,950.00
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[1]/tr[11]/td[4]          21,950.00
    # ยอดขาย(เครดิต)  = 0.00
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[1]/tr[11]/td[5]          0.00
    # ยอดขาย(เช็ค) = 0.00
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[1]/tr[11]/td[6]          0.00       
    # ราคาขายเฉลี่ย/บาท(เงินสด) = 21,950.00
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[1]/tr[12]/td[4]          21,950.00
    ปิด Preview

Test41.3 ทดสอบการแสดงรายงานสรุุปประจำวัน (รายจ่ายรวม-รายรับรวม)
    เลือกรายการ     //*[@id="root-content"]/div/div/div[1]/div/div[4]/div[2]/a[1]
    
    ไปลิงค์รายงานพร้อม start_date end_date          daily                   15/10/2019          15/10/2019
    Wait Until Page Contains                        เงือนไขค้นหา            300
    Sleep                                           1
    # สาขา  บางพลี
    Wait Until Element Is Visible           //*[@id="content-body"]/div/div/div/form[1]/div/div[1]/div
    Click Element                           //*[@id="content-body"]/div/div/div/form[1]/div/div[1]/div
    Wait Until Element Is Visible           //*[@id="content-body"]/div/div/div/form[1]/div/div[1]/div/div[2]/div[6]
    Click Element                           //*[@id="content-body"]/div/div/div/form[1]/div/div[1]/div/div[2]/div[6]
    # กดค้นหา
    Wait Until Element Is Visible           //*[@id="content-body"]/div/div/div/form[1]/div/div[4]/button
    Click Element                           //*[@id="content-body"]/div/div/div/form[1]/div/div[4]/button
    Wait Until Page Does Not Contain        Loading                     300
    Sleep                           1
    # รายจ่ายรวม 79,500.00  
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/table/tbody/tr[14]/td[2]/div/input          79,500.00
    # รายรับรวม เงินสด 607,974.00
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/table/tbody/tr[14]/td[4]/div/input          607,974.00
    # รายรับรวม เครดิต 0.00
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/table/tbody/tr[14]/td[5]/div/input          0.00
    # รายรับรวม เช็ค 0.00
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/table/tbody/tr[14]/td[6]/div/input          0.00

    # พิมพ์
    Wait Until Element Is Visible           //*[@id="content-body"]/div/div/div/form[2]/div/div/div[2]/button[1]                          300
    Sleep                                           3
    Click Element                           //*[@id="content-body"]/div/div/div/form[2]/div/div/div[2]/button[1]
    Wait Until Page Does Not Contain        Loading                     300

    Sleep                                           3
    # รายจ่ายรวม 79,500.00  
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[1]/tr[14]/td[2]          79,500.00
    # รายรับรวม เงินสด 607,974.00
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[1]/tr[14]/td[4]          607,974.00
    # รายรับรวม เครดิต 0.00
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[1]/tr[14]/td[5]          0.00
    # รายรับรวม เช็ค 0.00
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[1]/tr[14]/td[6]          0.00

    ปิด Preview

Test41.4 ทดสอบการแสดงรายงานสรุุปประจำวัน (รวมรับ-ยอดคงเหลือสุทธิ-รวมยอดเงินสด-รวมยอดเครดิต-รวมยอดเช็ค)	
    เลือกรายการ     //*[@id="root-content"]/div/div/div[1]/div/div[4]/div[2]/a[1]
    
    ไปลิงค์รายงานพร้อม start_date end_date          daily                   15/10/2019          15/10/2019
    Wait Until Page Contains                        เงือนไขค้นหา            300
    Sleep                                           1
    # สาขา  บางพลี
    Wait Until Element Is Visible           //*[@id="content-body"]/div/div/div/form[1]/div/div[1]/div
    Click Element                           //*[@id="content-body"]/div/div/div/form[1]/div/div[1]/div
    Wait Until Element Is Visible           //*[@id="content-body"]/div/div/div/form[1]/div/div[1]/div/div[2]/div[6]
    Click Element                           //*[@id="content-body"]/div/div/div/form[1]/div/div[1]/div/div[2]/div[6]
    # กดค้นหา
    Wait Until Element Is Visible           //*[@id="content-body"]/div/div/div/form[1]/div/div[4]/button
    Click Element                           //*[@id="content-body"]/div/div/div/form[1]/div/div[4]/button
    Wait Until Page Does Not Contain        Loading                     300
    Sleep                           1

    Sleep                                           3
    # รวมรับ 607,974.00
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/div/div/div[2]/table/tbody/tr[1]/td[2]/div/input        607,974.00
    # ยอดคงเหลือสุทธิ 528,474.00
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/div/div/div[2]/table/tbody/tr[2]/td[2]/div/input        528,474.00
    # รวมยอดเงินสด 528,474.00
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/div/div/div[2]/table/tbody/tr[3]/td[2]/div/input        528,474.00
    # รวมยอดเครดิต 0.00
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/div/div/div[2]/table/tbody/tr[4]/td[2]/div/input        0.00
    # รวมยอดเช็ค 0.00
    เปรียบเทียบข้อความ input field      //*[@id="content-body"]/div/div/div/form[2]/div/div/div[2]/table/tbody/tr[4]/td[2]/div/input        0.00

    # พิมพ์
    Wait Until Element Is Visible           //*[@id="content-body"]/div/div/div/form[2]/div/div/div[2]/button[1]                          300
    Sleep                                           3
    Click Element                           //*[@id="content-body"]/div/div/div/form[2]/div/div/div[2]/button[1]
    Wait Until Page Does Not Contain        Loading                     300

    Sleep                                           3
    # รวมรับ 607,974.00
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[3]/tr[1]/td[2]        607,974.00
    # ยอดคงเหลือสุทธิ 528,474.00
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[3]/tr[2]/td[2]        528,474.00
    # รวมยอดเงินสด 528,474.00
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[3]/tr[3]/td[2]        528,474.00
    # รวมยอดเครดิต 0.00
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[3]/tr[4]/td[2]        0.00
    # รวมยอดเช็ค 0.00
    เปรียบเทียบข้อความ      //*[@id="table-to-xls"]/tbody[3]/tr[5]/td[2]        0.00

    ปิด Preview

Test42 ทดสอบการแสดงรายงานรายรับ-รายจ่ายประจำวัน		
    เลือกรายการ     //*[@id="root-content"]/div/div/div[1]/div/div[4]/div[2]/a[2]
    
    ไปลิงค์รายงานพร้อม start_date                   ledger_daily            10/10/2019
    Wait Until Page Contains                        เงือนไขค้นหา            300
    Sleep                                           1
    # สาขา  กิ่งแก้ว
    กรอกข้อมูลและตัวเลือก                       1                       2
    กดพิมพ์

    Sleep                                           3
    # สาขา : กิ่งแก้ว
    เปรียบเทียบข้อความ                          //*[@id="branch"]               สาขา : กิ่งแก้ว
    # วันที่ : 10/10/2019
    เปรียบเทียบข้อความ                          //*[@id="date"]                 วันที่ : 10/10/2019
    # รายรับ
    # ทอง 96.5% คอ(รวม) = 1
    เปรียบเทียบข้อความ                          //*[@id="n_neck"]               1
    # ทอง 96.5% มือ(รวม) = 0 
    เปรียบเทียบข้อความ                          //*[@id="n_nl"]                 0
    # ทอง 96.5% แหวน(รวม) = 0
    เปรียบเทียบข้อความ                          //*[@id="n_r"]                  0
    # ทอง 96.5% อื่นๆ(รวม) = 0 
    เปรียบเทียบข้อความ                          //*[@id="n_o"]                  0
    # ทอง 96.5% น.น.(รวม) = 3.800
    เปรียบเทียบข้อความ                          //*[@id="n_w"]                  3.800
    # ทอง 96.5% ราคา(รวม) = 6,100.00
    เปรียบเทียบข้อความ                          //*[@id="sell96"]               6,100.00
    # ไถ่คืน(รวม) = 106,500.00
    เปรียบเทียบข้อความ                          //*[@id="amount_ledger"]        106,500.00
    # รายรับอื่นๆ(รวม) = 242,029.00
    เปรียบเทียบข้อความ                          //*[@id="amount_other"]         242,029.00
    # จำนวนเงินรับ(รวม) = 348,529.00
    เปรียบเทียบข้อความ                          //*[@id="total_inc"]            348,529.00
    # สด(รวม) = 534,564.00
    เปรียบเทียบข้อความ                          //*[@id="cash"]                 534,564.00
    # เครดิต(รวม) = 0.00
    เปรียบเทียบข้อความ                          //*[@id="card"]                 0.00
    # Fee(รวม) = 0.00 
    เปรียบเทียบข้อความ                          //*[@id="card_fee"]             0.00

    # รายจ่าย
    # จำนำ(รวม) = 132,500.00
    เปรียบเทียบข้อความ                          //*[@id="lease_amount"]         132,500.00
    # 96.5% น.น. (รวม) = 40.000
    เปรียบเทียบข้อความ                          //*[@id="buy_n_w"]              40.000
    # 96.5% ราคา (รวม) = 0.00
    เปรียบเทียบข้อความ                          //*[@id="buy96"]                0.00           
    # รายจ่ายอื่นๆ(รวม) = 35.00
    เปรียบเทียบข้อความ                          //*[@id="pay_other"]            35.00
    # จำนวนเงินจ่าย(รวม) = 186,035.00
    เปรียบเทียบข้อความ                          //*[@id="total_exc"]            186,035.00
    ปิด Preview 

Test43 ทดสอบการแสดงรายงานบิลรับเงิน-จ่ายเงิน	
    เลือกรายการ     //*[@id="root-content"]/div/div/div[1]/div/div[4]/div[2]/a[3]
    
    ไปลิงค์รายงานพร้อม start_date end_date          ledger_list             01/09/2019          30/09/2019
    Wait Until Page Contains                        เงือนไขค้นหา            300
    Sleep                                           1
    # สาขา  ปากน้ำ
    กรอกข้อมูลและตัวเลือก                       1                       9

    กดพิมพ์
    Sleep                                           3
    # สาขา : ปากน้ำ
    เปรียบเทียบข้อความ                          //*[@id="branch"]               สาขา : ปากน้ำ
    # วันที่ : 01/09/2019 ถึงวันที่ : 30/09/2019
    เปรียบเทียบข้อความ                          //*[@id="date"]                 วันที่ : 01/09/2019 ถึงวันที่ : 30/09/2019
    # จำนวนรับเงิน = 6,062,700.00
    เปรียบเทียบข้อความ                          //*[@id="income"]               6,062,700.00
    # จำนวนจ่ายเงิน = 1,836,338.00
    เปรียบเทียบข้อความ                          //*[@id="expenses"]             1,836,338.00
    # ยอดคงเหลือ = 4,226,362.00
    เปรียบเทียบข้อความ                          //*[@id="balance"]              4,226,362.00
    ปิด Preview 

Test44 ทดสอบการแสดงรายงานยอดขาย
    เลือกรายการ     //*[@id="root-content"]/div/div/div[1]/div/div[4]/div[2]/a[4]
    
    ไปลิงค์รายงานพร้อม start_date end_date          sales                   26/09/2019          26/09/2019
    Wait Until Page Contains                        เงือนไขค้นหา            300
    Sleep                                           1
    # สาขา  ลาดกระบัง2
    กรอกข้อมูลและตัวเลือก                       1                       11
    กดพิมพ์
    Sleep                                           3
    # สาขา : ลาดกระบัง2
    เปรียบเทียบข้อความ                          //*[@id="branch"]               สาขา : ลาดกระบัง2
    # ตั้งแต่ : 26/09/2019 ถึง 26/09/2019
    เปรียบเทียบข้อความ                          //*[@id="date"]                 ตั้งแต่ : 26/09/2019 ถึง 26/09/2019
    # รวมทั้งหมด
    # รับเงินสด = 59,900.00
    เปรียบเทียบข้อความ                          //*[@id="total_cash"]           59,900.00
    # รับเครดิต+ค่าธรรมเนียม= 23,000.00
    เปรียบเทียบข้อความ                          //*[@id="total_card"]           23,000.00
    # รับเช็ค = 0.00
    เปรียบเทียบข้อความ                          //*[@id="total_check"]          0.00
    ปิด Preview 

Test45 ทดสอบการแสดงรายงานบิลรับเงิน-จ่ายเงินแยกตามประเภท		
    เลือกรายการ     //*[@id="root-content"]/div/div/div[1]/div/div[4]/div[2]/a[5]
    
    ไปลิงค์รายงานพร้อม start_date end_date          ledger_kind             01/10/2019          20/10/2019
    Wait Until Page Contains                        เงือนไขค้นหา            300
    Sleep                                           1
    # สาขา  พูนทรัพย์
    กรอกข้อมูลและตัวเลือก                       1                       8
    กดพิมพ์

    Sleep                                           3
    # สาขา : พูนทรัพย์
    เปรียบเทียบข้อความ                          //*[@id="branch"]               สาขา : พูนทรัพย์
    # วันที่ : 01/10/2019 ถึงวันที่ : 20/10/2019
    เปรียบเทียบข้อความ                          //*[@id="date"]                 วันที่ : 01/10/2019 ถึงวันที่ : 20/10/2019
    # ยอดเงินสดคงเหลือ = 5,929,831.00
    เปรียบเทียบข้อความ                          //*[@id="balance_cash"]         5,929,831.00
    # ยอดคงเหลือทั้งหมด = 5,954,345.00
    เปรียบเทียบข้อความ                          //*[@id="balance_total"]        5,954,345.00
    ปิด Preview 

Test46 ทดสอบการแสดงรายงานการชำระเงินแบบเครดิต (คำนวณผิด)
    เลือกรายการ     //*[@id="root-content"]/div/div/div[1]/div/div[4]/div[2]/a[6]
    
    ไปลิงค์รายงานพร้อม start_date end_date          ledger_card             19/09/2019          23/09/2019
    Wait Until Page Contains                        เงือนไขค้นหา            300
    Sleep                                           1
    # สาขา  สินสาคร
    กรอกข้อมูลและตัวเลือก                       1                       7

    กดพิมพ์
    Sleep                                           3
    # สาขา : สินสาคร
    เปรียบเทียบข้อความ                          //*[@id="branch"]               สาขา : สินสาคร
    # วันที่ : 23/09/2019 ถึงวันที่ : 27/09/2019
    เปรียบเทียบข้อความ                          //*[@id="date"]                 วันที่ : 19/09/2019 ถึงวันที่ : 23/09/2019
    # รวมทั้งหมด
    # ยอดสุทธิ = 165,806.00
    เปรียบเทียบข้อความ                          //*[@id="tota"]                 165,806.00
    # เครดิต+ค่าธรรมเนียม = 160,124.00
    เปรียบเทียบข้อความ                          //*[@id="card"]                 160,124.00
    ปิด Preview 

# Test47 ทดสอบการแสดงรายงานการชำระเงินแบบเช็ค (ไม่มีข้อมูล)
#     เลือกรายการ     //*[@id="root-content"]/div/div/div[1]/div/div[4]/div[2]/a[7]
    
#     ไปลิงค์รายงานพร้อม start_date end_date          ledger_check            24/09/2019          27/09/2019
#     Wait Until Page Contains                        เงือนไขค้นหา            300
#     Sleep                                           1
#     # สาขา  สะแกงาม
#     กรอกข้อมูลและตัวเลือก                       1                       4

#     กดพิมพ์
#     # สาขา : สะแกงาม
#     เปรียบเทียบข้อความ                          //*[@id="branch"]               สาขา : สะแกงาม
#     # วันที่ : 24/09/2019 ถึงวันที่ : 27/09/2019
#     เปรียบเทียบข้อความ                          //*[@id="date"]                 วันที่ : 24/09/2019 ถึงวันที่ : 27/09/2019
#     ปิด Preview 

Test48 ทดสอบการแสดงรายงานสรุปยอดขายทองรวม เมื่อเลือก ปี 2019	
    เลือกรายการ     //*[@id="root-content"]/div/div/div[1]/div/div[4]/div[2]/a[8]
    
    # สาขา  อัญธานี
    กรอกข้อมูลและตัวเลือก                       1                       3
    # ปี      2019
    กรอกข้อมูลและตัวเลือก                       2                       4
    กดพิมพ์
    Sleep                                           3
    # สาขา : อัญธานี
    เปรียบเทียบข้อความ                          //*[@id="branch"]               สาขา : อัญธานี
    # เดือน 10/2019
    # -รวม = 769,528.00
    เปรียบเทียบข้อความ                          //*[@id="total_10/2019"]        769,528.00
    # เดือน 09/2019
    # -รวม = 479,950.00
    เปรียบเทียบข้อความ                          //*[@id="total_09/2019"]        479,950.00
    ปิด Preview 

*** Keywords ***