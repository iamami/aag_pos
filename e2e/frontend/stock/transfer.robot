*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบAdminและinit_data
Test Setup        เข้าหน้าสต็อกทอง
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data_admin
*** Variables ***

*** Test cases ***
Test1 ทดสอบการแสดงรายการโอนทองใหม่ระหว่างสาขาเมื่อกดปุ่มวันนี้
    เข้าหน้าโอนทองใหม่ระหว่าสาขา
    Sleep                                 3
    กดปุ่มสร้างใบโอนทอง
    Sleep                                 3
    เลือกจากสาขาลาดกระบังเพื่อสร้าง
    Sleep                                 3
    เลือกไปสาขากิ่งแก้วเพื่อสร้าง
    Sleep                                 3
    กดปุ่มสร้าง
    พบข้อความ                             แก้ไขรายการโอนทองใหม่ระหว่างสาขา
    พบข้อความ                             ยังไม่อัพเดทสต๊อก
    #พบpopup แก้ไขรายการโอนทองใหม่ระหว่างสาขา
    กดปุ่มปิดโอนทอง
    Sleep                                 3
    กดปุ่มวันนี้
    พบจำนวนแถว                            1
    พบข้อความ                             TR-191100001
    #พบรายการโอนทองใหม่ระหว่างสาขาวันนี้

Test2 ทดสอบการแสดงรายการโอนทองใหม่ระหว่างสาขาเมื่อเลือกจากสาขา
    เข้าหน้าโอนทองใหม่ระหว่าสาขา
    Sleep                               3
    Go To                               ${BASE_URL}stock/transfer?start_date=01/01/2016&end_date=04/11/2019
    Sleep                                 3
    เลือกจากสาขากิ่งแก้ว
    Sleep                                 3
    กดปุ่มค้นหา
    Sleep                                 3
    พบจำนวนแถว                            2
    #พบรายการโอนทองใหม่ระหว่างสาขาเฉพาะสาขาต้นทางที่เลือก

Test3 ทดสอบการแสดงรายการโอนทองใหม่ระหว่างสาขาเมื่อเลือกไปสาขา
    เข้าหน้าโอนทองใหม่ระหว่าสาขา
    Sleep                               3
    Go To                               ${BASE_URL}stock/transfer?start_date=01/01/2016&end_date=04/11/2019
    Sleep                                 3
    เลือกไปสาขาลาดกระบัง
    Sleep                                 3
    กดปุ่มค้นหา
    Sleep                                 3
    พบจำนวนแถว                            2
    #พบรายการโอนทองใหม่ระหว่างสาขาเฉพาะสาขาต้นทางที่เลือก

Test4 ทดสอบการแสดงรายการโอนทองใหม่ระหว่างสาขาเมื่อเลือกวันที่ถึงวันที่
    เข้าหน้าโอนทองใหม่ระหว่าสาขา
    Sleep                               3
    Go To                               ${BASE_URL}stock/transfer?start_date=01/01/2016&end_date=04/11/2019
    Sleep                                 3
    กดปุ่มค้นหา
    Sleep                                 3
    พบจำนวนแถว                            4

Test5 ทดสอบการแสดงรายการโอนทองใหม่ระหว่างสาขาเมื่อเลือกยังไม่ปรับปรุงสต็อก
    เข้าหน้าโอนทองใหม่ระหว่าสาขา
    Sleep                               3
    Go To                               ${BASE_URL}stock/transfer?start_date=01/01/2016&end_date=04/11/2019
    Sleep                                 3
    เลือกยังไม่ปรับปรุงสต็อก
    Sleep                                 3
    กดปุ่มค้นหา
    Sleep                                 3
    พบจำนวนแถว                            2
    #พบรายการโอนทองใหม่ระหว่างสาขาเฉพาะที่ยังไม่ปรับปรุงสต็อก
    ไม่พบวันที่ปรับปรุงสต็อก
    #ไม่พบวันที่ปรับปรุงสต็อก

Test6 ทดสอบการแสดงรายการโอนทองใหม่ระหว่างสาขาเมื่อเลือกยังไม่เคลียร์บิล
    เข้าหน้าโอนทองใหม่ระหว่าสาขา
    Sleep                               3
    Go To                               ${BASE_URL}stock/transfer?start_date=01/01/2016&end_date=04/11/2019
    Sleep                                 3
    เลือกยังไม่เคลียร์บิล
    Sleep                                 3
    กดปุ่มค้นหา
    Sleep                                 3
    พบจำนวนแถว                           3
    #พบรายการโอนทองใหม่ระหว่างสาขาเฉพาะที่ยังไม่เคลียร์บิล

Test7 ทดสอบการแสดงรายการโอนทองใหม่ระหว่างสาขาเมื่อเลือกจากสาขา/ไปสาขา/วันที่/ยังไม่เคลียร์บิล
    เข้าหน้าโอนทองใหม่ระหว่าสาขา
    Sleep                               3
    Go To                               ${BASE_URL}stock/transfer?start_date=01/01/2016&end_date=04/11/2019
    Sleep                                 3
    เลือกจากสาขากิ่งแก้ว
    Sleep                                 3
    เลือกไปสาขาลาดกระบัง
    Sleep                                 3
    เลือกยังไม่เคลียร์บิล
    Sleep                                 3
    กดปุ่มค้นหา
    Sleep                                 3
    พบจำนวนแถว                            2
    #พบรายการที่ต้องการค้นหา

Test8 ทดสอบการไม่สามารถเพิ่มรายการสินค้าในหน้าโอนทองใหม่ระหว่างสาขาเมื่อยังไม่สร้างรายการ
    เข้าหน้าโอนทองใหม่ระหว่าสาขา
    Sleep                                 3
    กดปุ่มสร้างใบโอนทอง
    Wait Until Element Is Visible         id:btnAdd
    ${value} =                            Get Element Attribute                                                                                          id:btnAdd                                     disabled
    Should Be Equal As Strings            ${value}                                                                                                       true
    กดปุ่มปิดโอนทอง
    #ไม่สามารถกดปุ่มเพิ่มรายการสินค้าได้

Test9 ทดสอบการไม่สามารถสั่งพิมพ์ในหน้าโอนทองใหม่ระหว่างสาขาได้เมื่อยังไม่สร้างรายการ
    เข้าหน้าโอนทองใหม่ระหว่าสาขา
    Sleep                                 3
    กดปุ่มสร้างใบโอนทอง
    Wait Until Element Is Visible         id:btnPrint
    ${value} =                            Get Element Attribute                                                                                          id:btnPrint                                   disabled
    Should Be Equal As Strings            ${value}                                                                                                       true
    กดปุ่มปิดโอนทอง
    #ไม่สามารถกดปุ่มพิมพ์ได้

Test10 ทดสอบการไม่สามารถบันทึกและอัพเดทสต็อกในหน้าโอนทองใหม่ระหว่างสาขาได้เมื่อยังไม่สร้างรายการและเพิ่มรายการสินค้า
    เข้าหน้าโอนทองใหม่ระหว่าสาขา
    Sleep                                 3
    กดปุ่มสร้างใบโอนทอง
    Wait Until Element Is Visible         id:btnSaveAndUpdate
    ${value} =                            Get Element Attribute                                                                                          id:btnSaveAndUpdate                           disabled
    Should Be Equal As Strings            ${value}                                                                                                       true
    กดปุ่มปิดโอนทอง
    #ไม่สามารถกดปุ่มบันทึกและอัพเดทสต็อกได้

Test12 ทดสอบการเพิ่มรายการโอนทองใหม่ระหว่างสาขาไม่สำเร็จเมื่อไม่เลือกจากสาขา
    เข้าหน้าโอนทองใหม่ระหว่าสาขา
    Sleep                                 3
    กดปุ่มสร้างใบโอนทอง
    Sleep                                 3
    เลือกไปสาขาลาดกระบังเพื่อสร้าง
    Sleep                                 3
    กดปุ่มสร้าง
    Sleep                                 3
    พบข้อความ                             *จากสาขากรุณาเลือกสาขา
    กดปุ่มปิดโอนทอง
    #พบข้อความ *จากสาขากรุณาเลือกสาขา

Test13 ทดสอบการเพิ่มรายการโอนทองใหม่ระหว่างสาขาไม่สำเร็จเมื่อไม่เลือกไปสาขา
    เข้าหน้าโอนทองใหม่ระหว่าสาขา
    Sleep                                 3
    กดปุ่มสร้างใบโอนทอง
    Sleep                                 3
    เลือกจากสาขาลาดกระบังเพื่อสร้าง
    Sleep                                 3
    กดปุ่มสร้าง
    Sleep                                 3
    พบข้อความ                             *ไปสาขากรุณาเลือกสาขา
    กดปุ่มปิดโอนทอง
    #พบข้อความ *ไปสาขากรุณาเลือกสาขา

Test14 ทดสอบการเพิ่มรายการโอนทองใหม่ระหว่างสาขาสำเร็จเมื่อกดปุ่มสร้าง
    เข้าหน้าโอนทองใหม่ระหว่าสาขา
    Sleep                                 3
    กดปุ่มสร้างใบโอนทอง
    Sleep                                 3
    เลือกจากสาขาลาดกระบังเพื่อสร้าง
    Sleep                                 3
    เลือกไปสาขากิ่งแก้วเพื่อสร้าง
    Sleep                                 3
    กดปุ่มสร้าง
    Sleep                                 3
    พบข้อความ                             แก้ไขรายการโอนทองใหม่ระหว่างสาขา
    Sleep                                 3    
    พบข้อความ                       ยังไม่อัพเดทสต๊อก
    Sleep                                 3    
    #พบpopup แก้ไขรายการโอนทองใหม่ระหว่างสาขา
    กดปุ่มปิดโอนทอง
    #พบข้อความ *ไปสาขาเลือกสาขาไม่ถูกต้อง
    Sleep                                 3
    พบจำนวนแถว                            2
    พบข้อความ                             TR-191100002

Test15 ทดสอบการแสดงรายละเอียดบนpopupแก้ไขรายการโอนทองใหม่ระหว่างสาขาถูกต้อง เมื่อกดปุ่มแก้ไขบนหน้าโอนทองใหม่ระหว่างสาขา
    เข้าหน้าโอนทองใหม่ระหว่าสาขา
    Sleep                                 3
    กดปุ่มแก้ไขใบโอนทอง
    Sleep                                 3
    พบจากสาขา                             ลาดกระบัง
    พบไปสาขา                              กิ่งแก้ว
    พบเลขที่บิล                           [บิลเลขที่ TR-191100001]
    #ข้อมูลจากสาขา/ไปสาขา/วันที่ถูกต้อง
    กดปุ่มปิดโอนทอง

# Test16 ทดสอบการไม่สามารถแก้ไขจากสาขาในหน้าแก้ไขรายการโอนทองใหม่ระหว่างสาขา
#     เข้าหน้าโอนทองใหม่ระหว่าสาขา
#     Sleep                                 3
#     กดปุ่มแก้ไขใบโอนทอง
#     Sleep                                 3
#     Wait Until Element Is Visible         id:dropDownFromBranchForAdd
#     ${value} =                            Get Element Attribute                                                                                 id:dropDownFromBranchForAdd                   disabled
#     Should Be Equal As Strings            ${value}                                                                                                       true
#     #ข้อมูลจากสาขา/ไปสาขา/วันที่ถูกต้อง
#     กดปุ่มปิดโอนทอง

# Test17 ทดสอบการไม่สามารถแก้ไขวันที่ในหน้าแก้ไขรายการโอนทองใหม่ระหว่างสาขา
#     เข้าหน้าโอนทองใหม่ระหว่าสาขา
#     Sleep                                 3
#     กดปุ่มแก้ไขใบโอนทอง
#     Sleep                                 3
#     Wait Until Element Is Visible         id:dropDownToBranchForAdd
#     ${value} =                            Get Element Attribute                                                                    id:dropDownToBranchForAdd                   disabled
#     Should Be Equal As Strings            ${value}                                                                                                       true
#     #ไม่สามารถแก้ไขไปสาขาได้
#     กดปุ่มปิดโอนทอง

# Test18 ทดสอบการไม่สามารถแก้ไขวันที่ในหน้าแก้ไขรายการโอนทองใหม่ระหว่างสาขา
#     เข้าหน้าโอนทองใหม่ระหว่าสาขา
#     Sleep                                 3
#     กดปุ่มแก้ไขใบโอนทอง
#     Sleep                                 3
#     Wait Until Element Is Visible         id:dateStartForAdd
#     ${value} =                            Get Element Attribute                                                                                          id:dateStartForAdd                   disabled
#     Should Be Equal As Strings            ${value}                                                                                                       true
#     #ไม่สามารถแก้ไขวันที่ได้
#     กดปุ่มปิดโอนทอง

Test19 ทดสอบการไม่สามารถบันทึกและอัพเดทสต็อกในหน้าแก้ไขรายการโอนทองใหม่ระหว่างสาขาได้เมื่อยังไม่เพิ่มรายการสินค้า
    เข้าหน้าโอนทองใหม่ระหว่าสาขา
    Sleep                                 3
    กดปุ่มแก้ไขใบโอนทอง
    Sleep                                 3
    Wait Until Element Is Visible         id:btnSaveAndUpdate
    ${value} =                            Get Element Attribute                                                                                          id:btnSaveAndUpdate                   disabled
    Should Be Equal As Strings            ${value}                                                                                                       true
    #ไม่สามารถกดปุ่มบันทึกและอัพเดทสต็อกได้
    กดปุ่มปิดโอนทอง

Test20 ทดสอบการสามารถสั่งพิมพ์ในหน้าแก้ไขรายการโอนทองใหม่ระหว่างสาขา
    เข้าหน้าโอนทองใหม่ระหว่าสาขา
    Sleep                                 3
    กดปุ่มแก้ไขใบโอนทอง
    Sleep                                 3
    Wait Until Element Is Visible         id:btnPrint
    ${value} =                            Get Element Attribute                                                                                          id:btnPrint                                   disabled
    Should Be Equal As Strings            ${value}                                                                                                       None
    #สามารถกดปุ่มพิมพ์ได้
    กดปุ่มปิดโอนทอง

Test21 ทดสอบการสามารถบันทึกและอัพเดทสต็อกในหน้าแก้ไขรายการโอนทองใหม่ระหว่างสาขาเมื่อมีรายการสินค้า
    เข้าหน้าโอนทองใหม่ระหว่าสาขา
    Sleep                                 3
    กดปุ่มแก้ไขใบโอนทอง
    Sleep                                 3
    กดปุ่มเพิ่มรายการสินค้า
    Sleep                                 3
    เลือกรหัสสินค้า
    Sleep                                 3
    กรอกจำนวน(ชิ้น)                       1
    Sleep                                 3
    กดปุ่มเพิ่มสินค้าเพื่อรายการสินค้า
    Sleep                                 3
    Wait Until Element Is Visible         id:btnSaveAndUpdate
    ${value} =                            Get Element Attribute                                                                                          id:btnSaveAndUpdate                           disabled
    Should Be Equal As Strings            ${value}                                                                                                       None
    #สามารถกดปุ่มบันทึกและอัพเดทสต็อกได้
    กดปุ่มปิดโอนทอง

Test23 ทดสอบการแก้ไขรายการโอนทองใหม่ระหว่างสาขาไม่สำเร็จเมื่อปฏิเสธการยืนยันการบันทึกและอัพเดทสต๊อก
    เข้าหน้าโอนทองใหม่ระหว่าสาขา
    Sleep                                 3
    กดปุ่มแก้ไขใบโอนทอง2
    Sleep                                 3
    กดปุ่มเพิ่มรายการสินค้า
    Sleep                                 3
    เลือกรหัสสินค้า
    Sleep                                 3
    กรอกจำนวน(ชิ้น)                          1
    Sleep                                 3
    กดปุ่มเพิ่มสินค้าเพื่อรายการสินค้า
    Sleep                                 3
    กดปุ่มบันทึกและอัพเดทสต็อก
    Sleep                                 3
    ตรวจพบข้อความบน Alertแล้วCancel       ยืนยันบันทึกและอัพเดทสต๊อก
    Sleep                                 3
    กดปุ่มปิดโอนทอง
    Sleep                                 3
    เลือกยังไม่ปรับปรุงสต็อก
    Sleep                                 3
    กดปุ่มค้นหา
    Sleep                                 3
    พบจำนวนแถว                            2
    #ไม่พบการเปลี่ยนแปลงของรายการที่ทำล่าสุด

Test24 ทดสอบการแก้ไขรายการโอนทองใหม่ระหว่างสาขาสำเร็จเมื่อกดปุ่มบันทึก
    เข้าหน้าโอนทองใหม่ระหว่าสาขา
    Sleep                                 3
    กดปุ่มแก้ไขใบโอนทอง2
    Sleep                                 3
    กดปุ่มสร้าง
    Sleep                                 3
    พบข้อความ                            ยังไม่อัพเดทสต๊อก    
    #พบข้อความ อัพเดทสต็อกแล้ว
    Sleep                                 3
    Wait Until Element Is Visible         id:btnAdd
    ${value} =                            Get Element Attribute                                                                                          id:btnAdd                                   disabled
    Should Be Equal As Strings            ${value}                                                                                                       None
    #ปุ่มเพิ่มรายการสินค้าไม่สามารถกดได้
    Sleep                                 3
    Wait Until Element Is Visible         id:btnSaveAndUpdate
    ${value} =                            Get Element Attribute                                                                                          id:btnSaveAndUpdate                   disabled
    Should Be Equal As Strings            ${value}                                                                                                       None
    #ไม่สามารถกดปุ่มบันทึกและอัพเดทสต็อกได้
    Sleep                                 3           
    กดปุ่มปิดโอนทอง
    Sleep                                 3
    เลือกยังไม่ปรับปรุงสต็อก
    Sleep                                 3
    กดปุ่มค้นหา
    Sleep                                 3
    พบจำนวนแถว                            2
    #ไม่พบการเปลี่ยนแปลงของรายการที่ทำล่าสุด

Test25 ทดสอบการแก้ไขรายการโอนทองใหม่ระหว่างสาขาสำเร็จเมื่อตกลงการยืนยันการบันทึกและอัพเดทสต๊อก
    เข้าหน้าโอนทองใหม่ระหว่าสาขา
    Sleep                                 3
    กดปุ่มแก้ไขใบโอนทอง2
    Sleep                                 3
    กดปุ่มบันทึกและอัพเดทสต็อก
    Sleep                                 3
    ตรวจพบข้อความบน Alert                 ยืนยันบันทึกและอัพเดทสต๊อก
    Sleep                                 3
    พบข้อความ                          อัพเดทสต๊อกแล้ว    
    #พบข้อความ อัพเดทสต็อกแล้ว
    Sleep                                 3
    Wait Until Element Is Visible         id:btnAdd
    ${value} =                            Get Element Attribute                                                                                          id:btnAdd                                   disabled
    Should Be Equal As Strings            ${value}                                                                                                       true
    #ปุ่มเพิ่มรายการสินค้าไม่สามารถกดได้
    Sleep                                 3
    Wait Until Element Is Visible         id:btnSaveAndUpdate
    ${value} =                            Get Element Attribute                                                                                          id:btnSaveAndUpdate                   disabled
    Should Be Equal As Strings            ${value}                                                                                                       true
    #ไม่สามารถกดปุ่มบันทึกและอัพเดทสต็อกได้
    Sleep                                 3           
    กดปุ่มปิดโอนทอง
    Sleep                                 3
    เลือกยังไม่ปรับปรุงสต็อก
    Sleep                                 3
    กดปุ่มค้นหา
    Sleep                                 3
    พบจำนวนแถว                            1
    #ไม่พบการเปลี่ยนแปลงของรายการที่ทำล่าสุด
    เข้าหน้าสต็อกทองใหม่
    Sleep                                 3
    กดเข้าดูประวัติสาขาลาดกระบัง
    Sleep                                 3
    พบสต็อกการ์ดทอง                         7
    Sleep                                 3
    กดปุ่มปิดสต็อกการ์ทอง

Test26 ทดสอบการลบรายการสินค้าที่จะถูกโอนระกว่างสาขาไม่สำเร็จเมื่อกดปฏิเสธยืนยันการลบรายการที่ยังไม่อัพเดทสต็อก
    เข้าหน้าโอนทองใหม่ระหว่าสาขา
    Sleep                                 3
    กดปุ่มแก้ไขใบโอนทอง
    Sleep                                 3
    กดปุ่มลบสินค้า
    Sleep                                 3
    ตรวจพบข้อความบน Alertแล้วCancel       ยืนยันลบ
    Sleep                                 3
    #รายการไม่ถูกลบ
    Wait Until Element Is Visible         id:btnSaveAndUpdate
    ${value} =                            Get Element Attribute                                                                                          id:btnSaveAndUpdate                   disabled
    Should Be Equal As Strings            ${value}                                                                                                       None
    Sleep                                 3
    พบจำนวนรายการ                       1
    Sleep                                 3
    กดปุ่มปิดโอนทอง

Test27 ทดสอบการลบรายการสินค้าที่จะถูกโอนระกว่างสาขาไม่สำเร็จเมื่อกดปฏิเสธการลบรายการที่อัพเดทสต็อกไปแล้ว
    เข้าหน้าโอนทองใหม่ระหว่าสาขา
    Sleep                                 3
    กดปุ่มแก้ไขใบโอนทอง2
    Sleep                                 3
    กดปุ่มลบสินค้า
    Sleep                                 3
    ตรวจพบข้อความบน Alertแล้วCancel       ยืนยันลบ
    Sleep                                 3
    #พบ popup แก้ไขโอนทองใหม่ระหว่างสาขา
    Sleep                                 3
    Wait Until Element Is Visible         id:btnSaveAndUpdate
    ${value} =                            Get Element Attribute                                                                                          id:btnSaveAndUpdate                   disabled
    Should Be Equal As Strings            ${value}                                                                                                       true
    Sleep                                 3
    พบจำนวนรายการ                       1
    Sleep                                 3
    กดปุ่มปิดโอนทอง

Test28 ทดสอบการลบรายการสินค้าที่จะถูกโอนระกว่างสาขาไม่สำเร็จเมื่อกดยืนยันการลบรายการที่อัพเดทสต็อกไปแล้ว
    เข้าหน้าโอนทองใหม่ระหว่าสาขา
    Sleep                                 3
    กดปุ่มแก้ไขใบโอนทอง2
    Sleep                                 3
    กดปุ่มลบสินค้า
    Sleep                                 3
    ตรวจพบข้อความบน Alert                  ยืนยันลบ
    Sleep                                 3
    ตรวจพบข้อความบน Alert                  ไม่สามารถลบรายการนี้ได้
    Sleep                                 3
    #พบ alert ไม่สามารถลบรายการนี้ได้
    Sleep                                 3
    Wait Until Element Is Visible         id:btnSaveAndUpdate
    ${value} =                            Get Element Attribute                                                                                          id:btnSaveAndUpdate                   disabled
    Should Be Equal As Strings            ${value}                                                                                                       true
    Sleep                                 3
    พบจำนวนรายการ                       1
    Sleep                                 3
    กดปุ่มปิดโอนทอง

Test29 ทดสอบการลบรายการสินค้าที่จะถูกโอนระกว่างสาขาสำเร็จเมื่อกดยืนยันยืนยันการลบรายการที่ยังไม่อัพเดทสต็อก
    เข้าหน้าโอนทองใหม่ระหว่าสาขา
    Sleep                                 3
    กดปุ่มแก้ไขใบโอนทอง
    Sleep                                 3
    กดปุ่มลบสินค้า
    Sleep                                 3
    ตรวจพบข้อความบน Alert                  ยืนยันลบ
    Sleep                                 3
    #รายการถูกลบ
    Sleep                                 3
    Wait Until Element Is Visible         id:btnSaveAndUpdate
    ${value} =                            Get Element Attribute                                                                                          id:btnSaveAndUpdate                   disabled
    Should Be Equal As Strings            ${value}                                                                                                       true
    Sleep                                 3
    พบจำนวนรายการ                           0  
    Sleep                                 3  
    กดปุ่มปิดโอนทอง

Test31 ทดสอบการคำนวณในหน้าแก้ไขโอนทองใหม่ระหว่างสาขา
    เข้าหน้าโอนทองใหม่ระหว่าสาขา
    Sleep                                 3
    กดปุ่มแก้ไขใบโอนทอง2
    พบจำนวนรายการ                       1
    Sleep                                 3
    #พบจำนวนรายการถูกต้อง
    พบค่าแรงขายปลีกรวม                     0.00
    Sleep                                 3
    #พบค่าแรงขายปลีกรวมถูกต้อง
    พบน้ำหนักรวม                            9.200
    Sleep                                 3  
    #พบน้ำหนักรวมถูกต้อง 
    พบจำนวนรวม                          1  
    #พบจำนวนรวมถูกต้อง 
    Sleep                                 3
    กดปุ่มปิดโอนทอง

*** Keywords ***
เข้าหน้าโอนทองใหม่ระหว่าสาขา
    Sleep                                 3
    Wait Until Page Contains Element      id:transferStock                                                                                               300
    Click Element                         id:transferStock

กดปุ่มวันนี้
    Sleep                                 2
    Wait Until Page Contains Element      id:btnToday                                                                                                    300
    Click Element                         id:btnToday

กดปุ่มค้นหา
    Sleep                                 2
    Wait Until Page Contains Element      id:btnSearch                                                                                                   300
    Click Element                         id:btnSearch

กดปุ่มสร้างใบโอนทอง
    Sleep                                 2
    Wait Until Page Contains Element      id:btnCreateTransfer                                                                                           300
    Click Element                         id:btnCreateTransfer

กดปุ่มปิดโอนทอง
    Sleep                                 2
    Wait Until Page Contains Element      id:btnClose                                                                                                    300
    Click Element                         id:btnClose

กดปุ่มสร้าง
    Sleep                                 2
    # Scroll Element Into View            id:btnSave
    Wait Until Element Is Visible         id:btnSave                                                                                                     300
    Click Element                         id:btnSave

เลือกจากสาขากิ่งแก้ว
    Wait Until Element Is Visible         id:dropDownFromBranch                                                                                          300
    Click Element                         id:dropDownFromBranch
    Wait Until Element Is Visible         //*[@id="dropDownFromBranch"]/div[2]/div[3]                                                                    300
    Click Element                         //*[@id="dropDownFromBranch"]/div[2]/div[3]

เลือกจากสาขาลาดกระบังเพื่อสร้าง
    Wait Until Element Is Visible         id:dropDownFromBranchForAdd                                                                                    300
    Click Element                         id:dropDownFromBranchForAdd
    Wait Until Element Is Visible         //*[@id="dropDownFromBranchForAdd"]/div[2]/div[1]                                                              300
    Click Element                         //*[@id="dropDownFromBranchForAdd"]/div[2]/div[1]

เลือกไปสาขาลาดกระบัง
    Wait Until Element Is Visible         id:dropDownToBranch                                                                                            300
    Click Element                         id:dropDownToBranch
    Wait Until Element Is Visible         //*[@id="dropDownToBranch"]/div[2]/div[2]                                                                      300
    Click Element                         //*[@id="dropDownToBranch"]/div[2]/div[2]

เลือกไปสาขากิ่งแก้ว
    Wait Until Element Is Visible         id:dropDownToBranch                                                                                            300
    Click Element                         id:dropDownToBranch
    Wait Until Element Is Visible         //*[@id="dropDownToBranch"]/div[2]/div[1]                                                                      300
    Click Element                         //*[@id="dropDownToBranch"]/div[2]/div[1]

เลือกไปสาขาลาดกระบังเพื่อสร้าง
    Wait Until Element Is Visible         id:dropDownToBranchForAdd                                                                                      300
    Click Element                         id:dropDownToBranchForAdd
    Wait Until Element Is Visible         //*[@id="dropDownToBranchForAdd"]/div[2]/div[1]                                                                300
    Click Element                         //*[@id="dropDownToBranchForAdd"]/div[2]/div[1]

เลือกไปสาขากิ่งแก้วเพื่อสร้าง
    Wait Until Element Is Visible         id:dropDownToBranchForAdd                                                                                      300
    Click Element                         id:dropDownToBranchForAdd
    Wait Until Element Is Visible         //*[@id="dropDownToBranchForAdd"]/div[2]/div[1]                                                                300
    Click Element                         //*[@id="dropDownToBranchForAdd"]/div[2]/div[1]

เลือกยังไม่ปรับปรุงสต็อก
    Sleep                                 2
    Wait Until Page Contains Element      //*[@id="root-content"]/div/div/div[2]/div/div/form[2]/div[2]/div[1]/div/label                                 300
    Click Element                         //*[@id="root-content"]/div/div/div[2]/div/div/form[2]/div[2]/div[1]/div/label

เลือกยังไม่เคลียร์บิล
    Sleep                                 2
    Wait Until Page Contains Element      //*[@id="root-content"]/div/div/div[2]/div/div/form[2]/div[2]/div[2]/div/label                                 300
    Click Element                         //*[@id="root-content"]/div/div/div[2]/div/div/form[2]/div[2]/div[2]/div/label

พบจำนวนแถว
    [Arguments]                           ${value}
    ${count} =                            Get Value                                                                                                      id:amountRow
    ${count_str} =                        Convert To String                                                                                              ${count}
    Should Be Equal                       ${count_str}                                                                                                   ${value}

ไม่พบวันที่ปรับปรุงสต็อก
    ${count} =                            Get Value                                                                                                      id:dateUpdate
    ${count_str} =                        Convert To String                                                                                              ${count}
    Should Be Equal                       ${count_str}                                                                                                   None

กดปุ่มแก้ไขใบโอนทอง
    Wait Until Element Is Visible         //*[@id="table_width"]/div/div/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a/i                 300
    Click Element                         //*[@id="table_width"]/div/div/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a/i

กดปุ่มแก้ไขใบโอนทอง2
    Wait Until Element Is Visible         //*[@id="table_width"]/div/div/div[3]/div[2]/div/div/div[2]/div/div[1]/div/div/div/div/center/a/i                 300
    Click Element                         //*[@id="table_width"]/div/div/div[3]/div[2]/div/div/div[2]/div/div[1]/div/div/div/div/center/a/i

พบจากสาขา
    [Arguments]                           ${value}
    ${count} =                            Get Text                                                                                                       //*[@id="dropDownFromBranchForAdd"]/div[1]
    ${count_str} =                        Convert To String                                                                                              ${count}
    Should Be Equal                       ${count_str}                                                                                                   ${value}

พบไปสาขา
    [Arguments]                           ${value}
    ${count} =                            Get Text                                                                                                       //*[@id="dropDownToBranchForAdd"]/div[1]
    ${count_str} =                        Convert To String                                                                                              ${count}
    Should Be Equal                       ${count_str}                                                                                                   ${value}

พบเลขที่บิล
    [Arguments]                           ${value}
    ${count} =                            Get Text                                                                                                       //*[@id="textBillID"]
    ${count_str} =                        Convert To String                                                                                              ${count}
    Should Be Equal                       ${count_str}                                                                                                   ${value}

พบวันที่
    [Arguments]                           ${value}
    ${count} =                            Get Value                                                                                                      //*[@id="dateStartForAdd"]
    ${count_str} =                        Convert To String                                                                                              ${count}
    Should Be Equal                       ${count_str}                                                                                                   ${value}

กดปุ่มเพิ่มรายการสินค้า
    Sleep                                 2
    Wait Until Element Is Visible         id:btnAdd                                                                                                      300
    Click Element                         id:btnAdd

เลือกรหัสสินค้า
    Wait Until Element Is Visible         id:dropDownProductID                                                                                           300
    Click Element                         id:dropDownProductID
    Wait Until Element Is Visible         //*[@id="dropDownProductID"]/div[2]/div                                                                        300
    Click Element                         //*[@id="dropDownProductID"]/div[2]/div

กรอกจำนวน(ชิ้น)
    [Arguments]                           ${value}
    Sleep                                 2
    Wait Until Element Is Visible         id:inputAmount
    Input Text                            id:inputAmount                                                                                                 ${value}

กดปุ่มเพิ่มสินค้าเพื่อรายการสินค้า
    Sleep                                 2
    Wait Until Element Is Visible         id:btnAddProduct                                                                                               300
    Click Element                         id:btnAddProduct

กดปุ่มบันทึกและอัพเดทสต็อก
    Sleep                                 2
    Wait Until Element Is Visible         id:btnSaveAndUpdate                                                                                            300
    Click Element                         id:btnSaveAndUpdate

พบIcon Green Check
    ${count} =                            Get Value                                                                                                      //*[@id="statusBill"]/i
    ${count_str} =                        Convert To String                                                                                              ${count}
    Should Be Equal                       ${count_str}                                                                                                   green check circle icon

พบIcon Yellow Warning
    ${count} =                            Get Value                                                                                                      //*[@id="statusBill"]/i
    ${count_str} =                        Convert To String                                                                                              ${count}
    Should Be Equal                       ${count_str}                                                                                                   yellow warning circle icon

กดปุ่มลบสินค้า
    Sleep                                 2
    Wait Until Element Is Visible         //*[@id="table_width2"]/div/div/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a/i                                                                                          300
    Click Element                         //*[@id="table_width2"]/div/div/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a/i

พบจำนวนรายการ
    [Arguments]                           ${value}
    ${count} =                            Get Value                                                                                                       //*[@id="invoice_itme_total"]
    ${count_str} =                        Convert To String                                                                                              ${count}
    Should Be Equal                       ${count_str}                                                                                                   ${value}

พบค่าแรงขายปลีกรวม
    [Arguments]                           ${value}
    ${count} =                            Get Value                                                                                                       //*[@id="invoice_profit_total"]
    ${count_str} =                        Convert To String                                                                                              ${count}
    Should Be Equal                       ${count_str}                                                                                                   ${value}

พบน้ำหนักรวม
    [Arguments]                           ${value}
    ${count} =                            Get Value                                                                                                       //*[@id="invoice_weight_total"]
    ${count_str} =                        Convert To String                                                                                              ${count}
    Should Be Equal                       ${count_str}                                                                                                   ${value}

พบจำนวนรวม
    [Arguments]                           ${value}
    ${count} =                            Get Value                                                                                                       //*[@id="invoice_product_total"]
    ${count_str} =                        Convert To String                                                                                              ${count}
    Should Be Equal                       ${count_str}                                                                                                   ${value}
