*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบAdminและinit_data
Test Setup        เข้าหน้าสต็อกทอง
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data_admin
*** Variables ***

*** Test cases ***
Test1 ทดสอบการแสดงรายการสต็อกทองใหม่เมื่อกดปุ่มทั้งหมด
    เข้าหน้าสต็อกทองใหม่
    Sleep                               3
    กดปุ่มทั้งหมด
    Sleep                               3
    พบจำนวนแถว                          2
    #พบรายการสต็อกทองใหม่ทั้งหมด

Test2 ทดสอบการแสดงรายการสต็อกทองใหม่เมื่อเลือกสาขา
    เข้าหน้าสต็อกทองใหม่
    Sleep                               3
    เลือกสาขาลาดกระบัง
    Sleep                               3
    กดปุ่มค้นหา
    Sleep                               3
    พบจำนวนแถว                          1
    #พบรายการสต็อกทองใหม่เฉพาะสาขาที่เลือก
  
Test3 ทดสอบการแสดงรายการสต็อกทองใหม่เมื่อเลือกกลุ่มสินค้า
    เข้าหน้าสต็อกทองใหม่
    Sleep                               3
    เลือกกลุ่มสินค้า
    Sleep                               3
    กดปุ่มค้นหา
    Sleep                               3
    พบจำนวนแถว                          2
    #พบรายการสต็อกทองใหม่เฉพาะกลุ่มสินค้าที่เลือก
      
Test4 ทดสอบการแสดงรายการสต็อกทองใหม่เมื่อเลือกรหัสสินค้า
    เข้าหน้าสต็อกทองใหม่
    Sleep                               3
    เลือกรหัสสินค้า
    Sleep                               3
    กดปุ่มค้นหา
    Sleep                               3
    พบจำนวนแถว                          2
    #พบรายการสต็อกทองใหม่เฉพาะรหัสสินค้าที่เลือก
      
Test5 ทดสอบการแสดงรายการสต็อกทองใหม่เมื่อเลือกชื่อสินค้า
    เข้าหน้าสต็อกทองใหม่
    Sleep                               3
    เลือกชื่อสินค้า
    Sleep                               3
    กดปุ่มค้นหา
    Sleep                               3
    พบจำนวนแถว                          2
    #พบรายการสต็อกทองใหม่เฉพาะชื่อสินค้าที่เลือก
      
Test6 ทดสอบการแสดงรายการสต็อกทองใหม่เมื่อเลือกประเภทสินค้า
    เข้าหน้าสต็อกทองใหม่
    Sleep                               3
    เลือกประเภทสินค้า
    Sleep                               3
    กดปุ่มค้นหา
    Sleep                               3
    พบจำนวนแถว                          2
    #พบรายการสต็อกทองใหม่เฉพาะประเภทสินค้าที่เลือก
      
Test7 ทดสอบการแสดงรายการสต็อกทองใหม่เมื่อเลือกประเภทงานขาย
    เข้าหน้าสต็อกทองใหม่
    Sleep                               3
    เลือกประเภทงานขาย
    Sleep                               3
    กดปุ่มค้นหา
    Sleep                               3
    พบจำนวนแถว                          2
    #พบรายการสต็อกทองใหม่เฉพาะประเภทงานขายเลือก
   
Test8 ทดสอบการแสดงรายการสต็อกทองใหม่เมื่อเลือกน้ำหนัก บาท
    เข้าหน้าสต็อกทองใหม่
    Sleep                               3
    เลือกน้ำหนักบาท
    Sleep                               3
    กดปุ่มค้นหา
    Sleep                               3
    พบจำนวนแถว                          0
    #พบรายการสต็อกทองใหม่เฉพาะน้ำหนัก บาทที่เลือก
   
Test9 ทดสอบการแปลงน้ำหนัก บาทเป็นน้ำหนัก กรัม ถูกต้อง
    เข้าหน้าสต็อกทองใหม่
    Sleep                               3
    เลือกน้ำหนักบาท
    Sleep                               3
    พบน้ำหนักกรัม                           3.800
    #พบน้ำหนัก กรัมถูกต้อง
    Sleep                               3
   
Test12 ทดสอบการ import ไม่สำเร็จเมื่อไม่มีข้อมูลแล้วกดบันทึก
    เข้าหน้าสต็อกทองใหม่
    Sleep                               3
    กดปุ่มimport
    Sleep                               3
    กดปุ่มบันทึกimport                           
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="btnCloseCSV"]
    #หน้า pop up Import CSV ยังคงอยู่
    กดปุ่มปิดimport                           
    Sleep                               3
    พบจำนวนแถว                          2
    #ไม่มีข้อมูลสต็อกเพิ่ม

Test16 ทดสอบการพิมพ์ไม่สำเร็จเมื่อกดปุ่มปิด
    เข้าหน้าสต็อกทองใหม่
    Sleep                               3
    กดปุ่มพิมพ์สต็อกทองใหม่
    Sleep                               3
    กดปุ่มปิดพิมพ์สต็อกทองใหม่                           
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="btnImport"]
    พบจำนวนแถว                          2
    #พบหน้าสต็อกทองใหม่

Test18 ทดสอบการแสดงรายสต็อกทองเก่าเมื่อกดปุ่มทั้งหมด
    เข้าหน้าสต็อกทองเก่า
    Sleep                               3
    กดปุ่มทั้งหมด
    Sleep                               3
    พบจำนวนแถวสต็อกทองเก่า                          4
    #พบรายการสต็อกทองเก่าทั้งหมด

Test19 ทดสอบการแสดงรายสต็อกทองเก่าเมื่อเลือกสาขา
    เข้าหน้าสต็อกทองเก่า
    Sleep                               3
    เลือกสาขาลาดกระบัง
    Sleep                               3
    กดปุ่มค้นหา
    Sleep                               3
    พบจำนวนแถวสต็อกทองเก่า                          2
    #พบรายการสต็อกทองใหม่เฉพาะสาขาที่เลือก

*** Keywords ***
เข้าหน้าสต็อกทองเก่า
    Wait Until Page Contains Element      id:oldStock                                                                                              300
    Click Element                         id:oldStock

กดปุ่มทั้งหมด
    Wait Until Page Contains Element      id:btnAll                                                                                                 300
    Click Element                         id:btnAll

กดปุ่มค้นหา
    Wait Until Page Contains Element      id:btnSearch                                                                                                      300
    Click Element                         id:btnSearch

กดปุ่มimport
    Wait Until Page Contains Element      id:btnImport                                                                                                      300
    Click Element                         id:btnImport

กดปุ่มบันทึกimport
    Wait Until Page Contains Element      id:btnSaveCSV                                                                                                      300
    Click Element                         id:btnSaveCSV

กดปุ่มปิดimport
    Wait Until Page Contains Element      id:btnCloseCSV                                                                                                      300
    Click Element                         id:btnCloseCSV

กดปุ่มพิมพ์สต็อกทองใหม่
    Wait Until Page Contains Element      id:btnPrint                                                                                                      300
    Click Element                         id:btnPrint

กดปุ่มปิดพิมพ์สต็อกทองใหม่
    Wait Until Page Contains Element      id:btnClose                                                                                                  300
    Click Element                         id:btnClose

เลือกสาขาลาดกระบัง
    Wait Until Element Is Visible         id:dropDownBranch                                                                                                300
    Click Element                         id:dropDownBranch
    Wait Until Element Is Visible         //*[@id="dropDownBranch"]/div[2]/div[2]                                                                 300
    Click Element                         //*[@id="dropDownBranch"]/div[2]/div[2]

เลือกกลุ่มสินค้า
    Wait Until Element Is Visible         id:dropDownGropProduct                                                                                                      300
    Click Element                         id:dropDownGropProduct
    Wait Until Element Is Visible         //*[@id="dropDownGropProduct"]/div[2]/div[3]                                                                            300
    Click Element                         //*[@id="dropDownGropProduct"]/div[2]/div[3]

เลือกรหัสสินค้า
    Wait Until Element Is Visible         id:dropDownProductID                                                                                                      300
    Click Element                         id:dropDownProductID
    Wait Until Element Is Visible         //*[@id="dropDownProductID"]/div[2]/div[2]                                                                                300
    Click Element                         //*[@id="dropDownProductID"]/div[2]/div[2]

เลือกชื่อสินค้า
    Wait Until Element Is Visible         id:dropDownProductName                                                                                                      300
    Click Element                         id:dropDownProductName
    Wait Until Element Is Visible         //*[@id="dropDownProductName"]/div[2]/div[2]                                                                              300
    Click Element                         //*[@id="dropDownProductName"]/div[2]/div[2]

พบจำนวนแถว
    [Arguments]                           ${value}
    ${count} =                            Get Element Count                                                                                                       id:rowBranch
    ${count_str} =                        Convert To String                                                                                              ${count}
    Should Be Equal                       ${count_str}                                                                                                   ${value}

พบจำนวนแถวสต็อกทองเก่า
    [Arguments]                           ${value}
    ${count} =                            Get Element Count                                                                                                       id:branch
    ${count_str} =                        Convert To String                                                                                              ${count}
    Should Be Equal                       ${count_str}                                                                                                   ${value}

เลือกประเภทสินค้า
    Wait Until Element Is Visible         id:dropDownProductType                                                                                                      300
    Click Element                         id:dropDownProductType
    Wait Until Element Is Visible         //*[@id="dropDownProductType"]/div[2]/div[2]                                                                              300
    Click Element                         //*[@id="dropDownProductType"]/div[2]/div[2]

เลือกประเภทงานขาย
    Wait Until Element Is Visible         id:dropDownSaleType                                                                                                      300
    Click Element                         id:dropDownSaleType
    Wait Until Element Is Visible         //*[@id="dropDownSaleType"]/div[2]/div[1]                                                                              300
    Click Element                         //*[@id="dropDownSaleType"]/div[2]/div[1]

เลือกน้ำหนักบาท
    Wait Until Element Is Visible         id:dropDownWeightB                                                                                                      300
    Click Element                         id:dropDownWeightB
    Wait Until Element Is Visible         //*[@id="dropDownWeightB"]/div[2]/div[2]                                                                              300
    Click Element                         //*[@id="dropDownWeightB"]/div[2]/div[2]

พบน้ำหนักกรัม
    [Arguments]                           ${value}
    ${count} =                            Get Value                                                                                                      //*[@id="inputWeightF"]
    ${count_str} =                        Convert To String                                                                                              ${count}
    Should Be Equal                       ${count_str}                                                                                                   ${value}
