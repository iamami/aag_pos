*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบAdminและinit_data
Test Setup        เข้าหน้าสต็อกทอง
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data_admin
*** Variables ***

*** Test cases ***
# ค้นหา
Test2 ทดสอบการแสดงรายการนำออกทองใหม่เมื่อเลือกสาขา
    เข้าหน้านำออกทองใหม่
    Sleep                               3
    Go To                               ${BASE_URL}stock/export?start_date=01/01/2016&end_date=04/11/2019
    Sleep                               3
    # เลือกสาขา
    Wait Until Page Contains Element    //*[@id="branch"]/i                                                                                            300
    Sleep                               3
    Click Element                       //*[@id="branch"]/i
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="branch"]/div[2]/div[2]/span                                                                           300
    Sleep                               3
    Click Element                       //*[@id="branch"]/div[2]/div[2]/span
    Sleep                               3

    Sleep                               3
    กดปุ่มค้นหารายการนำออกทองใหม่
    Sleep                               3
    ตรวจรายการนำออกทองใหม่              1

Test3 ทดสอบการแสดงรายการนำออกทองใหม่เมื่อเลือกวันที่ถึงวันที่
    เข้าหน้านำออกทองใหม่
    Sleep                               3
    Go To                               ${BASE_URL}stock/export?start_date=01/01/2016&end_date=04/11/2019
    กดปุ่มค้นหารายการนำออกทองใหม่
    Sleep                               3
    ตรวจรายการนำออกทองใหม่              3

Test4 ทดสอบการแสดงรายการนำออกทองใหม่เมื่อเลือกยังไม่ปรับปรุงสต็อก
    เข้าหน้านำออกทองใหม่
    Sleep                               3
    Go To                               ${BASE_URL}stock/export?start_date=01/01/2016&end_date=04/11/2019
    Sleep                               3
    Wait Until Page Contains Element    //*[@id="root-content"]/div/div/div[2]/div/div/form[2]/div[2]/div[1]/div/label                                 300
    Sleep                               3
    Click Element                       //*[@id="root-content"]/div/div/div[2]/div/div/form[2]/div[2]/div[1]/div/label
    Sleep                               3
    กดปุ่มค้นหารายการนำออกทองใหม่
    Sleep                               3
    ตรวจรายการนำออกทองใหม่              1

Test5 ทดสอบการแสดงรายการนำออกทองใหม่เมื่อเลือกยังไม่เคลียร์บิล
    เข้าหน้านำออกทองใหม่
    Sleep                               3
    Go To                               ${BASE_URL}stock/export?start_date=01/01/2016&end_date=04/11/2019
    Sleep                               3
    Wait Until Page Contains Element    //*[@id="root-content"]/div/div/div[2]/div/div/form[2]/div[2]/div[2]/div/label                                 300
    Sleep                               3
    Click Element                       //*[@id="root-content"]/div/div/div[2]/div/div/form[2]/div[2]/div[2]/div/label
    กดปุ่มค้นหารายการนำออกทองใหม่
    Sleep                               3
    ตรวจรายการนำออกทองใหม่              2

Test6 ทดสอบการแสดงรายการนำออกทองใหม่เมื่อเลือกสาขา/ยังไม่เคลียร์บิล
    เข้าหน้านำออกทองใหม่
    Sleep                               3
    Go To                               ${BASE_URL}stock/export?start_date=01/01/2016&end_date=04/11/2019
    Sleep                               1
    Wait Until Page Contains Element    //*[@id="branch"]/i                                                                                            300
    Sleep                               3
    Click Element                       //*[@id="branch"]/i
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="branch"]/div[2]/div[3]/span                                                                           300
    Sleep                               3
    Click Element                       //*[@id="branch"]/div[2]/div[3]/span
    Sleep                               1
    Wait Until Page Contains Element    //*[@id="root-content"]/div/div/div[2]/div/div/form[2]/div[2]/div[2]/div/label                                 300
    Sleep                               3
    Click Element                       //*[@id="root-content"]/div/div/div[2]/div/div/form[2]/div[2]/div[2]/div/label
    Sleep                               1
    กดปุ่มค้นหารายการนำออกทองใหม่
    Sleep                               3
    ตรวจรายการนำออกทองใหม่              2

# เพิ่มรายการนำออกทองใหม่
Test8 ทดสอบการไม่สามารถเพิ่มรายการสินค้าในหน้านำออกทองใหม่ได้เมื่อยังไม่สร้างรายการ
    เข้าหน้านำออกทองใหม่
    Sleep                               1
    กดเพิ่มสร้างใบนำออกทอง
    Sleep                               1
    ${value} =                          Get Element Attribute                                                                                          id:addproductimport               disabled
    Sleep                               1
    กดกากบาทสร้างใบนำออกทอง

Test9 ทดสอบการไม่สามารถสั่งพิมพ์ในหน้านำออกทองใหม่ได้เมื่อยังไม่สร้างรายการ
    เข้าหน้านำออกทองใหม่
    Sleep                               1
    กดเพิ่มสร้างใบนำออกทอง
    Sleep                               1
    ${value} =                          Get Element Attribute                                                                                          id:print                          disabled
    Sleep                               1
    กดกากบาทสร้างใบนำออกทอง

Test10 ทดสอบการไม่สามารถบันทึกและอัพเดทสต็อกในหน้านำออกทองใหม่ได้เมื่อยังไม่สร้างรายการและเพิ่มรายการสินค้า
    เข้าหน้านำออกทองใหม่
    Sleep                               1
    กดเพิ่มสร้างใบนำออกทอง
    Sleep                               1
    ${value} =                          Get Element Attribute                                                                                          //*[@id="updateimport"]           disabled
    Sleep                               1
    กดกากบาทสร้างใบนำออกทอง

Test11 ททดสอบการเพิ่มรายการนำออกทองใหม่ไม่สำเร็จเมื่อไม่เลือกสาขา
    เข้าหน้านำออกทองใหม่
    Sleep                               3
    กดเพิ่มสร้างใบนำออกทอง
    Sleep                               3
    เลือกโรงงานหน้าสร้างใบนำออกทอง
    Sleep                               3
    กดสร้างใบนำออกทอง
    Sleep                               3
    พบข้อความ                           *สาขากรุณาเลือกสาขา
    Sleep                               3
    กดกากบาทสร้างใบนำออกทอง

Test12 ทดสอบการเพิ่มรายการนำออกทองใหม่สำเร็จเมื่อกดปุ่มสร้าง
    # Maximize Browser Window
    เข้าหน้านำออกทองใหม่
    Sleep                               3
    กดเพิ่มสร้างใบนำออกทอง
    Sleep                               3
    เลือกสาขาหน้าสร้างใบนำออกทอง
    Sleep                               5
    เลือกโรงงานหน้าสร้างใบนำออกทอง
    Sleep                               3
    กดสร้างใบนำออกทอง
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="add"]                          300
    Sleep                               3
    กดกากบาทสร้างใบนำออกทอง

# แก้ไขรายการนำออกทองใหม่
Test13 ทดสอบการแสดงรายละเอียดบนหน้าแก้ไขรายการนำออกทองใหม่ถูกต้อง เมื่อกดปุ่มแก้ไขบนหน้านำออกทองใหม่
    เข้าหน้านำออกทองใหม่
    Sleep                               3
    กดแก้ไขนำออกทองใหม่
    ${branch}                           Get Text                                                                                                       //*[@id="branchimport"]/div[1]
    Should Be Equal                     ${branch}                                                                                                      ลาดกระบัง
    ${vendor}                           Get Text                                                                                                       //*[@id="vendorimport"]/div[1]
    Should Be Equal                     ${vendor}                                                                                                      vendor1
    Sleep                               3
    กดกากบาทสร้างใบนำออกทอง

Test14 ทดสอบการไม่สามารถแก้ไขชื่อโรงงาน/ร้านส่งในหน้าแก้ไขรายการนำออกทองใหม่
    เข้าหน้านำออกทองใหม่
    กดแก้ไขนำออกทองใหม่
    ${vendor} =                         Get Element Attribute                                                                                          //*[@id="vendorimport"]           disabled
    กดกากบาทสร้างใบนำออกทอง

Test15 ทดสอบการไม่สามารถแก้ไขวันที่ในหน้าแก้ไขรายการนำออกทองใหม่
    เข้าหน้านำออกทองใหม่
    กดแก้ไขนำออกทองใหม่
    ${date} =                           Get Element Attribute                                                                                          //*[@id="date"]                   disabled
    กดกากบาทสร้างใบนำออกทอง

Test16 ทดสอบการไม่สามารถแก้ไขสาขาในหน้าแก้ไขรายการนำออกทองใหม่
    เข้าหน้านำออกทองใหม่
    กดแก้ไขนำออกทองใหม่
    ${branch} =                         Get Element Attribute                                                                                          //*[@id="branchimport"]           disabled
    กดกากบาทสร้างใบนำออกทอง

Test17 ทดสอบการไม่สามารถบันทึกและอัพเดทสต็อกในหน้าแก้ไขรายการทองใหม่ได้เมื่อยังไม่เพิ่มรายการสินค้า
    เข้าหน้านำออกทองใหม่
    กดแก้ไขนำออกทองใหม่
    ${update} =                         Get Element Attribute                                                                                          //*[@id="updateimport"]           disabled
    กดกากบาทสร้างใบนำออกทอง

Test18 ทดสอบการสามารถสั่งพิมพ์ในหน้าแก้ไขรายการทองใหม่เมื่อมีรายการสินค้า
    เข้าหน้านำออกทองใหม่
    กดแก้ไขนำออกทองใหม่
    ${print} =                          Get Element Attribute                                                                                          //*[@id="print"]                  labeled
    กดกากบาทสร้างใบนำออกทอง

Test19 ทดสอบการสามารถกดปุ่มบันทึกและอัพเดทสต็อกในหน้าแก้ไขรายการทองใหม่เมื่อมีรายการสินค้า
    เข้าหน้านำออกทองใหม่
    กดแก้ไขนำออกทองใหม่
    กดเพิ่มสินค้านำออกทองใหม่
    เลือกชื่อสินค้านำออกทองใหม่
    กรอกจำนวนสินค้านำออกทองใหม่         1
    กดยืนยันเพิ่มสินค้านำออกทองใหม่
    ${update} =                         Get Element Attribute                                                                                          //*[@id="updateimport"]           labeled
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="table_w"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a                    300
    Click Element                       //*[@id="table_w"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a
    Sleep                               3
    ตรวจพบข้อความบน Alert               ยืนยันลบ
    กดกากบาทสร้างใบนำออกทอง

Test21 ทดสอบการกรอกจำนวนมากกว่าจำนวนคงเหลือไม่สำเร็จ
    เข้าหน้านำออกทองใหม่
    กดแก้ไขนำออกทองใหม่
    กดเพิ่มสินค้านำออกทองใหม่
    เลือกชื่อสินค้านำออกทองใหม่
    กรอกจำนวนสินค้านำออกทองใหม่         3000
    ${count} =                          Get Value                                                                                                      //*[@id="amountimport"]
    ${count_str} =                      Convert To String                                                                                              ${count}
    Should Be Equal                     ${count_str}                                                                                                   987
    กดปุ่มปิดเพิ่มสินค้านำออกทองใหม่
    กดกากบาทสร้างใบนำออกทอง

Test22 ทดสอบการแก้ไขรายการนำออกทองใหม่ไม่สำเร็จเมื่อปฏิเสธการยืนยันการบันทึกและอัพเดทสต๊อก
    เข้าหน้านำออกทองใหม่
    กดแก้ไขนำออกทองใหม่
    กดเพิ่มสินค้านำออกทองใหม่
    เลือกชื่อสินค้านำออกทองใหม่
    กรอกจำนวนสินค้านำออกทองใหม่         1
    กดยืนยันเพิ่มสินค้านำออกทองใหม่
    Sleep                               2
    Wait Until Element Is Visible       //*[@id="updateimport"]                                                                                        300
    Click Element                       //*[@id="updateimport"]
    Sleep                               2
    ตรวจพบข้อความบน Alertแล้วCancel     ยืนยันบันทึกและอัพเดทสต๊อก
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="table_w"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a                    300
    Click Element                       //*[@id="table_w"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a
    Sleep                               3
    ตรวจพบข้อความบน Alert               ยืนยันลบ
    กดกากบาทสร้างใบนำออกทอง

Test23 ทดสอบการแก้ไขรายการนำออกทองใหม่สำเร็จเมื่อกดปุ่มบันทึก
    เข้าหน้านำออกทองใหม่
    กดแก้ไขนำออกทองใหม่
    กดเพิ่มสินค้านำออกทองใหม่
    เลือกชื่อสินค้านำออกทองใหม่
    กรอกจำนวนสินค้านำออกทองใหม่         1
    กดยืนยันเพิ่มสินค้านำออกทองใหม่
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="table_w"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a                    300
    Click Element                       //*[@id="table_w"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a
    Sleep                               3
    ตรวจพบข้อความบน Alert               ยืนยันลบ
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="save"]                                                                                                300
    Click Element                       //*[@id="save"]
    พบข้อความ                           ยังไม่อัพเดทสต๊อก
    กดกากบาทสร้างใบนำออกทอง

Test24 ทดสอบการแก้ไขรายการนำออกทองใหม่สำเร็จเมื่อตกลงการยืนยันการบันทึกและอัพเดทสต๊อก
    เข้าหน้านำออกทองใหม่
    กดแก้ไขนำออกทองใหม่
    กดเพิ่มสินค้านำออกทองใหม่
    เลือกชื่อสินค้านำออกทองใหม่
    กรอกจำนวนสินค้านำออกทองใหม่         1
    กดยืนยันเพิ่มสินค้านำออกทองใหม่
    กดบันทึกและอัพเดทสต็อก
    ตรวจพบข้อความบน Alert               ยืนยันบันทึกและอัพเดทสต๊อก
    พบข้อความ                           อัพเดทสต๊อกแล้ว
    กดกากบาทสร้างใบนำออกทอง


Test25 ทดสอบการลบรายการสินค้าที่จะนำออกทองใหม่ไม่สำเร็จเมื่อกดปฏิเสธยืนยันการลบรายการที่ยังไม่อัพเดทสต็อก
    เข้าหน้านำออกทองใหม่
    กดเพิ่มสร้างใบนำออกทอง
    เลือกสาขาหน้าสร้างใบนำออกทอง
    เลือกโรงงานหน้าสร้างใบนำออกทอง
    กดสร้างใบนำออกทอง
    กดเพิ่มสินค้านำออกทองใหม่
    เลือกชื่อสินค้านำออกทองใหม่
    กรอกจำนวนสินค้านำออกทองใหม่        1
    กดยืนยันเพิ่มสินค้านำออกทองใหม่
    Sleep                               2
    Wait Until Element Is Visible       //*[@id="0_Delete"]                                                                                            300
    Sleep                               2
    Click Element                       //*[@id="0_Delete"]
    Sleep                               3
    ตรวจพบข้อความบน Alertแล้วCancel     ยืนยันลบ
    Sleep                               3
    Wait Until Element Is Visible       id:add
    ${value} =                          Get Element Attribute                                                                                          id:add                            disabled
    Should Be Equal As Strings          ${value}                                                                                                       None
    #ปุ่มเพิ่มรายการสินค้าสามารถกดได้
    Sleep                               3
    Wait Until Element Is Visible       id:updateimport
    ${value} =                          Get Element Attribute                                                                                          id:updateimport                   disabled
    Should Be Equal As Strings          ${value}                                                                                                       None
    #ไม่สามารถกดปุ่มบันทึกและอัพเดทสต็อกได้
    Sleep                               5
    กดกากบาทสร้างใบนำออกทอง
    Sleep                               3

Test26 ทดสอบการลบรายการสินค้าที่จะนำออกทองใหม่สำเร็จเมื่อกดยืนยันยืนยันการลบรายการที่ยังไม่อัพเดทสต็อก
    เข้าหน้านำออกทองใหม่
    Sleep                               2
    Wait Until Element Is Visible       //*[@id="1_Edit"]                                                                                              300
    Sleep                               2
    Click Element                       //*[@id="1_Edit"]
    Sleep                               2
    Wait Until Element Is Visible       //*[@id="0_Delete"]                                                                                            300
    Sleep                               2
    Click Element                       //*[@id="0_Delete"]
    Sleep                               2
    ตรวจพบข้อความบน Alert               ยืนยันลบ
    Sleep                               3
    Wait Until Element Is Visible       id:add
    ${value} =                          Get Element Attribute                                                                                          id:add                            disabled
    Should Be Equal As Strings          ${value}                                                                                                       None
    #ปุ่มเพิ่มรายการสินค้าสามารถกดได้
    Sleep                               3
    Wait Until Element Is Visible       id:updateimport
    ${value} =                          Get Element Attribute                                                                                          id:updateimport                   disabled
    Should Be Equal As Strings          ${value}                                                                                                       true
    #ไม่สามารถกดปุ่มบันทึกและอัพเดทสต็อกได้
    Sleep                               3
    กดกากบาทสร้างใบนำออกทอง
    Sleep                               3

Test27 ทดสอบการลบรายการสินค้าที่จะนำออกทองใหม่ไม่สำเร็จเมื่อกดลบรายการที่อัพเดทสต็อกไปแล้ว
    เข้าหน้านำออกทองใหม่
    Sleep                               2
    Wait Until Element Is Visible       //*[@id="0_Edit"]                                                                                              300
    Click Element                       //*[@id="0_Edit"]
    Sleep                               2
    Wait Until Element Is Visible       //*[@id="0_Delete"]                                                                                            300
    Click Element                       //*[@id="0_Delete"]
    Sleep                               3
    ตรวจพบข้อความบน Alert               ไม่สามารถลบรายการนี้ได้
    Sleep                               3
    Wait Until Element Is Visible       id:add
    ${value} =                          Get Element Attribute                                                                                          id:add                            disabled
    Should Be Equal As Strings          ${value}                                                                                                       true
    #ปุ่มเพิ่มรายการสินค้าไม่สามารถกดได้
    Sleep                               3
    Wait Until Element Is Visible       id:updateimport
    ${value} =                          Get Element Attribute                                                                                          id:updateimport                   disabled
    Should Be Equal As Strings          ${value}                                                                                                       true
    #ไม่สามารถกดปุ่มบันทึกและอัพเดทสต็อกได้
    Sleep                               3
    กดกากบาทสร้างใบนำออกทอง
    Sleep                               3

Test29 ทดสอบการเพิ่มสินค้าไม่สำเร็จเมื่อไม่เลือกชื่อสินค้า
    เข้าหน้านำออกทองใหม่
    Sleep                               2
    Wait Until Element Is Visible       //*[@id="1_Edit"]                                                                                              300
    Click Element                       //*[@id="1_Edit"]
    กดเพิ่มสินค้านำออกทองใหม่
    กรอกจำนวนสินค้านำออกทองใหม่         1
    กดยืนยันเพิ่มสินค้านำออกทองใหม่
    Sleep                               3
    พบข้อความ                           *ชื่อสินค้า กรุณาเลือกสินค้า
    พบข้อความ                           น.น.(กรัม) กรุณาระบุ น.น.(กรัม) ให้ถูกต้อง
    #พบข้อความ *ชื่อสินค้า กรุณาเลือกชื่อสินค้า
    Sleep                               3
    กดปุ่มปิดเพิ่มสินค้านำออกทองใหม่
    กดกากบาทสร้างใบนำออกทอง

Test35 ทดสอบการแสดงจำนวนสินค้าถูกต้องเมื่อเลือกชื่อสินค้า
    เข้าหน้านำออกทองใหม่
    Sleep                               2
    Wait Until Element Is Visible       //*[@id="1_Edit"]                                                                                              300
    Click Element                       //*[@id="1_Edit"]
    กดเพิ่มสินค้านำออกทองใหม่
    เลือกชื่อสินค้านำออกทองใหม่
    พบข้อความ                           คงเหลือ 986ชิ้น  
    กดปุ่มปิดเพิ่มสินค้านำออกทองใหม่
    กดกากบาทสร้างใบนำออกทอง
    #พบจำนวนสินค้าถูกต้องตามสต็อก

Test36 ทดสอบการไม่สามารถระบุจำนวนสินค้าที่ต้องการนำออกเกินจำนวนคงเหลือ
    เข้าหน้านำออกทองใหม่
    Sleep                               2
    Wait Until Element Is Visible       //*[@id="1_Edit"]                                                                                              300
    Click Element                       //*[@id="1_Edit"]
    กดเพิ่มสินค้านำออกทองใหม่
    Sleep                               3
    เลือกชื่อสินค้านำออกทองใหม่
    Sleep                               5
    พบข้อความ                           คงเหลือ 986ชิ้น  
    กรอกจำนวนสินค้านำออกทองใหม่         3000
    Sleep                               3
    พบจำนวนสินค้า                       986
    กดปุ่มปิดเพิ่มสินค้านำออกทองใหม่
    กดกากบาทสร้างใบนำออกทอง
    #พบจำนวนสินค้าถูกต้องตามสต็อก

Test37 ทดสอบการแสดงน.น.(กรัม)ถูกต้องเมื่อเลือกชื่อสินค้า
    เข้าหน้านำออกทองใหม่
    Sleep                               2
    Wait Until Element Is Visible       //*[@id="1_Edit"]                                                                                              300
    Click Element                       //*[@id="1_Edit"]
    กดเพิ่มสินค้านำออกทองใหม่
    เลือกชื่อสินค้านำออกทองใหม่
    Sleep                               3
    พบน.น.(กรัม)                        9.200
    #น.น.(กรัม)ถูกต้อง
    กดปุ่มปิดเพิ่มสินค้านำออกทองใหม่
    กดกากบาทสร้างใบนำออกทอง

Test38 ทดสอบการแสดงน.น.รวม(กรัม)/น.น.ชั่ง(กรัม)ถูกต้องเมื่อเลือกชื่อสินค้าแล้วกรอกจำนวน
    เข้าหน้านำออกทองใหม่
    Sleep                               2
    Wait Until Element Is Visible       //*[@id="1_Edit"]                                                                                              300
    Click Element                       //*[@id="1_Edit"]
    กดเพิ่มสินค้านำออกทองใหม่
    เลือกชื่อสินค้านำออกทองใหม่
    กรอกจำนวนสินค้านำออกทองใหม่         2
    Sleep                               3
    พบน.น.รวม(กรัม)                     18.400
    พบ*น.น.ชั่ง(กรัม)                   18.400
    #น.น.รวม(กรัม)/น.น.ชั่ง(กรัม)ถูกต้อง
    กดปุ่มปิดเพิ่มสินค้านำออกทองใหม่
    กดกากบาทสร้างใบนำออกทอง

Test39 ทดสอบการเพิ่มรายการสินค้าสำเร็จเมื่อกรอกข้อมูลครบ
    เข้าหน้านำออกทองใหม่
    Sleep                               2
    Wait Until Element Is Visible       //*[@id="1_Edit"]                                                                                              300
    Click Element                       //*[@id="1_Edit"]
    กดเพิ่มสินค้านำออกทองใหม่
    Sleep                               3
    เลือกชื่อสินค้านำออกทองใหม่
    Sleep                               3
    กรอกจำนวนสินค้านำออกทองใหม่         1
    Sleep                               3
    กดยืนยันเพิ่มสินค้านำออกทองใหม่
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="0_Delete"]
    #พบรายการสินค้าบนหน้าแก้ไขรายการทองใหม่
    Sleep                               3
    กดกากบาทสร้างใบนำออกทอง

Test41 ทดสอบการคำนวณในหน้าแก้ไขรายการนำออกทองใหม่
    เข้าหน้านำออกทองใหม่
    Sleep                               2
    Wait Until Element Is Visible       //*[@id="1_Edit"]                                                                                              300
    Click Element                       //*[@id="1_Edit"]
    Sleep                               3
    พบจำนวนรายการ                       1
    พบค่าแรงขายปลีกรวม                  0.00
    พบน้ำหนักรวม                        9.200
    พบจำนวนรวม                          1
    กดกากบาทสร้างใบนำออกทอง

Test40.1 ทดสอบการพิมพ์รายการนำออกทองใหม่ไม่สำเร็จเมื่อกดปุ่มปิด
    เข้าหน้านำออกทองใหม่
    Sleep                               2
    Wait Until Element Is Visible       //*[@id="0_Edit"]                                                                                              300
    Click Element                       //*[@id="0_Edit"]
    Sleep                               3
    กดปุ่มพิมพ์นำออกทองใหม่
    Sleep                               3
    กดปุ่มปิดพิมพ์นำออกทองใหม่
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="addproductimport"]
    พบจำนวนรายการ                       1
    #พบหน้านำออกทองใหม่
    Sleep                               3
    กดกากบาทสร้างใบนำออกทอง

*** Keywords ***
เข้าหน้านำออกทองใหม่
    Sleep                               2
    Wait Until Page Contains Element    //*[@id="exportStock"]                                                                                         300
    Click Element                       //*[@id="exportStock"]

ตรวจรายการนำออกทองใหม่
    [Arguments]                         ${value}
    Sleep                               2
    ${count}                            Get Value                                                                                                      //*[@id="amount"]
    Should Be Equal                     ${count}                                                                                                       ${value}

กดปุ่มค้นหารายการนำออกทองใหม่
    Sleep                               1
    Wait Until Page Contains Element    //*[@id="search"]                                                                                              300
    Click Element                       //*[@id="search"]

กดเพิ่มสร้างใบนำออกทอง
    Sleep                               2
    Wait Until Page Contains Element    //*[@id="addproductimport"]                                                                                    300
    Click Element                       //*[@id="addproductimport"]

กดกากบาทสร้างใบนำออกทอง
    Sleep                               2
    Wait Until Element Is Visible       //*[@id="btnCloseModalImEx"]                                                                                       300
    Sleep                               2
    Click Element                       //*[@id="btnCloseModalImEx"]

กดสร้างใบนำออกทอง
    Sleep                               2
    Wait Until Element Is Visible    //*[@id="save"]                                                                                                300
    Click Element                       //*[@id="save"]

เลือกสาขาหน้าสร้างใบนำออกทอง
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="branchimport"]/i                                                                                  300
    Click Element                       //*[@id="branchimport"]/i 
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="branchimport"]/div[2]/div[1]                                                                    300
    Click Element                       //*[@id="branchimport"]/div[2]/div[1]

เลือกโรงงานหน้าสร้างใบนำออกทอง
    Sleep                               2
    Wait Until Element Is Visible       //*[@id="vendorimport"]/i                                                                                 300
    Click Element                       //*[@id="vendorimport"]/i
    Sleep                               5
    Wait Until Element Is Visible       //*[@id="vendorimport"]/div[2]/div[2]                                                                   300
    Double Click Element                       //*[@id="vendorimport"]/div[2]/div[2]

กดแก้ไขนำออกทองใหม่
    Sleep                               2
    Wait Until Element Is Visible       //*[@id="0_Edit"]                300
    Click Element                       //*[@id="0_Edit"]


กดเพิ่มสินค้านำออกทองใหม่
    Sleep                               2
    Wait Until Element Is Visible       //*[@id="add"]                                                                                                 300
    Click Element                       //*[@id="add"]

เลือกชื่อสินค้านำออกทองใหม่
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="productimport"]/i                                                                                     300
    Click Element                       //*[@id="productimport"]/i
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="productimport"]/div[2]/div/span                                                                       300
    Click Element                       //*[@id="productimport"]/div[2]/div/span

กรอกจำนวนสินค้านำออกทองใหม่
    [Arguments]                         ${value}
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="amountimport"]                                                                                        300
    Input Text                          //*[@id="amountimport"]                                                                                        ${value}

กรอกน.น.ชั่ง(กรัม)สินค้านำออกทองใหม่
    [Arguments]                         ${value}
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="weightrealimport"]                                                                                    300
    Input Text                          //*[@id="weightrealimport"]                                                                                    ${value}

กดยืนยันเพิ่มสินค้านำออกทองใหม่
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="addimport"]                                                                                           300
    Click Element                       //*[@id="addimport"]

กดปุ่มบันทึก
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="save"]                                                                                                300
    Click Element                       //*[@id="save"]

กดบันทึกและอัพเดทสต็อก
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="updateimport"]                                                                                        300
    Click Element                       //*[@id="updateimport"]

พบน.น.(กรัม)
    [Arguments]                         ${value}
    ${count} =                          Get Value                                                                                                      //*[@id="weightimport"]
    ${count_str} =                      Convert To String                                                                                              ${count}
    Should Be Equal                     ${count_str}                                                                                                   ${value}

พบน.น.รวม(กรัม)
    [Arguments]                         ${value}
    ${count} =                          Get Value                                                                                                      //*[@id="weighttotalimport"]
    ${count_str} =                      Convert To String                                                                                              ${count}
    Should Be Equal                     ${count_str}                                                                                                   ${value}

พบจำนวนสินค้า
    [Arguments]                         ${value}
    ${count} =                          Get Value                                                                                                      //*[@id="amountimport"]
    ${count_str} =                      Convert To String                                                                                              ${count}
    Should Be Equal                     ${count_str}                                                                                                   ${value}

พบ*น.น.ชั่ง(กรัม)
    [Arguments]                         ${value}
    ${count} =                          Get Value                                                                                                      //*[@id="weightrealimport"]
    ${count_str} =                      Convert To String                                                                                              ${count}
    Should Be Equal                     ${count_str}                                                                                                   ${value}

พบจำนวนรายการ
    [Arguments]                         ${value}
    ${count} =                          Get Value                                                                                                      //*[@id="invoiceitem"]
    ${count_str} =                      Convert To String                                                                                              ${count}
    Should Be Equal                     ${count_str}                                                                                                   ${value}

พบค่าแรงขายปลีกรวม
    [Arguments]                         ${value}
    ${count} =                          Get Value                                                                                                      //*[@id="invoiceprofit"]
    ${count_str} =                      Convert To String                                                                                              ${count}
    Should Be Equal                     ${count_str}                                                                                                   ${value}

พบน้ำหนักรวม
    [Arguments]                         ${value}
    ${count} =                          Get Value                                                                                                      //*[@id="weighttotal"]
    ${count_str} =                      Convert To String                                                                                              ${count}
    Should Be Equal                     ${count_str}                                                                                                   ${value}

พบจำนวนรวม
    [Arguments]                         ${value}
    ${count} =                          Get Value                                                                                                      //*[@id="producttotal"]
    ${count_str} =                      Convert To String                                                                                              ${count}
    Should Be Equal                     ${count_str}                                                                                                   ${value}

กดปุ่มปิดพิมพ์นำออกทองใหม่
    Sleep                               1
    Wait Until Element Is Visible       id:btnClosePreview                                                                                             300
    Click Element                       id:btnClosePreview

กดปุ่มพิมพ์นำออกทองใหม่
    Sleep                               1
    Wait Until Element Is Visible       id:print                                                                                                       300
    Click Element                       id:print

กดปุ่มปิดเพิ่มสินค้านำออกทองใหม่
    Sleep                               1
    Wait Until Element Is Visible       id:btnCloseProduct                                                                                             300
    Click Element                       id:btnCloseProduct

กดปุ่มเคลียร์บิล
    Sleep                               1
    Wait Until Element Is Visible       id:clearbillimport                                                                                             300
    Click Element                       id:clearbillimport
