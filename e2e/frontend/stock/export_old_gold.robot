*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบAdminและinit_data
Test Setup        เข้าหน้าสต็อกทอง
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data_admin
*** Variables ***

*** Test cases ***
Test1 ทดสอบการแสดงรายการนำออกทองเก่าเมื่อกดปุ่มวันนี้
    เข้าหน้านำออกทองเก่า
    Sleep                                 3
    กดปุ่มสร้างใบนำออกทองเก่า
    Sleep                                 3
    เลือกสาขาลาดกระบังเพื่อสร้าง
    Sleep                                 3
    เลือกโรงงานเพื่อสร้าง
    Sleep                                 3
    กดปุ่มสร้าง
    Sleep                               2
    พบข้อความ                             แก้ไขรายการนำออกทองเก่า
    พบข้อความ                             ยังไม่อัพเดทสต๊อก
    #พบpopup แก้ไขรายการนำออกทองเก่า
    กดปุ่มปิดนำออกทองเก่า
    Sleep                                 3
    กดปุ่มวันนี้
    Sleep                                 3
    พบจำนวนแถว                            1
    Sleep                               2
    พบข้อความ                            OC-01191100001
    #พบรายการนำออกทองเก่าวันนี้

Test2 ทดสอบการแสดงรายการนำออกทองเก่าเมื่อเลือกสาขา
    เข้าหน้านำออกทองเก่า
    Sleep                               3
    Go To                               ${BASE_URL}stock/export_old?start_date=01/01/2016&end_date=04/11/2019
    Sleep                                 3
    เลือกสาขาลาดกระบัง
    Sleep                                   3
    กดปุ่มค้นหา
    Sleep                                 3
    พบจำนวนแถว                            3
    #พบรายการนำออกทองเก่าเฉพาะสาขา

Test3 ทดสอบการแสดงรายการนำออกทองเก่าเมื่อเลือกวันที่ถึงวันที่
    เข้าหน้านำออกทองเก่า
    Sleep                               3
    Go To                               ${BASE_URL}stock/export_old?start_date=01/01/2016&end_date=04/11/2019
    Sleep                                 3
    กดปุ่มค้นหา
    Sleep                                 3
    พบจำนวนแถว                            4
    # พบรายการนำออกทองเก่าเฉพาะระยะเวลาที่กำหนด

Test4 ทดสอบการแสดงรายการนำออกทองเก่าเมื่อเลือกยังไม่ปรับปรุงสต็อก
    เข้าหน้านำออกทองเก่า
    Sleep                               3
    Go To                               ${BASE_URL}stock/export_old?start_date=01/01/2016&end_date=04/11/2019
    Sleep                                 3
    เลือกยังไม่ปรับปรุงสต็อก
    Sleep                                 3
    กดปุ่มค้นหา
    Sleep                                 3
    พบจำนวนแถว                            2
    #พบรายการนำออกทองเก่าเฉพาะที่ยังไม่ปรับปรุงสต็อก
    ไม่พบวันที่ปรับปรุงสต็อก
    #ไม่พบวันที่ปรับปรุงสต็อก

Test5 ทดสอบการแสดงรายการนำออกทองเก่าเมื่อเลือกยังไม่เคลียร์บิล
    เข้าหน้านำออกทองเก่า
    Sleep                               3
    Go To                               ${BASE_URL}stock/export_old?start_date=01/01/2016&end_date=04/11/2019
    Sleep                                 3
    เลือกยังไม่เคลียร์บิล
    Sleep                                 3
    กดปุ่มค้นหา
    Sleep                                 3
    พบจำนวนแถว                            3
    # พบรายการนำออกทองเก่าเฉพาะที่ยังไม่เคลียร์บิล

Test6 ทดสอบการแสดงรายการนำออกทองเก่าเมื่อเลือกสาขา/วันที่/ยังไม่เคลียร์บิล
    เข้าหน้านำออกทองเก่า
    Sleep                               3
    Go To                               ${BASE_URL}stock/export_old?start_date=01/01/2016&end_date=04/11/2019
    Sleep                                 3
    เลือกสาขาลาดกระบัง
    Sleep                                 3
    เลือกยังไม่เคลียร์บิล
    Sleep                                 3
    กดปุ่มค้นหา
    Sleep                                 3
    พบจำนวนแถว                            2
    #พบรายการที่ต้องการค้นหา

Test9 ทดสอบการไม่สามารถเพิ่มรายการสินค้าในหน้านำออกทองเก่าได้เมื่อยังไม่สร้างรายการ
    เข้าหน้านำออกทองเก่า
    Sleep                                 3
    กดปุ่มสร้างใบนำออกทองเก่า
    Wait Until Element Is Visible         id:add                                                                                                         300
    ${value} =                            Get Element Attribute                                                                                          id:add                    disabled
    Should Be Equal As Strings            ${value}                                                                                                       true
    Sleep                               5
    กดปุ่มปิดนำออกทองเก่า
    #ไม่สามารถกดปุ่มเพิ่มรายการสินค้าได้

Test10 ทดสอบการไม่สามารถบันทึกและอัพเดทสต็อกในหน้านำออกทองเก่าได้เมื่อยังไม่สร้างรายการและเพิ่มรายการสินค้า
    เข้าหน้านำออกทองเก่า
    Sleep                                 3
    กดปุ่มสร้างใบนำออกทองเก่า
    Sleep                                 3
    Wait Until Element Is Visible         //*[@id="btnUpdate"]                                                                                           300
    ${value} =                            Get Element Attribute                                                                                          //*[@id="btnUpdate"]      disabled
    Should Be Equal As Strings            ${value}                                                                                                       true
    
    กดปุ่มปิดนำออกทองเก่า
    #ไม่สามารถกดปุ่มบันทึกและอัพเดทสต็อกได้

Test11 ทดสอบการเพิ่มรายการนำออกทองเก่าไม่สำเร็จเมื่อไม่เลือกชื่อโรงงาน/ร้านส่ง
    เข้าหน้านำออกทองเก่า
    Sleep                                 3
    กดปุ่มสร้างใบนำออกทองเก่า
    Sleep                                 3
    เลือกสาขาลาดกระบังเพื่อสร้าง
    Sleep                                 3
    กดปุ่มสร้าง
    Sleep                                 3
    ตรวจพบข้อความบน Alert                 กรุณาเลือกโรงงาน/ร้านส่ง
    #พบ alert กรุณาเลือกโรงงาน/ร้านส่ง
    กดปุ่มปิดนำออกทองเก่า
    Sleep                                 3
    พบจำนวนแถว                            1

Test12 ทดสอบการเพิ่มรายการนำออกทองเก่าสำเร็จเมื่อกดปุ่มสร้าง
    เข้าหน้านำออกทองเก่า
    Sleep                                 3
    กดปุ่มสร้างใบนำออกทองเก่า
    Sleep                                 3
    เลือกสาขาลาดกระบังเพื่อสร้าง
    Sleep                                 3
    เลือกโรงงานเพื่อสร้าง
    Sleep                                 3
    กดปุ่มสร้าง
    Sleep                                 3
    พบข้อความ                             แก้ไขรายการนำออกทองเก่า
    # พบpopup แก้ไขรายการนำออกทองเก่า
    พบข้อความ                             ยังไม่อัพเดทสต๊อก
    กดปุ่มปิดนำออกทองเก่า
    Sleep                                 3
    พบจำนวนแถว                            2
    # พบรายการนำทองเก่าออกล่าสุด
    # พบicon yellow warning

Test13 ทดสอบการแสดงรายละเอียดบนหน้าแก้ไขรายการนำออกทองเก่าถูกต้อง เมื่อกดปุ่มแก้ไขบนหน้านำออกทองเก่า
    เข้าหน้านำออกทองเก่า
    Sleep                                 3
    กดปุ่มแก้ไขใบนำออกทองเก่า
    Sleep                                 3
    พบสาขา                                ลาดกระบัง
    พบโรงงาน                              vendor1
    พบเลขที่บิล                           [บิลเลขที่ OC-01191100001]
    #ข้อมูลจากสาขา/ไปสาขา/วันที่ถูกต้อง
    กดปุ่มปิดนำออกทองเก่า

Test17 ทดสอบการไม่สามารถบันทึกและอัพเดทสต็อกบนหน้าแก้ไขรายการนำออกทองเก่าได้เมื่อยังไม่เพิ่มรายการสินค้า
    เข้าหน้านำออกทองเก่า
    Sleep                                 3
    กดปุ่มแก้ไขใบนำออกทองเก่า
    Sleep                                 3
    Wait Until Element Is Visible         id:btnUpdate
    ${value} =                            Get Element Attribute                                                                                          id:btnUpdate              disabled
    Should Be Equal As Strings            ${value}                                                                                                       true
    # ไม่สามารถกดปุ่มบันทึกและอัพเดทสต็อกได้
    กดปุ่มปิดนำออกทองเก่า


Test18 ทดสอบการสามารถกดปุ่มบันทึกและอัพเดทสต็อกบนหน้าแก้ไขรายการนำออกทองเก่าเมื่อมีรายการสินค้า
    เข้าหน้านำออกทองเก่า
    Sleep                                 3
    กดปุ่มแก้ไขใบนำออกทองเก่า
    Sleep                                 3
    กดปุ่มเพิ่มรายการนำออกทองเก่า
    Sleep                                 3
    เลือกเปอร์เซ็นทองแท่ง 96.5
    Sleep                                 3
    กรอกน้ำหนักกรัม                       15.2
    Sleep                                 3
    กรอกราคาตัด                           25000
    Sleep                                 3
    กรอกราคาหลอม                          300
    Sleep                                 3
    กดปุ่มเพิ่มสินค้าเพื่อรายการสินค้า
    Sleep                                 3
    Wait Until Element Is Visible         id:btnUpdate
    ${value} =                            Get Element Attribute                                                                                          id:btnUpdate              disabled
    Should Be Equal As Strings            ${value}                                                                                                       None
    #สามารถกดปุ่มบันทึกและอัพเดทสต็อกได้
    กดปุ่มปิดนำออกทองเก่า

Test20 ทดสอบการแก้ไขรายการนำออกทองเก่าสำเร็จเมื่อกดปุ่มบันทึก
    เข้าหน้านำออกทองเก่า
    Sleep                                 3
    กดปุ่มแก้ไขใบนำออกทองเก่า2
    Sleep                                 3
    # พบ popup แก้ไขรายการทองใหม่เข้าสต๊อก
    กดปุ่มเพิ่มรายการนำออกทองเก่า
    Sleep                                 3
    เลือกเปอร์เซ็นทองแท่ง 96.5
    Sleep                                 3
    กรอกน้ำหนักกรัม                       15.2
    Sleep                                 3
    กรอกราคาตัด                           25000
    Sleep                                 3
    กรอกราคาหลอม                          300
    Sleep                                 3
    กดปุ่มเพิ่มสินค้าเพื่อรายการสินค้า
    Sleep                                 3
    กดปุ่มบันทึก
    พบข้อความ                             ยังไม่อัพเดทสต๊อก
    # พบข้อความยังไม่อัพเดทสต๊อก
    Sleep                                 3
    Wait Until Element Is Visible         id:add
    ${value} =                            Get Element Attribute                                                                                          id:add                    disabled
    Should Be Equal As Strings            ${value}                                                                                                       None
    #ปุ่มเพิ่มรายการสินค้าสามารถกดได้
    Sleep                                 3
    Wait Until Element Is Visible         id:btnUpdate
    ${value} =                            Get Element Attribute                                                                                          id:btnUpdate              disabled
    Should Be Equal As Strings            ${value}                                                                                                       None
    #สามารถกดปุ่มบันทึกและอัพเดทสต็อกได้
    กดปุ่มปิดนำออกทองเก่า

Test21 ทดสอบการชำระเงินรายการนำออกทองเก่าไม่สำเร็จ เมื่อปฏิเสธการยืนยันชำระเงิน
    เข้าหน้านำออกทองเก่า
    Sleep                                 3
    กดปุ่มแก้ไขใบนำออกทองเก่า
    Sleep                                 3
    กดปุ่มบันทึกและอัพเดทสต็อก
    Sleep                                 3
    กรอกจำนวนรับเงินสด                    24829
    กดบันทึกชำระเงิน
    ตรวจพบข้อความบน Alertแล้วCancel       ยืนยันการชำระเงิน
    #พบ popup  ชำระเงิน
    กดปุ่มปิดชำระเงิน
    Sleep                                 3
    กดปุ่มปิดนำออกทองเก่า

Test22 ทดสอบการชำระเงินรายการนำออกทองเก่าสำเร็จ เมื่อยืนยันการยืนยันชำระเงิน
    เข้าหน้านำออกทองเก่า
    Sleep                                 3
    กดปุ่มแก้ไขใบนำออกทองเก่า2
    Sleep                                 3
    กดปุ่มบันทึกและอัพเดทสต็อก
    Sleep                                 3
    กรอกจำนวนรับเงินสด                    24829
    กดบันทึกชำระเงิน
    ตรวจพบข้อความบน Alert                 ยืนยันการชำระเงิน
    Sleep                                 3
    ตรวจพบข้อความบน Alertแล้วCancel       ยืนยันบันทึกและอัพเดทสต็อก
    Sleep                                 3
    #พบ alert ยืนยันบันทึกและอัพเดทสต็อก
    กดปุ่มปิดชำระเงิน
    Sleep                                 3
    กดปุ่มปิดนำออกทองเก่า

Test23 ทดสอบการบันทึกและอัพเดทสต็อกรายการนำทองเก่าออกไม่สำเร็จเมื่อปฏิเสธการยืนยันการชำระเงิน
    เข้าหน้านำออกทองเก่า
    Sleep                                 3
    กดปุ่มแก้ไขใบนำออกทองเก่า
    Sleep                                 3
    กดปุ่มบันทึกและอัพเดทสต็อก
    Sleep                                 3
    กรอกจำนวนรับเงินสด                    24829
    กดบันทึกชำระเงิน
    ตรวจพบข้อความบน Alertแล้วCancel       ยืนยันการชำระเงิน
    Sleep                                 3
    Wait Until Element Is Visible         //*[@id="headerModalPayment"]
    Wait Until Element Is Visible         //*[@id="btnSave"]
    #พบ popup  ชำระเงิน
    กดปุ่มปิดชำระเงิน
    Sleep                                 3
    กดปุ่มปิดนำออกทองเก่า

Test25 ทดสอบการบันทึกและอัพเดทสต็อกรายการนำทองเก่าออกไม่สำเร็จเมื่อปฏิเสธการยืนยันการบันทึกและอัพเดทสต๊อก
    เข้าหน้านำออกทองเก่า
    Sleep                                 3
    กดปุ่มแก้ไขใบนำออกทองเก่า
    Sleep                                 3
    กดปุ่มบันทึกและอัพเดทสต็อก
    Sleep                                 3
    กรอกจำนวนรับเงินสด                    24829
    กดบันทึกชำระเงิน
    ตรวจพบข้อความบน Alert                 ยืนยันการชำระเงิน
    Sleep                                 3
    ตรวจพบข้อความบน Alertแล้วCancel       ยืนยันบันทึกและอัพเดทสต็อก
    Sleep                                 3
    Wait Until Element Is Visible         //*[@id="headerModalPayment"]
    Wait Until Element Is Visible         //*[@id="btnSave"]
    #พบ popup  ชำระเงิน
    กดปุ่มปิดชำระเงิน
    Sleep                                 3
    กดปุ่มปิดนำออกทองเก่า

Test26 ทดสอบการบันทึกและอัพเดทสต็อกรายการนำทองเก่าออกสำเร็จเมื่อตกลงการยืนยันการบันทึกและอัพเดทสต๊อก
    เข้าหน้านำออกทองเก่า
    Sleep                                 3
    กดปุ่มแก้ไขใบนำออกทองเก่า
    Sleep                                 3
    # พบ popup แก้ไขรายการทองใหม่เข้าสต๊อก
    กดปุ่มบันทึกและอัพเดทสต็อก
    Sleep                                 3
    กรอกจำนวนรับเงินสด                    24829
    กดบันทึกชำระเงิน
    ตรวจพบข้อความบน Alert                 ยืนยันการชำระเงิน
    Sleep                                 3
    ตรวจพบข้อความบน Alert                 ยืนยันบันทึกและอัพเดทสต็อก
    Sleep                                 3
    พบข้อความ                             อัพเดทสต๊อกแล้ว
    #พบข้อความ อัพเดทสต็อกแล้ว
    Sleep                                 3
    Wait Until Element Is Visible         id:add
    ${value} =                            Get Element Attribute                                                                                          id:add                    disabled
    Should Be Equal As Strings            ${value}                                                                                                       true
    #ปุ่มเพิ่มรายการสินค้าไม่สามารถกดได้
    Sleep                                 3
    Wait Until Element Is Visible         id:btnUpdate
    ${value} =                            Get Element Attribute                                                                                          id:btnUpdate              disabled
    Should Be Equal As Strings            ${value}                                                                                                       true
    #ปุ่มบันทึกและอัพเดทสต็อกไม่สามารถกดได้
    Sleep                                 3
    กดปุ่มปิดนำออกทองเก่า

Test27 ทดสอบการลบรายการทองเก่าที่จะนำออกไม่สำเร็จเมื่อกดปฏิเสธยืนยันการลบรายการที่ยังไม่อัพเดทสต็อก
    เข้าหน้านำออกทองเก่า
    Sleep                                 3
    กดปุ่มสร้างใบนำออกทองเก่า
    Sleep                                 3
    เลือกสาขาลาดกระบังเพื่อสร้าง
    Sleep                                 3
    เลือกโรงงานเพื่อสร้าง
    Sleep                                 3
    กดปุ่มสร้าง
    Sleep                                 3
    กดปุ่มเพิ่มรายการนำออกทองเก่า
    Sleep                                 3
    เลือกเปอร์เซ็นทองแท่ง 96.5
    Sleep                                 3
    กรอกน้ำหนักกรัม                       15.2
    Sleep                                 3
    กรอกราคาตัด                           25000
    Sleep                                 3
    กรอกราคาหลอม                          300
    Sleep                                 3
    กดปุ่มเพิ่มสินค้าเพื่อรายการสินค้า
    Sleep                                 3
    กดปุ่มลบสินค้า
    ตรวจพบข้อความบน Alertแล้วCancel       ยืนยันลบรายการนี้
    Sleep                                 3
    Wait Until Element Is Visible         id:add
    ${value} =                            Get Element Attribute                                                                                          id:add                    disabled
    Should Be Equal As Strings            ${value}                                                                                                       None
    #ปุ่มเพิ่มรายการสินค้าสามารถกดได้
    Sleep                                 3
    Wait Until Element Is Visible         id:btnUpdate
    ${value} =                            Get Element Attribute                                                                                          id:btnUpdate              disabled
    Should Be Equal As Strings            ${value}                                                                                                       None
    #สามารถกดปุ่มบันทึกและอัพเดทสต็อกได้
    Sleep                                 3
    # รายการไม่ถูกลบ
    กดปุ่มปิดนำออกทองเก่า

Test28 ทดสอบการลบรายการทองเก่าที่จะนำออกสำเร็จเมื่อกดยืนยันยืนยันการลบรายการที่ยังไม่อัพเดทสต็อก
    เข้าหน้านำออกทองเก่า
    Sleep                                 3
    กดปุ่มแก้ไขใบนำออกทองเก่า3
    Sleep                                 3
    กดปุ่มลบสินค้า
    ตรวจพบข้อความบน Alert                 ยืนยันลบรายการนี้
    Sleep                                 3
    Wait Until Element Is Visible         id:add
    ${value} =                            Get Element Attribute                                                                                          id:add                    disabled
    Should Be Equal As Strings            ${value}                                                                                                       None
    #ปุ่มเพิ่มรายการสินค้าสามารถกดได้
    Sleep                                 3
    Wait Until Element Is Visible         id:btnUpdate
    ${value} =                            Get Element Attribute                                                                                          id:btnUpdate              disabled
    Should Be Equal As Strings            ${value}                                                                                                       true
    #ไม่สามารถกดปุ่มบันทึกและอัพเดทสต็อกได้
    Sleep                                 3
    # รายการถูกลบ
    กดปุ่มปิดนำออกทองเก่า

Test29 ทดสอบการลบรายการทองเก่าที่จะนำออกไม่สำเร็จเมื่อกดปฏิเสธการลบรายการที่อัพเดทสต็อกไปแล้ว
    เข้าหน้านำออกทองเก่า
    Sleep                                 3
    กดปุ่มแก้ไขใบนำออกทองเก่า2
    Sleep                                 3
    กดปุ่มลบสินค้า
    ตรวจพบข้อความบน Alertแล้วCancel       ยืนยันลบรายการนี้
    Sleep                                 3
    Wait Until Element Is Visible         id:add
    ${value} =                            Get Element Attribute                                                                                          id:add                    disabled
    Should Be Equal As Strings            ${value}                                                                                                       None
    #ปุ่มเพิ่มรายการสินค้าไม่สามารถกดได้
    Sleep                                 3
    Wait Until Element Is Visible         id:btnUpdate
    ${value} =                            Get Element Attribute                                                                                          id:btnUpdate              disabled
    Should Be Equal As Strings            ${value}                                                                                                       None
    #ไม่สามารถกดปุ่มบันทึกและอัพเดทสต็อกได้
    #พบ popup แก้ไขรายการทองใหม่ออกสต๊อก
    Sleep                                 3
    กดปุ่มปิดนำออกทองเก่า

Test30 ทดสอบการลบรายการทองเก่าที่จะนำออกไม่สำเร็จเมื่อกดยืนยันการลบรายการที่อัพเดทสต็อกไปแล้ว
    เข้าหน้านำออกทองเก่า
    Sleep                                 3
    กดปุ่มแก้ไขใบนำออกทองเก่า
    Sleep                                 3
    กดปุ่มลบสินค้า
    ตรวจพบข้อความบน Alert                 ยืนยันลบรายการนี้
    Sleep                                 3
    ตรวจพบข้อความบน Alert                 ไม่สามารถลบรายการนี้ได้
    #พบ alert ไม่สามารถลบรายการนี้ได้
    Sleep                                 3
    Wait Until Element Is Visible         id:add
    ${value} =                            Get Element Attribute                                                                                          id:add                    disabled
    Should Be Equal As Strings            ${value}                                                                                                       true
    #ปุ่มเพิ่มรายการสินค้าไม่สามารถกดได้
    Sleep                                 3
    Wait Until Element Is Visible         id:btnUpdate
    ${value} =                            Get Element Attribute                                                                                          id:btnUpdate              disabled
    Should Be Equal As Strings            ${value}                                                                                                       true
    #ไม่สามารถกดปุ่มบันทึกและอัพเดทสต็อกได้
    Sleep                                 3
    กดปุ่มปิดนำออกทองเก่า

Test32 ทดสอบการเพิ่มสินค้าในหน้านำออกทองเก่าไม่สำเร็จเมื่อไม่เลือกเปอร์เซ็นต์ทองเก่า
    เข้าหน้านำออกทองเก่า
    Sleep                                 3
    กดปุ่มแก้ไขใบนำออกทองเก่า3
    Sleep                                 3
    กดปุ่มเพิ่มรายการนำออกทองเก่า
    Sleep                                 3
    กรอกน้ำหนักกรัม                       15.2
    Sleep                                 3
    กรอกราคาตัด                           25000
    Sleep                                 3
    กรอกราคาหลอม                          300
    Sleep                                 3
    กดปุ่มเพิ่มสินค้าเพื่อรายการสินค้า
    Sleep                                 3
    พบข้อความ                             เปอร์เซ็นต์ทองเก่าเลือกเปอร์เซ็นต์ทองเก่า
    # พบข้อความ เปอร์เซ็นต์ทองเก่าเลือกเปอร์เซ็นต์ทองเก่า
    กดปุ่มปิดเพิ่มสินค้า
    กดปุ่มปิดนำออกทองเก่า

# Test33 ทดสอบการเพิ่มสินค้าในหน้านำออกทองเก่าไม่สำเร็จเมื่อกรอกน.น.ทองเก่า(กรัม)เป็นค่าว่าง
#                 เข้าหน้านำออกทองเก่า
#                 Sleep                                         3
#                 กดปุ่มแก้ไขใบนำออกทองเก่า3
#                 Sleep                                         3
#                 กดปุ่มเพิ่มรายการนำออกทองเก่า
#                 Sleep                                         3
#                 เลือกเปอร์เซ็นทองแท่ง 96.5
#                 Sleep                                         3
#                 กรอกราคาตัด                                   3000
#                 Sleep                                         3
#                 กรอกราคาหลอม                                  3000
#                 Sleep                                         3
#                 กดปุ่มเพิ่มสินค้าเพื่อรายการสินค้า

Test34 ทดสอบการเพิ่มสินค้าในหน้านำออกทองเก่าไม่สำเร็จเมื่อกรอกราคาที่ตัด/บาทเป็นค่าว่าง
    เข้าหน้านำออกทองเก่า
    Sleep                                 3
    กดปุ่มแก้ไขใบนำออกทองเก่า3
    Sleep                                 3
    กดปุ่มเพิ่มรายการนำออกทองเก่า
    Sleep                                 3
    เลือกเปอร์เซ็นทองแท่ง 96.5
    Sleep                                 3
    กรอกน้ำหนักกรัม                       15.2
    Sleep                                 3
    กรอกราคาหลอม                          300
    Sleep                                 3
    กดปุ่มเพิ่มสินค้าเพื่อรายการสินค้า
    Sleep                                 3
    พบข้อความ                             ราคาที่ตัด/บาท*ต้องไม่เป็นค่าว่าง
    # พบข้อความ ราคาที่ตัด/บาท*ต้องไม่เป็นค่าว่าง
    กดปุ่มปิดเพิ่มสินค้า
    กดปุ่มปิดนำออกทองเก่า

Test35 ทดสอบการเพิ่มสินค้าในหน้านำออกทองเก่าไม่่สำเร็จเมื่อกรอกค่าหลอม/บาทเป็นค่าว่าง
    เข้าหน้านำออกทองเก่า
    Sleep                                 3
    กดปุ่มแก้ไขใบนำออกทองเก่า3
    Sleep                                 3
    กดปุ่มเพิ่มรายการนำออกทองเก่า
    Sleep                                 3
    เลือกเปอร์เซ็นทองแท่ง 96.5
    Sleep                                 3
    กรอกน้ำหนักกรัม                       15.2
    Sleep                                 3
    กรอกราคาตัด                           25000
    Sleep                                 3
    กดปุ่มเพิ่มสินค้าเพื่อรายการสินค้า
    Sleep                                 3
    พบข้อความ                             ค่าหลอม/บาท*ต้องไม่เป็นค่าว่าง
    # พบข้อความ ค่าหลอม/บาท*ต้องไม่เป็นค่าว่าง
    กดปุ่มปิดเพิ่มสินค้า
    กดปุ่มปิดนำออกทองเก่า

Test36 ทดสอบการแสดงน.น.ทองเก่า(กรัม)ถูกต้องเมื่อเลือกเปอร์เซ็นต์ทองเก่า
    เข้าหน้านำออกทองเก่า
    Sleep                                 3
    กดปุ่มแก้ไขใบนำออกทองเก่า3
    Sleep                                 3
    กดปุ่มเพิ่มรายการนำออกทองเก่า
    Sleep                                 3
    เลือกเปอร์เซ็นทองแท่ง 96.5
    Sleep                                 3
    พบน้ำหนักกรัม                         45.776
    #น.น.ทองเก่า(กรัม)ถูกต้อง
    กดปุ่มปิดเพิ่มสินค้า
    กดปุ่มปิดนำออกทองเก่า

Test37 ทดสอบการแสดงส่วนต่างเมื่อกรอกราคาที่ตัด/บาท
    Sleep                                 3
    เข้าหน้านำออกทองเก่า
    Sleep                                 3
    กดปุ่มแก้ไขใบนำออกทองเก่า3
    Sleep                                 3
    กดปุ่มเพิ่มรายการนำออกทองเก่า
    Sleep                                 3
    เลือกเปอร์เซ็นทองแท่ง 96.5
    Sleep                                 3
    กรอกน้ำหนักกรัม                       30.576
    Sleep                                 3
    กรอกราคาตัด                           25000
    Sleep                                 3
    พบส่วนต่าง                            3,600.00
    #ส่วนต่าง/บาทถูกต้อง
    กดปุ่มปิดเพิ่มสินค้า
    กดปุ่มปิดนำออกทองเก่า

Test38 ทดสอบการเคลียร์ข้อมูลรายการสินค้าในหน้านำออกทองเมื่อกดปุ่มเริ่มใหม่
    เข้าหน้านำออกทองเก่า
    Sleep                                 3
    กดปุ่มแก้ไขใบนำออกทองเก่า3
    Sleep                                 3
    กดปุ่มเพิ่มรายการนำออกทองเก่า
    Sleep                                 3
    เลือกเปอร์เซ็นทองแท่ง 96.5
    Sleep                                 3
    กรอกน้ำหนักกรัม                       15.2
    Sleep                                 3
    กรอกราคาตัด                           25000
    Sleep                                 3
    กรอกราคาหลอม                          300
    Sleep                                 3
    กดปุ่มเริ่มใหม่
    Sleep                                 3
    กดปุ่มเพิ่มสินค้าเพื่อรายการสินค้า
    Sleep                                 3
    พบข้อความ                             เปอร์เซ็นต์ทองเก่าเลือกเปอร์เซ็นต์ทองเก่า
    พบข้อความ                             น.น.ทองเก่า(กรัม)*ต้องไม่เป็นค่าว่าง
    พบข้อความ                             ราคาที่ตัด/บาท*ต้องไม่เป็นค่าว่าง
    พบข้อความ                             ค่าหลอม/บาท*ต้องไม่เป็นค่าว่าง
    # ทุกช่องเป็นค่าว่าง
    กดปุ่มปิดเพิ่มสินค้า
    กดปุ่มปิดนำออกทองเก่า

Test39 ทดสอบการเพิ่มรายการสินค้าในหน้านำออกทองเก่าสำเร็จเมื่อกรอกข้อมูลครบ
    เข้าหน้านำออกทองเก่า
    Sleep                                 3
    กดปุ่มแก้ไขใบนำออกทองเก่า3
    Sleep                                 3
    กดปุ่มเพิ่มรายการนำออกทองเก่า
    Sleep                                 3
    เลือกเปอร์เซ็นทองแท่ง 96.5
    Sleep                                 3
    กรอกน้ำหนักกรัม                       15.2
    Sleep                                 3
    กรอกราคาตัด                           25000
    Sleep                                 3
    กรอกราคาหลอม                          300
    Sleep                                 3
    กดปุ่มเพิ่มสินค้าเพื่อรายการสินค้า
    Sleep                                 3
    Wait Until Element Is Visible         id:btnUpdate
    ${value} =                            Get Element Attribute                                                                                          id:btnUpdate              disabled
    Should Be Equal As Strings            ${value}                                                                                                       None
    #สามารถกดปุ่มบันทึกและอัพเดทสต็อกได้
    # พบรายการสินค้าบนหน้าแก้ไขรายการนำออกทองเก่า
    กดปุ่มปิดนำออกทองเก่า

Test40 ทดสอบการเพิ่มรายการสินค้้าในหน้านำออกทองเก่าไม่สำเร็จเมื่อเพิ่มสิ้นค้าไปแล้วครั้งหนึ่ง
    เข้าหน้านำออกทองเก่า
    Sleep                                 3
    กดปุ่มแก้ไขใบนำออกทองเก่า3
    Sleep                                 3
    กดปุ่มเพิ่มรายการนำออกทองเก่า
    Sleep                                 3
    เลือกเปอร์เซ็นทองแท่ง 96.5
    Sleep                                 3
    กรอกน้ำหนักกรัม                       15.2
    Sleep                                 3
    กรอกราคาตัด                           25000
    Sleep                                 3
    กรอกราคาหลอม                          300
    Sleep                                 3
    กดปุ่มเพิ่มสินค้าเพื่อรายการสินค้า
    Sleep                                 3
    ตรวจพบข้อความบน Alert                 ไม่สามารถเพิ่มสินค้าได้อีกครั้ง
    # พบ alert ไม่สามารถเพิ่มสินค้าได้อีกครั้ง
    Sleep                                 3
    กดปุ่มปิดเพิ่มสินค้า
    กดปุ่มปิดนำออกทองเก่า

Test41 ทดสอบการคำนวณในหน้าแก้ไขรายการนำออกทองเก่า
    เข้าหน้านำออกทองเก่า
    Sleep                                 3
    กดปุ่มแก้ไขใบนำออกทองเก่า3
    Sleep                                 3
    พบน้ำหนักรวม                          15.20
    # พบน้ำหนักรวมถูกต้อง
    พบค่าตัดรวม                           24,928.00
    # พบราคาที่ตัดรวมถูกต้อง
    พบค่าหลอมรวม                          299.13
    # พบค่าหลอมรวมถูกต้อง
    พบราคาสุทธิ                           24,629.00
    # พบราคาสุทธิถูกต้อง
    Sleep                                 3
    กดปุ่มปิดนำออกทองเก่า

*** Keywords ***
เข้าหน้านำออกทองเก่า
    Wait Until Page Contains Element      id:exportOldStock                                                                                              300
    Click Element                         id:exportOldStock

กดปุ่มวันนี้
    Wait Until Page Contains Element      id:searchtoday                                                                                                 300
    Click Element                         id:searchtoday

กดปุ่มค้นหา
    Wait Until Page Contains Element      id:search                                                                                                      300
    Click Element                         id:search

กดปุ่มสร้างใบนำออกทองเก่า
    Wait Until Page Contains Element      id:addExportCategory                                                                                           300
    Click Element                         id:addExportCategory

กดปุ่มปิดนำออกทองเก่า
    Wait Until Page Contains Element      id:btnClose                                                                                                    300
    Click Element                         id:btnClose

กดปุ่มสร้าง
    Wait Until Element Is Visible         id:save                                                                                                        300
    Click Element                         id:save

เลือกสาขาลาดกระบัง
    Wait Until Element Is Visible         id:searchbranch                                                                                                300
    Click Element                         id:searchbranch
    Sleep                               1
    Wait Until Element Is Visible         //*[@id="searchbranch"]/div[2]/div[2]                                                                          300
    Click Element                         //*[@id="searchbranch"]/div[2]/div[2]

เลือกสาขาลาดกระบังเพื่อสร้าง
    Wait Until Element Is Visible         id:branch                                                                                                      300
    Click Element                         id:branch
    Wait Until Element Is Visible         //*[@id="branch"]/div[2]/div[1]                                                                                300
    Click Element                         //*[@id="branch"]/div[2]/div[1]

เลือกโรงงานเพื่อสร้าง
    Wait Until Element Is Visible         id:vendor                                                                                                      300
    Click Element                         id:vendor
    Wait Until Element Is Visible         //*[@id="vendor"]/div[2]/div[1]                                                                                300
    Click Element                         //*[@id="vendor"]/div[2]/div[1]

เลือกยังไม่ปรับปรุงสต็อก
    Wait Until Page Contains Element      //*[@id="root-content"]/div/div/div[2]/div/div/form[2]/div[2]/div[1]/div/label                                 300
    Click Element                         //*[@id="root-content"]/div/div/div[2]/div/div/form[2]/div[2]/div[1]/div/label

เลือกยังไม่เคลียร์บิล
    Wait Until Page Contains Element      //*[@id="root-content"]/div/div/div[2]/div/div/form[2]/div[2]/div[2]/div/label                                 300
    Click Element                         //*[@id="root-content"]/div/div/div[2]/div/div/form[2]/div[2]/div[2]/div/label

พบจำนวนแถว
    [Arguments]                           ${value}
    ${count} =                            Get Value                                                                                                      id:amount
    ${count_str} =                        Convert To String                                                                                              ${count}
    Should Be Equal                       ${count_str}                                                                                                   ${value}

ไม่พบวันที่ปรับปรุงสต็อก
    ${count} =                            Get Value                                                                                                      id:dateUpdate
    ${count_str} =                        Convert To String                                                                                              ${count}
    Should Be Equal                       ${count_str}                                                                                                   None

กดปุ่มแก้ไขใบนำออกทองเก่า
    Wait Until Element Is Visible         id:0_Edit                                                                                                      300
    Click Element                         id:0_Edit

กดปุ่มแก้ไขใบนำออกทองเก่า2
    Wait Until Element Is Visible         id:1_Edit                                                                                                      300
    Click Element                         id:1_Edit

กดปุ่มแก้ไขใบนำออกทองเก่า3
    Wait Until Element Is Visible         id:2_Edit                                                                                                      300
    Click Element                         id:2_Edit

กดปุ่มเพิ่มรายการนำออกทองเก่า
    Wait Until Element Is Visible         id:add                                                                                                         300
    Click Element                         id:add

เลือกเปอร์เซ็นทองแท่ง 96.5
    Wait Until Element Is Visible         id:percent                                                                                                     300
    Click Element                         id:percent
    Wait Until Element Is Visible         //*[@id="percent"]/div[2]/div[2]                                                                               300
    Click Element                         //*[@id="percent"]/div[2]/div[2]

เลือกเปอร์เซ็น96.5
    Wait Until Element Is Visible         id:percent                                                                                                     300
    Click Element                         id:percent
    Wait Until Element Is Visible         //*[@id="percent"]/div[2]/div[1]                                                                               300
    Click Element                         //*[@id="percent"]/div[2]/div[1]

กรอกน้ำหนักกรัม
    [Arguments]                           ${value}
    Sleep                                 2
    Wait Until Element Is Visible         id:weight
    Input Text                            id:weight                                                                                                      ${value}

กรอกราคาตัด
    [Arguments]                           ${value}
    Sleep                                 2
    Wait Until Element Is Visible         id:price
    Input Text                            id:price                                                                                                       ${value}

กรอกราคาหลอม
    [Arguments]                           ${value}
    Sleep                                 2
    Wait Until Element Is Visible         id:cost
    Input Text                            id:cost                                                                                                        ${value}

กรอกจำนวนรับเงินสด
    [Arguments]                           ${value}
    Sleep                                 2
    Wait Until Element Is Visible         id:inputCash                                                                                                   300
    Input Text                            id:inputCash                                                                                                   ${value}

กดปุ่มเพิ่มสินค้าเพื่อรายการสินค้า
    Wait Until Element Is Visible         id:confirmadd                                                                                                  300
    Click Element                         id:confirmadd

กดปุ่มเริ่มใหม่
    # Scroll Element Into View                /*[@id="btnClear"]
    Wait Until Element Is Visible         //*[@id="btnClear"]                                                                                            300
    Double Click Element                  //*[@id="btnClear"]

กดปุ่มบันทึกและอัพเดทสต็อก
    Wait Until Element Is Visible         id:btnUpdate                                                                                                   300
    Click Element                         id:btnUpdate

กดปุ่มลบสินค้า
    Wait Until Element Is Visible         id:0_Delete                                                                                                    300
    Click Element                         id:0_Delete

พบค่าหลอมรวม
    [Arguments]                           ${value}
    ${count} =                            Get Value                                                                                                      //*[@id="costtotal"]
    ${count_str} =                        Convert To String                                                                                              ${count}
    Should Be Equal                       ${count_str}                                                                                                   ${value}

พบค่าตัดรวม
    [Arguments]                           ${value}
    ${count} =                            Get Value                                                                                                      //*[@id="pricetotal"]
    ${count_str} =                        Convert To String                                                                                              ${count}
    Should Be Equal                       ${count_str}                                                                                                   ${value}

พบน้ำหนักรวม
    [Arguments]                           ${value}
    ${count} =                            Get Value                                                                                                      //*[@id="weighttotal"]
    ${count_str} =                        Convert To String                                                                                              ${count}
    Should Be Equal                       ${count_str}                                                                                                   ${value}

พบราคาสุทธิ
    [Arguments]                           ${value}
    ${count} =                            Get Value                                                                                                      //*[@id="invoicenet"]
    ${count_str} =                        Convert To String                                                                                              ${count}
    Should Be Equal                       ${count_str}                                                                                                   ${value}

พบสาขา
    [Arguments]                           ${value}
    ${count} =                            Get Text                                                                                                       //*[@id="branch"]
    ${count_str} =                        Convert To String                                                                                              ${count}
    Should Be Equal                       ${count_str}                                                                                                   ${value}

พบโรงงาน
    [Arguments]                           ${value}
    ${count} =                            Get Text                                                                                                       //*[@id="vendor"]
    ${count_str} =                        Convert To String                                                                                              ${count}
    Should Be Equal                       ${count_str}                                                                                                   ${value}

พบเลขที่บิล
    [Arguments]                           ${value}
    ${count} =                            Get Text                                                                                                       //*[@id="textBillID"]
    ${count_str} =                        Convert To String                                                                                              ${count}
    Should Be Equal                       ${count_str}                                                                                                   ${value}

กดบันทึกชำระเงิน
    Sleep                                 2
    Wait Until Element Is Visible         id:btnSave                                                                                                     300
    Click Element                         id:btnSave

กดปุ่มปิดชำระเงิน
    Wait Until Element Is Visible         id:closePaymentModal                                                                                           300
    Click Element                         id:closePaymentModal

กดปุ่มปิดเพิ่มสินค้า
    Wait Until Element Is Visible         id:closeaddproduct                                                                                             300
    Click Element                         id:closeaddproduct

กดปุ่มบันทึก
    Wait Until Element Is Visible         id:save                                                                                                        300
    Click Element                         id:save

พบน้ำหนักกรัม
    [Arguments]                           ${value}
    ${count} =                            Get Value                                                                                                      //*[@id="weight"]
    ${count_str} =                        Convert To String                                                                                              ${count}
    Should Be Equal                       ${count_str}                                                                                                   ${value}

พบส่วนต่าง
    [Arguments]                           ${value}
    ${count} =                            Get Value                                                                                                      //*[@id="pricediff"]
    ${count_str} =                        Convert To String                                                                                              ${count}
    Should Be Equal                       ${count_str}                                                                                                   ${value}

