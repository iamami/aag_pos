*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบAdminและinit_data
Test Setup        เข้าหน้าสต็อกทอง
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data_admin
*** Variables ***

*** Test cases ***
# ค้นหา
Test2 ทดสอบการแสดงรายการนำเข้าทองใหม่เมื่อเลือกสาขา
    เข้าหน้านำเข้าทองใหม่
    Sleep                                3
    Go To                                ${BASE_URL}stock/import?start_date=01/01/2016&end_date=04/11/2019
    # เลือกสาขา
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="branch"]/i                                                                                            300
    Click Element                        //*[@id="branch"]/i
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="branch"]/div[2]/div[2]/span                                                                           300
    Click Element                        //*[@id="branch"]/div[2]/div[2]/span
    กดปุ่มค้นหารายการนำเข้าทองใหม่
    ตรวจรายการนำเข้าทองใหม่              1

Test3 ทดสอบการแสดงรายการนำเข้าทองใหม่เมื่อเลือกชื่อโรงงาน
    เข้าหน้านำเข้าทองใหม่
    Sleep                               3
    Go To                               ${BASE_URL}stock/import?start_date=01/01/2016&end_date=04/11/2019
    # เลือกสาขา
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="branch"]/i                                                                                            300
    Click Element                        //*[@id="branch"]/i
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="branch"]/div[2]/div[2]/span                                                                           300
    Click Element                        //*[@id="branch"]/div[2]/div[2]/span
    กดปุ่มค้นหารายการนำเข้าทองใหม่
    ตรวจรายการนำเข้าทองใหม่              1

Test4 ทดสอบการแสดงรายการนำเข้าทองใหม่เมื่อเลือกวันที่ถึงวันที่
    เข้าหน้านำเข้าทองใหม่
    Sleep                               3
    Go To                               ${BASE_URL}stock/import?start_date=01/01/2016&end_date=04/11/2019
    กดปุ่มค้นหารายการนำเข้าทองใหม่
    ตรวจรายการนำเข้าทองใหม่              3

Test5 ทดสอบการแสดงรายการนำเข้าทองใหม่เมื่อเลือกยังไม่ปรับปรุงสต็อก
    เข้าหน้านำเข้าทองใหม่
    Sleep                               3
    Go To                               ${BASE_URL}stock/import?start_date=01/01/2016&end_date=04/11/2019
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="root-content"]/div/div/div[2]/div/div/form[2]/div[2]/div[1]/div/label                                 300
    Click Element                        //*[@id="root-content"]/div/div/div[2]/div/div/form[2]/div[2]/div[1]/div/label
    กดปุ่มค้นหารายการนำเข้าทองใหม่
    ตรวจรายการนำเข้าทองใหม่              1

Test6 ทดสอบการแสดงรายการนำเข้าทองใหม่เมื่อเลือกยังไม่เคลียร์บิล
    เข้าหน้านำเข้าทองใหม่
    Sleep                               3
    Go To                               ${BASE_URL}stock/import?start_date=01/01/2016&end_date=04/11/2019
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="root-content"]/div/div/div[2]/div/div/form[2]/div[2]/div[2]/div/label                                 300
    Click Element                        //*[@id="root-content"]/div/div/div[2]/div/div/form[2]/div[2]/div[2]/div/label
    กดปุ่มค้นหารายการนำเข้าทองใหม่
    ตรวจรายการนำเข้าทองใหม่              2

Test7 ทดสอบการแสดงรายการนำเข้าทองใหม่เมื่อเลือกสาขา/ยังไม่เคลียร์บิล
    เข้าหน้านำเข้าทองใหม่
    Sleep                               3
    Go To                               ${BASE_URL}stock/import?start_date=01/01/2016&end_date=04/11/2019
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="branch"]/i                                                                                            300
    Click Element                        //*[@id="branch"]/i
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="branch"]/div[2]/div[3]/span                                                                           300
    Click Element                        //*[@id="branch"]/div[2]/div[3]/span
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="root-content"]/div/div/div[2]/div/div/form[2]/div[2]/div[2]/div/label                                 300
    Click Element                        //*[@id="root-content"]/div/div/div[2]/div/div/form[2]/div[2]/div[2]/div/label
    กดปุ่มค้นหารายการนำเข้าทองใหม่
    ตรวจรายการนำเข้าทองใหม่              2

# เพิ่มรายการนำเข้าทองใหม่
Test9 ทดสอบการไม่สามารถเพิ่มรายการสินค้าในหน้านำเข้าทองใหม่ได้เมื่อยังไม่สร้างรายการ
    เข้าหน้านำเข้าทองใหม่
    กดเพิ่มสร้างใบนำเข้าทอง
    ${value} =                           Get Element Attribute                                                                                          id:addproductimport               disabled
    กดกากบาทสร้างใบนำเข้าทอง

Test10 ทดสอบการไม่สามารถสั่งพิมพ์ในหน้านำเข้าทองใหม่ได้เมื่อยังไม่สร้างรายการ
    เข้าหน้านำเข้าทองใหม่
    กดเพิ่มสร้างใบนำเข้าทอง
    ${value} =                           Get Element Attribute                                                                                          id:print                          disabled
    กดกากบาทสร้างใบนำเข้าทอง

Test11 ทดสอบการไม่สามารถบันทึกและอัพเดทสต็อกในหน้านำเข้าทองใหม่ได้เมื่อยังไม่สร้างรายการและเพิ่มรายการสินค้า
    เข้าหน้านำเข้าทองใหม่
    กดเพิ่มสร้างใบนำเข้าทอง
    ${value} =                           Get Element Attribute                                                                                          //*[@id="updateimport"]           disabled
    กดกากบาทสร้างใบนำเข้าทอง

Test12 ททดสอบการเพิ่มรายการนำเข้าทองใหม่ไม่สำเร็จเมื่อไม่เลือกสาขา
    เข้าหน้านำเข้าทองใหม่
    กดเพิ่มสร้างใบนำเข้าทอง
    เลือกโรงงานหน้าสร้างใบนำเข้าทอง
    กดสร้างใบนำเข้าทอง
    พบข้อความ                            *สาขากรุณาเลือกสาขา
    กดกากบาทสร้างใบนำเข้าทอง

Test13 ทดสอบการเพิ่มรายกานำนำเข้าทองใหม่ไม่สำเร็จเมื่อไม่เลือกชื่อโรงงาน/ร้านส่ง
    เข้าหน้านำเข้าทองใหม่
    กดเพิ่มสร้างใบนำเข้าทอง
    เลือกสาขาหน้าสร้างใบนำเข้าทอง
    กดสร้างใบนำเข้าทอง
    พบข้อความ                            *ชื่อโรงงาน/ร้านส่งกรุณาเลือกโรงงาน/ร้านส่ง
    กดกากบาทสร้างใบนำเข้าทอง

Test14 ทดสอบการเพิ่มรายการนำเข้าทองใหม่สำเร็จเมื่อกดปุ่มสร้าง
    เข้าหน้านำเข้าทองใหม่
    กดเพิ่มสร้างใบนำเข้าทอง
    เลือกสาขาหน้าสร้างใบนำเข้าทอง
    เลือกโรงงานหน้าสร้างใบนำเข้าทอง
    กดสร้างใบนำเข้าทอง
    พบข้อความ                            แก้ไขรายการทองใหม่เข้าสต๊อก
    กดกากบาทสร้างใบนำเข้าทอง

# แก้ไขรายการนำเข้าทองใหม่
Test15 ทดสอบการแสดงรายละเอียดบนหน้าแก้ไขรายการนำเข้าทองใหม่ถูกต้อง เมื่อกดปุ่มแก้ไขบนหน้านำเข้าทองใหม่
    เข้าหน้านำเข้าทองใหม่
    กดแก้ไขนำเข้าทองใหม่
    ${branch}                            Get Text                                                                                                       //*[@id="branchimport"]/div[1]
    Should Be Equal                      ${branch}                                                                                                      ลาดกระบัง
    ${vendor}                            Get Text                                                                                                       //*[@id="vendorimport"]/div[1]
    Should Be Equal                      ${vendor}                                                                                                      vendor1
    กดกากบาทสร้างใบนำเข้าทอง

Test16 ทดสอบการไม่สามารถบันทึกและอัพเดทสต็อกในหน้าแก้ไขรายการทองใหม่ได้เมื่อยังไม่เพิ่มรายการสินค้า
    เข้าหน้านำเข้าทองใหม่
    กดแก้ไขนำเข้าทองใหม่
    ${update} =                          Get Element Attribute                                                                                          //*[@id="updateimport"]           disabled
    กดกากบาทสร้างใบนำเข้าทอง

Test17 ทดสอบการสามารถสั่งพิมพ์ในหน้าแก้ไขรายการทองใหม่เมื่อมีรายการสินค้า
    เข้าหน้านำเข้าทองใหม่
    กดแก้ไขนำเข้าทองใหม่
    ${print} =                           Get Element Attribute                                                                                          //*[@id="print"]                  labeled
    กดกากบาทสร้างใบนำเข้าทอง

Test18 ทดสอบการสามารถกดปุ่มบันทึกและอัพเดทสต็อกในหน้าแก้ไขรายการทองใหม่เมื่อมีรายการสินค้า
    เข้าหน้านำเข้าทองใหม่
    กดแก้ไขนำเข้าทองใหม่
    กดเพิ่มสินค้านำเข้าทองใหม่
    เลือกชื่อสินค้านำเข้าทองใหม่
    กรอกจำนวนสินค้านำเข้าทองใหม่         1
    กดยืนยันเพิ่มสินค้านำเข้าทองใหม่
    ${update} =                          Get Element Attribute                                                                                          //*[@id="updateimport"]           labeled
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="table_w"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a                    300
    Click Element                        //*[@id="table_w"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a
    ตรวจพบข้อความบน Alert                ยืนยันลบ
    กดกากบาทสร้างใบนำเข้าทอง

Test19 ทดสอบการไม่สามารถแก้ไขสาขาในหน้าแก้ไขรายการนำเข้าทองใหม่
    เข้าหน้านำเข้าทองใหม่
    กดแก้ไขนำเข้าทองใหม่
    ${branch} =                          Get Element Attribute                                                                                          //*[@id="branchimport"]           disabled
    กดกากบาทสร้างใบนำเข้าทอง

Test20 ทดสอบการไม่สามารถแก้ไขชื่อโรงงาน/ร้านส่งในหน้าแก้ไขรายการนำเข้าทองใหม่
    เข้าหน้านำเข้าทองใหม่
    กดแก้ไขนำเข้าทองใหม่
    ${vendor} =                          Get Element Attribute                                                                                          //*[@id="vendorimport"]           disabled
    กดกากบาทสร้างใบนำเข้าทอง

Test21 ทดสอบการไม่สามารถแก้ไขวันที่ในหน้าแก้ไขรายการนำเข้าทองใหม่
    เข้าหน้านำเข้าทองใหม่
    กดแก้ไขนำเข้าทองใหม่
    ${date} =                            Get Element Attribute                                                                                          //*[@id="date"]                   disabled
    กดกากบาทสร้างใบนำเข้าทอง

Test23 ทดสอบการแก้ไขรายการนำเข้าทองใหม่ไม่สำเร็จเมื่อปฏิเสธการยืนยันการบันทึกและอัพเดทสต๊อก
    เข้าหน้านำเข้าทองใหม่
    กดแก้ไขนำเข้าทองใหม่
    กดเพิ่มสินค้านำเข้าทองใหม่
    เลือกชื่อสินค้านำเข้าทองใหม่
    กรอกจำนวนสินค้านำเข้าทองใหม่         1
    กดยืนยันเพิ่มสินค้านำเข้าทองใหม่
    Sleep                                2
    Wait Until Element Is Visible        //*[@id="updateimport"]                                                                                        300
    Click Element                        //*[@id="updateimport"]
    Sleep                                2
    ตรวจพบข้อความบน Alertแล้วCancel      ยืนยันบันทึกและอัพเดทสต๊อก
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="table_w"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a                    300
    Click Element                        //*[@id="table_w"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a
    ตรวจพบข้อความบน Alert                ยืนยันลบ
    กดกากบาทสร้างใบนำเข้าทอง

Test24 ทดสอบการแก้ไขรายการนำเข้าทองใหม่สำเร็จเมื่อกดปุ่มบันทึก
    เข้าหน้านำเข้าทองใหม่
    กดแก้ไขนำเข้าทองใหม่
    กดเพิ่มสินค้านำเข้าทองใหม่
    เลือกชื่อสินค้านำเข้าทองใหม่
    กรอกจำนวนสินค้านำเข้าทองใหม่         1
    กดยืนยันเพิ่มสินค้านำเข้าทองใหม่
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="table_w"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a                    300
    Click Element                        //*[@id="table_w"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a
    ตรวจพบข้อความบน Alert                ยืนยันลบ
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="save"]                                                                                                300
    Click Element                        //*[@id="save"]
    พบข้อความ                            ยังไม่อัพเดทสต๊อก
    กดกากบาทสร้างใบนำเข้าทอง

Test26 ทดสอบการแก้ไขรายการนำเข้าทองใหม่สำเร็จเมื่อตกลงการยืนยันการบันทึกและอัพเดทสต๊อก
    เข้าหน้านำเข้าทองใหม่
    กดแก้ไขนำเข้าทองใหม่
    กดเพิ่มสินค้านำเข้าทองใหม่
    เลือกชื่อสินค้านำเข้าทองใหม่
    กรอกจำนวนสินค้านำเข้าทองใหม่         1
    กดยืนยันเพิ่มสินค้านำเข้าทองใหม่
    กดบันทึกและอัพเดทสต็อก
    ตรวจพบข้อความบน Alert                ยืนยันบันทึกและอัพเดทสต๊อก
    พบข้อความ                            อัพเดทสต๊อกแล้ว
    กดกากบาทสร้างใบนำเข้าทอง



Test27 ทดสอบการลบรายการสินค้าที่จะนำเข้าทองใหม่ไม่สำเร็จเมื่อกดปฏิเสธยืนยันการลบรายการที่ยังไม่อัพเดทสต็อก
    เข้าหน้านำเข้าทองใหม่
    กดเพิ่มสร้างใบนำเข้าทอง
    เลือกสาขาหน้าสร้างใบนำเข้าทอง
    เลือกโรงงานหน้าสร้างใบนำเข้าทอง
    กดสร้างใบนำเข้าทอง
    พบข้อความ                            แก้ไขรายการทองใหม่เข้าสต๊อก
    Sleep                                2
    กดเพิ่มสินค้านำเข้าทองใหม่
    เลือกชื่อสินค้านำเข้าทองใหม่
    กรอกจำนวนสินค้านำเข้าทองใหม่         1
    กดยืนยันเพิ่มสินค้านำเข้าทองใหม่
    Sleep                                3
    Wait Until Element Is Visible        //*[@id="0_Delete"]                                                                                            300
    Click Element                        //*[@id="0_Delete"]
    ตรวจพบข้อความบน Alertแล้วCancel      ยืนยันลบ
    Sleep                                3
    Wait Until Element Is Visible        id:add
    ${value} =                           Get Element Attribute                                                                                          id:add                            disabled
    Should Be Equal As Strings           ${value}                                                                                                       None
    #ปุ่มเพิ่มรายการสินค้าสามารถกดได้
    Sleep                                3
    Wait Until Element Is Visible        id:updateimport
    ${value} =                           Get Element Attribute                                                                                          id:updateimport                   disabled
    Should Be Equal As Strings           ${value}                                                                                                       None
    #ไม่สามารถกดปุ่มบันทึกและอัพเดทสต็อกได้
    Sleep                                3
    กดกากบาทสร้างใบนำเข้าทอง
    Sleep                                3

Test28 ทดสอบการลบรายการสินค้าที่จะนำเข้าทองใหม่สำเร็จเมื่อกดยืนยันยืนยันการลบรายการที่ยังไม่อัพเดทสต็อก
    เข้าหน้านำเข้าทองใหม่
    Sleep                                2
    Wait Until Element Is Visible        //*[@id="1_Edit"]                                                                                              300
    Click Element                        //*[@id="1_Edit"]
    Sleep                               2
    Wait Until Element Is Visible        //*[@id="0_Delete"]                                                                                            300
    Click Element                        //*[@id="0_Delete"]
    ตรวจพบข้อความบน Alert                ยืนยันลบ
    Sleep                                3
    Wait Until Element Is Visible        id:add
    ${value} =                           Get Element Attribute                                                                                          id:add                            disabled
    Should Be Equal As Strings           ${value}                                                                                                       None
    #ปุ่มเพิ่มรายการสินค้าสามารถกดได้
    Sleep                                3
    Wait Until Element Is Visible        id:updateimport
    ${value} =                           Get Element Attribute                                                                                          id:updateimport                   disabled
    Should Be Equal As Strings           ${value}                                                                                                       true
    #ไม่สามารถกดปุ่มบันทึกและอัพเดทสต็อกได้
    Sleep                                3
    กดกากบาทสร้างใบนำเข้าทอง
    Sleep                                3

Test29 ทดสอบการลบรายการสินค้าที่จะนำเข้าทองใหม่ไม่สำเร็จเมื่อกดลบรายการที่อัพเดทสต็อกไปแล้ว
    เข้าหน้านำเข้าทองใหม่
    Sleep                                2
    Wait Until Element Is Visible        //*[@id="0_Edit"]                                                                                              300
    Click Element                        //*[@id="0_Edit"]
    Sleep                               2
    Wait Until Element Is Visible        //*[@id="0_Delete"]                                                                                            300
    Click Element                        //*[@id="0_Delete"]
    ตรวจพบข้อความบน Alert                ไม่สามารถลบรายการนี้ได้
    Sleep                                3
    Wait Until Element Is Visible        id:add
    ${value} =                           Get Element Attribute                                                                                          id:add                            disabled
    Should Be Equal As Strings           ${value}                                                                                                       true
    #ปุ่มเพิ่มรายการสินค้าไม่สามารถกดได้
    Sleep                                3
    Wait Until Element Is Visible        id:updateimport
    ${value} =                           Get Element Attribute                                                                                          id:updateimport                   disabled
    Should Be Equal As Strings           ${value}                                                                                                       true
    #ไม่สามารถกดปุ่มบันทึกและอัพเดทสต็อกได้
    Sleep                                3
    กดกากบาทสร้างใบนำเข้าทอง
    Sleep                                3

Test31 ทดสอบการเพิ่มสินค้าไม่สำเร็จเมื่อไม่เลือกชื่อสินค้า
    เข้าหน้านำเข้าทองใหม่
    Sleep                                2
    Wait Until Element Is Visible        //*[@id="1_Edit"]                                                                                              300
    Click Element                        //*[@id="1_Edit"]
    กดเพิ่มสินค้านำเข้าทองใหม่
    กรอกจำนวนสินค้านำเข้าทองใหม่         1
    กดยืนยันเพิ่มสินค้านำเข้าทองใหม่
    Sleep                                3
    พบข้อความ                            *ชื่อสินค้า กรุณาเลือกสินค้า
    พบข้อความ                            น.น.(กรัม) กรุณาระบุ น.น.(กรัม) ให้ถูกต้อง
    #พบข้อความ *ชื่อสินค้า กรุณาเลือกชื่อสินค้า
    Sleep                                3
    กดปุ่มปิดเพิ่มสินค้านำเข้าทองใหม่
    กดกากบาทสร้างใบนำเข้าทอง

# Test32 ทดสอบการเพิ่มสินค้าไม่สำเร็จเมื่อกรอกจำนวนเป็นค่าว่าง
#                 เข้าหน้านำเข้าทองใหม่
#                 Sleep                                         2
#                 Wait Until Element Is Visible                 //*[@id="1_Edit"]                                      300
#                 Click Element                                 //*[@id="1_Edit"]
#                 กดเพิ่มสินค้านำเข้าทองใหม่
#                 เลือกชื่อสินค้านำเข้าทองใหม่
#                 กดยืนยันเพิ่มสินค้านำเข้าทองใหม่
#                 Sleep                                         3
#                 พบข้อความ                                     *จำนวน กรุณาระบุจำนวนให้ถูกต้อง
#                 พบข้อความ                                     น.น.รวม(กรัม) กรุณาระบุ น.น.รวม(กรัม) ให้ถูกต้อง
#                 พบข้อความ                                     *น.น.ชั่ง(กรัม) กรุณาระบุ น.น.ชั่ง(กรัม) ให้ถูกต้อง
#                 #พบข้อความ *จำนวน กรุณาระบุจำนวนให้ถูกต้อง
#                 Sleep                                         3
#                 กดปุ่มปิดเพิ่มสินค้านำเข้าทองใหม่
#                 กดกากบาทสร้างใบนำเข้าทอง

Test37 ทดสอบการแสดงน.น.(กรัม)ถูกต้องเมื่อเลือกชื่อสินค้า
    เข้าหน้านำเข้าทองใหม่
    Sleep                                2
    Wait Until Element Is Visible        //*[@id="1_Edit"]                                                                                              300
    Click Element                        //*[@id="1_Edit"]
    กดเพิ่มสินค้านำเข้าทองใหม่
    เลือกชื่อสินค้านำเข้าทองใหม่
    พบน.น.(กรัม)                         9.200
    #น.น.(กรัม)ถูกต้อง
    กดปุ่มปิดเพิ่มสินค้านำเข้าทองใหม่
    กดกากบาทสร้างใบนำเข้าทอง

Test38 ทดสอบการแสดงน.น.รวม(กรัม)/น.น.ชั่ง(กรัม)ถูกต้องเมื่อเลือกชื่อสินค้าแล้วกรอกจำนวน
    เข้าหน้านำเข้าทองใหม่
    Sleep                                2
    Wait Until Element Is Visible        //*[@id="1_Edit"]                                                                                              300
    Click Element                        //*[@id="1_Edit"]
    กดเพิ่มสินค้านำเข้าทองใหม่
    เลือกชื่อสินค้านำเข้าทองใหม่
    กรอกจำนวนสินค้านำเข้าทองใหม่         2
    พบน.น.รวม(กรัม)                      18.400
    พบ*น.น.ชั่ง(กรัม)                    18.400
    #น.น.รวม(กรัม)/น.น.ชั่ง(กรัม)ถูกต้อง
    กดปุ่มปิดเพิ่มสินค้านำเข้าทองใหม่
    กดกากบาทสร้างใบนำเข้าทอง

Test39 ทดสอบการเพิ่มรายการสินค้าสำเร็จเมื่อกรอกข้อมูลครบ
    เข้าหน้านำเข้าทองใหม่
    Sleep                                2
    Wait Until Element Is Visible        //*[@id="1_Edit"]                                                                                              300
    Click Element                        //*[@id="1_Edit"]
    กดเพิ่มสินค้านำเข้าทองใหม่
    เลือกชื่อสินค้านำเข้าทองใหม่
    กรอกจำนวนสินค้านำเข้าทองใหม่         1
    กดยืนยันเพิ่มสินค้านำเข้าทองใหม่
    Sleep                                3
    Wait Until Element Is Visible        //*[@id="0_Delete"]
    #พบรายการสินค้าบนหน้าแก้ไขรายการทองใหม่
    Sleep                                3
    กดกากบาทสร้างใบนำเข้าทอง

Test40 ทดสอบการคำนวณในหน้าแก้ไขรายการนำเข้าทองใหม่
    เข้าหน้านำเข้าทองใหม่
    Sleep                                2
    Wait Until Element Is Visible        //*[@id="1_Edit"]                                                                                              300
    Click Element                        //*[@id="1_Edit"]
    พบจำนวนรายการ                        1
    พบค่าแรงขายปลีกรวม                   0.00
    พบน้ำหนักรวม                         9.200
    พบจำนวนรวม                           1
    กดกากบาทสร้างใบนำเข้าทอง

Test41 ทดสอบการพิมพ์รายการนำเข้าทองใหม่ไม่สำเร็จเมื่อกดปุ่มปิด
    เข้าหน้านำเข้าทองใหม่
    Sleep                                2
    Wait Until Element Is Visible        //*[@id="0_Edit"]                                                                                              300
    Click Element                        //*[@id="0_Edit"]
    กดปุ่มพิมพ์นำเข้าทองใหม่
    Sleep                                3
    กดปุ่มปิดพิมพ์นำเข้าทองใหม่
    Sleep                                3
    Wait Until Element Is Visible        //*[@id="addproductimport"]
    พบจำนวนรายการ                        1
    #พบหน้านำเข้าทองใหม่
    กดกากบาทสร้างใบนำเข้าทอง

*** Keywords ***
เข้าหน้านำเข้าทองใหม่
    Sleep                                2
    Wait Until Page Contains Element     //*[@id="importStock"]                                                                                         300
    Click Element                        //*[@id="importStock"]

ตรวจรายการนำเข้าทองใหม่
    [Arguments]                          ${value}
    Sleep                                2
    ${count}                             Get Value                                                                                                      //*[@id="amount"]
    Should Be Equal                      ${count}                                                                                                       ${value}

กดปุ่มค้นหารายการนำเข้าทองใหม่
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="search"]                                                                                              300
    Click Element                        //*[@id="search"]

กดเพิ่มสร้างใบนำเข้าทอง
    Sleep                                2
    Wait Until Page Contains Element     //*[@id="addproductimport"]                                                                                    300
    Click Element                        //*[@id="addproductimport"]

กดกากบาทสร้างใบนำเข้าทอง
    Sleep                                2
    Wait Until Page Contains Element     //*[@id="btnCloseModalImEx"]                                                                                   300
    Click Element                        //*[@id="btnCloseModalImEx"]

กดสร้างใบนำเข้าทอง
    Sleep                                2
    Wait Until Page Contains Element     //*[@id="save"]                                                                                                300
    Click Element                        //*[@id="save"]

เลือกสาขาหน้าสร้างใบนำเข้าทอง
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="branchimport"]/i                                                                                      300
    Click Element                        //*[@id="branchimport"]/i
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="branchimport"]/div[2]/div[1]/span                                                                     300
    Click Element                        //*[@id="branchimport"]/div[2]/div[1]/span

เลือกโรงงานหน้าสร้างใบนำเข้าทอง
    Sleep                                2
    Wait Until Element Is Visible        //*[@id="vendorimport"]/i                                                                                      300
    Click Element                        //*[@id="vendorimport"]/i
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="vendorimport"]/div[2]/div[2]/span                                                                     300
    Click Element                        //*[@id="vendorimport"]/div[2]/div[2]/span

กดแก้ไขนำเข้าทองใหม่
    Sleep                                2
    Wait Until Element Is Visible        //*[@id="0_Edit"]                                                                                              300
    Click Element                        //*[@id="0_Edit"]

กดเพิ่มสินค้านำเข้าทองใหม่
    Sleep                                2
    Wait Until Element Is Visible        //*[@id="add"]                                                                                                 300
    Click Element                        //*[@id="add"]

เลือกชื่อสินค้านำเข้าทองใหม่
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="productimport"]/i                                                                                     300
    Click Element                        //*[@id="productimport"]/i
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="productimport"]/div[2]/div/span                                                                       300
    Click Element                        //*[@id="productimport"]/div[2]/div/span

กรอกจำนวนสินค้านำเข้าทองใหม่
    [Arguments]                          ${value}
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="amountimport"]                                                                                        300
    Input Text                           //*[@id="amountimport"]                                                                                        ${value}

กรอกน.น.ชั่ง(กรัม)สินค้านำเข้าทองใหม่
    [Arguments]                          ${value}
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="weightrealimport"]                                                                                    300
    Input Text                           //*[@id="weightrealimport"]                                                                                    ${value}

กดยืนยันเพิ่มสินค้านำเข้าทองใหม่
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="addimport"]                                                                                           300
    Click Element                        //*[@id="addimport"]

กดบันทึกและอัพเดทสต็อก
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="updateimport"]                                                                                        300
    Click Element                        //*[@id="updateimport"]

พบน.น.(กรัม)
    [Arguments]                          ${value}
    ${count} =                           Get Value                                                                                                      //*[@id="weightimport"]
    ${count_str} =                       Convert To String                                                                                              ${count}
    Should Be Equal                      ${count_str}                                                                                                   ${value}

พบน.น.รวม(กรัม)
    [Arguments]                          ${value}
    ${count} =                           Get Value                                                                                                      //*[@id="weighttotalimport"]
    ${count_str} =                       Convert To String                                                                                              ${count}
    Should Be Equal                      ${count_str}                                                                                                   ${value}

พบ*น.น.ชั่ง(กรัม)
    [Arguments]                          ${value}
    ${count} =                           Get Value                                                                                                      //*[@id="weightrealimport"]
    ${count_str} =                       Convert To String                                                                                              ${count}
    Should Be Equal                      ${count_str}                                                                                                   ${value}

พบจำนวนรายการ
    [Arguments]                          ${value}
    ${count} =                           Get Value                                                                                                      //*[@id="invoiceitem"]
    ${count_str} =                       Convert To String                                                                                              ${count}
    Should Be Equal                      ${count_str}                                                                                                   ${value}

พบค่าแรงขายปลีกรวม
    [Arguments]                          ${value}
    ${count} =                           Get Value                                                                                                      //*[@id="invoiceprofit"]
    ${count_str} =                       Convert To String                                                                                              ${count}
    Should Be Equal                      ${count_str}                                                                                                   ${value}

พบน้ำหนักรวม
    [Arguments]                          ${value}
    ${count} =                           Get Value                                                                                                      //*[@id="weighttotal"]
    ${count_str} =                       Convert To String                                                                                              ${count}
    Should Be Equal                      ${count_str}                                                                                                   ${value}

พบจำนวนรวม
    [Arguments]                          ${value}
    ${count} =                           Get Value                                                                                                      //*[@id="producttotal"]
    ${count_str} =                       Convert To String                                                                                              ${count}
    Should Be Equal                      ${count_str}                                                                                                   ${value}

กดปุ่มปิดพิมพ์นำเข้าทองใหม่
    Sleep                                1
    Wait Until Element Is Visible        id:btnClosePreview                                                                                             300
    Click Element                        id:btnClosePreview

กดปุ่มพิมพ์นำเข้าทองใหม่
    Sleep                                1
    Wait Until Element Is Visible        id:print                                                                                                       300
    Click Element                        id:print

กดปุ่มปิดเพิ่มสินค้านำเข้าทองใหม่
    Sleep                                1
    Wait Until Element Is Visible        id:btnCloseProduct                                                                                             300
    Click Element                        id:btnCloseProduct

กดปุ่มบันทึก
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="save"]                                                                                                300
    Click Element                        //*[@id="save"]

กดปุ่มเคลียร์บิล
    Sleep                                1
    Wait Until Element Is Visible        id:clearbillimport                                                                                             300
    Click Element                        id:clearbillimport
