*** Settings ***
Library            Selenium2Library
Resource           ${EXECDIR}/e2e/resource/keyword.robot
Resource           ${EXECDIR}/e2e/resource/variable.robot
Suite Setup        เปิดbrowserและเข้าสู่ระบบและinit_data     clearSettingCustomer
Test Setup         เข้าหน้าลูกค้า
Test Teardown      เข้าสู่หน้าหลัก
Suite Teardown     Close Browser
*** Variables ***

*** Test cases ***
test1 ทดสอบการไม่สามารถกดยกเลิก ชื่อลูกค้า
    เข้าหน้าลูกค้า
    กดปุ่มตั้งค่า
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="customername"]/div/label              300
    Click Element                        //*[@id="customername"]/div/label              
    Sleep                                1
    Wait Until Element Is Visible        id:settingconfirm              300
    Click Element                        id:settingconfirm
    Sleep                                1
    Wait Until Element Is Not Visible    id:settingModal                300
    กดปุ่มเพิ่ม
    Sleep                                1
    พบข้อความ                            ชื่อ
    ปิดPopupเพิ่ม

test2 ทดสอบการไม่สามารถกดยกเลิก หมายเหตุ
    เข้าหน้าลูกค้า
    กดปุ่มตั้งค่า
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="note"]/div/label              300
    Click Element                        //*[@id="note"]/div/label
    Sleep                                1
    Wait Until Element Is Visible        id:settingconfirm              300
    Click Element                        id:settingconfirm
    Sleep                                1
    Wait Until Element Is Not Visible    id:settingModal                300
    กดปุ่มเพิ่ม
    Sleep                                1
    พบข้อความ                            หมายเหตุ
    ปิดPopupเพิ่ม

test3 ทดสอบการกดเลือก ชื่อเล่น สำเร็จ เมื่อกดบันทึก
    เข้าหน้าลูกค้า
    กดปุ่มตั้งค่า
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="nick_name"]/div/label             300
    Click Element                        //*[@id="nick_name"]/div/label
    Sleep                                1
    Wait Until Element Is Visible        id:settingconfirm              300
    Click Element                        id:settingconfirm
    Sleep                                1
    Wait Until Element Is Not Visible    id:settingModal                300
    กดปุ่มเพิ่ม
    Sleep                                1
    พบข้อความ                            ชื่อเล่น
    ปิดPopupเพิ่ม

test4 ทดสอบการกดเลือก เชื้อชาติ สำเร็จ เมื่อกดบันทึก
    เข้าหน้าลูกค้า
    กดปุ่มตั้งค่า
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="ethnicity"]/div/label             300
    Click Element                        //*[@id="ethnicity"]/div/label
    Sleep                                1
    Wait Until Element Is Visible        id:settingconfirm              300
    Click Element                        id:settingconfirm
    Sleep                                1
    Wait Until Element Is Not Visible    id:settingModal                300
    กดปุ่มเพิ่ม
    Sleep                                1
    พบข้อความ                            เชื้อชาติ
    ปิดPopupเพิ่ม

test5 ทดสอบการกดยกเลิก ชื่อเล่น สำเร็จ เมื่อกดบันทึก
    เข้าหน้าลูกค้า
    กดปุ่มตั้งค่า
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="nick_name"]/div/label             300
    Click Element                        //*[@id="nick_name"]/div/label
    Sleep                                1
    Wait Until Element Is Visible        id:settingconfirm              300
    Click Element                        id:settingconfirm
    Sleep                                1
    Wait Until Element Is Not Visible    id:settingModal                300
    กดปุ่มเพิ่ม
    Sleep                                1
    ไม่พบข้อความ                         ชื่อเล่น
    ปิดPopupเพิ่ม

test6 ทดสอบการกดยกเลิก เชื้อชาติ สำเร็จ เมื่อกดบันทึก
    เข้าหน้าลูกค้า
    กดปุ่มตั้งค่า
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="ethnicity"]/div/label             300
    Click Element                        //*[@id="ethnicity"]/div/label
    Sleep                                1
    Wait Until Element Is Visible        id:settingconfirm              300
    Click Element                        id:settingconfirm
    Sleep                                1
    Wait Until Element Is Not Visible    id:settingModal                300
    กดปุ่มเพิ่ม
    Sleep                                1
    ไม่พบข้อความ                         เชื้อชาติ
    ปิดPopupเพิ่ม

test7 ทดสอบการบันทึกpop upตั้งค่าข้อมูลไม่สำเร็จเมื่อกดกากบาท
    เข้าหน้าลูกค้า
    กดปุ่มตั้งค่า
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="ethnicity"]/div/label             300
    Click Element                        //*[@id="ethnicity"]/div/label
    Sleep                                1
    Wait Until Element Is Visible        id:CloseSettingModal                   300
    Click Element                        id:CloseSettingModal
    Sleep                                1
    Wait Until Element Is Not Visible    id:settingModal                300
    กดปุ่มเพิ่ม
    Sleep                                1
    ไม่พบข้อความ                         เชื้อชาติ
    ปิดPopupเพิ่ม

test8 ทดสอบการบันทึกpop upตั้งค่าข้อมูลไม่สำเร็จเมื่อกดปุ่มปิด
    เข้าหน้าลูกค้า
    กดปุ่มตั้งค่า
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="ethnicity"]/div/label             300
    Click Element                        //*[@id="ethnicity"]/div/label
    Sleep                                1
    Wait Until Element Is Visible        id:settingcancel               300
    Click Element                        id:settingcancel
    Sleep                                1
    Wait Until Element Is Not Visible    id:settingModal                300
    กดปุ่มเพิ่ม
    Sleep                                1
    ไม่พบข้อความ                         เชื้อชาติ
    ปิดPopupเพิ่ม

test9 ทดสอบการบันทึกpop upตั้งค่าข้อมูลสำเร็จ
    เข้าหน้าลูกค้า
    กดปุ่มตั้งค่า
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="nick_name"]/div/label             300
    Click Element                        //*[@id="nick_name"]/div/label
    Sleep                                1
    Wait Until Element Is Visible        id:settingconfirm              300
    Click Element                        id:settingconfirm
    Sleep                                1
    Wait Until Element Is Not Visible    id:settingModal                300
    กดปุ่มเพิ่ม
    Sleep                                1
    พบข้อความ                            ชื่อเล่น
    ปิดPopupเพิ่ม


*** Keywords ***
กดปุ่มตั้งค่า
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="root-content"]/div/form/div/div[2]/button[2]              300
    Sleep                                1
    Click Element                        //*[@id="root-content"]/div/form/div/div[2]/button[2]
    Sleep                                5
    Wait Until Element Is Visible        id:settingModal                300

กดปุ่มเพิ่ม
    Wait Until Page Contains Element     //*[@id="root-content"]/div/form/div/div[2]/button[1]              300
    Click Element                        //*[@id="root-content"]/div/form/div/div[2]/button[1]
    Wait Until Element Is Visible        id:AddCustomerModal                300