*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบและinit_data     searchCustomer
Test Setup        เข้าหน้าลูกค้า
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    Close Browser
*** Variables ***

*** Test cases ***
Test50 ทดสอบการค้นหาด้วยรหัสลูกค้า
    Sleep                               1
    ค้นหาลูกค้าด้วย                     C000001
    Sleep                               1
    กดปุ่มค้นหา
    Sleep                               1
    พบข้อความ                           C000001
    #แสดงผลเฉพาะรหัสลูกค้าที่พิมพ์
    ไม่พบข้อความ                        C000002
    #ไม่แสดงผลรหัสลูกค้าอื่น
    Sleep                               5
    จำนวนข้อมูลลูกค้าที่ต้องการค้นหา    1

Test51 ทดสอบการค้นหาด้วยรหัสลูกค้าที่ไม่มีในระบบ
    Sleep                               1
    ค้นหาลูกค้าด้วย                     C000004
    Sleep                               1
    กดปุ่มค้นหา
    Sleep                               1
    ไม่พบข้อความ                        C000004
    #ไม่แสดงผลรหัสลูกค้าอื่น
    Sleep                               5
    จำนวนข้อมูลลูกค้าที่ต้องการค้นหา    0

Test52 ทดสอบการค้นหาด้วยชื่อลูกค้า
    Sleep                               1
    ค้นหาลูกค้าด้วย                     John
    Sleep                               1
    กดปุ่มค้นหา
    Sleep                               1
    พบข้อความ                           John
    #แสดงผลเฉพาะชื่อลูกค้าที่พิมพ์
    ไม่พบข้อความ                        จอห์น
    #ไม่แสดงผลชื่ออื่น
    Sleep                               5
    จำนวนข้อมูลลูกค้าที่ต้องการค้นหา    1

Test53 ทดสอบการค้นหาด้วยชื่อลูกค้าที่ไม่มีในระบบ
    Sleep                               1
    ค้นหาลูกค้าด้วย                     Robert
    Sleep                               1
    กดปุ่มค้นหา
    Sleep                               1
    ไม่พบข้อความ                        Robert
    #ไม่แสดงผลรหัสลูกค้าอื่น
    Sleep                               5
    จำนวนข้อมูลลูกค้าที่ต้องการค้นหา    0

Test54 ทดสอบการค้นหาด้วยเบอร์มือถือ
    Sleep                               1
    ค้นหาลูกค้าด้วย                     0802222229
    Sleep                               1
    กดปุ่มค้นหา
    Sleep                               1
    พบข้อความ                           0802222229
    #แสดงผลเฉพาะเบอร์มือถือที่พิมพ์
    ไม่พบข้อความ                        0802222222
    #ไม่แสดงผลเบอร์มือถืออื่น
    Sleep                               5
    จำนวนข้อมูลลูกค้าที่ต้องการค้นหา    1

Test55 ทดสอบการค้นหาด้วยเบอร์มือถือที่ไม่มีในระบบ
    Sleep                               1
    ค้นหาลูกค้าด้วย                     0802222220
    Sleep                               1
    กดปุ่มค้นหา
    Sleep                               1
    ไม่พบข้อความ                        0802222220
    #ไม่แสดงผลรหัสลูกค้าอื่น
    Sleep                               5
    จำนวนข้อมูลลูกค้าที่ต้องการค้นหา    0

Test56 ทดสอบการค้นหาด้วยเลขประจำตัวประชาชน
    Sleep                               1
    ค้นหาลูกค้าด้วย                     1111111111111
    Sleep                               1
    กดปุ่มค้นหา
    Sleep                               1
    พบข้อความ                           1111111111111
    #แสดงผลเฉพาะเลขประจำตัวประชาชนที่พิมพ์
    ไม่พบข้อความ                        1111111111112
    #ไม่แสดงผลเลขประจำตัวประชาชนอื่น
    Sleep                               5
    จำนวนข้อมูลลูกค้าที่ต้องการค้นหา    1

Test57 ทดสอบการค้นหาด้วยเลขประจำตัวประชาชนที่ไม่มีในระบบ
    Sleep                               1
    ค้นหาลูกค้าด้วย                     1111111111113
    Sleep                               1
    กดปุ่มค้นหา
    Sleep                               1
    ไม่พบข้อความ                        1111111111113
    #ไม่แสดงผลรหัสลูกค้าอื่น
    Sleep                               5
    จำนวนข้อมูลลูกค้าที่ต้องการค้นหา    0

Test58 ทดสอบการค้นหาด้วยเลขประจำตัวประชาชน
    Sleep                               1
    ค้นหาลูกค้าด้วย                     linda@mail.com
    Sleep                               1
    กดปุ่มค้นหา
    Sleep                               1
    พบข้อความ                           linda@mail.com
    #แสดงผลเฉพาะอีเมลที่พิมพ์
    ไม่พบข้อความ                        John@mail.com
    #ไม่แสดงผลอีเมลอื่น
    Sleep                               5
    จำนวนข้อมูลลูกค้าที่ต้องการค้นหา    1

Test59 ทดสอบการค้นหาด้วยเลขประจำตัวประชาชนที่ไม่มีในระบบ
    Sleep                               1
    ค้นหาลูกค้าด้วย                     Robert@mail.com
    Sleep                               1
    กดปุ่มค้นหา
    Sleep                               1
    ไม่พบข้อความ                        Robert@mail.com
    #ไม่แสดงผลรหัสลูกค้าอื่น
    Sleep                               5
    จำนวนข้อมูลลูกค้าที่ต้องการค้นหา    0

*** Keywords ***
ค้นหาลูกค้าด้วย
    [Arguments]                         ${value}
    Wait Until Page Contains Element    //*[@id="root-segment"]/form[1]/div/div[1]/div/input                300
    Input Text                          //*[@id="root-segment"]/form[1]/div/div[1]/div/input      ${value}

กดปุ่มค้นหา
    Wait Until Page Contains Element    //*[@id="root-segment"]/form[1]/div/div[2]/button[2]/i              300
    Double Click Element                //*[@id="root-segment"]/form[1]/div/div[2]/button[2]/i

จำนวนข้อมูลลูกค้าที่ต้องการค้นหา
    [Arguments]                         ${value}
    ${count} =                          Get Element Count                                         id:tableSeachCustomerName
    ${count_str} =                      Convert To String                                         ${count}
    Should Be Equal                     ${count_str}                                              ${value}