*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบและinit_data Infocustomer
Test Setup        เข้าหน้าลูกค้า
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data Infocustomer
*** Variables ***

*** Test cases ***
Test31 ทดสอบการเข้าหน้า pop up ข้อมูลลูกค้าของ user C000003
    เข้าหน้าCustomer Profile             3
    Sleep                                10
    Wait Until Element Is Visible        id:cusInfoModal                                                                                                   300
    พบข้อความ                            C000003
    พบข้อความ                            linda
    พบข้อความ                            lin
    พบข้อความ                            111/11
    พบข้อความ                            Lat Krabang
    พบข้อความ                            Lat Krabang
    พบข้อความ                            BKK
    พบข้อความ                            10520
    พบข้อความ                            0808592229
    พบข้อความ                            06/02/1988
    พบข้อความ                            หญิง
    พบข้อความ                            thai
    พบข้อความ                            thai
    พบข้อความ                            1111111111112
    พบข้อความ                            0802222229
                #เจอข้อมูลลูกค้า
    ปิดpopup

Test32 ทดสอบการแสดงข้อมูลที่อยู่2ของ user C000002
    เข้าหน้าCustomer Profile             2
    Sleep                                10
    Wait Until Element Is Visible        id:cusInfoModal                                                                                                   300
    Sleep                                1
    พบข้อความ                            C000002
    Sleep                                1
    เข้าหน้าที่อยู่2
    Sleep                                1
    พบข้อความ                            111/12
    พบข้อความ                            Bang Kapi
    พบข้อความ                            Bang Kapi
    พบข้อความ                            BKK
    พบข้อความ                            10240
                #พบข้อมูลที่อยู่2
    ปิดpopup

Test33 ทดสอบการแสดงข้อมูลที่อยู่3ของ user C000001
    เข้าหน้าCustomer Profile             1
    Sleep                                10
    Wait Until Element Is Visible        id:cusInfoModal                                                                                                   300
    Sleep                                1
    พบข้อความ                            C000001
    พบข้อความ                            ลูกค้าทั่วไป
    Sleep                                1
    เข้าหน้าที่อยู่3
    Sleep                                1
    พบข้อความ                            Bang Bon
    พบข้อความ                            Bang Bon
    พบข้อความ                            BKK
    พบข้อความ                            10040
                #พบข้อมูลที่อยู่3
    ปิดpopup

Test34 ทดสอบการแสดงประวัติซื้อขายทองในส่วนของซื้อทองรูปพรรณ ของ user C000003
    เข้าหน้าCustomer Profile             3
    Sleep                                10
    Wait Until Element Is Visible        id:cusInfoModal                                                                                                   300
    พบข้อความ                            C000003
    พบข้อความ                            linda
    Sleep                                1
    เข้าหน้าซื้อ-ขายทอง
    Sleep                                1
    พบข้อความ                            E96G100
    พบข้อความ                            1,349.00
                #เจอข้อมูลซื้อทองรูปพรรณ
    ปิดpopup

Test34.1 ทดสอบการแสดงประวัติซื้อขายทองในส่วนของขายทองรูปพรรณ ของ user C000002
    เข้าหน้าCustomer Profile             2
    Sleep                                10
    Wait Until Element Is Visible        id:cusInfoModal                                                                                                   300
    พบข้อความ                            C000002
    พบข้อความ                            John
    Sleep                                1
    เข้าหน้าซื้อ-ขายทอง
    Sleep                                1
    เข้าหน้าขายทองรูปพรรณ
    Sleep                                1
    พบข้อความ                            สร้อย1บาท
    พบข้อความ                            15.200
                #เจอข้อมูลขายทองรูปพรรณ
    ปิดpopup

Test34.2 ทดสอบการแสดงประวัติซื้อขายทองในส่วนของซื้อทองรูปแท่ง ของ user C000003
    เข้าหน้าCustomer Profile             3
    Sleep                                10
    Wait Until Element Is Visible        id:cusInfoModal                                                                                                   300
    Sleep                                1
    พบข้อความ                            C000003
    พบข้อความ                            linda
    Sleep                                1
    เข้าหน้าซื้อ-ขายทอง
    Sleep                                1
    เข้าหน้าซื้อทองแท่ง
    Sleep                                1
    พบข้อความ                            B965B500
    พบข้อความ                            102,500.00
                #เจอข้อมูลซื้อทองรูปแท่ง
    ปิดpopup

Test34.3 ทดสอบการแสดงประวัติซื้อขายทองในส่วนของขายทองแท่ง ของ user C000003
    เข้าหน้าCustomer Profile             3
    Sleep                                10
    Wait Until Element Is Visible        id:cusInfoModal                                                                                                   300
    Sleep                                1
    พบข้อความ                            C000003
    พบข้อความ                            linda
    Sleep                                1
    เข้าหน้าซื้อ-ขายทอง
    Sleep                                1
    เข้าหน้าขายทองแท่ง
    Sleep                                1
    พบข้อความ                            ทองแท่ง1บาท
    พบข้อความ                            20,200.00
                #เจอข้อมูลซื้อทองรูปแท่ง
    ปิดpopup

Test35 ทดสอบการแสดงข้อมูลการขายฝากของ user C000002
    เข้าหน้าCustomer Profile             2
    Sleep                                10
    Wait Until Element Is Visible        id:cusInfoModal                                                                                                   300
    Sleep                                1
    พบข้อความ                            C000002
    พบข้อความ                            John
    Sleep                                1
    เข้าหน้าขายฝาก
    Sleep                                1
    พบข้อความ                            ลาดกระบัง
    พบข้อความ                            AA01001
    พบข้อความ                            20000.00

                #เจอข้อมูลขายฝาก
    ปิดpopup

Test36 ทดสอบการแสดงข้อมูลคะแนน ของ user C000002
    เข้าหน้าCustomer Profile             2
    Sleep                                10
    Wait Until Element Is Visible        id:cusInfoModal                                                                                                   300
    Sleep                                1
    พบข้อความ                            C000002
    พบข้อความ                            John
    Sleep                                1
    เข้าหน้าคะแนน
    Sleep                                1
    พบข้อความ                            1,160
    พบข้อความ                            ไถ่คืน
    พบข้อความ                            860
                #เจอข้อมูลขายฝาก
    ปิดpopup

Test37 ทดสอบการคำนวณแต้มให้ลูกค้า เมื่อมีการถอนเงินจากการออมทอง (4,000) ของ user C000002
    เข้าหน้าCustomer Profile             2
    Sleep                                10
    Wait Until Element Is Visible        id:cusInfoModal                                                                                                   300
    Sleep                                1
    พบข้อความ                            C000002
    พบข้อความ                            John
    Sleep                                1
    เข้าหน้าคะแนน
    Sleep                                1
    กดปุ่มเพิ่มเเต้มออมทอง
    Sleep                                1
    บันทึกยอดถอน                         4000
    Sleep                                5
    ${value} =                           Get Value                                                                                                         //*[@id="inputPoint"]
    Should Be Equal                      ${value}                                                                                                          200
    Sleep                                1
    กดปุ่มบันทึก หน้าเพิ่มแต้มออมทอง
    Sleep                                1
    Wait Until Element Is Visible        id:cusInfoModal                                                                                                   300
    Sleep                                1
    พบข้อความ                            ออมทอง
    พบข้อความ                            200
                #แต้มเพิ่ม 200 แต้ม
    พบข้อความ                            1,360
                #เจอข้อมูลขายฝาก
    ปิดpopup

Test38 ทดสอบการคำนวณแต้มให้ลูกค้า เมื่อมีการถอนเงิน 0 บาท ของ user C000002
    เข้าหน้าCustomer Profile             2
    Sleep                                10
    Wait Until Element Is Visible        id:cusInfoModal                                                                                                   300
    Sleep                                1
    พบข้อความ                            C000002
    พบข้อความ                            John
    Sleep                                1
    เข้าหน้าคะแนน
    Sleep                                1
    กดปุ่มเพิ่มเเต้มออมทอง
    Sleep                                1
    บันทึกยอดถอน                         0
    Sleep                                1
    พบข้อความ                            0
    Sleep                                1
    กดปุ่มบันทึก หน้าเพิ่มแต้มออมทอง
    Sleep                                1
    พบข้อความ                            ต้องมากกว่า 1
                #ข้อความแจ้งเตือน "ต้องการมากกว่า 1"
    ปิดpopupเพิ่มแต้มออมทอง
    ปิดpopup

Test39 ทดสอบการคำนวณแต้มให้ลูกค้า เมื่อไม่ใส่ยอดถอนเงิน ของ user C000002
    เข้าหน้าCustomer Profile             2
    Sleep                                10
    Wait Until Element Is Visible        id:cusInfoModal                                                                                                   300
    Sleep                                1
    พบข้อความ                            C000002
    พบข้อความ                            John
    Sleep                                1
    เข้าหน้าคะแนน
    Sleep                                1
    กดปุ่มเพิ่มเเต้มออมทอง
    Sleep                                1
    กดปุ่มบันทึก หน้าเพิ่มแต้มออมทอง
    Sleep                                1
    พบข้อความ                            ยอดถอน ฟิลด์นี้จำเป็น
    พบข้อความ                            คะแนน ฟิลด์นี้จำเป็น
                #ข้อความแจ้งเตือน"ฟิลด์นี้จำเป็น" ทั้งยอดถอน และ คะแนน
    ปิดpopupเพิ่มแต้มออมทอง
    ปิดpopup

Test40 ทดสอบการคำนวณแต้มให้ลูกค้า เมื่อมีการถอนเงินจากการออมทองเท่ากับค่าสูงสุดที่คำนวณ (10,000) ของ user C000002
    Sleep                                1
    เข้าหน้าCustomer Profile             2
    Sleep                                10
    Wait Until Element Is Visible        id:cusInfoModal  
    Sleep                                1                                                                                                 300
    พบข้อความ                            C000002
    Sleep                                1
    พบข้อความ                            John
    Sleep                                1
    เข้าหน้าคะแนน
    Sleep                                1
    กดปุ่มเพิ่มเเต้มออมทอง
    Sleep                                10
    บันทึกยอดถอน                         10000
    Sleep                                1
    ${value} =                           Get Value                                                                                                         //*[@id="inputPoint"]
    Should Be Equal                      ${value}                                                                                                          500
    Sleep                                3
    กดปุ่มบันทึก หน้าเพิ่มแต้มออมทอง
    Sleep                               3
    Wait Until Element Is Visible        id:cusInfoModal                                                                                                   300
    Sleep                                3
    พบข้อความ                            ออมทอง
    Sleep                                3
    พบข้อความ                            500
    Sleep                                10
    #แต้มเพิ่ม 500 แต้ม
    ${score} =                          Get Text                                        //*[@id="table_width"]/div[4]/div/div[3]/div[1]/div/div/div[2]/div/div[4]/div/div/div/div/div
    Should Be Equal                      ${score}                                                                                                          1,860
    Sleep                                3
    ปิดpopup

Test41 ทดสอบการคำนวณแต้มให้ลูกค้า เมื่อมีการถอนเงินจากการออมทองเกินค่าสูงสุดที่คำนวณ ของ user C000002
    เข้าหน้าCustomer Profile             2
    Sleep                                10
    Wait Until Element Is Visible        id:cusInfoModal                                                                                                   300
    Sleep                                1
    พบข้อความ                            C000002
    พบข้อความ                            John
    Sleep                                1
    เข้าหน้าคะแนน
    Sleep                                1
    กดปุ่มเพิ่มเเต้มออมทอง
    Sleep                                1
    บันทึกยอดถอน                         11000
    Sleep                                1
    ${value} =                           Get Value                                                                                                         //*[@id="inputPoint"]
    Should Be Equal                      ${value}                                                                                                          500
    Sleep                                1
    กดปุ่มบันทึก หน้าเพิ่มแต้มออมทอง
    Sleep                                1
    Wait Until Element Is Visible        id:cusInfoModal                                                                                                   300
    Sleep                                1
    พบข้อความ                            ออมทอง
    พบข้อความ                            500
                #แต้มเพิ่ม 500 แต้ม
    พบข้อความ                            2,360
    ปิดpopup

Test42 ทดสอบการแสดงรายชื่อลูกค้า เรียงตามรหัสตัวเลขจากน้อยไปมาก
    Sleep                                10
    ตรวจสอบรหัสลูกค้าบรรทัดที่           0                                                                                                                 C000001
    Sleep                                5
    filterรหัสลูกค้า
    Sleep                                5
    ตรวจสอบรหัสลูกค้าบรรทัดที่           0                                                                                                                 C000003
                #เจอรายชื่อลูกค้าเรียงตามรหัสลูกค้าจากน้อยไปมาก

*** Keywords ***

เข้าหน้าCustomer Profile
    [Arguments]                          ${value}
    Wait Until Page Contains Element     //*[@id="root-segment"]/div/div/div/div[3]/div[${value}]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]    300
    Double Click Element                 //*[@id="root-segment"]/div/div/div/div[3]/div[${value}]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]
    Wait Until Element Is Visible        id:cusInfoModal                                                                                                   300

เข้าหน้าที่อยู่1
    Wait Until Element Is Visible        id:cusInfo                                                                                                        300
    Double Click Element                 id:cusInfo

เข้าหน้าที่อยู่2
    Wait Until Element Is Visible        id:add2                                                                                                           300
    Double Click Element                 id:add2

เข้าหน้าที่อยู่3
    Wait Until Element Is Visible        id:add3                                                                                                           300
    Double Click Element                 id:add3

เข้าหน้าซื้อ-ขายทอง
    Wait Until Element Is Visible        id:posPage                                                                                                        300
    Double Click Element                 id:posPage

เข้าหน้าขายฝาก
    Wait Until Element Is Visible        id:leasePage                                                                                                      300
    Double Click Element                 id:leasePage

เข้าหน้าคะแนน
    Wait Until Element Is Visible        id:pointPage                                                                                                      300
    Double Click Element                 id:pointPage

เข้าหน้าซื้อทองรูปพรรณ
    Wait Until Element Is Visible        id:buyGold1                                                                                                       300
    Double Click Element                 id:buyGold1

เข้าหน้าขายทองรูปพรรณ
    Wait Until Element Is Visible        id:sellGold1                                                                                                      300
    Double Click Element                 id:sellGold1

เข้าหน้าซื้อทองแท่ง
    Wait Until Element Is Visible        id:buyGold2                                                                                                       300
    Double Click Element                 id:buyGold2

เข้าหน้าขายทองแท่ง
    Wait Until Element Is Visible        id:sellGold2                                                                                                      300
    Double Click Element                 id:sellGold2

ปิดpopup
    Wait Until Element Is Visible        id:btnClose                                                                                                       300
    Double Click Element                 id:btnClose
    Sleep                                1
    Wait Until Element Is Not Visible    id:cusInfoModal                                                                                                   300

ปิดpopupเพิ่มแต้มออมทอง
    Wait Until Element Is Visible        id:btnClosemodalAddPoint                                                                                          300
    Double Click Element                 id:btnClosemodalAddPoint
    Sleep                                1
    Wait Until Element Is Not Visible    id:modalAddPoint                                                                                                  300

กดปุ่มเพิ่มเเต้มออมทอง
    Wait Until Element Is Visible        id:btnAddPoint                                                                                                    300
    Double Click Element                 id:btnAddPoint

พบหน้าเพิ่มแต้มออมทอง
    Wait Until Element Is Visible        id:modalAddPoint                                                                                                  300
    Wait Until Element Is Visible        id:inputWithdraw                                                                                                  300
    Wait Until Element Is Visible        id:inputPoint                                                                                                     300
    Wait Until Element Is Visible        id:btnSave                                                                                                        300
    Wait Until Element Is Visible        id:btnCancel                                                                                                      300

บันทึกยอดถอน
    [Arguments]                          ${value}
    Wait Until Element Is Visible        id:inputWithdraw                                                                                                  300
    Input Text                           id:inputWithdraw                                                                                                  ${value}

กดปุ่มบันทึก หน้าเพิ่มแต้มออมทอง
    Wait Until Element Is Visible        id:btnSave                                                                                                        300
    Click Element                 id:btnSave

ตรวจสอบรหัสลูกค้าบรรทัดที่
    [Arguments]                          ${number}                                                                                                         ${text}
    Wait Until Page Contains Element     id:${number}                                                                                                      300
    ${value} =                           Get Text                                                                                                          id:${number}
    Should Be Equal                      ${value}                                                                                                          ${text}

filterรหัสลูกค้า
    Wait Until Page Contains Element     //*[@id="root-segment"]/div/div/div/div[2]/div/div/div[2]/div/div[2]/div/div/div/div                              300
    Sleep                                1
    Click Element                        //*[@id="root-segment"]/div/div/div/div[2]/div/div/div[2]/div/div[2]/div/div/div/div
    Sleep                                1