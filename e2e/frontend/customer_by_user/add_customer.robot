*** Settings ***
Library           Selenium2Library
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบและinit_data                 searchCustomer
Test Setup        เข้าหน้าลูกค้า
Test Teardown     เข้าสู่หน้าหลักและinit_data2อัน           searchCustomer    clearSettingCustomer
Suite Teardown    Close Browser
*** Variables ***

*** Test cases ***
test10 ทดสอบการบันทึกไม่สำเร็จเมื่อกดปุ่มกากบาท
    Sleep                               1
    กดปุ่มเพิ่ม
    Sleep                               1
    กรอกชื่อ                            daniel
    Sleep                               1
    ปิดPopupเพิ่ม
    Sleep                               1
    พบข้อความ                           ลูกค้า
    ไม่พบข้อความ                        daniel

test11 ทดสอบการเพิ่มข้อมูลลูกค้า ว่าเพิ่มไม่สำเร็จเมื่อคลิกปุ่มยกเลิก
    Sleep                               1
    กดปุ่มเพิ่ม
    Sleep                               1
    กรอกชื่อ                            daniel
    Sleep                               1
    กดปุ่มยกเลิก
    Sleep                               1
    พบข้อความ                           ลูกค้า
    Sleep                               1
    ไม่พบข้อความ                        daniel

test12 ทดสอบการเพิ่มลูกค้าสำเร็จโดยเลือกไม่ประสงค์จะใช้งานแอพพลิเคชั่น โดยกรอกแค่ชื่อเพียงอย่างเดียว
    Sleep                               3
    กดปุ่มเพิ่ม
    Sleep                               3
    Wait Until Element Is Visible       //*[@id="AddCustomerModal"]/div[2]/div/form/div[2]/div                                                            300
    Sleep                               3
    Click Element                       //*[@id="AddCustomerModal"]/div[2]/div/form/div[2]/div
    Sleep                               3
    กรอกชื่อ                            daniel
    Sleep                               3
    กดปุ่มบันทึก
    Sleep                               10
    ตรวจพบข้อความบน Alert               บันทึกข้อมูลสำเร็จ

test13 ทดสอบการเพิ่มลูกค้าที่ไม่ประสงค์จะใช้งานแอพพลิเคชั่น ไม่สำเร็จ เมื่อกรอกชื่อ ด้วยค่าว่าง
    กดปุ่มเพิ่ม
    Sleep                               1
    กดปุ่มบันทึก
    Sleep                               1
    พบข้อความ                           ชื่อ *ต้องไม่เป็นค่าว่าง
    กดปุ่มยกเลิก

test14 ทดสอบการเพิ่มลูกค้าที่ไม่ประสงค์จะใช้งานแอพพลิเคชั่น เมื่อกรอกชื่อ ที่มีอยู่แล้ว
    Sleep                               1
    กดปุ่มเพิ่ม
    กรอกชื่อ                            daniel
    Sleep                               1
    กดปุ่มบันทึก
    Sleep                               1
    ตรวจพบข้อความบน Alert               เบอร์มือถือหรือ อีเมล ต้องไม่เป็นค่าว่าง
    Sleep                               1
    กดปุ่มยกเลิก

test15 ทดสอบการเพิ่มลูกค้าที่ต้องการใช้งานแอพพลิเคชั่น ไม่สำเร็จ ด้วยการกรอกเบอร์มือถือเป็นตัวหนังสือ
    กดปุ่มเพิ่ม
    กรอกชื่อ                            daniel
    กรอกเบอร์มือถือ                     invalid
    Sleep                               1
    กดปุ่มบันทึก
    Sleep                               1
    Wait Until Page Contains            เบอร์มือถือไม่ถูกต้อง                                                                                             300
    กดปุ่มยกเลิก

test16 ทดสอบการเพิ่มลูกค้าที่ต้องการใช้งานแอพพลิเคชั่น ไม่สำเร็จ ด้วยการกรอกเบอร์มือถือ 9 หลัก
    กดปุ่มเพิ่ม
    กรอกชื่อ                            daniel
    กรอกเบอร์มือถือ                     098888888
    Sleep                               1
    กดปุ่มบันทึก
    Sleep                               1
    Wait Until Page Contains            เบอร์มือถือไม่ถูกต้อง                                                                                             300
    กดปุ่มยกเลิก

test17 ทดสอบการเพิ่มลูกค้าที่ต้องการใช้งานแอพพลิเคชั่น ไม่สำเร็จ ด้วยการกรอกเบอร์มือถือขึ้นต้นด้วยเลข 8
    กดปุ่มเพิ่ม
    กรอกชื่อ                            daniel
    กรอกเบอร์มือถือ                     8888888888
    Sleep                               1
    กดปุ่มบันทึก
    Sleep                               1
    Wait Until Page Contains            เบอร์มือถือไม่ถูกต้อง                                                                                             300
    กดปุ่มยกเลิก

test18 ทดสอบการเพิ่มลูกค้าที่ต้องการใช้งานแอพพลิเคชั่น ไม่สำเร็จ ด้วยการกรอกเบอร์มือถือด้วยค่าว่าง
    Sleep                               1
    กดปุ่มเพิ่ม
    กรอกชื่อ                            daniel
    Sleep                               1
    กดปุ่มบันทึก
    Sleep                               1
    ตรวจพบข้อความบน Alert               เบอร์มือถือหรือ อีเมล ต้องไม่เป็นค่าว่าง
    กดปุ่มยกเลิก

test19 ทดสอบการเพิ่มลูกค้าที่ต้องการใช้งานแอพพลิเคชั่น ไม่สำเร็จ ด้วยการกรอกเบอร์มือถือด้วยข้อมูลที่มีอยู่แล้ว
    กดปุ่มเพิ่ม
    กรอกชื่อ                            daniel
    กรอกเบอร์มือถือ                     0802222222
    Sleep                               1
    กดปุ่มบันทึก
    Sleep                               1
    Wait Until Page Contains            เบอร์มือถือcustomer และ mobile มีอยู่แล้ว                                                                         300
    กดปุ่มยกเลิก

test20 ทดสอบการเพิ่มลูกค้าที่ต้องการใช้งานแอพพลิเคชั่น ไม่สำเร็จ ด้วยการกรอกอีเมลโดยไม่ใส่ @
    กดปุ่มเพิ่ม
    กรอกชื่อ                            daniel
    กรอกอีเมล                           email
    Sleep                               1
    กดปุ่มบันทึก
    Sleep                               1
    Wait Until Page Contains            ป้อนที่อยู่อีเมลที่ถูกต้อง                                                                                        300
    กดปุ่มยกเลิก

test21 ทดสอบการเพิ่มลูกค้าที่ต้องการใช้งานแอพพลิเคชั่น ไม่สำเร็จ ด้วยการกรอกอีเมลด้วยค่าว่าง
    Sleep                               1
    กดปุ่มเพิ่ม
    กรอกชื่อ                            daniel
    Sleep                               1
    กดปุ่มบันทึก
    Sleep                               1
    ตรวจพบข้อความบน Alert               เบอร์มือถือหรือ อีเมล ต้องไม่เป็นค่าว่าง
    กดปุ่มยกเลิก

test22 ทดสอบการเพิ่มลูกค้าที่ต้องการใช้งานแอพพลิเคชั่น ไม่สำเร็จ ด้วยการกรอกอีเมลด้วยข้อมูลที่มีอยู่แล้ว
    กดปุ่มเพิ่ม
    กรอกชื่อ                            daniel
    กรอกอีเมล                           John@mail.com
    Sleep                               1
    กดปุ่มบันทึก
    Sleep                               1
    Wait Until Page Contains            อีเมลcustomer และ email มีอยู่แล้ว                                                                                300
    กดปุ่มยกเลิก

test23 ทดสอบการเพิ่มลูกค้าที่ต้องการใช้งานแอพพลิเคชั่น กรอกชื่อและเบอร์มือถือ / สำเร็จ
    Sleep                               1
    กดปุ่มเพิ่ม
    กรอกชื่อ                            daniel
    กรอกเบอร์มือถือ                     0877920941
    Sleep                               1
    กดปุ่มบันทึก
    Sleep                               1
    ตรวจพบข้อความบน Alert               บันทึกข้อมูลสำเร็จ
    Wait Until Page Contains            daniel                                                                                                            300
    Wait Until Page Contains            0877920941                                                                                                        300

test24 ทดสอบการเพิ่มลูกค้าที่ต้องการใช้งานแอพพลิเคชั่น กรอกชื่อและอีเมล / สำเร็จ
    Sleep                               1
    กดปุ่มเพิ่ม
    กรอกชื่อ                            daniel
    กรอกอีเมล                           daniel@mail.com
    Sleep                               1
    กดปุ่มบันทึก
    Sleep                               1
    ตรวจพบข้อความบน Alert               บันทึกข้อมูลสำเร็จ
    Wait Until Page Contains            daniel                                                                                                            300
    Wait Until Page Contains            daniel@mail.com                                                                                                   300

test25 ทดสอบการเพิ่มที่อยู่ 1 สำเร็จ
    กดปุ่มตั้งค่า
    Sleep                               1
    Wait Until Page Contains Element    //*[@id="address"]/div/label                                                                                      300
    Click Element                       //*[@id="address"]/div/label
    Sleep                               1
    Wait Until Page Contains Element    id:settingconfirm                                                                                                 300
    Click Element                       id:settingconfirm
    Sleep                               1
    กดปุ่มเพิ่ม
    กรอกชื่อ                            jackson
    กรอกอีเมล                           jackson@mail.com
    กรอกที่อยู่                         bangkok
    Sleep                               1
    กดปุ่มบันทึก
    Sleep                               1
    ตรวจพบข้อความบน Alert               บันทึกข้อมูลสำเร็จ
    เข้าหน้าCustomer Profile            4
    Wait Until Page Contains            bangkok                                                                                                           300
    Wait Until Page Contains Element    //*[@id="btnClose"]                                                                                               300
    Sleep                               1
    Click Element                       //*[@id="btnClose"]

test26 ทดสอบการเพิ่มที่อยู่ 2 สำเร็จ
    กดปุ่มเพิ่ม
    กรอกชื่อ                            danik
    กรอกอีเมล                           danik@mail.com
    Wait Until Page Contains Element    //*[@id="AddCustomerModal"]/div[2]/div/form/div[3]/a[2]                                                           300
    Click Element                       //*[@id="AddCustomerModal"]/div[2]/div/form/div[3]/a[2]
    Sleep                               1
    Input Text                          //*[@id="AddCustomerModal"]/div[2]/div/form/div[4]/div/div[1]/div/input                                           bangkok
    Sleep                               1
    กดปุ่มบันทึก
    Sleep                               1
    ตรวจพบข้อความบน Alert               บันทึกข้อมูลสำเร็จ
    เข้าหน้าCustomer Profile            4
    Sleep                               1
    Wait Until Page Contains Element    //*[@id="add2"]                                                                                                   300
    Sleep                               1
    Click Element                       //*[@id="add2"]
    Wait Until Page Contains            bangkok                                                                                                           300
    Sleep                               1
    Wait Until Page Contains Element    //*[@id="btnClose"]                                                                                               300
    Click Element                       //*[@id="btnClose"]

test27 ทดสอบการเพิ่มที่อยู่ 3 สำเร็จ
    กดปุ่มเพิ่ม
    กรอกชื่อ                            dan
    กรอกอีเมล                           dan@mail.com
    Wait Until Page Contains Element    //*[@id="AddCustomerModal"]/div[2]/div/form/div[3]/a[3]                                                           300
    Click Element                       //*[@id="AddCustomerModal"]/div[2]/div/form/div[3]/a[3]
    Sleep                               1
    Input Text                          //*[@id="AddCustomerModal"]/div[2]/div/form/div[4]/div/div[1]/div/input                                           bangkok
    Sleep                               1
    กดปุ่มบันทึก
    Sleep                               1
    ตรวจพบข้อความบน Alert               บันทึกข้อมูลสำเร็จ
    เข้าหน้าCustomer Profile            4
    Sleep                               1
    Wait Until Page Contains Element    //*[@id="add3"]                                                                                                   300
    Click Element                       //*[@id="add3"]
    Wait Until Page Contains            bangkok                                                                                                           300
    Sleep                               1
    Wait Until Page Contains Element    //*[@id="btnClose"]                                                                                               300
    Click Element                       //*[@id="btnClose"]

test28 ทดสอบการตั้งค่าที่อยู่1 เป็นค่าเริ่มต้น
    กดปุ่มเพิ่ม
    กรอกชื่อ                            jackson
    กรอกอีเมล                           jackson@mail.com
    ${value} =                          Get Text                                                                                                          //*[@id="AddCustomerModal"]/div[2]/div/form/div[1]/div/div[1]
    Sleep                               1
    กดปุ่มบันทึก
    Sleep                               1
    ตรวจพบข้อความบน Alert               บันทึกข้อมูลสำเร็จ
    กดปุ่มแก้ไข
    ตรวจสอบที่อยู่หลัก                  ${value}

test29 ทดสอบการตั้งค่าที่อยู่2 เป็นค่าเริ่มต้น
    กดปุ่มเพิ่ม
    Wait Until Page Contains Element    //*[@id="AddCustomerModal"]/div[2]/div/form/div[1]/div                                                            300
    Click Element                       //*[@id="AddCustomerModal"]/div[2]/div/form/div[1]/div
    Wait Until Element Is Visible       //*[@id="AddCustomerModal"]/div[2]/div/form/div[1]/div/div[2]/div[2]                                              300
    Click Element                       //*[@id="AddCustomerModal"]/div[2]/div/form/div[1]/div/div[2]/div[2]
    กรอกชื่อ                            jackson
    กรอกอีเมล                           jackson@mail.com
    ${value} =                          Get Text                                                                                                          //*[@id="AddCustomerModal"]/div[2]/div/form/div[1]/div/div[1]
    Sleep                               1
    กดปุ่มบันทึก
    Sleep                               1
    ตรวจพบข้อความบน Alert               บันทึกข้อมูลสำเร็จ
    กดปุ่มแก้ไข
    ตรวจสอบที่อยู่หลัก                  ${value}

test30 ทดสอบการตั้งค่าที่อยู่3 เป็นค่าเริ่มต้น
    กดปุ่มเพิ่ม
    Wait Until Page Contains Element    //*[@id="AddCustomerModal"]/div[2]/div/form/div[1]/div                                                            300
    Click Element                       //*[@id="AddCustomerModal"]/div[2]/div/form/div[1]/div
    Wait Until Element Is Visible       //*[@id="AddCustomerModal"]/div[2]/div/form/div[1]/div/div[2]/div[3]                                              300
    Click Element                       //*[@id="AddCustomerModal"]/div[2]/div/form/div[1]/div/div[2]/div[3]
    กรอกชื่อ                            jackson
    กรอกอีเมล                           jackson@mail.com
    ${value} =                          Get Text                                                                                                          //*[@id="AddCustomerModal"]/div[2]/div/form/div[1]/div/div[1]
    Sleep                               1
    กดปุ่มบันทึก
    Sleep                               1
    ตรวจพบข้อความบน Alert               บันทึกข้อมูลสำเร็จ
    กดปุ่มแก้ไข
    ตรวจสอบที่อยู่หลัก                  ${value}

*** Keywords ***
กดปุ่มตั้งค่า
    Sleep                               1
    Wait Until Page Contains Element    //*[@id="root-content"]/div/form/div/div[2]/button[2]                                                             300
    Click Element                       //*[@id="root-content"]/div/form/div/div[2]/button[2]
    Wait Until Element Is Visible       id:settingModal                                                                                                   300

กดปุ่มเพิ่ม
    Wait Until Page Contains Element    //*[@id="root-content"]/div/form/div/div[2]/button[1]                                                             300
    Click Element                       //*[@id="root-content"]/div/form/div/div[2]/button[1]
    Wait Until Element Is Visible       id:AddCustomerModal                                                                                               300

กรอกอีเมล
    [Arguments]                         ${email}
    Sleep                               1
    Input Text                          //*[@id="AddCustomerModal"]/div[2]/div/form/div[4]/div/div[4]/div/input                                           ${email}

กรอกที่อยู่
    [Arguments]                         ${address}
    Sleep                               1
    Input Text                          //*[@id="AddCustomerModal"]/div[2]/div/form/div[4]/div/div[6]/div/div/input                                       ${address}

เข้าหน้าCustomer Profile
    [Arguments]                         ${value}
    Wait Until Page Contains Element    //*[@id="root-segment"]/div/div/div/div[3]/div[${value}]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]    300
    Double Click Element                //*[@id="root-segment"]/div/div/div/div[3]/div[${value}]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]
    Wait Until Element Is Visible       id:cusInfoModal                                                                                                   300

ตรวจสอบที่อยู่หลัก
    [Arguments]                         ${text}
    Wait Until Page Contains Element    //*[@id="AddCustomerModal"]/div[2]/div/form/div[1]/div/div[1]                                                     300
    ${value} =                          Get Text                                                                                                          //*[@id="AddCustomerModal"]/div[2]/div/form/div[1]/div/div[1]
    Should Be Equal                     ${value}                                                                                                          ${text}
    Wait Until Page Contains Element    id:CloseAddCustomer                                                                                               300
    Click Element                       id:CloseAddCustomer

กดปุ่มแก้ไข
    Wait Until Page Contains Element    //*[@id="root-segment"]/div/div/div[1]/div[3]/div[4]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[2]        300
    Click Element                       //*[@id="root-segment"]/div/div/div[1]/div[3]/div[4]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[2]





