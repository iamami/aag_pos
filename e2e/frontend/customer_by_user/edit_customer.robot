*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบและinit_data     searchCustomer
Test Setup        เข้าหน้าลูกค้า
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    Close Browser
*** Variables ***

*** Test cases ***
Test43 ทดสอบการกดเลือก ชื่อเล่น สำเร็จ เมื่อกดบันทึก
    เข้าหน้าลูกค้า
    Sleep                                1
    กดปุ่มตั้งค่า
    Sleep                                1
    กดเลือกชื่อเล่น
    #สามารถกดเลือกcheckboxได้
    #pop up ถูกปิด
    กดปุ่มแก้ไข
    Sleep                                1
    พบPopupแก้ไข
    พบข้อความ                            ชื่อเล่น
    #พบช่องชื่อเล่น ในหน้าต่างแก้ไข
    ปิดpopupแก้ไข

Test44 ทดสอบการกดยกเลิก ชื่อเล่น สำเร็จ เมื่อกดบันทึก
    เข้าหน้าลูกค้า
    Sleep                                1
    กดปุ่มตั้งค่า
    Sleep                                1
    กดเลือกชื่อเล่น
    #สามารถกดยกเลิกcheckboxได้
    #pop up ถูกปิด
    กดปุ่มแก้ไข
    Sleep                                1
    พบPopupแก้ไข
    ไม่พบข้อความ                         ชื่อเล่น
    #ไม่พบช่องชื่อเล่น ในหน้าต่างแก้ไข
    ปิดpopupแก้ไข

Test45 ทดสอบการไม่สามารถเปลี่ยนรหัสลูกค้าได้
    เข้าหน้าลูกค้า
    กดปุ่มแก้ไข
    Sleep                                1
    พบPopupแก้ไข
    Sleep                                1
    ตรวจสอบไม่สามารถเปลี่ยนรหัสลูกค้า
    #ไม่สามารถแก้ไขได้
    ปิดpopupแก้ไข

Test46 ทดสอบการแก้ไขข้อมูลไม่สำเร็จของ user C000002 เมื่อกดปุ่มยกเลิก
    เข้าหน้าลูกค้า
    กดปุ่มแก้ไข
    Sleep                                1
    พบPopupแก้ไข
    Sleep                                1
    แก้ไขข้อมูลลูกค้า                    Johnny                                                                                                     0802222224                                                                 Johnny@mail.com    1111111111188
    กดปุ่มยกเลิก
    #pop up ถูกปิด
    Sleep                                1
    พบข้อความ                            ลูกค้า
    #เจอหน้าลูกค้า
    Sleep                                1
    ไม่พบข้อความ                         Johnny
    ไม่พบข้อความ                         0802222224
    ไม่พบข้อความ                         Johnny@mail.com
    ไม่พบข้อความ                         1111111111188
    Sleep                                1
    พบข้อความ                            John
    พบข้อความ                            0802222222
    พบข้อความ                            John@mail.com
    พบข้อความ                            1111111111111
    #ไม่พบข้อมูลที่ถูกแก้ไข

Test47 ทดสอบการแก้ไขข้อมูลไม่สำเร็จของ user C000002 เมื่อกดปุ่มกากบาท
    เข้าหน้าลูกค้า
    กดปุ่มแก้ไข
    Sleep                                1
    พบPopupแก้ไข
    Sleep                                1
    แก้ไขข้อมูลลูกค้า                    Johnny                                                                                                     0802222224                                                                 Johnny@mail.com    1111111111188
    กดปุ่มยกเลิก
    #pop up ถูกปิด
    Sleep                                1
    พบข้อความ                            ลูกค้า
    #เจอหน้าลูกค้า
    Sleep                                1
    ไม่พบข้อความ                         Johnny
    ไม่พบข้อความ                         0802222224
    ไม่พบข้อความ                         Johnny@mail.com
    ไม่พบข้อความ                         1111111111188
    Sleep                                1
    พบข้อความ                            John
    พบข้อความ                            0802222222
    พบข้อความ                            John@mail.com
    พบข้อความ                            1111111111111
    #ไม่พบข้อมูลที่ถูกแก้ไข

Test48 ทดสอบการเปลี่ยนที่อยู่หลักของ user C000002 เป็นที่อยู่2 เมื่อกดบันทึก
    เข้าหน้าลูกค้า
    กดปุ่มแก้ไข
    Sleep                                1
    พบPopupแก้ไข
    Sleep                                1
    เลือกที่อยู่                         2
    Sleep                                1
    กดปุ่มบันทึก
    Sleep                                1
    ตรวจพบข้อความบน Alert                บันทึกข้อมูลสำเร็จ
    #เจอalert บันทึกข้อมูลสำเร็จ
    Sleep                                1
    พบPopupแก้ไข
    #พบ pop up แก้ไข
    Sleep                                1
    พบข้อความ                            ที่อยู่ 2
    Sleep                                1
    ตรวจสอบที่อยู่หลัก                   ที่อยู่ 2
    #พบตัวเลือกที่อยู่หลักเป็นที่อยู่2
    ปิดpopupแก้ไข

Test49 ทดสอบการแก้ไขข้อมูลสำเร็จของ user C000002
    เข้าหน้าลูกค้า
    กดปุ่มแก้ไข
    Sleep                                1
    พบPopupแก้ไข
    Sleep                                1
    แก้ไขข้อมูลลูกค้า                    Johnny                                                                                                     0802222224                                                                 Johnny@mail.com    1111111111188
    กดปุ่มบันทึก
    #แก้ไขข้อมูลสำเร็จ
    Sleep                                1
    ตรวจพบข้อความบน Alert                บันทึกข้อมูลสำเร็จ
    #แสดงalert บันทึกข้อมูลสำเร็จ
    Sleep                                1
    พบPopupแก้ไข
    Sleep                                1
    พบข้อความ                            Johnny
    พบข้อความ                            0802222224
    พบข้อความ                            Johnny@mail.com
    พบข้อความ                            1111111111188
    #พบข้อมูลที่ถูกแก้ไข
    ปิดpopupแก้ไข

*** Keywords ***

กดปุ่มตั้งค่า
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="root-content"]/div/form/div/div[2]/button[2]                                                      300
    Sleep                                1
    Click Element                        //*[@id="root-content"]/div/form/div/div[2]/button[2]
    Sleep                                5
    Wait Until Element Is Visible        id:settingModal                                                                                            300

กดเลือกชื่อเล่น
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="nick_name"]/div/label                                                                             300
    Click Element                        //*[@id="nick_name"]/div/label
    Sleep                                1
    Wait Until Page Contains Element     id:settingconfirm                                                                                          300
    Double Click Element                 id:settingconfirm

กดปุ่มแก้ไข
    Wait Until Page Contains Element     //*[@id="root-segment"]/div/div/div/div[3]/div[2]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[2]    300
    Double Click Element                 //*[@id="root-segment"]/div/div/div/div[3]/div[2]/div/div/div[2]/div/div[1]/div/div/div/div/center/a[2]

พบPopupแก้ไข
    Wait Until Element Is Visible        id:AddCustomerModal                                                                                        300

เลือกที่อยู่
    [Arguments]                          ${value}
    Wait Until Element Is Visible        //*[@id="AddCustomerModal"]/div[2]/div/form/div[1]/div                                                     300
    Double Click Element                 //*[@id="AddCustomerModal"]/div[2]/div/form/div[1]/div
    Wait Until Element Is Visible        //*[@id="AddCustomerModal"]/div[2]/div/form/div[1]/div/div[2]/div[${value}]                                300
    Double Click Element                 //*[@id="AddCustomerModal"]/div[2]/div/form/div[1]/div/div[2]/div[${value}]

ปิดpopupแก้ไข
    Wait Until Element Is Visible        id:CloseAddCustomer                                                                                        300
    Double Click Element                 id:CloseAddCustomer
    Sleep                                1
    Wait Until Element Is Not Visible    id:AddCustomerModal                                                                                        300

ตรวจสอบที่อยู่หลัก
    [Arguments]                          ${text}
    Wait Until Page Contains Element     //*[@id="AddCustomerModal"]/div[2]/div/form/div[1]/div/div[1]                                              300
    ${value} =                           Get Text                                                                                                   //*[@id="AddCustomerModal"]/div[2]/div/form/div[1]/div/div[1]
    Should Be Equal                      ${value}                                                                                                   ${text}

ตรวจสอบไม่สามารถเปลี่ยนรหัสลูกค้า
    Wait Until Page Contains Element     //*[@id="AddCustomerModal"]/div[2]/div/form/div[4]/div/div[2]/div/input                                    300
    ${value} =                           Get Element Attribute                                                                                      //*[@id="AddCustomerModal"]/div[2]/div/form/div[4]/div/div[2]/div/input    readonly
    Should Be Equal As Strings           ${value}                                                                                                   true

แก้ไขข้อมูลลูกค้า
    [Arguments]                          ${name}                                                                                                    ${phone}                                                                   ${email}           ${id}
    Wait Until Element Is Visible        //*[@id="AddCustomerModal"]/div[2]/div/form/div[4]/div/div[3]/div/input                                    300
    Wait Until Element Is Visible        //*[@id="AddCustomerModal"]/div[2]/div/form/div[4]/div/div[4]/div/input                                    300
    Wait Until Element Is Visible        //*[@id="AddCustomerModal"]/div[2]/div/form/div[4]/div/div[5]/div/input                                    300
    Wait Until Element Is Visible        //*[@id="AddCustomerModal"]/div[2]/div/form/div[4]/div/div[6]/div/input                                    300
    Input Text                           //*[@id="AddCustomerModal"]/div[2]/div/form/div[4]/div/div[3]/div/input                                    ${name}
    Input Text                           //*[@id="AddCustomerModal"]/div[2]/div/form/div[4]/div/div[4]/div/input                                    ${phone}
    Input Text                           //*[@id="AddCustomerModal"]/div[2]/div/form/div[4]/div/div[5]/div/input                                    ${email}
    Input Text                           //*[@id="AddCustomerModal"]/div[2]/div/form/div[4]/div/div[6]/div/input                                    ${id}

ตรวจพบข้อความบน Alert
    [Arguments]                          ${text}
    ${message}                           Handle Alert
    Should Be Equal                      ${message}                                                                                                 ${text}