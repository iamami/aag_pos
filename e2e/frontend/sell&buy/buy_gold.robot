*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบและinit_data Infocustomer
Test Setup        เข้าหน้าPos ซื้อ-ขายทอง
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data Infocustomer
*** Variables ***

*** Test cases ***
Test20 ทดสอบการแสดงค่าเป็นเงิน และ น.น.ซื้อ (บ.) เมื่อเลือก %ทองเป็น 90 และกรอก น.น.ซื้อ (ก.) เป็น 7
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                                     1
    เข้าหน้าซื้อทอง
    Sleep                                     1
    พบPopup POS
    Sleep                                     3
    กดเพิ่มรายการสินค้า
    Sleep                                     1
    เลือกเปอร์เซ็นต์ทอง90เปอร์เซ็นต์
    Sleep                                     1
    กรอกนน.(ก)                                7
    Sleep                                     1
    # ค่าน.น.ซื้อ (บ.) = 0.461
    ตรวจสอบนน.(บ)                             0.461
    Sleep                                     1
    # ค่าเป็นเงิน = 8455
    ตรวจสอบเป็นเงิน                           8455
    Sleep                                     1
    ปิดpopupเลือกรายการสินค้า
    Sleep                                     1
    ปิดpopupPOS

Test21 ทดสอบการแสดงค่าเป็นเงิน และ น.น.ซื้อ (ก.) เมื่อเลือก %ทองเป็น 90 และกรอก น.น.ซื้อ (บ.) เป็น 0.75
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                                     1
    เข้าหน้าซื้อทอง
    Sleep                                     1
    พบPopup POS
    Sleep                                     3
    กดเพิ่มรายการสินค้า
    Sleep                                     1
    เลือกเปอร์เซ็นต์ทอง90เปอร์เซ็นต์
    Sleep                                     1
    กรอกนน.(บ)                                0.75
    Sleep                                     1
    # ค่าน.น.ซื้อ (ก.) = 11.400
    ตรวจสอบนน.(ก)                             11.400
    Sleep                                     1
    # ค่าเป็นเงิน = 13770
    ตรวจสอบเป็นเงิน                           13770
    Sleep                                     1
    ปิดpopupเลือกรายการสินค้า
    ปิดpopupPOS

Test22 ทดสอบการแสดงค่าเป็นเงิน และ น.น.ซื้อ (บ.) เมื่อเลือก %ทองเป็น 96.5 และกรอก น.น.ซื้อ (ก.) เป็น 8
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                                     1
    เข้าหน้าซื้อทอง
    Sleep                                     1
    พบPopup POS
    Sleep                                     3
    กดเพิ่มรายการสินค้า
    Sleep                                     1
    เลือกเปอร์เซ็นต์ทอง96.5เปอร์เซ็นต์
    Sleep                                     1
    กรอกนน.(ก)                                8
    Sleep                                     1
    # ค่าน.น.ซื้อ (บ.) = 0.526
    ตรวจสอบนน.(บ)                             0.526
    Sleep                                     1
    # ค่าเป็นเงิน = 10537
    ตรวจสอบเป็นเงิน                           10537
    Sleep                                     1
    ปิดpopupเลือกรายการสินค้า
    ปิดpopupPOS

Test23 ทดสอบการแสดงค่าเป็นเงิน และ น.น.ซื้อ (ก.) เมื่อเลือก %ทองเป็น 96.5 และกรอก น.น.ซื้อ (บ.) เป็น 0.8
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                                     1
    เข้าหน้าซื้อทอง
    Sleep                                     1
    พบPopup POS
    Sleep                                     3
    กดเพิ่มรายการสินค้า
    Sleep                                     1
    เลือกเปอร์เซ็นต์ทอง96.5เปอร์เซ็นต์
    Sleep                                     1
    กรอกนน.(บ)                                0.8
    Sleep                                     1
    # ค่าน.น.ซื้อ (ก.) = 12.160
    ตรวจสอบนน.(ก)                             12.160
    Sleep                                     1
    # ค่าเป็นเงิน = 16160
    ตรวจสอบเป็นเงิน                           16160
    Sleep                                     1
    ปิดpopupเลือกรายการสินค้า
    ปิดpopupPOS

Test24 ทดสอบการแสดงค่าเป็นเงิน และ น.น.ซื้อ (บ.) เมื่อเลือก %ทองเป็น ทองแท่ง96.5 และกรอก น.น.ซื้อ (ก.) เป็น 9
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                                     1
    เข้าหน้าซื้อทอง
    Sleep                                     1
    พบPopup POS
    Sleep                                     3
    กดเพิ่มรายการสินค้า
    Sleep                                     1
    เลือกเปอร์เซ็นต์ทองแท่ง96.5เปอร์เซ็นต์
    Sleep                                     1
    กรอกนน.(ก)                                9
    Sleep                                     1
    # ค่าน.น.ซื้อ (บ.) = 0.590
    ตรวจสอบนน.(บ)                             0.590
    Sleep                                     1
    # ค่าเป็นเงิน = 11844
    ตรวจสอบเป็นเงิน                           11844
    Sleep                                     1
    ปิดpopupเลือกรายการสินค้า
    ปิดpopupPOS

Test25 ทดสอบการแสดงค่าเป็นเงิน และ น.น.ซื้อ (ก.) เมื่อเลือก %ทองเป็น ทองแท่ง96.5 และกรอก น.น.ซื้อ (บ.) เป็น 0.9
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                                     1
    เข้าหน้าซื้อทอง
    Sleep                                     1
    พบPopup POS
    Sleep                                     3
    กดเพิ่มรายการสินค้า
    Sleep                                     1
    เลือกเปอร์เซ็นต์ทองแท่ง96.5เปอร์เซ็นต์
    Sleep                                     1
    กรอกนน.(บ)                                0.9
    Sleep                                     1
    # ค่าน.น.ซื้อ (ก.) = 13.720
    ตรวจสอบนน.(ก)                             13.720
    Sleep                                     1
    # ค่าเป็นเงิน = 18180
    ตรวจสอบเป็นเงิน                           18180
    Sleep                                     1
    ปิดpopupเลือกรายการสินค้า
    ปิดpopupPOS

Test26 ทดสอบการบันทึกรายการสินค้าไม่สำเร็จเมื่อไม่กรอกข้อมูลแล้วกดปุ่มลงรายการ
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                                     1
    เข้าหน้าซื้อทอง
    Sleep                                     1
    พบPopup POS
    Sleep                                     3
    กดเพิ่มรายการสินค้า
    Sleep                                     1
    กดปุ่มลงรายการ
    Sleep                                     1
    #พบข้อความแจ้งเตือน
    พบข้อความ                                 *%ทอง เลือก%ทอง
    พบข้อความ                                 ชื่อสินค้า ต้องไม่เป็นค่าว่าง
    พบข้อความ                                 น.น.ซื้อ (ก.) ต้องไม่เป็นค่าว่าง
    พบข้อความ                                 น.น.ซื้อ (บ.) ต้องไม่เป็นค่าว่าง
    ปิดpopupเลือกรายการสินค้า
    ปิดpopupPOS

Test27 ทดสอบการบันทึกรายการสินค้าไม่สำเร็จเมื่อไม่กรอก%ทองแล้วกดปุ่มลงรายการ
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                                     1
    เข้าหน้าซื้อทอง
    Sleep                                     1
    พบPopup POS
    Sleep                                     3
    กดเพิ่มรายการสินค้า
    Sleep                                     1
    กรอกชื่อสินค้า                            สร้อยคอ
    Sleep                                     1
    กดปุ่มลงรายการ
    Sleep                                     1
    # พบข้อความแจ้งเตือน
    พบข้อความ                                 *%ทอง เลือก%ทอง
    พบข้อความ                                 น.น.ซื้อ (ก.) ต้องไม่เป็นค่าว่าง
    พบข้อความ                                 น.น.ซื้อ (บ.) ต้องไม่เป็นค่าว่าง
    ปิดpopupเลือกรายการสินค้า
    ปิดpopupPOS

Test28 ทดสอบการบันทึกรายการสินค้าไม่สำเร็จเมื่อไม่กรอกชื่อสินค้าแล้วกดปุ่มลงรายการ
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                                     1
    เข้าหน้าซื้อทอง
    Sleep                                     1
    พบPopup POS
    Sleep                                     3
    กดเพิ่มรายการสินค้า
    Sleep                                     1
    เลือกเปอร์เซ็นต์ทอง90เปอร์เซ็นต์
    Sleep                                     1
    กรอกนน.(ก)                                9
    Sleep                                     1
    กดปุ่มลงรายการ
    Sleep                                     1
    # พบข้อความแจ้งเตือน
    พบข้อความ                                 ชื่อสินค้า ต้องไม่เป็นค่าว่าง
    Sleep                                     1
    ปิดpopupเลือกรายการสินค้า
    ปิดpopupPOS

Test29 ทดสอบการบันทึกรายการสินค้าไม่สำเร็จเมื่อไม่กรอกชื่อสินค้าแล้วกดปุ่มลงรายการ
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                                     1
    เข้าหน้าซื้อทอง
    Sleep                                     1
    พบPopup POS
    Sleep                                     3
    กดเพิ่มรายการสินค้า
    Sleep                                     1
    เลือกเปอร์เซ็นต์ทอง90เปอร์เซ็นต์
    Sleep                                     1
    กรอกชื่อสินค้า                            สร้อยคอ
    Sleep                                     1
    กดปุ่มลงรายการ
    Sleep                                     1
    # .พบข้อความแจ้งเตือน
    พบข้อความ                                 น.น.ซื้อ (ก.) ต้องไม่เป็นค่าว่าง
    พบข้อความ                                 น.น.ซื้อ (บ.) ต้องไม่เป็นค่าว่าง
    Sleep                                     1
    ปิดpopupเลือกรายการสินค้า
    ปิดpopupPOS

Test30 ทดสอบการบันทึกรายการสินค้าสำเร็จเมื่อกดปุ่มลงรายการ
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                                     1
    เข้าหน้าซื้อทอง
    Sleep                                     1
    พบPopup POS
    Sleep                                     3
    กดเพิ่มรายการสินค้า
    Sleep                                     1
    เลือกเปอร์เซ็นต์ทอง90เปอร์เซ็นต์
    Sleep                                     1
    กรอกชื่อสินค้า                            สร้อยคอ
    Sleep                                     1
    กรอกนน.(ก)                                9
    Sleep                                     1
    กดปุ่มลงรายการ
    Sleep                                     1
    # เจอรายการซื้อที่บันทึกสำเร็จ
    พบข้อความ                                 สร้อยคอ
    ปิดpopupPOS


*** Keywords ***
ปิดpopupเลือกรายการสินค้า
    Sleep                                     1
    Wait Until Element Is Visible             id:btnCloseProductSelect                                                  300
    Sleep                               5
    Click Element                             id:btnCloseProductSelect

เลือกเปอร์เซ็นต์ทอง96.5เปอร์เซ็นต์
    Sleep                                     1
    Wait Until Element Is Visible             //*[@id="percengold"]/select                                                  300
    Sleep                               5
    Click Element                             //*[@id="percengold"]/select
    Sleep                                     1
    Wait Until Element Is Visible             //*[@id="percengold"]/select/option[3]                                                  300
    Sleep                               5
    Click Element                             //*[@id="percengold"]/select/option[3]

เลือกเปอร์เซ็นต์ทองแท่ง96.5เปอร์เซ็นต์
    Sleep                                     1
    Wait Until Element Is Visible             //*[@id="percengold"]/select                                                  300
    Sleep                               5
    Click Element                             //*[@id="percengold"]/select
    Sleep                                     1
    Wait Until Element Is Visible             //*[@id="percengold"]/select/option[4]                                                  300
    Sleep                               5
    Click Element                             //*[@id="percengold"]/select/option[4]

กรอกนน.(บ)
    [Arguments]                               ${value}
    Sleep                                     1
    Wait Until Element Is Visible             //*[@id="weight-b"]/div/input                                                  300
    Sleep                               5
    Input Text                                //*[@id="weight-b"]/div/input             ${value}

ตรวจสอบนน.(บ)
    [Arguments]                               ${w-b}
    ${weight-b}                               Get Value                                 //*[@id="weight-b"]/div/input
    Should Be Equal                           ${weight-b}                               ${w-b}

ตรวจสอบนน.(ก)
    [Arguments]                               ${w-g}
    ${weight-g}                               Get Value                                 //*[@id="weight-g"]/div/input
    Should Be Equal                           ${weight-g}                               ${w-g}

ตรวจสอบเป็นเงิน
    [Arguments]                               ${m}
    ${money}                                  Get Value                                 //*[@id="money"]/div/input
    Should Be Equal                           ${money}                                  ${m}


