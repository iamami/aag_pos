*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Library           String
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบและinit_data Infocustomer            
Test Setup        เข้าหน้าPos ซื้อ-ขายทอง
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data Infocustomer
*** Variables ***

*** Test cases ***
Test31 ทดสอบการแสดงชื่อสินค้าในส่วนซื้อทอง เมื่อเลือกสินค้าในส่วนขายทอง
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                            1
    เข้าหน้าเปลี่ยนทอง
    Sleep                            1
    พบPopup POS
    Sleep                            3
    กดเพิ่มรายการสินค้า
    Sleep                            1
    Wait Until Element Is Visible    //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select                                                  300
    Sleep                            1
    Click Element                    //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select
    Sleep                            3
    Wait Until Element Is Visible    //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select/option[2]                                                  300
    Sleep                            1
    Click Element                    //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select/option[2]
    Sleep                            1
                # ชื่อสินค้าในส่วนซื้อทอง และ สินค้าในส่วนขายทองต้องตรงกัน
    ${product_temp}                  Get Text                                                                                           //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select/option[2]
    ${product}                       Get Substring                                                                                      ${product_temp}                                                                               10
    ${productname}                   Get Value                                                                                          //*[@id="ProductName"]/div/input
    Should Be Equal                  ${product}                                                                                         ${productname}
    Sleep                            1
    ปิดpopupเลือกรายการสินค้า
    ปิดpopupPOS

Test32 ทดสอบการคำนวณค่าเป็นเงิน เมื่อมีค่าเปลี่ยนทอง = 500
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                            1
    เข้าหน้าเปลี่ยนทอง
    Sleep                            1
    พบPopup POS
    Sleep                            3
    กดเพิ่มรายการสินค้า
    Sleep                            1
    Wait Until Element Is Visible    //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select                                                  300
    Sleep                            1
    Click Element                    //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select
    Sleep                            3
    Wait Until Element Is Visible    //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select/option[2]                                                  300
    Sleep                            1
    Click Element                    //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select/option[2]
    Sleep                            1
    ${money}                         Convert To Integer                                                                                 500
    Input Text                       //*[@id="money"]/div/input                                                                         ${money}
    Sleep                            1
                # ราคาซื้อทองถูกกว่าราคาขาย500
    ${selltemp}                      Get Value                                                                                          //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[2]/div[3]/div/input
    ${sell}                          Convert To Integer                                                                                 ${selltemp}
    ${changetemp}                    Get Value                                                                                          //*[@id="contentModalSelectProduct"]/div[2]/div/div[2]/form/div[2]/div[4]/div/input
    ${change}                        Convert To Integer                                                                                 ${changetemp}
    ${result}                        Evaluate                                                                                           ${sell}-${change}
    Should Be Equal                  ${money}                                                                                           ${result}
    Sleep                            1
    ปิดpopupเลือกรายการสินค้า
    ปิดpopupPOS

Test33 ทดสอบการคำนวณค่าเป็นเงิน เมื่อมีค่าเปลี่ยนทอง = 500
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                            1
    เข้าหน้าเปลี่ยนทอง
    Sleep                            1
    พบPopup POS
    Sleep                            3
    กดเพิ่มรายการสินค้า
    Sleep                            1
    กดเพิ่มรายการเปลี่ยนทอง
    Sleep                            1
    ${selltemp}                      Get Value                                                                                          //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[2]/div[3]/div/input
    ${sell}                          Convert To Integer                                                                                 ${selltemp}
    ${1}                             Convert To Integer                                                                                 1
    ${moneybuygold}                  Evaluate                                                                                           ${sell}+${1}
    Input Text                       //*[@id="money"]/div/input                                                                         ${moneybuygold}
    Sleep                            1
    กดปุ่มลงรายการ
    Sleep                            1
                # พบข้อความแจ้งเตือนกรุณากรอกเงินเปลี่ยนให้ถูกต้อง
    พบข้อความ                        กรุณากรอกเงินเปลี่ยนให้ถูกต้อง
    Sleep                            1
    ปิดpopupเลือกรายการสินค้า
    ปิดpopupPOS

Test34 ทดสอบการบันทึกสำเร็จ เมื่อกรอกข้อมูลครบ
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                            1
    เข้าหน้าเปลี่ยนทอง
    Sleep                            1
    พบPopup POS
    Sleep                            3
    กดเพิ่มรายการสินค้า
    Sleep                            1
    กดเพิ่มรายการเปลี่ยนทอง
    Sleep                            1
    ${product_temp}                  Get Text                                                                                           //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select/option[2]
    ${product}                       Get Substring                                                                                      ${product_temp}                                                                               9     19
    Sleep                            1
    กดปุ่มลงรายการ
    Sleep                            1
                # พบจำนวนรายการ = 2
    ${counttemp}                     Get Element Count                                                                                  id:idproduct
    ${count}                         Convert To String                                                                                  ${counttemp}
    Should Be Equal                  ${count}                                                                                           2
    Sleep                            1
    ปิดpopupPOS

Test35 ทดสอบการลบรายการซื้อทอง1รายการสำเร็จเมื่อมีการลบรายการขายรายการนั้น
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                            1
    เข้าหน้าเปลี่ยนทอง
    Sleep                            1
    พบPopup POS
    Sleep                            3
    กดเพิ่มรายการสินค้า
    Sleep                            1
    กดเพิ่มรายการเปลี่ยนทอง
    Sleep                            1
    ${product_temp}                  Get Text                                                                                           //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select/option[2]
    ${product}                       Get Substring                                                                                      ${product_temp}                                                                               9     19
    Sleep                            1
    กดปุ่มลงรายการ
    Sleep                            1
    Wait Until Element Is Visible    //*[@id="root-table-bill"]/div/div[1]/div[3]/div[1]/div/div/div[1]/div/div[1]/div/div/div/div/a                                                  300
    Sleep                            1
    Click Element                    //*[@id="root-table-bill"]/div/div[1]/div[3]/div[1]/div/div/div[1]/div/div[1]/div/div/div/div/a
    Sleep                            1
    Handle Alert
    Sleep                            1
                #ไม่พบรายการซื้อทองและขายทองที่ลบ
    ไม่พบข้อความ                     ${product}
    ปิดpopupPOS

Test36 ทดสอบการลบรายการขายทอง1รายการสำเร็จเมื่อมีการลบรายการซื้อรายการนั้น
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                            1
    เข้าหน้าเปลี่ยนทอง
    Sleep                            1
    พบPopup POS
    Sleep                            3
    กดเพิ่มรายการสินค้า
    Sleep                            1
    กดเพิ่มรายการเปลี่ยนทอง
    Sleep                            1
    ${product_temp}                  Get Text                                                                                           //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select/option[2]
    ${product}                       Get Substring                                                                                      ${product_temp}                                                                               9     19
    Sleep                            1
    กดปุ่มลงรายการ
    Sleep                            1
    Wait Until Element Is Visible    //*[@id="root-table-bill"]/div/div[1]/div[3]/div[2]/div/div/div[1]/div/div[1]/div/div/div/div/a                                                  300
    Sleep                            1
    Click Element                    //*[@id="root-table-bill"]/div/div[1]/div[3]/div[2]/div/div/div[1]/div/div[1]/div/div/div/div/a
    Sleep                            1
    Handle Alert
    Sleep                            1
                #ไม่พบรายการซื้อทองและขายทองที่ลบ
    ไม่พบข้อความ                     ${product}
    ปิดpopupPOS

Test37 ทดสอบการแสดงยอดรวมถูกต้อง
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                            1
    เข้าหน้าเปลี่ยนทอง
    Sleep                            1
    พบPopup POS
    Sleep                            3
    กดเพิ่มรายการสินค้า
    Sleep                            1
    กดเพิ่มรายการเปลี่ยนทอง
    Sleep                            1
    ${product_temp}                  Get Text                                                                                           //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select/option[2]
    ${product}                       Get Substring                                                                                      ${product_temp}                                                                               9     19
    กดปุ่มลงรายการ
    Sleep                            1
    # พบจำนวนรายการ = 2
    ${counttemp}                     Get Element Count                                                                                  id:idproduct
    ${count}                         Convert To String                                                                                  ${counttemp}
    Should Be Equal                  ${count}                                                                                           2
    ปิดpopupPOS

*** Keywords ***

ปิดpopupเลือกรายการสินค้า
    Sleep                            1
    Wait Until Element Is Visible    id:btnCloseProductSelect                                                  300
    Sleep                               5
    Click Element                    id:btnCloseProductSelect

เลือกเปอร์เซ็นต์ทอง90เปอร์เซ็นต์
    Sleep                            1
    Wait Until Element Is Visible    //*[@id="percengold"]/select                                                  300
    Sleep                               5
    Click Element                    //*[@id="percengold"]/select
    Sleep                            1
    Wait Until Element Is Visible    //*[@id="percengold"]/select/option[2]                                                  300
    Sleep                               5
    Click Element                    //*[@id="percengold"]/select/option[2]

