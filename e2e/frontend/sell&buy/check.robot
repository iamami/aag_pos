*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Library           String
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบและinit_data Infocustomer
Test Setup        เข้าหน้าPos ซื้อ-ขายทอง
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data InfoCustomer
*** Variables ***

*** Test cases ***
Test38 ทดสอบการแสดงเงินทอน เมื่อจำนวนรับเงินสด มากกว่า ยอดจ่าย
    Sleep                               1
    เข้าหน้าขายทอง
    Sleep                               1
    พบPopup POS
    Sleep                               5
    กดเพิ่มรายการสินค้า
    Sleep                               1
    เลือกรายการ
    Sleep                               1
    กดปุ่มลงรายการ
    Sleep                               1
    กดปุ่มบันทึกบิล
    Sleep                               1
    พบPopup ชำระเงิน
    Sleep                               1
    Wait Until Element Is Visible       id:cash                                                  300
    ${val}                              Convert To Integer                                                                             500
    ${result_temp}                      Get Value                                                                                      id:cash
    ${result}                           Convert To Integer                                                                             ${result_temp}
    ${cash}                             Evaluate                                                                                       ${result}+${val}
    Sleep                               1
    Input Text                          id:inputCash                                                                                   ${cash}
    Sleep                               1
    ${change_temp}                      Get Value                                                                                      id:change
    ${change_t}                         Get Substring                                                                                  ${change_temp}          0    3
    ${change}                           Convert To Integer                                                                             ${change_t}
    Should Be Equal                     ${change}                                                                                      ${val}
    Sleep                               1
    ปิดpopup ชำระเงิน
    Sleep                               1
    ปิดpopupPOS

Test39 ทดสอบการแสดงเงินทอน เมื่อจำนวนรับเงินสด+ยอดจ่ายเครดิต มากกว่า ยอดจ่าย
    Sleep                               1
    เข้าหน้าขายทอง
    Sleep                               1
    พบPopup POS
    Sleep                               5
    กดเพิ่มรายการสินค้า
    Sleep                               1
    เลือกรายการ
    Sleep                               1
    Sleep                               1
    กดปุ่มลงรายการ
    Sleep                               1
    กดปุ่มบันทึกบิล
    Sleep                               1
    พบPopup ชำระเงิน
    Sleep                               1
    ${val}                              Convert To Integer                                                                             500
    ${cash_temp}                        Get Value                                                                                      id:cash
    ${cash}                             Convert To Integer                                                                             ${cash_temp}
    ${c}                                Evaluate                                                                                       ${cash}-${val}
    Input Text                          id:cash                                                                                        ${c}
    ${inputcash}                        Evaluate                                                                                       ${c}+${val}
    Input Text                          id:inputCash                                                                                   ${inputcash}
    Sleep                               1
    ${change_temp}                      Get Value                                                                                      id:change
    ${change_t}                         Get Substring                                                                                  ${change_temp}          0    3
    ${change}                           Convert To Integer                                                                             ${change_t}
    Should Be Equal                     ${change}                                                                                      ${val}
    ปิดpopup ชำระเงิน
    ปิดpopupPOS

Test40 ทดสอบการคำนวณยอดจ่ายรวมของบัตรเครดิต เมื่อมีค่าบริการ
    Sleep                               1
    เข้าหน้าขายทอง
    Sleep                               1
    พบPopup POS
    Sleep                               5
    กดเพิ่มรายการสินค้า
    Sleep                               1
    เลือกรายการ
    Sleep                               1
    กดปุ่มลงรายการ
    Sleep                               1
    กดปุ่มบันทึกบิล
    Sleep                               1
    พบPopup ชำระเงิน
    Sleep                               1
    Sleep                               1
    ${val}                              Convert To Integer                                                                             250
    ${cash_temp}                        Get Value                                                                                      id:cash
    ${cash}                             Convert To Integer                                                                             ${cash_temp}
    ${c}                                Evaluate                                                                                       ${cash}-${val}
    Input Text                          id:cash                                                                                        ${c}
    Input Text                          id:service                                                                                     ${val}
    Sleep                               1
    ${total_temp}                       Get Value                                                                                      id:total
    ${total_t}                          Get Substring                                                                                  ${total_temp}           0    3
    ${total}                            Convert To Integer                                                                             ${total_t}
    ${credit_temp}                      Get Value                                                                                      id:credit
    ${credit_t}                         Get Substring                                                                                  ${credit_temp}          0    3
    ${credit}                           Convert To Integer                                                                             ${credit_t}
    ${service_temp}                     Get Value                                                                                      id:service
    ${service_t}                        Get Substring                                                                                  ${service_temp}         0    3
    ${service}                          Convert To Integer                                                                             ${service_t}
    ${ans}                              Evaluate                                                                                       ${credit}+${service}
    Should Be Equal                     ${total}                                                                                       ${ans}
    ปิดpopup ชำระเงิน
    ปิดpopupPOS

Test41 ทดสอบการเลือกชำระด้วยเงินสดไม่สำเร็จ เมื่อไม่กรอกจำนวนรับ เงินสด
    Sleep                               1
    เข้าหน้าขายทอง
    Sleep                               1
    พบPopup POS
    Sleep                               5
    กดเพิ่มรายการสินค้า
    Sleep                               1
    เลือกรายการ
    Sleep                               1
    กดปุ่มลงรายการ
    Sleep                               1
    กดปุ่มบันทึกบิล
    Sleep                               1
    พบPopup ชำระเงิน
    Wait Until Element Is Visible       id:btnSave                                                  300
    Sleep                               5
    Click Element                       id:btnSave
    Sleep                               5
    Handle Alert
    Sleep                               5
    พบข้อความ                           กรุณาระบุยอดรับเงินสด
    ปิดpopup ชำระเงิน
    ปิดpopupPOS

Test42 ทดสอบการเลือกชำระด้วยเงินสดไม่สำเร็จ เมื่อกรอกจำนวนรับ เงินสด น้อยกว่ายอดจริง
    Sleep                               1
    เข้าหน้าขายทอง
    Sleep                               1
    พบPopup POS
    Sleep                               5
    กดเพิ่มรายการสินค้า
    Sleep                               1
    เลือกรายการ
    Sleep                               1
    กดปุ่มลงรายการ
    Sleep                               1
    กดปุ่มบันทึกบิล
    Sleep                               1
    พบPopup ชำระเงิน
    Sleep                               1
    ${val}                              Convert To Integer                                                                             500
    ${result_temp}                      Get Value                                                                                      id:cash
    ${result}                           Convert To Integer                                                                             ${result_temp}
    ${cash}                             Evaluate                                                                                       ${result}-${val}
    Input Text                          id:inputCash                                                                                   ${cash}
    Sleep                               0.5
    Wait Until Element Is Visible       id:btnSave                                                  300
    Sleep                               5
    Click Element                       id:btnSave
    Sleep                               5
    Handle Alert
    Sleep                               5
    พบข้อความ                           กรุณาระบุยอดรับเงินสดให้ถูกต้อง
    ปิดpopup ชำระเงิน
    ปิดpopupPOS

Test43 ทดสอบการเลือกชำระด้วยบัตรเครดิตไม่สำเร็จ เมื่อไม่กรอกรหัสบัตร
    Sleep                               1
    เข้าหน้าขายทอง
    Sleep                               1
    พบPopup POS
    Sleep                               5
    กดเพิ่มรายการสินค้า
    Sleep                               1
    เลือกรายการ
    Sleep                               1
    กดปุ่มลงรายการ
    Sleep                               1
    กดปุ่มบันทึกบิล
    Sleep                               1
    พบPopup ชำระเงิน
    Sleep                               1
    ${val}                              Convert To Integer                                                                             500
    ${cash_temp}                        Get Value                                                                                      id:cash
    ${cash}                             Convert To Integer                                                                             ${cash_temp}
    ${c}                                Evaluate                                                                                       ${cash}-${val}
    Sleep                               5
    Input Text                          id:cash                                                                                        ${c}
    Sleep                               5
    Wait Until Element Is Visible       id:type                                                  300
    Sleep                               5
    Click Element                       id:type
    Sleep                               5
    Wait Until Element Is Visible       //*[@id="type"]/div[2]/div[1]/span                                                  300
    Sleep                               5
    Click Element                       //*[@id="type"]/div[2]/div[1]/span
    Sleep                               5
    Wait Until Element Is Visible       id:btnSave                                                  300
    Sleep                               5
    Click Element                       id:btnSave
    Sleep                               5
    Handle Alert
    Sleep                               5
    พบข้อความ                           กรุณาระบุรหัสบัตรให้ถูกต้อง
    ปิดpopup ชำระเงิน
    ปิดpopupPOS

Test44 ทดสอบการเลือกชำระด้วยบัตรเครดิตไม่สำเร็จ เมื่อไม่เลือกชนิดบัตร
    Sleep                               1
    เข้าหน้าขายทอง
    Sleep                               1
    พบPopup POS
    Sleep                               5
    กดเพิ่มรายการสินค้า
    Sleep                               1
    เลือกรายการ
    Sleep                               1
    กดปุ่มลงรายการ
    Sleep                               1
    กดปุ่มบันทึกบิล
    Sleep                               1
    พบPopup ชำระเงิน
    Sleep                               1
    ${val}                              Convert To Integer                                                                             500
    ${cash_temp}                        Get Value                                                                                      id:cash
    ${cash}                             Convert To Integer                                                                             ${cash_temp}
    ${c}                                Evaluate                                                                                       ${cash}-${val}
    Input Text                          id:cash                                                                                        ${c}
    Sleep                               5
    Input Text                          id:IDcredit                                                                                    1234567890
    Sleep                               5
    Wait Until Element Is Visible       id:btnSave                                                  300
    Sleep                               5
    Click Element                       id:btnSave
    Sleep                               5
    Handle Alert
    Sleep                               5
    พบข้อความ                           กรุณาเลือกชนิดบัตร
    ปิดpopup ชำระเงิน
    ปิดpopupPOS

Test45 ทดสอบการบันทึกชำระเงินสำเร็จด้วยการชำระเงินด้วยเงินสด
    Sleep                               1
    เข้าหน้าขายทอง
    Sleep                               1
    พบPopup POS
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="contentModalPOS"]/div[2]/div/div/div[1]/div/form/div[3]/div/div                                                  300
    Sleep                               1
    Click Element                       //*[@id="contentModalPOS"]/div[2]/div/div/div[1]/div/form/div[3]/div/div
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="contentModalPOS"]/div[2]/div/div/div[1]/div/form/div[3]/div/div/div[2]/div[1]                                                  300
    Sleep                               1
    Click Element                       //*[@id="contentModalPOS"]/div[2]/div/div/div[1]/div/form/div[3]/div/div/div[2]/div[1]
    Sleep                               5
    กดเพิ่มรายการสินค้า
    Sleep                               1
    เลือกรายการ
    Sleep                               1
    กดปุ่มลงรายการ
    Sleep                               1
    กดปุ่มบันทึกบิล
    Sleep                               1
    พบPopup ชำระเงิน
    ${cash_temp}                        Get Value                                                                                      id:cash
    ${cash}                             Convert To Integer                                                                             ${cash_temp}
    Input Text                          id:inputCash                                                                                   ${cash}
    Sleep                               5
    Wait Until Element Is Visible       id:btnSave                                                  300
    Sleep                               5
    Click Element                       id:btnSave
    Sleep                               5
    ตรวจพบข้อความบน Alert               ยืนยันการชำระเงิน
    Sleep                               5
    ตรวจพบข้อความบน Alert               บันทึกบิลและชำระเงินสำเร็จ
    Sleep                               5
    ปิดpopupPOS

Test46 ทดสอบการบันทึกชำระเงินสำเร็จด้วยการชำระเงินด้วยบัตรเครดิตทั้งหมด
    Sleep                               1
    เข้าหน้าขายทอง
    Sleep                               1
    พบPopup POS
    Sleep                               5
    กดเพิ่มรายการสินค้า
    Sleep                               1
    เลือกรายการ
    Sleep                               1
    กดปุ่มลงรายการ
    Sleep                               1
    กดปุ่มบันทึกบิล
    Sleep                               1
    พบPopup ชำระเงิน
    Input Text                          id:cash                                                                                        0
    Sleep                               1
    Wait Until Element Is Visible       id:type                                                  300
    Sleep                               5
    Click Element                       id:type
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="type"]/div[2]/div[1]/span                                                  300
    Sleep                               5
    Click Element                       //*[@id="type"]/div[2]/div[1]/span
    Sleep                               1
    Input Text                          id:IDcredit                                                                                    1234567890
    Sleep                               5
    Wait Until Element Is Visible       id:btnSave                                                  300
    Sleep                               5
    Click Element                       id:btnSave
    Sleep                               5
    ตรวจพบข้อความบน Alert               ยืนยันการชำระเงิน
    Sleep                               5
    ตรวจพบข้อความบน Alert               บันทึกบิลและชำระเงินสำเร็จ
    ปิดpopupPOS

Test47 ทดสอบการบันทึกชำระเงินสำเร็จด้วยการชำระเงินด้วยบัตรเครดิตทและเงินสด
    Sleep                               1
    เข้าหน้าขายทอง
    Sleep                               1
    พบPopup POS
    Sleep                               5
    กดเพิ่มรายการสินค้า
    Sleep                               1
    เลือกรายการ
    Sleep                               1
    กดปุ่มลงรายการ
    Sleep                               1
    กดปุ่มบันทึกบิล
    Sleep                               1
    พบPopup ชำระเงิน
    Sleep                               1
    ${val}                              Convert To Integer                                                                             500
    ${cash_temp}                        Get Value                                                                                      id:cash
    ${cash}                             Convert To Integer                                                                             ${cash_temp}
    ${c}                                Evaluate                                                                                       ${cash}-${val}
    Sleep                               1
    Input Text                          id:cash                                                                                        ${c}
    Sleep                               1
    Input Text                          id:IDcredit                                                                                    12345678
    Sleep                               5
    Wait Until Element Is Visible       id:type                                                  300
    Sleep                               5
    Click Element                       id:type
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="type"]/div[2]/div[1]/span                                                  300
    Sleep                               5
    Click Element                       //*[@id="type"]/div[2]/div[1]/span
    Sleep                               1
    Input Text                          id:inputCash                                                                                   ${c}
    Sleep                               1
    Wait Until Element Is Visible       id:btnSave                                                  300
    Sleep                               5
    Click Element                       id:btnSave
    Sleep                               5
    ตรวจพบข้อความบน Alert               ยืนยันการชำระเงิน
    Sleep                               5
    ตรวจพบข้อความบน Alert               บันทึกบิลและชำระเงินสำเร็จ
    ปิดpopupPOS

Test48 ทดสอบการบันทึกชำระเงินสำเร็จเมื่อไม่กรอกลูกค้า พนักงาน และหมายเหตุ
    Sleep                               1
    เข้าหน้าขายทอง
    Sleep                               1
    พบPopup POS
    Sleep                               5
    กดเพิ่มรายการสินค้า
    Sleep                               1
    เลือกรายการ
    Sleep                               1
    กดปุ่มลงรายการ
    Sleep                               1
    กดปุ่มบันทึกบิล
    Sleep                               1
    พบPopup ชำระเงิน
    Sleep                               1
    ${cash_temp}                        Get Value                                                                                      id:cash
    ${cash}                             Convert To Integer                                                                             ${cash_temp}
    Sleep                               1
    Input Text                          id:inputCash                                                                                   ${cash}
    Sleep                               5
    Wait Until Element Is Visible       id:btnSave                                                  300
    Sleep                               5
    Click Element                       id:btnSave
    Sleep                               5
    ตรวจพบข้อความบน Alert               ยืนยันการชำระเงิน
    Sleep                               5
    ตรวจพบข้อความบน Alert               บันทึกบิลและชำระเงินสำเร็จ
    Sleep                               5
    ปิดpopupPOS

Test49 ทดสอบการบันทึกบิลสำเร็จ เมื่อกดปุ่ม บันทึกบิลและชำระเงิน ใน pop up ซื้อทอง
    Sleep                               1
    เข้าหน้าซื้อทอง
    Sleep                               1
    พบPopup POS
    Sleep                               5
    กดเพิ่มรายการสินค้า
    Sleep                               1
    เลือกเปอร์เซ็นต์ทอง90เปอร์เซ็นต์
    Sleep                               1
    กรอกชื่อสินค้า                      สร้อยคอ
    Sleep                               1
    กรอกนน.(ก)                          1
    Sleep                               1
    กดปุ่มลงรายการ
    Sleep                               1
    กดปุ่มบันทึกบิล
    Sleep                               5
    ตรวจพบข้อความบน Alert               บันทึกบิลและชำระเงินสำเร็จ
    ปิดpopupPOS

*** Keywords ***
เลือกเปอร์เซ็นต์ทอง90เปอร์เซ็นต์
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="percengold"]/select                                                  300
    Sleep                               5
    Click Element                       //*[@id="percengold"]/select
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="percengold"]/select/option[2]                                                  300
    Sleep                               5
    Click Element                       //*[@id="percengold"]/select/option[2]

กรอกนน.(ก)
    [Arguments]                         ${value}
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="weight-g"]/div/input                                                  300
    Sleep                               5
    Input Text                          //*[@id="weight-g"]/div/input                                                                  ${value}

กดเพิ่มรายการสินค้า
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="addstock"]/button                                                  300
    Sleep                               5
    Click Element                       //*[@id="addstock"]/button

เลือกรายการ
    Wait Until Element Is Visible       //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select                                                  300
    Sleep                               5
    Click Element                       //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select
    Sleep                               0.5
    Wait Until Element Is Visible       //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select/option[67]                                                  300
    Sleep                               5
    Click Element                       //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select/option[67]

กดปุ่มบันทึกบิล
    Sleep                               1
    Wait Until Element Is Visible       id:btnSaveandCheck                                                  300
    Sleep                               5
    Click Element                       id:btnSaveandCheck

