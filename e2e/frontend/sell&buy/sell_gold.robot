*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบและinit_data Infocustomer
Test Setup        เข้าหน้าPos ซื้อ-ขายทอง
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data Infocustomer
*** Variables ***

*** Test cases ***
Test1 ทดสอบการไม่สามารถแก้ไขวันที่
                # เข้าหน้าPos ซื้อ-ขายทอง
    เข้าหน้าขายทอง
    Sleep                                1
    พบPopup POS
    Sleep                                1
    ตรวจสอบไม่สามารถแก้ไขวันที่ได้
                #ไม่สามารถแก้ไขได้
    ปิดpopupPOS

Test2 ทดสอบการกดเพิ่มลูกค้าใหม่เมื่อกดปุ่มบวก
                # เข้าหน้าPos ซื้อ-ขายทอง
    เข้าหน้าขายทอง
    Sleep                                1
    พบPopup POS
    Sleep                                1
    เพิ่มลูกค้า
                #พบ pop upสร้างลูกค้า
    ปิดPopupเพิ่ม
    ปิดpopupPOS

# Test3 ทดสอบการแสดงชื่อลูกค้าทันทีเมื่อกดปุ่มบวกแล้วบันทึก
#                 # เข้าหน้าPos ซื้อ-ขายทอง
#                 เข้าหน้าขายทอง
#                 Sleep                                                 1
#                 พบPopup POS
#                 Sleep                                                 1
#                 เพิ่มลูกค้า
#                 #พบ pop upสร้างลูกค้า
#                 กรอกชื่อ                                              daniel
#                 Sleep                                                 1
#                 กรอกเบอร์มือถือ                                       0877920941
#                 Sleep                                                 2
#                 กดปุ่มบันทึก
#                 Sleep                                                 5
#                 ตรวจพบข้อความบน Alert                                 บันทึกข้อมูลสำเร็จ
#                 #บันทึกข้อมูลสำเร็จ
#                 พบข้อความ                                             daniel
#                 #พบรายชื่อลูกค้าที่เพิ่ม
#                 ปิดpopupPOS

Test4 ทดสอบการค้นหาชื่อลูกค้าด้วยตัวอักษร "linda"
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                                1
    เข้าหน้าขายทอง
    Sleep                                1
    พบPopup POS
    Sleep                                1
    หาชื่อลูกค้า                         linda
                #พบชื่อ linda
    ปิดpopupPOS

Test5 ทดสอบการแสดงรายชื่อพนักงานเมื่อกดตัวเลือกบันทึกพนักงานขาย
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                                1
    เข้าหน้าขายทอง
    Sleep                                1
    พบPopup POS
    Sleep                                1
    หาชื่อพนักงาน                        3                                                                                                    นางสาวสมใจ ตันหยี
                #พบข้อมูลพนักงาน
    ปิดpopupPOS

Test6 ทดสอบการกรอกข้อมูลไม่สำเร็จโดยการกรอกข้อมูลแต่ไม่เพิ่มรายการสินค้า
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                                1
    เข้าหน้าขายทอง
    Sleep                                1
    พบPopup POS
    Sleep                                1
    เลือกพนักงานคนที่                    3
    Sleep                                1
    กดปุุ่มบันทึกและชำระเงิน
    Sleep                                5
    ตรวจพบข้อความบน Alert                กรุณาเพิ่มรายการซื้อ-ขาย
                #แสดงalert กรุณาเพิ่มรายการซื้อขาย
    พบPopup เลือกสินค้าเพื่อขายทอง
                #เจอ pop up ซื้อขายทอง
    ปิดpopupเลือกรายการสินค้า
    ปิดpopupPOS

Test7 ทดสอบการแสดงจำนวนสินค้าเมื่อกดสินค้าเป็นสร้อยคอ2สลึง
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                                1
    เข้าหน้าขายทอง
    Sleep                                1
    พบPopup POS
    Sleep                                1
    เลือกพนักงานคนที่                    3
    Sleep                                1
    กดเพิ่มรายการสินค้า
    Sleep                                1
    พบPopup เลือกสินค้าเพื่อขายทอง
                #เจอ pop up ซื้อขายทอง
    Sleep                                1
    เลือกชื่อสินค้า
    Sleep                                1
    พบข้อความ                            คงเหลือ 752ชิ้น
                #พบจำนวนคงเหลือ 752ชิ้น
    ปิดpopupเลือกรายการสินค้า
    ปิดpopupPOS

Test8 ทดสอบการบันทึกรายการสินค้าไม่สำเร็จเมื่อไม่กรอกสินค้า
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                                1
    เข้าหน้าขายทอง
    Sleep                                1
    พบPopup POS
    Sleep                                1
    เลือกพนักงานคนที่                    3
    Sleep                                1
    กดเพิ่มรายการสินค้า
    Sleep                                1
    พบPopup เลือกสินค้าเพื่อขายทอง
                #เจอ pop up ซื้อขายทอง
    Sleep                                1
    กรอกราคาขาย                          8888
    Sleep                                1
    กดปุ่มลงรายการ
    Sleep                                1
    พบข้อความ                            เลือกสินค้า
    พบข้อความ                            *ไม่คงเหลือ
                #พบข้อความแจ้งเตือน
    ปิดpopupเลือกรายการสินค้า
    ปิดpopupPOS

Test9 ทดสอบการบันทึกรายการสินค้าไม่สำเร็จ เมื่อใส่จำนวนสินค้าเกินค่าคงเหลือ
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                                1
    เข้าหน้าขายทอง
    Sleep                                1
    พบPopup POS
    Sleep                                1
    เลือกพนักงานคนที่                    3
    Sleep                                1
    กดเพิ่มรายการสินค้า
    Sleep                                1
    พบPopup เลือกสินค้าเพื่อขายทอง
                #เจอ pop up ซื้อขายทอง
    Sleep                                1
    เลือกชื่อสินค้า
    Sleep                                1
    กรอกจำนวนสินค้า                      3000
    Sleep                                1
    ${value}                             Get value                                                                                            //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[4]/div/input
    Should Be Equal                      ${value}                                                                                             752
    Sleep                                1
    กดปุ่มลงรายการ
                #พบข้อความสินค้าในสต็อก=752
    Sleep                                1
    ปิดpopupPOS

Test10 ทดสอบการบันทึกรายการสินค้าไม่สำเร็จ เมื่อแก้ไขราคาขายต่ำกว่าราคาขายจริง
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                                1
    เข้าหน้าขายทอง
    Sleep                                1
    พบPopup POS
    Sleep                                1
    เลือกพนักงานคนที่                    3
    Sleep                                1
    กดเพิ่มรายการสินค้า
    Sleep                                1
    พบPopup เลือกสินค้าเพื่อขายทอง
                #เจอ pop up ซื้อขายทอง
    Sleep                                1
    เลือกชื่อสินค้า
    Sleep                                1
    กรอกราคาขาย                          1
    Sleep                                1
    กดปุ่มลงรายการ
    Sleep                                1
    พบข้อความ                            ต้องไม่น้อยกว่าราคาขายจริง
                #พบข้อความต้องไม่น้อยกว่าราคาขายจริง
    ปิดpopupเลือกรายการสินค้า
    ปิดpopupPOS

Test11 ทดสอบการบันทึกรายการสินค้าสำเร็จ เมื่อไม่แก้ไขราคาขาย
    เข้าหน้าPos ซื้อ-ขายทอง
    Maximize Browser Window
    Sleep                                1
    เข้าหน้าขายทอง
    Sleep                                1
    พบPopup POS
    Sleep                                1
    เลือกพนักงานคนที่                    3
    Sleep                                1
    กดเพิ่มรายการสินค้า
    Sleep                                1
    พบPopup เลือกสินค้าเพื่อขายทอง
                #เจอ pop up ซื้อขายทอง
    Sleep                                1
    เลือกชื่อสินค้า
    Sleep                                1
    กดปุ่มลงรายการ
    Sleep                                1
                #pop up เลือกรายการสินค้า ถูกปิด
    พบPopup POS
                #แสดง pop up ขายทอง
    Sleep                                1
    ตรวจสอบค่าแรงงาน                     0
                #พบค่าแรงเป็น 0
    ปิดpopupPOS

Test12 ทดสอบการบันทึกรายการสินค้าสำเร็จ เมื่อแก้ไขราคาขายให้สูงขึ้น
    เข้าหน้าPos ซื้อ-ขายทอง
    Maximize Browser Window
    Sleep                                1
    เข้าหน้าขายทอง
    Sleep                                1
    พบPopup POS
    Sleep                                1
    เลือกพนักงานคนที่                    3
    Sleep                                1
    กดเพิ่มรายการสินค้า
    Sleep                                1
    พบPopup เลือกสินค้าเพื่อขายทอง
                #เจอ pop up ซื้อขายทอง
    Sleep                                1
    เลือกชื่อสินค้า
    Sleep                                1
    กรอกราคาขาย                          10500
    Sleep                                1
    กดปุ่มลงรายการ
    Sleep                                1
                #pop up เลือกรายการสินค้า ถูกปิด
    พบPopup POS
                #แสดง pop up ขายทอง
    Sleep                                1
    ตรวจสอบค่าแรงงาน                     250
                #พบค่าแรงเป็น 250
    ปิดpopupPOS

Test13 ทดสอบการยกเลิกการขายทอง เมื่อกดปุ่มกากบาท
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                                1
    เข้าหน้าขายทอง
    Sleep                                1
    พบPopup POS
    Sleep                                1
    เลือกพนักงานคนที่                    3
    Sleep                                1
    กดเพิ่มรายการสินค้า
    Sleep                                1
    พบPopup เลือกสินค้าเพื่อขายทอง
                #เจอ pop up ซื้อขายทอง
    Sleep                                1
    เลือกชื่อสินค้า
    Sleep                                1
    กดปุ่มลงรายการ
    Sleep                                1
                #pop up เลือกรายการสินค้า ถูกปิด
    พบPopup POS
    ปิดpopupPOS
    Sleep                                1
    ไม่พบข้อความ                         AA000000010

Test14 ทดสอบการลบรายการสินค้าก่อนบันทึกบิลสำเร็จ เมื่อกดปุ่มกากบาทสี่เหลี่ยม
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                                1
    เข้าหน้าขายทอง
    Sleep                                1
    พบPopup POS
    Sleep                                1
    เลือกพนักงานคนที่                    3
    Sleep                                1
    กดเพิ่มรายการสินค้า
    Sleep                                1
    พบPopup เลือกสินค้าเพื่อขายทอง
                #เจอ pop up ซื้อขายทอง
    Sleep                                1
    เลือกชื่อสินค้า
    Sleep                                1
    กดปุ่มลงรายการ
    Sleep                                1
                #pop up เลือกรายการสินค้า ถูกปิด
    พบPopup POS
    Sleep                                1
    Wait Until Element Is Visible        id:textCost                                                                                          300
    Sleep                                1
    ลบรายการสินค้า
    Sleep                                5
    ตรวจพบข้อความบน Alert                ยืนยันลบรายการนี้
    Sleep                                1
    Wait Until Element Is Not Visible    id:textCost                                                                                          300
    ปิดpopupPOS

Test15 ทดสอบการยกเลิกการยืนยันลบรายการสินค้าก่อนบันทึกบิลสำเร็จ เมื่อกดปุ่มcancel
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                                1
    เข้าหน้าขายทอง
    Sleep                                1
    พบPopup POS
    Sleep                                1
    เลือกพนักงานคนที่                    3
    Sleep                                1
    กดเพิ่มรายการสินค้า
    Sleep                                1
    พบPopup เลือกสินค้าเพื่อขายทอง
                #เจอ pop up ซื้อขายทอง
    Sleep                                1
    เลือกชื่อสินค้า
    Sleep                                1
    กดปุ่มลงรายการ
    Sleep                                1
                #pop up เลือกรายการสินค้า ถูกปิด
    พบPopup POS
    Wait Until Element Is Visible        id:textCost                                                                                          300
    Sleep                                1
    ลบรายการสินค้า
    Sleep                                5
    ตรวจพบข้อความบน Alertแล้วCancel      ยืนยันลบรายการนี้
                #alertถูกปิด
    พบPopup POS
                #เจอหน้าขายทอง
    Wait Until Element Is Visible        id:textCost                                                                                          300
                #รายการที่ต้องการลบยังคงอยู่
    ปิดpopupPOS

Test16 ทดสอบการยืนยันการยืนยันลบรายการสินค้าก่อนบันทึกบิลสำเร็จ เมื่อกดปุ่มok
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                                1
    เข้าหน้าขายทอง
    Sleep                                1
    พบPopup POS
    Sleep                                1
    เลือกพนักงานคนที่                    3
    Sleep                                1
    กดเพิ่มรายการสินค้า
    Sleep                                1
    พบPopup เลือกสินค้าเพื่อขายทอง
                #เจอ pop up ซื้อขายทอง
    Sleep                                1
    เลือกชื่อสินค้า
    Sleep                                1
    กดปุ่มลงรายการ
    Sleep                                1
                #pop up เลือกรายการสินค้า ถูกปิด
    พบPopup POS
    Wait Until Element Is Visible        id:textCost                                                                                          300
    Sleep                                1
    ลบรายการสินค้า
    Sleep                                5
    ตรวจพบข้อความบน Alert                ยืนยันลบรายการนี้
    Sleep                                1
                #alertถูกปิด
    พบPopup POS
                #เจอหน้าขายทอง
    Wait Until Element Is Not Visible    id:textCost                                                                                          300
                #รายการที่ต้องการลบหายไป
    ปิดpopupPOS

Test17 ทดสอบการเคลียร์ข้อมูลเมื่อกดปุ่มเริ่มใหม่
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                                1
    เข้าหน้าขายทอง
    Sleep                                1
    พบPopup POS
    Sleep                                1
    เลือกพนักงานคนที่                    3
    Sleep                                1
    กดเพิ่มรายการสินค้า
    Sleep                                1
    พบPopup เลือกสินค้าเพื่อขายทอง
                #เจอ pop up ซื้อขายทอง
    Sleep                                1
    เลือกชื่อสินค้า
    Sleep                                1
    กดปุ่มลงรายการ
    Sleep                                1
                #pop up เลือกรายการสินค้า ถูกปิด
    พบPopup POS
    Sleep                                1
    กดปุ่มเริ่มใหม่
    Sleep                                1
    พบPopup POS
    Wait Until Element Is Not Visible    id:textCost                                                                                          300
                #ไม่พบข้อมูล
    ปิดpopupPOS

Test18 ทดสอบการไม่สามารถกดปุ่มพิมพ์VAT ได้ เมื่อยังไม่ทำการบันทึกบิลและชำระเงิน
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                                1
    เข้าหน้าขายทอง
    Sleep                                1
    พบPopup POS
    Sleep                                1
    เลือกพนักงานคนที่                    3
    Sleep                                1
    กดเพิ่มรายการสินค้า
    Sleep                                1
    พบPopup เลือกสินค้าเพื่อขายทอง
                #เจอ pop up ซื้อขายทอง
    เลือกชื่อสินค้า
    Sleep                                1
    กดปุ่มลงรายการ
    Sleep                                1
                #pop up เลือกรายการสินค้า ถูกปิด
    พบPopup POS
    Sleep                                1
    ตรวจสอบไม่สามารถกดปุ่มVatได้
    ปิดpopupPOS

Test19 ทดสอบการเข้าหน้าpop up preview เมื่อกดปุ่มพิมพ์VAT
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                                1
    เข้าหน้าขายทอง
    Sleep                                1
    พบPopup POS
    Sleep                                1
    เลือกพนักงานคนที่                    3
    Sleep                                1
    กดเพิ่มรายการสินค้า
    Sleep                                1
    พบPopup เลือกสินค้าเพื่อขายทอง
                #เจอ pop up ซื้อขายทอง
    Sleep                                1
    เลือกชื่อสินค้า
    Sleep                                1
    กดปุ่มลงรายการ
    Sleep                                1
                #pop up เลือกรายการสินค้า ถูกปิด
    พบPopup POS
    Sleep                                1
    กดปุ่มบันทึกและชำระเงิน
    Sleep                                1
    พบPopup ชำระเงิน
    Sleep                                1
    กรอกจำนวนรับเงินสด                   10250
    Sleep                                1
    กดปุ่มบันทึกชำระเงิน
    Sleep                                5
    ตรวจพบข้อความบน Alert                ยืนยันการชำระเงิน
    Sleep                                5
    ตรวจพบข้อความบน Alert                บันทึกบิลและชำระเงินสำเร็จ
    Sleep                                1
    Wait Until Element Is Visible        id:textCost                                                                                          300
    กดปุ่มVat
    Sleep                                10
    พบยอดเงินรับสุทธิ                           10,250.00
    Sleep                                5
    กดปุ่มปิดPopup Preview
    ปิดpopupPOS
    Wait Until Element Is Visible        id:sumMoney1                                                                                         300

*** Keywords ***

ตรวจสอบไม่สามารถแก้ไขวันที่ได้
    Wait Until Page Contains Element     //*[@id="contentModalPOS"]/div[2]/div/div/div[1]/div/form/div[1]/div/div/input                       300
    ${value} =                           Get Element Attribute                                                                                //*[@id="contentModalPOS"]/div[2]/div/div/div[1]/div/form/div[1]/div/div/input                       readonly
    Should Be Equal As Strings           ${value}                                                                                             true

เพิ่มลูกค้า
    Sleep                                1
    Wait Until Element Is Visible        id:addCustomerPOS                                                                                    300
    Sleep                                1
    Click Element                        id:addCustomerPOS
    Sleep                                1
    Wait Until Element Is Visible        id:AddCustomerModal                                                                                  300

หาชื่อลูกค้า
    [Arguments]                          ${name}
    Wait Until Element Is Visible        //*[@id="contentModalPOS"]/div[2]/div/div/div[1]/div/form/div[2]/div/div/div/div                     300
    Sleep                                1
    Click Element                        //*[@id="contentModalPOS"]/div[2]/div/div/div[1]/div/form/div[2]/div/div/div/div
    Sleep                                1
    Input Text                           //*[@id="contentModalPOS"]/div[2]/div/div/div[1]/div/form/div[2]/div/div/div/div/input               ${name}
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="contentModalPOS"]/div[2]/div/div/div[1]/div/form/div[2]/div/div/div/div/div[2]/div/div/b    300
    ${text}                              Get Text                                                                                             //*[@id="contentModalPOS"]/div[2]/div/div/div[1]/div/form/div[2]/div/div/div/div/div[2]/div/div/b
    Should Be Equal                      ${text}                                                                                              ${name}

หาชื่อพนักงาน
    [Arguments]                          ${value}                                                                                             ${name}
    Wait Until Page Contains Element     //*[@id="contentModalPOS"]/div[2]/div/div/div[1]/div/form/div[3]/div/div                             300
    Sleep                                1
    Click Element                        //*[@id="contentModalPOS"]/div[2]/div/div/div[1]/div/form/div[3]/div/div
    ${text}                              Get Text                                                                                             //*[@id="contentModalPOS"]/div[2]/div/div/div[1]/div/form/div[3]/div/div/div[2]/div[${value}]
    Should Be Equal                      ${text}                                                                                              ${name}

เลือกลูกค้าชื่อ
    [Arguments]                          ${name}
    Wait Until Page Contains Element     //*[@id="contentModalPOS"]/div[2]/div/div/div[1]/div/form/div[2]/div/div/div/div/input               300
    Sleep                                1
    Click Element                        //*[@id="contentModalPOS"]/div[2]/div/div/div[1]/div/form/div[2]/div/div/div/div/input
    Input Text                           ${name}                                                                                              //*[@id="contentModalPOS"]/div[2]/div/div/div[1]/div/form/div[2]/div/div/div/div/input
    Sleep                                5
    Wait Until Element Is Visible        ${name}
    Click Element                        //*[@id="contentModalPOS"]/div[2]/div/div/div[1]/div/form/div[2]/div/div/div/div/div[2]/div

กดปุุ่มบันทึกและชำระเงิน
    Wait Until Element Is Visible        id:btnSaveandCheck                                                                                   300
    Sleep                                1
    Click Element                        id:btnSaveandCheck

ปิดpopupเลือกรายการสินค้า
    Sleep                                1
    Wait Until Element Is Visible        id:btnCloseProductSelect                                                                             300
    Sleep                                1
    Click Element                        id:btnCloseProductSelect

เลือก %ทอง
    Wait Until Element Is Visible        //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[1]/select                     300
    Sleep                                1
    Click Element                        //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[1]/select
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[1]/select/option[3]           300
    Sleep                                1
    Click Element                        //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[1]/select/option[3]
    Sleep                                1

เลือกประเภทสินค้า
    Wait Until Element Is Visible        //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[2]/select                     300
    Sleep                                1
    Click Element                        //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[2]/select
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[2]/select/option[3]           300
    Sleep                                1
    Click Element                        //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[2]/select/option[3]
    Sleep                                1

กรอกราคาขาย
    [Arguments]                          ${value}
    Wait Until Element Is Visible        //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[2]/div[3]/div/input                  300
    Sleep                                1
    Click Element                        //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[2]/div[3]/div/input
    Sleep                                1
    Input Text                           //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[2]/div[3]/div/input                  ${value}

กรอกจำนวนสินค้า
    [Arguments]                          ${value}
    Wait Until Element Is Visible        //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[4]/div/input                  300
    Sleep                                1
    Click Element                        //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[4]/div/input
    Sleep                                1
    Input Text                           //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[4]/div/input                  ${value}

ตรวจสอบค่าแรงงาน
    [Arguments]                          ${value}
    Wait Until Element Is Visible        //*[@id="textCost"]/div                                                                              300
    ${text} =                            Get Text                                                                                             //*[@id="textCost"]/div
    Should Be Equal                      ${text}                                                                                              ${value}

ลบรายการสินค้า
    Wait Until Element Is Visible        id:btnDelete                                                                                         300
    Sleep                                1
    Double Click Element                 id:btnDelete

กดปุ่มเริ่มใหม่
    Wait Until Element Is Visible        id:btnRefesh                                                                                         300
    Sleep                                1
    Click Element                        id:btnRefesh

ไม่พบPopup ชำระเงิน
    Wait Until Element Is Not Visible    id:headerModalPayment                                                                                300
    Wait Until Element Is Not Visible    id:descriptModalPayment                                                                              300

ตรวจสอบไม่สามารถกดปุ่มVatได้
    Wait Until Page Contains Element     id:btnPrintVat                                                                                       300
    ${value} =                           Get Element Attribute                                                                                id:btnPrintVat                                                                                       disabled
    Should Be Equal As Strings           ${value}                                                                                             true

พบยอดเงินรับสุทธิ
    [Arguments]                          ${value}
    Wait Until Element Is Visible        //*[@id="table-to-xls"]/tbody[5]/tr[6]/td[3]                                                         300
    ${text} =                            Get Text                                                                                             //*[@id="table-to-xls"]/tbody[5]/tr[6]/td[3]
    Should Be Equal                      ${text}                                                                                              ${value}




