*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบและinit_data Infocustomer
Test Setup        เข้าหน้าPos ซื้อ-ขายทอง
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data Infocustomer
*** Variables ***

*** Test cases ***
Test50 ทดสอบการค้นหารายการซื้อ-ขายทองด้วย เลขที่บิล CH-01190800001
    # Set Selenium Speed      0.5
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                                        5
    เลือกวันที่ตั้งต้น
    Sleep                                        5
    กรอกเลขที่บิล                       CH-01190800001
    Sleep                               1
    กดปุ่มค้นหา
    Sleep                               1
    พบข้อความ                           CH-01190800001
    พบข้อความ                           linda
    ไม่พบข้อความ                        John
    #พบรายการเลขที่บิล CH-01190800001
    ตรวจสอบตัวเลขในช่องคำนวณ            1
    กดปุ่มเคลียร์

Test51 ทดสอบการค้นหารายการซื้อ-ขายทองด้วยเลขที่Vat AA000000001
    เข้าหน้าPos ซื้อ-ขายทอง
    เลือกวันที่ตั้งต้น
    กรอกเลขที่Vat                       AA000000001
    Sleep                               1
    กดปุ่มค้นหา
    Sleep                               1
    พบข้อความ                           AA000000001
    ไม่พบข้อความ                        linda
    พบข้อความ                           John
    ตรวจสอบตัวเลขในช่องคำนวณ            1
    กดปุ่มเคลียร์

Test52 ทดสอบการค้นหารายการซื้อ-ขายทองด้วยชื่อลูกค้า linda
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                               1
    เลือกวันที่ตั้งต้น
    Sleep                               1
    กรอกชื่อลูกค้า                      linda
    Sleep                               1
    กดปุ่มค้นหา
    Sleep                               1
    พบข้อความ                           linda
    พบข้อความ                           AA000000002
    พบข้อความ                           AA000000004
    พบข้อความ                           AA000000005
    Sleep                               5
    ตรวจสอบตัวเลขในช่องคำนวณ            3
    กดปุ่มเคลียร์

Test53 ทดสอบการค้นหารายการซื้อ-ขายทองด้วยประเภทชำระ เป็นเงินสด
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                               1
    เลือกวันที่ตั้งต้น
    Sleep                               1
    เลือกประเภทชำระเงินสด
    Sleep                               1
    กดปุ่มค้นหา
    Sleep                               1
    พบข้อความ                           เงินสด
    Sleep                               5
    ตรวจสอบตัวเลขในช่องคำนวณ            5
    กดปุ่มเคลียร์

Test54 ทดสอบการค้นหารายการซื้อ-ขายทอง ด้วยวันที่ปัจจุบัน
    เข้าหน้าขายทอง
    Sleep                               1
    พบPopup POS
    Sleep                               1
    เลือกพนักงานคนที่                   3
    Sleep                               5
    กดเพิ่มรายการสินค้า
    Sleep                               1
    พบPopup เลือกสินค้าเพื่อขายทอง
    Sleep                               1
    เลือกชื่อสินค้า
    Sleep                               1
    กดปุ่มลงรายการ
    Sleep                               1
    พบPopup POS
    กดปุ่มบันทึกและชำระเงิน
    Sleep                               1
    พบPopup ชำระเงิน
    Sleep                               1
    กรอกจำนวนรับเงินสด                  10250
    Sleep                               1
    กดปุ่มบันทึกชำระเงิน
    Sleep                               3
    ตรวจพบข้อความบน Alert               ยืนยันการชำระเงิน
    ตรวจพบข้อความบน Alert               บันทึกบิลและชำระเงินสำเร็จ
    Sleep                               1
    กดปุ่มVat
    Sleep                               1
    กดปุ่มปิดPopup Preview
    Sleep                               1
    ปิดpopupPOS
    Sleep                               1
    #ขายทอง
    เข้าหน้าซื้อทอง
    Sleep                               1
    พบPopup POS
    Sleep                               5
    กดเพิ่มรายการสินค้า
    Sleep                               1
    เลือกเปอร์เซ็นต์ทอง90เปอร์เซ็นต์
    Sleep                               1
    กรอกชื่อสินค้า                      สร้อยคอ
    Sleep                               1
    กรอกนน.(ก)                          9
    Sleep                               1
    กดปุ่มลงรายการ
    พบPopup POS
    Sleep                               1
    กดปุ่มบันทึกและชำระเงิน
    Sleep                               3
    ตรวจพบข้อความบน Alert               บันทึกบิลและชำระเงินสำเร็จ
    Sleep                               1
    กดปุ่มVat
    Sleep                               1
    กดปุ่มปิดPopup Preview
    Sleep                               1
    ปิดpopupPOS
    Sleep                               1
    #ซื้อทอง
    เข้าหน้าเปลี่ยนทอง
    Sleep                               1
    พบPopup POS
    Sleep                               1
    กดเพิ่มรายการสินค้า
    Sleep                               1
    กดเพิ่มรายการเปลี่ยนทอง
    Sleep                               1
    กดปุ่มลงรายการ
    Sleep                               1
    พบPopup POS
    Sleep                               1
    กดปุ่มบันทึกและชำระเงิน
    Sleep                               1
    พบPopup ชำระเงิน
    Sleep                               5
    กรอกจำนวนรับเงินสด                  200000
    Sleep                               1
    กดปุ่มบันทึกชำระเงิน
    Sleep                               5
    ตรวจพบข้อความบน Alert               ยืนยันการชำระเงิน
    Sleep                               5
    ตรวจพบข้อความบน Alert               บันทึกบิลและชำระเงินสำเร็จ
    Sleep                               1
    กดปุ่มVat
    Sleep                               1
    กดปุ่มปิดPopup Preview
    Sleep                               1
    ปิดpopupPOS
    #เปลี่ยนทอง
    เข้าหน้าPos ซื้อ-ขายทอง
    กดปุ่มค้นหา
    Sleep                               3
    ตรวจสอบตัวเลขในช่องคำนวณ            3
    กดปุ่มเคลียร์

Test55 ทดสอบการค้นหารายการซื้อ-ขายทอง ด้วยวันที่ปัจจุบันและวันก่อนหน้า
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                               1
    เลือกวันที่ตั้งต้น
    Sleep                               1
    กดปุ่มค้นหา
    Sleep                               3
    ตรวจสอบตัวเลขในช่องคำนวณ            8
    กดปุ่มเคลียร์

Test56 ทดสอบการค้นหารายการซื้อเพียงอย่างเดียว เมื่อกดปุ่มค้นหา
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                               1
    เลือกวันที่ตั้งต้น
    Sleep                               1
    เลือกรายการซื้อ
    Sleep                               1
    กดปุ่มค้นหา
    Sleep                               1
    พบข้อความ                           AA000000003
    พบข้อความ                           AA000000004
    พบข้อความ                           AA000000007
    Sleep                               5
    ตรวจสอบตัวเลขในช่องคำนวณ            3

Test57 ทดสอบการค้นหารายการขายเพียงอย่างเดียว เมื่อกดปุ่มค้นหา
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                               1
    เลือกวันที่ตั้งต้น
    Sleep                               1
    เลือกรายการขาย
    Sleep                               1
    กดปุ่มค้นหา
    Sleep                               1
    พบข้อความ                           AA000000001
    พบข้อความ                           AA000000002
    พบข้อความ                           AA000000006
    Sleep                               5
    ตรวจสอบตัวเลขในช่องคำนวณ            3

Test58 ทดสอบการค้นหารายการเปลี่ยนเพียงอย่างเดียว เมื่อกดปุ่มค้นหา
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                               1
    เลือกวันที่ตั้งต้น
    Sleep                               1
    เลือกรายการเปลี่ยน
    Sleep                               1
    กดปุ่มค้นหา
    Sleep                               1
    พบข้อความ                           AA000000005
    พบข้อความ                           AA000000008
    Sleep                               5
    ตรวจสอบตัวเลขในช่องคำนวณ            2

Test59 ทดสอบการค้นหารายการทั้งหมด เมื่อกดปุ่มค้นหา
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                               1
    เลือกวันที่ตั้งต้น
    Sleep                               1
    เลือกรายการทั้งหมด
    Sleep                               1
    กดปุ่มค้นหา
    Sleep                               5
    ตรวจสอบตัวเลขในช่องคำนวณ            8

Test63 ทดสอบการค้นหารายการวันนี้ทั้งหมด เมื่อกดปุ่มวันนี้
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                               1
    เลือกรายการทั้งหมด
    Sleep                               1
    กดปุ่มวันนี้
    Sleep                               1
    พบข้อความ                           AA000000006
    พบข้อความ                           AA000000007
    พบข้อความ                           AA000000008
    Sleep                               5
    ตรวจสอบตัวเลขในช่องคำนวณ            3

Test64 ทดสอบการยกเลิกการค้นหาเมื่อกดปุ่มเคลียร์
    เข้าหน้าPos ซื้อ-ขายทอง
    Sleep                               1
    เลือกวันที่ตั้งต้น
    Sleep                               1
    กรอกเลขที่บิล                       CH-01190800001
    Sleep                               1
    กรอกเลขที่Vat                       AA000000001
    Sleep                               1
    กรอกชื่อลูกค้า                      linda
    Sleep                               1
    เลือกประเภทชำระเงินสด
    Sleep                               1
    กดปุ่มเคลียร์
    Sleep                               5
    ตรวจสอบช่องว่าง                     //*[@id="searchBill"]
    ตรวจสอบช่องว่าง                     //*[@id="searchVat"]
    ตรวจสอบช่องว่าง                     //*[@id="searchNameCus"]
    ตรวจสอบช่องว่าง                     id:searchPayment

*** Keywords ***
เลือกวันที่ตั้งต้น
    Sleep                                                3
    Wait Until Element Is Visible                        //*[@id="searchDateStart"]                          300
    Sleep                                                3
    Double Click Element                                 //*[@id="searchDateStart"]
    : FOR  ${INDEX}  IN RANGE  0  11
    \    Sleep                                           3
    \    Click Element                                   //*[@id="searchDateStart"]/div[2]/div/button
    Click Element                                        //*[@id="searchDateStart"]/div[2]/div/div[2]/div[2]/div[1]/div[1]


กรอกเลขที่บิล
    [Arguments]                         ${value}
    Wait Until Page Contains Element    id:searchBill                                                  300
    Sleep                               5
    Click Element                       id:searchBill
    Sleep                               5
    Input Text                          //*[@id="searchBill"]/input                                              ${value}
    Sleep                               1

กรอกเลขที่Vat
    [Arguments]                         ${value}
    Wait Until Page Contains Element    id:searchVat                                                  300
    Sleep                               5
    Click Element                       id:searchVat
    Sleep                               5
    Input Text                          //*[@id="searchVat"]/input                                               ${value}
    Sleep                               1

กรอกชื่อลูกค้า
    [Arguments]                         ${value}
    Wait Until Page Contains Element    id:searchNameCus                                                  300
    Sleep                               5
    Click Element                       id:searchNameCus
    Sleep                               5
    Input Text                          //*[@id="searchNameCus"]/input                                           ${value}
    Sleep                               1

เลือกประเภทชำระเงินสด
    Wait Until Page Contains Element    id:searchPayment                                                  300
    Sleep                               5
    Click Element                       id:searchPayment
    Sleep                               5
    Wait Until Element Is Visible       //*[@id="searchPayment"]/div[2]/div[2]                                                  300
    Sleep                               5
    Click Element                       //*[@id="searchPayment"]/div[2]/div[2]
    Sleep                               1

เลือกรายการทั้งหมด
    Wait Until Page Contains Element    //*[@id="root-content"]/div/div/div/form[1]/div[2]/div[1]/div                                                  300
    Sleep                               5
    Click Element                       //*[@id="root-content"]/div/div/div/form[1]/div[2]/div[1]/div

เลือกรายการขาย
    Wait Until Page Contains Element    //*[@id="root-content"]/div/div/div/form[1]/div[2]/div[2]/div                                                  300
    Sleep                               5
    Click Element                       //*[@id="root-content"]/div/div/div/form[1]/div[2]/div[2]/div

เลือกรายการซื้อ
    Wait Until Page Contains Element    //*[@id="root-content"]/div/div/div/form[1]/div[2]/div[3]/div                                                  300
    Sleep                               5
    Click Element                       //*[@id="root-content"]/div/div/div/form[1]/div[2]/div[3]/div

เลือกรายการเปลี่ยน
    Wait Until Page Contains Element    //*[@id="root-content"]/div/div/div/form[1]/div[2]/div[4]/div                                                  300
    Sleep                               5
    Click Element                       //*[@id="root-content"]/div/div/div/form[1]/div[2]/div[4]/div

กดปุ่มค้นหา
    Wait Until Page Contains Element    id:btnSearch                                                  300
    Sleep                               1
    Double Click Element                id:btnSearch

กดปุ่มวันนี้
    Wait Until Page Contains Element    id:btnToday                                                  300
    Sleep                               1
    Click Element                       id:btnToday

กดปุ่มเคลียร์
    Wait Until Page Contains Element    id:btnClear                                                  300
    Sleep                               1
    Click Element                       id:btnClear

กดปุ่มพิมพ์รายงาน
    Wait Until Page Contains Element    id:btnPrintReport                                                  300
    Sleep                               1
    Click Element                       id:btnPrintReport

ตรวจสอบตัวเลขในช่องคำนวณ
    [Arguments]                         ${value}
    Wait Until Page Contains Element    //*[@id="totalRow"]/div/input                                                  300
    ${text}                             Get Value                                                                //*[@id="totalRow"]/div/input
    Should Be Equal                     ${text}                                                                  ${value}

ตรวจสอบช่องว่าง
    [Arguments]                         ${value}
    Wait Until Page Contains Element    ${value}                                                  300
    ${text}                             Get Value                                                                ${value}
    ${text_str} =                       Convert To String                                                        ${text}
    Should Be Equal                     ${text_str}                                                              None


