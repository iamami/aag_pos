*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบและinit_data Infocustomer
Test Setup        เข้าหน้าPos ซื้อ-ขายทอง
Test Teardown     ออกจากระบบ
Suite Teardown    ปิดbrowserและinit_data Infocustomer
*** Variables ***

*** Test cases ***
Test71 ทดสอบการเข้าหน้้าpop up preview เมื่อกดปุ่มพิมพ์รายงาน
    Sleep                               5
    Wait Until Page Contains Element    id:btnPrintReport                                                  300
    Sleep                               5
    Click Element                       id:btnPrintReport
    Sleep                               1
    พบข้อความ                           สรุปรายการซื้อ-ขายทอง
    พบข้อความ                           สาขา
    Wait Until Element Is Visible       id:closePreview                                                  300
    Sleep                               5
    Click Element                       closePreview




*** Keywords ***