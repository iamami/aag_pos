*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบAdminและinit_data
Test Setup        เข้าหน้าขายฝากหลัก
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data_admin
*** Variables ***

*** Test cases ***
Test1 ทดสอบการแสดงรายการขายฝากเมื่อเลือกสาขาเป็นกิ่งแก้ว
    เลือกสาขากิ่งแก้ว
    กดปุ่มค้นหารายการขายฝาก
    พบข้อความ                                            AB00001
    พบจำนวนแถว                                           1
                #พบรายการขายฝากของเฉพาะสาขากิ่งแก้ว

Test2 ทดสอบการแสดงปุ่มลบเมื่อกดปุ่มค้นหา
    กดปุ่มค้นหารายการขายฝาก
    Sleep                                                3
    พบปุุ่มยกเลิกรายการขายฝาก
                #พบปุ่มยกเลิก

Test3 ทดสอบการแสดงหน้าแก้ไขบิลเมื่อกดปุ่มแก้ไข
    กดปุ่มค้นหารายการขายฝาก
    Sleep                                                3
    กดปุ่มแก้ไขรายการขายฝาก
    Sleep                                                3
    พบหน้าแก้ไขบิลขายฝาก
    Sleep                                                3
                #พบหน้าแก้ไขบิลขายฝาก
    ปิดหน้าแก้ไขบิลขายฝาก

Test4 ทดสอบการค้นหารายการขายฝากไม่สำเร็จเมื่อเลือกรายการสินค้าแต่ไม่ใส่ชื่อรายการสินค้า
    Sleep                                                3
    เลือกรายการสินค้า
    Sleep                                                3
    กดปุ่มค้นหารายการขายฝาก
    Sleep                                                3
    พบข้อความ กรุณาเลือกชื่อสินค้า
                #พบข้อความ                                    กรุณาเลือกชื่อสินค้า

# Test5 ทดสอบการค้นหารายการขายฝากสำเร็จเมื่อเลือกรายการสินค้าและใส่ชื่อรายการสินค้าเป็น คอ1ส(ไม่มี Data)

Test6 ทดสอบการยกเลิกรายการขายฝากไม่สำเร็จเมื่อยังไม่ได้ทำการยกเลิกบิล
    กดปุ่มค้นหารายการขายฝาก
    พบปุุ่มยกเลิกรายการขายฝาก
    กดปุ่มยกเลิกรายการขายฝาก
    ตรวจพบข้อความบน Alert                                ไม่สามารถลบได้ กรุณายกเลิกบิลก่อน
                #พบalert ไม่สามารถลบได้ กรุณายกเลิกบิลก่อน


# //////////////////////////// Manual////////////////////////////////////////////
# Test7 ทดสอบการยกเลิกรายการไม่สำเร็จเมื่อกดปุ่มลบ
# Test8 ทดสอบการยกเลิกรายการไม่สำเร็จเมื่อยกเลิกการยืนยันยกเลิกรายการขายฝาก
# Test9 ทดสอบการยกเลิกรายการไม่สำเร็จเมื่อยกเลิกรายการขายฝากย้อนหลัง
# Test10 ทดสอบการยกเลิกรายการสำเร็จแก้ไขบิลเมื่อยกเลิกรายการขายฝากวันนี้
# Test11 ทดสอบการยกเลิกรายการไถ่คืนไม่สำเร็จเมื่อยกเลิกรายการที่ไม่อยสถานะไถ่คืน
# Test12 ทดสอบการยกเลิกรายการไถ่คืนไม่สำเร็จเมื่อยกเลิกรายการที่อยู่ในสถานะไถ่คืนย้อนหลัง
# Test13 ทดสอบการยกเลิกรายการไถ่คืนไม่สำเร็จเมื่อยกเลิกการยืนยันยกเลิกไถ่คืน
# Test14 ทดสอบการยกเลิกรายการไถ่คืนสำเร็จเมื่อยกเลิกรายการที่อยู่ในสถานะไถ่คืน
# Test15 ทดสอบการยกเลิกการคัดออกไม่สำเร็จเมื่อยกเลิกรายการที่ไม่อยู่ในสถานะคัดออก
# Test16 ทดสอบการยกเลิกการคัดออกไม่สำเร็จเมื่อยกเลิกการยืนยันการคัดออก
# Test17 ทดสอบการยกเลิกการคัดออกสำเร็จเมื่อยกเลิกรายการที่อยู่ในสถานะคัดออก
# Test18 ทดสอบการยกเลิกรายการขายฝากไม่สำเร็จเมื่อยกเลิกรายการขายฝากที่เป็นอยู่ในสถานะยกเลิกบิลแล้ว
# Test19 ทดสอบการยกเลิกการไถ่คืนไม่สำเร็จเมื่อยกเลิกรายการขายฝากที่เป็นอยู่ในสถานะยกเลิกบิลแล้ว
# Test20 ทดสอบการยกเลิกการคัดออกไม่สำเร็จเมื่อยกเลิกรายการขายฝากที่เป็นอยู่ในสถานะยกเลิกบิลแล้ว
# Test21 ทดสอบการแก้ไขรายการยกเลิกบิลไม่สำเร็จเมื่อทำการแก้ไขรายการที่อยู่ในสถานะยกเลิกบิลแล้ว
# Test22 ทดสอบการลบรายการยกเลิกบิลไม่สำเร็จเมื่อยกเลิกการยืนยันลบรายการที่อยู่ในสถานะยกเลิกบิลแล้ว
# Test23 ทดสอบการลบรายการยกเลิกบิลสำเร็จเมื่อลบรายการที่อยู่ในสถานะยกเลิกบิลแล้ว
# //////////////////////////// Manual////////////////////////////////////////////



Test24 ทดสอบการคัดออกไม่สำเร็จเมื่อไม่เลือกรายการขายฝากที่จะคัดออก
    Sleep                                                3
    เข้าหน้าคัดออก
    Sleep                                                3
    กดปุ่มคัดออก
    Sleep                                                3
    ตรวจพบข้อความบน Alert                                กรุณาเลือกรายการขายฝากเพื่อคัดออก
                #พบ alert กรุณาเลือกรายการขายฝากเพื่อคัดออก

Test25 ทดสอบการคัดออกไม่สำเร็จเมื่อยกเลิกการยืนยันการคัดออก
    เข้าหน้าคัดออก
    Sleep                                                1
    กดปุ่มค้นหาการคัดออก
    Sleep                                                1
    กดปุ่มเลือกรายการคัดออกทั้งหมด
    Sleep                                                1
    กดปุ่มคัดออก
    Sleep                                                1
    ตรวจพบข้อความบน Alertแล้วCancel                      ยืนยันคัดออก
    Sleep                                                1
    พบหน้าคัดออก
                #พบหน้าคัดออก

Test26 ทดสอบการคัดออกสำเร็จเมื่อปฏิเสธต้องการบันทึกประเภทและน้ำหนัก ไปยังสต็อกทองเก่าหรือไม่?
    เข้าหน้าคัดออก
    Sleep                                                1
    กดปุ่มค้นหาการคัดออก
    กดปุ่มเลือกรายการคัดออกทั้งหมด
    กดปุ่มคัดออก
    ตรวจพบข้อความบน Alert                                ยืนยันคัดออก
    ตรวจพบข้อความบน Alertแล้วCancel                      ต้องการบันทึกประเภทและน้ำหนัก ไปยังสต็อกทองเก่าหรือไม่?
    ตรวจพบข้อความบน Alert                                คัดออกสำเร็จ
                #พบ alert คัดออกสำเร็จ
    เข้าหน้าสต็อกทอง
    เข้าหน้าสต็อกทองเก่า
    sleep                                                1s
    Wait Until Element Is Visible                        //*[@id="table_width"]/div/div/div[3]/div[1]/div/div/div[2]/div/div[3]/div/div/div/div/div                              300
    ${weight}=                                           Get Text                                                                                                                //*[@id="table_width"]/div/div/div[3]/div[1]/div/div/div[2]/div/div[3]/div/div/div/div/div
    Should Be Equal                                      ${weight}                                                                                                               0.000
    sleep                                                1s
    Wait Until Element Is Visible                        //*[@id="table_width"]/div/div/div[3]/div[1]/div/div/div[2]/div/div[4]/div/div/div/div/div                              300
    ${weight}=                                           Get Text                                                                                                                //*[@id="table_width"]/div/div/div[3]/div[1]/div/div/div[2]/div/div[4]/div/div/div/div/div
    Should Be Equal                                      ${weight}                                                                                                               0
                #น้ำหนักและราคาทองเฉลี่ยในหน้าสต็อกทองเก่าไม่เปลี่ยนแปลง

Test27 ทดสอบการคัดออกสำเร็จเมื่อตกลงต้องการบันทึกประเภทและน้ำหนัก ไปยังสต็อกทองเก่าหรือไม่?
    เข้าหน้าคัดออก
    Sleep                                                3
    กดปุ่มค้นหาการคัดออก
    Sleep                                                3
    กดปุ่มเลือกรายการคัดออกทั้งหมด
    กดปุ่มคัดออก
    ตรวจพบข้อความบน Alert                                ยืนยันคัดออก
    ตรวจพบข้อความบน Alert                                ต้องการบันทึกประเภทและน้ำหนัก ไปยังสต็อกทองเก่าหรือไม่?
    ตรวจพบข้อความบน Alert                                คัดออกสำเร็จ
                #พบ alert คัดออกสำเร็จ
    เข้าหน้าสต็อกทอง
    เข้าหน้าสต็อกทองเก่า
    sleep                                                1s
    Wait Until Element Is Visible                        //*[@id="table_width"]/div/div/div[3]/div[1]/div/div/div[2]/div/div[3]/div/div/div/div/div                              300
    ${weight}=                                           Get Text                                                                                                                //*[@id="table_width"]/div/div/div[3]/div[1]/div/div/div[2]/div/div[3]/div/div/div/div/div
    Should Be Equal                                      ${weight}                                                                                                               9.200
    sleep                                                1s
    Wait Until Element Is Visible                        //*[@id="table_width"]/div/div/div[3]/div[1]/div/div/div[2]/div/div[4]/div/div/div/div/div                              300
    ${weight}=                                           Get Text                                                                                                                //*[@id="table_width"]/div/div/div[3]/div[1]/div/div/div[2]/div/div[4]/div/div/div/div/div
    Should Be Equal                                      ${weight}                                                                                                               21,434.478
                #พบความเปลี่ยนแปลงของน้ำหนักและราคาทองเฉลี่ยในหน้าสต็อกทองเก่า

Test28 ทดสอบการแสดงรายการคัดออกเมื่อระบุระยะเวลาเกินกำหนด150วัน
    Sleep                                                3
    สร้างรายการขายฝาก
    Sleep                                                3
    เลือกขายฝากที่สาขาลาดกระบัง
    Sleep                                                3
    เข้าหน้าคัดออก
    Sleep                                                3
    กรอกระยะเวลาเกินกำหนด
    Sleep                                                3
    กดปุ่มค้นหาการคัดออก
    Sleep                                                10
    พบข้อความ                                            AA00002
    #พบรายการ 1 รายการ


Test30 ทดสอบการแสดงรายการคัดออกเฉพาะสาขาเมื่อเลือกจำนวนเงินตั้งแต่ 100000 - 105000
    เข้าหน้าคัดออก
    Sleep                                                1
    กรอกช่วงจำนวนเงิน
    Sleep                                                1
    กดปุ่มค้นหาการคัดออก
    Sleep                                                10
    พบข้อความ                                            AA00002
    #พบรายการ 1 รายการ

Test31 ทดสอบการแสดงรายการคัดออกเฉพาะสาขาเมื่อใส่เลขที่ขายฝาก AA00002
    เข้าหน้าคัดออก
    Sleep                                                1
    กรอกเลขที่ใบขายฝาก
    Sleep                                                1
    กดปุ่มค้นหาการคัดออก
    Sleep                                                10
    พบข้อความ                                            AA00002
    #พบรายการ 1 รายการ

Test32 ทดสอบการคำนวณน้ำหนักและราคาทองเฉลี่ยบาทละถูกต้อง
    เข้าหน้าคัดออก
    Sleep                                                1
    กดปุ่มค้นหาการคัดออก
    Sleep                                                3
    กดปุ่มคำนวณน้ำหนักทอง
    Sleep                                                10
    ตรวจสอบจำนวนรายการ
    Sleep                                                10
    ตรวจสอบน้ำหนักรวม
    #พบจำนวนรายการ 1 รายการ น้ำหนัก 2.000


Test33 ทดสอบการแสดงสต็อกขายฝากเมื่อค้นหาจากสาขา
    เข้าหน้าสต็อกขายฝาก
    Sleep                                                2
    กดค้นหาที่สาขาลาดกระบัง
    Sleep                                                10
    พบข้อความ                                            AA00002
    #เจอข้อมูลสต็อกขายฝากสาขานั้นๆ


Test34 ทดสอบการแสดงสต็อกขายฝากเมื่อค้นหาจากสาขาทั้งหมด
    เข้าหน้าสต็อกขายฝาก
    Sleep                                                2
    กดทั้งหมดในหน้าสต๊อกขายฝาก
    Sleep                                                10
    พบข้อความ                                            AA00002
    พบข้อความ                                            AB00001
    #เจอข้อมูลสต็อกขายฝากทั้งหมด

Test35 ทดสอบการสร้างใบขายฝากตามสาขาที่เลือกสำเร็จ
    สร้างรายการขายฝาก
    กดปุ่มค้นหารายการขายฝาก
    Sleep                                                10
    Wait Until Page Contains                             AA00003
    #พบใบขายฝากล่าสุดที่มีสาขาตามที่เลือกไว้ในหน้าขายฝาก

Test36 ทดสอบการเข้าหน้าต่อดอกไม่สำเร็จเมื่อค้นหาใบขายฝากต่างสาขา
    เข้าหน้าไถ่คืน
    Sleep                                                3
    กรอกเลขที่รายการสาขากิ่งแก้ว
    Sleep                                                3
    กดปุ่มค้นหารายการหน้าไถ่คืน/เพิ่มลดเงินต้น/ต่อดอก
    Sleep                                                3
    ตรวจพบข้อความบน Alert                                ไม่พบข้อมูลขายฝากนี้
    #พบ alert ไม่พบข้อมูลขายฝากนี้
    Sleep                                                3
    เข้าหน้าเพิ่มลดเงินต้น
    Sleep                                                3
    กรอกเลขที่รายการสาขากิ่งแก้ว
    Sleep                                                3
    กดปุ่มค้นหารายการหน้าไถ่คืน/เพิ่มลดเงินต้น/ต่อดอก
    Sleep                                                3
    ตรวจพบข้อความบน Alert                                ไม่พบข้อมูลขายฝากนี้
    #พบ alert ไม่พบข้อมูลขายฝากนี้
    Sleep                                                3
    เข้าหน้าต่อดอก
    Sleep                                                3
    กรอกเลขที่รายการสาขากิ่งแก้ว
    Sleep                                                3
    กดปุ่มค้นหารายการหน้าไถ่คืน/เพิ่มลดเงินต้น/ต่อดอก
    Sleep                                                3
    ตรวจพบข้อความบน Alert                                ไม่พบข้อมูลขายฝากนี้
    #พบ alert ไม่พบข้อมูลขายฝากนี้

Test37 ทดสอบการเข้าหน้าต่อดอกสำเร็จเมื่อค้นหาใบขายฝากสาขาที่เลือก
    เข้าหน้าไถ่คืน
    Sleep                                                3
    กรอกเลขที่รายการสาขาลาดกระบัง
    Sleep                                                3
    กดปุ่มค้นหารายการหน้าไถ่คืน/เพิ่มลดเงินต้น/ต่อดอก
    Sleep                                                3
    Wait Until Element Is Visible                        //*[@id="modalInterest"]/div[1]                                                                                         300
    Sleep                                                3
    #สามารถเข้าหน้าต่อดอกได้
    #ปิดModalไถ่คืน และ บิลขายฝาก
    Wait Until Element Is Visible                        id:btnCloseInterest                                                                                                     300
    Sleep                                                3
    Click Element                                        id:btnCloseInterest
    Sleep                                                2
    Wait Until Element Is Visible                        id:btnClose                                                                                                             300
    Sleep                                                3
    Click Element                                        id:btnClose
    Sleep                                                3
    เข้าหน้าเพิ่มลดเงินต้น
    Sleep                                                3
    กรอกเลขที่รายการสาขาลาดกระบัง
    Sleep                                                3
    กดปุ่มค้นหารายการหน้าไถ่คืน/เพิ่มลดเงินต้น/ต่อดอก
    Sleep                                                3
    Wait Until Element Is Visible                        //*[@id="ICDCModal"]/div[1]                                                                                             300
    Sleep                                                3
    #สามารถเข้าหน้าต่อดอกได้
    #ปิดModalไถ่คืน และ บิลขายฝาก
    Wait Until Element Is Visible                        id:btnCloseInDe                                                                                                         300
    Sleep                                                3
    Click Element                                        id:btnCloseInDe
    Sleep                                                2
    Wait Until Element Is Visible                        id:btnClose                                                                                                             300
    Sleep                                                3
    Click Element                                        id:btnClose
    Sleep                                                3
    เข้าหน้าต่อดอก
    Sleep                                                3
    กรอกเลขที่รายการสาขาลาดกระบัง
    Sleep                                                3
    กดปุ่มค้นหารายการหน้าไถ่คืน/เพิ่มลดเงินต้น/ต่อดอก
    Sleep                                                3
    Wait Until Element Is Visible                        //*[@id="modalInterest"]/div[1]                                                                                         300
    Sleep                                                3
    #สามารถเข้าหน้าต่อดอกได้
    #ปิดModalไถ่คืน และ บิลขายฝาก
    Wait Until Element Is Visible                        id:btnCloseInterest                                                                                                     300
    Sleep                                                3
    Click Element                                        id:btnCloseInterest
    Sleep                                                2
    Wait Until Element Is Visible                        id:btnClose                                                                                                             300
    Sleep                                                3
    Click Element                                        id:btnClose

*** Keywords ***
เลือกสาขากิ่งแก้ว
    Wait Until Element Is Visible                        //*[@id="root-content"]/div/form/div/div/h3/div[2]/div                                                                  300
    Sleep                                                3
    Click Element                                        //*[@id="root-content"]/div/form/div/div/h3/div[2]/div
    Sleep                                                3
    Wait Until Element Is Visible                        //*[@id="root-content"]/div/form/div/div/h3/div[2]/div/div[2]/div[2]                                                    300
    Sleep                                                3
    Click Element                                        //*[@id="root-content"]/div/form/div/div/h3/div[2]/div/div[2]/div[2]

พบจำนวนแถว
    [Arguments]                                          ${value}
    ${count} =                                           Get Element Count                                                                                                       id:leaseID
    ${count_str} =                                       Convert To String                                                                                                       ${count}
    Should Be Equal                                      ${count_str}                                                                                                            ${value}

พบปุุ่มยกเลิกรายการขายฝาก
    Wait Until Element Is Visible                        //*[@id="client_width"]/div[2]/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a[2]                  300

กดปุ่มแก้ไขรายการขายฝาก
    Wait Until Element Is Visible                        //*[@id="client_width"]/div[2]/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]                  300
    Sleep                                                3
    Click Element                                        //*[@id="client_width"]/div[2]/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a[1]

พบหน้าแก้ไขบิลขายฝาก
    Wait Until Element Is Visible                        id:ModalLeaseForm                                                                                                       300

ปิดหน้าแก้ไขบิลขายฝาก
    Wait Until Element Is Visible                        id:btnClose                                                                                                             300
    Click Element                                        id:btnClose

เลือกรายการสินค้า
    Wait Until Element Is Visible                        //*[@id="client_width"]/div[1]/form/div[2]/div[3]/label/div                                                             300
    Sleep                                                3
    Click Element                                        //*[@id="client_width"]/div[1]/form/div[2]/div[3]/label/div

พบข้อความ กรุณาเลือกชื่อสินค้า
    Wait Until Page Contains                             กรุณาเลือกชื่อสินค้า

กดปุ่มยกเลิกรายการขายฝาก
    Wait Until Element Is Visible                        //*[@id="client_width"]/div[2]/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a[2]                  300
    Sleep                                                3
    Click Element                                        //*[@id="client_width"]/div[2]/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a[2]

เข้าหน้าคัดออก
    Go To                                                ${BASE_URL}lease/out
    Sleep                                                5


กดปุ่มคัดออก
    Sleep                                                3
    Wait Until Element Is Visible                        //*[@id="btnLeaseOutItem"]                                                                                              300
    Sleep                                                3
    Click Element                                        //*[@id="btnLeaseOutItem"]

กดปุ่มค้นหาการคัดออก
    Wait Until Element Is Visible                        //*[@id="root-content"]/div/div/div[2]/div/div/div[2]/div/div[1]/form/button                                            300
    Sleep                                                5
    Click Element                                        //*[@id="root-content"]/div/div/div[2]/div/div/div[2]/div/div[1]/form/button
    Sleep                                                5

กดปุ่มเลือกรายการคัดออกทั้งหมด
    Wait Until Element Is Visible                        //*[@id="root-content"]/div/div/div[2]/div/div/div[2]/div/div[2]/button[1]                                              300
    Sleep                                                5
    Click Element                                        //*[@id="root-content"]/div/div/div[2]/div/div/div[2]/div/div[2]/button[1]

พบหน้าคัดออก
    Wait Until Element Is Visible                        //*[@id="root-content"]/div/div/div[2]/div/div/div[2]/div/div[2]/button[1]                                              300

เข้าหน้าสต็อกทองเก่า
    Wait Until Element Is Visible                        id:oldStock                                                                                                             300
    Sleep                                                5
    Click Element                                        id:oldStock

สร้างรายการขายฝาก
    Sleep                                                3
    เลือกขายฝากที่สาขาลาดกระบัง
    Sleep                                                3
    กดปุ่มสร้างรายการขายฝาก
    Sleep                                                3
    พบPopupขายฝาก
    Sleep                                                5
    กรอกชื่อลูกค้า                                       linda
    Sleep                                                5
    กรอกจำนวนเงิน                                        102000
    Sleep                                                3
    Input Text                                           id:inputTime                                                                                                            1
    Sleep                                                3
    Wait Until Element Is Visible                        //*[@id="ModalLeaseForm"]/div[2]/div/div[2]/form/div/div/div[1]/div[7]/div[1]/div[1]/div/input                          300
    Sleep                                                3
    Double Click Element                                 //*[@id="ModalLeaseForm"]/div[2]/div/div[2]/form/div/div/div[1]/div[7]/div[1]/div[1]/div/input
    : FOR  ${INDEX}  IN RANGE  0  11
    \    Sleep                                           3
    \    Click Element                                   //*[@id="ModalLeaseForm"]/div[2]/div/div[2]/form/div/div/div[1]/div[7]/div[1]/div[2]/div/button[1]
    Click Element                                        //*[@id="ModalLeaseForm"]/div[2]/div/div[2]/form/div/div/div[1]/div[7]/div[1]/div[2]/div/div[2]/div[2]/div[2]/div[1]

    Sleep                                                3
    Wait Until Element Is Visible                        //*[@id="ModalLeaseForm"]/div[2]/div/div[2]/form/div/div/div[1]/div[7]/div[2]/div[1]/div/input                          300
    Sleep                                                5
    Double Click Element                                 //*[@id="ModalLeaseForm"]/div[2]/div/div[2]/form/div/div/div[1]/div[7]/div[2]/div[1]/div/input
    : FOR  ${INDEX}  IN RANGE  0  10
    \    Sleep                                           3
    \    Click Element                                   //*[@id="ModalLeaseForm"]/div[2]/div/div[2]/form/div/div/div[1]/div[7]/div[2]/div[2]/div/button[1]
    Click Element                                        //*[@id="ModalLeaseForm"]/div[2]/div/div[2]/form/div/div/div[1]/div[7]/div[2]/div[2]/div/div[2]/div[2]/div[2]/div[1]

    กดปุ่มเพิ่มรายการสินค้า
    Sleep                                                3
    พบPopupเลือกรายการสินค้า
    Sleep                                                5
    เลือก % ทอง
    Sleep                                                3
    ใส่ชื่อสินค้า
    Sleep                                                3
    ใส่จำนวน
    Sleep                                                3
    ใส่น้ำหนัก
    Sleep                                                3
    กดปุ่มเพิ่มสินค้า
    Sleep                                                3
    พบPopupขายฝาก
    Sleep                                                2
    กดปุ่มสร้าง
    Sleep                                                1
    พบPopupจ่ายเงิน
    Sleep                                                1
    เลือกพนักงานเพื่อเพิ่ม
    Sleep                                                1
    กดปุ่มบันทึกการจ่ายเงิน
    Sleep                                                5
    ตรวจพบข้อความบน Alert                                บันทึกข้อมูลสำเร็จ
    Sleep                                                3
    Click Element                                        id:btnClose
    #พบalert บันทึกข้อมูลสำเร็จ
    Sleep                                                3

กรอกระยะเวลาเกินกำหนด
    Sleep                                                3
    Wait Until Element Is Visible                        //*[@id="root-content"]/div/div/div[2]/div/div/div[2]/div/div[1]/form/table/tr[1]/td[2]/div/input                       300
    Sleep                                                3
    Input Text                                           //*[@id="root-content"]/div/div/div[2]/div/div/div[2]/div/div[1]/form/table/tr[1]/td[2]/div/input                       150

พบจำนวนแถวค้นหาการคัดออก
    [Arguments]                                          ${value}
    Wait Until Element Is Visible                        //*[@id="LeaseNum"]/div/div/div/div                                                                                     300
    ${count} =                                           Get Element Count                                                                                                       id:LeaseNum
    ${count_str} =                                       Convert To String                                                                                                       ${count}
    Should Be Equal                                      ${count_str}                                                                                                            ${value}

กรอกช่วงจำนวนเงิน
    Input Text                                           //*[@id="root-content"]/div/div/div[2]/div/div/div[2]/div/div[1]/form/table/tr[2]/td[2]/div[1]/input                    100000
    Sleep                                                2
    Input Text                                           //*[@id="root-content"]/div/div/div[2]/div/div/div[2]/div/div[1]/form/table/tr[2]/td[2]/div[2]/input                    105000

กรอกเลขที่ใบขายฝาก
    Sleep                                                2
    Input Text                                           //*[@id="root-content"]/div/div/div[2]/div/div/div[2]/div/div[1]/form/table/tr[3]/td[2]/div/input                       AA00002

กดปุ่มคำนวณน้ำหนักทอง
    Click Element                                        //*[@id="width_1"]/form/div/div[3]/button

ตรวจสอบจำนวนรายการ
    ${count}=                                            Get Value                                                                                                               //*[@id="width_1"]/form/div/div[1]/div/input
    Should Be Equal                                      ${count}                                                                                                                2

ตรวจสอบน้ำหนักรวม
    ${count}=                                            Get Value                                                                                                               //*[@id="width_1"]/form/div/div[2]/div/input
    Should Be Equal                                      ${count}                                                                                                                11.200

เข้าหน้าสต็อกขายฝาก
    Go To                                                ${BASE_URL}lease/stock
    Sleep                                                5

เข้าหน้าไถ่คืน
    Wait Until Element Is Visible                        id:btnLeaseRedeem                                                                                                       300
    Click Element                                        id:btnLeaseRedeem

เข้าหน้าเพิ่มลดเงินต้น
    Wait Until Element Is Visible                        id:btnLeaseDelete                                                                                                       300
    Click Element                                        id:btnLeaseDelete

เข้าหน้าต่อดอก
    Wait Until Element Is Visible                        id:btnLeaseInterest                                                                                                     300
    Click Element                                        id:btnLeaseInterest

กดค้นหาที่สาขาลาดกระบัง
    Wait Until Element Is Visible                        //*[@id="root-content"]/div/div/div[2]/div/div/form/div/div[2]/button[1]                                                300
    Click Element                                        //*[@id="root-content"]/div/div/div[2]/div/div/form/div/div[2]/button[1]

กดทั้งหมดในหน้าสต๊อกขายฝาก
    Wait Until Element Is Visible                        //*[@id="root-content"]/div/div/div[2]/div/div/form/div/div[2]/button[2]                                                300
    Click Element                                        //*[@id="root-content"]/div/div/div[2]/div/div/form/div/div[2]/button[2]

กรอกเลขที่รายการสาขากิ่งแก้ว
    Wait Until Element Is Visible                        id:inputBillNum                                                                                                         300
    Input Text                                           id:inputBillNum                                                                                                         AB00001

กรอกเลขที่รายการสาขาลาดกระบัง
    Wait Until Element Is Visible                        id:inputBillNum                                                                                                         300
    Input Text                                           id:inputBillNum                                                                                                         AA00003

กดปุ่มค้นหารายการหน้าไถ่คืน/เพิ่มลดเงินต้น/ต่อดอก
    Wait Until Element Is Visible                        id:btnSearch                                                                                                            300
    Click Element                                        id:btnSearch

เลือกขายฝากที่สาขาลาดกระบัง
    Wait Until Element Is Visible                        //*[@id="root-content"]/div/form/div/div/h3/div[2]/div                                                                  300
    Click Element                                        //*[@id="root-content"]/div/form/div/div/h3/div[2]/div
    Sleep                                                3
    Wait Until Element Is Visible                        //*[@id="root-content"]/div/form/div/div/h3/div[2]/div/div[2]/div[1]                                                    300
    Click Element                                        //*[@id="root-content"]/div/form/div/div/h3/div[2]/div/div[2]/div[1]
    Sleep                                                3

