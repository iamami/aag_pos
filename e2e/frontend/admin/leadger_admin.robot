*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบAdminและinit_data 8 Min.                       
Test Setup        เข้าหน้ารายรับรายจ่าย
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data_admin                
*** Variables ***

*** Test cases ***
Test1 ทดสอบการแสดงรายรับรายจ่ายเมื่อค้นหาเลขที่ใบสำคัญ
    Sleep                                   5
    กรอกเลขที่ใบสำคัญ                          PA-01191000003
    Sleep                                   5
    กดปุ่มค้นหาบิล        
    Sleep                                   10
    # พบรายการของสาขาตามสาขาที่เลือก
    พบจำนวนรายการรายรับรายจ่าย                 1 
    Sleep                                   5     
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[3]/div/div/div/div/div/div/span         ลาดกระบัง

Test2 ทดสอบการแสดงรายรับรายจ่ายเมื่อค้นหาเลขที่ใบสำคัญต่างสาขา
    Sleep                                   5
    กรอกเลขที่ใบสำคัญ                          PA-02191000003
    Sleep                                   5
    กดปุ่มค้นหาบิล 
    Sleep                                   10
    # พบรายการของสาขาตามเลขที่ใบสำคัญ
    พบจำนวนรายการรายรับรายจ่าย                 1  
    Sleep                                   5    
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[3]/div/div/div/div/div/div/span         กิ่งแก้ว

Test3 ทดสอบการแสดงรายรับรายจ่ายเมื่อเลือกประเภทรายจ่ายและรายการซื้อทอง
    Sleep                                   5
    เลือกประเภทรายจ่าย
    Sleep                                   5
    เลือกรายการซื้อ
    Sleep                                   5
    เปลี่ยนวันที่เริ่มต้นเป็น
    Sleep                                   5
    กดปุ่มค้นหาบิล 
    Sleep                                   10
    # พบรายการที่เป็นรายจ่ายที่เป็นรายการซื้อทองเฉพาะสาขาที่เลือก
    พบจำนวนรายการรายรับรายจ่าย                 2
    Sleep                                   5
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[3]/div/div/div/div/div/div/span             ลาดกระบัง
    Sleep                                   5
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div[2]/div/div/div[2]/div/div[3]/div/div/div/div/div/div/span          ลาดกระบัง

Test4 ทดสอบการแสดงรายรับรายจ่ายเฉพาะสาขา เมื่อเลือกวันที่ 1/9/19 - วันที่ปัจจุบัน
    Sleep                                   5
    เปลี่ยนวันที่เริ่มต้นเป็น
    Sleep                                   5
    กดปุ่มค้นหาบิล 
    Sleep                                   10
    # พบรายรับรายจ่ายเฉพาะสาขา
    พบจำนวนรายการรายรับรายจ่าย                 6
    Sleep                                   5
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[3]/div/div/div/div/div/div/span             ลาดกระบัง
    Sleep                                   5
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div[2]/div/div/div[2]/div/div[3]/div/div/div/div/div/div/span          ลาดกระบัง
    Sleep                                   5
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div[3]/div/div/div[2]/div/div[3]/div/div/div/div/div/div/span          ลาดกระบัง
    Sleep                                   5
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div[4]/div/div/div[2]/div/div[3]/div/div/div/div/div/div/span          ลาดกระบัง
    Sleep                                   5
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div[5]/div/div/div[2]/div/div[3]/div/div/div/div/div/div/span          ลาดกระบัง
    Sleep                                   5
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div[6]/div/div/div[2]/div/div[3]/div/div/div/div/div/div/span          ลาดกระบัง
    # พบรายรับรายจ่ายเฉพาะวันที่ที่ถูกเลือก
    Sleep                                   5
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div[1]/div/div/div[2]/div/div[6]/div/div/div/div/div/div/span          03/10/2019
    Sleep                                   5
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div[2]/div/div/div[2]/div/div[6]/div/div/div/div/div/div/span          03/10/2019
    Sleep                                   5
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div[3]/div/div/div[2]/div/div[6]/div/div/div/div/div/div/span          03/10/2019
    Sleep                                   5
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div[4]/div/div/div[2]/div/div[6]/div/div/div/div/div/div/span          03/10/2019
    Sleep                                   5
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div[5]/div/div/div[2]/div/div[6]/div/div/div/div/div/div/span          03/10/2019
    Sleep                                   5
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div[6]/div/div/div[2]/div/div[6]/div/div/div/div/div/div/span          03/10/2019

Test5 ทดสอบการแสดงรายรับรายจ่ายเฉพาะสาขา เมื่อเลือกชื่อลูกค้า
    กรอกชื่อลูกค้า                              ลูกค้าทั่วไป
    เปลี่ยนวันที่เริ่มต้นเป็น
    กดปุ่มค้นหาบิล 
    Sleep                                   10
    # พบรายรับรายจ่ายเฉพาะสาขา
    # พบรายรับรายจ่ายเฉพาะชื่อลูกค้าที่ถูกเลือก
    พบจำนวนรายการรายรับรายจ่าย                 2
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[3]/div/div/div/div/div/div/span             ลาดกระบัง
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div[2]/div/div/div[2]/div/div[3]/div/div/div/div/div/div/span          ลาดกระบัง
    
Test6 ทดสอบการแสดงรายรับรายจ่ายเฉพาะสาขา เมื่อเลือกเลขที่อ้างอิง
    กรอกเลขที่อ้างอิง                            AA00001
    กดปุ่มค้นหาบิล        
    Sleep                                   10
    # พบรายรับรายจ่ายเฉพาะสาขา
    พบจำนวนรายการรายรับรายจ่าย                 1      
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[3]/div/div/div/div/div/div/span         ลาดกระบัง
    # พบรายรับรายจ่ายเฉพาะเลขที่อ้างอิงที่เลือก
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div[1]/div/div/div[2]/div/div[5]/div/div/div/div/div/div/span      AA00001

Test7 ทดสอบการแสดงรายรับรายจ่ายต่างสาขา เมื่อเลือกเลขที่อ้างอิงต่างสาขา
    กรอกเลขที่อ้างอิง                            AB00001
    กดปุ่มค้นหาบิล        
    Sleep                                   10
    # พบรายรับรายจ่ายต่างสาขา
    พบจำนวนรายการรายรับรายจ่าย                 1      
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[3]/div/div/div/div/div/div/span         กิ่งแก้ว
    # พบรายรับรายจ่ายเฉพาะเลขที่อ้างอิงที่เลือก
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div[1]/div/div/div[2]/div/div[5]/div/div/div/div/div/div/span      AB00001

Test8 ทดสอบการแสดงรายรับรายจ่ายเฉพาะสาขา เมื่อค้นหาทั้งหมด
    เพิ่มรายการรายรับ
    Wait Until Page Contains Element        id:btnAll                                                                                                 300
    Click Element                           id:btnAll
    # พบรายรับรายจ่ายเฉพาะสาขาทั้งหมด
    Sleep                                   10
    พบจำนวนรายการรายรับรายจ่าย                 1
    ลบข้อมูลรายรับรายจ่าย

Test9 ทดสอบการแสดงรายการรายรับรายจ่ายเฉพาะสาขาตามที่เลือกในรายงาน
    เปลี่ยนวันที่เริ่มต้นเป็น
    กดปุ่มค้นหาบิล 
    Sleep                                   10
    พบจำนวนรายการรายรับรายจ่าย                 6
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[3]/div/div/div/div/div/div/span             ลาดกระบัง
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div[2]/div/div/div[2]/div/div[3]/div/div/div/div/div/div/span          ลาดกระบัง
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div[3]/div/div/div[2]/div/div[3]/div/div/div/div/div/div/span          ลาดกระบัง
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div[4]/div/div/div[2]/div/div[3]/div/div/div/div/div/div/span          ลาดกระบัง
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div[5]/div/div/div[2]/div/div[3]/div/div/div/div/div/div/span          ลาดกระบัง
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div[6]/div/div/div[2]/div/div[3]/div/div/div/div/div/div/span          ลาดกระบัง
    # พบรายการรายรับรายจ่ายเฉพาะสาขาที่ถูกเลือกในรายงาน
    กดปุ่มพิมพ์รายรับรายจ่าย
    ${count} =	                            Get Element Count	        //*[@id="leadger-preview"]/tr
    ${count} =	                            Evaluate                    ${count} - 2
    Should Be Equal As Numbers              ${count}                    6
    กดปุ่มปิดPopupPreview

Test10 ทดสอบการแสดงรายการรายรับรายจ่ายต่างสาขาตามที่เลือกในรายงาน
    เปลี่ยนสาขาเป็นกิ่งแก้ว
    เปลี่ยนวันที่เริ่มต้นเป็น
    กดปุ่มค้นหาบิล 
    Sleep                                   10
    พบจำนวนรายการรายรับรายจ่าย                 6
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[3]/div/div/div/div/div/div/span             กิ่งแก้ว
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div[2]/div/div/div[2]/div/div[3]/div/div/div/div/div/div/span          กิ่งแก้ว
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div[3]/div/div/div[2]/div/div[3]/div/div/div/div/div/div/span          กิ่งแก้ว
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div[4]/div/div/div[2]/div/div[3]/div/div/div/div/div/div/span          กิ่งแก้ว
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div[5]/div/div/div[2]/div/div[3]/div/div/div/div/div/div/span          กิ่งแก้ว
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div[6]/div/div/div[2]/div/div[3]/div/div/div/div/div/div/span          กิ่งแก้ว
    # พบรายการรายรับรายจ่ายต่างสาขาที่ถูกเลือกในรายงาน
    กดปุ่มพิมพ์รายรับรายจ่าย
    ${count} =	                            Get Element Count	        //*[@id="leadger-preview"]/tr
    ${count} =	                            Evaluate                    ${count} - 2
    Should Be Equal As Numbers              ${count}                    6
    กดปุ่มปิดPopupPreview

Test11 ทดสอบการเพิ่มรายการรายรับไม่สำเร็จเมื่อไม่กรอกรายการ
    กดปุ่มเพิ่มรายการรายรับรายจ่าย
    เลือกประเภทการชำระรายรับรายจ่าย              2
    เลือกพนักงานหน้ารายรับรายจ่าย
    กรอกชำระเงินสด                            3000
    กรอกหมายเหตุ                              ลืมรายการ
    กดปุ่มบันทึกรายรับรายจ่าย
    # พบข้อความ รายการ *กรุณาเลือก รายการ
    Page Should Contain                      *กรุณาเลือก รายการ
    กดปิดpopupรายรับรายจ่าย

Test12 ทดสอบการเพิ่มรายการรายรับไม่สำเร็จเมื่อไม่เลือกประเภท
    กดปุ่มเพิ่มรายการรายรับรายจ่าย
    เพิ่มรายการ                                ขาย             1
    เลือกพนักงานหน้ารายรับรายจ่าย
    กรอกหมายเหตุ                              ลืมประเภท
    กดปุ่มบันทึกรายรับรายจ่าย
    # พบข้อความ ประเภท *กรุณาเลือก ประเภทการชำระ 
    Page Should Contain                     *กรุณาเลือก ประเภทการชำระ        
    กดปิดpopupรายรับรายจ่าย

Test13 ทดสอบการเพิ่มรายการรายรับสำเร็จเมื่อกดปุ่มบันทึก
    เพิ่มรายการรายรับ
    Sleep                                   3
    # พบชื่อรายการที่เพิ่มล่าสุด
    พบจำนวนรายการรายรับรายจ่าย                1
    # ไม่พบชื่อรายการที่รายการของรายจ่าย
    Sleep                                   3
    เลือกประเภทรายจ่าย
    Sleep                                   3
    กดปุ่มค้นหาบิล 
    Sleep                                   10
    พบจำนวนรายการรายรับรายจ่าย               0
    # ไม่พบชื่อรายการในต่างสาขา
    Sleep                                   3
    เปลี่ยนสาขาเป็นกิ่งแก้ว
    Sleep                                   3
    Wait Until Page Contains Element      id:btnAll                                                                                                 300
    Sleep                                   3
    Click Element                         id:btnAll
    Sleep                                 3
    ลบรายการ
    Sleep                                   3

Test14 ทดสอบการเพิ่มรายการรายจ่ายสำเร็จเมื่อกดปุ่มบันทึก
    เพิ่มรายการรายจ่าย
    # พบชื่อรายการที่เพิ่มล่าสุด
    Sleep                                   4
    พบจำนวนรายการรายรับรายจ่าย               1
    # ไม่พบชื่อรายการที่รายการของรายรับ
    เลือกประเภทรายรับ
    Sleep                                   4
    กดปุ่มค้นหาบิล 
    Sleep                                   10
    พบจำนวนรายการรายรับรายจ่าย               0
    # ไม่พบชื่อรายการในต่างสาขา
    Sleep                                   4
    เปลี่ยนสาขาเป็นกิ่งแก้ว
    Sleep                                   4
    Wait Until Page Contains Element      id:btnAll                                                                                                 300
    Click Element                         id:btnAll
    Sleep                                 1
    ลบรายการ


*** Keywords ***
เปลี่ยนวันที่เริ่มต้นเป็น
    Sleep                                                3
    Wait Until Element Is Visible       //*[@id="root-content"]/div/div/form[1]/div/div/div[4]/div[1]/div/div/input
    Sleep                                                3    
    Click Element                       //*[@id="root-content"]/div/div/form[1]/div/div/div[4]/div[1]/div/div/input
    Sleep                                               1    
    : FOR  ${INDEX}  IN RANGE  0  5
    \    Sleep                                           3
    \    Click Element                                   //*[@id="root-content"]/div/div/form[1]/div/div/div[4]/div[1]/div[2]/div/button
    Wait Until Element Is Visible       //*[@id="root-content"]/div/div/form[1]/div/div/div[4]/div[1]/div[2]/div/div[2]/div[2]/div[1]/div[3]
    Click Element                       //*[@id="root-content"]/div/div/form[1]/div/div/div[4]/div[1]/div[2]/div/div[2]/div[2]/div[1]/div[3]

กรอกชื่อลูกค้า
    [Arguments]                          ${value}
    Wait Until Page Contains Element     //*[@id="searchNameCus"]/input                                                                        300
    Sleep                                1
    Input Text                           //*[@id="searchNameCus"]/input                                                                 ${value}
    Sleep                                5
    Wait Until Element Is Visible        //*[@id="searchNameCus"]/div[2]/div                                                          300
    Sleep                                1
    Click Element                        //*[@id="searchNameCus"]/div[2]/div

กดปุ่มพิมพ์รายรับรายจ่าย
    Wait Until Page Contains Element    //*[@id="root-content"]/div/div/form[2]/div[2]/div/button    300
    Sleep                               5
    Click Button                        //*[@id="root-content"]/div/div/form[2]/div[2]/div/button

เปลี่ยนสาขาเป็นกิ่งแก้ว
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="root-content"]/div/form/div/div[1]/h3/div[2]/div/div[1]                                                                          300
    Click Element                        //*[@id="root-content"]/div/form/div/div[1]/h3/div[2]/div/div[1]
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="root-content"]/div/form/div/div[1]/h3/div[2]/div/div[2]/div[2]                                                          300
    Click Element                        //*[@id="root-content"]/div/form/div/div[1]/h3/div[2]/div/div[2]/div[2]

เปลี่ยนสาขาเป็นลาดกระบัง
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="root-content"]/div/form/div/div[1]/h3/div[2]/div/div[1]                                                                          300
    Click Element                        //*[@id="root-content"]/div/form/div/div[1]/h3/div[2]/div/div[1]
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="root-content"]/div/form/div/div[1]/h3/div[2]/div/div[2]/div[1]                                                          300
    Click Element                        //*[@id="root-content"]/div/form/div/div[1]/h3/div[2]/div/div[2]/div[1]

กดปุ่มปิดPopupPreview
    Sleep                               5
    Click Element                       //*[@id="btnPrint"]
    Go To                               ${BASE_URL} 

กรอกเลขที่อ้างอิง
    [Arguments]                          ${value}
    Wait Until Page Contains Element     //*[@id="root-content"]/div/div/form[1]/div/div/div[2]/div[2]/div/input                                                                    300
    Sleep                                1
    Input Text                           //*[@id="root-content"]/div/div/form[1]/div/div/div[2]/div[2]/div/input                                                                    ${value}
    Sleep                                5
    Wait Until Element Is Visible        //*[@id="root-content"]/div/div/form[1]/div/div/div[2]/div[2]/div/div[2]/div                                                               300
    Sleep                                1
    Click Element                        //*[@id="root-content"]/div/div/form[1]/div/div/div[2]/div[2]/div/div[2]/div

เปลี่ยนประเภทเป็นรายจ่าย
    Wait Until Element Is Visible       id:outcome                              300
    Sleep                               1
    Click Element                       id:outcome
    Sleep                               1
    พบข้อความ                            รายละเอียดรายจ่าย
    Sleep                               1

ลบรายการ
    เปลี่ยนสาขาเป็นลาดกระบัง
    Wait Until Page Contains Element     id:btnAll                                                                                                 300
    Click Element                        id:btnAll
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="table_width"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a/i
    Sleep                                1
    Click Element                         //*[@id="table_width"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a/i
    Sleep                                   4
    ตรวจพบข้อความบน Alert                 ยืนยันลบรายการนี้
    Sleep                                   4
    ตรวจพบข้อความบน Alert                 ลบข้อมูลสำเร็จ

เพิ่มรายการ
    [Arguments]                          ${value}                                       ${value2}
    Wait Until Element Is Visible        //*[@id="add-order"]                           300
    Sleep                                1
    Click Element                        //*[@id="add-order"]
    Sleep                                3
    Wait Until Page Contains Element     //*[@id="order-name"]                          300
    Sleep                                1
    Input Text                           //*[@id="order-name"]                          ${value}
    Wait Until Element Is Visible        //*[@id="order-type"]/input                    300
    Sleep                                1
    Click Element                        //*[@id="order-type"]/input
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="order-type"]/div[2]/div[${value2}]    300
    Sleep                                1
    Click Element                        //*[@id="order-type"]/div[2]/div[${value2}]
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="order-save"]                          300
    Sleep                                1
    Click Element                        //*[@id="order-save"]
    
เพิ่มรายการรายรับ
    กดปุ่มเพิ่มรายการรายรับรายจ่าย
    เพิ่มรายการ                               ขาย             1
    เลือกประเภทการชำระรายรับรายจ่าย             2
    เลือกพนักงานหน้ารายรับรายจ่าย
    กรอกชำระเงินสด                           3000
    กรอกหมายเหตุ                             รายรับ
    กดปุ่มบันทึกรายรับรายจ่าย
    Sleep                                   4
    ตรวจพบข้อความบน Alert                   บันทึกข้อมูลสำเร็จ!

เพิ่มรายการรายจ่าย
    กดปุ่มเพิ่มรายการรายรับรายจ่าย
    เปลี่ยนประเภทเป็นรายจ่าย
    เพิ่มรายการ                              ซื้อ             2
    เลือกประเภทการชำระรายรับรายจ่าย            2
    เลือกพนักงานหน้ารายรับรายจ่าย
    กรอกชำระเงินสด                          3000
    กรอกหมายเหตุ                            รายจ่าย
    กดปุ่มบันทึกรายรับรายจ่าย
    Sleep                                   4
    ตรวจพบข้อความบน Alert                  บันทึกข้อมูลสำเร็จ!

ลบข้อมูลรายรับรายจ่าย
    Wait Until Page Contains Element     //*[@id="table_width"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a/i
    Sleep                                1
    Click Element                         //*[@id="table_width"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div/div/center/a/i
    Sleep                                   4
    ตรวจพบข้อความบน Alert                 ยืนยันลบรายการนี้
    Sleep                                   4
    ตรวจพบข้อความบน Alert                 ลบข้อมูลสำเร็จ
    