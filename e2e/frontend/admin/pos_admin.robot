*** Settings ***
Library           Selenium2Library
Library           BuiltIn
Resource          ${EXECDIR}/e2e/resource/keyword.robot
Resource          ${EXECDIR}/e2e/resource/variable.robot
Suite Setup       เปิดbrowserและเข้าสู่ระบบAdminและinit_data              
Test Setup        เข้าหน้าPos ซื้อ-ขายทอง
Test Teardown     เข้าสู่หน้าหลัก
Suite Teardown    ปิดbrowserและinit_data_admin
*** Variables ***

*** Test cases ***
Test1 ทดสอบการขายทองสำเร็จเมื่อเลือกสาขากิ่งแก้ว
    เปลี่ยนสาขาเป็นกิ่งแก้ว
    Sleep                           3
    เข้าหน้าขายทอง
    Sleep                           3
    พบPopup POS
    Sleep                           3
    กดเพิ่มรายการสินค้า
    Sleep                           3
    พบPopup เลือกสินค้าเพื่อขายทอง
    Sleep                           3
    เพิ่มรายการขายทอง
    Sleep                           3
    กดปุ่มลงรายการ
    Sleep                           3
    เลือกพนักงานคนที่                   1
    Sleep                           3
    กดปุ่มบันทึกและชำระเงิน
    Sleep                           3
    พบPopup ชำระเงิน
    Sleep                           3
    กรอกจำนวนรับเงินสด                20000
    Sleep                           3
    กดปุ่มบันทึกชำระเงิน
    Sleep                           3
    ตรวจพบข้อความบน Alert           ยืนยันการชำระเงิน
    Sleep                           3
    ตรวจพบข้อความบน Alert           บันทึกบิลและชำระเงินสำเร็จ
    Sleep                           3
    ปิดpopupPOS
    # พบรายการขายทองเป็นสาขากิ่งแก้ว
    Wait Until Element Is Visible           //*[@id="root-content"]/div/div/div/form[1]/div[2]/div[2]/div/label            300
    Sleep                           3
    Click Element                           //*[@id="root-content"]/div/div/div/form[1]/div[2]/div[2]/div/label
    Sleep                           3
    กดปุ่มค้นหาบิล
    Sleep                           3
    Element Should Contain          //*[@id="table_width"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[5]/div/div/div/div/div/div      กิ่งแก้ว
    Sleep                           3
    เปลี่ยนสาขาเป็นลาดกระบัง
    Sleep                           3
    # พบรายการทองที่ทำรายการสำเร็จบนหน้าสต็อกทองใหม่
    เข้าหน้าสต็อกทอง
    Sleep                           3
    เข้าหน้าสต็อกทองใหม่
    Sleep                           3
    เลือกสาขากิ่งแก้ว
    Sleep                           3
    กดปุ่มค้นหา
    Sleep                           3
    Wait Until Page Contains Element    //*[@id="table_width"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[2]/div/div/div          300
    Sleep                           3
    Click Element                       //*[@id="table_width"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[2]/div/div/div
    Sleep                               3
    ${count} =	                        Get Element Count	        //*[@id="table_width2"]/div/div[1]/div[3]/div
    Should Be Equal As Numbers          ${count}                    7
    Wait Until Page Contains Element    //*[@id="btnClose"]                 300
    Sleep                           3
    Click Element                       //*[@id="btnClose"]
    
Test2 ทดสอบการซื้อทองสำเร็จเมื่อเลือกสาขากิ่งแก้ว
    เปลี่ยนสาขาเป็นกิ่งแก้ว
    เข้าหน้าซื้อทอง
    พบPopup POS
    กดเพิ่มรายการสินค้า
    พบPopup เลือกสินค้าเพื่อขายทอง
    เลือกเปอร์เซ็นต์ทอง90เปอร์เซ็นต์
    กรอกชื่อสินค้า                      แหวน
    กรอกนน.(ก)                      10
    กดปุ่มลงรายการ
    เลือกพนักงานคนที่                   1
    กดปุ่มบันทึกและชำระเงิน
    ตรวจพบข้อความบน Alert           บันทึกบิลและชำระเงินสำเร็จ
    ปิดpopupPOS
    # พบรายการซื้อทองเป็นสาขากิ่งแก้ว
    Wait Until Element Is Visible           //*[@id="root-content"]/div/div/div/form[1]/div[2]/div[3]/div/label             300
    Click Element                           //*[@id="root-content"]/div/div/div/form[1]/div[2]/div[3]/div/label
    กดปุ่มค้นหาบิล
    Element Should Contain          //*[@id="table_width"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[5]/div/div/div/div/div/div      กิ่งแก้ว
    เปลี่ยนสาขาเป็นลาดกระบัง

Test3 ทดสอบการเปลี่ยนทองสำเร็จเมื่อเลือกสาขากิ่งแก้ว
    เปลี่ยนสาขาเป็นกิ่งแก้ว
    เข้าหน้าเปลี่ยนทอง
    พบPopup POS
    กดเพิ่มรายการสินค้า
    พบPopup เลือกสินค้าเพื่อขายทอง
    เพิ่มรายการขายทอง
    เลือกเปอร์เซ็นต์ทอง90เปอร์เซ็นต์
    กรอกชื่อสินค้า                      แหวน9.2ก.
    กรอกนน.(ก)                      9.2
    กดปุ่มลงรายการ
    เลือกพนักงานคนที่                   1
    กดปุ่มบันทึกและชำระเงิน
    พบPopup ชำระเงิน
    กรอกจำนวนรับเงินสด                2000
    กดปุ่มบันทึกชำระเงิน
    ตรวจพบข้อความบน Alert           ยืนยันการชำระเงิน
    ตรวจพบข้อความบน Alert           บันทึกบิลและชำระเงินสำเร็จ
    ปิดpopupPOS
    # พบรายการเปลี่ยนทองเป็นสาขากิ่งแก้ว
    Wait Until Element Is Visible           //*[@id="root-content"]/div/div/div/form[1]/div[2]/div[4]/div/label          300
    Click Element                           //*[@id="root-content"]/div/div/div/form[1]/div[2]/div[4]/div/label
    กดปุ่มค้นหาบิล
    Element Should Contain          //*[@id="table_width"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[5]/div/div/div/div/div/div      กิ่งแก้ว
    เปลี่ยนสาขาเป็นลาดกระบัง
    # พบรายการทองที่ทำรายการสำเร็จบนหน้าสต็อกทองใหม่
    เข้าหน้าสต็อกทอง
    เข้าหน้าสต็อกทองใหม่
    เลือกสาขากิ่งแก้ว
    กดปุ่มค้นหา
    Wait Until Page Contains Element    //*[@id="table_width"]/div/div[1]/div[3]/div/div/div/div[2]/div    300
    Click Element                       //*[@id="table_width"]/div/div[1]/div[3]/div/div/div/div[2]/div
    Sleep                               1
    ${count} =	                        Get Element Count	        //*[@id="table_width2"]/div/div[1]/div[3]/div
    Should Be Equal As Numbers          ${count}                    8
    Wait Until Page Contains Element    //*[@id="btnClose"]                 300
    Click Element                       //*[@id="btnClose"]

Test4 ทดสอบการแสดงรายการเมื่อค้นหาเลขที่บิล
    กรอกเลขที่บิล                               CH-01191000001
    กดปุ่มค้นหาบิล
    # พบรายการซื้อขายทองเฉพาะสาขา
    # ไม่พบรายการซื้อขายทองต่างสาขา
    ตรวจสอบจำนวนบิล                          1
    ตรวจสาขา                                1           ลาดกระบัง
    
Test5 ทดสอบการแสดงรายการเมื่อค้นหาเลขที่Vat
    กรอกเลขที่Vat                           AA000000004
    กดปุ่มค้นหาบิล
    # พบรายการซื้อขายทองเฉพาะสาขา
    # ไม่พบรายการซื้อขายทองต่างสาขา
    ตรวจสอบจำนวนบิล                          1
    ตรวจสาขา                                1           ลาดกระบัง

Test6 ทดสอบการแสดงรายการซื้อ/ขาย/เปลี่ยนทอง เฉพาะสาขาเมื่อเลือกลูกค้าทั่วไป
    Sleep                               5
    เปลี่ยนวันที่เริ่มต้นเป็น
    Sleep                               5
    กรอกชื่อลูกค้าทั่วไป
    Sleep                               5
    กดปุ่มค้นหาบิล
    # พบรายการซื้อขายเฉพาะสาขาที่เลือกของลูกค้าทั่วไป
    Sleep                               5
    ตรวจสอบจำนวนบิล                          4
    ตรวจสาขา                                1           ลาดกระบัง
    ตรวจชื่อลูกค้า                              1           ลูกค้าทั่วไป
    ตรวจสาขา                                2           ลาดกระบัง
    ตรวจชื่อลูกค้า                              2           ลูกค้าทั่วไป
    ตรวจสาขา                                3           ลาดกระบัง
    ตรวจชื่อลูกค้า                              3           ลูกค้าทั่วไป
    ตรวจสาขา                                4           ลาดกระบัง
    ตรวจชื่อลูกค้า                              4           ลูกค้าทั่วไป

Test7 ทดสอบการแสดงรายการซื้อ/ขาย/เปลี่ยนทอง เฉพาะสาขาเมื่อเลือกประเภทชำระเป็นเงินสด
    เปลี่ยนวันที่เริ่มต้นเป็น
    เลือกประเภทชำระเงินสด
    กดปุ่มค้นหาบิล
    # พบรายการซื้อขายเฉพาะสาขาที่ประเภทชำระเป็นเงินสด
    Sleep                               5
    ตรวจสอบจำนวนบิล                          4

Test8 ทดสอบการแสดงรายการซื้อ/ขาย/เปลี่ยนทอง เฉพาะสาขาเมื่อเลือกวันที่เป็น 1/9/19 - วันที่ปัจจุบัน
    เปลี่ยนวันที่เริ่มต้นเป็น
    กดปุ่มค้นหาบิล
    # พบรายการซื้อขายเฉพาะสาขาตามวันที่เลือก
    Sleep                               5
    ตรวจสอบจำนวนบิล                          4

Test9 ทดสอบการแสดงรายการซื้อ/ขาย/เปลี่ยนทอง เฉพาะสาขาเมื่อเลือกวันที่เป็น 1/9/19 - วันที่ปัจจุบัน เเล้วเลือกทั้งหมด
    เปลี่ยนวันที่เริ่มต้นเป็น
    Wait Until Element Is Visible           //*[@id="root-content"]/div/div/div/form[1]/div[2]/div[1]/div/label             300
    Click Element                           //*[@id="root-content"]/div/div/div/form[1]/div[2]/div[1]/div/label
    กดปุ่มค้นหาบิล
    # พบรายการซื้อขายเฉพาะสาขาที่เลือกของรายการทั้งหมด
    Sleep                               5
    ตรวจสอบจำนวนบิล                          4

Test10 ทดสอบการแสดงรายการซื้อ/ขาย/เปลี่ยนทอง เฉพาะสาขาเมื่อเลือกวันที่เป็น 1/9/19 - วันที่ปัจจุบัน เเล้วเลือกรายการขาย
    เปลี่ยนวันที่เริ่มต้นเป็น
    Wait Until Element Is Visible           //*[@id="root-content"]/div/div/div/form[1]/div[2]/div[2]/div/label            300
    Click Element                           //*[@id="root-content"]/div/div/div/form[1]/div[2]/div[2]/div/label
    กดปุ่มค้นหาบิล
    # พบรายการขายเฉพาะสาขาที่เลือกของรายการขาย
    Sleep                               5
    ตรวจสอบจำนวนบิล                          1

Test11 ทดสอบการแสดงรายการซื้อ/ขาย/เปลี่ยนทอง เฉพาะสาขาเมื่อเลือกวันที่เป็น 1/9/19 - วันที่ปัจจุบัน เเล้วเลือกรายการซื้อ
    เปลี่ยนวันที่เริ่มต้นเป็น
    Wait Until Element Is Visible           //*[@id="root-content"]/div/div/div/form[1]/div[2]/div[3]/div/label             300
    Click Element                           //*[@id="root-content"]/div/div/div/form[1]/div[2]/div[3]/div/label
    กดปุ่มค้นหาบิล
    # พบรายการซื้อเฉพาะสาขาที่เลือก
    Sleep                               5
    ตรวจสอบจำนวนบิล                          2

Test12 ทดสอบการแสดงรายการซื้อ/ขาย/เปลี่ยนทอง เฉพาะสาขาเมื่อเลือกวันที่เป็น 1/9/19 - วันที่ปัจจุบัน เเล้วเลือกรายการเปลี่ยน
    เปลี่ยนวันที่เริ่มต้นเป็น
    Wait Until Element Is Visible           //*[@id="root-content"]/div/div/div/form[1]/div[2]/div[4]/div/label          300
    Click Element                           //*[@id="root-content"]/div/div/div/form[1]/div[2]/div[4]/div/label
    กดปุ่มค้นหาบิล
    # พบรายการเปลี่ยนเฉพาะสาขาที่เลือก
    Sleep                               5
    ตรวจสอบจำนวนบิล                          1

Test13 ทดสอบการแสดงรายการวันนี้ เฉพาะสาขาเมื่อเลือกรายการวันนี้
    เปลี่ยนสาขาเป็นกิ่งแก้ว
    Wait Until Page Contains Element     id:btnToday                                                                                          300
    Sleep                                1
    Click Element                        id:btnToday
    # พบรายการวันนี้เฉพาะสาขาที่เลือก
    Sleep                               5
    ตรวจสอบจำนวนบิล                       3
    เปลี่ยนสาขาเป็นลาดกระบัง

Test14 ทดสอบการแสดงรายการซื้อขายทองเมื่อเลือกเลขที่บิลของสาขากิ่งแก้วและเลือกลูกค้าทั่วไป
    เปลี่ยนสาขาเป็นกิ่งแก้ว
    กรอกชื่อลูกค้าทั่วไป
    กดปุ่มค้นหาบิล
    # พบรายการ1รายการ
    Sleep                               5
    ตรวจสอบจำนวนบิล                          3
    ตรวจสาขา                                1           กิ่งแก้ว
    ตรวจชื่อลูกค้า                              1           ลูกค้าทั่วไป
    ตรวจสาขา                                2           กิ่งแก้ว
    ตรวจชื่อลูกค้า                              2           ลูกค้าทั่วไป
    ตรวจสาขา                                3           กิ่งแก้ว
    ตรวจชื่อลูกค้า                              3           ลูกค้าทั่วไป
    เปลี่ยนสาขาเป็นลาดกระบัง

Test15 ทดสอบการแสดงรายการซื้อขายทองเมื่อเลือกลูกค้าทั่วไปและเลือกรายการขาย
    เปลี่ยนวันที่เริ่มต้นเป็น
    กรอกชื่อลูกค้าทั่วไป
    Wait Until Element Is Visible           //*[@id="root-content"]/div/div/div/form[1]/div[2]/div[2]/div/label            300
    Click Element                           //*[@id="root-content"]/div/div/div/form[1]/div[2]/div[2]/div/label
    กดปุ่มค้นหาบิล
    # พบเฉพาะรายการขายของลูกค้าทั่วไป
    Sleep                               5
    ตรวจสอบจำนวนบิล                          1

Test16 ทดสอบการแสดงสาขาในรายงาน
    เปลี่ยนวันที่เริ่มต้นเป็น
    กดปุ่มค้นหาบิล
    Sleep                               5
    ตรวจสอบจำนวนบิล                          4
    กดปุ่มพิมพ์รายงาน
    # พบสาขาที่เลือกในรายงาน
    Wait Until Page Contains Element        //*[@id="table-to-xls"]/thead/tr[3]/th
    Element Should Contain                  //*[@id="table-to-xls"]/thead/tr[3]/th              ลาดกระบัง
    กดปุ่มปิดPopupPreview

Test19 ทดสอบการยกเลิกรายการไม่สำเร็จ เมื่อไม่กรอกสาเหตุ
    เปลี่ยนวันที่เริ่มต้นเป็น
    กดปุ่มค้นหาบิล
    Sleep                               5
    เลือกรายการบิล                         1
    Wait Until Element Is Visible       //*[@id="cancel-this"]                  300
    Sleep                               1
    Click Element                       //*[@id="cancel-this"]
    # พบ alert กรุณาระบุหมายเหตุ
    Sleep                               5
    ตรวจพบข้อความบน Alert               กรุณาระบุหมายเหตุ    
    Wait Until Element Is Visible       //*[@id="btnClose"]
    Click Element                       //*[@id="btnClose"]
    Sleep                               1

Test20 ทดสอบการยกเลิกรายการสำเร็จ เมื่อกรอกสาเหตุ
    เปลี่ยนวันที่เริ่มต้นเป็น
    กดปุ่มค้นหาบิล
    Sleep                               5
    เลือกรายการบิล                         1
    Wait Until Element Is Visible       //*[@id="cancel-this"]                  300
    Sleep                               1
    Wait Until Element Is Visible       //*[@id="contentModalPOS"]/div[2]/div/div/div[1]/div/form/div[4]/div/textarea       300
    Input Text                          //*[@id="contentModalPOS"]/div[2]/div/div/div[1]/div/form/div[4]/div/textarea       ยกเลิกบิล
    Click Element                       //*[@id="cancel-this"]
    # พบ alert ยกเลิกบิลสำเร็จ
    Sleep                               5
    ตรวจพบข้อความบน Alert               ยกเลิกบิลสำเร็จ    
    Wait Until Element Is Visible       //*[@id="btnClose"]
    Click Element                       //*[@id="btnClose"]
    Sleep                               1
    # รายการในสต็อกทองใหม่เพิ่มยกเลิกบิล
    เข้าหน้าสต็อกทอง
    Sleep                               5
    เข้าหน้าสต็อกทองใหม่
    Sleep                               5
    เลือกสาขาลาดกระบัง
    Sleep                               5
    กดปุ่มค้นหา
    Sleep                               5
    Wait Until Element Is Visible       //*[@id="table_width"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[3]/div/div    300
    Sleep                               5
    Click Element                       //*[@id="table_width"]/div/div[1]/div[3]/div/div/div/div[2]/div/div[3]/div/div
    Sleep                               10
    Page Should Contain                 ยกเลิกบิล
    Sleep                               5
    Wait Until Element Is Visible    //*[@id="btnClose"]                 300
    Sleep                               5
    Click Element                       //*[@id="btnClose"]
    Sleep                               1

*** Keywords ***

เปลี่ยนวันที่เริ่มต้นเป็น
    Wait Until Element Is Visible       //*[@id="searchDateStart"]/div[1]/div/input
    Sleep                           3
    Click Element                       //*[@id="searchDateStart"]/div[1]/div/input
    Sleep                                               1
    : FOR  ${INDEX}  IN RANGE  0  11
    \    Sleep                                           3
    \    Click Element                                   //*[@id="searchDateStart"]/div[2]/div/button
    Wait Until Element Is Visible       //*[@id="searchDateStart"]/div[2]/div/div[2]/div[2]/div[1]/div[3]
    Sleep                           3
    Click Element                       //*[@id="searchDateStart"]/div[2]/div/div[2]/div[2]/div[1]/div[3]
    


กรอกเลขที่บิล
    [Arguments]                         ${value}
    Wait Until Page Contains Element    id:searchBill                                       300
    Sleep                           3
    Click Element                       id:searchBill
    Sleep                           3
    Input Text                          //*[@id="searchBill"]/input                         ${value}
    Sleep                               3

เลือกประเภทชำระเงินสด
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="searchPayment"]                                                                                     300
    Sleep                           3
    Click Element                        //*[@id="searchPayment"]
    Sleep                           3
    Wait Until Element Is Visible        //*[@id="searchPayment"]/div[2]/div[2]                                                        300
    Sleep                               3
    Click Element                        //*[@id="searchPayment"]/div[2]/div[2]

กรอกเลขที่Vat
    [Arguments]                         ${value}
    Wait Until Page Contains Element    id:searchVat                                        300
    Sleep                           3
    Click Element                       id:searchVat
    Sleep                           3
    Input Text                          //*[@id="searchVat"]/input                          ${value}
    Sleep                               3

กรอกชื่อลูกค้าทั่วไป
    Wait Until Page Contains Element    id:searchNameCus                                    300
    Sleep                           3
    Click Element                       id:searchNameCus
    Sleep                           3
    Input Text                          //*[@id="searchNameCus"]/input                      ลูกค้าทั่วไป
    Sleep                               3

ตรวจสอบจำนวนบิล
    [Arguments]                         ${value}
    Sleep                               1
    ${count} =	                        Get Element Count	        //*[@id="table_width"]/div/div[1]/div[3]/div
    Should Be Equal As Numbers          ${count}                    ${value}

ตรวจสาขา
    [Arguments]                             ${value}            ${value2}
    Wait Until Page Contains Element        //*[@id="table_width"]/div/div[1]/div[3]/div[${value}]/div/div/div[2]/div/div[5]/div/div/div/div/div/div        300
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div[${value}]/div/div/div[2]/div/div[5]/div/div/div/div/div/div        ${value2}

ตรวจชื่อลูกค้า
    [Arguments]                             ${value}            ${value2}
    Wait Until Page Contains Element        //*[@id="table_width"]/div/div[1]/div[3]/div[${value}]/div/div/div[2]/div/div[8]/div/div/div/div/div/div        300
    Element Should Contain                  //*[@id="table_width"]/div/div[1]/div[3]/div[${value}]/div/div/div[2]/div/div[8]/div/div/div/div/div/div        ${value2}

เปลี่ยนสาขาเป็นกิ่งแก้ว
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="root-content"]/div/form/div/div[1]/h3/div[2]/div/div[1]                                                                        300
    Sleep                           3
    Click Element                        //*[@id="root-content"]/div/form/div/div[1]/h3/div[2]/div/div[1]
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="root-content"]/div/form/div/div[1]/h3/div[2]/div/div[2]/div[2]                                                          300
    Sleep                           3
    Click Element                        //*[@id="root-content"]/div/form/div/div[1]/h3/div[2]/div/div[2]/div[2]

เปลี่ยนสาขาเป็นลาดกระบัง
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="root-content"]/div/form/div/div[1]/h3/div[2]/div/div[1]                                                                          300
    Sleep                           3
    Click Element                        //*[@id="root-content"]/div/form/div/div[1]/h3/div[2]/div/div[1]
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="root-content"]/div/form/div/div[1]/h3/div[2]/div/div[2]/div[1]                                                          300
    Sleep                           3
    Click Element                        //*[@id="root-content"]/div/form/div/div[1]/h3/div[2]/div/div[2]/div[1]

กดปุ่มพิมพ์รายงาน
    Wait Until Page Contains Element    id:btnPrintReport                                                  300
    Sleep                               1
    Click Element                       id:btnPrintReport

กดปุ่มปิดPopupPreview
    Sleep                               3
    Click Element                       //*[@id="closePreview"]
    Sleep                           3
    Go To                               ${BASE_URL}

เลือกรายการบิล
    [Arguments]                         ${value}
    Wait Until Page Contains Element    //*[@id="table_width"]/div/div[1]/div[3]/div[${value}]/div/div/div[2]/div/div[1]/div/div/div/div/center/a/i                                                 300
    Sleep                               1
    Click Element                       //*[@id="table_width"]/div/div[1]/div[3]/div[${value}]/div/div/div[2]/div/div[1]/div/div/div/div/center/a/i

เลือกสาขาลาดกระบัง
    Wait Until Element Is Visible         id:dropDownBranch                                                                                                300
    Sleep                           3
    Click Element                         id:dropDownBranch
    Wait Until Element Is Visible         //*[@id="dropDownBranch"]/div[2]/div[2]                           300
    Sleep                               1                                                               
    Click Element                         //*[@id="dropDownBranch"]/div[2]/div[2]

เลือกสาขากิ่งแก้ว
    Wait Until Element Is Visible         id:dropDownBranch                                                                                                300
    Sleep                           3
    Click Element                         id:dropDownBranch
    Wait Until Element Is Visible         //*[@id="dropDownBranch"]/div[2]/div[3]                            300
    Sleep                               1                                                              
    Click Element                         //*[@id="dropDownBranch"]/div[2]/div[3]

กดปุ่มค้นหา
    Wait Until Page Contains Element      id:btnSearch                                                                                                      300
    Sleep                           3
    Click Element                         id:btnSearch

เพิ่มรายการขายทอง
    # เลือก %ทอง 96.5
    Wait Until Page Contains Element      //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[1]/select                      300
    Sleep                           3
    Click Element                         //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[1]/select
    Wait Until Page Contains Element      //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[1]/select/option[3]            300
    Sleep                           3
    Click Element                         //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[1]/select/option[3]
    # เลือก ประเภทสินค้า แหวน
    Wait Until Page Contains Element      //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[2]/select                      300
    Sleep                           3
    Click Element                         //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[2]/select 
    Sleep                           3
    Wait Until Page Contains Element      //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[2]/select/option[2]            300
    Sleep                           3
    Click Element                         //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[2]/select/option[2]
    # เลือก สินค้า แหวน9.2ก. R96G920
    Sleep                           3
    Wait Until Page Contains Element      //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select                      300
    Sleep                           3
    Click Element                         //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select
    Sleep                           3
    Wait Until Page Contains Element      //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select/option[2]            300
    Sleep                           3
    Click Element                         //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select/option[2]
