*** Settings ***
Library     Selenium2Library
Library     RequestsLibrary
Library     Collections
Resource    ${EXECDIR}/e2e/resource/variable.robot

*** Keywords ***

###เปิดbrowser###

เปิดbrowser
    Sleep                                10
    Open Browser                         ${BASE_URL}                                                                                               ${BROWSER}                                   remote_url=${REMOTE}
    Title Should Be                      AAGold POS
    init_data                            v1
เปิดbrowserและเข้าสู่ระบบ
    Sleep                                10
    Open Browser                         ${BASE_URL}                                                                                               ${BROWSER}                                   remote_url=${REMOTE}
    Sleep                                3
    Title Should Be                      AAGold POS
    init_data                            v1
    Wait Until Element Is Visible        id:userInput                                                                                              300
    Sleep                                30
    Input Text                           id:userInput                                                                                              ${USER}
    Sleep                                1
    Wait Until Page Contains Element     id:passwordInput                                                                                          300
    Sleep                                1
    Input Text                           id:passwordInput                                                                                          ${PASS_USER}
    Sleep                                1
    Wait Until Page Contains Element     id:btnLogin                                                                                               300
    Sleep                                0.5
    Click Element                        id:btnLogin

เปิดbrowserและเข้าสู่ระบบด้วยAdmin
    Sleep                                10
    Open Browser                         ${BASE_URL}                                                                                               ${BROWSER}                                   remote_url=${REMOTE}
    Sleep                                3
    Title Should Be                      AAGold POS
    init_data                            v1
    Wait Until Element Is Visible        id:userInput                                                                                              300
    Sleep                                3
    Input Text                           id:userInput                                                                                              ${ADMIN}
    Sleep                                1
    Wait Until Page Contains Element     id:passwordInput                                                                                          300
    Sleep                                1
    Input Text                           id:passwordInput                                                                                          ${PASS_USER}
    Sleep                                1
    Wait Until Page Contains Element     id:btnLogin                                                                                               300
    Sleep                                0.5
    Click Element                        id:btnLogin

เปิดbrowserและเข้าสู่ระบบและinit_data
    [Arguments]                          ${value}
    Sleep                                10
    Open Browser                         ${BASE_URL}                                                                                               ${BROWSER}                                   remote_url=${REMOTE}
    Title Should Be                      AAGold POS
    Sleep                                30
    init_data                            v1
    Wait Until Page Contains Element     id:userInput                                                                                              300
    Sleep                                1
    Input Text                           id:userInput                                                                                              ${USER}
    Sleep                                1
    Wait Until Page Contains Element     id:passwordInput                                                                                          300
    Sleep                                1
    Input Text                           id:passwordInput                                                                                          ${PASS_USER}
    Sleep                                1
    Wait Until Page Contains Element     id:btnLogin                                                                                               300
    Sleep                                0.5
    Click Element                        id:btnLogin
    Sleep                                1
    init_data                            ${value}

เปิดbrowserและเข้าสู่ระบบAdminและinit_data
    Sleep                                10
    Open Browser                         ${BASE_URL}                                                                                               ${BROWSER}                                   remote_url=${REMOTE}
    Title Should Be                      AAGold POS
    Maximize Browser Window
    init Admin
    Wait Until Page Contains Element     id:userInput                                                                                              300
    Sleep                                1
    Input Text                           id:userInput                                                                                              ${ADMIN}
    Sleep                                1
    Wait Until Page Contains Element     id:passwordInput                                                                                          300
    Sleep                                1
    Input Text                           id:passwordInput                                                                                          ${PASS_USER}
    Sleep                                1
    Wait Until Page Contains Element     id:btnLogin                                                                                               300
    Sleep                                0.5
    Click Element                        id:btnLogin
    Sleep                                1

เปิดbrowserและเข้าสู่ระบบAdminและinit_data 8 Min.
    Sleep                                480
    Open Browser                         ${BASE_URL}                                                                                               ${BROWSER}                                   remote_url=${REMOTE}
    Title Should Be                      AAGold POS
    Maximize Browser Window
    init Admin
    Wait Until Page Contains Element     id:userInput                                                                                              300
    Sleep                                1
    Input Text                           id:userInput                                                                                              ${ADMIN}
    Sleep                                1
    Wait Until Page Contains Element     id:passwordInput                                                                                          300
    Sleep                                1
    Input Text                           id:passwordInput                                                                                          ${PASS_USER}
    Sleep                                1
    Wait Until Page Contains Element     id:btnLogin                                                                                               300
    Sleep                                0.5
    Click Element                        id:btnLogin
    Sleep                                1

เปิดbrowserและเข้าสู่ระบบAdmin_IBและinit_data
    # [Arguments]                          ${value}
    Sleep                                10
    Open Browser                         ${BASE_URL}                                                                                               ${BROWSER}                                   remote_url=${REMOTE}
    Title Should Be                      AAGold POS
    # Sleep                                30
    # init_data                            v1
    Maximize Browser Window
    # init_data                            ${value}
    init Report
    Wait Until Page Contains Element     id:userInput                                                                                              300
    Sleep                                1
    Input Text                           id:userInput                                                                                              ${ADMIN_IB}
    Sleep                                1
    Wait Until Page Contains Element     id:passwordInput                                                                                          300
    Sleep                                1
    Input Text                           id:passwordInput                                                                                          ${PASS_USER}
    Sleep                                1
    Wait Until Page Contains Element     id:btnLogin                                                                                               300
    Sleep                                0.5
    Click Element                        id:btnLogin
    Sleep                                1


เปิดbrowserและเข้าสู่ระบบและinit_data Infocustomer
    Sleep                                10
    Open Browser                         ${BASE_URL}                                                                                               ${BROWSER}                                   remote_url=${REMOTE}
    Sleep                                3
    Maximize Browser Window
    Sleep                                1
    Title Should Be                      AAGold POS
    Sleep                                30
    init V1
    Sleep                                1
    init Information
    Sleep                                30
    Wait Until Element Is Visible        id:userInput                                                                                              1000
    Sleep                                1
    Input Text                           id:userInput                                                                                              ${USER}
    Sleep                                1
    Wait Until Page Contains Element     id:passwordInput                                                                                          300
    Sleep                                1
    Input Text                           id:passwordInput                                                                                          ${PASS_USER}
    Sleep                                1
    Wait Until Page Contains Element     id:btnLogin                                                                                               300
    Sleep                                0.5
    Click Element                        id:btnLogin
    Sleep                                1

เปิดbrowserและinit_data
    [Arguments]                          ${value}
    Sleep                                10
    Open Browser                         ${BASE_URL}                                                                                               ${BROWSER}                                   remote_url=${REMOTE}
    Title Should Be                      AAGold POS
    Sleep                                1
    init_data                            v1
    init_data                            ${value}

เปิดbrowserและเข้าสู่ระบบด้วยUserและinit_data สองอัน
    [Arguments]                          ${value}                                                                                                  ${value2}
    Sleep                                10
    Open Browser                         ${BASE_URL}                                                                                               ${BROWSER}                                   remote_url=${REMOTE}
    Title Should Be                      AAGold POS
    init_data                            ${value}
    init_data                            ${value2}
    Sleep                                30
    Wait Until Page Contains Element     id:userInput                                                                                              300
    Sleep                                1
    Input Text                           id:userInput                                                                                              ${USER}
    Sleep                                1
    Wait Until Page Contains Element     id:passwordInput                                                                                          300
    Sleep                                1
    Input Text                           id:passwordInput                                                                                          ${PASS_USER}
    Sleep                                1
    Wait Until Page Contains Element     id:btnLogin                                                                                               300
    Sleep                                1
    Click Element                        id:btnLogin

###init data###

init_data
    [Arguments]                          ${option}
    Create Session                       api                                                                                                       ${INIT_URL}${option}                         verify=True
    ${headers}=                          Create Dictionary                                                                                         Content-Type=application/json
    ${resp}=                             GET Request                                                                                               api                                          ${EMPTY}                headers=${headers}    timeout=1000
    # Should Be Equal As Strings           ${resp.status_code}                                                                                   200

###เข้าสู่ระบบ###

เข้าสู่ระบบด้วยUser
    Sleep                                30
    Wait Until Page Contains Element     id:userInput                                                                                              300
    Sleep                                1
    Input Text                           id:userInput                                                                                              ${USER}
    Sleep                                1
    Wait Until Page Contains Element     id:passwordInput                                                                                          300
    Sleep                                1
    Input Text                           id:passwordInput                                                                                          ${PASS_USER}
    Sleep                                1
    Wait Until Page Contains Element     id:btnLogin                                                                                               300
    Sleep                                0.5
    Click Element                        id:btnLogin

เข้าสู่ระบบด้วยAdmin
    # Sleep                                30
    Wait Until Page Contains Element     id:userInput                                                                                              300
    Sleep                                1
    Input Text                           id:userInput                                                                                              ${ADMIN}
    Sleep                                1
    Wait Until Page Contains Element     id:passwordInput                                                                                          300
    Sleep                                1
    Input Text                           id:passwordInput                                                                                          ${PASS_USER}
    Sleep                                1
    Wait Until Page Contains Element     id:btnLogin                                                                                               300
    Sleep                                0.5
    Click Element                        id:btnLogin

เข้าสู่ระบบด้วยUserและinit_data
    [Arguments]                          ${value}
    Sleep                                1
    init_data                            ${value}
    Sleep                                30
    Wait Until Page Contains Element     id:userInput                                                                                              300
    Sleep                                1
    Input Text                           id:userInput                                                                                              ${USER}
    Sleep                                1
    Wait Until Page Contains Element     id:passwordInput                                                                                          300
    Sleep                                1
    Input Text                           id:passwordInput                                                                                          ${PASS_USER}
    Sleep                                1
    Wait Until Page Contains Element     id:btnLogin                                                                                               300
    Sleep                                1
    Click Element                        id:btnLogin

###ออกจากระบบ###

ออกจากระบบ
    Maximize Browser Window
    Wait Until Page Contains Element     id:logout                                                                                                 300
    Click Element                        id:logout
    Wait Until Element Is Visible        //*[@id="confirmLogout"]/div[2]/button[1]                                                                 300
    Wait Until Element Is Visible        //*[@id="confirmLogout"]/div[2]/button[2]                                                                 300
    Double Click Element                 //*[@id="confirmLogout"]/div[2]/button[2]
    Wait Until Page Contains Element     //*[@id="root"]/div/div/div/div                                                                           300

ออกจากระบบและinit_dataสองอัน
    [Arguments]                          ${value1}                                                                                                 ${value2}
    Wait Until Page Contains Element     id:logout                                                                                                 300
    Click Element                        id:logout
    Wait Until Element Is Visible        //*[@id="confirmLogout"]/div[2]/button[1]                                                                 300
    Wait Until Element Is Visible        //*[@id="confirmLogout"]/div[2]/button[2]                                                                 300
    Double Click Element                 //*[@id="confirmLogout"]/div[2]/button[2]
    Wait Until Page Contains Element     //*[@id="root"]/div/div/div/div                                                                           300
    init_data                            ${value1}
    init_data                            ${value2}

###Close Browser####

ปิดbrowserและinit_data
    [Arguments]                          ${value}
    init_data                            ${value}
    Close Browser

ปิดbrowserและinit_data_admin
    Sleep                           3
    init Admin
    Sleep                           3
    Close Browser

ปิดbrowserและinit_data_report
    Sleep                           3
    init Report
    Sleep                           3
    Close Browser

ปิดbrowserและinit_data Infocustomer
    Sleep                           5
    init V1
    Sleep                           3
    init Information
    Sleep                           3
    Close Browser

ปิดbrowserและออกจากระบบและinit_dataสองอัน
    [Arguments]                          ${value1}                                                                                                 ${value2}
    Wait Until Page Contains Element     id:logout                                                                                                 300
    Click Element                        id:logout
    Wait Until Element Is Visible        //*[@id="confirmLogout"]/div[2]/button[1]                                                                 300
    Wait Until Element Is Visible        //*[@id="confirmLogout"]/div[2]/button[2]                                                                 300
    Double Click Element                 //*[@id="confirmLogout"]/div[2]/button[2]
    Wait Until Page Contains Element     //*[@id="root"]/div/div/div/div                                                                           300
    init_data                            ${value1}
    init_data                            ${value2}
    Close Browser

###เข้าหน้า###

เข้าสู่หน้าหลัก
    Sleep                                3
    Wait Until Page Contains Element     //*[@id="root"]/div/div/div/div/div[1]/a[1]                                                               300
    Sleep                                3
    Click Element                        //*[@id="root"]/div/div/div/div/div[1]/a[1]

เข้าสู่หน้าหลักและinit_data2อัน
    [Arguments]                          ${value1}                                                                                                 ${value2}
    Wait Until Page Contains Element     //*[@id="root"]/div/div/div/div/div[1]/a[1]                                                               300
    Click Element                        //*[@id="root"]/div/div/div/div/div[1]/a[1]
    init_data                            ${value1}
    init_data                            ${value2}

เข้าหน้าลูกค้า
    Wait Until Page Contains Element     id:btnCustomer                                                                                            300
    Sleep                                3
    Double Click Element                 id:btnCustomer

เข้าหน้าPos ซื้อ-ขายทอง
    Wait Until Page Contains Element     id:btnPOS                                                                                                 300
    Sleep                                3
    Double Click Element                 id:btnPOS

เข้าหน้าขายฝากหลัก
    Wait Until Page Contains Element     id:btnLease                                                                                               300
    Sleep                                3
    Double Click Element                 id:btnLease

เข้าหน้าขายทอง
    Wait Until Page Contains Element     id:sellGold                                                                                               300
    Sleep                                3
    Double Click Element                 id:sellGold

เข้าหน้าซื้อทอง
    Wait Until Page Contains Element     id:buyGold                                                                                                300
    Sleep                                3
    Double Click Element                 id:buyGold

เข้าหน้าเปลี่ยนทอง
    Wait Until Page Contains Element     id:changeGold                                                                                             300
    Sleep                                3
    Double Click Element                 id:changeGold

เข้าหน้ารายรับรายจ่าย
    Wait Until Page Contains Element     id:btnReportMoney                                                                                         300
    Sleep                                3
    Click Element                        id:btnReportMoney

เข้าหน้าแฟ้มข้อมูล
    Wait Until Page Contains Element     id:btnSetting                                                                                             300
    Sleep                                3
    Click Element                        id:btnSetting

เข้าหน้าสต็อกทอง
    Wait Until Page Contains Element     id:btnStock                                                                                               300
    Sleep                                3
    Click Element                        id:btnStock

เข้าหน้ารายงาน
    Wait Until Page Contains Element     id:btnReport                                                                                              300
    Sleep                                3
    Click Element                        id:btnReport
############

พบข้อความ
    [Arguments]                          ${value}
    Sleep                                1
    Wait Until Page Contains             ${value}                                                                                                  300

ไม่พบข้อความ
    [Arguments]                          ${value}
    Sleep                                1
    Wait Until Element Is Not Visible    ${value}

#####add&edit######
กรอกชื่อ
    [Arguments]                          ${name}
    Sleep                                1
    Input Text                           //*[@id="AddCustomerModal"]/div[2]/div/form/div[4]/div/div[2]/div/input                                   ${name}

กรอกเบอร์มือถือ
    [Arguments]                          ${phone}
    Sleep                                1
    Input Text                           //*[@id="AddCustomerModal"]/div[2]/div/form/div[4]/div/div[3]/div/input                                   ${phone}

กดปุ่มบันทึก
    Sleep                                2
    Wait Until Element Is Visible        //*[@id="AddConfirm"]                                                                                     300
    Sleep                                10
    Click Element                        //*[@id="AddConfirm"]

กดปุ่มยกเลิก
    Wait Until Element Is Visible        id:AddCancel                                                                                              300
    Click Element                        id:AddCancel
    Wait Until Element Is Not Visible    id:AddCustomerModal                                                                                       300

ตรวจพบข้อความบน Alert
    [Arguments]                          ${text}
    Sleep                                5
    ${message}                           Handle Alert
    Should Be Equal                      ${message}                                                                                                ${text}

####add&setting######
ปิดPopupเพิ่ม
    Sleep                                1
    Wait Until Element Is Visible        id:CloseAddCustomer                                                                                       300
    Sleep                                1
    Click Element                        id:CloseAddCustomer
    Sleep                                1
    Wait Until Element Is Not Visible    id:AddCustomerModal                                                                                       300

#####POS######

พบPopup POS
    Sleep                                1
    Wait Until Element Is Visible        id:headerModalPOS                                                                                         300
    Wait Until Element Is Visible        id:contentModalPOS                                                                                        300
    Wait Until Element Is Visible        id:btnClose                                                                                               300

ปิดpopupPOS
    Sleep                                1
    Wait Until Element Is Visible        id:btnClose                                                                                               300
    Sleep                                1
    Double Click Element                 id:btnClose
    Sleep                                1
    Wait Until Element Is Not Visible    id:headerModalPOS                                                                                         300
    Wait Until Element Is Not Visible    id:contentModalPOS                                                                                        300

####search & POS####
พบPopup เลือกสินค้าเพื่อขายทอง
    Wait Until Element Is Visible        id:headerModalSelectProduct                                                                               300
    Wait Until Element Is Visible        id:contentModalSelectProduct                                                                              300
    Wait Until Element Is Visible        id:btnCloseProductSelect                                                                                  300
    Wait Until Element Is Visible        id:confirmProduct                                                                                         300

กดเพิ่มรายการสินค้า
    Sleep                                3
    Wait Until Element Is Visible        id:btnAddProduct                                                                                          300
    Sleep                                3
    Click Element                        id:btnAddProduct

เลือกชื่อสินค้า
    Wait Until Element Is Visible        //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select                          300
    Sleep                                3
    Click Element                        //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select
    Wait Until Element Is Visible        //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select/option[67]               300
    Sleep                                3
    Click Element                        //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select/option[67]
    Sleep                                1

กดปุ่มลงรายการ
    Wait Until Element Is Visible        id:confirmProduct                                                                                         300
    Sleep                                1
    Click Element                        id:confirmProduct

กดปุ่มบันทึกและชำระเงิน
    Wait Until Element Is Visible        //*[@id="btnSaveandCheck"]                                                                                300
    Sleep                                7
    Click Element                        //*[@id="btnSaveandCheck"]

เลือกเปอร์เซ็นต์ทอง90เปอร์เซ็นต์
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="percengold"]/select                                                                              300
    Sleep                                1
    Click Element                        //*[@id="percengold"]/select
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="percengold"]/select/option[2]                                                                    300
    Sleep                                1
    Click Element                        //*[@id="percengold"]/select/option[2]

กรอกชื่อสินค้า
    [Arguments]                          ${value}
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="ProductName"]/div/input                                                                          300
    Sleep                                1
    Input Text                           //*[@id="ProductName"]/div/input                                                                          ${value}

กรอกนน.(ก)
    [Arguments]                          ${value}
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="weight-g"]/div/input                                                                             300
    Sleep                                1
    Input Text                           //*[@id="weight-g"]/div/input                                                                             ${value}

กดเพิ่มรายการเปลี่ยนทอง
    Wait Until Element Is Visible        //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select                          300
    Click Element                        //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select/option[97]               300
    Click Element                        //*[@id="contentModalSelectProduct"]/div[2]/div/div[1]/form/div[1]/div[3]/select/option[97]
    เลือกเปอร์เซ็นต์ทอง90เปอร์เซ็นต์
    กรอกนน.(ก)                           1

เลือกพนักงานคนที่
    [Arguments]                          ${value}
    Wait Until Element Is Visible        //*[@id="contentModalPOS"]/div[2]/div/div/div[1]/div/form/div[3]/div/div                                  300
    Sleep                                0.5
    Click Element                        //*[@id="contentModalPOS"]/div[2]/div/div/div[1]/div/form/div[3]/div/div
    Wait Until Element Is Visible        //*[@id="contentModalPOS"]/div[2]/div/div/div[1]/div/form/div[3]/div/div/div[2]/div[${value}]             300
    Sleep                                0.5
    Click Element                        //*[@id="contentModalPOS"]/div[2]/div/div/div[1]/div/form/div[3]/div/div/div[2]/div[${value}]

กดปุ่มVat
    Wait Until Element Is Visible        id:btnPrintVat                                                                                            300
    Sleep                                1
    Click Element                        id:btnPrintVat

กดปุ่มปิดPopup Preview
    Wait Until Element Is Visible        id:btnClosePreview                                                                                        300
    Sleep                                1
    Click Element                        id:btnClosePreview
    Sleep                                1
    Wait Until Element Is Not Visible    id:modalPreview                                                                                           300

####Payment####
พบPopup ชำระเงิน
    Wait Until Element Is Visible        id:headerModalPayment                                                                                     300
    Wait Until Element Is Visible        id:descriptModalPayment                                                                                   300
    Wait Until Element Is Visible        id:closePaymentModal                                                                                      300
    Wait Until Element Is Visible        id:inputCash                                                                                              300
    Wait Until Element Is Visible        id:btnSave                                                                                                300

ปิดpopup ชำระเงิน
    Wait Until Element Is Visible        id:closePaymentModal                                                                                      300
    Double Click Element                 id:closePaymentModal
    Sleep                                1
    Wait Until Element Is Not Visible    id:headerModalPayment                                                                                     300
    Wait Until Element Is Not Visible    id:descriptModalPayment                                                                                   300

กรอกจำนวนรับเงินสด
    [Arguments]                          ${value}
    Wait Until Element Is Visible        id:inputCash
    Sleep                                1
    Input Text                           id:inputCash                                                                                              ${value}

กดปุ่มบันทึกชำระเงิน
    Wait Until Element Is Visible        id:btnSave                                                                                                300
    Sleep                                5
    Click Element                        id:btnSave

########navi#########
กดปุ่มรายการขายฝาก
    Wait Until Page Contains Element     id:btnLeasePOS                                                                                            300
    Sleep                                1
    Double Click Element                 id:btnLeasePOS

กดปุ่มต่อดอก
    Wait Until Page Contains Element     id:btnLeaseInterest                                                                                       300
    Sleep                                1
    Double Click Element                 id:btnLeaseInterest

กดปุ่มไถ่คืน
    Wait Until Page Contains Element     id:btnLeaseRedeem                                                                                         300
    Sleep                                1
    Double Click Element                 id:btnLeaseRedeem

กดปุ่มเพิ่มลดเงินต้น
    Wait Until Page Contains Element     id:btnLeaseDelete                                                                                         300
    Sleep                                1
    Double Click Element                 id:btnLeaseDelete

กรอกเลขที่บิลเพื่อค้นหา
    [Arguments]                          ${value}
    Wait Until Element Is Visible        id:inputBillNum                                                                                           300
    Sleep                                1
    Input Text                           id:inputBillNum                                                                                           ${value}

กดปุ่มค้นหาบิล
    Wait Until Page Contains Element     id:btnSearch                                                                                              300
    Sleep                                5
    Click Element                        id:btnSearch

######Interest#####

ปิดpopupต่อดอกไถ่คืน
    Wait Until Page Contains Element     id:btnCloseInterest                                                                                       300
    Sleep                                1
    Double Click Element                 id:btnCloseInterest

พบ popup ต่อดอก
    Sleep                                1
    พบข้อความ                            ต่อดอก
    Wait Until Element Is Visible        id:modalInterest                                                                                          300
    Wait Until Element Is Visible        id:btnCloseInterest                                                                                       300
    Wait Until Element Is Visible        id:selectMonth                                                                                            300
    Wait Until Element Is Visible        id:radio1                                                                                                 300
    Wait Until Element Is Visible        id:inputInterestTotal                                                                                     300
    Wait Until Element Is Visible        id:btnCal                                                                                                 300
    Wait Until Element Is Visible        id:btnSaveInterest                                                                                        300

พบ popup ไถ่คืน
    Sleep                                1
    พบข้อความ                            ไถ่คืน
    Sleep                                1
    Wait Until Element Is Visible        id:modalInterest                                                                                          300
    Wait Until Element Is Visible        id:btnCloseInterest                                                                                       300
    Wait Until Element Is Visible        id:selectMonth                                                                                            300
    Wait Until Element Is Visible        id:radio1                                                                                                 300
    Wait Until Element Is Visible        id:InterestTotal                                                                                          300
    Wait Until Element Is Visible        id:btnCal                                                                                                 300
    Wait Until Element Is Visible        id:btnSaveInterest                                                                                        300

เลือกจำนวนเดือน
    Wait Until Element Is Visible        id:selectMonth                                                                                            300
    Sleep                                5
    Click Element                        id:selectMonth
    Sleep                                5
    Wait Until Element Is Visible        //*[@id="selectMonth"]/div[2]/div[1]/span                                                                 300
    Sleep                                5
    Click Element                        //*[@id="selectMonth"]/div[2]/div[1]/span

กดปุ่มไถ่คืนในหน้าPopupต่อดอกไถ่คืน
    Wait Until Element Is Visible        id:redeem                                                                                                 300
    Sleep                                5
    Click Element                        id:redeem

กดปุ่มบันทึกไถ่คืน/ต่อดอก
    Sleep                                1
    Wait Until Element Is Visible        id:btnSaveInterest                                                                                        300
    Sleep                                1
    Click Element                        id:btnSaveInterest

#####Principle######
ปิดpopupเพิ่มลดเงินต้น
    Wait Until Page Contains Element     id:btnCloseInDe                                                                                           300
    Sleep                                1
    Click Element                        id:btnCloseInDe

พบpopupเพิ่มลดเงินต้น
    พบข้อความ                            เพิ่ม
    Wait Until Element Is Visible        id:btnIncease                                                                                             300
    Wait Until Element Is Visible        id:btnDecease                                                                                             300

กดปุ่มแก้ไข
    Sleep                                7
    Wait Until Page Contains Element     //*[@id="client_width"]/div[2]/div[1]/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a    300
    Sleep                                1
    Click Element                        //*[@id="client_width"]/div[2]/div[1]/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div/div/div/center/a

กดแถบเพิ่ม/ลด
    Sleep                                3
    Wait Until Element Is Visible        id:tab3                                                                                                   300
    Sleep                                1
    Click Element                        id:tab3

กดปุ่มเพิ่ม
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="content-body2"]/form/div/div/button                                                              300
    Sleep                                1
    Click Element                        //*[@id="content-body2"]/form/div/div/button

เลือกพนักงาน
    Wait Until Element Is Visible        //*[@id="inputStaff"]/input                                                                               300
    Sleep                                1
    Click Element                        //*[@id="inputStaff"]/input
    Wait Until Element Is Visible        //*[@id="inputStaff"]/div[2]/div[1]/span                                                                  300
    Sleep                                1
    Click Element                        //*[@id="inputStaff"]/div[2]/div[1]/span

กรอกจำนวนเงินเพิ่ม/ลด
    Wait Until Element Is Visible        id:inputLedgerTotal                                                                                       300
    Sleep                                1
    Input Text                           id:inputLedgerTotal                                                                                       5000

กดบันทึกเพิ่ม/ลด
    Wait Until Element Is Visible        id:btnSaveInDe                                                                                            300
    Sleep                                5
    Click Element                        id:btnSaveInDe

#####PopupLease######
ปิดPopupขายฝาก
    Sleep                                1
    Wait Until Page Contains Element     id:btnClose                                                                                               300
    Sleep                                1
    Click Element                        id:btnClose

พบPopupขายฝาก
    Sleep                                8
    Wait Until Element Is Visible        id:ModalLeaseForm                                                                                         300
    Wait Until Element Is Visible        id:btnClose                                                                                               300
    Wait Until Element Is Visible        id:tab2                                                                                                   300
    Wait Until Element Is Visible        id:tab3                                                                                                   300
    Wait Until Element Is Visible        id:searchNameCus2                                                                                         300
    Wait Until Element Is Visible        id:btnRefesh                                                                                              300
    Wait Until Element Is Visible        id:printContact                                                                                           300
    Wait Until Element Is Visible        id:btnSaveLease                                                                                           300

กดปุ่มเพิ่มรายการดอกเบี้ยรับ
    Wait Until Element Is Visible        id:btnAdd                                                                                                 300
    Sleep                                3
    Click Element                        id:btnAdd

#####Lease#######

กดปุ่มสร้างลูกค้า
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="ModalLeaseForm"]/div[2]/div/div[2]/form/div/div/div[1]/div[1]/div/button/i                       300
    Sleep                                1
    Click Element                        //*[@id="ModalLeaseForm"]/div[2]/div/div[2]/form/div/div/div[1]/div[1]/div/button/i

กรอกเลขบัตรประชาชน
    [Arguments]                          ${value}
    Wait Until Element Is Visible        //*[@id="inputID"]                                                                                        300
    Sleep                                1
    Click Element                        //*[@id="inputID"]
    Input Text                           //*[@id="inputID"]                                                                                        ${value}

กรอกเบอร์โทรศัพท์
    [Arguments]                          ${value}
    Wait Until Element Is Visible        //*[@id="inputPhone"]                                                                                     300
    Sleep                                1
    Input Text                           //*[@id="inputPhone"]                                                                                     ${value}

กดปุ่มสร้างใหม่
    Wait Until Element Is Visible        id:btnRefesh                                                                                              300
    Sleep                                1
    Click Element                        id:btnRefesh

ตรวจสอบไม่สามารถกดปุ่มพิมพ์สัญญาได้
    Wait Until Element Is Visible        //*[@id="printContact"]                                                                                   300
    Sleep                                1
    ${value} =                           Get Element Attribute                                                                                     //*[@id="printContact"]                      disabled
    Should Be Equal As Strings           ${value}                                                                                                  true

ปิดPopupเลือกรายการสินค้า
    Wait Until Element Is Visible        id:btnCloseAddProduct                                                                                     300
    Sleep                                1
    Click Element                        id:btnCloseAddProduct



ใส่จำนวนinvalid
    Wait Until Element Is Visible        //*[@id="inputNumber"]                                                                                    300
    Sleep                                1
    Input Text                           //*[@id="inputNumber"]                                                                                    ??

จำนวนรายการสินค้า
    [Arguments]                          ${value}
    ${count} =                           Get Element Count                                                                                         id:ID
    ${count_str} =                       Convert To String                                                                                         ${count}
    Should Be Equal                      ${count_str}                                                                                              ${value}

ปิดPopupจ่ายเงิน
    Wait Until Element Is Visible        id:btnClosePayLease                                                                                       300
    Sleep                                1
    Click Element                        id:btnClosePayLease

ตรวจสอบไม่สามารถแก้ไขจำนวนเงินได้
    Wait Until Element Is Visible        //*[@id="inputAmountMoney"]                                                                               300
    Sleep                                1
    ${value} =                           Get Element Attribute                                                                                     //*[@id="inputAmountMoney"]                  readonly
    Should Be Equal As Strings           ${value}                                                                                                  true

ตรวจสอบเลขที่บิลขายฝาก
    [Arguments]                          ${input}
    Wait Until Element Is Visible        //*[@id="paperA4-portrait"]/div[1]/b                                                                      300
    Sleep                                1
    ${ID} =                              Get Text                                                                                                  //*[@id="paperA4-portrait"]/div[1]/b
    Should Be Equal                      ${ID}                                                                                                     ${input}

ตรวจสอบชื่อลูกค้า
    [Arguments]                          ${input}
    Wait Until Element Is Visible        //*[@id="paperA4-portrait"]/div[3]/div[1]                                                                 300
    Sleep                                1
    ${name} =                            Get Text                                                                                                  //*[@id="paperA4-portrait"]/div[3]/div[1]
    Should Be Equal                      ${name}                                                                                                   ${input}

ตรวจสอบจำนวนเงินที่จ่าย
    [Arguments]                          ${input}
    Wait Until Element Is Visible        //*[@id="paperA4-portrait"]/div[6]/div[1]                                                                 300
    Sleep                                1
    ${money} =                           Get Text                                                                                                  //*[@id="paperA4-portrait"]/div[6]/div[1]
    Should Be Equal                      ${money}                                                                                                  ${input}

กรอกเลขที่ใบขายฝาก
    [Arguments]                          ${value}
    Wait Until Page Contains Element     id:inputLeaseNum                                                                                          300
    Sleep                                1
    Input Text                           id:inputLeaseNum                                                                                          ${value}

กดปุ่มค้นหารายการขายฝาก
    Wait Until Page Contains Element     id:btnSearch                                                                                              300
    Sleep                                1
    Double Click Element                 id:btnSearch

กดปุ่มสร้างรายการขายฝาก
    Sleep                                5
    Wait Until Page Contains Element     id:btnCreateLease                                                                                         300
    Sleep                                5
    Click Element                        id:btnCreateLease

ปิดpopupขาย/ฝาก
    Sleep                                1
    Wait Until Element Is Visible        id:btnClose                                                                                               300
    Sleep                                1
    Click Element                        id:btnClose

####creat & search#####
กรอกชื่อลูกค้า
    [Arguments]                          ${name}
    Wait Until Element Is Visible        //*[@id="searchNameCus2"]/input                                                                           300
    Sleep                                2
    Input Text                           //*[@id="searchNameCus2"]/input                                                                           ${name}
    Sleep                                5

กรอกจำนวนเงิน
    [Arguments]                          ${value}
    Wait Until Element Is Visible        id:inputAmount                                                                                            300
    Sleep                                2
    Click Element                        id:inputAmount
    Sleep                                5
    Input Text                           id:inputAmount                                                                                            ${value}

กดปุ่มสร้าง
    Wait Until Element Is Visible        id:btnSaveLease                                                                                           300
    Sleep                                4
    Click Element                        id:btnSaveLease

กดปุ่มเพิ่มรายการสินค้า
    Wait Until Element Is Visible        //*[@id="content-body"]/form/div/div[2]/button                                                            300
    Sleep                                1
    Click Element                        //*[@id="content-body"]/form/div/div[2]/button

พบPopupเลือกรายการสินค้า
    Wait Until Element Is Visible        id:ModalAddProduct                                                                                        300
    Wait Until Element Is Visible        id:btnCloseAddProduct                                                                                     300
    Wait Until Element Is Visible        id:headerModalAddProduct                                                                                  300
    Wait Until Element Is Visible        id:contentModalAddProduct                                                                                 300
    Wait Until Element Is Visible        //*[@id="contentModalAddProduct"]/form/div[1]/select                                                      300
    Wait Until Element Is Visible        id:inputNameProduct                                                                                       300
    Wait Until Element Is Visible        id:inputWeight                                                                                            300
    Wait Until Element Is Visible        id:btnAdd                                                                                                 300

เลือก % ทอง
    Wait Until Element Is Visible        //*[@id="contentModalAddProduct"]/form/div[1]/select                                                      300
    Sleep                                1
    Click Element                        //*[@id="contentModalAddProduct"]/form/div[1]/select
    Wait Until Element Is Visible        //*[@id="contentModalAddProduct"]/form/div[1]/select/option[4]                                            300
    Sleep                                1
    Click Element                        //*[@id="contentModalAddProduct"]/form/div[1]/select/option[4]
    Sleep                                1

ใส่ชื่อสินค้า
    Wait Until Element Is Visible        //*[@id="inputNameProduct"]                                                                               300
    Sleep                                1
    Input Text                           //*[@id="inputNameProduct"]                                                                               ทองแท่ง 2 บาท

ใส่น้ำหนัก
    Wait Until Element Is Visible        //*[@id="inputWeight"]                                                                                    300
    Sleep                                1
    Input Text                           //*[@id="inputWeight"]                                                                                    2

ใส่จำนวน
    Wait Until Element Is Visible        //*[@id="inputNumber"]                                                                                    300
    Sleep                                1
    Input Text                           //*[@id="inputNumber"]                                                                                    1

กดปุ่มเพิ่มสินค้า
    Wait Until Element Is Visible        id:btnAdd                                                                                                 300
    Sleep                                1
    Click Element                        id:btnAdd

พบPopupจ่ายเงิน
    Wait Until Element Is Visible        id:modalPayLease                                                                                          300
    Wait Until Element Is Visible        id:btnClosePayLease                                                                                       300
    Wait Until Element Is Visible        id:dropDownEmp                                                                                            300
    Wait Until Element Is Visible        id:inputAmountMoney                                                                                       300
    Wait Until Element Is Visible        id:btnSavePayLease                                                                                        300

เลือกพนักงานเพื่อเพิ่ม
    Wait Until Element Is Visible        //*[@id="dropDownEmp"]/i                                                                                  300
    Click Element                        //*[@id="dropDownEmp"]/i
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="dropDownEmp"]/div[2]/div[1]                                                                      300
    Click Element                        //*[@id="dropDownEmp"]/div[2]/div[1]
    Sleep                                1

กดปุ่มบันทึกการจ่ายเงิน
    Wait Until Element Is Visible        id:btnSavePayLease                                                                                        300
    Sleep                                1
    Click Element                        id:btnSavePayLease

กดปุ่มพิมพ์สัญญา
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="printContact"]                                                                                   300
    Sleep                                1
    Click Element                        //*[@id="printContact"]

พบPopup Preview Lease
    Sleep                                1
    Wait Until Element Is Visible        id:ModalLeasePreview                                                                                      300
    Wait Until Element Is Visible        id:printLease                                                                                             300
    Wait Until Element Is Visible        id:btnCloseLeasePreview                                                                                   300

กดปุ่มปิดPopup Preview Lease
    Sleep                                1
    Wait Until Element Is Visible        id:btnCloseLeasePreview                                                                                   300
    Sleep                                1
    Click Element                        id:btnCloseLeasePreview
    Sleep                                1
    Wait Until Element Is Not Visible    id:ModalPreview                                                                                           300


#####Ledger######
กดปุ่มเพิ่มรายการรายรับรายจ่าย
    Wait Until Element Is Visible        //*[@id="addIn-Out"]                                                                                      300
    Sleep                                1
    Click Element                        //*[@id="addIn-Out"]

เลือกรายการรายรับรายจ่าย
    sleep                                1
    Wait Until Element Is Visible        id:menuIn-Out                                                                                             300
    Click Element                        id:menuIn-Out
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="menuIn-Out"]/div[2]/div/span                                                                     300
    Click Element                        //*[@id="menuIn-Out"]/div[2]/div/span

กดปุ่มค้นหารายรับรายจ่าย
    Wait Until Page Contains Element     id:btnSearch                                                                                              300
    Click Element                        id:btnSearch

เลือกประเภทการชำระรายรับรายจ่าย
    [Arguments]                          ${val}
    Sleep                                1
    Wait Until Element Is Visible        id:typepayment                                                                                            300
    Click Element                        id:typepayment
    Wait Until Element Is Visible        //*[@id="typepayment"]/div[2]/div[${val}]/span                                                            300
    Click Element                        //*[@id="typepayment"]/div[2]/div[${val}]/span

เลือกพนักงานหน้ารายรับรายจ่าย
    Sleep                                1
    Wait Until Element Is Visible        id:staff                                                                                                  300
    Click Element                        id:staff
    Wait Until Element Is Visible        //*[@id="staff"]/div[2]/div[1]/span                                                                       300
    Click Element                        //*[@id="staff"]/div[2]/div[1]/span

กรอกชำระเงินสด
    [Arguments]                          ${val}
    Sleep                                1
    Input Text                           id:inputcashIn-Out                                                                                        ${val}

กรอกหมายเหตุ
    [Arguments]                          ${val}
    Sleep                                1
    Input Text                           id:ex                                                                                                     ${val}

กดปุ่มบันทึกรายรับรายจ่าย
    Sleep                                1
    Wait Until Element Is Visible        id:confirmIn-Out                                                                                          300
    Sleep                                1
    Click Element                        id:confirmIn-Out

กดปิดpopupรายรับรายจ่าย
    Sleep                                1
    Wait Until Element Is Visible        id:closeIn-Out                                                                                            300
    Click Element                        id:closeIn-Out

กรอกรหัสบัตรเครดิต
    [Arguments]                          ${val}
    Sleep                                1
    Input Text                           id:IDcredit                                                                                               ${val}

เลือกชนิดบัตรเครดิต
    Sleep                                1
    Wait Until Element Is Visible        id:Typecredit                                                                                             300
    Click Element                        id:Typecredit
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="Typecredit"]/div[2]/div[1]/span                                                                  300
    Click Element                        //*[@id="Typecredit"]/div[2]/div[1]/span

กรอก%หักค่าธรรมเนียมบัตรเครดิต
    [Arguments]                          ${val}
    Sleep                                1
    Input Text                           id:percenVatcredit                                                                                        ${val}

กรอกค่าธรรมเนียมบัตรเครดิต
    [Arguments]                          ${val}
    Sleep                                1
    Input Text                           id:Vatcredit                                                                                              ${val}

กรอกจำนวนเงินชำระ(บัตร)
    [Arguments]                          ${val}
    Sleep                                5
    Input Text                           id:inputcreditIn-Out                                                                                      ${val}

พบจำนวนรายการรายรับรายจ่าย
    [Arguments]                          ${value}
    Sleep                                5
    ${count} =                           Get Value                                                                                                 id:amounttrans
    Should Be Equal                      ${count}                                                                                                  ${value}
กดปุ่มบันทึกบิล
    Sleep                                1
    Wait Until Element Is Visible        id:btnSaveandCheck                                                                                        300
    Sleep                                1
    Click Element                        id:btnSaveandCheck

ตรวจสอบรายรับ
    [Arguments]                          ${val}
    ${IN_temp}                           Get Value                                                                                                 id:amountIncomes
    ${IN_temp}=                          Remove String                                                                                             ${IN_temp}                                   ,                       .00
    ${IN}                                Convert To Number                                                                                         ${IN_temp}
    Sleep                                1
    Should Be Equal As Numbers           ${IN}                                                                                                     ${val}
กรอกเลขที่ใบสำคัญ
    [Arguments]                          ${value}
    Wait Until Page Contains Element     //*[@id="inputSearchLedger"]/input                                                                        300
    Sleep                                1
    Input Text                           //*[@id="inputSearchLedger"]/input                                                                        ${value}
    Sleep                                5
    Wait Until Element Is Visible        //*[@id="inputSearchLedger"]/div[2]/div                                                                   300
    Sleep                                1
    Click Element                        //*[@id="inputSearchLedger"]/div[2]/div

ตรวจสอบรายจ่าย
    [Arguments]                          ${val}
    ${OUT_temp}                          Get Value                                                                                                 id:amountOutcomes
    ${OUT_temp}=                         Remove String                                                                                             ${OUT_temp}                                  ,                       .00
    ${OUT}                               Convert To Number                                                                                         ${OUT_temp}
    Sleep                                1
    Should Be Equal As Numbers           ${OUT}                                                                                                    ${val}

ตรวจสอบยอดคงเหลือ
    [Arguments]                          ${val}
    Sleep                                1
    ${TOTAL_temp}                        Get Value                                                                                                 id:amountTotals
    ${TOTAL_temp}=                       Remove String                                                                                             ${TOTAL_temp}                                ,                       .00
    ${TOTAL}                             Convert To Number                                                                                         ${TOTAL_temp}
    Sleep                                1
    Should Be Equal As Numbers           ${TOTAL}                                                                                                  ${val}

กรอกชื่อลูกค้าเพื่อหารายรับรายจ่าย
    [Arguments]                          ${value}
    Wait Until Page Contains Element     //*[@id="searchNameCus"]/input                                                                            300
    Sleep                                1
    Input Text                           //*[@id="searchNameCus"]/input                                                                            ${value}

กรอกเลขที่อ้างอิง
    [Arguments]                          ${value}
    Wait Until Page Contains Element     //*[@id="root-content"]/div/div/form[1]/div/div/div[2]/div[2]/div/input                                   300
    Sleep                                1
    Input Text                           //*[@id="root-content"]/div/div/form[1]/div/div/div[2]/div[2]/div/input                                   ${value}
    Sleep                                5
    Wait Until Element Is Visible        //*[@id="root-content"]/div/div/form[1]/div/div/div[2]/div[2]/div/div[2]/div                              300
    Sleep                                1
    Click Element                        //*[@id="root-content"]/div/div/form[1]/div/div/div[2]/div[2]/div/div[2]/div

เลือกประเภททั้งหมด
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="inputSearchKind"]                                                                                300
    Click Element                        //*[@id="inputSearchKind"]
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="inputSearchKind"]/div[2]/div[1]                                                                  300
    Click Element                        //*[@id="inputSearchKind"]/div[2]/div[1]

เลือกประเภทรายรับ
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="inputSearchKind"]                                                                                300
    Click Element                        //*[@id="inputSearchKind"]
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="inputSearchKind"]/div[2]/div[2]                                                                  300
    Click Element                        //*[@id="inputSearchKind"]/div[2]/div[2]

เลือกประเภทรายจ่าย
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="inputSearchKind"]                                                                                300
    Click Element                        //*[@id="inputSearchKind"]
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="inputSearchKind"]/div[2]/div[3]                                                                  300
    Click Element                        //*[@id="inputSearchKind"]/div[2]/div[3]

เลือกรายการเงินสด ณ ต้นวัน
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="inputSearchLedgerCategory"]                                                                      300
    Click Element                        //*[@id="inputSearchLedgerCategory"]
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="inputSearchLedgerCategory"]/div[2]/div[8]                                                        300
    Click Element                        //*[@id="inputSearchLedgerCategory"]/div[2]/div[8]

เลือกรายการขายฝาก
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="inputSearchLedgerCategory"]                                                                      300
    Click Element                        //*[@id="inputSearchLedgerCategory"]
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="inputSearchLedgerCategory"]/div[2]/div[3]                                                        300
    Click Element                        //*[@id="inputSearchLedgerCategory"]/div[2]/div[3]

เลือกรายการทั้งหมดเมื่อเลือกประเภททั้งหมด
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="inputSearchLedgerCategory"]                                                                      300
    Click Element                        //*[@id="inputSearchLedgerCategory"]
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="inputSearchLedgerCategory"]/div[2]/div                                                           300
    Click Element                        //*[@id="inputSearchLedgerCategory"]/div[2]/div

เลือกรายการทั้งหมดเมื่อเลือกประเภทรายรับ/รายจ่าย
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="inputSearchLedgerCategory"]                                                                      300
    Click Element                        //*[@id="inputSearchLedgerCategory"]
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="inputSearchLedgerCategory"]/div[2]/div[1]                                                        300
    Click Element                        //*[@id="inputSearchLedgerCategory"]/div[2]/div[1]

เลือกรายการซื้อ
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="inputSearchLedgerCategory"]                                                                      300
    Click Element                        //*[@id="inputSearchLedgerCategory"]
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="inputSearchLedgerCategory"]/div[2]/div[2]                                                        300
    Click Element                        //*[@id="inputSearchLedgerCategory"]/div[2]/div[2]

เลือกรายการขาย
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="inputSearchLedgerCategory"]                                                                      300
    Click Element                        //*[@id="inputSearchLedgerCategory"]
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="inputSearchLedgerCategory"]/div[2]/div[2]                                                        300
    Click Element                        //*[@id="inputSearchLedgerCategory"]/div[2]/div[2]

เลือกรายการเปลี่ยน
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="inputSearchLedgerCategory"]                                                                      300
    Click Element                        //*[@id="inputSearchLedgerCategory"]
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="inputSearchLedgerCategory"]/div[2]/div[3]                                                        300
    Click Element                        //*[@id="inputSearchLedgerCategory"]/div[2]/div[3]

เลือกรายการลดเงินต้น
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="inputSearchLedgerCategory"]                                                                      300
    Click Element                        //*[@id="inputSearchLedgerCategory"]
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="inputSearchLedgerCategory"]/div[2]/div[6]                                                        300
    Click Element                        //*[@id="inputSearchLedgerCategory"]/div[2]/div[6]

เลือกรายการเพิ่มเงินต้น
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="inputSearchLedgerCategory"]                                                                      300
    Click Element                        //*[@id="inputSearchLedgerCategory"]
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="inputSearchLedgerCategory"]/div[2]/div[4]                                                        300
    Click Element                        //*[@id="inputSearchLedgerCategory"]/div[2]/div[4]

เลือกรายการค่าดอกเบี้ย
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="inputSearchLedgerCategory"]                                                                      300
    Click Element                        //*[@id="inputSearchLedgerCategory"]
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="inputSearchLedgerCategory"]/div[2]/div[5]                                                        300
    Click Element                        //*[@id="inputSearchLedgerCategory"]/div[2]/div[5]

เลือกรายการเงินต้น+ดอกเบี้ยไถ่คืน
    Sleep                                1
    Wait Until Page Contains Element     //*[@id="inputSearchLedgerCategory"]                                                                      300
    Click Element                        //*[@id="inputSearchLedgerCategory"]
    Sleep                                1
    Wait Until Element Is Visible        //*[@id="inputSearchLedgerCategory"]/div[2]/div[7]                                                        300
    Click Element                        //*[@id="inputSearchLedgerCategory"]/div[2]/div[7]

######Alert#######
ตรวจพบข้อความบน Alertแล้วCancel
    [Arguments]                          ${text}
    Sleep                                5
    ${message}                           Handle Alert                                                                                              action=DISMISS
    Should Be Equal                      ${message}                                                                                                ${text}

####ตรวจบนหน้าสต็อกทองใหม่####
เข้าหน้าสต็อกทองใหม่
    Wait Until Page Contains Element     id:newStock                                                                                               300
    Click Element                        id:newStock
กดเข้าดูประวัติสาขาลาดกระบัง
    Wait Until Page Contains Element     //*[@id="rowBranch"]/div/div/div                                                                          300
    Click Element                        //*[@id="rowBranch"]/div/div/div

พบสต็อกการ์ดทอง
    [Arguments]                          ${value}
    ${count} =                           Get Element Count                                                                                         id:billID
    ${count_str} =                       Convert To String                                                                                         ${count}
    Should Be Equal                      ${count_str}                                                                                              ${value}

กดปุ่มปิดสต็อกการ์ทอง
    Wait Until Page Contains Element     id:btnClose                                                                                               300
    Click Element                        id:btnClose

# report
ไปลิงค์รายงานพร้อม start_date end_date
    [Arguments]                          ${link}                                                                                                   ${date1}                                     ${date2}
    Goto                                 ${BASE_URL}report/${link}?start_date=${date1}&end_date=${date2}

ไปลิงค์รายงานพร้อม start_date
    [Arguments]                          ${link}                                                                                                   ${date}
    Goto                                 ${BASE_URL}report/${link}?start_date=${date}

ไปลิงค์รายงานพร้อม date_lt
    [Arguments]                          ${link}                                                                                                   ${date}
    Goto                                 ${BASE_URL}report/${link}?date_lt=${date}

เลือกรายการ
    [Arguments]                          ${path}
    Wait Until Page Contains Element     ${path}                                                                                                   300
    Click Element                        ${path}

กรอกข้อมูลและตัวเลือก
    [Arguments]                          ${value1}                                                                                                 ${value2}
    Wait Until Element Is Visible        //*[@id="content-body"]/div/div/div/form/div[${value1}]/div                                               300
    Sleep                                1
    Click Element                        //*[@id="content-body"]/div/div/div/form/div[${value1}]/div
    Wait Until Element Is Visible        //*[@id="content-body"]/div/div/div/form/div[${value1}]/div/div[2]/div[${value2}]                         300
    Click Element                        //*[@id="content-body"]/div/div/div/form/div[${value1}]/div/div[2]/div[${value2}]

กรอกข้อมูลและจำนวน
    [Arguments]                          ${value1}                                                                                                 ${value2}
    Wait Until Element Is Visible        //*[@id="content-body"]/div/div/div/form/div[${value1}]/div                                               300
    Click Element                        //*[@id="content-body"]/div/div/div/form/div[${value1}]/div
    Input Text                           //*[@id="content-body"]/div/div/div/form/div[${value1}]/div/input                                         ${value2}

กด พิมพ์
    Wait Until Element Is Visible        //*[@id="content-body"]/div/div/div/form/button[1]                                                        300
    Click Element                        //*[@id="content-body"]/div/div/div/form/button[1]
    Wait Until Page Does Not Contain     Loading                                                                                                   300

ปิด Preview
    Wait Until Element Is Enabled        id:btnClosePreview                                                                                        300
    Click Element                        id:btnClosePreview

เปรียบเทียบข้อความ
    [Arguments]                          ${loc}                                                                                                    ${expected}
    Wait Until Element Is Visible        ${loc}                                                                                                    300
    ${Value}=                            Get Text                                                                                                  ${loc}
    Should Be Equal As Strings           ${Value}                                                                                                  ${expected}

เปรียบเทียบข้อความ input field
    [Arguments]                          ${loc}                                                                                                    ${expected}
    Wait Until Element Is Visible        ${loc}                                                                                                    300
    ${Value}=                            Get Value                                                                                                 ${loc}
    Should Be Equal As Strings           ${Value}                                                                                                  ${expected}

init V1
    init_data                            V11
    Sleep                                2
    init_data                            V12
    Sleep                                2
    init_data                            V13
    Sleep                                2
    init_data                            V14
    Sleep                                2
    init_data                            V15
    Sleep                                2
    init_data                            V18
    Sleep                                2
    init_data                            V19
    Sleep                                2
init Information
    init_data                            infoCustomeretc
    Sleep                                2
    init_data                            product
    Sleep                                2
    init_data                            bill
    Sleep                                2
    init_data                            ledger
    Sleep                                2
    init_data                            lease
    Sleep                                2
    init_data                            score
    Sleep                                2
init Admin
    init_data                            admin_1
    Sleep                                2
    init_data                            admin_2
    Sleep                                2
    init_data                            admin_3
    Sleep                                2
    init_data                            admin_4
    Sleep                                2
    init_data                            admin_5
    Sleep                                2
    init_data                            admin_6
    Sleep                                2
    init_data                            admin_7
    Sleep                                2
    init_data                            admin_8
    Sleep                                2
    init_data                            admin_9
    Sleep                                2
    init_data                            admin_10
    Sleep                                2
    init_data                            admin_11
    Sleep                                2
    init_data                            admin_12
    Sleep                                2
    init_data                            admin_13
    Sleep                                2
    init_data                            admin_14
    Sleep                                2
    init_data                            admin_15
    Sleep                                2
    init_data                            admin_16
    Sleep                                2
    init_data                            admin_17
    Sleep                                2
    init_data                            admin_18
    Sleep                                2
    init_data                            admin_19
    Sleep                                2
    init_data                            admin_20
    Sleep                                2
    init_data                            admin_21
    Sleep                                2
    init_data                            admin_22
    Sleep                                2
    init_data                            admin_23
    Sleep                                2
    init_data                            admin_24
    Sleep                                2
    init_data                            admin_25
    Sleep                                2
    init_data                            admin_27
    Sleep                                2
    init_data                            admin_28
    Sleep                                2
    init_data                            admin_29
    Sleep                                2
    init_data                            admin_30
    Sleep                                2
    init_data                            admin_31
    Sleep                                2
    init_data                            admin_32
    Sleep                                2
    init_data                            admin_33
    Sleep                                2
    init_data                            admin_34
    Sleep                                2
    init_data                            admin_35
    Sleep                                2
    init_data                            admin_36
    Sleep                                2
    init_data                            admin_37
    Sleep                                2
    init_data                            admin_38
    Sleep                                2
    init_data                            admin_39
    Sleep                                2
    init_data                            admin_40
    Sleep                                2
    init_data                            admin_41
    Sleep                                2
    init_data                            admin_42
    Sleep                                2
    # init_data       admin_26
init Report
    init_data                            report_1
    init_data                            report_2
    init_data                            report_3
    init_data                            report_4
    init_data                            report_5
    init_data                            report_6
    init_data                            report_7
    init_data                            report_8
    init_data                            report_9
    init_data                            report_10
    init_data                            report_11
    init_data                            report_12
    init_data                            report_13
    init_data                            report_14
    init_data                            report_15
    init_data                            report_16
    init_data                            report_17
    init_data                            report_18
    init_data                            report_19
    init_data                            report_20
    init_data                            report_21
    init_data                            report_22
    init_data                            report_23
    init_data                            report_24
    init_data                            report_25
    init_data                            report_27
    init_data                            report_28
    init_data                            report_29
    init_data                            report_30
    init_data                            report_31
    init_data                            report_32
    init_data                            report_33
    init_data                            report_34
    init_data                            report_35
    init_data                            report_36
    init_data                            report_37
    init_data                            report_38
    init_data                            report_39
    init_data                            report_40
    init_data                            report_41
    init_data                            report_42

